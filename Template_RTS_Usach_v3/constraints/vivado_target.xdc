#-----------------------------
# IMPORTANT
# Use only for RTS Usach v2.1
#-----------------------------

# Clock definitions
create_clock -period 100.000 -name ADCLK1_P -waveform {0.000 50.000} [get_ports ADCLK1_P]
create_clock -period 100.000 -name ADCLK2_P -waveform {0.000 50.000} [get_ports ADCLK2_P]
create_clock -period 16.667 -name LCLK1_P -waveform {0.000 8.334} [get_ports LCLK1_P]
create_clock -period 16.667 -name LCLK2_P -waveform {0.000 8.334} [get_ports LCLK2_P]
create_generated_clock -name pefft_system_i/sample_clock_0/U0/sample_clock_v2_0_S0_AXI_Lite_inst/sample -source [get_pins {pefft_system_i/processing_system7_0/inst/PS7_i/FCLKCLK[0]}] -divide_by 1000 [get_pins pefft_system_i/sample_clock_0/U0/sample_clock_v2_0_S0_AXI_Lite_inst/clk_S_reg/Q]

# Fast ADC ADCLK are connected to Non-clock pins
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets {pefft_system_i/ADC_ads5272_1/U0/data7_S_reg[11]}]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets {pefft_system_i/ADC_ads5272_2/U0/data7_S_reg[11]}]


#FO1_TX1    -   FO_TX1    - B35_IO0
set_property PACKAGE_PIN B21 [get_ports {FO1[0]}]
#FO1_TX2    -   FO_TX2	-	B35_IO1
set_property PACKAGE_PIN A21 [get_ports {FO1[13]}]
#FO1_TX4    -   FO_TX3	-	B35_IO2
set_property PACKAGE_PIN B20 [get_ports {FO1[14]}]
#FO1_TX6    -   FO_TX4	-	B35_IO3
set_property PACKAGE_PIN B19 [get_ports {FO1[15]}]
#FO1_TX8    -   FO_TX5	-	B35_IO4
set_property PACKAGE_PIN A19 [get_ports {FO1[16]}]
#FO1_TX10    -   FO_TX6	-	B35_IO5
set_property PACKAGE_PIN A18 [get_ports {FO1[17]}]
#FO1_TX12    -   FO_TX7	-	B35_IO6
set_property PACKAGE_PIN B17 [get_ports {FO1[18]}]
#FO1_TX14   -   FO_TX8	-	B35_IO7
set_property PACKAGE_PIN A17 [get_ports {FO1[19]}]
#FO1_TX3    -   FO_TX9	-	B35_IO8
set_property PACKAGE_PIN B16 [get_ports {FO1[1]}]
#FO1_TX5    -   FO_TX10	-	B35_IO9
set_property PACKAGE_PIN A16 [get_ports {FO1[2]}]
#FO1_TX7    -   FO_TX11	-	B35_IO10
set_property PACKAGE_PIN B15 [get_ports {FO1[3]}]
#FO1_TX16    -   FO_TX12	-	B35_IO11
set_property PACKAGE_PIN E20 [get_ports {FO1[20]}]
#FO1_TX18    -   FO_TX13	-	B35_IO12
set_property PACKAGE_PIN F19 [get_ports {FO1[21]}]
#FO1_TX20    -   FO_TX14	-	B35_IO13
set_property PACKAGE_PIN D18 [get_ports {FO1[22]}]
#FO1_TX22    -   FO_TX15	-	B35_IO14
set_property PACKAGE_PIN E18 [get_ports {FO1[23]}]
#FO1_TX24    -   FO_TX16	-	B35_IO15
set_property PACKAGE_PIN D16 [get_ports {FO1[24]}]
#FO1_TX26    -   FO_TX17	-	B35_IO16
set_property PACKAGE_PIN F18 [get_ports {FO1[25]}]
#FO1_TX25    -   FO_TX18	-	B35_IO17
set_property PACKAGE_PIN G15 [get_ports {FO1[12]}]
#FO1_TX23    -   FO_TX19	-	B35_IO18
set_property PACKAGE_PIN C15 [get_ports {FO1[11]}]
#FO1_TX21    -   FO_TX20	-	B35_IO19
set_property PACKAGE_PIN D15 [get_ports {FO1[10]}]
#FO1_TX19    -   FO_TX21	-	B35_IO20
set_property PACKAGE_PIN F17 [get_ports {FO1[9]}]
#FO1_TX17    -   FO_TX22	-	B35_IO21
set_property PACKAGE_PIN E16 [get_ports {FO1[8]}]
#FO1_TX15    -   FO_TX23	-	B35_IO22
set_property PACKAGE_PIN E15 [get_ports {FO1[7]}]
#FO1_TX9    -   FO_TX24	-	B35_IO23
set_property PACKAGE_PIN D20 [get_ports {FO1[4]}]
#FO1_TX11    -   FO_TX25	-	B35_IO24
set_property PACKAGE_PIN C20 [get_ports {FO1[5]}]
#FO1_TX13    -   FO_TX26	-	B35_IO25
set_property PACKAGE_PIN E19 [get_ports {FO1[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO1[*]}]

#FO2_TX9	-	FO_TX27	-	B33_IO31
set_property PACKAGE_PIN V17 [get_ports {FO2[4]}]
#FO2_TX7	-	FO_TX28	-	B33_IO28
set_property PACKAGE_PIN U15 [get_ports {FO2[3]}]
#FO2_TX5	-	FO_TX29	-	B33_IO2
set_property PACKAGE_PIN T22 [get_ports {FO2[2]}]
#FO2_TX3	-	FO_TX30	-	B33_IO3
set_property PACKAGE_PIN U22 [get_ports {FO2[1]}]
#FO2_TX1	-	FO_TX31	-	B33_IO8
set_property PACKAGE_PIN U20 [get_ports {FO2[0]}]
#FO2_TX2	-	FO_TX32	-	B33_IO30
set_property PACKAGE_PIN U17 [get_ports {FO2[13]}]
#FO2_TX4	-	FO_TX33	-	B33_IO9
set_property PACKAGE_PIN V20 [get_ports {FO2[14]}]
#FO2_TX6	-	FO_TX34	-	B33_IO11
set_property PACKAGE_PIN V19 [get_ports {FO2[15]}]
#FO2_TX8	-	FO_TX35	-	B33_IO6
set_property PACKAGE_PIN W20 [get_ports {FO2[16]}]
#FO2_TX10	-	FO_TX36	-	B33_IO10
set_property PACKAGE_PIN V18 [get_ports {FO2[17]}]
#FO2_TX12	-	FO_TX37	-	B33_IO16
set_property PACKAGE_PIN Y20 [get_ports {FO2[18]}]
#FO2_TX14	-	FO_TX38	-	B33_IO20
set_property PACKAGE_PIN Y19 [get_ports {FO2[19]}]
#FO2_TX13	-	FO_TX39	-	B33_IO25
set_property PACKAGE_PIN W18 [get_ports {FO2[20]}]
#FO2_TX18	-	FO_TX40	-	B33_IO22
set_property PACKAGE_PIN Y18 [get_ports {FO2[21]}]
#FO2_TX20	-	FO_TX41	-	B33_IO24
set_property PACKAGE_PIN W17 [get_ports {FO2[22]}]
#FO2_TX22	-	FO_TX42	-	B33_IO26
set_property PACKAGE_PIN W16 [get_ports {FO2[23]}]
#FO2_TX24	-	FO_TX43	-	B33_IO27
set_property PACKAGE_PIN Y16 [get_ports {FO2[24]}]
#FO2_TX26	-	FO_TX44	-	B33_IO40
set_property PACKAGE_PIN W15 [get_ports {FO2[25]}]
#FO2_TX25	-	FO_TX45	-	B33_IO41
set_property PACKAGE_PIN Y15 [get_ports {FO2[12]}]
#FO2_TX23	-	FO_TX46	-	B33_IO36
set_property PACKAGE_PIN V14 [get_ports {FO2[11]}]
#FO2_TX21	-	FO_TX47	-	B33_IO42
set_property PACKAGE_PIN Y14 [get_ports {FO2[10]}]
#FO2_TX19	-	FO_TX48	-	B33_IO39
set_property PACKAGE_PIN W13 [get_ports {FO2[9]}]
#FO2_TX17	-	FO_TX49	-	B33_IO44
set_property PACKAGE_PIN Y13 [get_ports {FO2[8]}]
#FO2_TX15	-	FO_TX50	-	B33_IO38
set_property PACKAGE_PIN V13 [get_ports {FO2[7]}]
#FO2_TX11	-	FO_TX51	-	B33_IO29
set_property PACKAGE_PIN U16 [get_ports {FO2[5]}]
#FO2_TX13	-	FO_TX52	-	B33_IO37
set_property PACKAGE_PIN V15 [get_ports {FO2[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO2[*]}]

#FO3_TX18	-	FO_TX53	-	DISP_RX_CLK_N
set_property PACKAGE_PIN R21 [get_ports {FO3[22]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[22]}]
#FO3_TX20	-	FO_TX54	-	DISP_RX_CLK_P
set_property PACKAGE_PIN R20 [get_ports {FO3[23]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[23]}]
#FO3_TX22	-	FO_TX55	-	DISP_RX3_N
set_property PACKAGE_PIN P22 [get_ports {FO3[24]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[24]}]
#FO3_TX24	-	FO_TX56	-	DISP_RX3_P
set_property PACKAGE_PIN N22 [get_ports {FO3[25]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[25]}]
#FO3_TX26	-	FO_TX57	-	DISP_RX2_N
set_property PACKAGE_PIN M22 [get_ports {FO3[12]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[12]}]
#FO3_TX25	-	FO_TX58	-	DISP_RX2_P
set_property PACKAGE_PIN M21 [get_ports {FO3[11]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[11]}]
#FO3_TX23	-	FO_TX59	-	DISP_RX1_N
set_property PACKAGE_PIN L22 [get_ports {FO3[10]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[10]}]
#FO3_TX21	-	FO_TX60	-	DISP_RX1_P
set_property PACKAGE_PIN L21 [get_ports {FO3[9]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[9]}]
#FO3_TX19	-	FO_TX61	-	DISP_RX0_N
set_property PACKAGE_PIN J22 [get_ports {FO3[8]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[8]}]
#FO3_TX17	-	FO_TX62	-	DISP_RX0_P
set_property PACKAGE_PIN J21 [get_ports {FO3[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[7]}]
#FO3_TX16	-	FO_TX63	-	B33_IO0
set_property PACKAGE_PIN T21 [get_ports {FO3[20]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[20]}]
#FO3_TX14	-	FO_TX64	-	B33_IO1
set_property PACKAGE_PIN U21 [get_ports {FO3[19]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[19]}]
#FO3_TX12	-	FO_TX65	-	B33_IO4
set_property PACKAGE_PIN V22 [get_ports {FO3[18]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[18]}]
#FO3_TX10	-	FO_TX66	-	B33_IO5
set_property PACKAGE_PIN W22 [get_ports {FO3[17]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[17]}]
#FO3_TX8	-	FO_TX67	-	B33_IO7
set_property PACKAGE_PIN W21 [get_ports {FO3[16]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[16]}]
#FO3_TX6	-	FO_TX68	-	B33_IO17
set_property PACKAGE_PIN Y21 [get_ports {FO3[15]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[15]}]
#FO3_TX4	-	FO_TX69	-	B33_IO12
set_property PACKAGE_PIN AA22 [get_ports {FO3[14]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[14]}]
#FO3_TX2	-	FO_TX70	-	B13_L7_P
set_property PACKAGE_PIN AA12 [get_ports {FO3[13]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[13]}]
#FO3_TX1	-	FO_TX71	-	B13_L7_N
set_property PACKAGE_PIN AB12 [get_ports {FO3[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[0]}]
#FO3_TX3	-	FO_TX72	-	B13_L8_P
set_property PACKAGE_PIN AA11 [get_ports {FO3[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[1]}]
#FO3_TX5	-	FO_TX73	-	B13_L8_N
set_property PACKAGE_PIN AB11 [get_ports {FO3[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[2]}]
#FO3_TX7	-	FO_TX74	-	B13_L9_P
set_property PACKAGE_PIN AB10 [get_ports {FO3[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[3]}]
#FO3_TX9	-	FO_TX75	-	B13_L9_N
set_property PACKAGE_PIN AB9 [get_ports {FO3[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[4]}]
#FO3_TX11	-	FO_TX76	-	B13_L17_P
set_property PACKAGE_PIN AB7 [get_ports {FO3[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[5]}]
#FO3_TX13	-	FO_TX77	-	B13_L17_N
set_property PACKAGE_PIN AB6 [get_ports {FO3[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[6]}]
#FO3_TX13   -   FO_TX78	-	B35_IO27
set_property PACKAGE_PIN C18 [get_ports {FO3[21]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[21]}]


#ADCLK  -   B35_IO29
set_property PACKAGE_PIN C17 [get_ports ADCLK]
set_property IOSTANDARD LVCMOS33 [get_ports ADCLK]

#ADCLK_P1   -   B13_L18_P
#ADCLK_N1   -   B13_L18_N
set_property PACKAGE_PIN AA4 [get_ports ADCLK1_N]
set_property PACKAGE_PIN Y4 [get_ports ADCLK1_P]
#ADCLK_P2   -   B13_L4_P
#ADCLK_N2   -   B13_L4_N
set_property PACKAGE_PIN W12 [get_ports ADCLK2_N]
set_property PACKAGE_PIN V12 [get_ports ADCLK2_P]
set_property IOSTANDARD LVDS_25 [get_ports ADCLK1*]
set_property IOSTANDARD LVDS_25 [get_ports ADCLK2*]
set_property DIFF_TERM true [get_ports ADCLK1*]
set_property DIFF_TERM true [get_ports ADCLK2*]

#LCLK_P1   -   B13_L11_P
#LCLK_N1   -   B13_L11_N
set_property PACKAGE_PIN AA8 [get_ports LCLK1_N]
set_property PACKAGE_PIN AA9 [get_ports LCLK1_P]
#LCLK_P2   -   B13_L14_P
#LCLK_N2   -   B13_L14_N
set_property PACKAGE_PIN AA6 [get_ports LCLK2_N]
set_property PACKAGE_PIN AA7 [get_ports LCLK2_P]
set_property IOSTANDARD LVDS_25 [get_ports LCLK*]
set_property DIFF_TERM true [get_ports LCLK*]

#ADC_P1   -   B13_L23_P
#ADC_N1   -   B13_L23_N
set_property PACKAGE_PIN W7 [get_ports {ADC1_N[0]}]
set_property PACKAGE_PIN V7 [get_ports {ADC1_P[0]}]
#ADC_P2   -   B13_L19_P
#ADC_N2   -   B13_L19_N
set_property PACKAGE_PIN T6 [get_ports {ADC1_N[1]}]
set_property PACKAGE_PIN R6 [get_ports {ADC1_P[1]}]
#ADC_P3   -   B13_L22_P
#ADC_N3   -   B13_L22_N
set_property PACKAGE_PIN U5 [get_ports {ADC1_N[2]}]
set_property PACKAGE_PIN U6 [get_ports {ADC1_P[2]}]
#ADC_P4   -   B13_L6_P
#ADC_N4   -   B13_L6_N
set_property PACKAGE_PIN U9 [get_ports {ADC1_N[3]}]
set_property PACKAGE_PIN U10 [get_ports {ADC1_P[3]}]
#ADC_P5   -   B13_L1_P
#ADC_N5   -   B13_L1_N
set_property PACKAGE_PIN V9 [get_ports {ADC1_N[4]}]
set_property PACKAGE_PIN V10 [get_ports {ADC1_P[4]}]
#ADC_P6   -   B13_L5_P
#ADC_N6   -   B13_L5_N
set_property PACKAGE_PIN U11 [get_ports {ADC1_N[5]}]
set_property PACKAGE_PIN U12 [get_ports {ADC1_P[5]}]
#ADC_P7   -   B13_L15_P
#ADC_N7   -   B13_L15_N
set_property PACKAGE_PIN AB1 [get_ports {ADC1_N[6]}]
set_property PACKAGE_PIN AB2 [get_ports {ADC1_P[6]}]
#ADC_P8   -   B13_L16_P
#ADC_N8   -   B13_L16_N
set_property PACKAGE_PIN AB4 [get_ports {ADC1_N[7]}]
set_property PACKAGE_PIN AB5 [get_ports {ADC1_P[7]}]
#ADC_P9   -   B13_L12_P
#ADC_N9   -   B13_L12_N
set_property PACKAGE_PIN Y8 [get_ports {ADC2_N[0]}]
set_property PACKAGE_PIN Y9 [get_ports {ADC2_P[0]}]
#ADC_P10   -   B13_L3_P
#ADC_N10   -   B13_L3_N
set_property PACKAGE_PIN W10 [get_ports {ADC2_N[1]}]
set_property PACKAGE_PIN W11 [get_ports {ADC2_P[1]}]
#ADC_P11   -   B13_L10_P
#ADC_N11   -   B13_L10_N
set_property PACKAGE_PIN Y10 [get_ports {ADC2_N[2]}]
set_property PACKAGE_PIN Y11 [get_ports {ADC2_P[2]}]
#ADC_P12   -   B13_L2_P
#ADC_N12   -   B13_L2_N
set_property PACKAGE_PIN W8 [get_ports {ADC2_N[3]}]
set_property PACKAGE_PIN V8 [get_ports {ADC2_P[3]}]
#ADC_P13   -   B13_L21_P
#ADC_N13   -
set_property PACKAGE_PIN V4 [get_ports {ADC2_N[4]}]
set_property PACKAGE_PIN V5 [get_ports {ADC2_P[4]}]
#ADC_P14   -   B13_L13_P
#ADC_N14   -   B13_L13_N
set_property PACKAGE_PIN Y5 [get_ports {ADC2_N[5]}]
set_property PACKAGE_PIN Y6 [get_ports {ADC2_P[5]}]
#ADC_P15   -   B13_L24_P
#ADC_N15   -   B13_L24_N
set_property PACKAGE_PIN W5 [get_ports {ADC2_N[6]}]
set_property PACKAGE_PIN W6 [get_ports {ADC2_P[6]}]
#ADC_P16   -   B13_L20_P
#ADC_N16   -   B13_L20_N
set_property PACKAGE_PIN U4 [get_ports {ADC2_N[7]}]
set_property PACKAGE_PIN T4 [get_ports {ADC2_P[7]}]
set_property IOSTANDARD LVDS_25 [get_ports ADC1*]
set_property IOSTANDARD LVDS_25 [get_ports ADC2*]
set_property DIFF_TERM true [get_ports ADC1*]
set_property DIFF_TERM true [get_ports ADC1*]


#CLOCK_S_ADC    -   B33_IO43
set_property PACKAGE_PIN AA14 [get_ports SPI_sck]
#MOSI_S_ADC    -   B33_IO45
set_property PACKAGE_PIN AA13 [get_ports SPI_mosi]
#CS_S_ADC    -   B33_IO46
set_property PACKAGE_PIN AB14 [get_ports SPI_ss_n]
#MISO_S_ADC1    -   B33_IO33
set_property PACKAGE_PIN AB17 [get_ports {SPI_miso[0]}]
#MISO_S_ADC2    -   B33_IO32
set_property PACKAGE_PIN AA17 [get_ports {SPI_miso[1]}]
#MISO_S_ADC3    -   B33_IO23
set_property PACKAGE_PIN AA18 [get_ports {SPI_miso[2]}]
#MISO_S_ADC4    -   B33_IO34
set_property PACKAGE_PIN AA16 [get_ports {SPI_miso[3]}]
#MISO_S_ADC5    -   B33_IO47
set_property PACKAGE_PIN AB15 [get_ports {SPI_miso[4]}]
#MISO_S_ADC6    -   B33_IO35
set_property PACKAGE_PIN AB16 [get_ports {SPI_miso[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports SPI_*]

#TRIP_GEN	-	B35_IO28
set_property PACKAGE_PIN D17 [get_ports trip]
set_property IOSTANDARD LVCMOS33 [get_ports trip]

#CPLD_ready	-	B35_IO26
set_property PACKAGE_PIN C19 [get_ports CPLD_ready]
set_property IOSTANDARD LVCMOS33 [get_ports CPLD_ready]


#ENC1A	-	B33_IO14
set_property PACKAGE_PIN AA21 [get_ports {Encoder1[0]}]
#ENC1B	-	B33_IO15
set_property PACKAGE_PIN AB21 [get_ports {Encoder1[1]}]
#ENC1C	-	B33_IO13
set_property PACKAGE_PIN AB22 [get_ports {Encoder1[2]}]
#ENC2A	-	B33_IO21
set_property PACKAGE_PIN AA19 [get_ports {Encoder2[0]}]
#ENC2B	-	B33_IO19
set_property PACKAGE_PIN AB20 [get_ports {Encoder2[1]}]
#ENC2C	-	B33_IO18
set_property PACKAGE_PIN AB19 [get_ports {Encoder2[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {Encoder1[*]}]
set_property IOSTANDARD LVCMOS33 [get_ports {Encoder2[*]}]


