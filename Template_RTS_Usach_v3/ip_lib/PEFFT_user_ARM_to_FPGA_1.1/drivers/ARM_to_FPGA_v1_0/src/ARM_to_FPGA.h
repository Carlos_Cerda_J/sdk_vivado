
#ifndef ARM_TO_FPGA_H
#define ARM_TO_FPGA_H


/****************** Include Files ********************/
#include "xil_types.h"
#include "xstatus.h"

#define ARM_TO_FPGA_id_pos_OFFSET           0
#define ARM_TO_FPGA_iq_pos_OFFSET           4
#define ARM_TO_FPGA_id_neg_OFFSET           8
#define ARM_TO_FPGA_iq_neg_OFFSET           12
#define ARM_TO_FPGA_id_sigma_s_pos_OFFSET   16
#define ARM_TO_FPGA_iq_sigma_s_pos_OFFSET   20
#define ARM_TO_FPGA_id_sigma_s_neg_OFFSET   24
#define ARM_TO_FPGA_iq_sigma_s_neg_OFFSET   28
#define ARM_TO_FPGA_id_sigma_o_pos_OFFSET   32
#define ARM_TO_FPGA_iq_sigma_o_pos_OFFSET   36
#define ARM_TO_FPGA_id_sigma_o_neg_OFFSET   40
#define ARM_TO_FPGA_iq_sigma_o_neg_OFFSET   44
#define ARM_TO_FPGA_extra_1_OFFSET          48
#define ARM_TO_FPGA_extra_2_OFFSET          52
#define ARM_TO_FPGA_S0_AXI_Lite_SLV_REG14_OFFSET 56
#define ARM_TO_FPGA_S0_AXI_Lite_SLV_REG15_OFFSET 60


/**************************** Type Definitions *****************************/
/**
 *
 * Write a value to a ARM_TO_FPGA register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the ARM_TO_FPGAdevice.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void ARM_TO_FPGA_mWriteReg(u32 BaseAddress, unsigned RegOffset, u32 Data)
 *
 */
#define ARM_TO_FPGA_mWriteReg(BaseAddress, RegOffset, Data) \
  	Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))

/**
 *
 * Read a value from a ARM_TO_FPGA register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the ARM_TO_FPGA device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	u32 ARM_TO_FPGA_mReadReg(u32 BaseAddress, unsigned RegOffset)
 *
 */
#define ARM_TO_FPGA_mReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))

/************************** Function Prototypes ****************************/
/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the ARM_TO_FPGA instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus ARM_TO_FPGA_Reg_SelfTest(void * baseaddr_p);

#endif // ARM_TO_FPGA_H
