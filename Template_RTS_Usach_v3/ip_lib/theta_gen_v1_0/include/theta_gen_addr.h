/*
 * File Name:         C:\IPCORES\THETA_GEN\ipcore\theta_gen_v1_0\include\theta_gen_addr.h
 * Description:       C Header File
 * Created:           2019-01-10 21:38:40
*/

#ifndef THETA_GEN_H_
#define THETA_GEN_H_

#define  IPCore_Reset_theta_gen       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_theta_gen      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_theta_gen   0x8  //contains unique IP timestamp (yymmddHHMM): 1901102138

#endif /* THETA_GEN_H_ */
