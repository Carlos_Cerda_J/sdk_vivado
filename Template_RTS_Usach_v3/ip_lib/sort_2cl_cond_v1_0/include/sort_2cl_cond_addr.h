/*
 * File Name:         F:\xilinx\sort_sm\ipcore\sort_2cl_cond_v1_0\include\sort_2cl_cond_addr.h
 * Description:       C Header File
 * Created:           2019-05-13 10:10:22
*/

#ifndef SORT_2CL_COND_H_
#define SORT_2CL_COND_H_

#define  IPCore_Reset_sort_2cl_cond       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_sort_2cl_cond      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_sort_2cl_cond   0x8  //contains unique IP timestamp (yymmddHHMM): 1905131010

#endif /* SORT_2CL_COND_H_ */
