/*
 * File Name:         hdl_prj\ipcore\NLC_2cell_dual_M2C_v1_0\include\NLC_2cell_dual_M2C_addr.h
 * Description:       C Header File
 * Created:           2021-01-25 12:37:17
*/

#ifndef NLC_2CELL_DUAL_M2C_H_
#define NLC_2CELL_DUAL_M2C_H_

#define  IPCore_Reset_NLC_2cell_dual_M2C        0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_NLC_2cell_dual_M2C       0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_NLC_2cell_dual_M2C    0x8  //contains unique IP timestamp (yymmddHHMM): 2101251237
#define  Vdc_inv_Data_NLC_2cell_dual_M2C        0x100  //data register for Inport Vdc_inv
#define  Vdc_inv_prop_Data_NLC_2cell_dual_M2C   0x104  //data register for Inport Vdc_inv_prop
#define  selector_Data_NLC_2cell_dual_M2C       0x108  //data register for Inport selector

#endif /* NLC_2CELL_DUAL_M2C_H_ */
