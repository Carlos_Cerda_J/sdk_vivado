/*
 * File Name:         hdl_prj\ipcore\Notch_cluster_currents_v1_0\include\Notch_cluster_currents_addr.h
 * Description:       C Header File
 * Created:           2021-03-18 13:57:19
*/

#ifndef NOTCH_CLUSTER_CURRENTS_H_
#define NOTCH_CLUSTER_CURRENTS_H_

#define  IPCore_Reset_Notch_cluster_currents       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_Notch_cluster_currents      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_Notch_cluster_currents   0x8  //contains unique IP timestamp (yymmddHHMM): 2103181357
#define  selector_Data_Notch_cluster_currents      0x100  //data register for Inport selector
#define  activator_Data_Notch_cluster_currents     0x104  //data register for Inport activator
#define  i_ap_arm_Data_Notch_cluster_currents      0x108  //data register for Inport i_ap_arm
#define  i_bp_arm_Data_Notch_cluster_currents      0x10C  //data register for Inport i_bp_arm
#define  i_cp_arm_Data_Notch_cluster_currents      0x110  //data register for Inport i_cp_arm
#define  i_an_arm_Data_Notch_cluster_currents      0x114  //data register for Inport i_an_arm
#define  i_bn_arm_Data_Notch_cluster_currents      0x118  //data register for Inport i_bn_arm
#define  i_cn_arm_Data_Notch_cluster_currents      0x11C  //data register for Inport i_cn_arm
#define  a0_Data_Notch_cluster_currents            0x120  //data register for Inport a0
#define  a1_Data_Notch_cluster_currents            0x124  //data register for Inport a1
#define  a2_Data_Notch_cluster_currents            0x128  //data register for Inport a2
#define  b1_Data_Notch_cluster_currents            0x12C  //data register for Inport b1
#define  b2_Data_Notch_cluster_currents            0x130  //data register for Inport b2

#endif /* NOTCH_CLUSTER_CURRENTS_H_ */
