-- -------------------------------------------------------------
-- 
-- File Name: hdl_prj\hdlsrc\Notch_cluster_currents\Notch_cluster_currents_src_Notch_Filter_block.vhd
-- Created: 2021-03-18 13:57:11
-- 
-- Generated by MATLAB 9.8 and HDL Coder 3.16
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: Notch_cluster_currents_src_Notch_Filter_block
-- Source Path: Notch_cluster_currents/Notch_cluster_currents/filter_iap/Notch_Filter
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE work.Notch_cluster_currents_src_Notch_cluster_currents_pkg.ALL;

ENTITY Notch_cluster_currents_src_Notch_Filter_block IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb                               :   IN    std_logic;
        enb_1_2000_1                      :   IN    std_logic;
        enb_1_2000_0                      :   IN    std_logic;
        signal_in                         :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        a0                                :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En23
        a1                                :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En23
        a2                                :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En23
        b1                                :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En23
        b2                                :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En23
        activator                         :   IN    std_logic_vector(15 DOWNTO 0);  -- int16
        signal_out                        :   OUT   std_logic_vector(24 DOWNTO 0)  -- sfix25_En16
        );
END Notch_cluster_currents_src_Notch_Filter_block;


ARCHITECTURE rtl OF Notch_cluster_currents_src_Notch_Filter_block IS

  -- Signals
  SIGNAL signal_in_1                      : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL delayMatch1_reg                  : vector_of_signed25(0 TO 1);  -- sfix25 [2]
  SIGNAL delayMatch1_reg_next             : vector_of_signed25(0 TO 1);  -- sfix25_En16 [2]
  SIGNAL signal_in_2                      : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL out_rsvd                         : signed(15 DOWNTO 0);  -- int16
  SIGNAL Constant_out1                    : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL Switch_out1                      : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL Switch_out1_1                    : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL a0_1                             : signed(24 DOWNTO 0);  -- sfix25_En23
  SIGNAL a0_2                             : signed(24 DOWNTO 0);  -- sfix25_En23
  SIGNAL out_rsvd_1                       : signed(49 DOWNTO 0);  -- sfix50_En39
  SIGNAL Delay3_bypass_reg                : signed(24 DOWNTO 0);  -- sfix25
  SIGNAL Switch_out1_2                    : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL Delay3_out1                      : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL Delay3_out1_1                    : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL a1_1                             : signed(24 DOWNTO 0);  -- sfix25_En23
  SIGNAL a1_2                             : signed(24 DOWNTO 0);  -- sfix25_En23
  SIGNAL out_rsvd_2                       : signed(49 DOWNTO 0);  -- sfix50_En39
  SIGNAL Switch_out1_3                    : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL Delay2_out1                      : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL Delay2_out1_1                    : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL Delay2_out1_2                    : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL a2_1                             : signed(24 DOWNTO 0);  -- sfix25_En23
  SIGNAL a2_2                             : signed(24 DOWNTO 0);  -- sfix25_En23
  SIGNAL out_rsvd_3                       : signed(49 DOWNTO 0);  -- sfix50_En39
  SIGNAL out_rsvd_4                       : signed(49 DOWNTO 0);  -- sfix50_En39
  SIGNAL out_rsvd_5                       : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL out_rsvd_6                       : signed(49 DOWNTO 0);  -- sfix50_En39
  SIGNAL out_rsvd_7                       : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Add_stage2_add_cast              : signed(27 DOWNTO 0);  -- sfix28_En17
  SIGNAL Add_stage2_add_cast_1            : signed(27 DOWNTO 0);  -- sfix28_En17
  SIGNAL Add_op_stage2                    : signed(27 DOWNTO 0);  -- sfix28_En17
  SIGNAL out_rsvd_8                       : signed(49 DOWNTO 0);  -- sfix50_En39
  SIGNAL out_rsvd_9                       : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Add_stage3_add_cast              : signed(28 DOWNTO 0);  -- sfix29_En17
  SIGNAL Add_stage3_add_cast_1            : signed(27 DOWNTO 0);  -- sfix28_En17
  SIGNAL Add_stage3_add_cast_2            : signed(28 DOWNTO 0);  -- sfix29_En17
  SIGNAL Add_stage3_add_temp              : signed(28 DOWNTO 0);  -- sfix29_En17
  SIGNAL Add_op_stage3                    : signed(27 DOWNTO 0);  -- sfix28_En17
  SIGNAL Constant1_out1                   : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL b1_1                             : signed(24 DOWNTO 0);  -- sfix25_En23
  SIGNAL b1_2                             : signed(24 DOWNTO 0);  -- sfix25_En23
  SIGNAL b2_1                             : signed(24 DOWNTO 0);  -- sfix25_En23
  SIGNAL b2_2                             : signed(24 DOWNTO 0);  -- sfix25_En23
  SIGNAL Switch1_out1                     : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Switch1_out1_1                   : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL out_rsvd_10                      : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL out_rsvd_11                      : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL out_rsvd_12                      : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL out_rsvd_13                      : signed(49 DOWNTO 0);  -- sfix50_En40
  SIGNAL out_rsvd_14                      : signed(49 DOWNTO 0);  -- sfix50_En40
  SIGNAL out_rsvd_15                      : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Add_out1                         : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Switch1_out1_2                   : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Delay1_bypass_reg                : signed(24 DOWNTO 0);  -- sfix25
  SIGNAL Switch1_out1_3                   : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Delay1_out1                      : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Delay1_out1_1                    : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL out_rsvd_16                      : signed(49 DOWNTO 0);  -- sfix50_En40
  SIGNAL out_rsvd_17                      : signed(49 DOWNTO 0);  -- sfix50_En40
  SIGNAL out_rsvd_18                      : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Add_stage4_sub_cast              : signed(28 DOWNTO 0);  -- sfix29_En17
  SIGNAL Add_stage4_sub_cast_1            : signed(27 DOWNTO 0);  -- sfix28_En17
  SIGNAL Add_stage4_sub_cast_2            : signed(28 DOWNTO 0);  -- sfix29_En17
  SIGNAL Add_stage4_sub_temp              : signed(28 DOWNTO 0);  -- sfix29_En17
  SIGNAL Add_op_stage4                    : signed(27 DOWNTO 0);  -- sfix28_En17
  SIGNAL Add_stage5_sub_cast              : signed(28 DOWNTO 0);  -- sfix29_En17
  SIGNAL Add_stage5_sub_cast_1            : signed(27 DOWNTO 0);  -- sfix28_En17
  SIGNAL Add_stage5_sub_cast_2            : signed(28 DOWNTO 0);  -- sfix29_En17
  SIGNAL Add_stage5_sub_temp              : signed(28 DOWNTO 0);  -- sfix29_En17
  SIGNAL Add_stage5_cast                  : signed(27 DOWNTO 0);  -- sfix28_En17
  SIGNAL Data_Type_Conversion8_out1       : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL Sum_out1                         : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL Sum_out1_1                       : signed(24 DOWNTO 0);  -- sfix25_En16

BEGIN
  signal_in_1 <= signed(signal_in);

  delayMatch1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        delayMatch1_reg(0) <= to_signed(16#0000000#, 25);
        delayMatch1_reg(1) <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        delayMatch1_reg(0) <= delayMatch1_reg_next(0);
        delayMatch1_reg(1) <= delayMatch1_reg_next(1);
      END IF;
    END IF;
  END PROCESS delayMatch1_process;

  signal_in_2 <= delayMatch1_reg(1);
  delayMatch1_reg_next(0) <= signal_in_1;
  delayMatch1_reg_next(1) <= delayMatch1_reg(0);

  out_rsvd <= signed(activator);

  Constant_out1 <= to_signed(16#0000000#, 25);

  
  Switch_out1 <= Constant_out1 WHEN out_rsvd = to_signed(16#0000#, 16) ELSE
      signal_in_1;

  reduced_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Switch_out1_1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        Switch_out1_1 <= Switch_out1;
      END IF;
    END IF;
  END PROCESS reduced_process;


  a0_1 <= signed(a0);

  HwModeRegister1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        a0_2 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        a0_2 <= a0_1;
      END IF;
    END IF;
  END PROCESS HwModeRegister1_process;


  out_rsvd_1 <= Switch_out1_1 * a0_2;

  Delay3_bypass_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Delay3_bypass_reg <= to_signed(16#0000000#, 25);
      ELSIF enb_1_2000_1 = '1' THEN
        Delay3_bypass_reg <= Switch_out1_1;
      END IF;
    END IF;
  END PROCESS Delay3_bypass_process;

  
  Switch_out1_2 <= Switch_out1_1 WHEN enb_1_2000_1 = '1' ELSE
      Delay3_bypass_reg;

  Delay3_out1 <= Switch_out1_2;

  HwModeRegister2_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Delay3_out1_1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        Delay3_out1_1 <= Delay3_out1;
      END IF;
    END IF;
  END PROCESS HwModeRegister2_process;


  a1_1 <= signed(a1);

  HwModeRegister3_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        a1_2 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        a1_2 <= a1_1;
      END IF;
    END IF;
  END PROCESS HwModeRegister3_process;


  out_rsvd_2 <= Delay3_out1_1 * a1_2;

  Delay21_output_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Switch_out1_3 <= to_signed(16#0000000#, 25);
      ELSIF enb_1_2000_1 = '1' THEN
        Switch_out1_3 <= Switch_out1_1;
      END IF;
    END IF;
  END PROCESS Delay21_output_process;


  Delay2_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Delay2_out1 <= to_signed(16#0000000#, 25);
      ELSIF enb_1_2000_0 = '1' THEN
        Delay2_out1 <= Switch_out1_3;
      END IF;
    END IF;
  END PROCESS Delay2_process;


  Delay2_out1_1 <= Delay2_out1;

  HwModeRegister4_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Delay2_out1_2 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        Delay2_out1_2 <= Delay2_out1_1;
      END IF;
    END IF;
  END PROCESS HwModeRegister4_process;


  a2_1 <= signed(a2);

  HwModeRegister5_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        a2_2 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        a2_2 <= a2_1;
      END IF;
    END IF;
  END PROCESS HwModeRegister5_process;


  out_rsvd_3 <= Delay2_out1_2 * a2_2;

  PipelineRegister_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        out_rsvd_4 <= to_signed(0, 50);
      ELSIF enb = '1' THEN
        out_rsvd_4 <= out_rsvd_1;
      END IF;
    END IF;
  END PROCESS PipelineRegister_process;


  
  out_rsvd_5 <= "0111111111111111111111111" WHEN (out_rsvd_4(49) = '0') AND (out_rsvd_4(48 DOWNTO 46) /= "000") ELSE
      "1000000000000000000000000" WHEN (out_rsvd_4(49) = '1') AND (out_rsvd_4(48 DOWNTO 46) /= "111") ELSE
      out_rsvd_4(46 DOWNTO 22);

  PipelineRegister1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        out_rsvd_6 <= to_signed(0, 50);
      ELSIF enb = '1' THEN
        out_rsvd_6 <= out_rsvd_2;
      END IF;
    END IF;
  END PROCESS PipelineRegister1_process;


  
  out_rsvd_7 <= "0111111111111111111111111" WHEN (out_rsvd_6(49) = '0') AND (out_rsvd_6(48 DOWNTO 46) /= "000") ELSE
      "1000000000000000000000000" WHEN (out_rsvd_6(49) = '1') AND (out_rsvd_6(48 DOWNTO 46) /= "111") ELSE
      out_rsvd_6(46 DOWNTO 22);

  Add_stage2_add_cast <= resize(out_rsvd_5, 28);
  Add_stage2_add_cast_1 <= resize(out_rsvd_7, 28);
  Add_op_stage2 <= Add_stage2_add_cast + Add_stage2_add_cast_1;

  PipelineRegister2_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        out_rsvd_8 <= to_signed(0, 50);
      ELSIF enb = '1' THEN
        out_rsvd_8 <= out_rsvd_3;
      END IF;
    END IF;
  END PROCESS PipelineRegister2_process;


  
  out_rsvd_9 <= "0111111111111111111111111" WHEN (out_rsvd_8(49) = '0') AND (out_rsvd_8(48 DOWNTO 46) /= "000") ELSE
      "1000000000000000000000000" WHEN (out_rsvd_8(49) = '1') AND (out_rsvd_8(48 DOWNTO 46) /= "111") ELSE
      out_rsvd_8(46 DOWNTO 22);

  Add_stage3_add_cast <= resize(Add_op_stage2, 29);
  Add_stage3_add_cast_1 <= resize(out_rsvd_9, 28);
  Add_stage3_add_cast_2 <= resize(Add_stage3_add_cast_1, 29);
  Add_stage3_add_temp <= Add_stage3_add_cast + Add_stage3_add_cast_2;
  
  Add_op_stage3 <= X"7FFFFFF" WHEN (Add_stage3_add_temp(28) = '0') AND (Add_stage3_add_temp(27) /= '0') ELSE
      X"8000000" WHEN (Add_stage3_add_temp(28) = '1') AND (Add_stage3_add_temp(27) /= '1') ELSE
      Add_stage3_add_temp(27 DOWNTO 0);

  Constant1_out1 <= to_signed(16#0000000#, 25);

  b1_1 <= signed(b1);

  HwModeRegister7_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        b1_2 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        b1_2 <= b1_1;
      END IF;
    END IF;
  END PROCESS HwModeRegister7_process;


  b2_1 <= signed(b2);

  HwModeRegister9_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        b2_2 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        b2_2 <= b2_1;
      END IF;
    END IF;
  END PROCESS HwModeRegister9_process;


  Delay41_output_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Switch1_out1_1 <= to_signed(16#0000000#, 25);
      ELSIF enb_1_2000_1 = '1' THEN
        Switch1_out1_1 <= Switch1_out1;
      END IF;
    END IF;
  END PROCESS Delay41_output_process;


  Delay4_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        out_rsvd_10 <= to_signed(16#0000000#, 25);
      ELSIF enb_1_2000_0 = '1' THEN
        out_rsvd_10 <= Switch1_out1_1;
      END IF;
    END IF;
  END PROCESS Delay4_process;


  out_rsvd_11 <= out_rsvd_10;

  HwModeRegister8_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        out_rsvd_12 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        out_rsvd_12 <= out_rsvd_11;
      END IF;
    END IF;
  END PROCESS HwModeRegister8_process;


  out_rsvd_13 <= out_rsvd_12 * b2_2;

  PipelineRegister4_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        out_rsvd_14 <= to_signed(0, 50);
      ELSIF enb = '1' THEN
        out_rsvd_14 <= out_rsvd_13;
      END IF;
    END IF;
  END PROCESS PipelineRegister4_process;


  
  out_rsvd_15 <= "0111111111111111111111111" WHEN (out_rsvd_14(49) = '0') AND (out_rsvd_14(48 DOWNTO 47) /= "00") ELSE
      "1000000000000000000000000" WHEN (out_rsvd_14(49) = '1') AND (out_rsvd_14(48 DOWNTO 47) /= "11") ELSE
      out_rsvd_14(47 DOWNTO 23);

  
  Switch1_out1_2 <= Constant1_out1 WHEN out_rsvd = to_signed(16#0000#, 16) ELSE
      Add_out1;

  reduced_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Switch1_out1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        Switch1_out1 <= Switch1_out1_2;
      END IF;
    END IF;
  END PROCESS reduced_1_process;


  Delay1_bypass_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Delay1_bypass_reg <= to_signed(16#0000000#, 25);
      ELSIF enb_1_2000_1 = '1' THEN
        Delay1_bypass_reg <= Switch1_out1;
      END IF;
    END IF;
  END PROCESS Delay1_bypass_process;

  
  Switch1_out1_3 <= Switch1_out1 WHEN enb_1_2000_1 = '1' ELSE
      Delay1_bypass_reg;

  Delay1_out1 <= Switch1_out1_3;

  HwModeRegister6_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Delay1_out1_1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        Delay1_out1_1 <= Delay1_out1;
      END IF;
    END IF;
  END PROCESS HwModeRegister6_process;


  out_rsvd_16 <= Delay1_out1_1 * b1_2;

  PipelineRegister3_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        out_rsvd_17 <= to_signed(0, 50);
      ELSIF enb = '1' THEN
        out_rsvd_17 <= out_rsvd_16;
      END IF;
    END IF;
  END PROCESS PipelineRegister3_process;


  
  out_rsvd_18 <= "0111111111111111111111111" WHEN (out_rsvd_17(49) = '0') AND (out_rsvd_17(48 DOWNTO 47) /= "00") ELSE
      "1000000000000000000000000" WHEN (out_rsvd_17(49) = '1') AND (out_rsvd_17(48 DOWNTO 47) /= "11") ELSE
      out_rsvd_17(47 DOWNTO 23);

  Add_stage4_sub_cast <= resize(Add_op_stage3, 29);
  Add_stage4_sub_cast_1 <= resize(out_rsvd_18, 28);
  Add_stage4_sub_cast_2 <= resize(Add_stage4_sub_cast_1, 29);
  Add_stage4_sub_temp <= Add_stage4_sub_cast - Add_stage4_sub_cast_2;
  
  Add_op_stage4 <= X"7FFFFFF" WHEN (Add_stage4_sub_temp(28) = '0') AND (Add_stage4_sub_temp(27) /= '0') ELSE
      X"8000000" WHEN (Add_stage4_sub_temp(28) = '1') AND (Add_stage4_sub_temp(27) /= '1') ELSE
      Add_stage4_sub_temp(27 DOWNTO 0);

  Add_stage5_sub_cast <= resize(Add_op_stage4, 29);
  Add_stage5_sub_cast_1 <= resize(out_rsvd_15, 28);
  Add_stage5_sub_cast_2 <= resize(Add_stage5_sub_cast_1, 29);
  Add_stage5_sub_temp <= Add_stage5_sub_cast - Add_stage5_sub_cast_2;
  
  Add_stage5_cast <= X"7FFFFFF" WHEN (Add_stage5_sub_temp(28) = '0') AND (Add_stage5_sub_temp(27) /= '0') ELSE
      X"8000000" WHEN (Add_stage5_sub_temp(28) = '1') AND (Add_stage5_sub_temp(27) /= '1') ELSE
      Add_stage5_sub_temp(27 DOWNTO 0);
  
  Add_out1 <= "0111111111111111111111111" WHEN (Add_stage5_cast(27) = '0') AND (Add_stage5_cast(26 DOWNTO 24) /= "000") ELSE
      "1000000000000000000000000" WHEN (Add_stage5_cast(27) = '1') AND (Add_stage5_cast(26 DOWNTO 24) /= "111") ELSE
      Add_stage5_cast(24 DOWNTO 0);

  Data_Type_Conversion8_out1 <= resize(Add_out1(24 DOWNTO 1), 25);

  Sum_out1 <= signal_in_2 - Data_Type_Conversion8_out1;

  delayMatch_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Sum_out1_1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' THEN
        Sum_out1_1 <= Sum_out1;
      END IF;
    END IF;
  END PROCESS delayMatch_process;


  signal_out <= std_logic_vector(Sum_out1_1);

END rtl;

