SLStudio.Utils.RemoveHighlighting(get_param('sorting_3cell_sm_trigg_cond', 'handle'));
SLStudio.Utils.RemoveHighlighting(get_param('gm_sorting_3cell_sm_trigg_cond', 'handle'));
annotate_port('gm_sorting_3cell_sm_trigg_cond/a_plus_bc/Product', 0, 1, '');
annotate_port('gm_sorting_3cell_sm_trigg_cond/a_plus_bc/PipelineRegister', 1, 1, '');
annotate_port('gm_sorting_3cell_sm_trigg_cond/a_plus_bc/HwModeRegister', 0, 1, '');
