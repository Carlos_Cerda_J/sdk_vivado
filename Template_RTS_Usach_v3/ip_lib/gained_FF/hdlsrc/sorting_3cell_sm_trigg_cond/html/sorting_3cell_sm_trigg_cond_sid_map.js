function RTW_SidParentMap() {
    this.sidParentMap = new Array();
    this.sidParentMap["sorting_3cell_sm_trigg_cond:923"] = "sorting_3cell_sm_trigg_cond:922";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:924"] = "sorting_3cell_sm_trigg_cond:922";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:925"] = "sorting_3cell_sm_trigg_cond:922";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:974"] = "sorting_3cell_sm_trigg_cond:922";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:975"] = "sorting_3cell_sm_trigg_cond:922";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:976"] = "sorting_3cell_sm_trigg_cond:922";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:972"] = "sorting_3cell_sm_trigg_cond:922";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:977"] = "sorting_3cell_sm_trigg_cond:922";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:973"] = "sorting_3cell_sm_trigg_cond:922";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:978"] = "sorting_3cell_sm_trigg_cond:922";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:954"] = "sorting_3cell_sm_trigg_cond:922";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:979"] = "sorting_3cell_sm_trigg_cond:922";
    this.getParentSid = function(sid) { return this.sidParentMap[sid];}
}
    RTW_SidParentMap.instance = new RTW_SidParentMap();
