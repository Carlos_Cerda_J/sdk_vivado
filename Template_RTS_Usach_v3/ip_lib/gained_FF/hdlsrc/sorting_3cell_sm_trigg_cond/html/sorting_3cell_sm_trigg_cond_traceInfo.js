function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <S10>/PR1 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:923"] = "msg=&block=sorting_3cell_sm_trigg_cond:923";
	/* <S10>/FF_factor1 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:924"] = "msg=&block=sorting_3cell_sm_trigg_cond:924";
	/* <S10>/FF1 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:925"] = "msg=&block=sorting_3cell_sm_trigg_cond:925";
	/* <S10>/PR2 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:974"] = "msg=&block=sorting_3cell_sm_trigg_cond:974";
	/* <S10>/FF_factor2 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:975"] = "msg=&block=sorting_3cell_sm_trigg_cond:975";
	/* <S10>/FF2 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:976"] = "msg=&block=sorting_3cell_sm_trigg_cond:976";
	/* <S10>/Product */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:972"] = "gained_FF_src_a_plus_bc.vhd:134,135";
	/* <S10>/Product1 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:977"] = "gained_FF_src_a_plus_bc.vhd:197,198";
	/* <S10>/Sum */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:973"] = "gained_FF_src_a_plus_bc.vhd:149";
	/* <S10>/Sum1 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:978"] = "gained_FF_src_a_plus_bc.vhd:212";
	/* <S10>/PR_FF1 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:954"] = "msg=&block=sorting_3cell_sm_trigg_cond:954";
	/* <S10>/PR_FF2 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:979"] = "msg=&block=sorting_3cell_sm_trigg_cond:979";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "sorting_3cell_sm_trigg_cond"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S10>/PR1"] = {sid: "sorting_3cell_sm_trigg_cond:923"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:923"] = {rtwname: "<S10>/PR1"};
	this.rtwnameHashMap["<S10>/FF_factor1"] = {sid: "sorting_3cell_sm_trigg_cond:924"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:924"] = {rtwname: "<S10>/FF_factor1"};
	this.rtwnameHashMap["<S10>/FF1"] = {sid: "sorting_3cell_sm_trigg_cond:925"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:925"] = {rtwname: "<S10>/FF1"};
	this.rtwnameHashMap["<S10>/PR2"] = {sid: "sorting_3cell_sm_trigg_cond:974"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:974"] = {rtwname: "<S10>/PR2"};
	this.rtwnameHashMap["<S10>/FF_factor2"] = {sid: "sorting_3cell_sm_trigg_cond:975"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:975"] = {rtwname: "<S10>/FF_factor2"};
	this.rtwnameHashMap["<S10>/FF2"] = {sid: "sorting_3cell_sm_trigg_cond:976"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:976"] = {rtwname: "<S10>/FF2"};
	this.rtwnameHashMap["<S10>/Product"] = {sid: "sorting_3cell_sm_trigg_cond:972"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:972"] = {rtwname: "<S10>/Product"};
	this.rtwnameHashMap["<S10>/Product1"] = {sid: "sorting_3cell_sm_trigg_cond:977"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:977"] = {rtwname: "<S10>/Product1"};
	this.rtwnameHashMap["<S10>/Sum"] = {sid: "sorting_3cell_sm_trigg_cond:973"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:973"] = {rtwname: "<S10>/Sum"};
	this.rtwnameHashMap["<S10>/Sum1"] = {sid: "sorting_3cell_sm_trigg_cond:978"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:978"] = {rtwname: "<S10>/Sum1"};
	this.rtwnameHashMap["<S10>/PR_FF1"] = {sid: "sorting_3cell_sm_trigg_cond:954"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:954"] = {rtwname: "<S10>/PR_FF1"};
	this.rtwnameHashMap["<S10>/PR_FF2"] = {sid: "sorting_3cell_sm_trigg_cond:979"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:979"] = {rtwname: "<S10>/PR_FF2"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
