-- -------------------------------------------------------------
-- 
-- File Name: F:\sort_3_cell\gained_FF\hdlsrc\sorting_3cell_sm_trigg_cond\gained_FF_addr_decoder.vhd
-- Created: 2019-05-24 12:08:01
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: gained_FF_addr_decoder
-- Source Path: gained_FF/gained_FF_axi_lite/gained_FF_addr_decoder
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY gained_FF_addr_decoder IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        data_write                        :   IN    std_logic_vector(31 DOWNTO 0);  -- ufix32
        addr_sel                          :   IN    std_logic_vector(13 DOWNTO 0);  -- ufix14
        wr_enb                            :   IN    std_logic;  -- ufix1
        rd_enb                            :   IN    std_logic;  -- ufix1
        read_ip_timestamp                 :   IN    std_logic_vector(31 DOWNTO 0);  -- ufix32
        data_read                         :   OUT   std_logic_vector(31 DOWNTO 0);  -- ufix32
        write_axi_enable                  :   OUT   std_logic;  -- ufix1
        write_FF_factor1                  :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        write_FF_factor2                  :   OUT   std_logic_vector(24 DOWNTO 0)  -- sfix25_En13
        );
END gained_FF_addr_decoder;


ARCHITECTURE rtl OF gained_FF_addr_decoder IS

  -- Signals
  SIGNAL enb                              : std_logic;
  SIGNAL addr_sel_unsigned                : unsigned(13 DOWNTO 0);  -- ufix14
  SIGNAL decode_sel_ip_timestamp          : std_logic;  -- ufix1
  SIGNAL read_ip_timestamp_unsigned       : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL const_1                          : std_logic;  -- ufix1
  SIGNAL const_0                          : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_ip_timestamp            : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_ip_timestamp           : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_sel_axi_enable            : std_logic;  -- ufix1
  SIGNAL reg_enb_axi_enable               : std_logic;  -- ufix1
  SIGNAL data_write_unsigned              : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL data_in_axi_enable               : std_logic;  -- ufix1
  SIGNAL write_reg_axi_enable             : std_logic;  -- ufix1
  SIGNAL decode_sel_FF_factor1            : std_logic;  -- ufix1
  SIGNAL reg_enb_FF_factor1               : std_logic;  -- ufix1
  SIGNAL data_in_FF_factor1               : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL write_reg_FF_factor1             : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL decode_sel_FF_factor2            : std_logic;  -- ufix1
  SIGNAL reg_enb_FF_factor2               : std_logic;  -- ufix1
  SIGNAL data_in_FF_factor2               : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL write_reg_FF_factor2             : signed(24 DOWNTO 0);  -- sfix25_En13

BEGIN
  addr_sel_unsigned <= unsigned(addr_sel);

  
  decode_sel_ip_timestamp <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0002#, 14) ELSE
      '0';

  read_ip_timestamp_unsigned <= unsigned(read_ip_timestamp);

  const_1 <= '1';

  enb <= const_1;

  const_0 <= to_unsigned(0, 32);

  reg_ip_timestamp_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        read_reg_ip_timestamp <= to_unsigned(0, 32);
      ELSIF enb = '1' THEN
        read_reg_ip_timestamp <= read_ip_timestamp_unsigned;
      END IF;
    END IF;
  END PROCESS reg_ip_timestamp_process;


  
  decode_rd_ip_timestamp <= const_0 WHEN decode_sel_ip_timestamp = '0' ELSE
      read_reg_ip_timestamp;

  data_read <= std_logic_vector(decode_rd_ip_timestamp);

  
  decode_sel_axi_enable <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0001#, 14) ELSE
      '0';

  reg_enb_axi_enable <= decode_sel_axi_enable AND wr_enb;

  data_write_unsigned <= unsigned(data_write);

  data_in_axi_enable <= data_write_unsigned(0);

  reg_axi_enable_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_axi_enable <= '1';
      ELSIF enb = '1' AND reg_enb_axi_enable = '1' THEN
        write_reg_axi_enable <= data_in_axi_enable;
      END IF;
    END IF;
  END PROCESS reg_axi_enable_process;


  write_axi_enable <= write_reg_axi_enable;

  
  decode_sel_FF_factor1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0040#, 14) ELSE
      '0';

  reg_enb_FF_factor1 <= decode_sel_FF_factor1 AND wr_enb;

  data_in_FF_factor1 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_FF_factor1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_FF_factor1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_FF_factor1 = '1' THEN
        write_reg_FF_factor1 <= data_in_FF_factor1;
      END IF;
    END IF;
  END PROCESS reg_FF_factor1_process;


  write_FF_factor1 <= std_logic_vector(write_reg_FF_factor1);

  
  decode_sel_FF_factor2 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0041#, 14) ELSE
      '0';

  reg_enb_FF_factor2 <= decode_sel_FF_factor2 AND wr_enb;

  data_in_FF_factor2 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_FF_factor2_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_FF_factor2 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_FF_factor2 = '1' THEN
        write_reg_FF_factor2 <= data_in_FF_factor2;
      END IF;
    END IF;
  END PROCESS reg_FF_factor2_process;


  write_FF_factor2 <= std_logic_vector(write_reg_FF_factor2);

END rtl;

