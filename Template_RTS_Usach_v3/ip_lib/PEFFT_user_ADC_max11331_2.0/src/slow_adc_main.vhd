----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.11.2018 09:16:14
-- Design Name: 
-- Module Name: slow_adc_main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


-- Uncomment all the "--" in the first column to activate the second adc board

entity Slow_ADC_Memory_main is
	GENERIC
	(
		clk_div : INTEGER := 3;                   --system clock cycles per 1/2 period of sclk
		OUTPUT_WORD_WIDTH : INTEGER := 12;                   --system clock cycles per 1/2 period of sclk
		full_ch_mode : STD_LOGIC := '1'        --Select 8 or 4 channel to be measured per adc chip
	);
	Port
	(
		clk 								: in STD_LOGIC;
		reset_n 							: in STD_LOGIC;
		enable_measure 				        : in STD_LOGIC;
		sclk 								: out STD_LOGIC;
		mosi 								: out STD_LOGIC;
		miso 								: in STD_LOGIC_VECTOR (5 downto 0);
		ss_n 								: out STD_LOGIC;
		init_done 						: out STD_LOGIC;
		meas_done 						: out STD_LOGIC;
		new_data 						: out STD_LOGIC;
		error 							: out STD_LOGIC;
		data_board_1_input_1_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_1_ch2	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_1_ch3	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_1_ch4	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_2_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_2_ch2	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_2_ch3	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_2_ch4	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_3_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_3_ch2	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_3_ch3	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_3_ch4	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_4_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_4_ch2	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_4_ch3	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_4_ch4	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_5_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_5_ch2	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_5_ch3	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_5_ch4	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_6_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_6_ch2	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_6_ch3	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
		data_board_1_input_6_ch4	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);

        data_board_2_input_1_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_1_ch2    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_1_ch3    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_1_ch4    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_2_ch1    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_2_ch2    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_2_ch3    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_2_ch4    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_3_ch1    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_3_ch2    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_3_ch3    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_3_ch4    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_4_ch1    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_4_ch2    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_4_ch3    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_4_ch4    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_5_ch1    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_5_ch2    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_5_ch3    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_5_ch4    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_6_ch1    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_6_ch2    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_6_ch3    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
        data_board_2_input_6_ch4    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0)
	);
end Slow_ADC_Memory_main;


architecture Behavioral of Slow_ADC_Memory_main is

	signal  enable_spi_S :   std_logic := '0';
	signal  busy_spi_S :   std_logic := '0';
	signal  error_S :   		std_logic := '0';
	signal  meas_done_S :   	std_logic := '0';
	signal  init_done_S :	std_logic := '0';
	signal	new_data_S	:	std_logic := '0';
	signal  tx_data_S  :   std_logic_vector(15 downto 0) := (others=>'0'); 
	signal  rx_data_1_S  :   std_logic_vector(15 downto 0) := (others=>'0');
	signal  rx_data_2_S  :   std_logic_vector(15 downto 0) := (others=>'0');
	signal  rx_data_3_S  :   std_logic_vector(15 downto 0) := (others=>'0');
	signal  rx_data_4_S  :   std_logic_vector(15 downto 0) := (others=>'0');
	signal  rx_data_5_S  :   std_logic_vector(15 downto 0) := (others=>'0');
	signal  rx_data_6_S  :   std_logic_vector(15 downto 0) := (others=>'0');
	
	signal  data_b1_i1_ch1_S	:	STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i1_ch2_S	:	STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i1_ch3_S	:	STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i1_ch4_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i2_ch1_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i2_ch2_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i2_ch3_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i2_ch4_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i3_ch1_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i3_ch2_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i3_ch3_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i3_ch4_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i4_ch1_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i4_ch2_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i4_ch3_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i4_ch4_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i5_ch1_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i5_ch2_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i5_ch3_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i5_ch4_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i6_ch1_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i6_ch2_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i6_ch3_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b1_i6_ch4_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 

	signal  data_b2_i1_ch1_S	:	STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i1_ch2_S	:	STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i1_ch3_S	:	STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i1_ch4_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i2_ch1_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i2_ch2_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i2_ch3_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i2_ch4_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i3_ch1_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i3_ch2_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i3_ch3_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i3_ch4_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i4_ch1_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i4_ch2_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i4_ch3_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i4_ch4_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i5_ch1_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i5_ch2_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i5_ch3_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i5_ch4_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i6_ch1_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i6_ch2_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i6_ch3_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	signal  data_b2_i6_ch4_S	:  STD_LOGIC_VECTOR (15 downto 0) := (others=>'0'); 
	
	
	type state_t is (INIT_START, INIT_ADC_REG, INIT_BIPOLAR_REG, INIT_UNIPOLAR_REG, INIT_RANGE_REG,
							IDLE,	MEAS_ASK_0, MEAS_ASK_2, MEAS_ASK_4, MEAS_ASK_6, MEAS_ASK_8, 
							MEAS_ASK_10, MEAS_ASK_12, MEAS_ASK_14, MEAS_ASK_EXTRA,
							ERROR_STATE);
	signal state                    : state_t := IDLE;  

	type memory_t is array (0 to 7) of STD_LOGIC_VECTOR(15 downto 0);
	SIGNAL board_1_adc_1_array_S : memory_t := (others => (others => '0'));
	SIGNAL board_1_adc_2_array_S : memory_t := (others => (others => '0'));	
	SIGNAL board_1_adc_3_array_S : memory_t := (others => (others => '0'));	
	SIGNAL board_2_adc_1_array_S : memory_t := (others => (others => '0'));
	SIGNAL board_2_adc_2_array_S : memory_t := (others => (others => '0'));	
	SIGNAL board_2_adc_3_array_S : memory_t := (others => (others => '0'));	
 
	CONSTANT MSG_INIT_1	       : STD_LOGIC_VECTOR(15 downto 0) := "1000000000000100";
	CONSTANT MSG_INIT_2	       : STD_LOGIC_VECTOR(15 downto 0) := "1001011111111000";
	CONSTANT MSG_INIT_3	       : STD_LOGIC_VECTOR(15 downto 0) := "1000100000000000";
	CONSTANT MSG_INIT_4	       : STD_LOGIC_VECTOR(15 downto 0) := "1001111111111000";
    CONSTANT MSG_MEAS_ASK_0 	: STD_LOGIC_VECTOR(15 downto 0) := "0000100000000100";
	CONSTANT MSG_MEAS_ASK_2		: STD_LOGIC_VECTOR(15 downto 0) := "0000100100000100";
	CONSTANT MSG_MEAS_ASK_4		: STD_LOGIC_VECTOR(15 downto 0) := "0000101000000100";
	CONSTANT MSG_MEAS_ASK_6		: STD_LOGIC_VECTOR(15 downto 0) := "0000101100000100";
	CONSTANT MSG_MEAS_ASK_8		: STD_LOGIC_VECTOR(15 downto 0) := "0000110000000100";
	CONSTANT MSG_MEAS_ASK_10	: STD_LOGIC_VECTOR(15 downto 0) := "0000110100000100";
	CONSTANT MSG_MEAS_ASK_12	: STD_LOGIC_VECTOR(15 downto 0) := "0000111000000100";
	CONSTANT MSG_MEAS_ASK_14	: STD_LOGIC_VECTOR(15 downto 0) := "0000111100000100";
	
	 
	COMPONENT spi_master_multi_slave IS
		GENERIC
		(
			clk_div : INTEGER := 3          --system clock cycles per 1/2 period of sclk
		);
		PORT
		(
			clock   : IN     STD_LOGIC;                             --system clock
			reset_n : IN     STD_LOGIC;                             --asynchronous reset
			enable  : IN     STD_LOGIC;                             --initiate transaction
			tx_data : IN     STD_LOGIC_VECTOR(15 DOWNTO 0);  --data to transmit
			miso1   : IN     STD_LOGIC;                             --master in, slave out
			miso2   : IN     STD_LOGIC;                             --master in, slave out
			miso3   : IN     STD_LOGIC;                             --master in, slave out
			miso4   : IN     STD_LOGIC;                             --master in, slave out
			miso5   : IN     STD_LOGIC;                             --master in, slave out
			miso6   : IN     STD_LOGIC;                             --master in, slave out
			sclk    : OUT STD_LOGIC;                             --spi clock
			ss_n    : OUT STD_LOGIC;   --slave select
			mosi    : OUT    STD_LOGIC;                             --master out, slave in
			busy    : OUT    STD_LOGIC;                             --busy / data ready signal
			rx_data1 : OUT    STD_LOGIC_VECTOR(15 DOWNTO 0);   --data received
			rx_data2 : OUT    STD_LOGIC_VECTOR(15 DOWNTO 0);   --data received
			rx_data3 : OUT    STD_LOGIC_VECTOR(15 DOWNTO 0);   --data received
			rx_data4 : OUT    STD_LOGIC_VECTOR(15 DOWNTO 0);   --data received
			rx_data5 : OUT    STD_LOGIC_VECTOR(15 DOWNTO 0);   --data received
			rx_data6 : OUT    STD_LOGIC_VECTOR(15 DOWNTO 0)   --data received
		);
	END COMPONENT spi_master_multi_slave;

begin

	spi_master_multi_slave_0 : spi_master_multi_slave
	generic map
	(
		clk_div    => clk_div         --system clock cycles per 1/2 period of sclk
	)
	PORT MAP
   (
		clock   => clk,                --system clock
		reset_n => reset_n,              --asynchronous reset
		enable  => enable_spi_S,             --initiate transaction
		tx_data => tx_data_S(15 downto 0),                  --data to transmit
		miso1    => miso(0),               --master in, slave out
		miso2    => miso(1),               --master in, slave out
		miso3    => miso(2),               --master in, slave out
		miso4    => miso(3),               --master in, slave out
		miso5    => miso(4),               --master in, slave out
		miso6    => miso(5),               --master in, slave out
		sclk    => sclk,                --spi clock
		ss_n    => ss_n,                   --slave select
		mosi    => mosi,               --master out, slave in
		busy    => busy_spi_S,              --busy / data ready signal
		rx_data1 => rx_data_1_S,		--data received
		rx_data2 => rx_data_2_S,		--data received
		rx_data3 => rx_data_3_S,		--data received
		rx_data4 => rx_data_4_S,		--data received
		rx_data5 => rx_data_5_S,		--data received
		rx_data6 => rx_data_6_S			--data received
    );
    
    -- Slow ADC State Machine - Mealy / Moore
	State_Machine : PROCESS (clk, reset_n) IS
	BEGIN
		IF(reset_n = '0') THEN           --reset asserted
			state <= INIT_START;                      --return to initial state
			tx_data_S <= MSG_INIT_1; 
			enable_spi_S <= '0';
			meas_done_S <= '0';
			init_done_S <= '0';
			error_S <= '0';
            
		ELSIF rising_edge(clk) THEN
			CASE state IS
			--Start the initialization sending the first word via SPI.
			WHEN INIT_START =>
				init_done_S <= '0';
				meas_done_S <= '0';
				tx_data_S <= MSG_INIT_1;
				state <= INIT_ADC_REG;
				enable_spi_S <= '1';
				IF busy_spi_S = '1' THEN
                    state <= INIT_ADC_REG;
                ELSE
                    state <= INIT_START;
                END IF;
 
			--Initializing ADC Register 
			WHEN INIT_ADC_REG =>
				init_done_S <= '0';
				meas_done_S <= '0';
				tx_data_S <= MSG_INIT_2;
				IF busy_spi_S = '0' THEN
					state <= INIT_BIPOLAR_REG;
					enable_spi_S <= '1';
				ELSE
					state <= INIT_ADC_REG;
					enable_spi_S <= '1';			-- enable_spi must be LOW before the spi message ends
				END IF;
				
			--Initializing BIPOLAR Register 
			WHEN INIT_BIPOLAR_REG =>
				init_done_S <= '0';
				meas_done_S <= '0';
				tx_data_S <= MSG_INIT_2;
				IF busy_spi_S = '0' THEN
					--IF MSG_INIT_1 = rx_data_1_S THEN	-- Could be better if other channels are added as well
					IF enable_spi_S = '1' THEN
						state <= INIT_UNIPOLAR_REG;
						enable_spi_S <= '1';
					ELSE
						state <= ERROR_STATE;
						enable_spi_S <= '0';			-- enable_spi must be LOW before the spi message ends
					END IF;	
				ELSE
					state <= INIT_BIPOLAR_REG;
					enable_spi_S <= '1';			-- enable_spi must be LOW before the spi message ends
				END IF;			
				
			--Initializing UNIPOLAR Register 
			WHEN INIT_UNIPOLAR_REG =>
				init_done_S <= '0';
				meas_done_S <= '0';
				tx_data_S <= MSG_INIT_3;
				IF busy_spi_S = '0' THEN
					--IF MSG_INIT_2 = rx_data_1_S THEN	-- Could be better if other channels are added as well
					IF enable_spi_S = '1' THEN
						state <= INIT_RANGE_REG;
						enable_spi_S <= '1';
					ELSE
						state <= ERROR_STATE;
						enable_spi_S <= '0';			-- enable_spi must be LOW before the spi message ends
					END IF;	
				ELSE
					state <= INIT_UNIPOLAR_REG;
					enable_spi_S <= '1';			-- enable_spi must be LOW before the spi message ends
				END IF;
				
			--Initializing RANGE Register 
			WHEN INIT_RANGE_REG =>
				init_done_S <= '0';
				meas_done_S <= '0';
				tx_data_S <= MSG_INIT_4;			-- prepare the next message after init ends
				IF busy_spi_S = '0' THEN
					--IF MSG_INIT_3 = rx_data_1_S THEN	-- Could be better if other channels are added as well
					IF enable_spi_S = '1' THEN
						state <= IDLE;
						enable_spi_S <= '1';
					ELSE
						state <= ERROR_STATE;
						enable_spi_S <= '0';			-- enable_spi must be LOW before the spi message ends
					END IF;					
				ELSE
					state <= INIT_RANGE_REG;
					enable_spi_S <= '1';			-- enable_spi must be LOW before the spi message ends
				END IF;
				
	
			WHEN IDLE =>
				init_done_S <= '1';
				meas_done_S <= '1';
				tx_data_S <= MSG_MEAS_ASK_0;
				IF enable_measure = '1' THEN
					state <= MEAS_ASK_0;
					enable_spi_S <= '1';			-- Send the ask for channel 0 to the adc's
				ELSE
					state <= IDLE;
					enable_spi_S <= '0';
				END IF;	
				
			WHEN MEAS_ASK_0 =>					-- Asked ch0 to be measured. receive nothing usefull
				init_done_S <= '1';
				meas_done_S <= '0';
				tx_data_S <= MSG_MEAS_ASK_2;
				IF busy_spi_S = '0' THEN
					-- storage rx_data into the internal memory !!!!!!!!!!!. Do nothing here
					state <= MEAS_ASK_2;
					enable_spi_S <= '1';			-- Send the ask for channel 2 to the adc's
				ELSE
					state <= MEAS_ASK_0;
					enable_spi_S <= '1';
				END IF;	

			WHEN MEAS_ASK_2 =>					-- Asked ch2 to be measured. receive ch0 data
				init_done_S <= '1';
				meas_done_S <= '0';
				tx_data_S <= MSG_MEAS_ASK_4;
				IF busy_spi_S = '0' THEN
					board_1_adc_1_array_S(0) <= rx_data_1_S; -- storage rx_data into the internal memory
					board_1_adc_2_array_S(0) <= rx_data_2_S; -- storage rx_data into the internal memory
					board_1_adc_3_array_S(0) <= rx_data_3_S; -- storage rx_data into the internal memory
					board_2_adc_1_array_S(0) <= rx_data_4_S; -- storage rx_data into the internal memory
					board_2_adc_2_array_S(0) <= rx_data_5_S; -- storage rx_data into the internal memory
					board_2_adc_3_array_S(0) <= rx_data_6_S; -- storage rx_data into the internal memory
					state <= MEAS_ASK_4;
					enable_spi_S <= '1';			-- Send the ask for channel 4 to the adc's
				ELSE
					state <= MEAS_ASK_2;
					enable_spi_S <= '1';
				END IF;	
				
			WHEN MEAS_ASK_4 =>					-- Asked ch4 to be measured. receive ch2 data
				init_done_S <= '1';
				meas_done_S <= '0';
				tx_data_S <= MSG_MEAS_ASK_6;
				IF busy_spi_S = '0' THEN
					board_1_adc_1_array_S(1) <= rx_data_1_S; -- storage rx_data into the internal memory
					board_1_adc_2_array_S(1) <= rx_data_2_S; -- storage rx_data into the internal memory
					board_1_adc_3_array_S(1) <= rx_data_3_S; -- storage rx_data into the internal memory
					board_2_adc_1_array_S(1) <= rx_data_4_S; -- storage rx_data into the internal memory
					board_2_adc_2_array_S(1) <= rx_data_5_S; -- storage rx_data into the internal memory
					board_2_adc_3_array_S(1) <= rx_data_6_S; -- storage rx_data into the internal memory
					state <= MEAS_ASK_6;
					enable_spi_S <= '1';			-- Send the ask for channel 6 to the adc's
				ELSE
					state <= MEAS_ASK_4;
					enable_spi_S <= '1';
				END IF;	
				
			WHEN MEAS_ASK_6 =>					-- Asked ch6 to be measured. receive ch4 data
				init_done_S <= '1';
				meas_done_S <= '0';
				IF full_ch_mode = '0' THEN
                    tx_data_S <= MSG_MEAS_ASK_0;
                ELSE
                    tx_data_S <= MSG_MEAS_ASK_8;
                END IF;				
				IF busy_spi_S = '0' THEN
					board_1_adc_1_array_S(2) <= rx_data_1_S; -- storage rx_data into the internal memory
					board_1_adc_2_array_S(2) <= rx_data_2_S; -- storage rx_data into the internal memory
					board_1_adc_3_array_S(2) <= rx_data_3_S; -- storage rx_data into the internal memory
					board_2_adc_1_array_S(2) <= rx_data_4_S; -- storage rx_data into the internal memory
					board_2_adc_2_array_S(2) <= rx_data_5_S; -- storage rx_data into the internal memory
					board_2_adc_3_array_S(2) <= rx_data_6_S; -- storage rx_data into the internal memory
					state <= MEAS_ASK_8;
					enable_spi_S <= '1';			-- Send the ask for channel 8 to the adc's
				ELSE
					state <= MEAS_ASK_6;
					enable_spi_S <= '1';
				END IF;	

			WHEN MEAS_ASK_8 =>					-- Asked ch8 to be measured. receive ch6 data
				init_done_S <= '1';             -- Ask ch2 to be measured if channel sampled = "10" -> 4 channels
				meas_done_S <= '0';
				IF full_ch_mode = '0' THEN
				    tx_data_S <= MSG_MEAS_ASK_2;
                ELSE
                    tx_data_S <= MSG_MEAS_ASK_10;
                END IF;
				IF busy_spi_S = '0' THEN
					board_1_adc_1_array_S(3) <= rx_data_1_S; -- storage rx_data into the internal memory
					board_1_adc_2_array_S(3) <= rx_data_2_S; -- storage rx_data into the internal memory
					board_1_adc_3_array_S(3) <= rx_data_3_S; -- storage rx_data into the internal memory
					board_2_adc_1_array_S(3) <= rx_data_4_S; -- storage rx_data into the internal memory
					board_2_adc_2_array_S(3) <= rx_data_5_S; -- storage rx_data into the internal memory
					board_2_adc_3_array_S(3) <= rx_data_6_S; -- storage rx_data into the internal memory
    				IF full_ch_mode = '0' THEN
					   IF enable_measure = '1' THEN
                            state <= MEAS_ASK_2;            -- Re-start again reading the ch0 data
                            enable_spi_S <= '1';            -- Send the ask for channel 2 to the adc's
                        ELSE
                            state <= IDLE;            -- Return to IDLE state until enable_measure goes to HIGH.
                            enable_spi_S <= '0';            -- stop SPI transactions
                        END IF;    				
                    ELSE
                        state <= MEAS_ASK_10;
                        enable_spi_S <= '1';			-- Send the ask for channel 10 to the adc's
                    END IF;
				ELSE
					state <= MEAS_ASK_8;
					enable_spi_S <= '1';
				END IF;	
				
			WHEN MEAS_ASK_10 =>					-- Asked ch10 to be measured. receive ch8 data
				init_done_S <= '1';
				meas_done_S <= '0';
				tx_data_S <= MSG_MEAS_ASK_12;
				IF busy_spi_S = '0' THEN
					board_1_adc_1_array_S(4) <= rx_data_1_S; -- storage rx_data into the internal memory
					board_1_adc_2_array_S(4) <= rx_data_2_S; -- storage rx_data into the internal memory
					board_1_adc_3_array_S(4) <= rx_data_3_S; -- storage rx_data into the internal memory
					board_2_adc_1_array_S(4) <= rx_data_4_S; -- storage rx_data into the internal memory
					board_2_adc_2_array_S(4) <= rx_data_5_S; -- storage rx_data into the internal memory
					board_2_adc_3_array_S(4) <= rx_data_6_S; -- storage rx_data into the internal memory
					state <= MEAS_ASK_12;
					enable_spi_S <= '1';			-- Send the ask for channel 12 to the adc's
				ELSE
					state <= MEAS_ASK_10;
					enable_spi_S <= '1';
				END IF;	
				
			WHEN MEAS_ASK_12 =>					-- Asked ch12 to be measured. receive ch10 data
				init_done_S <= '1';
				meas_done_S <= '0';
				tx_data_S <= MSG_MEAS_ASK_14;
				IF busy_spi_S = '0' THEN
					board_1_adc_1_array_S(5) <= rx_data_1_S; -- storage rx_data into the internal memory
					board_1_adc_2_array_S(5) <= rx_data_2_S; -- storage rx_data into the internal memory
					board_1_adc_3_array_S(5) <= rx_data_3_S; -- storage rx_data into the internal memory
					board_2_adc_1_array_S(5) <= rx_data_4_S; -- storage rx_data into the internal memory
					board_2_adc_2_array_S(5) <= rx_data_5_S; -- storage rx_data into the internal memory
					board_2_adc_3_array_S(5) <= rx_data_6_S; -- storage rx_data into the internal memory
					state <= MEAS_ASK_14;
					enable_spi_S <= '1';			-- Send the ask for channel 14 to the adc's
				ELSE
					state <= MEAS_ASK_12;
					enable_spi_S <= '1';
				END IF;	

			WHEN MEAS_ASK_14 =>					-- Asked ch14 to be measured. receive ch12 data
				init_done_S <= '1';
				meas_done_S <= '0';
				tx_data_S <= MSG_MEAS_ASK_0;	-- Ch0 is selected in case of continuous sampling
				IF busy_spi_S = '0' THEN
					board_1_adc_1_array_S(6) <= rx_data_1_S; -- storage rx_data into the internal memory
					board_1_adc_2_array_S(6) <= rx_data_2_S; -- storage rx_data into the internal memory
					board_1_adc_3_array_S(6) <= rx_data_3_S; -- storage rx_data into the internal memory
					board_2_adc_1_array_S(6) <= rx_data_4_S; -- storage rx_data into the internal memory
					board_2_adc_2_array_S(6) <= rx_data_5_S; -- storage rx_data into the internal memory
					board_2_adc_3_array_S(6) <= rx_data_6_S; -- storage rx_data into the internal memory
					state <= MEAS_ASK_EXTRA;
					enable_spi_S <= '1';			-- Send the ask for channel 0 to the adc's
				ELSE
					state <= MEAS_ASK_14;
					enable_spi_S <= '1';
				END IF;	

			WHEN MEAS_ASK_EXTRA =>				-- Asked ch0 to be measured. receive ch14 data
				init_done_S <= '1';
				IF enable_measure = '1' THEN
				    tx_data_S <= MSG_MEAS_ASK_2;	-- Ch2 is selected in case of continuous sampling
                ELSE
				    tx_data_S <= MSG_MEAS_ASK_2;	-- Ch0 is selected in case of non-continuous sampling -- Fix this!
                END IF;
				IF busy_spi_S = '0' THEN
					board_1_adc_1_array_S(7) <= rx_data_1_S; -- storage rx_data into the internal memory
					board_1_adc_2_array_S(7) <= rx_data_2_S; -- storage rx_data into the internal memory
					board_1_adc_3_array_S(7) <= rx_data_3_S; -- storage rx_data into the internal memory
					board_2_adc_1_array_S(7) <= rx_data_4_S; -- storage rx_data into the internal memory
					board_2_adc_2_array_S(7) <= rx_data_5_S; -- storage rx_data into the internal memory
					board_2_adc_3_array_S(7) <= rx_data_6_S; -- storage rx_data into the internal memory
					meas_done_S <= '1';
					IF enable_measure = '1' THEN
						state <= MEAS_ASK_2;			-- Re-start again reading the ch0 data
						enable_spi_S <= '1';			-- Send the ask for channel 2 to the adc's
					ELSE
						state <= IDLE;			-- Return to IDLE state until enable_measure goes to HIGH.
						enable_spi_S <= '0';			-- stop SPI transactions
					END IF;
				ELSE
					state <= MEAS_ASK_EXTRA;
					enable_spi_S <= '1';
					meas_done_S <= '0';
				END IF;	
				
			-- Error state rise the error flag to 1 and stop the state machine
			WHEN ERROR_STATE =>
				error_S <= '1';
				state <= ERROR_STATE;
				tx_data_S <= (others => '0');
				enable_spi_S <= '0';
				init_done_S <= '0';
				meas_done_S <= '0';
				
			END CASE;
		END IF;
	END PROCESS;
	
	
    -- Assign output measurement arrays process
	Output_assignment : PROCESS (clk, reset_n) IS
	BEGIN
		IF(reset_n = '0') THEN           --reset asserted
			data_b1_i1_ch1_S <= (OTHERS => '0');
			data_b1_i1_ch2_S <= (OTHERS => '0');
			data_b1_i1_ch3_S <= (OTHERS => '0');
			data_b1_i1_ch4_S <= (OTHERS => '0');
			data_b1_i2_ch1_S <= (OTHERS => '0');
			data_b1_i2_ch2_S <= (OTHERS => '0');
			data_b1_i2_ch3_S <= (OTHERS => '0');
			data_b1_i2_ch4_S <= (OTHERS => '0');
			data_b1_i3_ch1_S <= (OTHERS => '0');
			data_b1_i3_ch2_S <= (OTHERS => '0');
			data_b1_i3_ch3_S <= (OTHERS => '0');
			data_b1_i3_ch4_S <= (OTHERS => '0');
			data_b1_i4_ch1_S <= (OTHERS => '0');
			data_b1_i4_ch2_S <= (OTHERS => '0');
			data_b1_i4_ch3_S <= (OTHERS => '0');
			data_b1_i4_ch4_S <= (OTHERS => '0');
			data_b1_i5_ch1_S <= (OTHERS => '0');
			data_b1_i5_ch2_S <= (OTHERS => '0');
			data_b1_i5_ch3_S <= (OTHERS => '0');
			data_b1_i5_ch4_S <= (OTHERS => '0');
			data_b1_i6_ch1_S <= (OTHERS => '0');
			data_b1_i6_ch2_S <= (OTHERS => '0');
			data_b1_i6_ch3_S <= (OTHERS => '0');
			data_b1_i6_ch4_S <= (OTHERS => '0');
			data_b2_i1_ch1_S <= (OTHERS => '0');
			data_b2_i1_ch2_S <= (OTHERS => '0');
			data_b2_i1_ch3_S <= (OTHERS => '0');
			data_b2_i1_ch4_S <= (OTHERS => '0');
			data_b2_i2_ch1_S <= (OTHERS => '0');
			data_b2_i2_ch2_S <= (OTHERS => '0');
			data_b2_i2_ch3_S <= (OTHERS => '0');
			data_b2_i2_ch4_S <= (OTHERS => '0');
			data_b2_i3_ch1_S <= (OTHERS => '0');
			data_b2_i3_ch2_S <= (OTHERS => '0');
			data_b2_i3_ch3_S <= (OTHERS => '0');
			data_b2_i3_ch4_S <= (OTHERS => '0');
			data_b2_i4_ch1_S <= (OTHERS => '0');
			data_b2_i4_ch2_S <= (OTHERS => '0');
			data_b2_i4_ch3_S <= (OTHERS => '0');
			data_b2_i4_ch4_S <= (OTHERS => '0');
			data_b2_i5_ch1_S <= (OTHERS => '0');
			data_b2_i5_ch2_S <= (OTHERS => '0');
			data_b2_i5_ch3_S <= (OTHERS => '0');
			data_b2_i5_ch4_S <= (OTHERS => '0');
			data_b2_i6_ch1_S <= (OTHERS => '0');
			data_b2_i6_ch2_S <= (OTHERS => '0');
			data_b2_i6_ch3_S <= (OTHERS => '0');
			data_b2_i6_ch4_S <= (OTHERS => '0');
			new_data_S <= '0';
			
			ELSIF rising_edge(clk) THEN
				IF meas_done_S = '1' THEN
					data_b1_i1_ch1_S <= board_1_adc_1_array_S(6);	-- ADC CH 12, input 1, board 1-- Fix this! in every input
					data_b1_i1_ch2_S <= board_1_adc_1_array_S(4);	-- ADC CH 8, input 1, board 1
					data_b1_i1_ch3_S <= board_1_adc_1_array_S(2);	-- ADC CH 4, input 1, board 1
					data_b1_i1_ch4_S <= board_1_adc_1_array_S(1);	-- ADC CH 0, input 1, board 1-- Fix this! in every input
					data_b1_i2_ch1_S <= board_1_adc_1_array_S(7);	-- ADC CH 14, input 2, board 1
					data_b1_i2_ch2_S <= board_1_adc_1_array_S(5);	-- ADC CH 10, input 2, board 1
					data_b1_i2_ch3_S <= board_1_adc_1_array_S(3);	-- ADC CH 6, input 2, board 1
					data_b1_i2_ch4_S <= board_1_adc_1_array_S(0);	-- ADC CH 2, input 2, board 1
					data_b1_i3_ch1_S <= board_1_adc_2_array_S(6);	-- ADC CH 12, input 3, board 1
					data_b1_i3_ch2_S <= board_1_adc_2_array_S(4);	-- ADC CH 8, input 3, board 1
					data_b1_i3_ch3_S <= board_1_adc_2_array_S(2);	-- ADC CH 4, input 3, board 1
					data_b1_i3_ch4_S <= board_1_adc_2_array_S(1);	-- ADC CH 0, input 3, board 1
					data_b1_i4_ch1_S <= board_1_adc_2_array_S(7);	-- ADC CH 14, input 4, board 1
					data_b1_i4_ch2_S <= board_1_adc_2_array_S(5);	-- ADC CH 10, input 4, board 1
					data_b1_i4_ch3_S <= board_1_adc_2_array_S(3);	-- ADC CH 6, input 4, board 1
					data_b1_i4_ch4_S <= board_1_adc_2_array_S(0);	-- ADC CH 2	, input 4, board 1
					data_b1_i5_ch1_S <= board_1_adc_3_array_S(6);	-- ADC CH 12, input 5, board 1
					data_b1_i5_ch2_S <= board_1_adc_3_array_S(4);	-- ADC CH 8, input 5, board 1
					data_b1_i5_ch3_S <= board_1_adc_3_array_S(2);	-- ADC CH 4, input 5, board 1
					data_b1_i5_ch4_S <= board_1_adc_3_array_S(1);	-- ADC CH 0, input 5, board 1
					data_b1_i6_ch1_S <= board_1_adc_3_array_S(7);	-- ADC CH 14, input 6, board 1
					data_b1_i6_ch2_S <= board_1_adc_3_array_S(5);	-- ADC CH 10, input 6, board 1
					data_b1_i6_ch3_S <= board_1_adc_3_array_S(3);	-- ADC CH 6, input 6, board 1
					data_b1_i6_ch4_S <= board_1_adc_3_array_S(0);	-- ADC CH 2	, input 6, board 1
					data_b2_i1_ch1_S <= board_2_adc_1_array_S(6);	-- ADC CH 12, input 1, board 2
                    data_b2_i1_ch2_S <= board_2_adc_1_array_S(4);    -- ADC CH 8, input 1, board 2
                    data_b2_i1_ch3_S <= board_2_adc_1_array_S(2);    -- ADC CH 4, input 1, board 2
                    data_b2_i1_ch4_S <= board_2_adc_1_array_S(1);    -- ADC CH 0, input 1, board 2
                    data_b2_i2_ch1_S <= board_2_adc_1_array_S(7);    -- ADC CH 14, input 2, board 2
                    data_b2_i2_ch2_S <= board_2_adc_1_array_S(5);    -- ADC CH 10, input 2, board 2
                    data_b2_i2_ch3_S <= board_2_adc_1_array_S(3);    -- ADC CH 6, input 2, board 2
                    data_b2_i2_ch4_S <= board_2_adc_1_array_S(0);    -- ADC CH 2, input 2, board 1
                    data_b2_i3_ch1_S <= board_2_adc_2_array_S(6);    -- ADC CH 12, input 3, board 1
                    data_b2_i3_ch2_S <= board_2_adc_2_array_S(4);    -- ADC CH 8, input 3, board 1
                    data_b2_i3_ch3_S <= board_2_adc_2_array_S(2);    -- ADC CH 4, input 3, board 1
                    data_b2_i3_ch4_S <= board_2_adc_2_array_S(1);    -- ADC CH 0, input 3, board 1
                    data_b2_i4_ch1_S <= board_2_adc_2_array_S(7);    -- ADC CH 14, input 4, board 1
                    data_b2_i4_ch2_S <= board_2_adc_2_array_S(5);    -- ADC CH 10, input 4, board 1
                    data_b2_i4_ch3_S <= board_2_adc_2_array_S(3);    -- ADC CH 6, input 4, board 1
                    data_b2_i4_ch4_S <= board_2_adc_2_array_S(0);    -- ADC CH 2    , input 4, board 1
                    data_b2_i5_ch1_S <= board_2_adc_3_array_S(6);    -- ADC CH 12, input 5, board 1
                    data_b2_i5_ch2_S <= board_2_adc_3_array_S(4);    -- ADC CH 8, input 5, board 1
                    data_b2_i5_ch3_S <= board_2_adc_3_array_S(2);    -- ADC CH 4, input 5, board 1
                    data_b2_i5_ch4_S <= board_2_adc_3_array_S(1);    -- ADC CH 0, input 5, board 1
                    data_b2_i6_ch1_S <= board_2_adc_3_array_S(7);    -- ADC CH 14, input 6, board 1
                    data_b2_i6_ch2_S <= board_2_adc_3_array_S(5);    -- ADC CH 10, input 6, board 1
                    data_b2_i6_ch3_S <= board_2_adc_3_array_S(3);    -- ADC CH 6, input 6, board 1
                    data_b2_i6_ch4_S <= board_2_adc_3_array_S(0);    -- ADC CH 2    , input 6, board 1					
					new_data_S <= '1';
				ELSE
					data_b1_i1_ch1_S <= data_b1_i1_ch1_S;
					data_b1_i1_ch2_S <= data_b1_i1_ch2_S;
					data_b1_i1_ch3_S <= data_b1_i1_ch3_S;
					data_b1_i1_ch4_S <= data_b1_i1_ch4_S;
					data_b1_i2_ch1_S <= data_b1_i2_ch1_S;
					data_b1_i2_ch2_S <= data_b1_i2_ch2_S;
					data_b1_i2_ch3_S <= data_b1_i2_ch3_S;
					data_b1_i2_ch4_S <= data_b1_i2_ch4_S;
					data_b1_i3_ch1_S <= data_b1_i3_ch1_S;
					data_b1_i3_ch2_S <= data_b1_i3_ch2_S;
					data_b1_i3_ch3_S <= data_b1_i3_ch3_S;
					data_b1_i3_ch4_S <= data_b1_i3_ch4_S;
					data_b1_i4_ch1_S <= data_b1_i4_ch1_S;
					data_b1_i4_ch2_S <= data_b1_i4_ch2_S;
					data_b1_i4_ch3_S <= data_b1_i4_ch3_S;
					data_b1_i4_ch4_S <= data_b1_i4_ch4_S;
					data_b1_i5_ch1_S <= data_b1_i5_ch1_S;
					data_b1_i5_ch2_S <= data_b1_i5_ch2_S;
					data_b1_i5_ch3_S <= data_b1_i5_ch3_S;
					data_b1_i5_ch4_S <= data_b1_i5_ch4_S;
					data_b1_i6_ch1_S <= data_b1_i6_ch1_S;
					data_b1_i6_ch2_S <= data_b1_i6_ch2_S;
					data_b1_i6_ch3_S <= data_b1_i6_ch3_S;
					data_b1_i6_ch4_S <= data_b1_i6_ch4_S;
					data_b2_i1_ch1_S <= data_b2_i1_ch1_S;
                    data_b2_i1_ch2_S <= data_b2_i1_ch2_S;
                    data_b2_i1_ch3_S <= data_b2_i1_ch3_S;
                    data_b2_i1_ch4_S <= data_b2_i1_ch4_S;
                    data_b2_i2_ch1_S <= data_b2_i2_ch1_S;
                    data_b2_i2_ch2_S <= data_b2_i2_ch2_S;
                    data_b2_i2_ch3_S <= data_b2_i2_ch3_S;
                    data_b2_i2_ch4_S <= data_b2_i2_ch4_S;
                    data_b2_i3_ch1_S <= data_b2_i3_ch1_S;
                    data_b2_i3_ch2_S <= data_b2_i3_ch2_S;
                    data_b2_i3_ch3_S <= data_b2_i3_ch3_S;
                    data_b2_i3_ch4_S <= data_b2_i3_ch4_S;
                    data_b2_i4_ch1_S <= data_b2_i4_ch1_S;
                    data_b2_i4_ch2_S <= data_b2_i4_ch2_S;
                    data_b2_i4_ch3_S <= data_b2_i4_ch3_S;
                    data_b2_i4_ch4_S <= data_b2_i4_ch4_S;
                    data_b2_i5_ch1_S <= data_b2_i5_ch1_S;
                    data_b2_i5_ch2_S <= data_b2_i5_ch2_S;
                    data_b2_i5_ch3_S <= data_b2_i5_ch3_S;
                    data_b2_i5_ch4_S <= data_b2_i5_ch4_S;
                    data_b2_i6_ch1_S <= data_b2_i6_ch1_S;
                    data_b2_i6_ch2_S <= data_b2_i6_ch2_S;
                    data_b2_i6_ch3_S <= data_b2_i6_ch3_S;
                    data_b2_i6_ch4_S <= data_b2_i6_ch4_S;
					new_data_S <= '0';
				END IF;
			END IF;
	END PROCESS;
	
	error <= error_S;
	new_data <= new_data_S;
	init_done <= init_done_S;
	meas_done <= meas_done_S;

	data_board_1_input_1_ch1 <= data_b1_i1_ch1_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_1_ch2 <= data_b1_i1_ch2_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_1_ch3 <= data_b1_i1_ch3_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_1_ch4 <= data_b1_i1_ch4_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_2_ch1 <= data_b1_i2_ch1_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_2_ch2 <= data_b1_i2_ch2_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_2_ch3 <= data_b1_i2_ch3_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_2_ch4 <= data_b1_i2_ch4_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_3_ch1 <= data_b1_i3_ch1_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_3_ch2 <= data_b1_i3_ch2_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_3_ch3 <= data_b1_i3_ch3_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_3_ch4 <= data_b1_i3_ch4_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_4_ch1 <= data_b1_i4_ch1_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_4_ch2 <= data_b1_i4_ch2_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_4_ch3 <= data_b1_i4_ch3_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_4_ch4 <= data_b1_i4_ch4_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_5_ch1 <= data_b1_i5_ch1_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_5_ch2 <= data_b1_i5_ch2_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_5_ch3 <= data_b1_i5_ch3_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_5_ch4 <= data_b1_i5_ch4_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_6_ch1 <= data_b1_i6_ch1_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_6_ch2 <= data_b1_i6_ch2_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_6_ch3 <= data_b1_i6_ch3_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_1_input_6_ch4 <= data_b1_i6_ch4_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_1_ch1 <= data_b2_i1_ch1_S(OUTPUT_WORD_WIDTH-1 downto 0);
    data_board_2_input_1_ch2 <= data_b2_i1_ch2_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_1_ch3 <= data_b2_i1_ch3_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_1_ch4 <= data_b2_i1_ch4_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_2_ch1 <= data_b2_i2_ch1_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_2_ch2 <= data_b2_i2_ch2_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_2_ch3 <= data_b2_i2_ch3_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_2_ch4 <= data_b2_i2_ch4_S(OUTPUT_WORD_WIDTH-1 downto 0);
    data_board_2_input_3_ch1 <= data_b2_i3_ch1_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_3_ch2 <= data_b2_i3_ch2_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_3_ch3 <= data_b2_i3_ch3_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_3_ch4 <= data_b2_i3_ch4_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_4_ch1 <= data_b2_i4_ch1_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_4_ch2 <= data_b2_i4_ch2_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_4_ch3 <= data_b2_i4_ch3_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_4_ch4 <= data_b2_i4_ch4_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_5_ch1 <= data_b2_i5_ch1_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_5_ch2 <= data_b2_i5_ch2_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_5_ch3 <= data_b2_i5_ch3_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_5_ch4 <= data_b2_i5_ch4_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_6_ch1 <= data_b2_i6_ch1_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_6_ch2 <= data_b2_i6_ch2_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_6_ch3 <= data_b2_i6_ch3_S(OUTPUT_WORD_WIDTH-1 downto 0);
	data_board_2_input_6_ch4 <= data_b2_i6_ch4_S(OUTPUT_WORD_WIDTH-1 downto 0);
end Behavioral;
