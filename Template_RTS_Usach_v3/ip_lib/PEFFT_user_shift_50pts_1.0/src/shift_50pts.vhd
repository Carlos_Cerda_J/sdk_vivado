----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/28/2019 02:25:52 PM
-- Design Name: 
-- Module Name: shift_50pts - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity shift_50pts is
  Port 
  ( 
	clk			: in std_logic;
	reset_n		: in std_logic;
	Vo			: in std_logic_vector(24 downto 0);
	Vo_beta		: out std_logic_vector(24 downto 0)
  );
end shift_50pts;

architecture Behavioral of shift_50pts is
	signal Vo_memory_S : std_logic_vector(1249 downto 0):= (others => '0');
begin

    shift_process : process (clk, reset_n) is
    begin
        if (reset_n = '0') then
            Vo_beta <= (others => '0');
			Vo_memory_S <= (others => '0');
        elsif (rising_edge(clk)) then
			Vo_beta		<= Vo_memory_S(1249 downto 1225);
            Vo_memory_S <= Vo_memory_S(1224 downto 0) & Vo;
        end if;                                                                            
    end process;

end Behavioral;
