-- -------------------------------------------------------------
-- 
-- File Name: hdl_prj\hdlsrc\M2C_NLC_6cell_Control_Vo\M2C_NLC_6cell_Control_Vo_src_M2C_NLC_6cell_Control_Vo_pkg.vhd
-- Created: 2021-03-11 21:15:51
-- 
-- Generated by MATLAB 9.8 and HDL Coder 3.16
-- 
-- -------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

PACKAGE M2C_NLC_6cell_Control_Vo_src_M2C_NLC_6cell_Control_Vo_pkg IS
  TYPE vector_of_signed5 IS ARRAY (NATURAL RANGE <>) OF signed(4 DOWNTO 0);
  TYPE vector_of_signed3 IS ARRAY (NATURAL RANGE <>) OF signed(2 DOWNTO 0);
  TYPE vector_of_unsigned3 IS ARRAY (NATURAL RANGE <>) OF unsigned(2 DOWNTO 0);
  TYPE vector_of_signed18 IS ARRAY (NATURAL RANGE <>) OF signed(17 DOWNTO 0);
  TYPE vector_of_unsigned10 IS ARRAY (NATURAL RANGE <>) OF unsigned(9 DOWNTO 0);
  TYPE vector_of_signed25 IS ARRAY (NATURAL RANGE <>) OF signed(24 DOWNTO 0);
END M2C_NLC_6cell_Control_Vo_src_M2C_NLC_6cell_Control_Vo_pkg;

