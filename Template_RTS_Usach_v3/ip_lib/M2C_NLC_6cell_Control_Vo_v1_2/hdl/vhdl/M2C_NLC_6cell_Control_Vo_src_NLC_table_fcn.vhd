-- -------------------------------------------------------------
-- 
-- File Name: hdl_prj\hdlsrc\M2C_NLC_6cell_Control_Vo\M2C_NLC_6cell_Control_Vo_src_NLC_table_fcn.vhd
-- Created: 2021-03-11 21:15:51
-- 
-- Generated by MATLAB 9.8 and HDL Coder 3.16
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: M2C_NLC_6cell_Control_Vo_src_NLC_table_fcn
-- Source Path: M2C_NLC_6cell_Control_Vo/M2C_NLC_6cell_Control_Vo/Gates_generator_an/NLC_table_fcn
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE work.M2C_NLC_6cell_Control_Vo_src_M2C_NLC_6cell_Control_Vo_pkg.ALL;

ENTITY M2C_NLC_6cell_Control_Vo_src_NLC_table_fcn IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb_1_50_0                        :   IN    std_logic;
        level                             :   IN    std_logic_vector(3 DOWNTO 0);  -- sfix4
        posC1                             :   IN    std_logic_vector(2 DOWNTO 0);  -- ufix3
        posC2                             :   IN    std_logic_vector(2 DOWNTO 0);  -- ufix3
        posC3                             :   IN    std_logic_vector(2 DOWNTO 0);  -- ufix3
        posC4                             :   IN    std_logic_vector(2 DOWNTO 0);  -- ufix3
        posC5                             :   IN    std_logic_vector(2 DOWNTO 0);  -- ufix3
        posC6                             :   IN    std_logic_vector(2 DOWNTO 0);  -- ufix3
        S1                                :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        S2                                :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        S3                                :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        S4                                :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        S5                                :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        S6                                :   OUT   std_logic_vector(4 DOWNTO 0)  -- sfix5
        );
END M2C_NLC_6cell_Control_Vo_src_NLC_table_fcn;


ARCHITECTURE rtl OF M2C_NLC_6cell_Control_Vo_src_NLC_table_fcn IS

  -- Constants
  CONSTANT nc                             : vector_of_signed5(0 TO 77) := 
    (to_signed(-16#01#, 5), to_signed(-16#01#, 5), to_signed(-16#01#, 5), to_signed(-16#01#, 5),
     to_signed(-16#01#, 5), to_signed(-16#01#, 5), to_signed(16#00#, 5), to_signed(16#01#, 5),
     to_signed(16#01#, 5), to_signed(16#01#, 5), to_signed(16#01#, 5), to_signed(16#01#, 5), to_signed(16#01#, 5),
     to_signed(-16#01#, 5), to_signed(-16#01#, 5), to_signed(-16#01#, 5), to_signed(-16#01#, 5),
     to_signed(-16#01#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5),
     to_signed(16#01#, 5), to_signed(16#01#, 5), to_signed(16#01#, 5), to_signed(16#01#, 5), to_signed(16#01#, 5),
     to_signed(-16#01#, 5), to_signed(-16#01#, 5), to_signed(-16#01#, 5), to_signed(-16#01#, 5),
     to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5),
     to_signed(16#01#, 5), to_signed(16#01#, 5), to_signed(16#01#, 5), to_signed(16#01#, 5),
     to_signed(-16#01#, 5), to_signed(-16#01#, 5), to_signed(-16#01#, 5), to_signed(16#00#, 5),
     to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5),
     to_signed(16#00#, 5), to_signed(16#01#, 5), to_signed(16#01#, 5), to_signed(16#01#, 5),
     to_signed(-16#01#, 5), to_signed(-16#01#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5),
     to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5),
     to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#01#, 5), to_signed(16#01#, 5),
     to_signed(-16#01#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5),
     to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5),
     to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#00#, 5), to_signed(16#01#, 5));  -- sfix5 [78]

  -- Signals
  SIGNAL level_signed                     : signed(3 DOWNTO 0);  -- sfix4
  SIGNAL posC1_unsigned                   : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL posC2_unsigned                   : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL posC3_unsigned                   : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL posC4_unsigned                   : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL posC5_unsigned                   : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL posC6_unsigned                   : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL S1_tmp                           : signed(4 DOWNTO 0);  -- sfix5
  SIGNAL S2_tmp                           : signed(4 DOWNTO 0);  -- sfix5
  SIGNAL S3_tmp                           : signed(4 DOWNTO 0);  -- sfix5
  SIGNAL S4_tmp                           : signed(4 DOWNTO 0);  -- sfix5
  SIGNAL S5_tmp                           : signed(4 DOWNTO 0);  -- sfix5
  SIGNAL S6_tmp                           : signed(4 DOWNTO 0);  -- sfix5
  SIGNAL inicio                           : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL inicio_not_empty                 : std_logic;
  SIGNAL posicionC_not_empty              : std_logic;
  SIGNAL tabla                            : vector_of_signed5(0 TO 77);  -- sfix5 [78]
  SIGNAL estadoSW                         : vector_of_signed3(0 TO 6);  -- sfix3 [7]
  SIGNAL inicio_next                      : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL inicio_not_empty_next            : std_logic;
  SIGNAL posicionC_not_empty_next         : std_logic;
  SIGNAL tabla_next                       : vector_of_signed5(0 TO 77);  -- sfix5 [78]
  SIGNAL estadoSW_next                    : vector_of_signed3(0 TO 6);  -- sfix3 [7]

BEGIN
  level_signed <= signed(level);

  posC1_unsigned <= unsigned(posC1);

  posC2_unsigned <= unsigned(posC2);

  posC3_unsigned <= unsigned(posC3);

  posC4_unsigned <= unsigned(posC4);

  posC5_unsigned <= unsigned(posC5);

  posC6_unsigned <= unsigned(posC6);

  NLC_table_fcn_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        inicio_not_empty <= '0';
        posicionC_not_empty <= '0';
        tabla <= nc;
        estadoSW <= (OTHERS => to_signed(16#0#, 3));
      ELSIF enb_1_50_0 = '1' THEN
        inicio <= inicio_next;
        inicio_not_empty <= inicio_not_empty_next;
        posicionC_not_empty <= posicionC_not_empty_next;
        tabla <= tabla_next;
        estadoSW <= estadoSW_next;
      END IF;
    END IF;
  END PROCESS NLC_table_fcn_process;

  NLC_table_fcn_output : PROCESS (estadoSW, inicio, inicio_not_empty, level_signed, posC1_unsigned,
       posC2_unsigned, posC3_unsigned, posC4_unsigned, posC5_unsigned,
       posC6_unsigned, posicionC_not_empty, tabla)
    VARIABLE posicionC : vector_of_unsigned3(0 TO 5);
    VARIABLE levels : signed(4 DOWNTO 0);
    VARIABLE inicio_temp : unsigned(2 DOWNTO 0);
    VARIABLE tabla_temp : vector_of_signed5(0 TO 77);
    VARIABLE estadoSW_temp : vector_of_signed3(0 TO 6);
    VARIABLE add_temp : signed(6 DOWNTO 0);
    VARIABLE add_temp_0 : unsigned(3 DOWNTO 0);
    VARIABLE sub_cast : signed(31 DOWNTO 0);
    VARIABLE add_temp_1 : unsigned(3 DOWNTO 0);
    VARIABLE sub_cast_0 : signed(31 DOWNTO 0);
    VARIABLE add_temp_2 : unsigned(3 DOWNTO 0);
    VARIABLE sub_cast_1 : signed(31 DOWNTO 0);
    VARIABLE add_temp_3 : unsigned(3 DOWNTO 0);
    VARIABLE sub_cast_2 : signed(31 DOWNTO 0);
    VARIABLE add_temp_4 : unsigned(3 DOWNTO 0);
    VARIABLE sub_cast_3 : signed(31 DOWNTO 0);
    VARIABLE add_temp_5 : unsigned(3 DOWNTO 0);
    VARIABLE sub_cast_4 : signed(31 DOWNTO 0);
  BEGIN
    inicio_temp := inicio;
    tabla_temp := tabla;
    estadoSW_temp := estadoSW;
    inicio_not_empty_next <= inicio_not_empty;
    posicionC_not_empty_next <= posicionC_not_empty;
    --MATLAB Function 'M2C_NLC_6cell_Control_Vo/Gates_generator_an/NLC_table_fcn'
    IF ( NOT inicio_not_empty) = '1' THEN 
      inicio_temp := to_unsigned(16#1#, 3);
      inicio_not_empty_next <= '1';
    END IF;
    IF ( NOT posicionC_not_empty) = '1' THEN 
      posicionC_not_empty_next <= '1';
    END IF;
    IF inicio_temp = to_unsigned(16#1#, 3) THEN 
      tabla_temp := nc;
      -- for i=1:(2*nc+1)
      --     for j=1:nc
      --         if(i<=nc)
      --             if(i<j+1)
      --                 tabla(i,j)=tabla(i,j)-1;
      --             end
      --         end
      --         if(i>nc)
      --            tabla(i,j)=tabla(i-nc,j)+1;
      --         end
      --        
      --     end
      -- end
      inicio_temp := to_unsigned(16#2#, 3);
    END IF;
    --ref_div_vcc=sfi(vrefn,w_length1,20);
    --level=sfi(round(ref_div_vcc),5,0);
    --if(level>nc)
    --    level=nc;
    --elseif (level<(ncm))
    --    level=(ncm);
    --end
    add_temp := resize(resize(level_signed, 6) + to_signed(16#06#, 6), 7) + to_signed(16#01#, 7);
    levels := add_temp(4 DOWNTO 0);
    posicionC(0) := posC1_unsigned;
    posicionC(1) := posC2_unsigned;
    posicionC(2) := posC3_unsigned;
    posicionC(3) := posC4_unsigned;
    posicionC(4) := posC5_unsigned;
    posicionC(5) := posC6_unsigned;
    add_temp_0 := resize(posicionC(0), 4) + to_unsigned(16#1#, 4);
    sub_cast := signed(resize(add_temp_0, 32));
    IF (tabla_temp(to_integer(resize(levels, 32) - 1))(4) = '0') AND (tabla_temp(to_integer(resize(levels, 32) - 1))(3 DOWNTO 2) /= "00") THEN 
      estadoSW_temp(to_integer(sub_cast - 1)) := "011";
    ELSIF (tabla_temp(to_integer(resize(levels, 32) - 1))(4) = '1') AND (tabla_temp(to_integer(resize(levels, 32) - 1))(3 DOWNTO 2) /= "11") THEN 
      estadoSW_temp(to_integer(sub_cast - 1)) := "100";
    ELSE 
      estadoSW_temp(to_integer(sub_cast - 1)) := tabla_temp(to_integer(resize(levels, 32) - 1))(2 DOWNTO 0);
    END IF;
    add_temp_1 := resize(posicionC(1), 4) + to_unsigned(16#1#, 4);
    sub_cast_0 := signed(resize(add_temp_1, 32));
    IF (tabla_temp(to_integer(resize(levels, 32) + 12))(4) = '0') AND (tabla_temp(to_integer(resize(levels, 32) + 12))(3 DOWNTO 2) /= "00") THEN 
      estadoSW_temp(to_integer(sub_cast_0 - 1)) := "011";
    ELSIF (tabla_temp(to_integer(resize(levels, 32) + 12))(4) = '1') AND (tabla_temp(to_integer(resize(levels, 32) + 12))(3 DOWNTO 2) /= "11") THEN 
      estadoSW_temp(to_integer(sub_cast_0 - 1)) := "100";
    ELSE 
      estadoSW_temp(to_integer(sub_cast_0 - 1)) := tabla_temp(to_integer(resize(levels, 32) + 12))(2 DOWNTO 0);
    END IF;
    add_temp_2 := resize(posicionC(2), 4) + to_unsigned(16#1#, 4);
    sub_cast_1 := signed(resize(add_temp_2, 32));
    IF (tabla_temp(to_integer(resize(levels, 32) + 25))(4) = '0') AND (tabla_temp(to_integer(resize(levels, 32) + 25))(3 DOWNTO 2) /= "00") THEN 
      estadoSW_temp(to_integer(sub_cast_1 - 1)) := "011";
    ELSIF (tabla_temp(to_integer(resize(levels, 32) + 25))(4) = '1') AND (tabla_temp(to_integer(resize(levels, 32) + 25))(3 DOWNTO 2) /= "11") THEN 
      estadoSW_temp(to_integer(sub_cast_1 - 1)) := "100";
    ELSE 
      estadoSW_temp(to_integer(sub_cast_1 - 1)) := tabla_temp(to_integer(resize(levels, 32) + 25))(2 DOWNTO 0);
    END IF;
    add_temp_3 := resize(posicionC(3), 4) + to_unsigned(16#1#, 4);
    sub_cast_2 := signed(resize(add_temp_3, 32));
    IF (tabla_temp(to_integer(resize(levels, 32) + 38))(4) = '0') AND (tabla_temp(to_integer(resize(levels, 32) + 38))(3 DOWNTO 2) /= "00") THEN 
      estadoSW_temp(to_integer(sub_cast_2 - 1)) := "011";
    ELSIF (tabla_temp(to_integer(resize(levels, 32) + 38))(4) = '1') AND (tabla_temp(to_integer(resize(levels, 32) + 38))(3 DOWNTO 2) /= "11") THEN 
      estadoSW_temp(to_integer(sub_cast_2 - 1)) := "100";
    ELSE 
      estadoSW_temp(to_integer(sub_cast_2 - 1)) := tabla_temp(to_integer(resize(levels, 32) + 38))(2 DOWNTO 0);
    END IF;
    add_temp_4 := resize(posicionC(4), 4) + to_unsigned(16#1#, 4);
    sub_cast_3 := signed(resize(add_temp_4, 32));
    IF (tabla_temp(to_integer(resize(levels, 32) + 51))(4) = '0') AND (tabla_temp(to_integer(resize(levels, 32) + 51))(3 DOWNTO 2) /= "00") THEN 
      estadoSW_temp(to_integer(sub_cast_3 - 1)) := "011";
    ELSIF (tabla_temp(to_integer(resize(levels, 32) + 51))(4) = '1') AND (tabla_temp(to_integer(resize(levels, 32) + 51))(3 DOWNTO 2) /= "11") THEN 
      estadoSW_temp(to_integer(sub_cast_3 - 1)) := "100";
    ELSE 
      estadoSW_temp(to_integer(sub_cast_3 - 1)) := tabla_temp(to_integer(resize(levels, 32) + 51))(2 DOWNTO 0);
    END IF;
    add_temp_5 := resize(posicionC(5), 4) + to_unsigned(16#1#, 4);
    sub_cast_4 := signed(resize(add_temp_5, 32));
    IF (tabla_temp(to_integer(resize(levels, 32) + 64))(4) = '0') AND (tabla_temp(to_integer(resize(levels, 32) + 64))(3 DOWNTO 2) /= "00") THEN 
      estadoSW_temp(to_integer(sub_cast_4 - 1)) := "011";
    ELSIF (tabla_temp(to_integer(resize(levels, 32) + 64))(4) = '1') AND (tabla_temp(to_integer(resize(levels, 32) + 64))(3 DOWNTO 2) /= "11") THEN 
      estadoSW_temp(to_integer(sub_cast_4 - 1)) := "100";
    ELSE 
      estadoSW_temp(to_integer(sub_cast_4 - 1)) := tabla_temp(to_integer(resize(levels, 32) + 64))(2 DOWNTO 0);
    END IF;
    S1_tmp <= resize(estadoSW_temp(1), 5);
    S2_tmp <= resize(estadoSW_temp(2), 5);
    S3_tmp <= resize(estadoSW_temp(3), 5);
    S4_tmp <= resize(estadoSW_temp(4), 5);
    S5_tmp <= resize(estadoSW_temp(5), 5);
    S6_tmp <= resize(estadoSW_temp(6), 5);
    inicio_next <= inicio_temp;
    tabla_next <= tabla_temp;
    estadoSW_next <= estadoSW_temp;
  END PROCESS NLC_table_fcn_output;


  S1 <= std_logic_vector(S1_tmp);

  S2 <= std_logic_vector(S2_tmp);

  S3 <= std_logic_vector(S3_tmp);

  S4 <= std_logic_vector(S4_tmp);

  S5 <= std_logic_vector(S5_tmp);

  S6 <= std_logic_vector(S6_tmp);

END rtl;

