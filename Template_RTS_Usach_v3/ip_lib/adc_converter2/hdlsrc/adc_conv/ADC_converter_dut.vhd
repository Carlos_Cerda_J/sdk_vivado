-- -------------------------------------------------------------
-- 
-- File Name: C:\Users\Victor\Documents\matlab_ipcores\Simulink\vo_conv\adc_converter2\hdlsrc\adc_conv\ADC_converter_dut.vhd
-- Created: 2019-11-25 16:24:07
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: ADC_converter_dut
-- Source Path: ADC_converter/ADC_converter_dut
-- Hierarchy Level: 1
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY ADC_converter_dut IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        dut_enable                        :   IN    std_logic;  -- ufix1
        Xmeas                             :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        gain                              :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En23
        offset                            :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
        ce_out                            :   OUT   std_logic;  -- ufix1
        Xconv                             :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En12
        Xconvdb                           :   OUT   std_logic_vector(24 DOWNTO 0)  -- sfix25_En12
        );
END ADC_converter_dut;


ARCHITECTURE rtl OF ADC_converter_dut IS

  -- Component Declarations
  COMPONENT ADC_converter_src_ADC_conv
    PORT( clk                             :   IN    std_logic;
          clk_enable                      :   IN    std_logic;
          reset                           :   IN    std_logic;
          Xmeas                           :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          gain                            :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En23
          offset                          :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          ce_out                          :   OUT   std_logic;  -- ufix1
          Xconv                           :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En12
          Xconvdb                         :   OUT   std_logic_vector(24 DOWNTO 0)  -- sfix25_En12
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : ADC_converter_src_ADC_conv
    USE ENTITY work.ADC_converter_src_ADC_conv(rtl);

  -- Signals
  SIGNAL enb                              : std_logic;
  SIGNAL ce_out_sig                       : std_logic;  -- ufix1
  SIGNAL Xconv_sig                        : std_logic_vector(24 DOWNTO 0);  -- ufix25
  SIGNAL Xconvdb_sig                      : std_logic_vector(24 DOWNTO 0);  -- ufix25

BEGIN
  u_ADC_converter_src_ADC_conv : ADC_converter_src_ADC_conv
    PORT MAP( clk => clk,
              clk_enable => enb,
              reset => reset,
              Xmeas => Xmeas,  -- ufix12
              gain => gain,  -- sfix25_En23
              offset => offset,  -- sfix25_En11
              ce_out => ce_out_sig,  -- ufix1
              Xconv => Xconv_sig,  -- sfix25_En12
              Xconvdb => Xconvdb_sig  -- sfix25_En12
              );

  enb <= dut_enable;

  ce_out <= ce_out_sig;

  Xconv <= Xconv_sig;

  Xconvdb <= Xconvdb_sig;

END rtl;

