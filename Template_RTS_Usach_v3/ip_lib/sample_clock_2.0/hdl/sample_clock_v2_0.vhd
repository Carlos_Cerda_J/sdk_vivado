library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity sample_clock_v2_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S0_AXI_Lite
		C_S0_AXI_Lite_DATA_WIDTH	: integer	:= 32;
		C_S0_AXI_Lite_ADDR_WIDTH	: integer	:= 4
	);
	port (
		-- Users to add ports here
		clk : in STD_LOGIC;
        reset_n : in STD_LOGIC;
        sample : out STD_LOGIC;
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S0_AXI_Lite
		s0_axi_lite_aclk	: in std_logic;
		s0_axi_lite_aresetn	: in std_logic;
		s0_axi_lite_awaddr	: in std_logic_vector(C_S0_AXI_Lite_ADDR_WIDTH-1 downto 0);
		s0_axi_lite_awprot	: in std_logic_vector(2 downto 0);
		s0_axi_lite_awvalid	: in std_logic;
		s0_axi_lite_awready	: out std_logic;
		s0_axi_lite_wdata	: in std_logic_vector(C_S0_AXI_Lite_DATA_WIDTH-1 downto 0);
		s0_axi_lite_wstrb	: in std_logic_vector((C_S0_AXI_Lite_DATA_WIDTH/8)-1 downto 0);
		s0_axi_lite_wvalid	: in std_logic;
		s0_axi_lite_wready	: out std_logic;
		s0_axi_lite_bresp	: out std_logic_vector(1 downto 0);
		s0_axi_lite_bvalid	: out std_logic;
		s0_axi_lite_bready	: in std_logic;
		s0_axi_lite_araddr	: in std_logic_vector(C_S0_AXI_Lite_ADDR_WIDTH-1 downto 0);
		s0_axi_lite_arprot	: in std_logic_vector(2 downto 0);
		s0_axi_lite_arvalid	: in std_logic;
		s0_axi_lite_arready	: out std_logic;
		s0_axi_lite_rdata	: out std_logic_vector(C_S0_AXI_Lite_DATA_WIDTH-1 downto 0);
		s0_axi_lite_rresp	: out std_logic_vector(1 downto 0);
		s0_axi_lite_rvalid	: out std_logic;
		s0_axi_lite_rready	: in std_logic
	);
end sample_clock_v2_0;

architecture arch_imp of sample_clock_v2_0 is

	-- component declaration
	component sample_clock_v2_0_S0_AXI_Lite is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 4
		);
		port (
		clk : in STD_LOGIC;
        reset_n : in STD_LOGIC;
        sample : out STD_LOGIC;		
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component sample_clock_v2_0_S0_AXI_Lite;

begin

-- Instantiation of Axi Bus Interface S0_AXI_Lite
sample_clock_v2_0_S0_AXI_Lite_inst : sample_clock_v2_0_S0_AXI_Lite
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S0_AXI_Lite_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S0_AXI_Lite_ADDR_WIDTH
	)
	port map (
		clk   => clk,
        reset_n => reset_n,
        sample  => sample,
		S_AXI_ACLK	=> s0_axi_lite_aclk,
		S_AXI_ARESETN	=> s0_axi_lite_aresetn,
		S_AXI_AWADDR	=> s0_axi_lite_awaddr,
		S_AXI_AWPROT	=> s0_axi_lite_awprot,
		S_AXI_AWVALID	=> s0_axi_lite_awvalid,
		S_AXI_AWREADY	=> s0_axi_lite_awready,
		S_AXI_WDATA	=> s0_axi_lite_wdata,
		S_AXI_WSTRB	=> s0_axi_lite_wstrb,
		S_AXI_WVALID	=> s0_axi_lite_wvalid,
		S_AXI_WREADY	=> s0_axi_lite_wready,
		S_AXI_BRESP	=> s0_axi_lite_bresp,
		S_AXI_BVALID	=> s0_axi_lite_bvalid,
		S_AXI_BREADY	=> s0_axi_lite_bready,
		S_AXI_ARADDR	=> s0_axi_lite_araddr,
		S_AXI_ARPROT	=> s0_axi_lite_arprot,
		S_AXI_ARVALID	=> s0_axi_lite_arvalid,
		S_AXI_ARREADY	=> s0_axi_lite_arready,
		S_AXI_RDATA	=> s0_axi_lite_rdata,
		S_AXI_RRESP	=> s0_axi_lite_rresp,
		S_AXI_RVALID	=> s0_axi_lite_rvalid,
		S_AXI_RREADY	=> s0_axi_lite_rready
	);

	-- Add user logic here

	-- User logic ends

end arch_imp;
