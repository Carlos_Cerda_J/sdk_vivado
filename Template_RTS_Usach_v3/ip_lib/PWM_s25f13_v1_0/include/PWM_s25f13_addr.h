/*
 * File Name:         hdl_prj\ipcore\PWM_s25f13_v1_0\include\PWM_s25f13_addr.h
 * Description:       C Header File
 * Created:           2019-01-25 10:19:05
*/

#ifndef PWM_S25F13_H_
#define PWM_S25F13_H_

#define  IPCore_Reset_PWM_s25f13       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PWM_s25f13      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PWM_s25f13   0x8  //contains unique IP timestamp (yymmddHHMM): 1901251019
#define  Max_point_Data_PWM_s25f13     0x100  //data register for Inport Max_point

#endif /* PWM_S25F13_H_ */
