library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity sample_clock_50duty_v2_0_tb is
end;

architecture bench of sample_clock_50duty_v2_0_tb is

  component sample_clock_50duty_v2_0
  	generic (
		C_S_AXI_Lite_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_Lite_ADDR_WIDTH	: integer	:= 4
  	);
  	port (
  		clk 	: in STD_LOGIC;
          reset_n : in STD_LOGIC;
          sample 	: out STD_LOGIC;
  		s_axi_lite_aclk	: in std_logic;
  		s_axi_lite_aresetn	: in std_logic;
  		s_axi_lite_awaddr	: in std_logic_vector(4-1 downto 0);
  		s_axi_lite_awprot	: in std_logic_vector(2 downto 0);
  		s_axi_lite_awvalid	: in std_logic;
  		s_axi_lite_awready	: out std_logic;
  		s_axi_lite_wdata	: in std_logic_vector(32-1 downto 0);
  		s_axi_lite_wstrb	: in std_logic_vector((32/8)-1 downto 0);
  		s_axi_lite_wvalid	: in std_logic;
  		s_axi_lite_wready	: out std_logic;
  		s_axi_lite_bresp	: out std_logic_vector(1 downto 0);
  		s_axi_lite_bvalid	: out std_logic;
  		s_axi_lite_bready	: in std_logic;
  		s_axi_lite_araddr	: in std_logic_vector(4-1 downto 0);
  		s_axi_lite_arprot	: in std_logic_vector(2 downto 0);
  		s_axi_lite_arvalid	: in std_logic;
  		s_axi_lite_arready	: out std_logic;
  		s_axi_lite_rdata	: out std_logic_vector(32-1 downto 0);
  		s_axi_lite_rresp	: out std_logic_vector(1 downto 0);
  		s_axi_lite_rvalid	: out std_logic;
  		s_axi_lite_rready	: in std_logic
  	);
  end component;

  signal clk: STD_LOGIC;
  signal reset_n: STD_LOGIC;
  signal sample: STD_LOGIC;
  signal s_axi_lite_aclk: std_logic;
  signal s_axi_lite_aresetn: std_logic;
  signal s_axi_lite_awaddr: std_logic_vector(4-1 downto 0);
  signal s_axi_lite_awprot: std_logic_vector(2 downto 0);
  signal s_axi_lite_awvalid: std_logic;
  signal s_axi_lite_awready: std_logic;
  signal s_axi_lite_wdata: std_logic_vector(32-1 downto 0);
  signal s_axi_lite_wstrb: std_logic_vector((32/8)-1 downto 0);
  signal s_axi_lite_wvalid: std_logic;
  signal s_axi_lite_wready: std_logic;
  signal s_axi_lite_bresp: std_logic_vector(1 downto 0);
  signal s_axi_lite_bvalid: std_logic;
  signal s_axi_lite_bready: std_logic;
  signal s_axi_lite_araddr: std_logic_vector(4-1 downto 0);
  signal s_axi_lite_arprot: std_logic_vector(2 downto 0);
  signal s_axi_lite_arvalid: std_logic;
  signal s_axi_lite_arready: std_logic;
  signal s_axi_lite_rdata: std_logic_vector(32-1 downto 0);
  signal s_axi_lite_rresp: std_logic_vector(1 downto 0);
  signal s_axi_lite_rvalid: std_logic;
  signal s_axi_lite_rready: std_logic ;

  constant clock_period: time := 10 ns;
  constant period_signals: time := 10 us;
  signal stop_the_clock: boolean;
begin

  -- Insert values for generic parameters !!
  uut: sample_clock_50duty_v2_0    port map ( clk                     => clk,
                                              reset_n                 => reset_n,
                                              sample                  => sample,
                                              s_axi_lite_aclk         => s_axi_lite_aclk,
                                              s_axi_lite_aresetn      => s_axi_lite_aresetn,
                                              s_axi_lite_awaddr       => s_axi_lite_awaddr,
                                              s_axi_lite_awprot       => s_axi_lite_awprot,
                                              s_axi_lite_awvalid      => s_axi_lite_awvalid,
                                              s_axi_lite_awready      => s_axi_lite_awready,
                                              s_axi_lite_wdata        => s_axi_lite_wdata,
                                              s_axi_lite_wstrb        => s_axi_lite_wstrb,
                                              s_axi_lite_wvalid       => s_axi_lite_wvalid,
                                              s_axi_lite_wready       => s_axi_lite_wready,
                                              s_axi_lite_bresp        => s_axi_lite_bresp,
                                              s_axi_lite_bvalid       => s_axi_lite_bvalid,
                                              s_axi_lite_bready       => s_axi_lite_bready,
                                              s_axi_lite_araddr       => s_axi_lite_araddr,
                                              s_axi_lite_arprot       => s_axi_lite_arprot,
                                              s_axi_lite_arvalid      => s_axi_lite_arvalid,
                                              s_axi_lite_arready      => s_axi_lite_arready,
                                              s_axi_lite_rdata        => s_axi_lite_rdata,
                                              s_axi_lite_rresp        => s_axi_lite_rresp,
                                              s_axi_lite_rvalid       => s_axi_lite_rvalid,
                                              s_axi_lite_rready       => s_axi_lite_rready );

  stimulus: process
  begin
  
    -- Put initialisation code here

	stop_the_clock <= false;
	reset_n <= '0';
    -- Put test bench stimulus code here
	wait for 100ns;
	reset_n <= '1';
    

    wait;
  end process;
  
  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;