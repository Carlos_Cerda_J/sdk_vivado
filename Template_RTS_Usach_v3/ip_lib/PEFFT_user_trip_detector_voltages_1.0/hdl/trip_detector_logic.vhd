----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/02/2020 04:07:52 PM
-- Design Name: 
-- Module Name: trip_detector_logic - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_misc.or_reduce;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity trip_detector_logic is
	port (
		clock				: in std_logic;
		reset_n				: in std_logic;
		trip_instantaneous	: in  std_logic_vector (35 downto 0);
		trip_triggered		: out std_logic_vector (35 downto 0);
		trip_out			: out std_logic
		);
end trip_detector_logic;

architecture Behavioral of trip_detector_logic is
	signal trip_triggered_S : std_logic_vector (35 downto 0) := (others => '0');
	signal trip_instantaneous_1bit : std_logic := '0';
	type state_t is (WAITING, TRIGGERED);						
    signal state	 : state_t := WAITING;	
begin
	PROCESS(clock, reset_n)
	BEGIN
		IF (reset_n = '0') THEN
			trip_triggered_S <= (others=>'0');
			trip_out <= '0';
			state <= WAITING;
			
		ELSIF rising_edge(clock) THEN
		
			CASE state IS		
            --waiting to general trip goes to 1 and continously assign input trips to the latch trips
            WHEN WAITING =>
					-- Action and Transition
					IF (trip_instantaneous_1bit = '1') THEN
						trip_triggered_S <= trip_instantaneous;
						trip_out <= '1';
						state <= TRIGGERED;
						
					ELSE 
						trip_triggered_S <= (others => '0');
						trip_out <= '0';
						state <= WAITING;
					END if;					
				
				--trip detected. maintain state that generate trip
				WHEN TRIGGERED =>
					-- Action: Maintain the triggered trip values
					trip_triggered_S <= trip_triggered_S;
					trip_out <= '1';
					
					-- Transition: Stay here until reset
					state <= TRIGGERED;
			END CASE;
		END IF;
	END PROCESS;	
	
	trip_instantaneous_1bit <= or_reduce(trip_instantaneous);
	trip_triggered <= trip_triggered_S;
	

end Behavioral;
