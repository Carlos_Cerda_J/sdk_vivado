library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity trip_detector_voltages_v1_0_tb is
end;

architecture bench of trip_detector_voltages_v1_0_tb is

  component trip_detector_voltages_v1_0
  	generic (
  		C_S_AXI_DATA_WIDTH	: integer	:= 32;
  		C_S_AXI_ADDR_WIDTH	: integer	:= 5
  	);
  	port (
  		clock			:  in std_logic;
  		reset_n			:  in std_logic;
  		trip_in			:  in std_logic_vector (35 downto 0);
  		trip_out		:  out std_logic;
  		s_axi_aclk	: in std_logic;
  		s_axi_aresetn	: in std_logic;
  		s_axi_awaddr	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
  		s_axi_awprot	: in std_logic_vector(2 downto 0);
  		s_axi_awvalid	: in std_logic;
  		s_axi_awready	: out std_logic;
  		s_axi_wdata	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
  		s_axi_wstrb	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
  		s_axi_wvalid	: in std_logic;
  		s_axi_wready	: out std_logic;
  		s_axi_bresp	: out std_logic_vector(1 downto 0);
  		s_axi_bvalid	: out std_logic;
  		s_axi_bready	: in std_logic;
  		s_axi_araddr	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
  		s_axi_arprot	: in std_logic_vector(2 downto 0);
  		s_axi_arvalid	: in std_logic;
  		s_axi_arready	: out std_logic;
  		s_axi_rdata	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
  		s_axi_rresp	: out std_logic_vector(1 downto 0);
  		s_axi_rvalid	: out std_logic;
  		s_axi_rready	: in std_logic
  	);
  end component;

	constant C_S_AXI_DATA_WIDTH : integer := 32;
	constant C_S_AXI_ADDR_WIDTH : integer := 5;
  signal clock: std_logic;
  signal reset_n: std_logic;
  signal trip_in: std_logic_vector (35 downto 0);
  signal trip_out: std_logic;
  signal s_axi_aclk: std_logic;
  signal s_axi_aresetn: std_logic;
  signal s_axi_awaddr: std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
  signal s_axi_awprot: std_logic_vector(2 downto 0);
  signal s_axi_awvalid: std_logic;
  signal s_axi_awready: std_logic;
  signal s_axi_wdata: std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
  signal s_axi_wstrb: std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
  signal s_axi_wvalid: std_logic;
  signal s_axi_wready: std_logic;
  signal s_axi_bresp: std_logic_vector(1 downto 0);
  signal s_axi_bvalid: std_logic;
  signal s_axi_bready: std_logic;
  signal s_axi_araddr: std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
  signal s_axi_arprot: std_logic_vector(2 downto 0);
  signal s_axi_arvalid: std_logic;
  signal s_axi_arready: std_logic;
  signal s_axi_rdata: std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
  signal s_axi_rresp: std_logic_vector(1 downto 0);
  signal s_axi_rvalid: std_logic;
  signal s_axi_rready: std_logic ;
  
  constant clock_period: time := 10 ns;
  signal stop_the_clock: boolean;
begin

  -- Insert values for generic parameters !!
  uut: trip_detector_voltages_v1_0 generic map ( C_S_AXI_DATA_WIDTH => 32,
                                                 C_S_AXI_ADDR_WIDTH =>  5)
                                      port map ( clock              => clock,
                                                 reset_n            => reset_n,
                                                 trip_in            => trip_in,
                                                 trip_out           => trip_out,
                                                 s_axi_aclk         => s_axi_aclk,
                                                 s_axi_aresetn      => s_axi_aresetn,
                                                 s_axi_awaddr       => s_axi_awaddr,
                                                 s_axi_awprot       => s_axi_awprot,
                                                 s_axi_awvalid      => s_axi_awvalid,
                                                 s_axi_awready      => s_axi_awready,
                                                 s_axi_wdata        => s_axi_wdata,
                                                 s_axi_wstrb        => s_axi_wstrb,
                                                 s_axi_wvalid       => s_axi_wvalid,
                                                 s_axi_wready       => s_axi_wready,
                                                 s_axi_bresp        => s_axi_bresp,
                                                 s_axi_bvalid       => s_axi_bvalid,
                                                 s_axi_bready       => s_axi_bready,
                                                 s_axi_araddr       => s_axi_araddr,
                                                 s_axi_arprot       => s_axi_arprot,
                                                 s_axi_arvalid      => s_axi_arvalid,
                                                 s_axi_arready      => s_axi_arready,
                                                 s_axi_rdata        => s_axi_rdata,
                                                 s_axi_rresp        => s_axi_rresp,
                                                 s_axi_rvalid       => s_axi_rvalid,
                                                 s_axi_rready       => s_axi_rready );

  stimulus: process
  begin
    -- Put initialisation code here
	stop_the_clock <= false;
	trip_in <= (others => '0');
	reset_n <= '0';
    wait for 20 ns;
    reset_n <= '1';

    wait for 50 ns;
    trip_in(0) <= '1';
	wait for 50 ns;
    trip_in(0) <= '0';
	wait for 50 ns;
    trip_in(0) <= '1';
    -- Put test bench stimulus code here

    wait;
  end process;

	clocking: process
	begin
		while not stop_the_clock loop
		  clock <= '0', '1' after clock_period / 2;
		  wait for clock_period;
		end loop;
		wait;
	end process;
end;