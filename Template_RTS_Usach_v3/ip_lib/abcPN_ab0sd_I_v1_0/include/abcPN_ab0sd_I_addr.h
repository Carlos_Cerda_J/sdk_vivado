/*
 * File Name:         C:\IPCORES\ABCPN_AB0SD_I\ipcore\abcPN_ab0sd_I_v1_0\include\abcPN_ab0sd_I_addr.h
 * Description:       C Header File
 * Created:           2019-01-11 11:59:17
*/

#ifndef ABCPN_AB0SD_I_H_
#define ABCPN_AB0SD_I_H_

#define  IPCore_Reset_abcPN_ab0sd_I       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_abcPN_ab0sd_I      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_abcPN_ab0sd_I   0x8  //contains unique IP timestamp (yymmddHHMM): 1901111159

#endif /* ABCPN_AB0SD_I_H_ */
