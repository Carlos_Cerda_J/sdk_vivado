-- -------------------------------------------------------------
-- 
-- File Name: hdl_prj\hdlsrc\shiftv2\firstO_Filter_ip_src_firstO_filter.vhd
-- Created: 2019-01-17 11:54:49
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- 
-- -------------------------------------------------------------
-- Rate and Clocking Details
-- -------------------------------------------------------------
-- Model base rate: 1e-07
-- Target subsystem base rate: 1e-06
-- 
-- 
-- Clock Enable  Sample Time
-- -------------------------------------------------------------
-- ce_out        1e-06
-- -------------------------------------------------------------
-- 
-- 
-- Output Signal                 Clock Enable  Sample Time
-- -------------------------------------------------------------
-- out_rsvd                      ce_out        1e-06
-- -------------------------------------------------------------
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: firstO_Filter_ip_src_firstO_filter
-- Source Path: shiftv2/firstO_filter
-- Hierarchy Level: 0
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE work.firstO_Filter_ip_src_firstO_filter_pkg.ALL;

ENTITY firstO_Filter_ip_src_firstO_filter IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        clk_enable                        :   IN    std_logic;
        a1                                :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En16
        input_shift                       :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        b0                                :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En16
        b1                                :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En16
        out_1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        in_1                              :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        ce_out                            :   OUT   std_logic;
        out_rsvd                          :   OUT   std_logic_vector(24 DOWNTO 0)  -- sfix25_En13
        );
END firstO_Filter_ip_src_firstO_filter;


ARCHITECTURE rtl OF firstO_Filter_ip_src_firstO_filter IS

  -- Signals
  SIGNAL enb_1_10_0                       : std_logic;
  SIGNAL TmpGroundAtMultiply_Add3Inport3_out1 : signed(42 DOWNTO 0);  -- sfix43_En29
  SIGNAL HwModeRegister_reg               : vector_of_signed43(0 TO 1);  -- sfix43 [2]
  SIGNAL TmpGroundAtMultiply_Add3Inport3_out1_1 : signed(42 DOWNTO 0);  -- sfix43_En29
  SIGNAL in_1_signed                      : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL in_1_1                           : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL b1_signed                        : signed(17 DOWNTO 0);  -- sfix18_En16
  SIGNAL b1_1                             : signed(17 DOWNTO 0);  -- sfix18_En16
  SIGNAL mulOutput                        : signed(42 DOWNTO 0);  -- sfix43_En29
  SIGNAL mulOutput_1                      : signed(42 DOWNTO 0);  -- sfix43_En29
  SIGNAL Multiply_Add3_add_add_cast       : signed(43 DOWNTO 0);  -- sfix44_En29
  SIGNAL Multiply_Add3_add_add_cast_1     : signed(43 DOWNTO 0);  -- sfix44_En29
  SIGNAL Multiply_Add3_out1               : signed(43 DOWNTO 0);  -- sfix44_En29
  SIGNAL Multiply_Add3_out1_1             : signed(43 DOWNTO 0);  -- sfix44_En29
  SIGNAL Multiply_Add3_out1_2             : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL HwModeRegister_reg_1             : vector_of_signed25(0 TO 1);  -- sfix25 [2]
  SIGNAL Multiply_Add3_out1_3             : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL out_1_signed                     : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL HwModeRegister3_reg              : vector_of_signed25(0 TO 3);  -- sfix25 [4]
  SIGNAL out_1_1                          : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL a1_signed                        : signed(17 DOWNTO 0);  -- sfix18_En16
  SIGNAL HwModeRegister4_reg              : vector_of_signed18(0 TO 3);  -- sfix18 [4]
  SIGNAL a1_1                             : signed(17 DOWNTO 0);  -- sfix18_En16
  SIGNAL mulOutput_2                      : signed(42 DOWNTO 0);  -- sfix43_En29
  SIGNAL mulOutput_3                      : signed(42 DOWNTO 0);  -- sfix43_En29
  SIGNAL Multiply_Add2_add_sub_cast       : signed(43 DOWNTO 0);  -- sfix44_En29
  SIGNAL Multiply_Add2_add_sub_cast_1     : signed(43 DOWNTO 0);  -- sfix44_En29
  SIGNAL out_rsvd_1                       : signed(43 DOWNTO 0);  -- sfix44_En29
  SIGNAL out_rsvd_2                       : signed(43 DOWNTO 0);  -- sfix44_En29
  SIGNAL out_rsvd_3                       : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL HwModeRegister_reg_2             : vector_of_signed25(0 TO 1);  -- sfix25 [2]
  SIGNAL out_rsvd_4                       : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL input_shift_signed               : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL HwModeRegister_reg_3             : vector_of_signed25(0 TO 6);  -- sfix25 [7]
  SIGNAL input_shift_1                    : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL b0_signed                        : signed(17 DOWNTO 0);  -- sfix18_En16
  SIGNAL HwModeRegister1_reg              : vector_of_signed18(0 TO 6);  -- sfix18 [7]
  SIGNAL b0_1                             : signed(17 DOWNTO 0);  -- sfix18_En16
  SIGNAL mulOutput_4                      : signed(42 DOWNTO 0);  -- sfix43_En29
  SIGNAL mulOutput_5                      : signed(42 DOWNTO 0);  -- sfix43_En29
  SIGNAL Multiply_Add1_add_add_cast       : signed(43 DOWNTO 0);  -- sfix44_En29
  SIGNAL Multiply_Add1_add_add_cast_1     : signed(43 DOWNTO 0);  -- sfix44_En29
  SIGNAL Multiply_Add1_out1               : signed(43 DOWNTO 0);  -- sfix44_En29
  SIGNAL Multiply_Add1_out1_1             : signed(43 DOWNTO 0);  -- sfix44_En29
  SIGNAL Multiply_Add1_out1_2             : signed(24 DOWNTO 0);  -- sfix25_En13

  ATTRIBUTE use_dsp48 : string;

  ATTRIBUTE use_dsp48 OF mulOutput : SIGNAL IS "yes";
  ATTRIBUTE use_dsp48 OF mulOutput_2 : SIGNAL IS "yes";
  ATTRIBUTE use_dsp48 OF mulOutput_4 : SIGNAL IS "yes";

BEGIN
  TmpGroundAtMultiply_Add3Inport3_out1 <= to_signed(0, 43);

  enb_1_10_0 <= clk_enable;

  HwModeRegister_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        HwModeRegister_reg <= (OTHERS => to_signed(0, 43));
      ELSIF enb_1_10_0 = '1' THEN
        HwModeRegister_reg(0) <= TmpGroundAtMultiply_Add3Inport3_out1;
        HwModeRegister_reg(1) <= HwModeRegister_reg(0);
      END IF;
    END IF;
  END PROCESS HwModeRegister_process;

  TmpGroundAtMultiply_Add3Inport3_out1_1 <= HwModeRegister_reg(1);

  in_1_signed <= signed(in_1);

  HwModeRegister6_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        in_1_1 <= to_signed(16#0000000#, 25);
      ELSIF enb_1_10_0 = '1' THEN
        in_1_1 <= in_1_signed;
      END IF;
    END IF;
  END PROCESS HwModeRegister6_process;


  b1_signed <= signed(b1);

  HwModeRegister7_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        b1_1 <= to_signed(16#00000#, 18);
      ELSIF enb_1_10_0 = '1' THEN
        b1_1 <= b1_signed;
      END IF;
    END IF;
  END PROCESS HwModeRegister7_process;


  mulOutput <= in_1_1 * b1_1;

  HwModeRegister_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        mulOutput_1 <= to_signed(0, 43);
      ELSIF enb_1_10_0 = '1' THEN
        mulOutput_1 <= mulOutput;
      END IF;
    END IF;
  END PROCESS HwModeRegister_1_process;


  Multiply_Add3_add_add_cast <= resize(TmpGroundAtMultiply_Add3Inport3_out1_1, 44);
  Multiply_Add3_add_add_cast_1 <= resize(mulOutput_1, 44);
  Multiply_Add3_out1 <= Multiply_Add3_add_add_cast + Multiply_Add3_add_add_cast_1;

  HwModeRegister_2_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Multiply_Add3_out1_1 <= to_signed(0, 44);
      ELSIF enb_1_10_0 = '1' THEN
        Multiply_Add3_out1_1 <= Multiply_Add3_out1;
      END IF;
    END IF;
  END PROCESS HwModeRegister_2_process;


  
  Multiply_Add3_out1_2 <= "0111111111111111111111111" WHEN (Multiply_Add3_out1_1(43) = '0') AND (Multiply_Add3_out1_1(42 DOWNTO 40) /= "000") ELSE
      "1000000000000000000000000" WHEN (Multiply_Add3_out1_1(43) = '1') AND (Multiply_Add3_out1_1(42 DOWNTO 40) /= "111") ELSE
      Multiply_Add3_out1_1(40 DOWNTO 16);

  HwModeRegister_3_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        HwModeRegister_reg_1 <= (OTHERS => to_signed(16#0000000#, 25));
      ELSIF enb_1_10_0 = '1' THEN
        HwModeRegister_reg_1(0) <= Multiply_Add3_out1_2;
        HwModeRegister_reg_1(1) <= HwModeRegister_reg_1(0);
      END IF;
    END IF;
  END PROCESS HwModeRegister_3_process;

  Multiply_Add3_out1_3 <= HwModeRegister_reg_1(1);

  out_1_signed <= signed(out_1);

  HwModeRegister3_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        HwModeRegister3_reg <= (OTHERS => to_signed(16#0000000#, 25));
      ELSIF enb_1_10_0 = '1' THEN
        HwModeRegister3_reg(0) <= out_1_signed;
        HwModeRegister3_reg(1 TO 3) <= HwModeRegister3_reg(0 TO 2);
      END IF;
    END IF;
  END PROCESS HwModeRegister3_process;

  out_1_1 <= HwModeRegister3_reg(3);

  a1_signed <= signed(a1);

  HwModeRegister4_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        HwModeRegister4_reg <= (OTHERS => to_signed(16#00000#, 18));
      ELSIF enb_1_10_0 = '1' THEN
        HwModeRegister4_reg(0) <= a1_signed;
        HwModeRegister4_reg(1 TO 3) <= HwModeRegister4_reg(0 TO 2);
      END IF;
    END IF;
  END PROCESS HwModeRegister4_process;

  a1_1 <= HwModeRegister4_reg(3);

  mulOutput_2 <= out_1_1 * a1_1;

  HwModeRegister_4_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        mulOutput_3 <= to_signed(0, 43);
      ELSIF enb_1_10_0 = '1' THEN
        mulOutput_3 <= mulOutput_2;
      END IF;
    END IF;
  END PROCESS HwModeRegister_4_process;


  Multiply_Add2_add_sub_cast <= resize(Multiply_Add3_out1_3 & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0', 44);
  Multiply_Add2_add_sub_cast_1 <= resize(mulOutput_3, 44);
  out_rsvd_1 <= Multiply_Add2_add_sub_cast - Multiply_Add2_add_sub_cast_1;

  HwModeRegister_5_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        out_rsvd_2 <= to_signed(0, 44);
      ELSIF enb_1_10_0 = '1' THEN
        out_rsvd_2 <= out_rsvd_1;
      END IF;
    END IF;
  END PROCESS HwModeRegister_5_process;


  
  out_rsvd_3 <= "0111111111111111111111111" WHEN (out_rsvd_2(43) = '0') AND (out_rsvd_2(42 DOWNTO 40) /= "000") ELSE
      "1000000000000000000000000" WHEN (out_rsvd_2(43) = '1') AND (out_rsvd_2(42 DOWNTO 40) /= "111") ELSE
      out_rsvd_2(40 DOWNTO 16);

  HwModeRegister_6_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        HwModeRegister_reg_2 <= (OTHERS => to_signed(16#0000000#, 25));
      ELSIF enb_1_10_0 = '1' THEN
        HwModeRegister_reg_2(0) <= out_rsvd_3;
        HwModeRegister_reg_2(1) <= HwModeRegister_reg_2(0);
      END IF;
    END IF;
  END PROCESS HwModeRegister_6_process;

  out_rsvd_4 <= HwModeRegister_reg_2(1);

  input_shift_signed <= signed(input_shift);

  HwModeRegister_7_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        HwModeRegister_reg_3 <= (OTHERS => to_signed(16#0000000#, 25));
      ELSIF enb_1_10_0 = '1' THEN
        HwModeRegister_reg_3(0) <= input_shift_signed;
        HwModeRegister_reg_3(1 TO 6) <= HwModeRegister_reg_3(0 TO 5);
      END IF;
    END IF;
  END PROCESS HwModeRegister_7_process;

  input_shift_1 <= HwModeRegister_reg_3(6);

  b0_signed <= signed(b0);

  HwModeRegister1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        HwModeRegister1_reg <= (OTHERS => to_signed(16#00000#, 18));
      ELSIF enb_1_10_0 = '1' THEN
        HwModeRegister1_reg(0) <= b0_signed;
        HwModeRegister1_reg(1 TO 6) <= HwModeRegister1_reg(0 TO 5);
      END IF;
    END IF;
  END PROCESS HwModeRegister1_process;

  b0_1 <= HwModeRegister1_reg(6);

  mulOutput_4 <= input_shift_1 * b0_1;

  HwModeRegister_8_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        mulOutput_5 <= to_signed(0, 43);
      ELSIF enb_1_10_0 = '1' THEN
        mulOutput_5 <= mulOutput_4;
      END IF;
    END IF;
  END PROCESS HwModeRegister_8_process;


  Multiply_Add1_add_add_cast <= resize(out_rsvd_4 & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0', 44);
  Multiply_Add1_add_add_cast_1 <= resize(mulOutput_5, 44);
  Multiply_Add1_out1 <= Multiply_Add1_add_add_cast + Multiply_Add1_add_add_cast_1;

  HwModeRegister_9_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        Multiply_Add1_out1_1 <= to_signed(0, 44);
      ELSIF enb_1_10_0 = '1' THEN
        Multiply_Add1_out1_1 <= Multiply_Add1_out1;
      END IF;
    END IF;
  END PROCESS HwModeRegister_9_process;


  
  Multiply_Add1_out1_2 <= "0111111111111111111111111" WHEN (Multiply_Add1_out1_1(43) = '0') AND (Multiply_Add1_out1_1(42 DOWNTO 40) /= "000") ELSE
      "1000000000000000000000000" WHEN (Multiply_Add1_out1_1(43) = '1') AND (Multiply_Add1_out1_1(42 DOWNTO 40) /= "111") ELSE
      Multiply_Add1_out1_1(40 DOWNTO 16);

  out_rsvd <= std_logic_vector(Multiply_Add1_out1_2);

  ce_out <= clk_enable;

END rtl;

