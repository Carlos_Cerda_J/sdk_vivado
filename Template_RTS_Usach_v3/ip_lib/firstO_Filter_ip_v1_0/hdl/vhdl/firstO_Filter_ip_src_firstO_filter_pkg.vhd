-- -------------------------------------------------------------
-- 
-- File Name: hdl_prj\hdlsrc\shiftv2\firstO_Filter_ip_src_firstO_filter_pkg.vhd
-- Created: 2019-01-17 11:54:49
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

PACKAGE firstO_Filter_ip_src_firstO_filter_pkg IS
  TYPE vector_of_signed43 IS ARRAY (NATURAL RANGE <>) OF signed(42 DOWNTO 0);
  TYPE vector_of_signed25 IS ARRAY (NATURAL RANGE <>) OF signed(24 DOWNTO 0);
  TYPE vector_of_signed18 IS ARRAY (NATURAL RANGE <>) OF signed(17 DOWNTO 0);
END firstO_Filter_ip_src_firstO_filter_pkg;

