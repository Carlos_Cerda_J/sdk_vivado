/*
 * File Name:         hdl_prj\ipcore\firstO_Filter_ip_v1_0\include\firstO_Filter_ip_addr.h
 * Description:       C Header File
 * Created:           2019-01-17 11:54:51
*/

#ifndef FIRSTO_FILTER_IP_H_
#define FIRSTO_FILTER_IP_H_

#define  IPCore_Reset_firstO_Filter_ip       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_firstO_Filter_ip      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_firstO_Filter_ip   0x8  //contains unique IP timestamp (yymmddHHMM): 1901171154
#define  a1_Data_firstO_Filter_ip            0x100  //data register for Inport a1
#define  b0_Data_firstO_Filter_ip            0x104  //data register for Inport b0
#define  b1_Data_firstO_Filter_ip            0x108  //data register for Inport b1

#endif /* FIRSTO_FILTER_IP_H_ */
