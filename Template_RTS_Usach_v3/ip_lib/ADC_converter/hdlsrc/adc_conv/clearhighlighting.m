SLStudio.Utils.RemoveHighlighting(get_param('adc_conv', 'handle'));
SLStudio.Utils.RemoveHighlighting(get_param('gm_adc_conv', 'handle'));
annotate_port('gm_adc_conv/ADC_conv/Product', 0, 1, '');
annotate_port('gm_adc_conv/ADC_conv/PipelineRegister', 1, 1, '');
annotate_port('gm_adc_conv/ADC_conv/HwModeRegister', 0, 1, '');
