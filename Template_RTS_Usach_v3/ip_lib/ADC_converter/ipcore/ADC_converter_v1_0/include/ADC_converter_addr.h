/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\vo_conv\ADC_converter\ipcore\ADC_converter_v1_0\include\ADC_converter_addr.h
 * Description:       C Header File
 * Created:           2019-07-30 17:40:11
*/

#ifndef ADC_CONVERTER_H_
#define ADC_CONVERTER_H_

#define  IPCore_Reset_ADC_converter       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_ADC_converter      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_ADC_converter   0x8  //contains unique IP timestamp (yymmddHHMM): 1907301740
#define  gain_Data_ADC_converter          0x100  //data register for Inport gain
#define  offset_Data_ADC_converter        0x104  //data register for Inport offset
#define  Xconvdb_Data_ADC_converter       0x108  //data register for Outport Xconvdb

#endif /* ADC_CONVERTER_H_ */
