-- -------------------------------------------------------------
-- 
-- File Name: E:\xilinx\IPCORES\fast_adc_conv\hdlsrc\trafo_fastadc\fast_adc_conv_addr_decoder.vhd
-- Created: 2019-01-30 18:33:47
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: fast_adc_conv_addr_decoder
-- Source Path: fast_adc_conv/fast_adc_conv_axi_lite/fast_adc_conv_addr_decoder
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY fast_adc_conv_addr_decoder IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        data_write                        :   IN    std_logic_vector(31 DOWNTO 0);  -- ufix32
        addr_sel                          :   IN    std_logic_vector(13 DOWNTO 0);  -- ufix14
        wr_enb                            :   IN    std_logic;  -- ufix1
        rd_enb                            :   IN    std_logic;  -- ufix1
        read_ip_timestamp                 :   IN    std_logic_vector(31 DOWNTO 0);  -- ufix32
        data_read                         :   OUT   std_logic_vector(31 DOWNTO 0);  -- ufix32
        write_axi_enable                  :   OUT   std_logic;  -- ufix1
        write_Gain1                       :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En24
        write_Offset1                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En19
        write_Gain2                       :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En24
        write_Offset2                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En19
        write_Gain3                       :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En24
        write_Offset3                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En19
        write_Gain4                       :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En24
        write_Offset4                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En19
        write_Gain5                       :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En24
        write_Offset5                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En19
        write_Gain6                       :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En24
        write_Offset6                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En19
        write_Gain7                       :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En24
        write_Offset7                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En19
        write_Gain8                       :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En24
        write_Offset8                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En19
        write_Gain9                       :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En24
        write_Offset9                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En19
        write_Gain10                      :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En24
        write_Offset10                    :   OUT   std_logic_vector(24 DOWNTO 0)  -- sfix25_En19
        );
END fast_adc_conv_addr_decoder;


ARCHITECTURE rtl OF fast_adc_conv_addr_decoder IS

  -- Signals
  SIGNAL enb                              : std_logic;
  SIGNAL addr_sel_unsigned                : unsigned(13 DOWNTO 0);  -- ufix14
  SIGNAL decode_sel_ip_timestamp          : std_logic;  -- ufix1
  SIGNAL read_ip_timestamp_unsigned       : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL const_1                          : std_logic;  -- ufix1
  SIGNAL const_0                          : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_ip_timestamp            : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_ip_timestamp           : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_sel_axi_enable            : std_logic;  -- ufix1
  SIGNAL reg_enb_axi_enable               : std_logic;  -- ufix1
  SIGNAL data_write_unsigned              : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL data_in_axi_enable               : std_logic;  -- ufix1
  SIGNAL write_reg_axi_enable             : std_logic;  -- ufix1
  SIGNAL decode_sel_Gain1                 : std_logic;  -- ufix1
  SIGNAL reg_enb_Gain1                    : std_logic;  -- ufix1
  SIGNAL data_in_Gain1                    : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL write_reg_Gain1                  : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL decode_sel_Offset1               : std_logic;  -- ufix1
  SIGNAL reg_enb_Offset1                  : std_logic;  -- ufix1
  SIGNAL data_in_Offset1                  : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL write_reg_Offset1                : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL decode_sel_Gain2                 : std_logic;  -- ufix1
  SIGNAL reg_enb_Gain2                    : std_logic;  -- ufix1
  SIGNAL data_in_Gain2                    : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL write_reg_Gain2                  : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL decode_sel_Offset2               : std_logic;  -- ufix1
  SIGNAL reg_enb_Offset2                  : std_logic;  -- ufix1
  SIGNAL data_in_Offset2                  : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL write_reg_Offset2                : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL decode_sel_Gain3                 : std_logic;  -- ufix1
  SIGNAL reg_enb_Gain3                    : std_logic;  -- ufix1
  SIGNAL data_in_Gain3                    : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL write_reg_Gain3                  : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL decode_sel_Offset3               : std_logic;  -- ufix1
  SIGNAL reg_enb_Offset3                  : std_logic;  -- ufix1
  SIGNAL data_in_Offset3                  : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL write_reg_Offset3                : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL decode_sel_Gain4                 : std_logic;  -- ufix1
  SIGNAL reg_enb_Gain4                    : std_logic;  -- ufix1
  SIGNAL data_in_Gain4                    : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL write_reg_Gain4                  : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL decode_sel_Offset4               : std_logic;  -- ufix1
  SIGNAL reg_enb_Offset4                  : std_logic;  -- ufix1
  SIGNAL data_in_Offset4                  : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL write_reg_Offset4                : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL decode_sel_Gain5                 : std_logic;  -- ufix1
  SIGNAL reg_enb_Gain5                    : std_logic;  -- ufix1
  SIGNAL data_in_Gain5                    : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL write_reg_Gain5                  : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL decode_sel_Offset5               : std_logic;  -- ufix1
  SIGNAL reg_enb_Offset5                  : std_logic;  -- ufix1
  SIGNAL data_in_Offset5                  : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL write_reg_Offset5                : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL decode_sel_Gain6                 : std_logic;  -- ufix1
  SIGNAL reg_enb_Gain6                    : std_logic;  -- ufix1
  SIGNAL data_in_Gain6                    : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL write_reg_Gain6                  : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL decode_sel_Offset6               : std_logic;  -- ufix1
  SIGNAL reg_enb_Offset6                  : std_logic;  -- ufix1
  SIGNAL data_in_Offset6                  : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL write_reg_Offset6                : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL decode_sel_Gain7                 : std_logic;  -- ufix1
  SIGNAL reg_enb_Gain7                    : std_logic;  -- ufix1
  SIGNAL data_in_Gain7                    : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL write_reg_Gain7                  : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL decode_sel_Offset7               : std_logic;  -- ufix1
  SIGNAL reg_enb_Offset7                  : std_logic;  -- ufix1
  SIGNAL data_in_Offset7                  : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL write_reg_Offset7                : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL decode_sel_Gain8                 : std_logic;  -- ufix1
  SIGNAL reg_enb_Gain8                    : std_logic;  -- ufix1
  SIGNAL data_in_Gain8                    : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL write_reg_Gain8                  : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL decode_sel_Offset8               : std_logic;  -- ufix1
  SIGNAL reg_enb_Offset8                  : std_logic;  -- ufix1
  SIGNAL data_in_Offset8                  : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL write_reg_Offset8                : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL decode_sel_Gain9                 : std_logic;  -- ufix1
  SIGNAL reg_enb_Gain9                    : std_logic;  -- ufix1
  SIGNAL data_in_Gain9                    : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL write_reg_Gain9                  : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL decode_sel_Offset9               : std_logic;  -- ufix1
  SIGNAL reg_enb_Offset9                  : std_logic;  -- ufix1
  SIGNAL data_in_Offset9                  : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL write_reg_Offset9                : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL decode_sel_Gain10                : std_logic;  -- ufix1
  SIGNAL reg_enb_Gain10                   : std_logic;  -- ufix1
  SIGNAL data_in_Gain10                   : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL write_reg_Gain10                 : signed(24 DOWNTO 0);  -- sfix25_En24
  SIGNAL decode_sel_Offset10              : std_logic;  -- ufix1
  SIGNAL reg_enb_Offset10                 : std_logic;  -- ufix1
  SIGNAL data_in_Offset10                 : signed(24 DOWNTO 0);  -- sfix25_En19
  SIGNAL write_reg_Offset10               : signed(24 DOWNTO 0);  -- sfix25_En19

BEGIN
  addr_sel_unsigned <= unsigned(addr_sel);

  
  decode_sel_ip_timestamp <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0002#, 14) ELSE
      '0';

  read_ip_timestamp_unsigned <= unsigned(read_ip_timestamp);

  const_1 <= '1';

  enb <= const_1;

  const_0 <= to_unsigned(0, 32);

  reg_ip_timestamp_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        read_reg_ip_timestamp <= to_unsigned(0, 32);
      ELSIF enb = '1' THEN
        read_reg_ip_timestamp <= read_ip_timestamp_unsigned;
      END IF;
    END IF;
  END PROCESS reg_ip_timestamp_process;


  
  decode_rd_ip_timestamp <= const_0 WHEN decode_sel_ip_timestamp = '0' ELSE
      read_reg_ip_timestamp;

  data_read <= std_logic_vector(decode_rd_ip_timestamp);

  
  decode_sel_axi_enable <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0001#, 14) ELSE
      '0';

  reg_enb_axi_enable <= decode_sel_axi_enable AND wr_enb;

  data_write_unsigned <= unsigned(data_write);

  data_in_axi_enable <= data_write_unsigned(0);

  reg_axi_enable_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_axi_enable <= '1';
      ELSIF enb = '1' AND reg_enb_axi_enable = '1' THEN
        write_reg_axi_enable <= data_in_axi_enable;
      END IF;
    END IF;
  END PROCESS reg_axi_enable_process;


  write_axi_enable <= write_reg_axi_enable;

  
  decode_sel_Gain1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0040#, 14) ELSE
      '0';

  reg_enb_Gain1 <= decode_sel_Gain1 AND wr_enb;

  data_in_Gain1 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Gain1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Gain1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Gain1 = '1' THEN
        write_reg_Gain1 <= data_in_Gain1;
      END IF;
    END IF;
  END PROCESS reg_Gain1_process;


  write_Gain1 <= std_logic_vector(write_reg_Gain1);

  
  decode_sel_Offset1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0041#, 14) ELSE
      '0';

  reg_enb_Offset1 <= decode_sel_Offset1 AND wr_enb;

  data_in_Offset1 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Offset1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Offset1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Offset1 = '1' THEN
        write_reg_Offset1 <= data_in_Offset1;
      END IF;
    END IF;
  END PROCESS reg_Offset1_process;


  write_Offset1 <= std_logic_vector(write_reg_Offset1);

  
  decode_sel_Gain2 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0042#, 14) ELSE
      '0';

  reg_enb_Gain2 <= decode_sel_Gain2 AND wr_enb;

  data_in_Gain2 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Gain2_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Gain2 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Gain2 = '1' THEN
        write_reg_Gain2 <= data_in_Gain2;
      END IF;
    END IF;
  END PROCESS reg_Gain2_process;


  write_Gain2 <= std_logic_vector(write_reg_Gain2);

  
  decode_sel_Offset2 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0043#, 14) ELSE
      '0';

  reg_enb_Offset2 <= decode_sel_Offset2 AND wr_enb;

  data_in_Offset2 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Offset2_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Offset2 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Offset2 = '1' THEN
        write_reg_Offset2 <= data_in_Offset2;
      END IF;
    END IF;
  END PROCESS reg_Offset2_process;


  write_Offset2 <= std_logic_vector(write_reg_Offset2);

  
  decode_sel_Gain3 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0044#, 14) ELSE
      '0';

  reg_enb_Gain3 <= decode_sel_Gain3 AND wr_enb;

  data_in_Gain3 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Gain3_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Gain3 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Gain3 = '1' THEN
        write_reg_Gain3 <= data_in_Gain3;
      END IF;
    END IF;
  END PROCESS reg_Gain3_process;


  write_Gain3 <= std_logic_vector(write_reg_Gain3);

  
  decode_sel_Offset3 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0045#, 14) ELSE
      '0';

  reg_enb_Offset3 <= decode_sel_Offset3 AND wr_enb;

  data_in_Offset3 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Offset3_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Offset3 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Offset3 = '1' THEN
        write_reg_Offset3 <= data_in_Offset3;
      END IF;
    END IF;
  END PROCESS reg_Offset3_process;


  write_Offset3 <= std_logic_vector(write_reg_Offset3);

  
  decode_sel_Gain4 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0046#, 14) ELSE
      '0';

  reg_enb_Gain4 <= decode_sel_Gain4 AND wr_enb;

  data_in_Gain4 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Gain4_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Gain4 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Gain4 = '1' THEN
        write_reg_Gain4 <= data_in_Gain4;
      END IF;
    END IF;
  END PROCESS reg_Gain4_process;


  write_Gain4 <= std_logic_vector(write_reg_Gain4);

  
  decode_sel_Offset4 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0047#, 14) ELSE
      '0';

  reg_enb_Offset4 <= decode_sel_Offset4 AND wr_enb;

  data_in_Offset4 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Offset4_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Offset4 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Offset4 = '1' THEN
        write_reg_Offset4 <= data_in_Offset4;
      END IF;
    END IF;
  END PROCESS reg_Offset4_process;


  write_Offset4 <= std_logic_vector(write_reg_Offset4);

  
  decode_sel_Gain5 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0048#, 14) ELSE
      '0';

  reg_enb_Gain5 <= decode_sel_Gain5 AND wr_enb;

  data_in_Gain5 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Gain5_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Gain5 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Gain5 = '1' THEN
        write_reg_Gain5 <= data_in_Gain5;
      END IF;
    END IF;
  END PROCESS reg_Gain5_process;


  write_Gain5 <= std_logic_vector(write_reg_Gain5);

  
  decode_sel_Offset5 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0049#, 14) ELSE
      '0';

  reg_enb_Offset5 <= decode_sel_Offset5 AND wr_enb;

  data_in_Offset5 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Offset5_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Offset5 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Offset5 = '1' THEN
        write_reg_Offset5 <= data_in_Offset5;
      END IF;
    END IF;
  END PROCESS reg_Offset5_process;


  write_Offset5 <= std_logic_vector(write_reg_Offset5);

  
  decode_sel_Gain6 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004A#, 14) ELSE
      '0';

  reg_enb_Gain6 <= decode_sel_Gain6 AND wr_enb;

  data_in_Gain6 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Gain6_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Gain6 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Gain6 = '1' THEN
        write_reg_Gain6 <= data_in_Gain6;
      END IF;
    END IF;
  END PROCESS reg_Gain6_process;


  write_Gain6 <= std_logic_vector(write_reg_Gain6);

  
  decode_sel_Offset6 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004B#, 14) ELSE
      '0';

  reg_enb_Offset6 <= decode_sel_Offset6 AND wr_enb;

  data_in_Offset6 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Offset6_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Offset6 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Offset6 = '1' THEN
        write_reg_Offset6 <= data_in_Offset6;
      END IF;
    END IF;
  END PROCESS reg_Offset6_process;


  write_Offset6 <= std_logic_vector(write_reg_Offset6);

  
  decode_sel_Gain7 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004C#, 14) ELSE
      '0';

  reg_enb_Gain7 <= decode_sel_Gain7 AND wr_enb;

  data_in_Gain7 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Gain7_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Gain7 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Gain7 = '1' THEN
        write_reg_Gain7 <= data_in_Gain7;
      END IF;
    END IF;
  END PROCESS reg_Gain7_process;


  write_Gain7 <= std_logic_vector(write_reg_Gain7);

  
  decode_sel_Offset7 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004D#, 14) ELSE
      '0';

  reg_enb_Offset7 <= decode_sel_Offset7 AND wr_enb;

  data_in_Offset7 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Offset7_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Offset7 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Offset7 = '1' THEN
        write_reg_Offset7 <= data_in_Offset7;
      END IF;
    END IF;
  END PROCESS reg_Offset7_process;


  write_Offset7 <= std_logic_vector(write_reg_Offset7);

  
  decode_sel_Gain8 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004E#, 14) ELSE
      '0';

  reg_enb_Gain8 <= decode_sel_Gain8 AND wr_enb;

  data_in_Gain8 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Gain8_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Gain8 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Gain8 = '1' THEN
        write_reg_Gain8 <= data_in_Gain8;
      END IF;
    END IF;
  END PROCESS reg_Gain8_process;


  write_Gain8 <= std_logic_vector(write_reg_Gain8);

  
  decode_sel_Offset8 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004F#, 14) ELSE
      '0';

  reg_enb_Offset8 <= decode_sel_Offset8 AND wr_enb;

  data_in_Offset8 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Offset8_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Offset8 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Offset8 = '1' THEN
        write_reg_Offset8 <= data_in_Offset8;
      END IF;
    END IF;
  END PROCESS reg_Offset8_process;


  write_Offset8 <= std_logic_vector(write_reg_Offset8);

  
  decode_sel_Gain9 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0050#, 14) ELSE
      '0';

  reg_enb_Gain9 <= decode_sel_Gain9 AND wr_enb;

  data_in_Gain9 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Gain9_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Gain9 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Gain9 = '1' THEN
        write_reg_Gain9 <= data_in_Gain9;
      END IF;
    END IF;
  END PROCESS reg_Gain9_process;


  write_Gain9 <= std_logic_vector(write_reg_Gain9);

  
  decode_sel_Offset9 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0051#, 14) ELSE
      '0';

  reg_enb_Offset9 <= decode_sel_Offset9 AND wr_enb;

  data_in_Offset9 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Offset9_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Offset9 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Offset9 = '1' THEN
        write_reg_Offset9 <= data_in_Offset9;
      END IF;
    END IF;
  END PROCESS reg_Offset9_process;


  write_Offset9 <= std_logic_vector(write_reg_Offset9);

  
  decode_sel_Gain10 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0052#, 14) ELSE
      '0';

  reg_enb_Gain10 <= decode_sel_Gain10 AND wr_enb;

  data_in_Gain10 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Gain10_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Gain10 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Gain10 = '1' THEN
        write_reg_Gain10 <= data_in_Gain10;
      END IF;
    END IF;
  END PROCESS reg_Gain10_process;


  write_Gain10 <= std_logic_vector(write_reg_Gain10);

  
  decode_sel_Offset10 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0053#, 14) ELSE
      '0';

  reg_enb_Offset10 <= decode_sel_Offset10 AND wr_enb;

  data_in_Offset10 <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Offset10_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        write_reg_Offset10 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Offset10 = '1' THEN
        write_reg_Offset10 <= data_in_Offset10;
      END IF;
    END IF;
  END PROCESS reg_Offset10_process;


  write_Offset10 <= std_logic_vector(write_reg_Offset10);

END rtl;

