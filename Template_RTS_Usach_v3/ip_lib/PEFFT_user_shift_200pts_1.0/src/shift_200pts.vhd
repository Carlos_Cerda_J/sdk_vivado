----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/05/2019 11:20:29 AM
-- Design Name: 
-- Module Name: shift_200pts - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity shift_200pts is
  Port 
  ( 
	clk			: in std_logic;
	reset_n		: in std_logic;
	Vo			: in std_logic_vector(24 downto 0);
	Vo_beta		: out std_logic_vector(24 downto 0)
  );
end shift_200pts;

architecture Behavioral of shift_200pts is
	signal Vo_memory_S : std_logic_vector(4999 downto 0):= (others => '0');
begin
    shift_process : process (clk, reset_n) is
    begin
        if (reset_n = '0') then
            Vo_beta <= (others => '0');
			Vo_memory_S <= (others => '0');
        elsif (rising_edge(clk)) then
			Vo_beta		<= Vo_memory_S(4999 downto 4975);
            Vo_memory_S <= Vo_memory_S(4974 downto 0) & Vo;
        end if;                                                                            
    end process;

end Behavioral;
