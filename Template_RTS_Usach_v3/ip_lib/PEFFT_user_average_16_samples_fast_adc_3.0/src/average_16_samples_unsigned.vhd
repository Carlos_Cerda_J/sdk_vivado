----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/10/2020 02:00:44 PM
-- Design Name: 
-- Module Name: average_16_samples_unsigned - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- David Arancibia G. 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity average_16_samples_unsigned is
port(
	clock:			in std_logic := '0';			--Clock signal
	reset_n:		in std_logic := '0';			--Reset signal
	
	data_in:		in std_logic_vector(11 downto 0) := (others => '0');			--Input data for the current clock cycle
	
	data_out:		out std_logic_vector(11 downto 0) := (others => '0');			--Current output data (valid for 16 clock cycles)
	new_value:		out std_logic := '0'                                 			--new valid value flag
	);
end average_16_samples_unsigned;

architecture Behavioral of average_16_samples_unsigned is

begin

	averaging_unsigned_process: process(clock, reset_n)
		variable data_accumulator:			unsigned(15 downto 0) := (others => '0');				--Stores the data for the averaging of the values
		variable counter:					integer range 0 to 15 := 0;								--Counter for the averaging
		variable round_up:					integer range 0 to 1 := 0;								--Flag if the averaged value has to be rounded up or not (1: round up; 0: do not round up)
	begin
		if rising_edge(clock) and reset_n = '1' then
			--Add the input data to the data accumulator
			data_accumulator := data_accumulator + unsigned(data_in);
			
			--Increase counter or set it back to 0
			if counter = 15 then
				--If the "first decimal place" (bit 3) = 1 => Round up
				if data_accumulator(3) = '1' then
					round_up := 1;
				end if;
				
				data_out <= std_logic_vector(data_accumulator(15 downto 4) + to_unsigned(round_up, 1));
				new_value <= '1';
				--Reset all variables for the next averaging period
				round_up := 0;
				data_accumulator := (others => '0');
				counter := 0;
			else --counter < 15
				counter := counter + 1;
				new_value <= '0';
			end if;
		end if;
	end process averaging_unsigned_process;

end Behavioral;
