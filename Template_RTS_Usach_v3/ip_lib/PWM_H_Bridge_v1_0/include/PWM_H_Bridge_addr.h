/*
 * File Name:         hdl_prj\ipcore\PWM_H_Bridge_v1_0\include\PWM_H_Bridge_addr.h
 * Description:       C Header File
 * Created:           2018-11-13 18:22:40
*/

#ifndef PWM_H_BRIDGE_H_
#define PWM_H_BRIDGE_H_

#define  IPCore_Reset_PWM_H_Bridge       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PWM_H_Bridge      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PWM_H_Bridge   0x8  //contains unique IP timestamp (yymmddHHMM): 1811131822
#define  Ref_Data_PWM_H_Bridge           0x100  //data register for Inport Ref
#define  Max_point_Data_PWM_H_Bridge     0x104  //data register for Inport Max_point
#define  En_Data_PWM_H_Bridge            0x108  //data register for Inport En

#endif /* PWM_H_BRIDGE_H_ */
