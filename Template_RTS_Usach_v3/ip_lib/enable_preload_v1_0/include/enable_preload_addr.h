/*
 * File Name:         hdl_prj\ipcore\enable_preload_v1_0\include\enable_preload_addr.h
 * Description:       C Header File
 * Created:           2019-01-25 12:06:32
*/

#ifndef ENABLE_PRELOAD_H_
#define ENABLE_PRELOAD_H_

#define  IPCore_Reset_enable_preload          0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_enable_preload         0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_enable_preload      0x8  //contains unique IP timestamp (yymmddHHMM): 1901251206
#define  enable_preload_Data_enable_preload   0x100  //data register for Inport enable_preload

#endif /* ENABLE_PRELOAD_H_ */
