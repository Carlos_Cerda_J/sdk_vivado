/*
 * File Name:         C:\vivado_6cell\SORT_NLC_6CELDAS_DESDE0\SORT_6CELL_0612\ipcore\sort_3c_v4_0\include\sort_3c_addr.h
 * Description:       C Header File
 * Created:           2019-12-06 03:54:01
*/

#ifndef SORT_3C_H_
#define SORT_3C_H_

#define  IPCore_Reset_sort_3c       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_sort_3c      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_sort_3c   0x8  //contains unique IP timestamp (yymmddHHMM): 1912060353

#endif /* SORT_3C_H_ */
