-- -------------------------------------------------------------
-- 
-- File Name: C:\vivado_6cell\SORT_NLC_6CELDAS_DESDE0\SORT_6CELL_0612\hdlsrc\nlc_6celdas_desde0_0612\sort_3c_src_sort_3c_pkg.vhd
-- Created: 2019-12-06 03:53:46
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

PACKAGE sort_3c_src_sort_3c_pkg IS
  TYPE vector_of_signed5 IS ARRAY (NATURAL RANGE <>) OF signed(4 DOWNTO 0);
  TYPE vector_of_signed25 IS ARRAY (NATURAL RANGE <>) OF signed(24 DOWNTO 0);
END sort_3c_src_sort_3c_pkg;

