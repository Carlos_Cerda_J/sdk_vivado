-- -------------------------------------------------------------
-- 
-- File Name: C:\vivado_6cell\SORT_NLC_6CELDAS_DESDE0\SORT_6CELL_0612\hdlsrc\nlc_6celdas_desde0_0612\sort_3c_src_sort_3c.vhd
-- Created: 2019-12-06 03:53:46
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- 
-- -------------------------------------------------------------
-- Rate and Clocking Details
-- -------------------------------------------------------------
-- Model base rate: 5e-09
-- Target subsystem base rate: 1e-08
-- 
-- 
-- Clock Enable  Sample Time
-- -------------------------------------------------------------
-- ce_out        1e-08
-- -------------------------------------------------------------
-- 
-- 
-- Output Signal                 Clock Enable  Sample Time
-- -------------------------------------------------------------
-- pap1                          ce_out        1e-08
-- pap2                          ce_out        1e-08
-- pap3                          ce_out        1e-08
-- pap4                          ce_out        1e-08
-- pap5                          ce_out        1e-08
-- pap6                          ce_out        1e-08
-- pbp1                          ce_out        1e-08
-- pbp2                          ce_out        1e-08
-- pbp3                          ce_out        1e-08
-- pbp4                          ce_out        1e-08
-- pbp5                          ce_out        1e-08
-- pbp6                          ce_out        1e-08
-- pcp1                          ce_out        1e-08
-- pcp2                          ce_out        1e-08
-- pcp3                          ce_out        1e-08
-- pcp4                          ce_out        1e-08
-- pcp5                          ce_out        1e-08
-- pcp6                          ce_out        1e-08
-- pan1                          ce_out        1e-08
-- pan2                          ce_out        1e-08
-- pan3                          ce_out        1e-08
-- pan4                          ce_out        1e-08
-- pan5                          ce_out        1e-08
-- pan6                          ce_out        1e-08
-- pbn1                          ce_out        1e-08
-- pbn2                          ce_out        1e-08
-- pbn3                          ce_out        1e-08
-- pbn4                          ce_out        1e-08
-- pbn5                          ce_out        1e-08
-- pbn6                          ce_out        1e-08
-- pcn1                          ce_out        1e-08
-- pcn2                          ce_out        1e-08
-- pcn3                          ce_out        1e-08
-- pcn4                          ce_out        1e-08
-- pcn5                          ce_out        1e-08
-- pcn6                          ce_out        1e-08
-- -------------------------------------------------------------
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: sort_3c_src_sort_3c
-- Source Path: nlc_6celdas_desde0_0612/sort_3c
-- Hierarchy Level: 0
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY sort_3c_src_sort_3c IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        clk_enable                        :   IN    std_logic;
        VcaP1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        VcaP2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        VcaP3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        VcaP4                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        VcaP5                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        VcaP6                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IaP                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vcbp1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbp2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbp3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbp4                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbp5                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbp6                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IbP                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vccp1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccp2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccp3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccp4                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccp5                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccp6                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IcP                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vcan1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcan2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcan3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcan4                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcan5                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcan6                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IaN                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vcbn1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbn2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbn3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbn4                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbn5                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbn6                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IbN                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vccn1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccn2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccn3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccn4                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccn5                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccn6                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IcN                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        sync                              :   IN    std_logic;  -- ufix1
        ce_out                            :   OUT   std_logic;
        pap1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pap2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pap3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pap4                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pap5                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pap6                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbp1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbp2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbp3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbp4                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbp5                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbp6                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcp1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcp2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcp3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcp4                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcp5                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcp6                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pan1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pan2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pan3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pan4                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pan5                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pan6                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbn1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbn2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbn3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbn4                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbn5                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbn6                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcn1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcn2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcn3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcn4                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcn5                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcn6                              :   OUT   std_logic_vector(4 DOWNTO 0)  -- sfix5
        );
END sort_3c_src_sort_3c;


ARCHITECTURE rtl OF sort_3c_src_sort_3c IS

  -- Component Declarations
  COMPONENT sort_3c_src_MLfcn1
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          v1                              :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          v2                              :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          v3                              :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          v4                              :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          v5                              :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          v6                              :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          icluster                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
          enable                          :   IN    std_logic;  -- ufix1
          pos1                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pos2                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pos3                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pos4                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pos5                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pos6                            :   OUT   std_logic_vector(4 DOWNTO 0)  -- sfix5
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : sort_3c_src_MLfcn1
    USE ENTITY work.sort_3c_src_MLfcn1(rtl);

  -- Signals
  SIGNAL pos1                             : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pos2                             : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pos3                             : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pos4                             : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pos5                             : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pos6                             : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn2_out1                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn2_out2                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn2_out3                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn2_out4                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn2_out5                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn2_out6                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn3_out1                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn3_out2                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn3_out3                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn3_out4                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn3_out5                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn3_out6                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn4_out1                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn4_out2                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn4_out3                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn4_out4                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn4_out5                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn4_out6                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn5_out1                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn5_out2                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn5_out3                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn5_out4                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn5_out5                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn5_out6                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn6_out1                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn6_out2                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn6_out3                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn6_out4                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn6_out5                      : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MLfcn6_out6                      : std_logic_vector(4 DOWNTO 0);  -- ufix5

BEGIN
  u_MLfcn1 : sort_3c_src_MLfcn1
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              v1 => VcaP1,  -- sfix25_En14
              v2 => VcaP2,  -- sfix25_En14
              v3 => VcaP3,  -- sfix25_En14
              v4 => VcaP4,  -- sfix25_En14
              v5 => VcaP5,  -- sfix25_En14
              v6 => VcaP6,  -- sfix25_En14
              icluster => IaP,  -- sfix25_En16
              enable => sync,  -- ufix1
              pos1 => pos1,  -- sfix5
              pos2 => pos2,  -- sfix5
              pos3 => pos3,  -- sfix5
              pos4 => pos4,  -- sfix5
              pos5 => pos5,  -- sfix5
              pos6 => pos6  -- sfix5
              );

  u_MLfcn2 : sort_3c_src_MLfcn1
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              v1 => Vcbp1,  -- sfix25_En14
              v2 => Vcbp2,  -- sfix25_En14
              v3 => Vcbp3,  -- sfix25_En14
              v4 => Vcbp4,  -- sfix25_En14
              v5 => Vcbp5,  -- sfix25_En14
              v6 => Vcbp6,  -- sfix25_En14
              icluster => IbP,  -- sfix25_En16
              enable => sync,  -- ufix1
              pos1 => MLfcn2_out1,  -- sfix5
              pos2 => MLfcn2_out2,  -- sfix5
              pos3 => MLfcn2_out3,  -- sfix5
              pos4 => MLfcn2_out4,  -- sfix5
              pos5 => MLfcn2_out5,  -- sfix5
              pos6 => MLfcn2_out6  -- sfix5
              );

  u_MLfcn3 : sort_3c_src_MLfcn1
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              v1 => Vccp1,  -- sfix25_En14
              v2 => Vccp2,  -- sfix25_En14
              v3 => Vccp3,  -- sfix25_En14
              v4 => Vccp4,  -- sfix25_En14
              v5 => Vccp5,  -- sfix25_En14
              v6 => Vccp6,  -- sfix25_En14
              icluster => IcP,  -- sfix25_En16
              enable => sync,  -- ufix1
              pos1 => MLfcn3_out1,  -- sfix5
              pos2 => MLfcn3_out2,  -- sfix5
              pos3 => MLfcn3_out3,  -- sfix5
              pos4 => MLfcn3_out4,  -- sfix5
              pos5 => MLfcn3_out5,  -- sfix5
              pos6 => MLfcn3_out6  -- sfix5
              );

  u_MLfcn4 : sort_3c_src_MLfcn1
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              v1 => Vcan1,  -- sfix25_En14
              v2 => Vcan2,  -- sfix25_En14
              v3 => Vcan3,  -- sfix25_En14
              v4 => Vcan4,  -- sfix25_En14
              v5 => Vcan5,  -- sfix25_En14
              v6 => Vcan6,  -- sfix25_En14
              icluster => IaN,  -- sfix25_En16
              enable => sync,  -- ufix1
              pos1 => MLfcn4_out1,  -- sfix5
              pos2 => MLfcn4_out2,  -- sfix5
              pos3 => MLfcn4_out3,  -- sfix5
              pos4 => MLfcn4_out4,  -- sfix5
              pos5 => MLfcn4_out5,  -- sfix5
              pos6 => MLfcn4_out6  -- sfix5
              );

  u_MLfcn5 : sort_3c_src_MLfcn1
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              v1 => Vcbn1,  -- sfix25_En14
              v2 => Vcbn2,  -- sfix25_En14
              v3 => Vcbn3,  -- sfix25_En14
              v4 => Vcbn4,  -- sfix25_En14
              v5 => Vcbn5,  -- sfix25_En14
              v6 => Vcbn6,  -- sfix25_En14
              icluster => IbN,  -- sfix25_En16
              enable => sync,  -- ufix1
              pos1 => MLfcn5_out1,  -- sfix5
              pos2 => MLfcn5_out2,  -- sfix5
              pos3 => MLfcn5_out3,  -- sfix5
              pos4 => MLfcn5_out4,  -- sfix5
              pos5 => MLfcn5_out5,  -- sfix5
              pos6 => MLfcn5_out6  -- sfix5
              );

  u_MLfcn6 : sort_3c_src_MLfcn1
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              v1 => Vccn1,  -- sfix25_En14
              v2 => Vccn2,  -- sfix25_En14
              v3 => Vccn3,  -- sfix25_En14
              v4 => Vccn4,  -- sfix25_En14
              v5 => Vccn5,  -- sfix25_En14
              v6 => Vccn6,  -- sfix25_En14
              icluster => IcN,  -- sfix25_En16
              enable => sync,  -- ufix1
              pos1 => MLfcn6_out1,  -- sfix5
              pos2 => MLfcn6_out2,  -- sfix5
              pos3 => MLfcn6_out3,  -- sfix5
              pos4 => MLfcn6_out4,  -- sfix5
              pos5 => MLfcn6_out5,  -- sfix5
              pos6 => MLfcn6_out6  -- sfix5
              );

  ce_out <= clk_enable;

  pap1 <= pos1;

  pap2 <= pos2;

  pap3 <= pos3;

  pap4 <= pos4;

  pap5 <= pos5;

  pap6 <= pos6;

  pbp1 <= MLfcn2_out1;

  pbp2 <= MLfcn2_out2;

  pbp3 <= MLfcn2_out3;

  pbp4 <= MLfcn2_out4;

  pbp5 <= MLfcn2_out5;

  pbp6 <= MLfcn2_out6;

  pcp1 <= MLfcn3_out1;

  pcp2 <= MLfcn3_out2;

  pcp3 <= MLfcn3_out3;

  pcp4 <= MLfcn3_out4;

  pcp5 <= MLfcn3_out5;

  pcp6 <= MLfcn3_out6;

  pan1 <= MLfcn4_out1;

  pan2 <= MLfcn4_out2;

  pan3 <= MLfcn4_out3;

  pan4 <= MLfcn4_out4;

  pan5 <= MLfcn4_out5;

  pan6 <= MLfcn4_out6;

  pbn1 <= MLfcn5_out1;

  pbn2 <= MLfcn5_out2;

  pbn3 <= MLfcn5_out3;

  pbn4 <= MLfcn5_out4;

  pbn5 <= MLfcn5_out5;

  pbn6 <= MLfcn5_out6;

  pcn1 <= MLfcn6_out1;

  pcn2 <= MLfcn6_out2;

  pcn3 <= MLfcn6_out3;

  pcn4 <= MLfcn6_out4;

  pcn5 <= MLfcn6_out5;

  pcn6 <= MLfcn6_out6;

END rtl;

