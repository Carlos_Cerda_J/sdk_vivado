----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/31/2019 10:30:41 AM
-- Design Name: 
-- Module Name: trip_cleaner - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity trip_cleaner is
    Port 
	( 
		clock : in STD_LOGIC;
        reset_n : in STD_LOGIC;
		max_count : in STD_LOGIC_VECTOR (15 downto 0);
        trip_in : in STD_LOGIC;
        trip_out : out STD_LOGIC
	);
end trip_cleaner;

architecture Behavioral of trip_cleaner is
	SIGNAL counter_S			: INTEGER RANGE 0 TO 16383 := 0;
	SIGNAL trip_verified_S		: STD_LOGIC := '0';
	SIGNAL max_count_integer	: INTEGER RANGE 0 TO 16383 := 0;

begin

	PROCESS(clock, reset_n)
	BEGIN
		IF (reset_n = '0') THEN
			counter_S <= 0;
		ELSIF rising_edge(clock) THEN
			IF (trip_in = '1') THEN
				counter_S <= counter_S + 1;
			ELSE
				counter_S <= 0;
			END IF;
		END IF;
	END PROCESS;
	

	PROCESS(clock, reset_n)
	BEGIN
		IF (reset_n = '0') THEN
			trip_verified_S <= '0';
			
		ELSIF rising_edge(clock) THEN
			IF (counter_S >= max_count_integer) THEN
				trip_verified_S <= '1';
			ELSE
				trip_verified_S <= '0';
			END IF;
		END IF;
	END PROCESS;	
	
	trip_out <= trip_verified_S;
	max_count_integer <= to_integer(unsigned(max_count));
	
end Behavioral;
