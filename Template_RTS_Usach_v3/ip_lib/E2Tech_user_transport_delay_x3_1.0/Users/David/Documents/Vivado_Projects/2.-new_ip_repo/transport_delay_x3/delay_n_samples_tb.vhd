library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity delay_n_samples_tb is
end;

architecture bench of delay_n_samples_tb is

  component delay_n_samples
    Generic
    (
  	WORD_WIDTH		: integer := 4;
	MAX_DELAY		: integer := 5
    );
      Port ( clk : in STD_LOGIC;
             reset_n : in STD_LOGIC;
             delay : in STD_LOGIC_VECTOR (6 downto 0);
             data1_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
  		   data2_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
  		   data3_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
             data1_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
  		   data2_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
  		   data3_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0));
  end component;
	
  constant WORD_WIDTH : integer := 4;
  signal clk: STD_LOGIC;
  signal reset_n: STD_LOGIC;
  signal delay: STD_LOGIC_VECTOR (6 downto 0);
  signal data1_in: STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
  signal data2_in: STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
  signal data3_in: STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
  signal data1_out: STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
  signal data2_out: STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
  signal data3_out: STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);

  constant clock_period: time := 500 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: delay_n_samples generic map ( WORD_WIDTH =>  4,
									MAX_DELAY => 5)
                          port map ( clk        => clk,
                                     reset_n    => reset_n,
                                     delay      => delay,
                                     data1_in   => data1_in,
                                     data2_in   => data2_in,
                                     data3_in   => data3_in,
                                     data1_out  => data1_out,
                                     data2_out  => data2_out,
                                     data3_out  => data3_out );

  stimulus: process
  begin
  
    -- Put initialisation code here
	reset_n <= '0';
	wait for 600ns;
	reset_n <= '1';
	
    -- Put test bench stimulus code here

    stop_the_clock <= false;
    wait;
  end process;

  change_delay: process
  begin
	delay <= "0000000";
	wait for clock_period*10;
    pulse_gen: loop
		delay <= STD_LOGIC_VECTOR((unsigned(delay) + 1));
		wait for clock_period*10;				
    end loop;
    wait;
  end process;	

  change_input: process
  begin
	wait for clock_period/4;
	data1_in <= (others => '0');
	wait for clock_period/2;
    pulse_gen: loop
		data1_in <= STD_LOGIC_VECTOR((unsigned(data1_in) + 1));
		wait for clock_period/2;				
    end loop;
    wait;
  end process;	
  
  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;