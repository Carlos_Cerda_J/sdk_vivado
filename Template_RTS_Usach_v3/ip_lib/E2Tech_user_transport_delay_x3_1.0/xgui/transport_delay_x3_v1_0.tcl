# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Transport_delay_settings [ipgui::add_page $IPINST -name "Transport delay settings"]
  set WORD_WIDTH [ipgui::add_param $IPINST -name "WORD_WIDTH" -parent ${Transport_delay_settings}]
  set_property tooltip {Input and output Word Width in bits} ${WORD_WIDTH}
  set MAX_DELAY [ipgui::add_param $IPINST -name "MAX_DELAY" -parent ${Transport_delay_settings}]
  set_property tooltip {Maximum allowable samples delay} ${MAX_DELAY}

  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0" -display_name {AXI Lite Settings}]
  set C_S_AXI_Lite_DATA_WIDTH [ipgui::add_param $IPINST -name "C_S_AXI_Lite_DATA_WIDTH" -parent ${Page_0} -widget comboBox]
  set_property tooltip {Width of S_AXI data bus} ${C_S_AXI_Lite_DATA_WIDTH}
  set C_S_AXI_Lite_ADDR_WIDTH [ipgui::add_param $IPINST -name "C_S_AXI_Lite_ADDR_WIDTH" -parent ${Page_0}]
  set_property tooltip {Width of S_AXI address bus} ${C_S_AXI_Lite_ADDR_WIDTH}
  ipgui::add_param $IPINST -name "C_S_AXI_Lite_BASEADDR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S_AXI_Lite_HIGHADDR" -parent ${Page_0}


}

proc update_PARAM_VALUE.MAX_DELAY { PARAM_VALUE.MAX_DELAY } {
	# Procedure called to update MAX_DELAY when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAX_DELAY { PARAM_VALUE.MAX_DELAY } {
	# Procedure called to validate MAX_DELAY
	return true
}

proc update_PARAM_VALUE.WORD_WIDTH { PARAM_VALUE.WORD_WIDTH } {
	# Procedure called to update WORD_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.WORD_WIDTH { PARAM_VALUE.WORD_WIDTH } {
	# Procedure called to validate WORD_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S_AXI_Lite_DATA_WIDTH { PARAM_VALUE.C_S_AXI_Lite_DATA_WIDTH } {
	# Procedure called to update C_S_AXI_Lite_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_AXI_Lite_DATA_WIDTH { PARAM_VALUE.C_S_AXI_Lite_DATA_WIDTH } {
	# Procedure called to validate C_S_AXI_Lite_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S_AXI_Lite_ADDR_WIDTH { PARAM_VALUE.C_S_AXI_Lite_ADDR_WIDTH } {
	# Procedure called to update C_S_AXI_Lite_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_AXI_Lite_ADDR_WIDTH { PARAM_VALUE.C_S_AXI_Lite_ADDR_WIDTH } {
	# Procedure called to validate C_S_AXI_Lite_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S_AXI_Lite_BASEADDR { PARAM_VALUE.C_S_AXI_Lite_BASEADDR } {
	# Procedure called to update C_S_AXI_Lite_BASEADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_AXI_Lite_BASEADDR { PARAM_VALUE.C_S_AXI_Lite_BASEADDR } {
	# Procedure called to validate C_S_AXI_Lite_BASEADDR
	return true
}

proc update_PARAM_VALUE.C_S_AXI_Lite_HIGHADDR { PARAM_VALUE.C_S_AXI_Lite_HIGHADDR } {
	# Procedure called to update C_S_AXI_Lite_HIGHADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S_AXI_Lite_HIGHADDR { PARAM_VALUE.C_S_AXI_Lite_HIGHADDR } {
	# Procedure called to validate C_S_AXI_Lite_HIGHADDR
	return true
}


proc update_MODELPARAM_VALUE.C_S_AXI_Lite_DATA_WIDTH { MODELPARAM_VALUE.C_S_AXI_Lite_DATA_WIDTH PARAM_VALUE.C_S_AXI_Lite_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S_AXI_Lite_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S_AXI_Lite_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S_AXI_Lite_ADDR_WIDTH { MODELPARAM_VALUE.C_S_AXI_Lite_ADDR_WIDTH PARAM_VALUE.C_S_AXI_Lite_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S_AXI_Lite_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S_AXI_Lite_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.WORD_WIDTH { MODELPARAM_VALUE.WORD_WIDTH PARAM_VALUE.WORD_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.WORD_WIDTH}] ${MODELPARAM_VALUE.WORD_WIDTH}
}

proc update_MODELPARAM_VALUE.MAX_DELAY { MODELPARAM_VALUE.MAX_DELAY PARAM_VALUE.MAX_DELAY } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAX_DELAY}] ${MODELPARAM_VALUE.MAX_DELAY}
}

