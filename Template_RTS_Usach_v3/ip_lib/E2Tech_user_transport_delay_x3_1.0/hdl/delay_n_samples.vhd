----------------------------------------------------------------------------------
-- Company: E2Tech
-- Engineer: David Arancibia G.
-- 
-- Create Date: 08/29/2019 06:53:25 PM
-- Design Name: 
-- Module Name: delay_n_samples - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity delay_n_samples is
  Generic
  (
	WORD_WIDTH		: integer := 25;
	MAX_DELAY		: integer := 100
  );
    Port ( clk : in STD_LOGIC;
           reset_n : in STD_LOGIC;
           delay : in STD_LOGIC_VECTOR (6 downto 0);		--max delay = 127
           data1_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data2_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data3_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
           data1_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data2_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data3_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0));
end delay_n_samples;

architecture Behavioral of delay_n_samples is
	constant MAX_BITS  : integer := WORD_WIDTH * (MAX_DELAY + 1);
	shared variable memory1_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory2_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory3_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	signal delay_int : integer range 0 to 127	:= 0;
	shared variable position : integer range 0 to MAX_BITS-1;
begin
	delay_int <= to_integer(unsigned(delay));
	
	position_process : process(clk, reset_n) is
	begin
		if (rising_edge(clk)) then
			if (delay_int <= MAX_DELAY) then
				position := delay_int * WORD_WIDTH;
			else
				position := MAX_DELAY * WORD_WIDTH;
			end if;
		end if;
	end process;
	
	memory_process : process (clk, reset_n) is
    begin
        if (reset_n = '0') then
			memory1_S := (others => '0');
			memory2_S := (others => '0');
			memory3_S := (others => '0');
        elsif (rising_edge(clk)) then
            memory1_S := memory1_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data1_in;
			memory2_S := memory2_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data2_in;
			memory3_S := memory3_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data3_in;
        end if;                                                                            
    end process;
	
	shift_process : process (clk, reset_n) is
    begin
        if (reset_n = '0') then
            data1_out <= (others => '0');
			data2_out <= (others => '0');
			data3_out <= (others => '0');
        elsif (rising_edge(clk)) then
			data1_out <= memory1_S((position+WORD_WIDTH-1) downto (position));
			data2_out <= memory2_S((position+WORD_WIDTH-1) downto (position));
			data3_out <= memory3_S((position+WORD_WIDTH-1) downto (position));
        end if;                                                                            
    end process;

end Behavioral;
