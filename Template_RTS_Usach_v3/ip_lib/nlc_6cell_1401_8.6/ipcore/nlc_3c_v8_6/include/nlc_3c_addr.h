/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\sort_nlc_0912\SORT_NLC_6CELDAS_OPT1\nlc_6cell_1401_8.6\ipcore\nlc_3c_v8_6\include\nlc_3c_addr.h
 * Description:       C Header File
 * Created:           2020-01-14 12:33:16
*/

#ifndef NLC_3C_H_
#define NLC_3C_H_

#define  IPCore_Reset_nlc_3c       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_nlc_3c      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_nlc_3c   0x8  //contains unique IP timestamp (yymmddHHMM): 2001141233
#define  Vc_ref_Data_nlc_3c        0x100  //data register for Inport Vc_ref

#endif /* NLC_3C_H_ */
