-- -------------------------------------------------------------
-- 
-- File Name: C:\Users\Victor\Documents\matlab_ipcores\Simulink\sort_nlc_0912\SORT_NLC_6CELDAS_OPT1\NLC_6cell_0912_8.4\hdlsrc\nlc_sort_6celdas_opt1_0912\nlc_3c_src_NLC_3C_pkg.vhd
-- Created: 2019-12-09 15:19:23
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

PACKAGE nlc_3c_src_NLC_3C_pkg IS
  TYPE vector_of_signed5 IS ARRAY (NATURAL RANGE <>) OF signed(4 DOWNTO 0);
  TYPE vector_of_signed3 IS ARRAY (NATURAL RANGE <>) OF signed(2 DOWNTO 0);
  TYPE vector_of_unsigned3 IS ARRAY (NATURAL RANGE <>) OF unsigned(2 DOWNTO 0);
  TYPE vector_of_signed32 IS ARRAY (NATURAL RANGE <>) OF signed(31 DOWNTO 0);
  TYPE vector_of_signed6 IS ARRAY (NATURAL RANGE <>) OF signed(5 DOWNTO 0);
END nlc_3c_src_NLC_3C_pkg;

