/*
 * File Name:         C:\IPCORES\FF_TO_FN\ipcore\LL_to_LN_ip_v1_0\include\LL_to_LN_ip_addr.h
 * Description:       C Header File
 * Created:           2019-01-15 15:44:38
*/

#ifndef LL_TO_LN_IP_H_
#define LL_TO_LN_IP_H_

#define  IPCore_Reset_LL_to_LN_ip       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_LL_to_LN_ip      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_LL_to_LN_ip   0x8  //contains unique IP timestamp (yymmddHHMM): 1901151544

#endif /* LL_TO_LN_IP_H_ */
