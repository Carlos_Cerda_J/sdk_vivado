-- -------------------------------------------------------------
-- 
-- File Name: C:\IPCORES\FF_TO_FN\hdlsrc\FF_FN\LL_to_LN_ip_dut.vhd
-- Created: 2019-01-15 15:44:38
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: LL_to_LN_ip_dut
-- Source Path: LL_to_LN_ip/LL_to_LN_ip_dut
-- Hierarchy Level: 1
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY LL_to_LN_ip_dut IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        dut_enable                        :   IN    std_logic;  -- ufix1
        vab                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        vbc                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        ce_out                            :   OUT   std_logic;  -- ufix1
        Va                                :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vb                                :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vc                                :   OUT   std_logic_vector(24 DOWNTO 0)  -- sfix25_En14
        );
END LL_to_LN_ip_dut;


ARCHITECTURE rtl OF LL_to_LN_ip_dut IS

  -- Component Declarations
  COMPONENT LL_to_LN_ip_src_LL_to_LN
    PORT( clk                             :   IN    std_logic;
          clk_enable                      :   IN    std_logic;
          reset                           :   IN    std_logic;
          vab                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          vbc                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          ce_out                          :   OUT   std_logic;  -- ufix1
          Va                              :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          Vb                              :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          Vc                              :   OUT   std_logic_vector(24 DOWNTO 0)  -- sfix25_En14
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : LL_to_LN_ip_src_LL_to_LN
    USE ENTITY work.LL_to_LN_ip_src_LL_to_LN(rtl);

  -- Signals
  SIGNAL enb                              : std_logic;
  SIGNAL ce_out_sig                       : std_logic;  -- ufix1
  SIGNAL Va_sig                           : std_logic_vector(24 DOWNTO 0);  -- ufix25
  SIGNAL Vb_sig                           : std_logic_vector(24 DOWNTO 0);  -- ufix25
  SIGNAL Vc_sig                           : std_logic_vector(24 DOWNTO 0);  -- ufix25

BEGIN
  u_LL_to_LN_ip_src_LL_to_LN : LL_to_LN_ip_src_LL_to_LN
    PORT MAP( clk => clk,
              clk_enable => enb,
              reset => reset,
              vab => vab,  -- sfix25_En14
              vbc => vbc,  -- sfix25_En14
              ce_out => ce_out_sig,  -- ufix1
              Va => Va_sig,  -- sfix25_En14
              Vb => Vb_sig,  -- sfix25_En14
              Vc => Vc_sig  -- sfix25_En14
              );

  enb <= dut_enable;

  ce_out <= ce_out_sig;

  Va <= Va_sig;

  Vb <= Vb_sig;

  Vc <= Vc_sig;

END rtl;

