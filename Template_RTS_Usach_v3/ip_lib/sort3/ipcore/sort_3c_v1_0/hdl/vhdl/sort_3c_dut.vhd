-- -------------------------------------------------------------
-- 
-- File Name: F:\sort_3_cell\sort3\hdlsrc\sorting_3cell_sm_trigg_cond\sort_3c_dut.vhd
-- Created: 2019-05-17 11:12:48
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: sort_3c_dut
-- Source Path: sort_3c/sort_3c_dut
-- Hierarchy Level: 1
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY sort_3c_dut IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        dut_enable                        :   IN    std_logic;  -- ufix1
        VcaP1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        VcaP2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        VcaP3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IaP                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vcbp1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbp2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbp3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IbP                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vccp1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccp2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccp3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IcP                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vcan1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcan2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcan3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IaN                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vcbn1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbn2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbn3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IbN                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vccn1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccn2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccn3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IcN                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        sync                              :   IN    std_logic;  -- ufix1
        ce_out                            :   OUT   std_logic;  -- ufix1
        pap1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pap2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pap3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbp1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbp2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbp3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcp1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcp2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcp3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pan1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pan2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pan3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbn1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbn2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbn3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcn1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcn2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcn3                              :   OUT   std_logic_vector(4 DOWNTO 0)  -- sfix5
        );
END sort_3c_dut;


ARCHITECTURE rtl OF sort_3c_dut IS

  -- Component Declarations
  COMPONENT sort_3c_src_Sorting3C
    PORT( clk                             :   IN    std_logic;
          clk_enable                      :   IN    std_logic;
          reset                           :   IN    std_logic;
          VcaP1                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          VcaP2                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          VcaP3                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          IaP                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
          Vcbp1                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          Vcbp2                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          Vcbp3                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          IbP                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
          Vccp1                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          Vccp2                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          Vccp3                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          IcP                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
          Vcan1                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          Vcan2                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          Vcan3                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          IaN                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
          Vcbn1                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          Vcbn2                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          Vcbn3                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          IbN                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
          Vccn1                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          Vccn2                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          Vccn3                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          IcN                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
          sync                            :   IN    std_logic;  -- ufix1
          ce_out                          :   OUT   std_logic;  -- ufix1
          pap1                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pap2                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pap3                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pbp1                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pbp2                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pbp3                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pcp1                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pcp2                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pcp3                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pan1                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pan2                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pan3                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pbn1                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pbn2                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pbn3                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pcn1                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pcn2                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pcn3                            :   OUT   std_logic_vector(4 DOWNTO 0)  -- sfix5
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : sort_3c_src_Sorting3C
    USE ENTITY work.sort_3c_src_Sorting3C(rtl);

  -- Signals
  SIGNAL enb                              : std_logic;
  SIGNAL sync_sig                         : std_logic;  -- ufix1
  SIGNAL ce_out_sig                       : std_logic;  -- ufix1
  SIGNAL pap1_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pap2_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pap3_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pbp1_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pbp2_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pbp3_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pcp1_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pcp2_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pcp3_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pan1_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pan2_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pan3_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pbn1_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pbn2_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pbn3_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pcn1_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pcn2_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pcn3_sig                         : std_logic_vector(4 DOWNTO 0);  -- ufix5

BEGIN
  u_sort_3c_src_Sorting3C : sort_3c_src_Sorting3C
    PORT MAP( clk => clk,
              clk_enable => enb,
              reset => reset,
              VcaP1 => VcaP1,  -- sfix25_En14
              VcaP2 => VcaP2,  -- sfix25_En14
              VcaP3 => VcaP3,  -- sfix25_En14
              IaP => IaP,  -- sfix25_En16
              Vcbp1 => Vcbp1,  -- sfix25_En14
              Vcbp2 => Vcbp2,  -- sfix25_En14
              Vcbp3 => Vcbp3,  -- sfix25_En14
              IbP => IbP,  -- sfix25_En16
              Vccp1 => Vccp1,  -- sfix25_En14
              Vccp2 => Vccp2,  -- sfix25_En14
              Vccp3 => Vccp3,  -- sfix25_En14
              IcP => IcP,  -- sfix25_En16
              Vcan1 => Vcan1,  -- sfix25_En14
              Vcan2 => Vcan2,  -- sfix25_En14
              Vcan3 => Vcan3,  -- sfix25_En14
              IaN => IaN,  -- sfix25_En16
              Vcbn1 => Vcbn1,  -- sfix25_En14
              Vcbn2 => Vcbn2,  -- sfix25_En14
              Vcbn3 => Vcbn3,  -- sfix25_En14
              IbN => IbN,  -- sfix25_En16
              Vccn1 => Vccn1,  -- sfix25_En14
              Vccn2 => Vccn2,  -- sfix25_En14
              Vccn3 => Vccn3,  -- sfix25_En14
              IcN => IcN,  -- sfix25_En16
              sync => sync_sig,  -- ufix1
              ce_out => ce_out_sig,  -- ufix1
              pap1 => pap1_sig,  -- sfix5
              pap2 => pap2_sig,  -- sfix5
              pap3 => pap3_sig,  -- sfix5
              pbp1 => pbp1_sig,  -- sfix5
              pbp2 => pbp2_sig,  -- sfix5
              pbp3 => pbp3_sig,  -- sfix5
              pcp1 => pcp1_sig,  -- sfix5
              pcp2 => pcp2_sig,  -- sfix5
              pcp3 => pcp3_sig,  -- sfix5
              pan1 => pan1_sig,  -- sfix5
              pan2 => pan2_sig,  -- sfix5
              pan3 => pan3_sig,  -- sfix5
              pbn1 => pbn1_sig,  -- sfix5
              pbn2 => pbn2_sig,  -- sfix5
              pbn3 => pbn3_sig,  -- sfix5
              pcn1 => pcn1_sig,  -- sfix5
              pcn2 => pcn2_sig,  -- sfix5
              pcn3 => pcn3_sig  -- sfix5
              );

  sync_sig <= sync;

  enb <= dut_enable;

  ce_out <= ce_out_sig;

  pap1 <= pap1_sig;

  pap2 <= pap2_sig;

  pap3 <= pap3_sig;

  pbp1 <= pbp1_sig;

  pbp2 <= pbp2_sig;

  pbp3 <= pbp3_sig;

  pcp1 <= pcp1_sig;

  pcp2 <= pcp2_sig;

  pcp3 <= pcp3_sig;

  pan1 <= pan1_sig;

  pan2 <= pan2_sig;

  pan3 <= pan3_sig;

  pbn1 <= pbn1_sig;

  pbn2 <= pbn2_sig;

  pbn3 <= pbn3_sig;

  pcn1 <= pcn1_sig;

  pcn2 <= pcn2_sig;

  pcn3 <= pcn3_sig;

END rtl;

