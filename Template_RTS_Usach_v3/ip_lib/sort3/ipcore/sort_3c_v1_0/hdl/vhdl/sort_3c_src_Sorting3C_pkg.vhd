-- -------------------------------------------------------------
-- 
-- File Name: F:\sort_3_cell\sort3\hdlsrc\sorting_3cell_sm_trigg_cond\sort_3c_src_Sorting3C_pkg.vhd
-- Created: 2019-05-17 11:12:40
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

PACKAGE sort_3c_src_Sorting3C_pkg IS
  TYPE vector_of_signed5 IS ARRAY (NATURAL RANGE <>) OF signed(4 DOWNTO 0);
  TYPE vector_of_signed25 IS ARRAY (NATURAL RANGE <>) OF signed(24 DOWNTO 0);
END sort_3c_src_Sorting3C_pkg;

