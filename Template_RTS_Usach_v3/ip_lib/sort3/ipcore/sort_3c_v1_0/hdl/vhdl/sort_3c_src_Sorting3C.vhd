-- -------------------------------------------------------------
-- 
-- File Name: F:\sort_3_cell\sort3\hdlsrc\sorting_3cell_sm_trigg_cond\sort_3c_src_Sorting3C.vhd
-- Created: 2019-05-17 11:12:40
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- 
-- -------------------------------------------------------------
-- Rate and Clocking Details
-- -------------------------------------------------------------
-- Model base rate: 5e-09
-- Target subsystem base rate: 1e-08
-- 
-- 
-- Clock Enable  Sample Time
-- -------------------------------------------------------------
-- ce_out        1e-08
-- -------------------------------------------------------------
-- 
-- 
-- Output Signal                 Clock Enable  Sample Time
-- -------------------------------------------------------------
-- pap1                          ce_out        1e-08
-- pap2                          ce_out        1e-08
-- pap3                          ce_out        1e-08
-- pbp1                          ce_out        1e-08
-- pbp2                          ce_out        1e-08
-- pbp3                          ce_out        1e-08
-- pcp1                          ce_out        1e-08
-- pcp2                          ce_out        1e-08
-- pcp3                          ce_out        1e-08
-- pan1                          ce_out        1e-08
-- pan2                          ce_out        1e-08
-- pan3                          ce_out        1e-08
-- pbn1                          ce_out        1e-08
-- pbn2                          ce_out        1e-08
-- pbn3                          ce_out        1e-08
-- pcn1                          ce_out        1e-08
-- pcn2                          ce_out        1e-08
-- pcn3                          ce_out        1e-08
-- -------------------------------------------------------------
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: sort_3c_src_Sorting3C
-- Source Path: sorting_3cell_sm_trigg_cond/Sorting3C
-- Hierarchy Level: 0
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY sort_3c_src_Sorting3C IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        clk_enable                        :   IN    std_logic;
        VcaP1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        VcaP2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        VcaP3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IaP                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vcbp1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbp2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbp3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IbP                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vccp1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccp2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccp3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IcP                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vcan1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcan2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcan3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IaN                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vcbn1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbn2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vcbn3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IbN                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        Vccn1                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccn2                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        Vccn3                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        IcN                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        sync                              :   IN    std_logic;  -- ufix1
        ce_out                            :   OUT   std_logic;
        pap1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pap2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pap3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbp1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbp2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbp3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcp1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcp2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcp3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pan1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pan2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pan3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbn1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbn2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pbn3                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcn1                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcn2                              :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
        pcn3                              :   OUT   std_logic_vector(4 DOWNTO 0)  -- sfix5
        );
END sort_3c_src_Sorting3C;


ARCHITECTURE rtl OF sort_3c_src_Sorting3C IS

  -- Component Declarations
  COMPONENT sort_3c_src_MATLAB_Function1
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          v1                              :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          v2                              :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          v3                              :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
          icluster                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
          enable                          :   IN    std_logic;  -- ufix1
          pos1                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pos2                            :   OUT   std_logic_vector(4 DOWNTO 0);  -- sfix5
          pos3                            :   OUT   std_logic_vector(4 DOWNTO 0)  -- sfix5
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : sort_3c_src_MATLAB_Function1
    USE ENTITY work.sort_3c_src_MATLAB_Function1(rtl);

  -- Signals
  SIGNAL MATLAB_Function6_out1            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function6_out2            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function6_out3            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pos1                             : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pos2                             : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL pos3                             : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function2_out1            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function2_out2            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function2_out3            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function5_out1            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function5_out2            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function5_out3            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function3_out1            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function3_out2            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function3_out3            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function4_out1            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function4_out2            : std_logic_vector(4 DOWNTO 0);  -- ufix5
  SIGNAL MATLAB_Function4_out3            : std_logic_vector(4 DOWNTO 0);  -- ufix5

BEGIN
  u_MATLAB_Function6 : sort_3c_src_MATLAB_Function1
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              v1 => VcaP1,  -- sfix25_En14
              v2 => VcaP2,  -- sfix25_En14
              v3 => VcaP3,  -- sfix25_En14
              icluster => IaP,  -- sfix25_En16
              enable => sync,  -- ufix1
              pos1 => MATLAB_Function6_out1,  -- sfix5
              pos2 => MATLAB_Function6_out2,  -- sfix5
              pos3 => MATLAB_Function6_out3  -- sfix5
              );

  u_MATLAB_Function1 : sort_3c_src_MATLAB_Function1
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              v1 => Vcbp1,  -- sfix25_En14
              v2 => Vcbp2,  -- sfix25_En14
              v3 => Vcbp3,  -- sfix25_En14
              icluster => IbP,  -- sfix25_En16
              enable => sync,  -- ufix1
              pos1 => pos1,  -- sfix5
              pos2 => pos2,  -- sfix5
              pos3 => pos3  -- sfix5
              );

  u_MATLAB_Function2 : sort_3c_src_MATLAB_Function1
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              v1 => Vccp1,  -- sfix25_En14
              v2 => Vccp2,  -- sfix25_En14
              v3 => Vccp3,  -- sfix25_En14
              icluster => IcP,  -- sfix25_En16
              enable => sync,  -- ufix1
              pos1 => MATLAB_Function2_out1,  -- sfix5
              pos2 => MATLAB_Function2_out2,  -- sfix5
              pos3 => MATLAB_Function2_out3  -- sfix5
              );

  u_MATLAB_Function5 : sort_3c_src_MATLAB_Function1
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              v1 => Vcan1,  -- sfix25_En14
              v2 => Vcan2,  -- sfix25_En14
              v3 => Vcan3,  -- sfix25_En14
              icluster => IaN,  -- sfix25_En16
              enable => sync,  -- ufix1
              pos1 => MATLAB_Function5_out1,  -- sfix5
              pos2 => MATLAB_Function5_out2,  -- sfix5
              pos3 => MATLAB_Function5_out3  -- sfix5
              );

  u_MATLAB_Function3 : sort_3c_src_MATLAB_Function1
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              v1 => Vcbn1,  -- sfix25_En14
              v2 => Vcbn2,  -- sfix25_En14
              v3 => Vcbn3,  -- sfix25_En14
              icluster => IbN,  -- sfix25_En16
              enable => sync,  -- ufix1
              pos1 => MATLAB_Function3_out1,  -- sfix5
              pos2 => MATLAB_Function3_out2,  -- sfix5
              pos3 => MATLAB_Function3_out3  -- sfix5
              );

  u_MATLAB_Function4 : sort_3c_src_MATLAB_Function1
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              v1 => Vccn1,  -- sfix25_En14
              v2 => Vccn2,  -- sfix25_En14
              v3 => Vccn3,  -- sfix25_En14
              icluster => IcN,  -- sfix25_En16
              enable => sync,  -- ufix1
              pos1 => MATLAB_Function4_out1,  -- sfix5
              pos2 => MATLAB_Function4_out2,  -- sfix5
              pos3 => MATLAB_Function4_out3  -- sfix5
              );

  ce_out <= clk_enable;

  pap1 <= MATLAB_Function6_out1;

  pap2 <= MATLAB_Function6_out2;

  pap3 <= MATLAB_Function6_out3;

  pbp1 <= pos1;

  pbp2 <= pos2;

  pbp3 <= pos3;

  pcp1 <= MATLAB_Function2_out1;

  pcp2 <= MATLAB_Function2_out2;

  pcp3 <= MATLAB_Function2_out3;

  pan1 <= MATLAB_Function5_out1;

  pan2 <= MATLAB_Function5_out2;

  pan3 <= MATLAB_Function5_out3;

  pbn1 <= MATLAB_Function3_out1;

  pbn2 <= MATLAB_Function3_out2;

  pbn3 <= MATLAB_Function3_out3;

  pcn1 <= MATLAB_Function4_out1;

  pcn2 <= MATLAB_Function4_out2;

  pcn3 <= MATLAB_Function4_out3;

END rtl;

