/*
 * File Name:         F:\sort_3_cell\sort3\ipcore\sort_3c_v1_0\include\sort_3c_addr.h
 * Description:       C Header File
 * Created:           2019-05-17 11:12:48
*/

#ifndef SORT_3C_H_
#define SORT_3C_H_

#define  IPCore_Reset_sort_3c       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_sort_3c      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_sort_3c   0x8  //contains unique IP timestamp (yymmddHHMM): 1905171112

#endif /* SORT_3C_H_ */
