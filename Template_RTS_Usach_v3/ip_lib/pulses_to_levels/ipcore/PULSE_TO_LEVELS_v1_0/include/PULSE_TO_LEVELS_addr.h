/*
 * File Name:         E:\sort_3_cell\pulses_to_levels\ipcore\PULSE_TO_LEVELS_v1_0\include\PULSE_TO_LEVELS_addr.h
 * Description:       C Header File
 * Created:           2019-09-05 16:01:30
*/

#ifndef PULSE_TO_LEVELS_H_
#define PULSE_TO_LEVELS_H_

#define  IPCore_Reset_PULSE_TO_LEVELS       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PULSE_TO_LEVELS      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PULSE_TO_LEVELS   0x8  //contains unique IP timestamp (yymmddHHMM): 1909051601

#endif /* PULSE_TO_LEVELS_H_ */
