SLStudio.Utils.RemoveHighlighting(get_param('sorting_3cell_sm_trigg_cond', 'handle'));
SLStudio.Utils.RemoveHighlighting(get_param('gm_sorting_3cell_sm_trigg_cond', 'handle'));
annotate_port('gm_sorting_3cell_sm_trigg_cond/PULSE_TO_LEVEL/Sum', 0, 1, '');
annotate_port('gm_sorting_3cell_sm_trigg_cond/PULSE_TO_LEVEL/Sum2', 0, 1, '');
annotate_port('gm_sorting_3cell_sm_trigg_cond/PULSE_TO_LEVEL/Sum2', 1, 1, '');
annotate_port('sorting_3cell_sm_trigg_cond/PULSE_TO_LEVEL/Sum2', 1, 1, '');
