function RTW_SidParentMap() {
    this.sidParentMap = new Array();
    this.sidParentMap["sorting_3cell_sm_trigg_cond:838"] = "sorting_3cell_sm_trigg_cond:837";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:839"] = "sorting_3cell_sm_trigg_cond:837";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:840"] = "sorting_3cell_sm_trigg_cond:837";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:841"] = "sorting_3cell_sm_trigg_cond:837";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:842"] = "sorting_3cell_sm_trigg_cond:837";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:843"] = "sorting_3cell_sm_trigg_cond:837";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:286"] = "sorting_3cell_sm_trigg_cond:837";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:287"] = "sorting_3cell_sm_trigg_cond:837";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:288"] = "sorting_3cell_sm_trigg_cond:837";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:552"] = "sorting_3cell_sm_trigg_cond:837";
    this.sidParentMap["sorting_3cell_sm_trigg_cond:844"] = "sorting_3cell_sm_trigg_cond:837";
    this.getParentSid = function(sid) { return this.sidParentMap[sid];}
}
    RTW_SidParentMap.instance = new RTW_SidParentMap();
