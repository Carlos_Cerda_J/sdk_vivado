function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <S2>/Sum */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:286"] = "PULSE_TO_LEVELS_src_PULSE_TO_LEVEL.vhd:57,58,59";
	/* <S2>/Sum1 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:287"] = "PULSE_TO_LEVELS_src_PULSE_TO_LEVEL.vhd:61,62,63";
	/* <S2>/Sum2 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:288"] = "PULSE_TO_LEVELS_src_PULSE_TO_LEVEL.vhd:69,70";
	/* <S2>/Sum4 */
	this.urlHashMap["sorting_3cell_sm_trigg_cond:552"] = "PULSE_TO_LEVELS_src_PULSE_TO_LEVEL.vhd:65,66,67";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "sorting_3cell_sm_trigg_cond"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S2>/S11"] = {sid: "sorting_3cell_sm_trigg_cond:838"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:838"] = {rtwname: "<S2>/S11"};
	this.rtwnameHashMap["<S2>/S12"] = {sid: "sorting_3cell_sm_trigg_cond:839"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:839"] = {rtwname: "<S2>/S12"};
	this.rtwnameHashMap["<S2>/S21"] = {sid: "sorting_3cell_sm_trigg_cond:840"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:840"] = {rtwname: "<S2>/S21"};
	this.rtwnameHashMap["<S2>/S22"] = {sid: "sorting_3cell_sm_trigg_cond:841"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:841"] = {rtwname: "<S2>/S22"};
	this.rtwnameHashMap["<S2>/S31"] = {sid: "sorting_3cell_sm_trigg_cond:842"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:842"] = {rtwname: "<S2>/S31"};
	this.rtwnameHashMap["<S2>/S32"] = {sid: "sorting_3cell_sm_trigg_cond:843"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:843"] = {rtwname: "<S2>/S32"};
	this.rtwnameHashMap["<S2>/Sum"] = {sid: "sorting_3cell_sm_trigg_cond:286"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:286"] = {rtwname: "<S2>/Sum"};
	this.rtwnameHashMap["<S2>/Sum1"] = {sid: "sorting_3cell_sm_trigg_cond:287"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:287"] = {rtwname: "<S2>/Sum1"};
	this.rtwnameHashMap["<S2>/Sum2"] = {sid: "sorting_3cell_sm_trigg_cond:288"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:288"] = {rtwname: "<S2>/Sum2"};
	this.rtwnameHashMap["<S2>/Sum4"] = {sid: "sorting_3cell_sm_trigg_cond:552"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:552"] = {rtwname: "<S2>/Sum4"};
	this.rtwnameHashMap["<S2>/LEVELS"] = {sid: "sorting_3cell_sm_trigg_cond:844"};
	this.sidHashMap["sorting_3cell_sm_trigg_cond:844"] = {rtwname: "<S2>/LEVELS"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
