/*
 * File Name:         C:\Users\Victor\Desktop\sorting\nlc_2cellcond\ipcore\nlc_2c_cond_v1_0\include\nlc_2c_cond_addr.h
 * Description:       C Header File
 * Created:           2019-05-03 14:41:11
*/

#ifndef NLC_2C_COND_H_
#define NLC_2C_COND_H_

#define  IPCore_Reset_nlc_2c_cond       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_nlc_2c_cond      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_nlc_2c_cond   0x8  //contains unique IP timestamp (yymmddHHMM): 1905031441
#define  Vc_ref_Data_nlc_2c_cond        0x100  //data register for Inport Vc_ref

#endif /* NLC_2C_COND_H_ */
