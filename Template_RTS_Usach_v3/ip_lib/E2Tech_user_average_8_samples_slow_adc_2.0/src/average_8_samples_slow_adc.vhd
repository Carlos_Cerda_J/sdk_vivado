----------------------------------------------------------------------------------
-- Company: E2Tech
-- Engineer: David Arancibia G.
-- 
-- Create Date: 11/19/2019 03:33:15 PM
-- Design Name: 
-- Module Name: average_8_samples_slow_adc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity average_8_samples_slow_adc is
    Port ( 
		clock	  :	in std_logic;			--Clock signal
		reset_n   :	in std_logic;			--Reset signal
		band	  : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_1 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_2 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_3 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_4 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_5 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_6 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_7 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_8 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_9 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_10 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_11 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_12 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_13 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_14 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_15 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_16 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_17 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_18 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_19 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_20 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_21 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_22 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_23 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_24 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_25 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_26 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_27 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_28 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_29 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_30 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_31 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_32 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_33 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_34 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_35 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_36 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_37 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_38 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_39 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_40 : in STD_LOGIC_VECTOR (11 downto 0);
		data_out_1 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_2 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_3 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_4 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_5 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_6 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_7 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_8 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_9 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_10 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_11 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_12 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_13 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_14 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_15 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_16 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_17 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_18 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_19 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_20 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_21 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_22 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_23 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_24 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_25 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_26 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_27 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_28 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_29 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_30 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_31 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_32 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_33 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_34 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_35 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_36 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_37 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_38 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_39 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_40 : out STD_LOGIC_VECTOR (11 downto 0);		
		new_value	:		out std_logic              			--new valid value flag
		);
end average_8_samples_slow_adc;

architecture Behavioral of average_8_samples_slow_adc is
	signal data_avg_1_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_2_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_3_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_4_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_5_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_6_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_7_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_8_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_9_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_10_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_11_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_12_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_13_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_14_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_15_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_16_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_17_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_18_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_19_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_20_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_21_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_22_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_23_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_24_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_25_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_26_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_27_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_28_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_29_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_30_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_31_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_32_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_33_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_34_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_35_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_36_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_37_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_38_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_39_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_40_S 	: std_logic_vector (11 downto 0) := (others => '0');
	
	signal data_clean_1_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_2_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_3_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_4_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_5_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_6_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_7_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_8_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_9_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_10_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_11_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_12_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_13_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_14_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_15_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_16_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_17_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_18_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_19_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_20_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_21_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_22_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_23_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_24_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_25_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_26_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_27_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_28_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_29_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_30_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_31_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_32_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_33_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_34_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_35_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_36_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_37_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_38_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_39_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_40_S 	: std_logic_vector (11 downto 0) := (others => '0');
	
	--Declare value_averaging componet
	component value_averaging  is 
	port
	(
		clock:			in std_logic := '0';			--Clock signal
		reset_n:		in std_logic := '0';			--Reset signal
		data_in:		in std_logic_vector(11 downto 0) := (others => '0');			--Input data for the current clock cycle
		data_out:		out std_logic_vector(11 downto 0) := (others => '0');			--Current output data (valid for 8 clock cycles)
		new_value:		out std_logic := '0'                                 			--new valid value flag
	);
	end component value_averaging;

	component spike_mitigation is
    Port ( clock : in STD_LOGIC;
           reset_n : in STD_LOGIC;
           data_in : in STD_LOGIC_VECTOR (11 downto 0);
           data_avg : in STD_LOGIC_VECTOR (11 downto 0);
		   band	 : in STD_LOGIC_VECTOR (11 downto 0);
           data_out : out STD_LOGIC_VECTOR (11 downto 0));
	end component spike_mitigation;
	
begin
	data_out_1 <= data_avg_1_S;
	data_out_2 <= data_avg_2_S;
	data_out_3 <= data_avg_3_S;
	data_out_4 <= data_avg_4_S;
	data_out_5 <= data_avg_5_S;
	data_out_6 <= data_avg_6_S;
	data_out_7 <= data_avg_7_S;
	data_out_8 <= data_avg_8_S;
	data_out_9 <= data_avg_9_S;
	data_out_10 <= data_avg_10_S;
	data_out_11 <= data_avg_11_S;
	data_out_12 <= data_avg_12_S;
	data_out_13 <= data_avg_13_S;
	data_out_14 <= data_avg_14_S;
	data_out_15 <= data_avg_15_S;
	data_out_16 <= data_avg_16_S;
	data_out_17 <= data_avg_17_S;
	data_out_18 <= data_avg_18_S;
	data_out_19 <= data_avg_19_S;
	data_out_20 <= data_avg_20_S;
	data_out_21 <= data_avg_21_S;
	data_out_22 <= data_avg_22_S;
	data_out_23 <= data_avg_23_S;
	data_out_24 <= data_avg_24_S;
	data_out_25 <= data_avg_25_S;
	data_out_26 <= data_avg_26_S;
	data_out_27 <= data_avg_27_S;
	data_out_28 <= data_avg_28_S;
	data_out_29 <= data_avg_29_S;
	data_out_30 <= data_avg_30_S;
	data_out_31 <= data_avg_31_S;
	data_out_32 <= data_avg_32_S;
	data_out_33 <= data_avg_33_S;
	data_out_34 <= data_avg_34_S;
	data_out_35 <= data_avg_35_S;
	data_out_36 <= data_avg_36_S;
	data_out_37 <= data_avg_37_S;
	data_out_38 <= data_avg_38_S;
	data_out_39 <= data_avg_39_S;
	data_out_40 <= data_avg_40_S;
	
	--Instantiate spike mitigation componets
	spike_1 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_1,
        data_avg 	=> data_avg_1_S,
		band	 	=> band,
        data_out 	=> data_clean_1_S
	);

	spike_2 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_2,
        data_avg 	=> data_avg_2_S,
		band	 	=> band,
        data_out 	=> data_clean_2_S
	);

	spike_3 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_3,
        data_avg 	=> data_avg_3_S,
		band	 	=> band,
        data_out 	=> data_clean_3_S
	);

	spike_4 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_4,
        data_avg 	=> data_avg_4_S,
		band	 	=> band,
        data_out 	=> data_clean_4_S
	);

	spike_5 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_5,
        data_avg 	=> data_avg_5_S,
		band	 	=> band,
        data_out 	=> data_clean_5_S
	);

	spike_6 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_6,
        data_avg 	=> data_avg_6_S,
		band	 	=> band,
        data_out 	=> data_clean_6_S
	);

	spike_7 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_7,
        data_avg 	=> data_avg_7_S,
		band	 	=> band,
        data_out 	=> data_clean_7_S
	);

	spike_8 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_8,
        data_avg 	=> data_avg_8_S,
		band	 	=> band,
        data_out 	=> data_clean_8_S
	);

	spike_9 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_9,
        data_avg 	=> data_avg_9_S,
		band	 	=> band,
        data_out 	=> data_clean_9_S
	);

	spike_10 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_10,
        data_avg 	=> data_avg_10_S,
		band	 	=> band,
        data_out 	=> data_clean_10_S
	);

	spike_11 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_11,
        data_avg 	=> data_avg_11_S,
		band	 	=> band,
        data_out 	=> data_clean_11_S
	);

	spike_12 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_12,
        data_avg 	=> data_avg_12_S,
		band	 	=> band,
        data_out 	=> data_clean_12_S
	);

	spike_13 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_13,
        data_avg 	=> data_avg_13_S,
		band	 	=> band,
        data_out 	=> data_clean_13_S
	);

	spike_14 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_14,
        data_avg 	=> data_avg_14_S,
		band	 	=> band,
        data_out 	=> data_clean_14_S
	);

	spike_15 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_15,
        data_avg 	=> data_avg_15_S,
		band	 	=> band,
        data_out 	=> data_clean_15_S
	);

	spike_16 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_16,
        data_avg 	=> data_avg_16_S,
		band	 	=> band,
        data_out 	=> data_clean_16_S
	);

	spike_17 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_17,
        data_avg 	=> data_avg_17_S,
		band	 	=> band,
        data_out 	=> data_clean_17_S
	);

	spike_18 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_18,
        data_avg 	=> data_avg_18_S,
		band	 	=> band,
        data_out 	=> data_clean_18_S
	);

	spike_19 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_19,
        data_avg 	=> data_avg_19_S,
		band	 	=> band,
        data_out 	=> data_clean_19_S
	);

	spike_20 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_20,
        data_avg 	=> data_avg_20_S,
		band	 	=> band,
        data_out 	=> data_clean_20_S
	);

	spike_21 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_21,
        data_avg 	=> data_avg_21_S,
		band	 	=> band,
        data_out 	=> data_clean_21_S
	);

	spike_22 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_22,
        data_avg 	=> data_avg_22_S,
		band	 	=> band,
        data_out 	=> data_clean_22_S
	);

	spike_23 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_23,
        data_avg 	=> data_avg_23_S,
		band	 	=> band,
        data_out 	=> data_clean_23_S
	);

	spike_24 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_24,
        data_avg 	=> data_avg_24_S,
		band	 	=> band,
        data_out 	=> data_clean_24_S
	);

	spike_25 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_25,
        data_avg 	=> data_avg_25_S,
		band	 	=> band,
        data_out 	=> data_clean_25_S
	);

	spike_26 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_26,
        data_avg 	=> data_avg_26_S,
		band	 	=> band,
        data_out 	=> data_clean_26_S
	);

	spike_27 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_27,
        data_avg 	=> data_avg_27_S,
		band	 	=> band,
        data_out 	=> data_clean_27_S
	);

	spike_28 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_28,
        data_avg 	=> data_avg_28_S,
		band	 	=> band,
        data_out 	=> data_clean_28_S
	);

	spike_29 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_29,
        data_avg 	=> data_avg_29_S,
		band	 	=> band,
        data_out 	=> data_clean_29_S
	);

	spike_30 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_30,
        data_avg 	=> data_avg_30_S,
		band	 	=> band,
        data_out 	=> data_clean_30_S
	);

	spike_31 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_31,
        data_avg 	=> data_avg_31_S,
		band	 	=> band,
        data_out 	=> data_clean_31_S
	);
	
	spike_32 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_32,
        data_avg 	=> data_avg_32_S,
		band	 	=> band,
        data_out 	=> data_clean_32_S
	);
	
	spike_33 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_33,
        data_avg 	=> data_avg_33_S,
		band	 	=> band,
        data_out 	=> data_clean_33_S
	);
	
	spike_34 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_34,
        data_avg 	=> data_avg_34_S,
		band	 	=> band,
        data_out 	=> data_clean_34_S
	);
	
	spike_35 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_35,
        data_avg 	=> data_avg_35_S,
		band	 	=> band,
        data_out 	=> data_clean_35_S
	);
	
	spike_36 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_36,
        data_avg 	=> data_avg_36_S,
		band	 	=> band,
        data_out 	=> data_clean_36_S
	);
	
	spike_37 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_37,
        data_avg 	=> data_avg_37_S,
		band	 	=> band,
        data_out 	=> data_clean_37_S
	);
	
	spike_38 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_38,
        data_avg 	=> data_avg_38_S,
		band	 	=> band,
        data_out 	=> data_clean_38_S
	);
	
	spike_39 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_39,
        data_avg 	=> data_avg_39_S,
		band	 	=> band,
        data_out 	=> data_clean_39_S
	);	
	
	spike_40 : spike_mitigation
    Port map( 
		clock	 	=> clock,
        reset_n 	=> reset_n,
        data_in 	=> data_in_40,
        data_avg 	=> data_avg_40_S,
		band	 	=> band,
        data_out 	=> data_clean_40_S
	);	
	
	--Instantiate value_averaging componets
	average_1 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_1_S,
		data_out	=> data_avg_1_S,--data_out_1,
		new_value	=> new_value
	 );

	average_2 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_2_S,
		data_out	=> data_avg_2_S
	 );
	 
	average_3 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_3_S,
		data_out	=> data_avg_3_S
	 );	 
	 
	average_4 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_4_S,
		data_out	=> data_avg_4_S
	 );	 
	 
	average_5 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_5_S,
		data_out	=> data_avg_5_S
	 );	 
	 
	average_6 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_6_S,
		data_out	=> data_avg_6_S
	 );	 
	 
	average_7 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_7_S,
		data_out	=> data_avg_7_S
	 );	 
	 
	average_8 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_8_S,
		data_out	=> data_avg_8_S
	 );	 
	 
	average_9 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_9_S,
		data_out	=> data_avg_9_S
	 );	 
	 
	average_10 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_10_S,
		data_out	=> data_avg_10_S
	 );	 
	
	average_11 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_11_S,
		data_out	=> data_avg_11_S
	 );	 
	 
	average_12 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_12_S,
		data_out	=> data_avg_12_S
	 );	 
	 
	average_13 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_13_S,
		data_out	=> data_avg_13_S
	 );	 
	 
	average_14 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_14_S,
		data_out	=> data_avg_14_S
	 );	 
	 
	average_15 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_15_S,
		data_out	=> data_avg_15_S
	 );	 
	 
	average_16 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_16_S,
		data_out	=> data_avg_16_S
	 );	 
	 
	average_17 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_17_S,
		data_out	=> data_avg_17_S
	 );	 
	 
	average_18 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_18_S,
		data_out	=> data_avg_18_S
	 );	 
	 
	average_19 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_19_S,
		data_out	=> data_avg_19_S
	 );	 
	 
	average_20 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_20_S,
		data_out	=> data_avg_20_S
	 );	 
	 
	average_21 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_21_S,
		data_out	=> data_avg_21_S
	 );	 
	 
	average_22 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_22_S,
		data_out	=> data_avg_22_S
	 );	 
	 
	average_23 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_23_S,
		data_out	=> data_avg_23_S
	 );	 
	 
	average_24 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_24_S,
		data_out	=> data_avg_24_S
	 );	 
	 
	average_25 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_25_S,
		data_out	=> data_avg_25_S
	 );	 
	 
	average_26 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_26_S,
		data_out	=> data_avg_26_S
	 );	 
	 
	average_27 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_27_S,
		data_out	=> data_avg_27_S
	 );	 
	 
	average_28 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_28_S,
		data_out	=> data_avg_28_S
	 );	 
	 
	average_29 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_29_S,
		data_out	=> data_avg_29_S
	 );	 
	 
	average_30 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_30_S,
		data_out	=> data_avg_30_S
	 );	 
	 
	average_31 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_31_S,
		data_out	=> data_avg_31_S
	 );	 
	 
	average_32 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_32_S,
		data_out	=> data_avg_32_S
	 );	 
	 
	average_33 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_33_S,
		data_out	=> data_avg_33_S
	 );	 
	 
	average_34 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_34_S,
		data_out	=> data_avg_34_S
	 );	 
	 
	average_35 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_35_S,
		data_out	=> data_avg_35_S
	 );	 
	 
	average_36 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_36_S,
		data_out	=> data_avg_36_S
	 );	 
	 
	average_37 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_37_S,
		data_out	=> data_avg_37_S
	 );	 
	 
	average_38 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_38_S,
		data_out	=> data_avg_38_S
	 );	 
	 
	average_39 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_39_S,
		data_out	=> data_avg_39_S
	 );	 
	 
	average_40 : value_averaging 
	PORT MAP
	(
		clock		=> clock,
		reset_n		=> reset_n,
		data_in		=> data_clean_40_S,
		data_out	=> data_avg_40_S
	 );	 
	 
		
end Behavioral;
