/*
 * File Name:         C:\HDL_IP\PLLL\ipcore\PLL2_ip_v1_0\include\PLL2_ip_addr.h
 * Description:       C Header File
 * Created:           2019-01-14 21:39:42
*/

#ifndef PLL2_IP_H_
#define PLL2_IP_H_

#define  IPCore_Reset_PLL2_ip        0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PLL2_ip       0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PLL2_ip    0x8  //contains unique IP timestamp (yymmddHHMM): 1901142139
#define  kpz_Data_PLL2_ip            0x100  //data register for Inport kpz
#define  kpzPkiz_Data_PLL2_ip        0x104  //data register for Inport kpzPkiz
#define  T_s_Data_PLL2_ip            0x10C  //data register for Inport T_s
#define  normalizer_Data_PLL2_ip     0x114  //data register for Inport normalizer
#define  denormalizer_Data_PLL2_ip   0x118  //data register for Inport denormalizer
#define  Vq__Data_PLL2_ip            0x124  //data register for Outport Vq_
#define  w__Data_PLL2_ip             0x128  //data register for Outport w_

#endif /* PLL2_IP_H_ */
