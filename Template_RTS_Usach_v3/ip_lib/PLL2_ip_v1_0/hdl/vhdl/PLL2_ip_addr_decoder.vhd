-- -------------------------------------------------------------
-- 
-- File Name: C:\HDL_IP\PLLL\hdlsrc\PLL_25bits_ok\PLL2_ip_addr_decoder.vhd
-- Created: 2019-01-14 21:39:42
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: PLL2_ip_addr_decoder
-- Source Path: PLL2_ip/PLL2_ip_axi_lite/PLL2_ip_addr_decoder
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY PLL2_ip_addr_decoder IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        data_write                        :   IN    std_logic_vector(31 DOWNTO 0);  -- ufix32
        addr_sel                          :   IN    std_logic_vector(13 DOWNTO 0);  -- ufix14
        wr_enb                            :   IN    std_logic;  -- ufix1
        rd_enb                            :   IN    std_logic;  -- ufix1
        read_ip_timestamp                 :   IN    std_logic_vector(31 DOWNTO 0);  -- ufix32
        read_Vq                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En21
        read_w                            :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        data_read                         :   OUT   std_logic_vector(31 DOWNTO 0);  -- ufix32
        write_axi_enable                  :   OUT   std_logic;  -- ufix1
        write_kpz                         :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En17
        write_kpzPkiz                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En17
        write_T_s                         :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En21
        write_normalizer                  :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En21
        write_denormalizer                :   OUT   std_logic_vector(24 DOWNTO 0)  -- sfix25_En14
        );
END PLL2_ip_addr_decoder;


ARCHITECTURE rtl OF PLL2_ip_addr_decoder IS

  -- Signals
  SIGNAL enb                              : std_logic;
  SIGNAL addr_sel_unsigned                : unsigned(13 DOWNTO 0);  -- ufix14
  SIGNAL decode_sel_ip_timestamp          : std_logic;  -- ufix1
  SIGNAL read_ip_timestamp_unsigned       : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL const_1                          : std_logic;  -- ufix1
  SIGNAL read_Vq_signed                   : signed(24 DOWNTO 0);  -- sfix25_En21
  SIGNAL read_w_signed                    : signed(24 DOWNTO 0);  -- sfix25_En14
  SIGNAL decode_sel_w                     : std_logic;  -- ufix1
  SIGNAL decode_sel_Vq                    : std_logic;  -- ufix1
  SIGNAL const_0                          : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_ip_timestamp            : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_ip_timestamp           : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_Vq                      : signed(24 DOWNTO 0);  -- sfix25_En21
  SIGNAL data_in_Vq                       : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_Vq                     : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_w                       : signed(24 DOWNTO 0);  -- sfix25_En14
  SIGNAL data_in_w                        : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_w                      : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_sel_axi_enable            : std_logic;  -- ufix1
  SIGNAL reg_enb_axi_enable               : std_logic;  -- ufix1
  SIGNAL data_write_unsigned              : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL data_in_axi_enable               : std_logic;  -- ufix1
  SIGNAL write_reg_axi_enable             : std_logic;  -- ufix1
  SIGNAL decode_sel_kpz                   : std_logic;  -- ufix1
  SIGNAL reg_enb_kpz                      : std_logic;  -- ufix1
  SIGNAL data_in_kpz                      : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL write_reg_kpz                    : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL decode_sel_kpzPkiz               : std_logic;  -- ufix1
  SIGNAL reg_enb_kpzPkiz                  : std_logic;  -- ufix1
  SIGNAL data_in_kpzPkiz                  : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL write_reg_kpzPkiz                : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL decode_sel_T_s                   : std_logic;  -- ufix1
  SIGNAL reg_enb_T_s                      : std_logic;  -- ufix1
  SIGNAL data_in_T_s                      : signed(24 DOWNTO 0);  -- sfix25_En21
  SIGNAL write_reg_T_s                    : signed(24 DOWNTO 0);  -- sfix25_En21
  SIGNAL decode_sel_normalizer            : std_logic;  -- ufix1
  SIGNAL reg_enb_normalizer               : std_logic;  -- ufix1
  SIGNAL data_in_normalizer               : signed(24 DOWNTO 0);  -- sfix25_En21
  SIGNAL write_reg_normalizer             : signed(24 DOWNTO 0);  -- sfix25_En21
  SIGNAL decode_sel_denormalizer          : std_logic;  -- ufix1
  SIGNAL reg_enb_denormalizer             : std_logic;  -- ufix1
  SIGNAL data_in_denormalizer             : signed(24 DOWNTO 0);  -- sfix25_En14
  SIGNAL write_reg_denormalizer           : signed(24 DOWNTO 0);  -- sfix25_En14

BEGIN
  addr_sel_unsigned <= unsigned(addr_sel);

  
  decode_sel_ip_timestamp <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0002#, 14) ELSE
      '0';

  read_ip_timestamp_unsigned <= unsigned(read_ip_timestamp);

  const_1 <= '1';

  enb <= const_1;

  read_Vq_signed <= signed(read_Vq);

  read_w_signed <= signed(read_w);

  
  decode_sel_w <= '1' WHEN addr_sel_unsigned = to_unsigned(16#004A#, 14) ELSE
      '0';

  
  decode_sel_Vq <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0049#, 14) ELSE
      '0';

  const_0 <= to_unsigned(0, 32);

  reg_ip_timestamp_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      read_reg_ip_timestamp <= to_unsigned(0, 32);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        read_reg_ip_timestamp <= read_ip_timestamp_unsigned;
      END IF;
    END IF;
  END PROCESS reg_ip_timestamp_process;


  
  decode_rd_ip_timestamp <= const_0 WHEN decode_sel_ip_timestamp = '0' ELSE
      read_reg_ip_timestamp;

  reg_Vq_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      read_reg_Vq <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        read_reg_Vq <= read_Vq_signed;
      END IF;
    END IF;
  END PROCESS reg_Vq_process;


  data_in_Vq <= unsigned(resize(read_reg_Vq, 32));

  
  decode_rd_Vq <= decode_rd_ip_timestamp WHEN decode_sel_Vq = '0' ELSE
      data_in_Vq;

  reg_w_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      read_reg_w <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        read_reg_w <= read_w_signed;
      END IF;
    END IF;
  END PROCESS reg_w_process;


  data_in_w <= unsigned(resize(read_reg_w, 32));

  
  decode_rd_w <= decode_rd_Vq WHEN decode_sel_w = '0' ELSE
      data_in_w;

  data_read <= std_logic_vector(decode_rd_w);

  
  decode_sel_axi_enable <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0001#, 14) ELSE
      '0';

  reg_enb_axi_enable <= decode_sel_axi_enable AND wr_enb;

  data_write_unsigned <= unsigned(data_write);

  data_in_axi_enable <= data_write_unsigned(0);

  reg_axi_enable_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      write_reg_axi_enable <= '1';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_axi_enable = '1' THEN
        write_reg_axi_enable <= data_in_axi_enable;
      END IF;
    END IF;
  END PROCESS reg_axi_enable_process;


  write_axi_enable <= write_reg_axi_enable;

  
  decode_sel_kpz <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0040#, 14) ELSE
      '0';

  reg_enb_kpz <= decode_sel_kpz AND wr_enb;

  data_in_kpz <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_kpz_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      write_reg_kpz <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_kpz = '1' THEN
        write_reg_kpz <= data_in_kpz;
      END IF;
    END IF;
  END PROCESS reg_kpz_process;


  write_kpz <= std_logic_vector(write_reg_kpz);

  
  decode_sel_kpzPkiz <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0041#, 14) ELSE
      '0';

  reg_enb_kpzPkiz <= decode_sel_kpzPkiz AND wr_enb;

  data_in_kpzPkiz <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_kpzPkiz_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      write_reg_kpzPkiz <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_kpzPkiz = '1' THEN
        write_reg_kpzPkiz <= data_in_kpzPkiz;
      END IF;
    END IF;
  END PROCESS reg_kpzPkiz_process;


  write_kpzPkiz <= std_logic_vector(write_reg_kpzPkiz);

  
  decode_sel_T_s <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0043#, 14) ELSE
      '0';

  reg_enb_T_s <= decode_sel_T_s AND wr_enb;

  data_in_T_s <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_T_s_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      write_reg_T_s <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_T_s = '1' THEN
        write_reg_T_s <= data_in_T_s;
      END IF;
    END IF;
  END PROCESS reg_T_s_process;


  write_T_s <= std_logic_vector(write_reg_T_s);

  
  decode_sel_normalizer <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0045#, 14) ELSE
      '0';

  reg_enb_normalizer <= decode_sel_normalizer AND wr_enb;

  data_in_normalizer <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_normalizer_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      write_reg_normalizer <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_normalizer = '1' THEN
        write_reg_normalizer <= data_in_normalizer;
      END IF;
    END IF;
  END PROCESS reg_normalizer_process;


  write_normalizer <= std_logic_vector(write_reg_normalizer);

  
  decode_sel_denormalizer <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0046#, 14) ELSE
      '0';

  reg_enb_denormalizer <= decode_sel_denormalizer AND wr_enb;

  data_in_denormalizer <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_denormalizer_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      write_reg_denormalizer <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_denormalizer = '1' THEN
        write_reg_denormalizer <= data_in_denormalizer;
      END IF;
    END IF;
  END PROCESS reg_denormalizer_process;


  write_denormalizer <= std_logic_vector(write_reg_denormalizer);

END rtl;

