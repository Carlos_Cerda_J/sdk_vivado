-- -------------------------------------------------------------
-- 
-- File Name: C:\HDL_IP\PLLL\hdlsrc\PLL_25bits_ok\PLL2_ip_src_Discrete_PI.vhd
-- Created: 2019-01-14 21:39:39
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: PLL2_ip_src_Discrete_PI
-- Source Path: PLL_25bits_ok/PLL2/Discrete_PI
-- Hierarchy Level: 1
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE work.PLL2_ip_src_PLL2_pkg.ALL;

ENTITY PLL2_ip_src_Discrete_PI IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb_1_10_0                        :   IN    std_logic;
        error_rsvd                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En21
        kpz                               :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En17
        kpz_kiz                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En17
        SCLK                              :   IN    std_logic;
        u                                 :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En15
        To_D1                             :   OUT   std_logic_vector(24 DOWNTO 0)  -- sfix25_En17
        );
END PLL2_ip_src_Discrete_PI;


ARCHITECTURE rtl OF PLL2_ip_src_Discrete_PI IS

  -- Signals
  SIGNAL error_signed                     : signed(24 DOWNTO 0);  -- sfix25_En21
  SIGNAL error_rsvd_1                     : signed(24 DOWNTO 0);  -- sfix25_En21
  SIGNAL kpz_kiz_signed                   : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL HwModeRegister1_reg              : vector_of_signed25(0 TO 14);  -- sfix25 [15]
  SIGNAL kpz_kiz_1                        : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Product1_out1                    : signed(49 DOWNTO 0);  -- sfix50_En38
  SIGNAL Product1_out1_1                  : signed(49 DOWNTO 0);  -- sfix50_En38
  SIGNAL Product1_out1_2                  : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL reduced_reg                      : std_logic_vector(0 TO 15);  -- ufix1 [16]
  SIGNAL SCLK_1                           : std_logic;
  SIGNAL kpz_signed                       : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL HwModeRegister3_reg              : vector_of_signed25(0 TO 14);  -- sfix25 [15]
  SIGNAL kpz_1                            : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Product2_out1                    : signed(49 DOWNTO 0);  -- sfix50_En38
  SIGNAL Product2_out1_1                  : signed(49 DOWNTO 0);  -- sfix50_En38
  SIGNAL Product2_out1_2                  : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Delay2_delOut                    : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Delay2_ectrl                     : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Delay2_out1                      : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Delay2_last_value                : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Sum_op_stage2                    : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Delay1_delOut                    : signed(24 DOWNTO 0);  -- sfix25_En15
  SIGNAL Sum_out1                         : signed(24 DOWNTO 0);  -- sfix25_En15
  SIGNAL Delay1_ectrl                     : signed(24 DOWNTO 0);  -- sfix25_En15
  SIGNAL Delay1_out1                      : signed(24 DOWNTO 0);  -- sfix25_En15
  SIGNAL Delay1_last_value                : signed(24 DOWNTO 0);  -- sfix25_En15
  SIGNAL Sum_stage3_add_cast              : signed(24 DOWNTO 0);  -- sfix25_En17
  SIGNAL Sum_stage3_add_temp              : signed(24 DOWNTO 0);  -- sfix25_En17

BEGIN
  error_signed <= signed(error_rsvd);

  reduced_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      error_rsvd_1 <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_10_0 = '1' THEN
        error_rsvd_1 <= error_signed;
      END IF;
    END IF;
  END PROCESS reduced_process;


  kpz_kiz_signed <= signed(kpz_kiz);

  HwModeRegister1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      HwModeRegister1_reg <= (OTHERS => to_signed(16#0000000#, 25));
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_10_0 = '1' THEN
        HwModeRegister1_reg(0) <= kpz_kiz_signed;
        HwModeRegister1_reg(1 TO 14) <= HwModeRegister1_reg(0 TO 13);
      END IF;
    END IF;
  END PROCESS HwModeRegister1_process;

  kpz_kiz_1 <= HwModeRegister1_reg(14);

  Product1_out1 <= error_rsvd_1 * kpz_kiz_1;

  PipelineRegister_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Product1_out1_1 <= to_signed(0, 50);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_10_0 = '1' THEN
        Product1_out1_1 <= Product1_out1;
      END IF;
    END IF;
  END PROCESS PipelineRegister_process;


  Product1_out1_2 <= Product1_out1_1(45 DOWNTO 21) + ('0' & Product1_out1_1(20));

  reduced_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      reduced_reg <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_10_0 = '1' THEN
        reduced_reg(0) <= SCLK;
        reduced_reg(1 TO 15) <= reduced_reg(0 TO 14);
      END IF;
    END IF;
  END PROCESS reduced_1_process;

  SCLK_1 <= reduced_reg(15);

  kpz_signed <= signed(kpz);

  HwModeRegister3_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      HwModeRegister3_reg <= (OTHERS => to_signed(16#0000000#, 25));
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_10_0 = '1' THEN
        HwModeRegister3_reg(0) <= kpz_signed;
        HwModeRegister3_reg(1 TO 14) <= HwModeRegister3_reg(0 TO 13);
      END IF;
    END IF;
  END PROCESS HwModeRegister3_process;

  kpz_1 <= HwModeRegister3_reg(14);

  Product2_out1 <= error_rsvd_1 * kpz_1;

  PipelineRegister1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Product2_out1_1 <= to_signed(0, 50);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_10_0 = '1' THEN
        Product2_out1_1 <= Product2_out1;
      END IF;
    END IF;
  END PROCESS PipelineRegister1_process;


  Product2_out1_2 <= Product2_out1_1(45 DOWNTO 21) + ('0' & Product2_out1_1(20));

  
  Delay2_ectrl <= Delay2_delOut WHEN SCLK_1 = '0' ELSE
      Product2_out1_2;

  Delay2_lowered_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay2_delOut <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_10_0 = '1' THEN
        Delay2_delOut <= Delay2_ectrl;
      END IF;
    END IF;
  END PROCESS Delay2_lowered_process;


  Delay2_bypass_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay2_last_value <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_10_0 = '1' THEN
        Delay2_last_value <= Delay2_out1;
      END IF;
    END IF;
  END PROCESS Delay2_bypass_process;


  
  Delay2_out1 <= Delay2_last_value WHEN SCLK_1 = '0' ELSE
      Delay2_delOut;

  Sum_op_stage2 <= Product1_out1_2 - Delay2_out1;

  
  Delay1_ectrl <= Delay1_delOut WHEN SCLK_1 = '0' ELSE
      Sum_out1;

  Delay1_lowered_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay1_delOut <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_10_0 = '1' THEN
        Delay1_delOut <= Delay1_ectrl;
      END IF;
    END IF;
  END PROCESS Delay1_lowered_process;


  Delay1_bypass_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay1_last_value <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_10_0 = '1' THEN
        Delay1_last_value <= Delay1_out1;
      END IF;
    END IF;
  END PROCESS Delay1_bypass_process;


  
  Delay1_out1 <= Delay1_last_value WHEN SCLK_1 = '0' ELSE
      Delay1_delOut;

  Sum_stage3_add_cast <= Delay1_out1(22 DOWNTO 0) & '0' & '0';
  Sum_stage3_add_temp <= Sum_op_stage2 + Sum_stage3_add_cast;
  Sum_out1 <= (resize(Sum_stage3_add_temp(24 DOWNTO 2), 25)) + ('0' & Sum_stage3_add_temp(1));

  u <= std_logic_vector(Sum_out1);

  To_D1 <= std_logic_vector(Product2_out1_2);

END rtl;

