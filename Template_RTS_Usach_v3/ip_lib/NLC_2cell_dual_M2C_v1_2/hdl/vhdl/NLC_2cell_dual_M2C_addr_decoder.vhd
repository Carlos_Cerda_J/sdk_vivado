-- -------------------------------------------------------------
-- 
-- File Name: hdl_prj\hdlsrc\NLC_dual_2cell\NLC_2cell_dual_M2C_addr_decoder.vhd
-- Created: 2021-01-27 17:23:27
-- 
-- Generated by MATLAB 9.8 and HDL Coder 3.16
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: NLC_2cell_dual_M2C_addr_decoder
-- Source Path: NLC_2cell_dual_M2C/NLC_2cell_dual_M2C_axi_lite/NLC_2cell_dual_M2C_addr_decoder
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY NLC_2cell_dual_M2C_addr_decoder IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        data_write                        :   IN    std_logic_vector(31 DOWNTO 0);  -- ufix32
        addr_sel                          :   IN    std_logic_vector(13 DOWNTO 0);  -- ufix14
        wr_enb                            :   IN    std_logic;  -- ufix1
        rd_enb                            :   IN    std_logic;  -- ufix1
        read_ip_timestamp                 :   IN    std_logic_vector(31 DOWNTO 0);  -- ufix32
        data_read                         :   OUT   std_logic_vector(31 DOWNTO 0);  -- ufix32
        write_axi_enable                  :   OUT   std_logic;  -- ufix1
        write_Vdc_inv                     :   OUT   std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
        write_Vdc_inv_prop                :   OUT   std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
        write_selector_NLC                :   OUT   std_logic;  -- ufix1
        write_selector_V0S                :   OUT   std_logic;  -- ufix1
        write_Vap_ref                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        write_Vbp_ref                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        write_Vcp_ref                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        write_Van_ref                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        write_Vbn_ref                     :   OUT   std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        write_Vcn_ref                     :   OUT   std_logic_vector(24 DOWNTO 0)  -- sfix25_En13
        );
END NLC_2cell_dual_M2C_addr_decoder;


ARCHITECTURE rtl OF NLC_2cell_dual_M2C_addr_decoder IS

  -- Signals
  SIGNAL enb                              : std_logic;
  SIGNAL addr_sel_unsigned                : unsigned(13 DOWNTO 0);  -- ufix14
  SIGNAL decode_sel_ip_timestamp_1_1      : std_logic;  -- ufix1
  SIGNAL read_ip_timestamp_unsigned       : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL const_1                          : std_logic;  -- ufix1
  SIGNAL const_0                          : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_ip_timestamp            : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_ip_timestamp_1_1       : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_sel_axi_enable_1_1        : std_logic;  -- ufix1
  SIGNAL reg_enb_axi_enable_1_1           : std_logic;  -- ufix1
  SIGNAL data_write_unsigned              : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL data_in_axi_enable               : std_logic;  -- ufix1
  SIGNAL data_reg_axi_enable_1_1          : std_logic;  -- ufix1
  SIGNAL write_concats_axi_enable_1       : std_logic;  -- ufix1
  SIGNAL decode_sel_Vdc_inv_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Vdc_inv_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Vdc_inv                  : signed(17 DOWNTO 0);  -- sfix18_En17
  SIGNAL data_reg_Vdc_inv_1_1             : signed(17 DOWNTO 0);  -- sfix18_En17
  SIGNAL decode_sel_Vdc_inv_prop_1_1      : std_logic;  -- ufix1
  SIGNAL reg_enb_Vdc_inv_prop_1_1         : std_logic;  -- ufix1
  SIGNAL data_in_Vdc_inv_prop             : signed(17 DOWNTO 0);  -- sfix18_En17
  SIGNAL data_reg_Vdc_inv_prop_1_1        : signed(17 DOWNTO 0);  -- sfix18_En17
  SIGNAL decode_sel_selector_NLC_1_1      : std_logic;  -- ufix1
  SIGNAL reg_enb_selector_NLC_1_1         : std_logic;  -- ufix1
  SIGNAL data_in_selector_NLC             : std_logic;  -- ufix1
  SIGNAL data_reg_selector_NLC_1_1        : std_logic;  -- ufix1
  SIGNAL write_concats_selector_NLC_1     : std_logic;  -- ufix1
  SIGNAL decode_sel_selector_V0S_1_1      : std_logic;  -- ufix1
  SIGNAL reg_enb_selector_V0S_1_1         : std_logic;  -- ufix1
  SIGNAL data_in_selector_V0S             : std_logic;  -- ufix1
  SIGNAL data_reg_selector_V0S_1_1        : std_logic;  -- ufix1
  SIGNAL write_concats_selector_V0S_1     : std_logic;  -- ufix1
  SIGNAL decode_sel_Vap_ref_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Vap_ref_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Vap_ref                  : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL data_reg_Vap_ref_1_1             : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL decode_sel_Vbp_ref_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Vbp_ref_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Vbp_ref                  : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL data_reg_Vbp_ref_1_1             : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL decode_sel_Vcp_ref_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Vcp_ref_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Vcp_ref                  : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL data_reg_Vcp_ref_1_1             : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL decode_sel_Van_ref_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Van_ref_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Van_ref                  : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL data_reg_Van_ref_1_1             : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL decode_sel_Vbn_ref_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Vbn_ref_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Vbn_ref                  : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL data_reg_Vbn_ref_1_1             : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL decode_sel_Vcn_ref_1_1           : std_logic;  -- ufix1
  SIGNAL reg_enb_Vcn_ref_1_1              : std_logic;  -- ufix1
  SIGNAL data_in_Vcn_ref                  : signed(24 DOWNTO 0);  -- sfix25_En13
  SIGNAL data_reg_Vcn_ref_1_1             : signed(24 DOWNTO 0);  -- sfix25_En13

BEGIN
  addr_sel_unsigned <= unsigned(addr_sel);

  
  decode_sel_ip_timestamp_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0002#, 14) ELSE
      '0';

  read_ip_timestamp_unsigned <= unsigned(read_ip_timestamp);

  const_1 <= '1';

  enb <= const_1;

  const_0 <= to_unsigned(0, 32);

  reg_ip_timestamp_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        read_reg_ip_timestamp <= to_unsigned(0, 32);
      ELSIF enb = '1' THEN
        read_reg_ip_timestamp <= read_ip_timestamp_unsigned;
      END IF;
    END IF;
  END PROCESS reg_ip_timestamp_process;


  
  decode_rd_ip_timestamp_1_1 <= const_0 WHEN decode_sel_ip_timestamp_1_1 = '0' ELSE
      read_reg_ip_timestamp;

  data_read <= std_logic_vector(decode_rd_ip_timestamp_1_1);

  
  decode_sel_axi_enable_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0001#, 14) ELSE
      '0';

  reg_enb_axi_enable_1_1 <= decode_sel_axi_enable_1_1 AND wr_enb;

  data_write_unsigned <= unsigned(data_write);

  data_in_axi_enable <= data_write_unsigned(0);

  reg_axi_enable_1_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        data_reg_axi_enable_1_1 <= '1';
      ELSIF enb = '1' AND reg_enb_axi_enable_1_1 = '1' THEN
        data_reg_axi_enable_1_1 <= data_in_axi_enable;
      END IF;
    END IF;
  END PROCESS reg_axi_enable_1_1_process;


  write_concats_axi_enable_1 <= data_reg_axi_enable_1_1;

  write_axi_enable <= write_concats_axi_enable_1;

  
  decode_sel_Vdc_inv_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0040#, 14) ELSE
      '0';

  reg_enb_Vdc_inv_1_1 <= decode_sel_Vdc_inv_1_1 AND wr_enb;

  data_in_Vdc_inv <= signed(data_write_unsigned(17 DOWNTO 0));

  reg_Vdc_inv_1_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        data_reg_Vdc_inv_1_1 <= to_signed(16#00000#, 18);
      ELSIF enb = '1' AND reg_enb_Vdc_inv_1_1 = '1' THEN
        data_reg_Vdc_inv_1_1 <= data_in_Vdc_inv;
      END IF;
    END IF;
  END PROCESS reg_Vdc_inv_1_1_process;


  write_Vdc_inv <= std_logic_vector(data_reg_Vdc_inv_1_1);

  
  decode_sel_Vdc_inv_prop_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0041#, 14) ELSE
      '0';

  reg_enb_Vdc_inv_prop_1_1 <= decode_sel_Vdc_inv_prop_1_1 AND wr_enb;

  data_in_Vdc_inv_prop <= signed(data_write_unsigned(17 DOWNTO 0));

  reg_Vdc_inv_prop_1_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        data_reg_Vdc_inv_prop_1_1 <= to_signed(16#00000#, 18);
      ELSIF enb = '1' AND reg_enb_Vdc_inv_prop_1_1 = '1' THEN
        data_reg_Vdc_inv_prop_1_1 <= data_in_Vdc_inv_prop;
      END IF;
    END IF;
  END PROCESS reg_Vdc_inv_prop_1_1_process;


  write_Vdc_inv_prop <= std_logic_vector(data_reg_Vdc_inv_prop_1_1);

  
  decode_sel_selector_NLC_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0042#, 14) ELSE
      '0';

  reg_enb_selector_NLC_1_1 <= decode_sel_selector_NLC_1_1 AND wr_enb;

  data_in_selector_NLC <= data_write_unsigned(0);

  reg_selector_NLC_1_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        data_reg_selector_NLC_1_1 <= '0';
      ELSIF enb = '1' AND reg_enb_selector_NLC_1_1 = '1' THEN
        data_reg_selector_NLC_1_1 <= data_in_selector_NLC;
      END IF;
    END IF;
  END PROCESS reg_selector_NLC_1_1_process;


  write_concats_selector_NLC_1 <= data_reg_selector_NLC_1_1;

  write_selector_NLC <= write_concats_selector_NLC_1;

  
  decode_sel_selector_V0S_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0043#, 14) ELSE
      '0';

  reg_enb_selector_V0S_1_1 <= decode_sel_selector_V0S_1_1 AND wr_enb;

  data_in_selector_V0S <= data_write_unsigned(0);

  reg_selector_V0S_1_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        data_reg_selector_V0S_1_1 <= '0';
      ELSIF enb = '1' AND reg_enb_selector_V0S_1_1 = '1' THEN
        data_reg_selector_V0S_1_1 <= data_in_selector_V0S;
      END IF;
    END IF;
  END PROCESS reg_selector_V0S_1_1_process;


  write_concats_selector_V0S_1 <= data_reg_selector_V0S_1_1;

  write_selector_V0S <= write_concats_selector_V0S_1;

  
  decode_sel_Vap_ref_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0044#, 14) ELSE
      '0';

  reg_enb_Vap_ref_1_1 <= decode_sel_Vap_ref_1_1 AND wr_enb;

  data_in_Vap_ref <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Vap_ref_1_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        data_reg_Vap_ref_1_1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Vap_ref_1_1 = '1' THEN
        data_reg_Vap_ref_1_1 <= data_in_Vap_ref;
      END IF;
    END IF;
  END PROCESS reg_Vap_ref_1_1_process;


  write_Vap_ref <= std_logic_vector(data_reg_Vap_ref_1_1);

  
  decode_sel_Vbp_ref_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0045#, 14) ELSE
      '0';

  reg_enb_Vbp_ref_1_1 <= decode_sel_Vbp_ref_1_1 AND wr_enb;

  data_in_Vbp_ref <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Vbp_ref_1_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        data_reg_Vbp_ref_1_1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Vbp_ref_1_1 = '1' THEN
        data_reg_Vbp_ref_1_1 <= data_in_Vbp_ref;
      END IF;
    END IF;
  END PROCESS reg_Vbp_ref_1_1_process;


  write_Vbp_ref <= std_logic_vector(data_reg_Vbp_ref_1_1);

  
  decode_sel_Vcp_ref_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0046#, 14) ELSE
      '0';

  reg_enb_Vcp_ref_1_1 <= decode_sel_Vcp_ref_1_1 AND wr_enb;

  data_in_Vcp_ref <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Vcp_ref_1_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        data_reg_Vcp_ref_1_1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Vcp_ref_1_1 = '1' THEN
        data_reg_Vcp_ref_1_1 <= data_in_Vcp_ref;
      END IF;
    END IF;
  END PROCESS reg_Vcp_ref_1_1_process;


  write_Vcp_ref <= std_logic_vector(data_reg_Vcp_ref_1_1);

  
  decode_sel_Van_ref_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0047#, 14) ELSE
      '0';

  reg_enb_Van_ref_1_1 <= decode_sel_Van_ref_1_1 AND wr_enb;

  data_in_Van_ref <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Van_ref_1_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        data_reg_Van_ref_1_1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Van_ref_1_1 = '1' THEN
        data_reg_Van_ref_1_1 <= data_in_Van_ref;
      END IF;
    END IF;
  END PROCESS reg_Van_ref_1_1_process;


  write_Van_ref <= std_logic_vector(data_reg_Van_ref_1_1);

  
  decode_sel_Vbn_ref_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0048#, 14) ELSE
      '0';

  reg_enb_Vbn_ref_1_1 <= decode_sel_Vbn_ref_1_1 AND wr_enb;

  data_in_Vbn_ref <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Vbn_ref_1_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        data_reg_Vbn_ref_1_1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Vbn_ref_1_1 = '1' THEN
        data_reg_Vbn_ref_1_1 <= data_in_Vbn_ref;
      END IF;
    END IF;
  END PROCESS reg_Vbn_ref_1_1_process;


  write_Vbn_ref <= std_logic_vector(data_reg_Vbn_ref_1_1);

  
  decode_sel_Vcn_ref_1_1 <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0049#, 14) ELSE
      '0';

  reg_enb_Vcn_ref_1_1 <= decode_sel_Vcn_ref_1_1 AND wr_enb;

  data_in_Vcn_ref <= signed(data_write_unsigned(24 DOWNTO 0));

  reg_Vcn_ref_1_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        data_reg_Vcn_ref_1_1 <= to_signed(16#0000000#, 25);
      ELSIF enb = '1' AND reg_enb_Vcn_ref_1_1 = '1' THEN
        data_reg_Vcn_ref_1_1 <= data_in_Vcn_ref;
      END IF;
    END IF;
  END PROCESS reg_Vcn_ref_1_1_process;


  write_Vcn_ref <= std_logic_vector(data_reg_Vcn_ref_1_1);

END rtl;

