-- -------------------------------------------------------------
-- 
-- File Name: hdl_prj\hdlsrc\NLC_dual_2cell\NLC_2cell_dual_M2C_dut.vhd
-- Created: 2021-01-27 17:23:27
-- 
-- Generated by MATLAB 9.8 and HDL Coder 3.16
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: NLC_2cell_dual_M2C_dut
-- Source Path: NLC_2cell_dual_M2C/NLC_2cell_dual_M2C_dut
-- Hierarchy Level: 1
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY NLC_2cell_dual_M2C_dut IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        dut_enable                        :   IN    std_logic;  -- ufix1
        Vap_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        Vbp_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        Vcp_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        Van_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        Vbn_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        Vcn_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        V0S_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        Vdc_inv                           :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
        Vdc_inv_prop                      :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
        selector_NLC                      :   IN    std_logic;  -- ufix1
        selector_V0S                      :   IN    std_logic;  -- ufix1
        ce_out                            :   OUT   std_logic;  -- ufix1
        Level_ap                          :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_bp                          :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_cp                          :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_an                          :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_bn                          :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_cn                          :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_ap_prop                     :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_bp_prop                     :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_cp_prop                     :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_an_prop                     :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_bn_prop                     :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_cn_prop                     :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_aSigma                      :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_bSigma                      :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_cSigma                      :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_aDelta                      :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_bDelta                      :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Level_cDelta                      :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
        Sap_11                            :   OUT   std_logic;  -- ufix1
        Sap_12                            :   OUT   std_logic;  -- ufix1
        Sap_21                            :   OUT   std_logic;  -- ufix1
        Sap_22                            :   OUT   std_logic;  -- ufix1
        Sbp_11                            :   OUT   std_logic;  -- ufix1
        Sbp_12                            :   OUT   std_logic;  -- ufix1
        Sbp_21                            :   OUT   std_logic;  -- ufix1
        Sbp_22                            :   OUT   std_logic;  -- ufix1
        Scp_11                            :   OUT   std_logic;  -- ufix1
        Scp_12                            :   OUT   std_logic;  -- ufix1
        Scp_21                            :   OUT   std_logic;  -- ufix1
        Scp_22                            :   OUT   std_logic;  -- ufix1
        San_11                            :   OUT   std_logic;  -- ufix1
        San_12                            :   OUT   std_logic;  -- ufix1
        San_21                            :   OUT   std_logic;  -- ufix1
        San_22                            :   OUT   std_logic;  -- ufix1
        Sbn_11                            :   OUT   std_logic;  -- ufix1
        Sbn_12                            :   OUT   std_logic;  -- ufix1
        Sbn_21                            :   OUT   std_logic;  -- ufix1
        Sbn_22                            :   OUT   std_logic;  -- ufix1
        Scn_11                            :   OUT   std_logic;  -- ufix1
        Scn_12                            :   OUT   std_logic;  -- ufix1
        Scn_21                            :   OUT   std_logic;  -- ufix1
        Scn_22                            :   OUT   std_logic  -- ufix1
        );
END NLC_2cell_dual_M2C_dut;


ARCHITECTURE rtl OF NLC_2cell_dual_M2C_dut IS

  -- Component Declarations
  COMPONENT NLC_2cell_dual_M2C_src_NLC_2cell_dual_M2C
    PORT( clk                             :   IN    std_logic;
          clk_enable                      :   IN    std_logic;
          reset                           :   IN    std_logic;
          Vap_ref                         :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          Vbp_ref                         :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          Vcp_ref                         :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          Van_ref                         :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          Vbn_ref                         :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          Vcn_ref                         :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          V0S_ref                         :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          Vdc_inv                         :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
          Vdc_inv_prop                    :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
          selector_NLC                    :   IN    std_logic;  -- ufix1
          selector_V0S                    :   IN    std_logic;  -- ufix1
          ce_out                          :   OUT   std_logic;  -- ufix1
          Level_ap                        :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_bp                        :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_cp                        :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_an                        :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_bn                        :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_cn                        :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_ap_prop                   :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_bp_prop                   :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_cp_prop                   :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_an_prop                   :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_bn_prop                   :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_cn_prop                   :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_aSigma                    :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_bSigma                    :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_cSigma                    :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_aDelta                    :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_bDelta                    :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Level_cDelta                    :   OUT   std_logic_vector(2 DOWNTO 0);  -- sfix3
          Sap_11                          :   OUT   std_logic;  -- ufix1
          Sap_12                          :   OUT   std_logic;  -- ufix1
          Sap_21                          :   OUT   std_logic;  -- ufix1
          Sap_22                          :   OUT   std_logic;  -- ufix1
          Sbp_11                          :   OUT   std_logic;  -- ufix1
          Sbp_12                          :   OUT   std_logic;  -- ufix1
          Sbp_21                          :   OUT   std_logic;  -- ufix1
          Sbp_22                          :   OUT   std_logic;  -- ufix1
          Scp_11                          :   OUT   std_logic;  -- ufix1
          Scp_12                          :   OUT   std_logic;  -- ufix1
          Scp_21                          :   OUT   std_logic;  -- ufix1
          Scp_22                          :   OUT   std_logic;  -- ufix1
          San_11                          :   OUT   std_logic;  -- ufix1
          San_12                          :   OUT   std_logic;  -- ufix1
          San_21                          :   OUT   std_logic;  -- ufix1
          San_22                          :   OUT   std_logic;  -- ufix1
          Sbn_11                          :   OUT   std_logic;  -- ufix1
          Sbn_12                          :   OUT   std_logic;  -- ufix1
          Sbn_21                          :   OUT   std_logic;  -- ufix1
          Sbn_22                          :   OUT   std_logic;  -- ufix1
          Scn_11                          :   OUT   std_logic;  -- ufix1
          Scn_12                          :   OUT   std_logic;  -- ufix1
          Scn_21                          :   OUT   std_logic;  -- ufix1
          Scn_22                          :   OUT   std_logic  -- ufix1
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : NLC_2cell_dual_M2C_src_NLC_2cell_dual_M2C
    USE ENTITY work.NLC_2cell_dual_M2C_src_NLC_2cell_dual_M2C(rtl);

  -- Signals
  SIGNAL enb                              : std_logic;
  SIGNAL selector_NLC_sig                 : std_logic;  -- ufix1
  SIGNAL selector_V0S_sig                 : std_logic;  -- ufix1
  SIGNAL ce_out_sig                       : std_logic;  -- ufix1
  SIGNAL Level_ap_sig                     : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_bp_sig                     : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_cp_sig                     : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_an_sig                     : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_bn_sig                     : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_cn_sig                     : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_ap_prop_sig                : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_bp_prop_sig                : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_cp_prop_sig                : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_an_prop_sig                : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_bn_prop_sig                : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_cn_prop_sig                : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_aSigma_sig                 : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_bSigma_sig                 : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_cSigma_sig                 : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_aDelta_sig                 : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_bDelta_sig                 : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Level_cDelta_sig                 : std_logic_vector(2 DOWNTO 0);  -- ufix3
  SIGNAL Sap_11_sig                       : std_logic;  -- ufix1
  SIGNAL Sap_12_sig                       : std_logic;  -- ufix1
  SIGNAL Sap_21_sig                       : std_logic;  -- ufix1
  SIGNAL Sap_22_sig                       : std_logic;  -- ufix1
  SIGNAL Sbp_11_sig                       : std_logic;  -- ufix1
  SIGNAL Sbp_12_sig                       : std_logic;  -- ufix1
  SIGNAL Sbp_21_sig                       : std_logic;  -- ufix1
  SIGNAL Sbp_22_sig                       : std_logic;  -- ufix1
  SIGNAL Scp_11_sig                       : std_logic;  -- ufix1
  SIGNAL Scp_12_sig                       : std_logic;  -- ufix1
  SIGNAL Scp_21_sig                       : std_logic;  -- ufix1
  SIGNAL Scp_22_sig                       : std_logic;  -- ufix1
  SIGNAL San_11_sig                       : std_logic;  -- ufix1
  SIGNAL San_12_sig                       : std_logic;  -- ufix1
  SIGNAL San_21_sig                       : std_logic;  -- ufix1
  SIGNAL San_22_sig                       : std_logic;  -- ufix1
  SIGNAL Sbn_11_sig                       : std_logic;  -- ufix1
  SIGNAL Sbn_12_sig                       : std_logic;  -- ufix1
  SIGNAL Sbn_21_sig                       : std_logic;  -- ufix1
  SIGNAL Sbn_22_sig                       : std_logic;  -- ufix1
  SIGNAL Scn_11_sig                       : std_logic;  -- ufix1
  SIGNAL Scn_12_sig                       : std_logic;  -- ufix1
  SIGNAL Scn_21_sig                       : std_logic;  -- ufix1
  SIGNAL Scn_22_sig                       : std_logic;  -- ufix1

BEGIN
  u_NLC_2cell_dual_M2C_src_NLC_2cell_dual_M2C : NLC_2cell_dual_M2C_src_NLC_2cell_dual_M2C
    PORT MAP( clk => clk,
              clk_enable => enb,
              reset => reset,
              Vap_ref => Vap_ref,  -- sfix25_En13
              Vbp_ref => Vbp_ref,  -- sfix25_En13
              Vcp_ref => Vcp_ref,  -- sfix25_En13
              Van_ref => Van_ref,  -- sfix25_En13
              Vbn_ref => Vbn_ref,  -- sfix25_En13
              Vcn_ref => Vcn_ref,  -- sfix25_En13
              V0S_ref => V0S_ref,  -- sfix25_En13
              Vdc_inv => Vdc_inv,  -- sfix18_En17
              Vdc_inv_prop => Vdc_inv_prop,  -- sfix18_En17
              selector_NLC => selector_NLC_sig,  -- ufix1
              selector_V0S => selector_V0S_sig,  -- ufix1
              ce_out => ce_out_sig,  -- ufix1
              Level_ap => Level_ap_sig,  -- sfix3
              Level_bp => Level_bp_sig,  -- sfix3
              Level_cp => Level_cp_sig,  -- sfix3
              Level_an => Level_an_sig,  -- sfix3
              Level_bn => Level_bn_sig,  -- sfix3
              Level_cn => Level_cn_sig,  -- sfix3
              Level_ap_prop => Level_ap_prop_sig,  -- sfix3
              Level_bp_prop => Level_bp_prop_sig,  -- sfix3
              Level_cp_prop => Level_cp_prop_sig,  -- sfix3
              Level_an_prop => Level_an_prop_sig,  -- sfix3
              Level_bn_prop => Level_bn_prop_sig,  -- sfix3
              Level_cn_prop => Level_cn_prop_sig,  -- sfix3
              Level_aSigma => Level_aSigma_sig,  -- sfix3
              Level_bSigma => Level_bSigma_sig,  -- sfix3
              Level_cSigma => Level_cSigma_sig,  -- sfix3
              Level_aDelta => Level_aDelta_sig,  -- sfix3
              Level_bDelta => Level_bDelta_sig,  -- sfix3
              Level_cDelta => Level_cDelta_sig,  -- sfix3
              Sap_11 => Sap_11_sig,  -- ufix1
              Sap_12 => Sap_12_sig,  -- ufix1
              Sap_21 => Sap_21_sig,  -- ufix1
              Sap_22 => Sap_22_sig,  -- ufix1
              Sbp_11 => Sbp_11_sig,  -- ufix1
              Sbp_12 => Sbp_12_sig,  -- ufix1
              Sbp_21 => Sbp_21_sig,  -- ufix1
              Sbp_22 => Sbp_22_sig,  -- ufix1
              Scp_11 => Scp_11_sig,  -- ufix1
              Scp_12 => Scp_12_sig,  -- ufix1
              Scp_21 => Scp_21_sig,  -- ufix1
              Scp_22 => Scp_22_sig,  -- ufix1
              San_11 => San_11_sig,  -- ufix1
              San_12 => San_12_sig,  -- ufix1
              San_21 => San_21_sig,  -- ufix1
              San_22 => San_22_sig,  -- ufix1
              Sbn_11 => Sbn_11_sig,  -- ufix1
              Sbn_12 => Sbn_12_sig,  -- ufix1
              Sbn_21 => Sbn_21_sig,  -- ufix1
              Sbn_22 => Sbn_22_sig,  -- ufix1
              Scn_11 => Scn_11_sig,  -- ufix1
              Scn_12 => Scn_12_sig,  -- ufix1
              Scn_21 => Scn_21_sig,  -- ufix1
              Scn_22 => Scn_22_sig  -- ufix1
              );

  selector_NLC_sig <= selector_NLC;

  selector_V0S_sig <= selector_V0S;

  enb <= dut_enable;

  ce_out <= ce_out_sig;

  Level_ap <= Level_ap_sig;

  Level_bp <= Level_bp_sig;

  Level_cp <= Level_cp_sig;

  Level_an <= Level_an_sig;

  Level_bn <= Level_bn_sig;

  Level_cn <= Level_cn_sig;

  Level_ap_prop <= Level_ap_prop_sig;

  Level_bp_prop <= Level_bp_prop_sig;

  Level_cp_prop <= Level_cp_prop_sig;

  Level_an_prop <= Level_an_prop_sig;

  Level_bn_prop <= Level_bn_prop_sig;

  Level_cn_prop <= Level_cn_prop_sig;

  Level_aSigma <= Level_aSigma_sig;

  Level_bSigma <= Level_bSigma_sig;

  Level_cSigma <= Level_cSigma_sig;

  Level_aDelta <= Level_aDelta_sig;

  Level_bDelta <= Level_bDelta_sig;

  Level_cDelta <= Level_cDelta_sig;

  Sap_11 <= Sap_11_sig;

  Sap_12 <= Sap_12_sig;

  Sap_21 <= Sap_21_sig;

  Sap_22 <= Sap_22_sig;

  Sbp_11 <= Sbp_11_sig;

  Sbp_12 <= Sbp_12_sig;

  Sbp_21 <= Sbp_21_sig;

  Sbp_22 <= Sbp_22_sig;

  Scp_11 <= Scp_11_sig;

  Scp_12 <= Scp_12_sig;

  Scp_21 <= Scp_21_sig;

  Scp_22 <= Scp_22_sig;

  San_11 <= San_11_sig;

  San_12 <= San_12_sig;

  San_21 <= San_21_sig;

  San_22 <= San_22_sig;

  Sbn_11 <= Sbn_11_sig;

  Sbn_12 <= Sbn_12_sig;

  Sbn_21 <= Sbn_21_sig;

  Sbn_22 <= Sbn_22_sig;

  Scn_11 <= Scn_11_sig;

  Scn_12 <= Scn_12_sig;

  Scn_21 <= Scn_21_sig;

  Scn_22 <= Scn_22_sig;

END rtl;

