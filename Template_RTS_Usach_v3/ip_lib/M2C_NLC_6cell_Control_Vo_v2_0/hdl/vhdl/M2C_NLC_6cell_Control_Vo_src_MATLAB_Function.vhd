-- -------------------------------------------------------------
-- 
-- File Name: hdl_prj\hdlsrc\M2C_NLC_6cell_Control_Vo\M2C_NLC_6cell_Control_Vo_src_MATLAB_Function.vhd
-- Created: 2021-04-06 16:51:12
-- 
-- Generated by MATLAB 9.8 and HDL Coder 3.16
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: M2C_NLC_6cell_Control_Vo_src_MATLAB_Function
-- Source Path: M2C_NLC_6cell_Control_Vo/M2C_NLC_6cell_Control_Vo/V0Sigma_generator/MATLAB Function
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY M2C_NLC_6cell_Control_Vo_src_MATLAB_Function IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb_1_50_0                        :   IN    std_logic;
        limit                             :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18
        ccount                            :   OUT   std_logic_vector(17 DOWNTO 0)  -- sfix18
        );
END M2C_NLC_6cell_Control_Vo_src_MATLAB_Function;


ARCHITECTURE rtl OF M2C_NLC_6cell_Control_Vo_src_MATLAB_Function IS

  -- Signals
  SIGNAL limit_signed                     : signed(17 DOWNTO 0);  -- sfix18
  SIGNAL ccount_tmp                       : signed(17 DOWNTO 0);  -- sfix18
  SIGNAL count                            : signed(17 DOWNTO 0);  -- sfix18
  SIGNAL count_next                       : signed(17 DOWNTO 0);  -- sfix18

BEGIN
  limit_signed <= signed(limit);

  MATLAB_Function_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        count <= to_signed(16#00000#, 18);
      ELSIF enb_1_50_0 = '1' THEN
        count <= count_next;
      END IF;
    END IF;
  END PROCESS MATLAB_Function_process;

  MATLAB_Function_output : PROCESS (count, limit_signed)
    VARIABLE count_temp : signed(17 DOWNTO 0);
    VARIABLE add_temp : signed(18 DOWNTO 0);
  BEGIN
    add_temp := to_signed(16#00000#, 19);
    --MATLAB Function 'M2C_NLC_6cell_Control_Vo/V0Sigma_generator/MATLAB Function'
    --contador de 0 hasta limit
    IF count < limit_signed THEN 
      add_temp := resize(count, 19) + to_signed(16#00001#, 19);
      IF (add_temp(18) = '0') AND (add_temp(17) /= '0') THEN 
        count_temp := "011111111111111111";
      ELSIF (add_temp(18) = '1') AND (add_temp(17) /= '1') THEN 
        count_temp := "100000000000000000";
      ELSE 
        count_temp := add_temp(17 DOWNTO 0);
      END IF;
    ELSE 
      count_temp := to_signed(16#00000#, 18);
    END IF;
    ccount_tmp <= count_temp;
    count_next <= count_temp;
  END PROCESS MATLAB_Function_output;


  ccount <= std_logic_vector(ccount_tmp);

END rtl;

