/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\abc2albe\abc2albe_2514\ipcore\abc2albe_ip_v2_0\include\abc2albe_ip_addr.h
 * Description:       C Header File
 * Created:           2019-07-19 16:54:48
*/

#ifndef ABC2ALBE_IP_H_
#define ABC2ALBE_IP_H_

#define  IPCore_Reset_abc2albe_ip       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_abc2albe_ip      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_abc2albe_ip   0x8  //contains unique IP timestamp (yymmddHHMM): 1907191654

#endif /* ABC2ALBE_IP_H_ */
