/*
 * File Name:         hdl_prj\ipcore\M2C_NLC_6cell_Control_Vo_v1_3\include\M2C_NLC_6cell_Control_Vo_addr.h
 * Description:       C Header File
 * Created:           2021-03-20 11:23:13
*/

#ifndef M2C_NLC_6CELL_CONTROL_VO_H_
#define M2C_NLC_6CELL_CONTROL_VO_H_

#define  IPCore_Reset_M2C_NLC_6cell_Control_Vo                0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_M2C_NLC_6cell_Control_Vo               0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_M2C_NLC_6cell_Control_Vo            0x8  //contains unique IP timestamp (yymmddHHMM): 2103201123
#define  limit_counter_wo_Data_M2C_NLC_6cell_Control_Vo       0x100  //data register for Inport limit_counter_wo
#define  limit_counter_wo_inv_Data_M2C_NLC_6cell_Control_Vo   0x104  //data register for Inport limit_counter_wo_inv
#define  Vdc_inv_Data_M2C_NLC_6cell_Control_Vo                0x108  //data register for Inport Vdc_inv
#define  V0Sigma_peak_ref_Data_M2C_NLC_6cell_Control_Vo       0x10C  //data register for Inport V0Sigma_peak_ref
#define  Vdc_inv_prop_Data_M2C_NLC_6cell_Control_Vo           0x110  //data register for Inport Vdc_inv_prop
#define  selector_NLC_Data_M2C_NLC_6cell_Control_Vo           0x114  //data register for Inport selector_NLC
#define  Vap_ref_Data_M2C_NLC_6cell_Control_Vo                0x118  //data register for Inport Vap_ref
#define  Vbp_ref_Data_M2C_NLC_6cell_Control_Vo                0x11C  //data register for Inport Vbp_ref
#define  Vcp_ref_Data_M2C_NLC_6cell_Control_Vo                0x120  //data register for Inport Vcp_ref
#define  Van_ref_Data_M2C_NLC_6cell_Control_Vo                0x124  //data register for Inport Van_ref
#define  Vbn_ref_Data_M2C_NLC_6cell_Control_Vo                0x128  //data register for Inport Vbn_ref
#define  Vcn_ref_Data_M2C_NLC_6cell_Control_Vo                0x12C  //data register for Inport Vcn_ref
#define  selector_V0S_Data_M2C_NLC_6cell_Control_Vo           0x130  //data register for Inport selector_V0S
#define  pap_Data_M2C_NLC_6cell_Control_Vo                    0x134  //data register for Inport pap
#define  pbp_Data_M2C_NLC_6cell_Control_Vo                    0x138  //data register for Inport pbp
#define  pcp_Data_M2C_NLC_6cell_Control_Vo                    0x13C  //data register for Inport pcp
#define  pan_Data_M2C_NLC_6cell_Control_Vo                    0x140  //data register for Inport pan
#define  pbn_Data_M2C_NLC_6cell_Control_Vo                    0x144  //data register for Inport pbn
#define  pcn_Data_M2C_NLC_6cell_Control_Vo                    0x148  //data register for Inport pcn

#endif /* M2C_NLC_6CELL_CONTROL_VO_H_ */
