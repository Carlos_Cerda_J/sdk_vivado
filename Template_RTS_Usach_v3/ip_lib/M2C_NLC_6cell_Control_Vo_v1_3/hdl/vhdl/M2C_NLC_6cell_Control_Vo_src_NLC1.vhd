-- -------------------------------------------------------------
-- 
-- File Name: hdl_prj\hdlsrc\M2C_NLC_6cell_Control_Vo\M2C_NLC_6cell_Control_Vo_src_NLC1.vhd
-- Created: 2021-03-20 11:22:51
-- 
-- Generated by MATLAB 9.8 and HDL Coder 3.16
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: M2C_NLC_6cell_Control_Vo_src_NLC1
-- Source Path: M2C_NLC_6cell_Control_Vo/M2C_NLC_6cell_Control_Vo/NLC1
-- Hierarchy Level: 1
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY M2C_NLC_6cell_Control_Vo_src_NLC1 IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb                               :   IN    std_logic;
        Vap_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        Vbp_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        Vcp_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        Van_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        Vbn_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        Vcn_ref                           :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
        Vdc_inv                           :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
        level_ap                          :   OUT   std_logic_vector(3 DOWNTO 0);  -- sfix4
        level_bp                          :   OUT   std_logic_vector(3 DOWNTO 0);  -- sfix4
        level_cp                          :   OUT   std_logic_vector(3 DOWNTO 0);  -- sfix4
        level_an                          :   OUT   std_logic_vector(3 DOWNTO 0);  -- sfix4
        level_bn                          :   OUT   std_logic_vector(3 DOWNTO 0);  -- sfix4
        level_cn                          :   OUT   std_logic_vector(3 DOWNTO 0)  -- sfix4
        );
END M2C_NLC_6cell_Control_Vo_src_NLC1;


ARCHITECTURE rtl OF M2C_NLC_6cell_Control_Vo_src_NLC1 IS

  -- Component Declarations
  COMPONENT M2C_NLC_6cell_Control_Vo_src_NLC_ap
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb                             :   IN    std_logic;
          ref                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          Vdc_inv                         :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
          level                           :   OUT   std_logic_vector(3 DOWNTO 0)  -- sfix4
          );
  END COMPONENT;

  COMPONENT M2C_NLC_6cell_Control_Vo_src_NLC_bp
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb                             :   IN    std_logic;
          ref                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          Vdc_inv                         :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
          level                           :   OUT   std_logic_vector(3 DOWNTO 0)  -- sfix4
          );
  END COMPONENT;

  COMPONENT M2C_NLC_6cell_Control_Vo_src_NLC_cp
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb                             :   IN    std_logic;
          ref                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          Vdc_inv                         :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
          level                           :   OUT   std_logic_vector(3 DOWNTO 0)  -- sfix4
          );
  END COMPONENT;

  COMPONENT M2C_NLC_6cell_Control_Vo_src_NLC_an
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb                             :   IN    std_logic;
          ref                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          Vdc_inv                         :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
          level                           :   OUT   std_logic_vector(3 DOWNTO 0)  -- sfix4
          );
  END COMPONENT;

  COMPONENT M2C_NLC_6cell_Control_Vo_src_NLC_bn
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb                             :   IN    std_logic;
          ref                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          Vdc_inv                         :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
          level                           :   OUT   std_logic_vector(3 DOWNTO 0)  -- sfix4
          );
  END COMPONENT;

  COMPONENT M2C_NLC_6cell_Control_Vo_src_NLC_cn
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb                             :   IN    std_logic;
          ref                             :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En13
          Vdc_inv                         :   IN    std_logic_vector(17 DOWNTO 0);  -- sfix18_En17
          level                           :   OUT   std_logic_vector(3 DOWNTO 0)  -- sfix4
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : M2C_NLC_6cell_Control_Vo_src_NLC_ap
    USE ENTITY work.M2C_NLC_6cell_Control_Vo_src_NLC_ap(rtl);

  FOR ALL : M2C_NLC_6cell_Control_Vo_src_NLC_bp
    USE ENTITY work.M2C_NLC_6cell_Control_Vo_src_NLC_bp(rtl);

  FOR ALL : M2C_NLC_6cell_Control_Vo_src_NLC_cp
    USE ENTITY work.M2C_NLC_6cell_Control_Vo_src_NLC_cp(rtl);

  FOR ALL : M2C_NLC_6cell_Control_Vo_src_NLC_an
    USE ENTITY work.M2C_NLC_6cell_Control_Vo_src_NLC_an(rtl);

  FOR ALL : M2C_NLC_6cell_Control_Vo_src_NLC_bn
    USE ENTITY work.M2C_NLC_6cell_Control_Vo_src_NLC_bn(rtl);

  FOR ALL : M2C_NLC_6cell_Control_Vo_src_NLC_cn
    USE ENTITY work.M2C_NLC_6cell_Control_Vo_src_NLC_cn(rtl);

  -- Signals
  SIGNAL NLC_ap_out1                      : std_logic_vector(3 DOWNTO 0);  -- ufix4
  SIGNAL NLC_bp_out1                      : std_logic_vector(3 DOWNTO 0);  -- ufix4
  SIGNAL NLC_cp_out1                      : std_logic_vector(3 DOWNTO 0);  -- ufix4
  SIGNAL NLC_an_out1                      : std_logic_vector(3 DOWNTO 0);  -- ufix4
  SIGNAL NLC_bn_out1                      : std_logic_vector(3 DOWNTO 0);  -- ufix4
  SIGNAL NLC_cn_out1                      : std_logic_vector(3 DOWNTO 0);  -- ufix4

BEGIN
  u_NLC_ap : M2C_NLC_6cell_Control_Vo_src_NLC_ap
    PORT MAP( clk => clk,
              reset => reset,
              enb => enb,
              ref => Vap_ref,  -- sfix25_En13
              Vdc_inv => Vdc_inv,  -- sfix18_En17
              level => NLC_ap_out1  -- sfix4
              );

  u_NLC_bp : M2C_NLC_6cell_Control_Vo_src_NLC_bp
    PORT MAP( clk => clk,
              reset => reset,
              enb => enb,
              ref => Vbp_ref,  -- sfix25_En13
              Vdc_inv => Vdc_inv,  -- sfix18_En17
              level => NLC_bp_out1  -- sfix4
              );

  u_NLC_cp : M2C_NLC_6cell_Control_Vo_src_NLC_cp
    PORT MAP( clk => clk,
              reset => reset,
              enb => enb,
              ref => Vcp_ref,  -- sfix25_En13
              Vdc_inv => Vdc_inv,  -- sfix18_En17
              level => NLC_cp_out1  -- sfix4
              );

  u_NLC_an : M2C_NLC_6cell_Control_Vo_src_NLC_an
    PORT MAP( clk => clk,
              reset => reset,
              enb => enb,
              ref => Van_ref,  -- sfix25_En13
              Vdc_inv => Vdc_inv,  -- sfix18_En17
              level => NLC_an_out1  -- sfix4
              );

  u_NLC_bn : M2C_NLC_6cell_Control_Vo_src_NLC_bn
    PORT MAP( clk => clk,
              reset => reset,
              enb => enb,
              ref => Vbn_ref,  -- sfix25_En13
              Vdc_inv => Vdc_inv,  -- sfix18_En17
              level => NLC_bn_out1  -- sfix4
              );

  u_NLC_cn : M2C_NLC_6cell_Control_Vo_src_NLC_cn
    PORT MAP( clk => clk,
              reset => reset,
              enb => enb,
              ref => Vcn_ref,  -- sfix25_En13
              Vdc_inv => Vdc_inv,  -- sfix18_En17
              level => NLC_cn_out1  -- sfix4
              );

  level_ap <= NLC_ap_out1;

  level_bp <= NLC_bp_out1;

  level_cp <= NLC_cp_out1;

  level_an <= NLC_an_out1;

  level_bn <= NLC_bn_out1;

  level_cn <= NLC_cn_out1;

END rtl;

