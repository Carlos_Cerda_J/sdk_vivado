-- -------------------------------------------------------------
-- 
-- File Name: C:\Users\Victor\Documents\matlab_ipcores\Simulink\envelope_detector\to_ARM\hdlsrc\toARM\to_ARM_addr_decoder.vhd
-- Created: 2019-11-04 11:29:24
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: to_ARM_addr_decoder
-- Source Path: to_ARM/to_ARM_axi_lite/to_ARM_addr_decoder
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY to_ARM_addr_decoder IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        data_write                        :   IN    std_logic_vector(31 DOWNTO 0);  -- ufix32
        addr_sel                          :   IN    std_logic_vector(13 DOWNTO 0);  -- ufix14
        wr_enb                            :   IN    std_logic;  -- ufix1
        rd_enb                            :   IN    std_logic;  -- ufix1
        read_ip_timestamp                 :   IN    std_logic_vector(31 DOWNTO 0);  -- ufix32
        read_alpha_S                      :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
        read_alpha_D                      :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
        read_beta_S                       :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
        read_beta_D                       :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
        data_read                         :   OUT   std_logic_vector(31 DOWNTO 0);  -- ufix32
        write_axi_enable                  :   OUT   std_logic  -- ufix1
        );
END to_ARM_addr_decoder;


ARCHITECTURE rtl OF to_ARM_addr_decoder IS

  -- Signals
  SIGNAL enb                              : std_logic;
  SIGNAL addr_sel_unsigned                : unsigned(13 DOWNTO 0);  -- ufix14
  SIGNAL decode_sel_ip_timestamp          : std_logic;  -- ufix1
  SIGNAL read_ip_timestamp_unsigned       : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL const_1                          : std_logic;  -- ufix1
  SIGNAL read_alpha_S_signed              : signed(24 DOWNTO 0);  -- sfix25_En11
  SIGNAL read_alpha_D_signed              : signed(24 DOWNTO 0);  -- sfix25_En11
  SIGNAL read_beta_S_signed               : signed(24 DOWNTO 0);  -- sfix25_En11
  SIGNAL read_beta_D_signed               : signed(24 DOWNTO 0);  -- sfix25_En11
  SIGNAL decode_sel_beta_D                : std_logic;  -- ufix1
  SIGNAL decode_sel_beta_S                : std_logic;  -- ufix1
  SIGNAL decode_sel_alpha_D               : std_logic;  -- ufix1
  SIGNAL decode_sel_alpha_S               : std_logic;  -- ufix1
  SIGNAL const_0                          : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_ip_timestamp            : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_ip_timestamp           : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_alpha_S                 : signed(24 DOWNTO 0);  -- sfix25_En11
  SIGNAL data_in_alpha_S                  : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_alpha_S                : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_alpha_D                 : signed(24 DOWNTO 0);  -- sfix25_En11
  SIGNAL data_in_alpha_D                  : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_alpha_D                : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_beta_S                  : signed(24 DOWNTO 0);  -- sfix25_En11
  SIGNAL data_in_beta_S                   : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_beta_S                 : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL read_reg_beta_D                  : signed(24 DOWNTO 0);  -- sfix25_En11
  SIGNAL data_in_beta_D                   : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_rd_beta_D                 : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL decode_sel_axi_enable            : std_logic;  -- ufix1
  SIGNAL reg_enb_axi_enable               : std_logic;  -- ufix1
  SIGNAL data_write_unsigned              : unsigned(31 DOWNTO 0);  -- ufix32
  SIGNAL data_in_axi_enable               : std_logic;  -- ufix1
  SIGNAL write_reg_axi_enable             : std_logic;  -- ufix1

BEGIN
  addr_sel_unsigned <= unsigned(addr_sel);

  
  decode_sel_ip_timestamp <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0002#, 14) ELSE
      '0';

  read_ip_timestamp_unsigned <= unsigned(read_ip_timestamp);

  const_1 <= '1';

  enb <= const_1;

  read_alpha_S_signed <= signed(read_alpha_S);

  read_alpha_D_signed <= signed(read_alpha_D);

  read_beta_S_signed <= signed(read_beta_S);

  read_beta_D_signed <= signed(read_beta_D);

  
  decode_sel_beta_D <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0043#, 14) ELSE
      '0';

  
  decode_sel_beta_S <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0042#, 14) ELSE
      '0';

  
  decode_sel_alpha_D <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0041#, 14) ELSE
      '0';

  
  decode_sel_alpha_S <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0040#, 14) ELSE
      '0';

  const_0 <= to_unsigned(0, 32);

  reg_ip_timestamp_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      read_reg_ip_timestamp <= to_unsigned(0, 32);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        read_reg_ip_timestamp <= read_ip_timestamp_unsigned;
      END IF;
    END IF;
  END PROCESS reg_ip_timestamp_process;


  
  decode_rd_ip_timestamp <= const_0 WHEN decode_sel_ip_timestamp = '0' ELSE
      read_reg_ip_timestamp;

  reg_alpha_S_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      read_reg_alpha_S <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        read_reg_alpha_S <= read_alpha_S_signed;
      END IF;
    END IF;
  END PROCESS reg_alpha_S_process;


  data_in_alpha_S <= unsigned(resize(read_reg_alpha_S, 32));

  
  decode_rd_alpha_S <= decode_rd_ip_timestamp WHEN decode_sel_alpha_S = '0' ELSE
      data_in_alpha_S;

  reg_alpha_D_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      read_reg_alpha_D <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        read_reg_alpha_D <= read_alpha_D_signed;
      END IF;
    END IF;
  END PROCESS reg_alpha_D_process;


  data_in_alpha_D <= unsigned(resize(read_reg_alpha_D, 32));

  
  decode_rd_alpha_D <= decode_rd_alpha_S WHEN decode_sel_alpha_D = '0' ELSE
      data_in_alpha_D;

  reg_beta_S_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      read_reg_beta_S <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        read_reg_beta_S <= read_beta_S_signed;
      END IF;
    END IF;
  END PROCESS reg_beta_S_process;


  data_in_beta_S <= unsigned(resize(read_reg_beta_S, 32));

  
  decode_rd_beta_S <= decode_rd_alpha_D WHEN decode_sel_beta_S = '0' ELSE
      data_in_beta_S;

  reg_beta_D_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      read_reg_beta_D <= to_signed(16#0000000#, 25);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        read_reg_beta_D <= read_beta_D_signed;
      END IF;
    END IF;
  END PROCESS reg_beta_D_process;


  data_in_beta_D <= unsigned(resize(read_reg_beta_D, 32));

  
  decode_rd_beta_D <= decode_rd_beta_S WHEN decode_sel_beta_D = '0' ELSE
      data_in_beta_D;

  data_read <= std_logic_vector(decode_rd_beta_D);

  
  decode_sel_axi_enable <= '1' WHEN addr_sel_unsigned = to_unsigned(16#0001#, 14) ELSE
      '0';

  reg_enb_axi_enable <= decode_sel_axi_enable AND wr_enb;

  data_write_unsigned <= unsigned(data_write);

  data_in_axi_enable <= data_write_unsigned(0);

  reg_axi_enable_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      write_reg_axi_enable <= '1';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND reg_enb_axi_enable = '1' THEN
        write_reg_axi_enable <= data_in_axi_enable;
      END IF;
    END IF;
  END PROCESS reg_axi_enable_process;


  write_axi_enable <= write_reg_axi_enable;

END rtl;

