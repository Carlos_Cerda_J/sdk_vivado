-- -------------------------------------------------------------
-- 
-- File Name: C:\Users\Victor\Documents\matlab_ipcores\Simulink\sort_nlc_0912\SORT_NLC_6CELDAS_OPT1\sort_6cell_0912_4.2\hdlsrc\nlc_sort_6celdas_opt1_0912\sort_3c_src_MLfcn1.vhd
-- Created: 2019-12-09 15:09:15
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: sort_3c_src_MLfcn1
-- Source Path: nlc_sort_6celdas_opt1_0912/sort_3c/sortxy1/Subsystem/MLfcn1
-- Hierarchy Level: 3
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE work.sort_3c_src_sort_3c_pkg.ALL;

ENTITY sort_3c_src_MLfcn1 IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb_1_2_0                         :   IN    std_logic;
        v1                                :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        v2                                :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        v3                                :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        v4                                :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        v5                                :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        v6                                :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En14
        icluster                          :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En16
        pos1                              :   OUT   std_logic_vector(2 DOWNTO 0);  -- ufix3
        pos2                              :   OUT   std_logic_vector(2 DOWNTO 0);  -- ufix3
        pos3                              :   OUT   std_logic_vector(2 DOWNTO 0);  -- ufix3
        pos4                              :   OUT   std_logic_vector(2 DOWNTO 0);  -- ufix3
        pos5                              :   OUT   std_logic_vector(2 DOWNTO 0);  -- ufix3
        pos6                              :   OUT   std_logic_vector(2 DOWNTO 0)  -- ufix3
        );
END sort_3c_src_MLfcn1;


ARCHITECTURE rtl OF sort_3c_src_MLfcn1 IS

  -- Constants
  CONSTANT P                              : vector_of_unsigned3(0 TO 5) := 
    (to_unsigned(16#1#, 3), to_unsigned(16#2#, 3), to_unsigned(16#3#, 3), to_unsigned(16#4#, 3),
     to_unsigned(16#5#, 3), to_unsigned(16#6#, 3));  -- ufix3 [6]

  -- Signals
  SIGNAL v1_signed                        : signed(24 DOWNTO 0);  -- sfix25_En14
  SIGNAL v2_signed                        : signed(24 DOWNTO 0);  -- sfix25_En14
  SIGNAL v3_signed                        : signed(24 DOWNTO 0);  -- sfix25_En14
  SIGNAL v4_signed                        : signed(24 DOWNTO 0);  -- sfix25_En14
  SIGNAL v5_signed                        : signed(24 DOWNTO 0);  -- sfix25_En14
  SIGNAL v6_signed                        : signed(24 DOWNTO 0);  -- sfix25_En14
  SIGNAL icluster_signed                  : signed(24 DOWNTO 0);  -- sfix25_En16
  SIGNAL pos1_tmp                         : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL pos2_tmp                         : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL pos3_tmp                         : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL pos4_tmp                         : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL pos5_tmp                         : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL pos6_tmp                         : unsigned(2 DOWNTO 0);  -- ufix3
  SIGNAL vc1_not_empty                    : std_logic;
  SIGNAL vc2_not_empty                    : std_logic;
  SIGNAL vc3_not_empty                    : std_logic;
  SIGNAL vc4_not_empty                    : std_logic;
  SIGNAL vc5_not_empty                    : std_logic;
  SIGNAL vc6_not_empty                    : std_logic;
  SIGNAL B_not_empty                      : std_logic;
  SIGNAL iclus_not_empty                  : std_logic;
  SIGNAL vc1_not_empty_next               : std_logic;
  SIGNAL vc2_not_empty_next               : std_logic;
  SIGNAL vc3_not_empty_next               : std_logic;
  SIGNAL vc4_not_empty_next               : std_logic;
  SIGNAL vc5_not_empty_next               : std_logic;
  SIGNAL vc6_not_empty_next               : std_logic;
  SIGNAL B_not_empty_next                 : std_logic;
  SIGNAL iclus_not_empty_next             : std_logic;

BEGIN
  v1_signed <= signed(v1);

  v2_signed <= signed(v2);

  v3_signed <= signed(v3);

  v4_signed <= signed(v4);

  v5_signed <= signed(v5);

  v6_signed <= signed(v6);

  icluster_signed <= signed(icluster);

  MLfcn1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        vc1_not_empty <= '0';
        vc2_not_empty <= '0';
        vc3_not_empty <= '0';
        vc4_not_empty <= '0';
        vc5_not_empty <= '0';
        vc6_not_empty <= '0';
        B_not_empty <= '0';
        iclus_not_empty <= '0';
      ELSIF enb_1_2_0 = '1' THEN
        vc1_not_empty <= vc1_not_empty_next;
        vc2_not_empty <= vc2_not_empty_next;
        vc3_not_empty <= vc3_not_empty_next;
        vc4_not_empty <= vc4_not_empty_next;
        vc5_not_empty <= vc5_not_empty_next;
        vc6_not_empty <= vc6_not_empty_next;
        B_not_empty <= B_not_empty_next;
        iclus_not_empty <= iclus_not_empty_next;
      END IF;
    END IF;
  END PROCESS MLfcn1_process;

  MLfcn1_output : PROCESS (v1_signed, v2_signed, v3_signed, v4_signed, v5_signed, v6_signed,
       icluster_signed, vc1_not_empty, vc2_not_empty, vc3_not_empty,
       vc4_not_empty, vc5_not_empty, vc6_not_empty, B_not_empty,
       iclus_not_empty)
    VARIABLE B : vector_of_signed25(0 TO 5);
    VARIABLE p_0 : vector_of_unsigned3(0 TO 5);
    VARIABLE swap_B : signed(24 DOWNTO 0);
    VARIABLE swap_P : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_0 : signed(24 DOWNTO 0);
    VARIABLE swap_b_1 : signed(24 DOWNTO 0);
    VARIABLE swap_p_0 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_2 : signed(24 DOWNTO 0);
    VARIABLE swap_p_1 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_3 : signed(24 DOWNTO 0);
    VARIABLE swap_p_2 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_4 : signed(24 DOWNTO 0);
    VARIABLE swap_p_3 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_5 : signed(24 DOWNTO 0);
    VARIABLE swap_p_4 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_6 : signed(24 DOWNTO 0);
    VARIABLE swap_p_5 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_7 : signed(24 DOWNTO 0);
    VARIABLE swap_p_6 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_8 : signed(24 DOWNTO 0);
    VARIABLE swap_p_7 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_9 : signed(24 DOWNTO 0);
    VARIABLE swap_p_8 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_10 : signed(24 DOWNTO 0);
    VARIABLE swap_p_9 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_11 : signed(24 DOWNTO 0);
    VARIABLE swap_p_10 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_12 : signed(24 DOWNTO 0);
    VARIABLE swap_p_11 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_13 : signed(24 DOWNTO 0);
    VARIABLE swap_p_12 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_14 : signed(24 DOWNTO 0);
    VARIABLE swap_p_13 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_15 : signed(24 DOWNTO 0);
    VARIABLE swap_p_14 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_16 : signed(24 DOWNTO 0);
    VARIABLE swap_p_15 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_17 : signed(24 DOWNTO 0);
    VARIABLE swap_p_16 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_18 : signed(24 DOWNTO 0);
    VARIABLE swap_p_17 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_19 : signed(24 DOWNTO 0);
    VARIABLE swap_p_18 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_20 : signed(24 DOWNTO 0);
    VARIABLE swap_p_19 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_21 : signed(24 DOWNTO 0);
    VARIABLE swap_p_20 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_22 : signed(24 DOWNTO 0);
    VARIABLE swap_p_21 : unsigned(2 DOWNTO 0);
    VARIABLE swap_p_22 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_23 : signed(24 DOWNTO 0);
    VARIABLE swap_b_24 : signed(24 DOWNTO 0);
    VARIABLE swap_p_23 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_25 : signed(24 DOWNTO 0);
    VARIABLE swap_p_24 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_26 : signed(24 DOWNTO 0);
    VARIABLE swap_p_25 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_27 : signed(24 DOWNTO 0);
    VARIABLE swap_p_26 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_28 : signed(24 DOWNTO 0);
    VARIABLE swap_p_27 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_29 : signed(24 DOWNTO 0);
    VARIABLE swap_p_28 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_30 : signed(24 DOWNTO 0);
    VARIABLE swap_p_29 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_31 : signed(24 DOWNTO 0);
    VARIABLE swap_p_30 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_32 : signed(24 DOWNTO 0);
    VARIABLE swap_p_31 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_33 : signed(24 DOWNTO 0);
    VARIABLE swap_p_32 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_34 : signed(24 DOWNTO 0);
    VARIABLE swap_p_33 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_35 : signed(24 DOWNTO 0);
    VARIABLE swap_p_34 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_36 : signed(24 DOWNTO 0);
    VARIABLE swap_p_35 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_37 : signed(24 DOWNTO 0);
    VARIABLE swap_p_36 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_38 : signed(24 DOWNTO 0);
    VARIABLE swap_p_37 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_39 : signed(24 DOWNTO 0);
    VARIABLE swap_p_38 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_40 : signed(24 DOWNTO 0);
    VARIABLE swap_p_39 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_41 : signed(24 DOWNTO 0);
    VARIABLE swap_p_40 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_42 : signed(24 DOWNTO 0);
    VARIABLE swap_p_41 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_43 : signed(24 DOWNTO 0);
    VARIABLE swap_p_42 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_44 : signed(24 DOWNTO 0);
    VARIABLE swap_p_43 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_45 : signed(24 DOWNTO 0);
    VARIABLE swap_p_44 : unsigned(2 DOWNTO 0);
    VARIABLE swap_b_46 : signed(24 DOWNTO 0);
    VARIABLE swap_p_45 : unsigned(2 DOWNTO 0);
    VARIABLE swap_p_46 : unsigned(2 DOWNTO 0);
  BEGIN
    vc1_not_empty_next <= vc1_not_empty;
    vc2_not_empty_next <= vc2_not_empty;
    vc3_not_empty_next <= vc3_not_empty;
    vc4_not_empty_next <= vc4_not_empty;
    vc5_not_empty_next <= vc5_not_empty;
    vc6_not_empty_next <= vc6_not_empty;
    B_not_empty_next <= B_not_empty;
    iclus_not_empty_next <= iclus_not_empty;
    --MATLAB Function 'sort_3c/sortxy1/Subsystem/MLfcn1'
    --declara v1,v2 y vectores v y pos como persistentes
    IF ( NOT vc1_not_empty) = '1' THEN 
      vc1_not_empty_next <= '1';
    END IF;
    IF ( NOT vc2_not_empty) = '1' THEN 
      vc2_not_empty_next <= '1';
    END IF;
    IF ( NOT vc3_not_empty) = '1' THEN 
      vc3_not_empty_next <= '1';
    END IF;
    IF ( NOT vc4_not_empty) = '1' THEN 
      vc4_not_empty_next <= '1';
    END IF;
    IF ( NOT vc5_not_empty) = '1' THEN 
      vc5_not_empty_next <= '1';
    END IF;
    IF ( NOT vc6_not_empty) = '1' THEN 
      vc6_not_empty_next <= '1';
    END IF;
    IF ( NOT B_not_empty) = '1' THEN 
      B_not_empty_next <= '1';
    END IF;
    IF ( NOT iclus_not_empty) = '1' THEN 
      iclus_not_empty_next <= '1';
    END IF;
    B(0) := v1_signed;
    B(1) := v2_signed;
    B(2) := v3_signed;
    B(3) := v4_signed;
    B(4) := v5_signed;
    B(5) := v6_signed;
    p_0 := P;
    IF icluster_signed > to_signed(16#0000000#, 25) THEN 
      --ordenar de menor a mayor    
      -- primera iteracion
      IF B(0) > B(1) THEN 
        swap_b_23 := B(0);
        B(0) := B(1);
        p_0(0) := to_unsigned(16#2#, 3);
        B(1) := swap_b_23;
        p_0(1) := to_unsigned(16#1#, 3);
      END IF;
      IF B(1) > B(2) THEN 
        swap_b_24 := B(1);
        swap_p_23 := p_0(1);
        B(1) := B(2);
        p_0(1) := to_unsigned(16#3#, 3);
        B(2) := swap_b_24;
        p_0(2) := swap_p_23;
      END IF;
      IF B(2) > B(3) THEN 
        swap_b_25 := B(2);
        swap_p_24 := p_0(2);
        B(2) := B(3);
        p_0(2) := to_unsigned(16#4#, 3);
        B(3) := swap_b_25;
        p_0(3) := swap_p_24;
      END IF;
      IF B(3) > B(4) THEN 
        swap_b_26 := B(3);
        swap_p_25 := p_0(3);
        B(3) := B(4);
        p_0(3) := to_unsigned(16#5#, 3);
        B(4) := swap_b_26;
        p_0(4) := swap_p_25;
      END IF;
      IF B(4) > B(5) THEN 
        swap_b_27 := B(4);
        swap_p_26 := p_0(4);
        B(4) := B(5);
        p_0(4) := to_unsigned(16#6#, 3);
        B(5) := swap_b_27;
        p_0(5) := swap_p_26;
      END IF;
      -- segunda iteracion   
      IF B(0) > B(1) THEN 
        swap_b_28 := B(0);
        swap_p_27 := p_0(0);
        B(0) := B(1);
        p_0(0) := p_0(1);
        B(1) := swap_b_28;
        p_0(1) := swap_p_27;
      END IF;
      IF B(1) > B(2) THEN 
        swap_b_29 := B(1);
        swap_p_28 := p_0(1);
        B(1) := B(2);
        p_0(1) := p_0(2);
        B(2) := swap_b_29;
        p_0(2) := swap_p_28;
      END IF;
      IF B(2) > B(3) THEN 
        swap_b_30 := B(2);
        swap_p_29 := p_0(2);
        B(2) := B(3);
        p_0(2) := p_0(3);
        B(3) := swap_b_30;
        p_0(3) := swap_p_29;
      END IF;
      IF B(3) > B(4) THEN 
        swap_b_31 := B(3);
        swap_p_30 := p_0(3);
        B(3) := B(4);
        p_0(3) := p_0(4);
        B(4) := swap_b_31;
        p_0(4) := swap_p_30;
      END IF;
      IF B(4) > B(5) THEN 
        swap_b_32 := B(4);
        swap_p_31 := p_0(4);
        B(4) := B(5);
        p_0(4) := p_0(5);
        B(5) := swap_b_32;
        p_0(5) := swap_p_31;
      END IF;
      -- tercera iteracion
      IF B(0) > B(1) THEN 
        swap_b_33 := B(0);
        swap_p_32 := p_0(0);
        B(0) := B(1);
        p_0(0) := p_0(1);
        B(1) := swap_b_33;
        p_0(1) := swap_p_32;
      END IF;
      IF B(1) > B(2) THEN 
        swap_b_34 := B(1);
        swap_p_33 := p_0(1);
        B(1) := B(2);
        p_0(1) := p_0(2);
        B(2) := swap_b_34;
        p_0(2) := swap_p_33;
      END IF;
      IF B(2) > B(3) THEN 
        swap_b_35 := B(2);
        swap_p_34 := p_0(2);
        B(2) := B(3);
        p_0(2) := p_0(3);
        B(3) := swap_b_35;
        p_0(3) := swap_p_34;
      END IF;
      IF B(3) > B(4) THEN 
        swap_b_36 := B(3);
        swap_p_35 := p_0(3);
        B(3) := B(4);
        p_0(3) := p_0(4);
        B(4) := swap_b_36;
        p_0(4) := swap_p_35;
      END IF;
      IF B(4) > B(5) THEN 
        swap_b_37 := B(4);
        swap_p_36 := p_0(4);
        B(4) := B(5);
        p_0(4) := p_0(5);
        B(5) := swap_b_37;
        p_0(5) := swap_p_36;
      END IF;
      -- cuarta iteracion
      IF B(0) > B(1) THEN 
        swap_b_38 := B(0);
        swap_p_37 := p_0(0);
        B(0) := B(1);
        p_0(0) := p_0(1);
        B(1) := swap_b_38;
        p_0(1) := swap_p_37;
      END IF;
      IF B(1) > B(2) THEN 
        swap_b_39 := B(1);
        swap_p_38 := p_0(1);
        B(1) := B(2);
        p_0(1) := p_0(2);
        B(2) := swap_b_39;
        p_0(2) := swap_p_38;
      END IF;
      IF B(2) > B(3) THEN 
        swap_b_40 := B(2);
        swap_p_39 := p_0(2);
        B(2) := B(3);
        p_0(2) := p_0(3);
        B(3) := swap_b_40;
        p_0(3) := swap_p_39;
      END IF;
      IF B(3) > B(4) THEN 
        swap_b_41 := B(3);
        swap_p_40 := p_0(3);
        B(3) := B(4);
        p_0(3) := p_0(4);
        B(4) := swap_b_41;
        p_0(4) := swap_p_40;
      END IF;
      IF B(4) > B(5) THEN 
        swap_b_42 := B(4);
        swap_p_41 := p_0(4);
        B(4) := B(5);
        p_0(4) := p_0(5);
        B(5) := swap_b_42;
        p_0(5) := swap_p_41;
      END IF;
      -- quinta iteracion
      IF B(0) > B(1) THEN 
        swap_b_43 := B(0);
        swap_p_42 := p_0(0);
        p_0(0) := p_0(1);
        B(1) := swap_b_43;
        p_0(1) := swap_p_42;
      END IF;
      IF B(1) > B(2) THEN 
        swap_b_44 := B(1);
        swap_p_43 := p_0(1);
        p_0(1) := p_0(2);
        B(2) := swap_b_44;
        p_0(2) := swap_p_43;
      END IF;
      IF B(2) > B(3) THEN 
        swap_b_45 := B(2);
        swap_p_44 := p_0(2);
        p_0(2) := p_0(3);
        B(3) := swap_b_45;
        p_0(3) := swap_p_44;
      END IF;
      IF B(3) > B(4) THEN 
        swap_b_46 := B(3);
        swap_p_45 := p_0(3);
        p_0(3) := p_0(4);
        B(4) := swap_b_46;
        p_0(4) := swap_p_45;
      END IF;
      IF B(4) > B(5) THEN 
        swap_p_46 := p_0(4);
        p_0(4) := p_0(5);
        p_0(5) := swap_p_46;
      END IF;
    ELSE 
      --ordenar de mayor a menor
      -- primera iteracion
      IF B(0) < B(1) THEN 
        swap_B := B(0);
        B(0) := B(1);
        p_0(0) := to_unsigned(16#2#, 3);
        B(1) := swap_B;
        p_0(1) := to_unsigned(16#1#, 3);
      END IF;
      IF B(1) < B(2) THEN 
        swap_b_0 := B(1);
        swap_P := p_0(1);
        B(1) := B(2);
        p_0(1) := to_unsigned(16#3#, 3);
        B(2) := swap_b_0;
        p_0(2) := swap_P;
      END IF;
      IF B(2) < B(3) THEN 
        swap_b_1 := B(2);
        swap_p_0 := p_0(2);
        B(2) := B(3);
        p_0(2) := to_unsigned(16#4#, 3);
        B(3) := swap_b_1;
        p_0(3) := swap_p_0;
      END IF;
      IF B(3) < B(4) THEN 
        swap_b_2 := B(3);
        swap_p_1 := p_0(3);
        B(3) := B(4);
        p_0(3) := to_unsigned(16#5#, 3);
        B(4) := swap_b_2;
        p_0(4) := swap_p_1;
      END IF;
      IF B(4) < B(5) THEN 
        swap_b_3 := B(4);
        swap_p_2 := p_0(4);
        B(4) := B(5);
        p_0(4) := to_unsigned(16#6#, 3);
        B(5) := swap_b_3;
        p_0(5) := swap_p_2;
      END IF;
      -- segunda iteracion   
      IF B(0) < B(1) THEN 
        swap_b_4 := B(0);
        swap_p_3 := p_0(0);
        B(0) := B(1);
        p_0(0) := p_0(1);
        B(1) := swap_b_4;
        p_0(1) := swap_p_3;
      END IF;
      IF B(1) < B(2) THEN 
        swap_b_5 := B(1);
        swap_p_4 := p_0(1);
        B(1) := B(2);
        p_0(1) := p_0(2);
        B(2) := swap_b_5;
        p_0(2) := swap_p_4;
      END IF;
      IF B(2) < B(3) THEN 
        swap_b_6 := B(2);
        swap_p_5 := p_0(2);
        B(2) := B(3);
        p_0(2) := p_0(3);
        B(3) := swap_b_6;
        p_0(3) := swap_p_5;
      END IF;
      IF B(3) < B(4) THEN 
        swap_b_7 := B(3);
        swap_p_6 := p_0(3);
        B(3) := B(4);
        p_0(3) := p_0(4);
        B(4) := swap_b_7;
        p_0(4) := swap_p_6;
      END IF;
      IF B(4) < B(5) THEN 
        swap_b_8 := B(4);
        swap_p_7 := p_0(4);
        B(4) := B(5);
        p_0(4) := p_0(5);
        B(5) := swap_b_8;
        p_0(5) := swap_p_7;
      END IF;
      -- tercera iteracion
      IF B(0) < B(1) THEN 
        swap_b_9 := B(0);
        swap_p_8 := p_0(0);
        B(0) := B(1);
        p_0(0) := p_0(1);
        B(1) := swap_b_9;
        p_0(1) := swap_p_8;
      END IF;
      IF B(1) < B(2) THEN 
        swap_b_10 := B(1);
        swap_p_9 := p_0(1);
        B(1) := B(2);
        p_0(1) := p_0(2);
        B(2) := swap_b_10;
        p_0(2) := swap_p_9;
      END IF;
      IF B(2) < B(3) THEN 
        swap_b_11 := B(2);
        swap_p_10 := p_0(2);
        B(2) := B(3);
        p_0(2) := p_0(3);
        B(3) := swap_b_11;
        p_0(3) := swap_p_10;
      END IF;
      IF B(3) < B(4) THEN 
        swap_b_12 := B(3);
        swap_p_11 := p_0(3);
        B(3) := B(4);
        p_0(3) := p_0(4);
        B(4) := swap_b_12;
        p_0(4) := swap_p_11;
      END IF;
      IF B(4) < B(5) THEN 
        swap_b_13 := B(4);
        swap_p_12 := p_0(4);
        B(4) := B(5);
        p_0(4) := p_0(5);
        B(5) := swap_b_13;
        p_0(5) := swap_p_12;
      END IF;
      -- cuarta iteracion
      IF B(0) < B(1) THEN 
        swap_b_14 := B(0);
        swap_p_13 := p_0(0);
        B(0) := B(1);
        p_0(0) := p_0(1);
        B(1) := swap_b_14;
        p_0(1) := swap_p_13;
      END IF;
      IF B(1) < B(2) THEN 
        swap_b_15 := B(1);
        swap_p_14 := p_0(1);
        B(1) := B(2);
        p_0(1) := p_0(2);
        B(2) := swap_b_15;
        p_0(2) := swap_p_14;
      END IF;
      IF B(2) < B(3) THEN 
        swap_b_16 := B(2);
        swap_p_15 := p_0(2);
        B(2) := B(3);
        p_0(2) := p_0(3);
        B(3) := swap_b_16;
        p_0(3) := swap_p_15;
      END IF;
      IF B(3) < B(4) THEN 
        swap_b_17 := B(3);
        swap_p_16 := p_0(3);
        B(3) := B(4);
        p_0(3) := p_0(4);
        B(4) := swap_b_17;
        p_0(4) := swap_p_16;
      END IF;
      IF B(4) < B(5) THEN 
        swap_b_18 := B(4);
        swap_p_17 := p_0(4);
        B(4) := B(5);
        p_0(4) := p_0(5);
        B(5) := swap_b_18;
        p_0(5) := swap_p_17;
      END IF;
      -- quinta iteracion
      IF B(0) < B(1) THEN 
        swap_b_19 := B(0);
        swap_p_18 := p_0(0);
        p_0(0) := p_0(1);
        B(1) := swap_b_19;
        p_0(1) := swap_p_18;
      END IF;
      IF B(1) < B(2) THEN 
        swap_b_20 := B(1);
        swap_p_19 := p_0(1);
        p_0(1) := p_0(2);
        B(2) := swap_b_20;
        p_0(2) := swap_p_19;
      END IF;
      IF B(2) < B(3) THEN 
        swap_b_21 := B(2);
        swap_p_20 := p_0(2);
        p_0(2) := p_0(3);
        B(3) := swap_b_21;
        p_0(3) := swap_p_20;
      END IF;
      IF B(3) < B(4) THEN 
        swap_b_22 := B(3);
        swap_p_21 := p_0(3);
        p_0(3) := p_0(4);
        B(4) := swap_b_22;
        p_0(4) := swap_p_21;
      END IF;
      IF B(4) < B(5) THEN 
        swap_p_22 := p_0(4);
        p_0(4) := p_0(5);
        p_0(5) := swap_p_22;
      END IF;
    END IF;
    pos1_tmp <= p_0(0);
    pos2_tmp <= p_0(1);
    pos3_tmp <= p_0(2);
    pos4_tmp <= p_0(3);
    pos5_tmp <= p_0(4);
    pos6_tmp <= p_0(5);
  END PROCESS MLfcn1_output;


  pos1 <= std_logic_vector(pos1_tmp);

  pos2 <= std_logic_vector(pos2_tmp);

  pos3 <= std_logic_vector(pos3_tmp);

  pos4 <= std_logic_vector(pos4_tmp);

  pos5 <= std_logic_vector(pos5_tmp);

  pos6 <= std_logic_vector(pos6_tmp);

END rtl;

