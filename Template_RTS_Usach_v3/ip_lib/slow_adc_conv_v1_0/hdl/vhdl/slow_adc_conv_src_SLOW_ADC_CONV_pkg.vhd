-- -------------------------------------------------------------
-- 
-- File Name: D:\xilinx\IPCORES\slow_adc_conv\hdlsrc\trafo_slowadc\slow_adc_conv_src_SLOW_ADC_CONV_pkg.vhd
-- Created: 2019-01-20 21:54:46
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

PACKAGE slow_adc_conv_src_SLOW_ADC_CONV_pkg IS
  TYPE vector_of_signed25 IS ARRAY (NATURAL RANGE <>) OF signed(24 DOWNTO 0);
END slow_adc_conv_src_SLOW_ADC_CONV_pkg;

