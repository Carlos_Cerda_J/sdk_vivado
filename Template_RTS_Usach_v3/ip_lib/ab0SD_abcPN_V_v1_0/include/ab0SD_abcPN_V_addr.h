/*
 * File Name:         E:\xilinx\IPCORES\AB0SD_ABCPN_V\ipcore\ab0SD_abcPN_V_v1_0\include\ab0SD_abcPN_V_addr.h
 * Description:       C Header File
 * Created:           2019-01-28 12:16:49
*/

#ifndef AB0SD_ABCPN_V_H_
#define AB0SD_ABCPN_V_H_

#define  IPCore_Reset_ab0SD_abcPN_V       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_ab0SD_abcPN_V      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_ab0SD_abcPN_V   0x8  //contains unique IP timestamp (yymmddHHMM): 1901281216

#endif /* AB0SD_ABCPN_V_H_ */
