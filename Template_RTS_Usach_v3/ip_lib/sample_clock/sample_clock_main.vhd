----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.11.2018 09:16:14
-- Design Name: 
-- Module Name: slow_adc_main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sample_clock_main is
	GENERIC
	(
		clk_div : INTEGER := 2000000          --system clock cycles per 1/2 period of sclk
	);
	Port
	(
		clk : in STD_LOGIC;
		reset_n : in STD_LOGIC;
		sample : out STD_LOGIC);
		
end sample_clock_main;


architecture Behavioral of sample_clock_main is

	SIGNAL	clk_S	:		STD_LOGIC;
	SIGNAL	counter	:		INTEGER RANGE 1 TO 5000000 :=1;
	
BEGIN
	frecuency_divider: PROCESS(clk)
	BEGIN
		IF reset_n = '0' THEN
			clk_S <= '0';
			counter <= 1;
		ELSIF RISING_EDGE(clk) THEN
			IF counter = clk_div then
				clk_S <= '1';
				counter <= 1;
			ELSE
				clk_S <= '0';
				counter <= counter + 1;
			END IF;
		END IF;
	END PROCESS frecuency_divider;

	sample <= clk_S;

end Behavioral;
