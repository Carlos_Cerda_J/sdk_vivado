-- -------------------------------------------------------------
-- 
-- File Name: hdl_prj\hdlsrc\FirstHDL\PWM_IPCOR_ref_ip_dut.vhd
-- Created: 2019-01-03 10:14:38
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: PWM_IPCOR_ref_ip_dut
-- Source Path: PWM_IPCOR_ref_ip/PWM_IPCOR_ref_ip_dut
-- Hierarchy Level: 1
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY PWM_IPCOR_ref_ip_dut IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        dut_enable                        :   IN    std_logic;  -- ufix1
        En                                :   IN    std_logic;  -- ufix1
        Max_point                         :   IN    std_logic_vector(31 DOWNTO 0);  -- sfix32_En18
        referencia                        :   IN    std_logic_vector(31 DOWNTO 0);  -- sfix32_En18
        ce_out                            :   OUT   std_logic;  -- ufix1
        PWM1                              :   OUT   std_logic;  -- ufix1
        PWM2                              :   OUT   std_logic  -- ufix1
        );
END PWM_IPCOR_ref_ip_dut;


ARCHITECTURE rtl OF PWM_IPCOR_ref_ip_dut IS

  -- Component Declarations
  COMPONENT PWM_IPCOR_ref_ip_src_PWM_IPCORE_ref
    PORT( clk                             :   IN    std_logic;
          clk_enable                      :   IN    std_logic;
          reset                           :   IN    std_logic;
          En                              :   IN    std_logic;  -- ufix1
          Max_point                       :   IN    std_logic_vector(31 DOWNTO 0);  -- sfix32_En18
          referencia                      :   IN    std_logic_vector(31 DOWNTO 0);  -- sfix32_En18
          ce_out                          :   OUT   std_logic;  -- ufix1
          PWM1                            :   OUT   std_logic;  -- ufix1
          PWM2                            :   OUT   std_logic  -- ufix1
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : PWM_IPCOR_ref_ip_src_PWM_IPCORE_ref
    USE ENTITY work.PWM_IPCOR_ref_ip_src_PWM_IPCORE_ref(rtl);

  -- Signals
  SIGNAL enb                              : std_logic;
  SIGNAL En_sig                           : std_logic;  -- ufix1
  SIGNAL ce_out_sig                       : std_logic;  -- ufix1
  SIGNAL PWM1_sig                         : std_logic;  -- ufix1
  SIGNAL PWM2_sig                         : std_logic;  -- ufix1

BEGIN
  u_PWM_IPCOR_ref_ip_src_PWM_IPCORE_ref : PWM_IPCOR_ref_ip_src_PWM_IPCORE_ref
    PORT MAP( clk => clk,
              clk_enable => enb,
              reset => reset,
              En => En_sig,  -- ufix1
              Max_point => Max_point,  -- sfix32_En18
              referencia => referencia,  -- sfix32_En18
              ce_out => ce_out_sig,  -- ufix1
              PWM1 => PWM1_sig,  -- ufix1
              PWM2 => PWM2_sig  -- ufix1
              );

  En_sig <= En;

  enb <= dut_enable;

  ce_out <= ce_out_sig;

  PWM1 <= PWM1_sig;

  PWM2 <= PWM2_sig;

END rtl;

