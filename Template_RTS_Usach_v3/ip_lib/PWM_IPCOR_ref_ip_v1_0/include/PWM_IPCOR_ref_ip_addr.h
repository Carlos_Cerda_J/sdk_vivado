/*
 * File Name:         hdl_prj\ipcore\PWM_IPCOR_ref_ip_v1_0\include\PWM_IPCOR_ref_ip_addr.h
 * Description:       C Header File
 * Created:           2019-01-03 10:14:38
*/

#ifndef PWM_IPCOR_REF_IP_H_
#define PWM_IPCOR_REF_IP_H_

#define  IPCore_Reset_PWM_IPCOR_ref_ip       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PWM_IPCOR_ref_ip      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PWM_IPCOR_ref_ip   0x8  //contains unique IP timestamp (yymmddHHMM): 1901031014
#define  Max_point_Data_PWM_IPCOR_ref_ip     0x100  //data register for Inport Max_point
#define  referencia_Data_PWM_IPCOR_ref_ip    0x104  //data register for Inport referencia

#endif /* PWM_IPCOR_REF_IP_H_ */
