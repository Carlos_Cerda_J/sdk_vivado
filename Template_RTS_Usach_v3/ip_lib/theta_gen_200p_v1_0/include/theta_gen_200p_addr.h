/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\PR\theta_gen_200p\ipcore\theta_gen_200p_v1_0\include\theta_gen_200p_addr.h
 * Description:       C Header File
 * Created:           2019-07-09 10:59:22
*/

#ifndef THETA_GEN_200P_H_
#define THETA_GEN_200P_H_

#define  IPCore_Reset_theta_gen_200p       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_theta_gen_200p      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_theta_gen_200p   0x8  //contains unique IP timestamp (yymmddHHMM): 1907091059

#endif /* THETA_GEN_200P_H_ */
