----------------------------------------------------------------------------------
-- Company: E2Tech
-- Engineer: David Arancibia G.
-- 
-- Create Date: 03/09/2020 11:44:11 AM
-- Design Name: average_16_samples_fast_adc
-- Module Name: average_16_samples_fast_adc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision: 1.0
-- Revision 0.01 - File Created
-- Additional Comments:
-- David Arancibia G.
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity average_16_samples_fast_adc is
    Port ( 
		clock	  :	in std_logic;			--Clock signal
		reset_n   :	in std_logic;			--Reset signal
		band	  : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_1 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_2 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_3 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_4 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_5 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_6 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_7 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_8 : in STD_LOGIC_VECTOR (11 downto 0);
		data_in_9 : in STD_LOGIC_VECTOR (11 downto 0);
		data_out_1 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_2 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_3 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_4 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_5 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_6 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_7 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_8 : out STD_LOGIC_VECTOR (11 downto 0);
		data_out_9 : out STD_LOGIC_VECTOR (11 downto 0);
		new_value	:		out std_logic              			--new valid value flag
		);
end average_16_samples_fast_adc;

architecture Behavioral of average_16_samples_fast_adc is
	signal data_avg_1_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_2_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_3_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_4_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_5_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_6_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_7_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_8_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_avg_9_S 	: std_logic_vector (11 downto 0) := (others => '0');
	
	signal data_clean_1_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_2_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_3_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_4_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_5_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_6_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_7_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_8_S 	: std_logic_vector (11 downto 0) := (others => '0');
	signal data_clean_9_S 	: std_logic_vector (11 downto 0) := (others => '0');

	--Declare average_16_samples_unsigned componet

	component average_16_samples_unsigned is
	port(
		clock:			in std_logic := '0';			--Clock signal
		reset_n:		in std_logic := '0';			--Reset signal		
		data_in:		in std_logic_vector(11 downto 0) := (others => '0');			--Input data for the current clock cycle
		data_out:		out std_logic_vector(11 downto 0) := (others => '0');			--Current output data (valid for 16 clock cycles)
		new_value:		out std_logic := '0'                                 			--new valid value flag
		);
	end component average_16_samples_unsigned;

	component spike_mitigation_unsigned is
		Port ( 
		clock : in STD_LOGIC;
	    reset_n : in STD_LOGIC;
	    data_in : in STD_LOGIC_VECTOR (11 downto 0);
	    data_avg : in STD_LOGIC_VECTOR (11 downto 0);
	    band	 : in STD_LOGIC_VECTOR (11 downto 0);
	    data_out : out STD_LOGIC_VECTOR (11 downto 0));
	end component spike_mitigation_unsigned;
	
begin
	data_out_1 <= data_avg_1_S;
	data_out_2 <= data_avg_2_S;
	data_out_3 <= data_avg_3_S;
	data_out_4 <= data_avg_4_S;
	data_out_5 <= data_avg_5_S;
	data_out_6 <= data_avg_6_S;
	data_out_7 <= data_avg_7_S;
	data_out_8 <= data_avg_8_S;
	data_out_9 <= data_avg_9_S;

	--Instantiate spike mitigation componets
		spike_1 : spike_mitigation_unsigned
		Port map( 
			clock	 	=> clock,
			reset_n 	=> reset_n,
			data_in 	=> data_in_1,
			data_avg 	=> data_avg_1_S,
			band	 	=> band,
			data_out 	=> data_clean_1_S
		);

		spike_2 : spike_mitigation_unsigned
		Port map( 
			clock	 	=> clock,
			reset_n 	=> reset_n,
			data_in 	=> data_in_2,
			data_avg 	=> data_avg_2_S,
			band	 	=> band,
			data_out 	=> data_clean_2_S
		);

		spike_3 : spike_mitigation_unsigned
		Port map( 
			clock	 	=> clock,
			reset_n 	=> reset_n,
			data_in 	=> data_in_3,
			data_avg 	=> data_avg_3_S,
			band	 	=> band,
			data_out 	=> data_clean_3_S
		);

		spike_4 : spike_mitigation_unsigned
		Port map( 
			clock	 	=> clock,
			reset_n 	=> reset_n,
			data_in 	=> data_in_4,
			data_avg 	=> data_avg_4_S,
			band	 	=> band,
			data_out 	=> data_clean_4_S
		);

		spike_5 : spike_mitigation_unsigned
		Port map( 
			clock	 	=> clock,
			reset_n 	=> reset_n,
			data_in 	=> data_in_5,
			data_avg 	=> data_avg_5_S,
			band	 	=> band,
			data_out 	=> data_clean_5_S
		);

		spike_6 : spike_mitigation_unsigned
		Port map( 
			clock	 	=> clock,
			reset_n 	=> reset_n,
			data_in 	=> data_in_6,
			data_avg 	=> data_avg_6_S,
			band	 	=> band,
			data_out 	=> data_clean_6_S
		);

		spike_7 : spike_mitigation_unsigned
		Port map( 
			clock	 	=> clock,
			reset_n 	=> reset_n,
			data_in 	=> data_in_7,
			data_avg 	=> data_avg_7_S,
			band	 	=> band,
			data_out 	=> data_clean_7_S
		);

		spike_8 : spike_mitigation_unsigned
		Port map( 
			clock	 	=> clock,
			reset_n 	=> reset_n,
			data_in 	=> data_in_8,
			data_avg 	=> data_avg_8_S,
			band	 	=> band,
			data_out 	=> data_clean_8_S
		);

		spike_9 : spike_mitigation_unsigned
		Port map( 
			clock	 	=> clock,
			reset_n 	=> reset_n,
			data_in 	=> data_in_9,
			data_avg 	=> data_avg_9_S,
			band	 	=> band,
			data_out 	=> data_clean_9_S
		);


	--Instantiate average_16_samples_unsigned componets
		average_1 : average_16_samples_unsigned 
		PORT MAP
		(
			clock		=> clock,
			reset_n		=> reset_n,
			data_in		=> data_clean_1_S,
			data_out	=> data_avg_1_S,--data_out_1,
			new_value	=> new_value
		 );

		average_2 : average_16_samples_unsigned 
		PORT MAP
		(
			clock		=> clock,
			reset_n		=> reset_n,
			data_in		=> data_clean_2_S,
			data_out	=> data_avg_2_S
		 );
		 
		average_3 : average_16_samples_unsigned 
		PORT MAP
		(
			clock		=> clock,
			reset_n		=> reset_n,
			data_in		=> data_clean_3_S,
			data_out	=> data_avg_3_S
		 );	 
		 
		average_4 : average_16_samples_unsigned 
		PORT MAP
		(
			clock		=> clock,
			reset_n		=> reset_n,
			data_in		=> data_clean_4_S,
			data_out	=> data_avg_4_S
		 );	 
		 
		average_5 : average_16_samples_unsigned 
		PORT MAP
		(
			clock		=> clock,
			reset_n		=> reset_n,
			data_in		=> data_clean_5_S,
			data_out	=> data_avg_5_S
		 );	 
		 
		average_6 : average_16_samples_unsigned 
		PORT MAP
		(
			clock		=> clock,
			reset_n		=> reset_n,
			data_in		=> data_clean_6_S,
			data_out	=> data_avg_6_S
		 );	 
		 
		average_7 : average_16_samples_unsigned 
		PORT MAP
		(
			clock		=> clock,
			reset_n		=> reset_n,
			data_in		=> data_clean_7_S,
			data_out	=> data_avg_7_S
		 );	 
		 
		average_8 : average_16_samples_unsigned 
		PORT MAP
		(
			clock		=> clock,
			reset_n		=> reset_n,
			data_in		=> data_clean_8_S,
			data_out	=> data_avg_8_S
		 );	 
		 
		average_9 : average_16_samples_unsigned 
		PORT MAP
		(
			clock		=> clock,
			reset_n		=> reset_n,
			data_in		=> data_clean_9_S,
			data_out	=> data_avg_9_S
		 );	 
	 
end Behavioral;
