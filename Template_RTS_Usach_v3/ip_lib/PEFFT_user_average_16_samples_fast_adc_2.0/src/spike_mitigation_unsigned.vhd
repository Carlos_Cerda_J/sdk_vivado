----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/10/2020 01:56:08 PM
-- Design Name: 
-- Module Name: spike_mitigation_unsigned - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity spike_mitigation_unsigned is
    Port ( clock : in STD_LOGIC;
	       reset_n : in STD_LOGIC;
           data_in : in STD_LOGIC_VECTOR (11 downto 0);
           data_avg : in STD_LOGIC_VECTOR (11 downto 0);
		   band	 : in STD_LOGIC_VECTOR (11 downto 0);
           data_out : out STD_LOGIC_VECTOR (11 downto 0));
end spike_mitigation_unsigned;

architecture Behavioral of spike_mitigation_unsigned is
 	signal data_in_signed 	: signed (12 downto 0) := (others => '0');
	signal data_avg_signed 	: signed (12 downto 0) := (others => '0');
	signal band_signed 		: signed (12 downto 0) := (others => '0');
	
begin

	data_in_signed 	<= signed('0'&data_in);
	data_avg_signed <= signed('0'&data_avg);
	band_signed		<= signed('0'&band);
	
	process(clock, reset_n)
	begin
		if (reset_n = '0') then
			data_out <= (others=>'0');
			
		elsif rising_edge(clock) then
			if (abs(data_in_signed-data_avg_signed) < band_signed) then
				data_out <= data_in;
			else
				data_out <= data_avg;
			end if;
		end if;
	end process;

end Behavioral;
