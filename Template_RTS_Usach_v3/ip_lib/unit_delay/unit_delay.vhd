----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.11.2018 09:16:14
-- Design Name: 
-- Module Name: slow_adc_main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity unit_delay is
	GENERIC
	(
		word_width : INTEGER := 25
	);
	Port
	(
		clk       : in STD_LOGIC;
		reset_n   : in STD_LOGIC;
		data_in  : in  STD_LOGIC_VECTOR (word_width-1 downto 0);
		data_out : out STD_LOGIC_VECTOR (word_width-1 downto 0));
		
end unit_delay;


architecture Behavioral of unit_delay is

	SIGNAL	data_S	:		STD_LOGIC_VECTOR (word_width-1 downto 0);
	
BEGIN
	PROCESS(clk, reset_n)
	BEGIN
	    IF reset_n = '0' THEN
	       data_S <= (others => '0');
		ELSIF RISING_EDGE(clk) THEN
			data_S <= data_in;
		ELSE
			data_S <= data_S;
		END IF;
	END PROCESS;

	data_out <= data_S;

end Behavioral;
