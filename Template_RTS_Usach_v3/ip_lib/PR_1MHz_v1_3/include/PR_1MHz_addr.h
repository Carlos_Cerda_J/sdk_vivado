/*
 * File Name:         hdl_prj\ipcore\PR_1MHz_v1_3\include\PR_1MHz_addr.h
 * Description:       C Header File
 * Created:           2021-01-28 13:29:44
*/

#ifndef PR_1MHZ_H_
#define PR_1MHZ_H_

#define  IPCore_Reset_PR_1MHz       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PR_1MHz      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PR_1MHz   0x8  //contains unique IP timestamp (yymmddHHMM): 2101281329
#define  a0_Data_PR_1MHz            0x100  //data register for Inport a0
#define  a1_Data_PR_1MHz            0x104  //data register for Inport a1
#define  a2_Data_PR_1MHz            0x108  //data register for Inport a2
#define  b1_Data_PR_1MHz            0x10C  //data register for Inport b1
#define  umax_pos_Data_PR_1MHz      0x110  //data register for Inport umax_pos
#define  activator_Data_PR_1MHz     0x114  //data register for Inport activator
#define  umax_neg_Data_PR_1MHz      0x118  //data register for Inport umax_neg
#define  gain_out_Data_PR_1MHz      0x11C  //data register for Inport gain_out

#endif /* PR_1MHZ_H_ */
