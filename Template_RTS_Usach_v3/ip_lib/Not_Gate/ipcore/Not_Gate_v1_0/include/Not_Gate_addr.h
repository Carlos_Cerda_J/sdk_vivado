/*
 * File Name:         C:\Users\VLC\Desktop\simulink IPCores\Not_Gate\ipcore\Not_Gate_v1_0\include\Not_Gate_addr.h
 * Description:       C Header File
 * Created:           2019-02-06 12:42:53
*/

#ifndef NOT_GATE_H_
#define NOT_GATE_H_

#define  IPCore_Reset_Not_Gate       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_Not_Gate      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_Not_Gate   0x8  //contains unique IP timestamp (yymmddHHMM): 1902061242

#endif /* NOT_GATE_H_ */
