/*
 * File Name:         hdl_prj\ipcore\PWM_IPCOR_ip_v1_0\include\PWM_IPCOR_ip_addr.h
 * Description:       C Header File
 * Created:           2018-12-21 12:20:08
*/

#ifndef PWM_IPCOR_IP_H_
#define PWM_IPCOR_IP_H_

#define  IPCore_Reset_PWM_IPCOR_ip       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PWM_IPCOR_ip      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PWM_IPCOR_ip   0x8  //contains unique IP timestamp (yymmddHHMM): 1812211220
#define  Max_point_Data_PWM_IPCOR_ip     0x100  //data register for Inport Max_point

#endif /* PWM_IPCOR_IP_H_ */
