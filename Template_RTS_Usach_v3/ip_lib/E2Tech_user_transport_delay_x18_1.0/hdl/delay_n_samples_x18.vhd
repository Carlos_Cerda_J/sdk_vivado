----------------------------------------------------------------------------------
-- Company: E2Tech
-- Engineer: David Arancibia G.
-- 
-- Create Date: 08/29/2019 06:53:25 PM
-- Design Name: 
-- Module Name: delay_n_samples_x18 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity delay_n_samples_x18 is
  Generic
  (
	WORD_WIDTH		: integer := 2;
	MAX_DELAY		: integer := 100
  );
    Port ( clk : in STD_LOGIC;
           reset_n : in STD_LOGIC;
           delay : in STD_LOGIC_VECTOR (6 downto 0);		--max delay = 127
           data1_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data2_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data3_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data4_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data5_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data6_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data7_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data8_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data9_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data10_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data11_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data12_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data13_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data14_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data15_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data16_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
           data17_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data18_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
           data1_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data2_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data3_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data4_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data5_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data6_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data7_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data8_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data9_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data10_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data11_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data12_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data13_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data14_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data15_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data16_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
           data17_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data18_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0));		   
end delay_n_samples_x18;

architecture Behavioral of delay_n_samples_x18 is
	constant MAX_BITS  : integer := WORD_WIDTH * (MAX_DELAY + 1);
	shared variable memory1_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory2_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory3_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory4_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory5_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory6_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory7_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory8_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory9_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory10_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory11_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory12_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory13_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory14_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory15_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory16_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory17_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	shared variable memory18_S : std_logic_vector(MAX_BITS-1 downto 0):= (others => '0');
	signal delay_int : integer range 0 to 127	:= 0;
	shared variable position : integer range 0 to MAX_BITS-1;
begin
	delay_int <= to_integer(unsigned(delay));
	
	position_process : process(clk, reset_n) is
	begin
		if (rising_edge(clk)) then
			if (delay_int <= MAX_DELAY) then
				position := delay_int * WORD_WIDTH;
			else
				position := MAX_DELAY * WORD_WIDTH;
			end if;
		end if;
	end process;
	
	memory_process : process (clk, reset_n) is
    begin
        if (reset_n = '0') then
			memory1_S := (others => '0');
			memory2_S := (others => '0');
			memory3_S := (others => '0');
			memory4_S := (others => '0');
			memory5_S := (others => '0');
			memory6_S := (others => '0');
			memory7_S := (others => '0');
			memory8_S := (others => '0');
			memory9_S := (others => '0');
			memory10_S := (others => '0');
			memory11_S := (others => '0');
			memory12_S := (others => '0');
			memory13_S := (others => '0');
			memory14_S := (others => '0');
			memory15_S := (others => '0');
			memory16_S := (others => '0');
			memory17_S := (others => '0');
			memory18_S := (others => '0');
        elsif (rising_edge(clk)) then
            memory1_S := memory1_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data1_in;
			memory2_S := memory2_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data2_in;
			memory3_S := memory3_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data3_in;
			memory4_S := memory4_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data4_in;
			memory5_S := memory5_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data5_in;
			memory6_S := memory6_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data6_in;
			memory7_S := memory7_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data7_in;
			memory8_S := memory8_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data8_in;
			memory9_S := memory9_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data9_in;
			memory10_S := memory10_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data10_in;
			memory11_S := memory11_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data11_in;
			memory12_S := memory12_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data12_in;
			memory13_S := memory13_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data13_in;
			memory14_S := memory14_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data14_in;
			memory15_S := memory15_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data15_in;
			memory16_S := memory16_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data16_in;
			memory17_S := memory17_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data17_in;
			memory18_S := memory18_S((MAX_BITS-WORD_WIDTH-1) downto 0) & data18_in;
        end if;                                                                            
    end process;
	
	shift_process : process (clk, reset_n) is
    begin
        if (reset_n = '0') then
            data1_out <= (others => '0');
			data2_out <= (others => '0');
			data3_out <= (others => '0');
			data4_out <= (others => '0');
			data5_out <= (others => '0');
			data6_out <= (others => '0');
			data7_out <= (others => '0');
			data8_out <= (others => '0');
			data9_out <= (others => '0');
			data10_out <= (others => '0');
			data11_out <= (others => '0');
			data12_out <= (others => '0');
			data13_out <= (others => '0');
			data14_out <= (others => '0');
			data15_out <= (others => '0');
			data16_out <= (others => '0');
			data17_out <= (others => '0');
			data18_out <= (others => '0');
        elsif (rising_edge(clk)) then
			data1_out <= memory1_S((position+WORD_WIDTH-1) downto (position));
			data2_out <= memory2_S((position+WORD_WIDTH-1) downto (position));
			data3_out <= memory3_S((position+WORD_WIDTH-1) downto (position));
			data4_out <= memory4_S((position+WORD_WIDTH-1) downto (position));
			data5_out <= memory5_S((position+WORD_WIDTH-1) downto (position));
			data6_out <= memory6_S((position+WORD_WIDTH-1) downto (position));
			data7_out <= memory7_S((position+WORD_WIDTH-1) downto (position));
			data8_out <= memory8_S((position+WORD_WIDTH-1) downto (position));
			data9_out <= memory9_S((position+WORD_WIDTH-1) downto (position));
			data10_out <= memory10_S((position+WORD_WIDTH-1) downto (position));
			data11_out <= memory11_S((position+WORD_WIDTH-1) downto (position));
			data12_out <= memory12_S((position+WORD_WIDTH-1) downto (position));
			data13_out <= memory13_S((position+WORD_WIDTH-1) downto (position));
			data14_out <= memory14_S((position+WORD_WIDTH-1) downto (position));
			data15_out <= memory15_S((position+WORD_WIDTH-1) downto (position));
			data16_out <= memory16_S((position+WORD_WIDTH-1) downto (position));
			data17_out <= memory17_S((position+WORD_WIDTH-1) downto (position));
			data18_out <= memory18_S((position+WORD_WIDTH-1) downto (position));
        end if;                                                                            
    end process;

end Behavioral;
