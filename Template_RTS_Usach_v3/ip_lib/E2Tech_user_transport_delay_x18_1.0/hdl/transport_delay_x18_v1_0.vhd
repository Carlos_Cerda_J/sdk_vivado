library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity transport_delay_x18_v1_0 is
	generic (
		-- Users to add parameters here
		WORD_WIDTH		: integer := 2;
		MAX_DELAY		: integer := 100;
		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S_AXI_Lite
		C_S_AXI_Lite_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_Lite_ADDR_WIDTH	: integer	:= 4
	);
	port (
		-- Users to add ports here
		clk			: in std_logic;
		reset_n		: in std_logic;
		San1			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		San2			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		San3			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		San4			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		San5			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		San6			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		Sbn1			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		Sbn2			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		Sbn3			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		Sbn4			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		Sbn5			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		Sbn6			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		Scn1			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		Scn2			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		Scn3			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		Scn4			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		Scn5			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		Scn6			: in std_logic_vector(WORD_WIDTH-1 downto 0);
		San1_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		San2_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		San3_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		San4_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		San5_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		San6_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		Sbn1_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		Sbn2_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		Sbn3_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		Sbn4_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		Sbn5_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		Sbn6_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		Scn1_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		Scn2_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		Scn3_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		Scn4_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		Scn5_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		Scn6_delayed	: out std_logic_vector(WORD_WIDTH-1 downto 0);
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S_AXI_Lite
		s_axi_lite_aclk	: in std_logic;
		s_axi_lite_aresetn	: in std_logic;
		s_axi_lite_awaddr	: in std_logic_vector(C_S_AXI_Lite_ADDR_WIDTH-1 downto 0);
		s_axi_lite_awprot	: in std_logic_vector(2 downto 0);
		s_axi_lite_awvalid	: in std_logic;
		s_axi_lite_awready	: out std_logic;
		s_axi_lite_wdata	: in std_logic_vector(C_S_AXI_Lite_DATA_WIDTH-1 downto 0);
		s_axi_lite_wstrb	: in std_logic_vector((C_S_AXI_Lite_DATA_WIDTH/8)-1 downto 0);
		s_axi_lite_wvalid	: in std_logic;
		s_axi_lite_wready	: out std_logic;
		s_axi_lite_bresp	: out std_logic_vector(1 downto 0);
		s_axi_lite_bvalid	: out std_logic;
		s_axi_lite_bready	: in std_logic;
		s_axi_lite_araddr	: in std_logic_vector(C_S_AXI_Lite_ADDR_WIDTH-1 downto 0);
		s_axi_lite_arprot	: in std_logic_vector(2 downto 0);
		s_axi_lite_arvalid	: in std_logic;
		s_axi_lite_arready	: out std_logic;
		s_axi_lite_rdata	: out std_logic_vector(C_S_AXI_Lite_DATA_WIDTH-1 downto 0);
		s_axi_lite_rresp	: out std_logic_vector(1 downto 0);
		s_axi_lite_rvalid	: out std_logic;
		s_axi_lite_rready	: in std_logic
	);
end transport_delay_x18_v1_0;

architecture arch_imp of transport_delay_x18_v1_0 is

	-- component declaration
	component transport_delay_x18_v1_0_S_AXI_Lite is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 4
		);
		port (
		delay		: out std_logic_vector(6 downto 0);
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component transport_delay_x18_v1_0_S_AXI_Lite;
	
	component delay_n_samples_x18 is
	Generic
	(
		WORD_WIDTH		: integer := 2;
		MAX_DELAY		: integer := 100
	);
    Port ( 
		   clk : in STD_LOGIC;
           reset_n : in STD_LOGIC;
           delay : in STD_LOGIC_VECTOR (6 downto 0);		--max delay = 127
           data1_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data2_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data3_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data4_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data5_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data6_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data7_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data8_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data9_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data10_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data11_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data12_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data13_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data14_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data15_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data16_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
           data17_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data18_in : in STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
           data1_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data2_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data3_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data4_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data5_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data6_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data7_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data8_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data9_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data10_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data11_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data12_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data13_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data14_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data15_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data16_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
           data17_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0);
		   data18_out : out STD_LOGIC_VECTOR (WORD_WIDTH-1 downto 0));		   
	end component delay_n_samples_x18;
	
	signal delay : STD_LOGIC_VECTOR (6 downto 0);		--max delay = 127
begin

-- Instantiation of Axi Bus Interface S_AXI_Lite
transport_delay_x18_v1_0_S_AXI_Lite_inst : transport_delay_x18_v1_0_S_AXI_Lite
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S_AXI_Lite_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S_AXI_Lite_ADDR_WIDTH
	)
	port map (
		delay 		=> delay,
		S_AXI_ACLK	=> s_axi_lite_aclk,
		S_AXI_ARESETN	=> s_axi_lite_aresetn,
		S_AXI_AWADDR	=> s_axi_lite_awaddr,
		S_AXI_AWPROT	=> s_axi_lite_awprot,
		S_AXI_AWVALID	=> s_axi_lite_awvalid,
		S_AXI_AWREADY	=> s_axi_lite_awready,
		S_AXI_WDATA	=> s_axi_lite_wdata,
		S_AXI_WSTRB	=> s_axi_lite_wstrb,
		S_AXI_WVALID	=> s_axi_lite_wvalid,
		S_AXI_WREADY	=> s_axi_lite_wready,
		S_AXI_BRESP	=> s_axi_lite_bresp,
		S_AXI_BVALID	=> s_axi_lite_bvalid,
		S_AXI_BREADY	=> s_axi_lite_bready,
		S_AXI_ARADDR	=> s_axi_lite_araddr,
		S_AXI_ARPROT	=> s_axi_lite_arprot,
		S_AXI_ARVALID	=> s_axi_lite_arvalid,
		S_AXI_ARREADY	=> s_axi_lite_arready,
		S_AXI_RDATA	=> s_axi_lite_rdata,
		S_AXI_RRESP	=> s_axi_lite_rresp,
		S_AXI_RVALID	=> s_axi_lite_rvalid,
		S_AXI_RREADY	=> s_axi_lite_rready
	);

	delay_n_samples_x18_inst : delay_n_samples_x18
	Generic map
	(
		WORD_WIDTH		=> WORD_WIDTH,
		MAX_DELAY		=> MAX_DELAY
	)
    Port map ( 
		clk 		=> clk,
        reset_n 	=> reset_n,
        delay		=> delay,		--max delay = 127
        data1_in 	=> San1,
		data2_in 	=> San2,
		data3_in 	=> San3,
        data4_in 	=> San4,
		data5_in 	=> San5,
		data6_in 	=> San6,
		data7_in 	=> Sbn1,
		data8_in 	=> Sbn2,
		data9_in 	=> Sbn3,
        data10_in 	=> Sbn4,
		data11_in 	=> Sbn5,
		data12_in 	=> Sbn6,
		data13_in 	=> Scn1,
		data14_in 	=> Scn2,
		data15_in 	=> Scn3,
        data16_in 	=> Scn4,
		data17_in 	=> Scn5,
		data18_in 	=> Scn6,
        data1_out 	=> San1_delayed,
		data2_out 	=> San2_delayed,
		data3_out 	=> San3_delayed,
		data4_out 	=> San4_delayed,
		data5_out 	=> San5_delayed,
		data6_out 	=> San6_delayed,
		data7_out 	=> Sbn1_delayed,
		data8_out 	=> Sbn2_delayed,
		data9_out 	=> Sbn3_delayed,
		data10_out 	=> Sbn4_delayed,
		data11_out 	=> Sbn5_delayed,
		data12_out 	=> Sbn6_delayed,
		data13_out 	=> Scn1_delayed,
		data14_out 	=> Scn2_delayed,
		data15_out 	=> Scn3_delayed,
		data16_out 	=> Scn4_delayed,
		data17_out 	=> Scn5_delayed,
		data18_out 	=> Scn6_delayed
	);
	
	-- Add user logic here
	-- User logic ends

end arch_imp;
