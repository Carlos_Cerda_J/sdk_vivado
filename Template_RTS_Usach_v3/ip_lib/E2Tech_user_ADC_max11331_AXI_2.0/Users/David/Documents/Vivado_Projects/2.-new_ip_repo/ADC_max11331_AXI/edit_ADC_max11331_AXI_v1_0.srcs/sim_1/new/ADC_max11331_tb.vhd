
library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity ADC_max11331_AXI_v1_0_tb is
end;

architecture bench of ADC_max11331_AXI_v1_0_tb is

  component ADC_max11331_AXI_v1_0
  	generic (
  		clk_div : INTEGER := 3;
  		OUTPUT_WORD_WIDTH : INTEGER := 12;
  		C_S0_AXI_Lite_DATA_WIDTH	: integer	:= 32;
  		C_S0_AXI_Lite_ADDR_WIDTH	: integer	:= 6
  	);
  	port (
  		clk 								: in STD_LOGIC;
  		reset_n 							: in STD_LOGIC;
  		enable_measure 				        : in STD_LOGIC;
  		sclk 								: out STD_LOGIC;
  		mosi 								: out STD_LOGIC;
  		miso 								: in STD_LOGIC_VECTOR (5 downto 0);
  		ss_n 								: out STD_LOGIC;
  		init_done 						: out STD_LOGIC;
  		meas_done 						: out STD_LOGIC;
  		new_data 						: out STD_LOGIC;
  		error 							: out STD_LOGIC;
  		data_board_1_input_1_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_1_ch2	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_1_ch3	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_1_ch4	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_2_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_2_ch2	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_2_ch3	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_2_ch4	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_3_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_3_ch2	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_3_ch3	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_3_ch4	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_4_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_4_ch2	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_4_ch3	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_4_ch4	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_5_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_5_ch2	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_5_ch3	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_5_ch4	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_6_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_6_ch2	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_6_ch3	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		data_board_1_input_6_ch4	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_1_ch1	: out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_1_ch2    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_1_ch3    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_1_ch4    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_2_ch1    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_2_ch2    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_2_ch3    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_2_ch4    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_3_ch1    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_3_ch2    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_3_ch3    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_3_ch4    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_4_ch1    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_4_ch2    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_4_ch3    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_4_ch4    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_5_ch1    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_5_ch2    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_5_ch3    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_5_ch4    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_6_ch1    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_6_ch2    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_6_ch3    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
          data_board_2_input_6_ch4    : out STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  		s0_axi_lite_aclk	: in std_logic;
  		s0_axi_lite_aresetn	: in std_logic;
  		s0_axi_lite_awaddr	: in std_logic_vector(C_S0_AXI_Lite_ADDR_WIDTH-1 downto 0);
  		s0_axi_lite_awprot	: in std_logic_vector(2 downto 0);
  		s0_axi_lite_awvalid	: in std_logic;
  		s0_axi_lite_awready	: out std_logic;
  		s0_axi_lite_wdata	: in std_logic_vector(C_S0_AXI_Lite_DATA_WIDTH-1 downto 0);
  		s0_axi_lite_wstrb	: in std_logic_vector((C_S0_AXI_Lite_DATA_WIDTH/8)-1 downto 0);
  		s0_axi_lite_wvalid	: in std_logic;
  		s0_axi_lite_wready	: out std_logic;
  		s0_axi_lite_bresp	: out std_logic_vector(1 downto 0);
  		s0_axi_lite_bvalid	: out std_logic;
  		s0_axi_lite_bready	: in std_logic;
  		s0_axi_lite_araddr	: in std_logic_vector(C_S0_AXI_Lite_ADDR_WIDTH-1 downto 0);
  		s0_axi_lite_arprot	: in std_logic_vector(2 downto 0);
  		s0_axi_lite_arvalid	: in std_logic;
  		s0_axi_lite_arready	: out std_logic;
  		s0_axi_lite_rdata	: out std_logic_vector(C_S0_AXI_Lite_DATA_WIDTH-1 downto 0);
  		s0_axi_lite_rresp	: out std_logic_vector(1 downto 0);
  		s0_axi_lite_rvalid	: out std_logic;
  		s0_axi_lite_rready	: in std_logic
  	);
  end component;
	
	constant OUTPUT_WORD_WIDTH : integer := 16;
	constant C_S0_AXI_Lite_ADDR_WIDTH : integer := 6;
	constant C_S0_AXI_Lite_DATA_WIDTH : integer := 32;
  signal clk: STD_LOGIC;
  signal reset_n: STD_LOGIC;
  signal enable_measure: STD_LOGIC;
  signal sclk: STD_LOGIC;
  signal mosi: STD_LOGIC;
  signal miso: STD_LOGIC_VECTOR (5 downto 0);
  signal ss_n: STD_LOGIC;
  signal init_done: STD_LOGIC;
  signal meas_done: STD_LOGIC;
  signal new_data: STD_LOGIC;
  signal error: STD_LOGIC;
  signal data_board_1_input_1_ch1: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_1_ch2: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_1_ch3: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_1_ch4: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_2_ch1: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_2_ch2: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_2_ch3: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_2_ch4: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_3_ch1: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_3_ch2: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_3_ch3: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_3_ch4: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_4_ch1: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_4_ch2: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_4_ch3: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_4_ch4: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_5_ch1: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_5_ch2: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_5_ch3: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_5_ch4: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_6_ch1: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_6_ch2: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_6_ch3: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_1_input_6_ch4: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_1_ch1: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_1_ch2: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_1_ch3: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_1_ch4: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_2_ch1: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_2_ch2: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_2_ch3: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_2_ch4: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_3_ch1: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_3_ch2: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_3_ch3: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_3_ch4: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_4_ch1: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_4_ch2: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_4_ch3: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_4_ch4: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_5_ch1: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_5_ch2: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_5_ch3: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_5_ch4: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_6_ch1: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_6_ch2: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_6_ch3: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal data_board_2_input_6_ch4: STD_LOGIC_VECTOR (OUTPUT_WORD_WIDTH-1 downto 0);
  signal s0_axi_lite_aclk: std_logic;
  signal s0_axi_lite_aresetn: std_logic;
  signal s0_axi_lite_awaddr: std_logic_vector(C_S0_AXI_Lite_ADDR_WIDTH-1 downto 0);
  signal s0_axi_lite_awprot: std_logic_vector(2 downto 0);
  signal s0_axi_lite_awvalid: std_logic;
  signal s0_axi_lite_awready: std_logic;
  signal s0_axi_lite_wdata: std_logic_vector(C_S0_AXI_Lite_DATA_WIDTH-1 downto 0);
  signal s0_axi_lite_wstrb: std_logic_vector((C_S0_AXI_Lite_DATA_WIDTH/8)-1 downto 0);
  signal s0_axi_lite_wvalid: std_logic;
  signal s0_axi_lite_wready: std_logic;
  signal s0_axi_lite_bresp: std_logic_vector(1 downto 0);
  signal s0_axi_lite_bvalid: std_logic;
  signal s0_axi_lite_bready: std_logic;
  signal s0_axi_lite_araddr: std_logic_vector(C_S0_AXI_Lite_ADDR_WIDTH-1 downto 0);
  signal s0_axi_lite_arprot: std_logic_vector(2 downto 0);
  signal s0_axi_lite_arvalid: std_logic;
  signal s0_axi_lite_arready: std_logic;
  signal s0_axi_lite_rdata: std_logic_vector(C_S0_AXI_Lite_DATA_WIDTH-1 downto 0);
  signal s0_axi_lite_rresp: std_logic_vector(1 downto 0);
  signal s0_axi_lite_rvalid: std_logic;
  signal s0_axi_lite_rready: std_logic ;
  
  constant clock_period: time := 10 ns;
  constant enable_measure_period: time := 10 us;
  signal stop_the_clock: boolean;
  
	CONSTANT MSG_INIT_1	       : STD_LOGIC_VECTOR(15 downto 0) := "1000000000000100";	-- ADC Configuration: activate echo
	CONSTANT MSG_INIT_2	       : STD_LOGIC_VECTOR(15 downto 0) := "1001011111111000";	-- Bipolar register
	CONSTANT MSG_INIT_3	       : STD_LOGIC_VECTOR(15 downto 0) := "1000100000000000";	-- Unipolar register
	CONSTANT MSG_INIT_4	       : STD_LOGIC_VECTOR(15 downto 0) := "1001111111111000";	-- Range register  
	CONSTANT MSG_MEAS_0	       : STD_LOGIC_VECTOR(15 downto 0) := "0000000000000001";	-- Ch0 response
	CONSTANT MSG_MEAS_2	       : STD_LOGIC_VECTOR(15 downto 0) := "0010000000000010";	-- Ch2 response
	CONSTANT MSG_MEAS_4	       : STD_LOGIC_VECTOR(15 downto 0) := "0100000000000100";	-- Ch4 response
	CONSTANT MSG_MEAS_6	       : STD_LOGIC_VECTOR(15 downto 0) := "0110000000000110";	-- Ch6 response
	CONSTANT MSG_MEAS_8	       : STD_LOGIC_VECTOR(15 downto 0) := "1000000000001000";	-- Ch8 response
	CONSTANT MSG_MEAS_10       : STD_LOGIC_VECTOR(15 downto 0) := "1010000000001010";	-- Ch10 response
	CONSTANT MSG_MEAS_12       : STD_LOGIC_VECTOR(15 downto 0) := "1100000000001100";	-- Ch12 response
	CONSTANT MSG_MEAS_14       : STD_LOGIC_VECTOR(15 downto 0) := "1110000000001110";	-- Ch14 response

	--CONSTANT MSG_MEAS_0	       : STD_LOGIC_VECTOR(15 downto 0) := "0000111111111110";	-- Ch0 response
	--CONSTANT MSG_MEAS_2	       : STD_LOGIC_VECTOR(15 downto 0) := "0010111111111100";	-- Ch2 response
	--CONSTANT MSG_MEAS_4	       : STD_LOGIC_VECTOR(15 downto 0) := "0100111111111000";	-- Ch4 response
	--CONSTANT MSG_MEAS_6	       : STD_LOGIC_VECTOR(15 downto 0) := "0110111111110000";	-- Ch6 response
	--CONSTANT MSG_MEAS_8	       : STD_LOGIC_VECTOR(15 downto 0) := "1000111111110000";	-- Ch8 response
	--CONSTANT MSG_MEAS_10       : STD_LOGIC_VECTOR(15 downto 0) := "1010111111110000";	-- Ch10 response
	--CONSTANT MSG_MEAS_12       : STD_LOGIC_VECTOR(15 downto 0) := "1100111111110000";	-- Ch12 response
	--CONSTANT MSG_MEAS_14       : STD_LOGIC_VECTOR(15 downto 0) := "1110111111110000";	-- Ch14 response	

begin

  -- Insert values for generic parameters !!
  uut: ADC_max11331_AXI_v1_0 generic map ( clk_div                  => 3,
                                           OUTPUT_WORD_WIDTH        => OUTPUT_WORD_WIDTH,
                                           C_S0_AXI_Lite_DATA_WIDTH => C_S0_AXI_Lite_DATA_WIDTH,
                                           C_S0_AXI_Lite_ADDR_WIDTH => C_S0_AXI_Lite_ADDR_WIDTH )
                                port map ( clk                      => clk,
                                           reset_n                  => reset_n,
                                           enable_measure           => enable_measure,
                                           sclk                     => sclk,
                                           mosi                     => mosi,
                                           miso                     => miso,
                                           ss_n                     => ss_n,
                                           init_done                => init_done,
                                           meas_done                => meas_done,
                                           new_data                 => new_data,
                                           error                    => error,
                                           data_board_1_input_1_ch1 => data_board_1_input_1_ch1,
                                           data_board_1_input_1_ch2 => data_board_1_input_1_ch2,
                                           data_board_1_input_1_ch3 => data_board_1_input_1_ch3,
                                           data_board_1_input_1_ch4 => data_board_1_input_1_ch4,
                                           data_board_1_input_2_ch1 => data_board_1_input_2_ch1,
                                           data_board_1_input_2_ch2 => data_board_1_input_2_ch2,
                                           data_board_1_input_2_ch3 => data_board_1_input_2_ch3,
                                           data_board_1_input_2_ch4 => data_board_1_input_2_ch4,
                                           data_board_1_input_3_ch1 => data_board_1_input_3_ch1,
                                           data_board_1_input_3_ch2 => data_board_1_input_3_ch2,
                                           data_board_1_input_3_ch3 => data_board_1_input_3_ch3,
                                           data_board_1_input_3_ch4 => data_board_1_input_3_ch4,
                                           data_board_1_input_4_ch1 => data_board_1_input_4_ch1,
                                           data_board_1_input_4_ch2 => data_board_1_input_4_ch2,
                                           data_board_1_input_4_ch3 => data_board_1_input_4_ch3,
                                           data_board_1_input_4_ch4 => data_board_1_input_4_ch4,
                                           data_board_1_input_5_ch1 => data_board_1_input_5_ch1,
                                           data_board_1_input_5_ch2 => data_board_1_input_5_ch2,
                                           data_board_1_input_5_ch3 => data_board_1_input_5_ch3,
                                           data_board_1_input_5_ch4 => data_board_1_input_5_ch4,
                                           data_board_1_input_6_ch1 => data_board_1_input_6_ch1,
                                           data_board_1_input_6_ch2 => data_board_1_input_6_ch2,
                                           data_board_1_input_6_ch3 => data_board_1_input_6_ch3,
                                           data_board_1_input_6_ch4 => data_board_1_input_6_ch4,
                                           data_board_2_input_1_ch1 => data_board_2_input_1_ch1,
                                           data_board_2_input_1_ch2 => data_board_2_input_1_ch2,
                                           data_board_2_input_1_ch3 => data_board_2_input_1_ch3,
                                           data_board_2_input_1_ch4 => data_board_2_input_1_ch4,
                                           data_board_2_input_2_ch1 => data_board_2_input_2_ch1,
                                           data_board_2_input_2_ch2 => data_board_2_input_2_ch2,
                                           data_board_2_input_2_ch3 => data_board_2_input_2_ch3,
                                           data_board_2_input_2_ch4 => data_board_2_input_2_ch4,
                                           data_board_2_input_3_ch1 => data_board_2_input_3_ch1,
                                           data_board_2_input_3_ch2 => data_board_2_input_3_ch2,
                                           data_board_2_input_3_ch3 => data_board_2_input_3_ch3,
                                           data_board_2_input_3_ch4 => data_board_2_input_3_ch4,
                                           data_board_2_input_4_ch1 => data_board_2_input_4_ch1,
                                           data_board_2_input_4_ch2 => data_board_2_input_4_ch2,
                                           data_board_2_input_4_ch3 => data_board_2_input_4_ch3,
                                           data_board_2_input_4_ch4 => data_board_2_input_4_ch4,
                                           data_board_2_input_5_ch1 => data_board_2_input_5_ch1,
                                           data_board_2_input_5_ch2 => data_board_2_input_5_ch2,
                                           data_board_2_input_5_ch3 => data_board_2_input_5_ch3,
                                           data_board_2_input_5_ch4 => data_board_2_input_5_ch4,
                                           data_board_2_input_6_ch1 => data_board_2_input_6_ch1,
                                           data_board_2_input_6_ch2 => data_board_2_input_6_ch2,
                                           data_board_2_input_6_ch3 => data_board_2_input_6_ch3,
                                           data_board_2_input_6_ch4 => data_board_2_input_6_ch4,
                                           s0_axi_lite_aclk         => s0_axi_lite_aclk,
                                           s0_axi_lite_aresetn      => s0_axi_lite_aresetn,
                                           s0_axi_lite_awaddr       => s0_axi_lite_awaddr,
                                           s0_axi_lite_awprot       => s0_axi_lite_awprot,
                                           s0_axi_lite_awvalid      => s0_axi_lite_awvalid,
                                           s0_axi_lite_awready      => s0_axi_lite_awready,
                                           s0_axi_lite_wdata        => s0_axi_lite_wdata,
                                           s0_axi_lite_wstrb        => s0_axi_lite_wstrb,
                                           s0_axi_lite_wvalid       => s0_axi_lite_wvalid,
                                           s0_axi_lite_wready       => s0_axi_lite_wready,
                                           s0_axi_lite_bresp        => s0_axi_lite_bresp,
                                           s0_axi_lite_bvalid       => s0_axi_lite_bvalid,
                                           s0_axi_lite_bready       => s0_axi_lite_bready,
                                           s0_axi_lite_araddr       => s0_axi_lite_araddr,
                                           s0_axi_lite_arprot       => s0_axi_lite_arprot,
                                           s0_axi_lite_arvalid      => s0_axi_lite_arvalid,
                                           s0_axi_lite_arready      => s0_axi_lite_arready,
                                           s0_axi_lite_rdata        => s0_axi_lite_rdata,
                                           s0_axi_lite_rresp        => s0_axi_lite_rresp,
                                           s0_axi_lite_rvalid       => s0_axi_lite_rvalid,
                                           s0_axi_lite_rready       => s0_axi_lite_rready );

  stimulus: process
  begin
  
    -- Put initialisation code here
	stop_the_clock <= false;
	reset_n <= '0';
    wait for 20 ns;
    reset_n <= '1';
	
    wait;
  end process;
  
  
	miso1: process
	variable count_miso			: integer;
	begin
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso <= (others=>'0');
		end loop;
		
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso <= (others=>'0');
		end loop;

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_INIT_2(15-count_miso);
			miso(1) <= MSG_INIT_2(15-count_miso);
			miso(2) <= MSG_INIT_2(15-count_miso);
			miso(3) <= MSG_INIT_2(15-count_miso);
			miso(4) <= MSG_INIT_2(15-count_miso);
			miso(5) <= MSG_INIT_2(15-count_miso);
		end loop;		
		
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= '1';--MSG_INIT_3(15-count_miso);
			miso(1) <= MSG_INIT_3(15-count_miso);
			miso(2) <= MSG_INIT_3(15-count_miso);
			miso(3) <= MSG_INIT_3(15-count_miso);
			miso(4) <= MSG_INIT_3(15-count_miso);
			miso(5) <= MSG_INIT_3(15-count_miso);
		end loop;	
		
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_INIT_4(15-count_miso);
			miso(1) <= MSG_INIT_4(15-count_miso);
			miso(2) <= MSG_INIT_4(15-count_miso);
			miso(3) <= MSG_INIT_4(15-count_miso);
			miso(4) <= MSG_INIT_4(15-count_miso);
			miso(5) <= MSG_INIT_4(15-count_miso);
		end loop;		
		
		--First Failure. Start Again
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso <= (others=>'0');
		end loop;
		
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso <= (others=>'0');
		end loop;

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_INIT_2(15-count_miso);
			miso(1) <= MSG_INIT_2(15-count_miso);
			miso(2) <= MSG_INIT_2(15-count_miso);
			miso(3) <= MSG_INIT_2(15-count_miso);
			miso(4) <= MSG_INIT_2(15-count_miso);
			miso(5) <= MSG_INIT_2(15-count_miso);
		end loop;		
		
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_INIT_3(15-count_miso);
			miso(1) <= MSG_INIT_3(15-count_miso);
			miso(2) <= MSG_INIT_3(15-count_miso);
			miso(3) <= MSG_INIT_3(15-count_miso);
			miso(4) <= MSG_INIT_3(15-count_miso);
			miso(5) <= MSG_INIT_3(15-count_miso);
		end loop;			

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_INIT_4(15-count_miso);
			miso(1) <= MSG_INIT_4(15-count_miso);
			miso(2) <= MSG_INIT_4(15-count_miso);
			miso(3) <= MSG_INIT_4(15-count_miso);
			miso(4) <= MSG_INIT_4(15-count_miso);
			miso(5) <= MSG_INIT_4(15-count_miso);
		end loop;
		
		--Init done. Start measurements
		--wait until ss_n = '0';	
		wait until meas_done = '1';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= '1';--MSG_MEAS_0(15-count_miso);
			miso(1) <= '1';--MSG_MEAS_0(15-count_miso);
			miso(2) <= '1';--MSG_MEAS_0(15-count_miso);
			miso(3) <= '1';--MSG_MEAS_0(15-count_miso);
			miso(4) <= '1';--MSG_MEAS_0(15-count_miso);
			miso(5) <= '1';--MSG_MEAS_0(15-count_miso);
		end loop;

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_0(15-count_miso);
			miso(1) <= MSG_MEAS_0(15-count_miso);
			miso(2) <= MSG_MEAS_0(15-count_miso);
			miso(3) <= MSG_MEAS_0(15-count_miso);
			miso(4) <= MSG_MEAS_0(15-count_miso);
			miso(5) <= MSG_MEAS_0(15-count_miso);
		end loop;

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_2(15-count_miso);
			miso(1) <= MSG_MEAS_2(15-count_miso);
			miso(2) <= MSG_MEAS_2(15-count_miso);
			miso(3) <= MSG_MEAS_2(15-count_miso);
			miso(4) <= MSG_MEAS_2(15-count_miso);
			miso(5) <= MSG_MEAS_2(15-count_miso);
		end loop;		
		
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_4(15-count_miso);
			miso(1) <= MSG_MEAS_4(15-count_miso);
			miso(2) <= MSG_MEAS_4(15-count_miso);
			miso(3) <= MSG_MEAS_4(15-count_miso);
			miso(4) <= MSG_MEAS_4(15-count_miso);
			miso(5) <= MSG_MEAS_4(15-count_miso);
		end loop;		

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_6(15-count_miso);
			miso(1) <= MSG_MEAS_6(15-count_miso);
			miso(2) <= MSG_MEAS_6(15-count_miso);
			miso(3) <= MSG_MEAS_6(15-count_miso);
			miso(4) <= MSG_MEAS_6(15-count_miso);
			miso(5) <= MSG_MEAS_6(15-count_miso);
		end loop;

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_8(15-count_miso);
			miso(1) <= MSG_MEAS_8(15-count_miso);
			miso(2) <= MSG_MEAS_8(15-count_miso);
			miso(3) <= MSG_MEAS_8(15-count_miso);
			miso(4) <= MSG_MEAS_8(15-count_miso);
			miso(5) <= MSG_MEAS_8(15-count_miso);
		end loop;
		
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_10(15-count_miso);
			miso(1) <= MSG_MEAS_10(15-count_miso);
			miso(2) <= MSG_MEAS_10(15-count_miso);
			miso(3) <= MSG_MEAS_10(15-count_miso);
			miso(4) <= MSG_MEAS_10(15-count_miso);
			miso(5) <= MSG_MEAS_10(15-count_miso);
		end loop;		

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_12(15-count_miso);
			miso(1) <= MSG_MEAS_12(15-count_miso);
			miso(2) <= MSG_MEAS_12(15-count_miso);
			miso(3) <= MSG_MEAS_12(15-count_miso);
			miso(4) <= MSG_MEAS_12(15-count_miso);
			miso(5) <= MSG_MEAS_12(15-count_miso);
		end loop;
		
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_14(15-count_miso);
			miso(1) <= MSG_MEAS_14(15-count_miso);
			miso(2) <= MSG_MEAS_14(15-count_miso);
			miso(3) <= MSG_MEAS_14(15-count_miso);
			miso(4) <= MSG_MEAS_14(15-count_miso);
			miso(5) <= MSG_MEAS_14(15-count_miso);
		end loop;

		--New set of data
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_0(15-count_miso);
			miso(1) <= MSG_MEAS_0(15-count_miso);
			miso(2) <= MSG_MEAS_0(15-count_miso);
			miso(3) <= MSG_MEAS_0(15-count_miso);
			miso(4) <= MSG_MEAS_0(15-count_miso);
			miso(5) <= MSG_MEAS_0(15-count_miso);
		end loop;

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_0(15-count_miso);
			miso(1) <= MSG_MEAS_0(15-count_miso);
			miso(2) <= MSG_MEAS_0(15-count_miso);
			miso(3) <= MSG_MEAS_0(15-count_miso);
			miso(4) <= MSG_MEAS_0(15-count_miso);
			miso(5) <= MSG_MEAS_0(15-count_miso);
		end loop;

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_2(15-count_miso);
			miso(1) <= MSG_MEAS_2(15-count_miso);
			miso(2) <= MSG_MEAS_2(15-count_miso);
			miso(3) <= MSG_MEAS_2(15-count_miso);
			miso(4) <= MSG_MEAS_2(15-count_miso);
			miso(5) <= MSG_MEAS_2(15-count_miso);
		end loop;		
		
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_4(15-count_miso);
			miso(1) <= MSG_MEAS_4(15-count_miso);
			miso(2) <= MSG_MEAS_4(15-count_miso);
			miso(3) <= MSG_MEAS_4(15-count_miso);
			miso(4) <= MSG_MEAS_4(15-count_miso);
			miso(5) <= MSG_MEAS_4(15-count_miso);
		end loop;		

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_6(15-count_miso);
			miso(1) <= MSG_MEAS_6(15-count_miso);
			miso(2) <= MSG_MEAS_6(15-count_miso);
			miso(3) <= MSG_MEAS_6(15-count_miso);
			miso(4) <= MSG_MEAS_6(15-count_miso);
			miso(5) <= MSG_MEAS_6(15-count_miso);
		end loop;

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_8(15-count_miso);
			miso(1) <= MSG_MEAS_8(15-count_miso);
			miso(2) <= MSG_MEAS_8(15-count_miso);
			miso(3) <= MSG_MEAS_8(15-count_miso);
			miso(4) <= MSG_MEAS_8(15-count_miso);
			miso(5) <= MSG_MEAS_8(15-count_miso);
		end loop;
		
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_10(15-count_miso);
			miso(1) <= MSG_MEAS_10(15-count_miso);
			miso(2) <= MSG_MEAS_10(15-count_miso);
			miso(3) <= MSG_MEAS_10(15-count_miso);
			miso(4) <= MSG_MEAS_10(15-count_miso);
			miso(5) <= MSG_MEAS_10(15-count_miso);
		end loop;		

		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_12(15-count_miso);
			miso(1) <= MSG_MEAS_12(15-count_miso);
			miso(2) <= MSG_MEAS_12(15-count_miso);
			miso(3) <= MSG_MEAS_12(15-count_miso);
			miso(4) <= MSG_MEAS_12(15-count_miso);
			miso(5) <= MSG_MEAS_12(15-count_miso);
		end loop;
		
		wait until ss_n = '0';	
		for count_miso in 0 to 15 loop
			wait until sclk = '0';
			miso(0) <= MSG_MEAS_14(15-count_miso);
			miso(1) <= MSG_MEAS_14(15-count_miso);
			miso(2) <= MSG_MEAS_14(15-count_miso);
			miso(3) <= MSG_MEAS_14(15-count_miso);
			miso(4) <= MSG_MEAS_14(15-count_miso);
			miso(5) <= MSG_MEAS_14(15-count_miso);
		end loop;
		
		wait;
	end process;
	
	clocking: process
	begin
		while not stop_the_clock loop
		  clk <= '0', '1' after clock_period / 2;
		  wait for clock_period;
		end loop;
		wait;
	end process;

	enable_meas_proc: process
	begin
		while not stop_the_clock loop
		  enable_measure <= '0', '1' after enable_measure_period/2;
		  wait for enable_measure_period;
		end loop;
		wait;
	end process;

end;