----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/07/2019 09:22:17 AM
-- Design Name: 
-- Module Name: trip_enable - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity trip_enable is
    Port ( trip : in STD_LOGIC;
		   trip_fpga : in STD_LOGIC;
           enable_in : in STD_LOGIC;
           enable_out : out STD_LOGIC);
end trip_enable;

architecture Behavioral of trip_enable is

begin

enable_out <= enable_in and (not trip) and (not trip_fpga);

end Behavioral;
