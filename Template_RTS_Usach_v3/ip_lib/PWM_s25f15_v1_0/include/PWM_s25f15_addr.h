/*
 * File Name:         hdl_prj\ipcore\PWM_s25f15_v1_0\include\PWM_s25f15_addr.h
 * Description:       C Header File
 * Created:           2019-01-10 13:56:42
*/

#ifndef PWM_S25F15_H_
#define PWM_S25F15_H_

#define  IPCore_Reset_PWM_s25f15       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PWM_s25f15      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PWM_s25f15   0x8  //contains unique IP timestamp (yymmddHHMM): 1901101356
#define  Max_point_Data_PWM_s25f15     0x100  //data register for Inport Max_point

#endif /* PWM_S25F15_H_ */
