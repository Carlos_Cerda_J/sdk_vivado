/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\sort_nlc_0912\SORT_NLC_6CELDAS_OPT1\sort_6cell_0912_4.3\ipcore\sort_3c_v4_3\include\sort_3c_addr.h
 * Description:       C Header File
 * Created:           2019-12-09 16:23:48
*/

#ifndef SORT_3C_H_
#define SORT_3C_H_

#define  IPCore_Reset_sort_3c       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_sort_3c      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_sort_3c   0x8  //contains unique IP timestamp (yymmddHHMM): 1912091623

#endif /* SORT_3C_H_ */
