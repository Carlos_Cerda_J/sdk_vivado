/*
 * File Name:         F:\sorting\sorting_2c\ipcore\sort_2cl_v1_0\include\sort_2cl_addr.h
 * Description:       C Header File
 * Created:           2019-04-17 12:24:48
*/

#ifndef SORT_2CL_H_
#define SORT_2CL_H_

#define  IPCore_Reset_sort_2cl       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_sort_2cl      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_sort_2cl   0x8  //contains unique IP timestamp (yymmddHHMM): 1904171224

#endif /* SORT_2CL_H_ */
