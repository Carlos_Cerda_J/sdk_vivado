library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity spike_mitigation_signed_tb is
end;

architecture bench of spike_mitigation_signed_tb is

  component spike_mitigation_signed
  	Generic 
  	(
  		DATA_WORD_WIDTH	: integer	:= 12
  	);
      Port 
  	( 
  		clk : in STD_LOGIC;
          reset_n : in STD_LOGIC;
  		delta_spike	: in STD_LOGIC_VECTOR (DATA_WORD_WIDTH-1 downto 0);
          data_in : in STD_LOGIC_VECTOR (DATA_WORD_WIDTH-1 downto 0);
          data_out : out STD_LOGIC_VECTOR (DATA_WORD_WIDTH-1 downto 0)
  	);
  end component;

	constant DATA_WORD_WIDTH : integer := 12;
  signal clk: STD_LOGIC;
  signal reset_n: STD_LOGIC;
  signal delta_spike: STD_LOGIC_VECTOR (DATA_WORD_WIDTH-1 downto 0);
  signal data_in: STD_LOGIC_VECTOR (DATA_WORD_WIDTH-1 downto 0);
  signal data_out: STD_LOGIC_VECTOR (DATA_WORD_WIDTH-1 downto 0) ;

  constant clock_period: time := 100 ns;
  signal stop_the_clock: boolean;

begin

  -- Insert values for generic parameters !!
  uut: spike_mitigation_signed generic map ( DATA_WORD_WIDTH =>  DATA_WORD_WIDTH)
                                  port map ( clk             => clk,
                                             reset_n         => reset_n,
                                             delta_spike     => delta_spike,
                                             data_in         => data_in,
                                             data_out        => data_out );

  stimulus: process
  begin
  
    -- Put initialisation code here

    reset_n <= '0';
    wait for 50 ns;
    reset_n <= '1';
    wait for 50 ns;

    -- Put test bench stimulus code here

    stop_the_clock <= false;
    wait;
  end process;

  delta: process
  begin
  	delta_spike <= std_logic_vector(to_unsigned(50,DATA_WORD_WIDTH));
    wait;
  end process;
 
  datos: process
  begin
  	data_in <= std_logic_vector(to_signed(100,DATA_WORD_WIDTH));
	wait for 300 ns;
	data_in <= std_logic_vector(to_signed(140,DATA_WORD_WIDTH));
	wait for 100 ns;
	data_in <= std_logic_vector(to_signed(200,DATA_WORD_WIDTH));
	wait for 500 ns;
	data_in <= std_logic_vector(to_signed(125,DATA_WORD_WIDTH));
	wait for 100 ns;
	data_in <= std_logic_vector(to_signed(80,DATA_WORD_WIDTH));
    wait;
  end process;
  
  clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after clock_period / 2;
      wait for clock_period;
    end loop;
    wait;
  end process;

end;