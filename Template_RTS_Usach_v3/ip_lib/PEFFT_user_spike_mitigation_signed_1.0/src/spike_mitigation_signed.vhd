----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/20/2019 08:58:06 AM
-- Design Name: 
-- Module Name: spike_mitigation_signed - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity spike_mitigation_signed is
	Generic 
	(
		-- Width of input/output data Buses
		DATA_WORD_WIDTH	: integer	:= 12
	);
    Port 
	( 
		clk : in STD_LOGIC;
        reset_n : in STD_LOGIC;
		delta_spike	: in STD_LOGIC_VECTOR (DATA_WORD_WIDTH-1 downto 0);
        data_in : in STD_LOGIC_VECTOR (DATA_WORD_WIDTH-1 downto 0);
        data_out : out STD_LOGIC_VECTOR (DATA_WORD_WIDTH-1 downto 0)
	);
end spike_mitigation_signed;

architecture Behavioral of spike_mitigation_signed is
	
	signal data_in_signed 			: signed(DATA_WORD_WIDTH-1 downto 0);
	signal data_out_signed 			: signed(DATA_WORD_WIDTH-1 downto 0);
	signal data_in_previous_signed 	: signed(DATA_WORD_WIDTH-1 downto 0);
	signal abs_difference_signed	: signed(DATA_WORD_WIDTH-1 downto 0);
	signal delta_spike_signed		: signed(DATA_WORD_WIDTH-1 downto 0);
	
begin

	-- data_out_signed process
	process(clk, reset_n)
    begin
        if reset_n = '0' then
            data_out_signed <= (others => '0');
        elsif rising_edge(clk) then
			if (abs_difference_signed < delta_spike_signed) then	-- no spike detected. assign output same as input.
				data_out_signed <= data_in_signed;
			else													-- spike detected. assign output same as previous input.
				data_out_signed <= data_in_previous_signed;
            end if;
        else
			data_out_signed <= data_out_signed;
        end if;
    end process;
    
	-- data_in_previous process
	process(clk, reset_n)
    begin
        if reset_n = '0' then
            data_in_previous_signed <= data_in_signed;
        elsif rising_edge(clk) then
			if (abs_difference_signed < delta_spike_signed) then	-- no spike detected. assign previous same as input. 
				data_in_previous_signed <= data_in_signed;
			else													-- spike detected. assign previous same as previous
				data_in_previous_signed <= data_in_previous_signed;
            end if;				
        else
			data_in_previous_signed <= data_in_previous_signed;
        end if;
    end process;
	
	-- combinatorial assigments
	data_in_signed 			<=	signed(data_in);
	delta_spike_signed		<= 	signed(delta_spike);
	data_out				<=	std_logic_vector(data_out_signed);
	abs_difference_signed 	<=	abs(data_in_signed - data_in_previous_signed);
	
end Behavioral;
