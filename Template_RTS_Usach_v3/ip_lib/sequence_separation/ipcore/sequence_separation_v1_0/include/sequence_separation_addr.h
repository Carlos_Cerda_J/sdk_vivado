/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\seq_sep\sequence_separation\ipcore\sequence_separation_v1_0\include\sequence_separation_addr.h
 * Description:       C Header File
 * Created:           2019-07-09 16:23:44
*/

#ifndef SEQUENCE_SEPARATION_H_
#define SEQUENCE_SEPARATION_H_

#define  IPCore_Reset_sequence_separation       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_sequence_separation      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_sequence_separation   0x8  //contains unique IP timestamp (yymmddHHMM): 1907091623

#endif /* SEQUENCE_SEPARATION_H_ */
