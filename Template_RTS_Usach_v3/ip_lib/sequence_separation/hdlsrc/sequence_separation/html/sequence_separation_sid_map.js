function RTW_SidParentMap() {
    this.sidParentMap = new Array();
    this.sidParentMap["sequence_separation:240"] = "sequence_separation:239";
    this.sidParentMap["sequence_separation:241"] = "sequence_separation:239";
    this.sidParentMap["sequence_separation:478"] = "sequence_separation:239";
    this.sidParentMap["sequence_separation:265"] = "sequence_separation:239";
    this.sidParentMap["sequence_separation:266"] = "sequence_separation:239";
    this.sidParentMap["sequence_separation:538"] = "sequence_separation:239";
    this.sidParentMap["sequence_separation:1948"] = "sequence_separation:239";
    this.sidParentMap["sequence_separation:478:1"] = "sequence_separation:478";
    this.sidParentMap["sequence_separation:478:1:14"] = "sequence_separation:478";
    this.sidParentMap["sequence_separation:478:1:22"] = "sequence_separation:478";
    this.sidParentMap["sequence_separation:478:1:23"] = "sequence_separation:478";
    this.sidParentMap["sequence_separation:478:1:26"] = "sequence_separation:478";
    this.sidParentMap["sequence_separation:478:1:27"] = "sequence_separation:478";
    this.sidParentMap["sequence_separation:478:1:28"] = "sequence_separation:478";
    this.sidParentMap["sequence_separation:478:1:29"] = "sequence_separation:478";
    this.sidParentMap["sequence_separation:478:1:32"] = "sequence_separation:478";
    this.sidParentMap["sequence_separation:478:1:33"] = "sequence_separation:478";
    this.sidParentMap["sequence_separation:478:1:34"] = "sequence_separation:478";
    this.sidParentMap["sequence_separation:478:1:35"] = "sequence_separation:478";
    this.getParentSid = function(sid) { return this.sidParentMap[sid];}
}
    RTW_SidParentMap.instance = new RTW_SidParentMap();
