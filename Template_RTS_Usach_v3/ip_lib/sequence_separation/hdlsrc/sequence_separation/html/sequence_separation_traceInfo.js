function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <S1>/seq_sep_fcn */
	this.urlHashMap["sequence_separation:478"] = "sequence_separation_src_Sequence_sep.vhd:112,113,114,115,116,117,118,119,120,121,122";
	/* <S9>:1 */
	this.urlHashMap["sequence_separation:478:1"] = "sequence_separation_src_seq_sep_fcn.vhd:92";
	/* <S9>:1:14 */
	this.urlHashMap["sequence_separation:478:1:14"] = "sequence_separation_src_seq_sep_fcn.vhd:94,95,98,99";
	/* <S9>:1:22 */
	this.urlHashMap["sequence_separation:478:1:22"] = "sequence_separation_src_seq_sep_fcn.vhd:100";
	/* <S9>:1:23 */
	this.urlHashMap["sequence_separation:478:1:23"] = "sequence_separation_src_seq_sep_fcn.vhd:101";
	/* <S9>:1:26 */
	this.urlHashMap["sequence_separation:478:1:26"] = "sequence_separation_src_seq_sep_fcn.vhd:103,104,105,107";
	/* <S9>:1:27 */
	this.urlHashMap["sequence_separation:478:1:27"] = "sequence_separation_src_seq_sep_fcn.vhd:108,109,110,112";
	/* <S9>:1:28 */
	this.urlHashMap["sequence_separation:478:1:28"] = "sequence_separation_src_seq_sep_fcn.vhd:113,114,115,117";
	/* <S9>:1:29 */
	this.urlHashMap["sequence_separation:478:1:29"] = "sequence_separation_src_seq_sep_fcn.vhd:118,119,120,122";
	/* <S9>:1:32 */
	this.urlHashMap["sequence_separation:478:1:32"] = "sequence_separation_src_seq_sep_fcn.vhd:123,124,125,126,127,128,129";
	/* <S9>:1:33 */
	this.urlHashMap["sequence_separation:478:1:33"] = "sequence_separation_src_seq_sep_fcn.vhd:130,131,132,133,134,135,136";
	/* <S9>:1:34 */
	this.urlHashMap["sequence_separation:478:1:34"] = "sequence_separation_src_seq_sep_fcn.vhd:137,138,139,140,141,142,143";
	/* <S9>:1:35 */
	this.urlHashMap["sequence_separation:478:1:35"] = "sequence_separation_src_seq_sep_fcn.vhd:144,145,146,147,148,149,150,151,152";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "sequence_separation"};
	this.sidHashMap["sequence_separation"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S1>/x_alpha"] = {sid: "sequence_separation:240"};
	this.sidHashMap["sequence_separation:240"] = {rtwname: "<S1>/x_alpha"};
	this.rtwnameHashMap["<S1>/x_beta"] = {sid: "sequence_separation:241"};
	this.sidHashMap["sequence_separation:241"] = {rtwname: "<S1>/x_beta"};
	this.rtwnameHashMap["<S1>/seq_sep_fcn"] = {sid: "sequence_separation:478"};
	this.sidHashMap["sequence_separation:478"] = {rtwname: "<S1>/seq_sep_fcn"};
	this.rtwnameHashMap["<S1>/x_alpha_P"] = {sid: "sequence_separation:265"};
	this.sidHashMap["sequence_separation:265"] = {rtwname: "<S1>/x_alpha_P"};
	this.rtwnameHashMap["<S1>/x_beta_P"] = {sid: "sequence_separation:266"};
	this.sidHashMap["sequence_separation:266"] = {rtwname: "<S1>/x_beta_P"};
	this.rtwnameHashMap["<S1>/x_alpha_N"] = {sid: "sequence_separation:538"};
	this.sidHashMap["sequence_separation:538"] = {rtwname: "<S1>/x_alpha_N"};
	this.rtwnameHashMap["<S1>/x_beta_N"] = {sid: "sequence_separation:1948"};
	this.sidHashMap["sequence_separation:1948"] = {rtwname: "<S1>/x_beta_N"};
	this.rtwnameHashMap["<S9>:1"] = {sid: "sequence_separation:478:1"};
	this.sidHashMap["sequence_separation:478:1"] = {rtwname: "<S9>:1"};
	this.rtwnameHashMap["<S9>:1:14"] = {sid: "sequence_separation:478:1:14"};
	this.sidHashMap["sequence_separation:478:1:14"] = {rtwname: "<S9>:1:14"};
	this.rtwnameHashMap["<S9>:1:22"] = {sid: "sequence_separation:478:1:22"};
	this.sidHashMap["sequence_separation:478:1:22"] = {rtwname: "<S9>:1:22"};
	this.rtwnameHashMap["<S9>:1:23"] = {sid: "sequence_separation:478:1:23"};
	this.sidHashMap["sequence_separation:478:1:23"] = {rtwname: "<S9>:1:23"};
	this.rtwnameHashMap["<S9>:1:26"] = {sid: "sequence_separation:478:1:26"};
	this.sidHashMap["sequence_separation:478:1:26"] = {rtwname: "<S9>:1:26"};
	this.rtwnameHashMap["<S9>:1:27"] = {sid: "sequence_separation:478:1:27"};
	this.sidHashMap["sequence_separation:478:1:27"] = {rtwname: "<S9>:1:27"};
	this.rtwnameHashMap["<S9>:1:28"] = {sid: "sequence_separation:478:1:28"};
	this.sidHashMap["sequence_separation:478:1:28"] = {rtwname: "<S9>:1:28"};
	this.rtwnameHashMap["<S9>:1:29"] = {sid: "sequence_separation:478:1:29"};
	this.sidHashMap["sequence_separation:478:1:29"] = {rtwname: "<S9>:1:29"};
	this.rtwnameHashMap["<S9>:1:32"] = {sid: "sequence_separation:478:1:32"};
	this.sidHashMap["sequence_separation:478:1:32"] = {rtwname: "<S9>:1:32"};
	this.rtwnameHashMap["<S9>:1:33"] = {sid: "sequence_separation:478:1:33"};
	this.sidHashMap["sequence_separation:478:1:33"] = {rtwname: "<S9>:1:33"};
	this.rtwnameHashMap["<S9>:1:34"] = {sid: "sequence_separation:478:1:34"};
	this.sidHashMap["sequence_separation:478:1:34"] = {rtwname: "<S9>:1:34"};
	this.rtwnameHashMap["<S9>:1:35"] = {sid: "sequence_separation:478:1:35"};
	this.sidHashMap["sequence_separation:478:1:35"] = {rtwname: "<S9>:1:35"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
