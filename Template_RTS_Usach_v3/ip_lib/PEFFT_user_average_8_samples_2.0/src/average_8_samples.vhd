--average_8_samples.vhd 8 samples
--Calculate the average value of 8 12-bit values which are given to the entity one after each other at each clock cycle
--Every 8th clock cycle the averaged value is updated
--Peter Stolze, 17/05/2012 version 1.0
--David Arancibia, 08/01/2019 version 2.0

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity average_8_samples is port
(
	clock:			in std_logic := '0';			--Clock signal
	reset_n:		in std_logic := '0';			--Reset signal
	
	data_in:		in std_logic_vector(11 downto 0) := (others => '0');			--Input data for the current clock cycle
	
	data_out:		out std_logic_vector(11 downto 0) := (others => '0');			--Current output data (valid for 8 clock cycles)
	new_value:		out std_logic := '0'                                 			--new valid value flag
);
end average_8_samples;

architecture average_8_samples_implementation of average_8_samples is
begin
	averaging_process: process(clock, reset_n)
		variable data_accumulator:			unsigned(14 downto 0) := (others => '0');				--Stores the data for the averaging of the values
		variable counter:						integer range 0 to 7 := 0;								--Counter for the averaging
		variable round_up:					integer range 0 to 1 := 0;									--Flag if the averaged value has to be rounded up or not (1: round up; 0: do not round up)
	begin
		if rising_edge(clock) and reset_n = '1' then
			--Add the input data to the data accumulator
			data_accumulator := data_accumulator + unsigned(data_in);
			
			--Increase counter or set it back to 0
			if counter = 7 then
				--If the "first decimal place" (bit 2) = 1 => Round up
				if data_accumulator(2) = '1' then
					round_up := 1;
				end if;
				
				data_out <= std_logic_vector(data_accumulator(14 downto 3) + to_unsigned(round_up, 1));
				new_value <= '1';
				--Reset all variables for the next averaging period
				round_up := 0;
				data_accumulator := (others => '0');
				counter := 0;
			else --counter < 7
				counter := counter + 1;
				new_value <= '0';
			end if;
		end if;
	end process averaging_process;
end average_8_samples_implementation;