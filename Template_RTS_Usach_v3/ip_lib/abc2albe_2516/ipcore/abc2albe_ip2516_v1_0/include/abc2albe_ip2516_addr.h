/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\abc2albe\abc2albe_2516\ipcore\abc2albe_ip2516_v1_0\include\abc2albe_ip2516_addr.h
 * Description:       C Header File
 * Created:           2019-07-19 16:59:15
*/

#ifndef ABC2ALBE_IP2516_H_
#define ABC2ALBE_IP2516_H_

#define  IPCore_Reset_abc2albe_ip2516       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_abc2albe_ip2516      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_abc2albe_ip2516   0x8  //contains unique IP timestamp (yymmddHHMM): 1907191659

#endif /* ABC2ALBE_IP2516_H_ */
