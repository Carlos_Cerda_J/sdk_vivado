function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <S3>/Subsystem */
	this.urlHashMap["lpf_test_pi:2039"] = "vo_LPF_src_VO_lpf.vhd:108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126";
	/* <S6>/Abs */
	this.urlHashMap["lpf_test_pi:2103"] = "vo_LPF_src_Subsystem.vhd:121,123,124,125";
	/* <S6>/Add */
	this.urlHashMap["lpf_test_pi:2047"] = "vo_LPF_src_Subsystem.vhd:210,211,212";
	/* <S6>/Add1 */
	this.urlHashMap["lpf_test_pi:2198"] = "vo_LPF_src_Subsystem.vhd:216";
	/* <S6>/Delay */
	this.urlHashMap["lpf_test_pi:2048"] = "vo_LPF_src_Subsystem.vhd:132,133,134,135,136,137,138,139,140,141";
	/* <S6>/Delay1 */
	this.urlHashMap["lpf_test_pi:2049"] = "vo_LPF_src_Subsystem.vhd:153,154,155,156,157,158,159,160,161,162";
	/* <S6>/Delay2 */
	this.urlHashMap["lpf_test_pi:2050"] = "vo_LPF_src_Subsystem.vhd:192,193,194,195,196,197,198,199,200,201";
	/* <S6>/Delay3 */
	this.urlHashMap["lpf_test_pi:2051"] = "vo_LPF_src_Subsystem.vhd:177,178,179,180,181,182,183,184,185,186";
	/* <S6>/Product1 */
	this.urlHashMap["lpf_test_pi:2053"] = "vo_LPF_src_Subsystem.vhd:146,147";
	/* <S6>/Product2 */
	this.urlHashMap["lpf_test_pi:2072"] = "vo_LPF_src_Subsystem.vhd:167,168";
	/* <S6>/Product3 */
	this.urlHashMap["lpf_test_pi:2055"] = "vo_LPF_src_Subsystem.vhd:204,205";
	/* <S6>/Product4 */
	this.urlHashMap["lpf_test_pi:2069"] = "vo_LPF_src_Subsystem.vhd:189,190";
	/* <S6>/Product5 */
	this.urlHashMap["lpf_test_pi:2070"] = "vo_LPF_src_Subsystem.vhd:129,130";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "lpf_test_pi"};
	this.sidHashMap["lpf_test_pi"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S3>/vo_in"] = {sid: "lpf_test_pi:2032"};
	this.sidHashMap["lpf_test_pi:2032"] = {rtwname: "<S3>/vo_in"};
	this.rtwnameHashMap["<S3>/a1"] = {sid: "lpf_test_pi:2033"};
	this.sidHashMap["lpf_test_pi:2033"] = {rtwname: "<S3>/a1"};
	this.rtwnameHashMap["<S3>/a2"] = {sid: "lpf_test_pi:2034"};
	this.sidHashMap["lpf_test_pi:2034"] = {rtwname: "<S3>/a2"};
	this.rtwnameHashMap["<S3>/a3"] = {sid: "lpf_test_pi:2035"};
	this.sidHashMap["lpf_test_pi:2035"] = {rtwname: "<S3>/a3"};
	this.rtwnameHashMap["<S3>/a4"] = {sid: "lpf_test_pi:2036"};
	this.sidHashMap["lpf_test_pi:2036"] = {rtwname: "<S3>/a4"};
	this.rtwnameHashMap["<S3>/a5"] = {sid: "lpf_test_pi:2037"};
	this.sidHashMap["lpf_test_pi:2037"] = {rtwname: "<S3>/a5"};
	this.rtwnameHashMap["<S3>/clock_in"] = {sid: "lpf_test_pi:2038"};
	this.sidHashMap["lpf_test_pi:2038"] = {rtwname: "<S3>/clock_in"};
	this.rtwnameHashMap["<S3>/offset"] = {sid: "lpf_test_pi:2194"};
	this.sidHashMap["lpf_test_pi:2194"] = {rtwname: "<S3>/offset"};
	this.rtwnameHashMap["<S3>/Subsystem"] = {sid: "lpf_test_pi:2039"};
	this.sidHashMap["lpf_test_pi:2039"] = {rtwname: "<S3>/Subsystem"};
	this.rtwnameHashMap["<S3>/out_filt"] = {sid: "lpf_test_pi:2063"};
	this.sidHashMap["lpf_test_pi:2063"] = {rtwname: "<S3>/out_filt"};
	this.rtwnameHashMap["<S3>/dbg11"] = {sid: "lpf_test_pi:2064"};
	this.sidHashMap["lpf_test_pi:2064"] = {rtwname: "<S3>/dbg11"};
	this.rtwnameHashMap["<S3>/dbg22"] = {sid: "lpf_test_pi:2065"};
	this.sidHashMap["lpf_test_pi:2065"] = {rtwname: "<S3>/dbg22"};
	this.rtwnameHashMap["<S3>/dbg33"] = {sid: "lpf_test_pi:2066"};
	this.sidHashMap["lpf_test_pi:2066"] = {rtwname: "<S3>/dbg33"};
	this.rtwnameHashMap["<S3>/dbg44"] = {sid: "lpf_test_pi:2067"};
	this.sidHashMap["lpf_test_pi:2067"] = {rtwname: "<S3>/dbg44"};
	this.rtwnameHashMap["<S3>/dbg55"] = {sid: "lpf_test_pi:2068"};
	this.sidHashMap["lpf_test_pi:2068"] = {rtwname: "<S3>/dbg55"};
	this.rtwnameHashMap["<S3>/out_filtered"] = {sid: "lpf_test_pi:2231"};
	this.sidHashMap["lpf_test_pi:2231"] = {rtwname: "<S3>/out_filtered"};
	this.rtwnameHashMap["<S6>/sig_in"] = {sid: "lpf_test_pi:2040"};
	this.sidHashMap["lpf_test_pi:2040"] = {rtwname: "<S6>/sig_in"};
	this.rtwnameHashMap["<S6>/a11"] = {sid: "lpf_test_pi:2041"};
	this.sidHashMap["lpf_test_pi:2041"] = {rtwname: "<S6>/a11"};
	this.rtwnameHashMap["<S6>/a22"] = {sid: "lpf_test_pi:2042"};
	this.sidHashMap["lpf_test_pi:2042"] = {rtwname: "<S6>/a22"};
	this.rtwnameHashMap["<S6>/a33"] = {sid: "lpf_test_pi:2043"};
	this.sidHashMap["lpf_test_pi:2043"] = {rtwname: "<S6>/a33"};
	this.rtwnameHashMap["<S6>/a44"] = {sid: "lpf_test_pi:2044"};
	this.sidHashMap["lpf_test_pi:2044"] = {rtwname: "<S6>/a44"};
	this.rtwnameHashMap["<S6>/a55"] = {sid: "lpf_test_pi:2045"};
	this.sidHashMap["lpf_test_pi:2045"] = {rtwname: "<S6>/a55"};
	this.rtwnameHashMap["<S6>/offset_sum"] = {sid: "lpf_test_pi:2193"};
	this.sidHashMap["lpf_test_pi:2193"] = {rtwname: "<S6>/offset_sum"};
	this.rtwnameHashMap["<S6>/Trigger"] = {sid: "lpf_test_pi:2046"};
	this.sidHashMap["lpf_test_pi:2046"] = {rtwname: "<S6>/Trigger"};
	this.rtwnameHashMap["<S6>/Abs"] = {sid: "lpf_test_pi:2103"};
	this.sidHashMap["lpf_test_pi:2103"] = {rtwname: "<S6>/Abs"};
	this.rtwnameHashMap["<S6>/Add"] = {sid: "lpf_test_pi:2047"};
	this.sidHashMap["lpf_test_pi:2047"] = {rtwname: "<S6>/Add"};
	this.rtwnameHashMap["<S6>/Add1"] = {sid: "lpf_test_pi:2198"};
	this.sidHashMap["lpf_test_pi:2198"] = {rtwname: "<S6>/Add1"};
	this.rtwnameHashMap["<S6>/Delay"] = {sid: "lpf_test_pi:2048"};
	this.sidHashMap["lpf_test_pi:2048"] = {rtwname: "<S6>/Delay"};
	this.rtwnameHashMap["<S6>/Delay1"] = {sid: "lpf_test_pi:2049"};
	this.sidHashMap["lpf_test_pi:2049"] = {rtwname: "<S6>/Delay1"};
	this.rtwnameHashMap["<S6>/Delay2"] = {sid: "lpf_test_pi:2050"};
	this.sidHashMap["lpf_test_pi:2050"] = {rtwname: "<S6>/Delay2"};
	this.rtwnameHashMap["<S6>/Delay3"] = {sid: "lpf_test_pi:2051"};
	this.sidHashMap["lpf_test_pi:2051"] = {rtwname: "<S6>/Delay3"};
	this.rtwnameHashMap["<S6>/Product1"] = {sid: "lpf_test_pi:2053"};
	this.sidHashMap["lpf_test_pi:2053"] = {rtwname: "<S6>/Product1"};
	this.rtwnameHashMap["<S6>/Product2"] = {sid: "lpf_test_pi:2072"};
	this.sidHashMap["lpf_test_pi:2072"] = {rtwname: "<S6>/Product2"};
	this.rtwnameHashMap["<S6>/Product3"] = {sid: "lpf_test_pi:2055"};
	this.sidHashMap["lpf_test_pi:2055"] = {rtwname: "<S6>/Product3"};
	this.rtwnameHashMap["<S6>/Product4"] = {sid: "lpf_test_pi:2069"};
	this.sidHashMap["lpf_test_pi:2069"] = {rtwname: "<S6>/Product4"};
	this.rtwnameHashMap["<S6>/Product5"] = {sid: "lpf_test_pi:2070"};
	this.sidHashMap["lpf_test_pi:2070"] = {rtwname: "<S6>/Product5"};
	this.rtwnameHashMap["<S6>/filt_out"] = {sid: "lpf_test_pi:2057"};
	this.sidHashMap["lpf_test_pi:2057"] = {rtwname: "<S6>/filt_out"};
	this.rtwnameHashMap["<S6>/dbg1"] = {sid: "lpf_test_pi:2058"};
	this.sidHashMap["lpf_test_pi:2058"] = {rtwname: "<S6>/dbg1"};
	this.rtwnameHashMap["<S6>/dbg2"] = {sid: "lpf_test_pi:2059"};
	this.sidHashMap["lpf_test_pi:2059"] = {rtwname: "<S6>/dbg2"};
	this.rtwnameHashMap["<S6>/dbg3"] = {sid: "lpf_test_pi:2060"};
	this.sidHashMap["lpf_test_pi:2060"] = {rtwname: "<S6>/dbg3"};
	this.rtwnameHashMap["<S6>/dbg4"] = {sid: "lpf_test_pi:2061"};
	this.sidHashMap["lpf_test_pi:2061"] = {rtwname: "<S6>/dbg4"};
	this.rtwnameHashMap["<S6>/dbg5"] = {sid: "lpf_test_pi:2062"};
	this.sidHashMap["lpf_test_pi:2062"] = {rtwname: "<S6>/dbg5"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
