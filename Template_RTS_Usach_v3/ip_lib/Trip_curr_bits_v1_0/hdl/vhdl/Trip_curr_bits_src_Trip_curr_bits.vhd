-- -------------------------------------------------------------
-- 
-- File Name: C:\Users\Victor\Documents\matlab_ipcores\Simulink\TRIP\trip_fpga\hdlsrc\tripazo_en_bits\Trip_curr_bits_src_Trip_curr_bits.vhd
-- Created: 2019-08-12 11:42:04
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- 
-- -------------------------------------------------------------
-- Rate and Clocking Details
-- -------------------------------------------------------------
-- Model base rate: 1e-08
-- Target subsystem base rate: 1e-08
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: Trip_curr_bits_src_Trip_curr_bits
-- Source Path: tripazo_en_bits/Trip_curr_bits
-- Hierarchy Level: 0
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY Trip_curr_bits_src_Trip_curr_bits IS
  PORT( Umbral_P                          :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
        Umbral_N                          :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
        IA                                :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        IB                                :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        IC                                :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        IAP                               :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        IBP                               :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        ICP                               :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        IAN                               :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        IBN                               :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        ICN                               :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        IO                                :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        aux0                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        aux1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        Trip_IA_0                         :   OUT   std_logic;
        Trip_IB_1                         :   OUT   std_logic;
        Trip_IC_2                         :   OUT   std_logic;
        Trip_IAP_3                        :   OUT   std_logic;
        Trip_IBP_4                        :   OUT   std_logic;
        Trip_ICP_5                        :   OUT   std_logic;
        Trip_IAN_6                        :   OUT   std_logic;
        Trip_IBN_7                        :   OUT   std_logic;
        Trip_ICN_8                        :   OUT   std_logic;
        Trip_IO_9                         :   OUT   std_logic;
        Trip_aux0_10                      :   OUT   std_logic;
        Trip_aux1_11                      :   OUT   std_logic
        );
END Trip_curr_bits_src_Trip_curr_bits;


ARCHITECTURE rtl OF Trip_curr_bits_src_Trip_curr_bits IS

  -- Component Declarations
  COMPONENT Trip_curr_bits_src_Trip_Detector
    PORT( Umbral_P                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          Umbral_N                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          I1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          Trip1                           :   OUT   std_logic
          );
  END COMPONENT;

  COMPONENT Trip_curr_bits_src_Trip_Detector1
    PORT( Umbral_P                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          Umbral_N                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          I1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          Trip1                           :   OUT   std_logic
          );
  END COMPONENT;

  COMPONENT Trip_curr_bits_src_Trip_Detector2
    PORT( Umbral_P                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          Umbral_N                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          I1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          Trip1                           :   OUT   std_logic
          );
  END COMPONENT;

  COMPONENT Trip_curr_bits_src_Trip_Detector3
    PORT( Umbral_P                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          Umbral_N                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          I1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          Trip1                           :   OUT   std_logic
          );
  END COMPONENT;

  COMPONENT Trip_curr_bits_src_Trip_Detector4
    PORT( Umbral_P                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          Umbral_N                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          I1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          Trip1                           :   OUT   std_logic
          );
  END COMPONENT;

  COMPONENT Trip_curr_bits_src_Trip_Detector5
    PORT( Umbral_P                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          Umbral_N                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          I1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          Trip1                           :   OUT   std_logic
          );
  END COMPONENT;

  COMPONENT Trip_curr_bits_src_Trip_Detector6
    PORT( Umbral_P                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          Umbral_N                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          I1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          Trip1                           :   OUT   std_logic
          );
  END COMPONENT;

  COMPONENT Trip_curr_bits_src_Trip_Detector7
    PORT( Umbral_P                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          Umbral_N                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          I1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          Trip1                           :   OUT   std_logic
          );
  END COMPONENT;

  COMPONENT Trip_curr_bits_src_Trip_Detector8
    PORT( Umbral_P                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          Umbral_N                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          I1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          Trip1                           :   OUT   std_logic
          );
  END COMPONENT;

  COMPONENT Trip_curr_bits_src_Trip_Detector9
    PORT( Umbral_P                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          Umbral_N                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          I1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          Trip1                           :   OUT   std_logic
          );
  END COMPONENT;

  COMPONENT Trip_curr_bits_src_Trip_Detector10
    PORT( Umbral_P                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          Umbral_N                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          I1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          Trip1                           :   OUT   std_logic
          );
  END COMPONENT;

  COMPONENT Trip_curr_bits_src_Trip_Detector11
    PORT( Umbral_P                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          Umbral_N                        :   IN    std_logic_vector(24 DOWNTO 0);  -- sfix25_En11
          I1                              :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          Trip1                           :   OUT   std_logic
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : Trip_curr_bits_src_Trip_Detector
    USE ENTITY work.Trip_curr_bits_src_Trip_Detector(rtl);

  FOR ALL : Trip_curr_bits_src_Trip_Detector1
    USE ENTITY work.Trip_curr_bits_src_Trip_Detector1(rtl);

  FOR ALL : Trip_curr_bits_src_Trip_Detector2
    USE ENTITY work.Trip_curr_bits_src_Trip_Detector2(rtl);

  FOR ALL : Trip_curr_bits_src_Trip_Detector3
    USE ENTITY work.Trip_curr_bits_src_Trip_Detector3(rtl);

  FOR ALL : Trip_curr_bits_src_Trip_Detector4
    USE ENTITY work.Trip_curr_bits_src_Trip_Detector4(rtl);

  FOR ALL : Trip_curr_bits_src_Trip_Detector5
    USE ENTITY work.Trip_curr_bits_src_Trip_Detector5(rtl);

  FOR ALL : Trip_curr_bits_src_Trip_Detector6
    USE ENTITY work.Trip_curr_bits_src_Trip_Detector6(rtl);

  FOR ALL : Trip_curr_bits_src_Trip_Detector7
    USE ENTITY work.Trip_curr_bits_src_Trip_Detector7(rtl);

  FOR ALL : Trip_curr_bits_src_Trip_Detector8
    USE ENTITY work.Trip_curr_bits_src_Trip_Detector8(rtl);

  FOR ALL : Trip_curr_bits_src_Trip_Detector9
    USE ENTITY work.Trip_curr_bits_src_Trip_Detector9(rtl);

  FOR ALL : Trip_curr_bits_src_Trip_Detector10
    USE ENTITY work.Trip_curr_bits_src_Trip_Detector10(rtl);

  FOR ALL : Trip_curr_bits_src_Trip_Detector11
    USE ENTITY work.Trip_curr_bits_src_Trip_Detector11(rtl);

  -- Signals
  SIGNAL Trip_Detector_out1               : std_logic;
  SIGNAL Trip_Detector1_out1              : std_logic;
  SIGNAL Trip_Detector2_out1              : std_logic;
  SIGNAL Trip_Detector3_out1              : std_logic;
  SIGNAL Trip_Detector4_out1              : std_logic;
  SIGNAL Trip_Detector5_out1              : std_logic;
  SIGNAL Trip_Detector6_out1              : std_logic;
  SIGNAL Trip_Detector7_out1              : std_logic;
  SIGNAL Trip_Detector8_out1              : std_logic;
  SIGNAL Trip_Detector9_out1              : std_logic;
  SIGNAL Trip_Detector10_out1             : std_logic;
  SIGNAL Trip_Detector11_out1             : std_logic;

BEGIN
  u_Trip_Detector : Trip_curr_bits_src_Trip_Detector
    PORT MAP( Umbral_P => Umbral_P,  -- sfix25_En11
              Umbral_N => Umbral_N,  -- sfix25_En11
              I1 => IA,  -- ufix12
              Trip1 => Trip_Detector_out1
              );

  u_Trip_Detector1 : Trip_curr_bits_src_Trip_Detector1
    PORT MAP( Umbral_P => Umbral_P,  -- sfix25_En11
              Umbral_N => Umbral_N,  -- sfix25_En11
              I1 => IB,  -- ufix12
              Trip1 => Trip_Detector1_out1
              );

  u_Trip_Detector2 : Trip_curr_bits_src_Trip_Detector2
    PORT MAP( Umbral_P => Umbral_P,  -- sfix25_En11
              Umbral_N => Umbral_N,  -- sfix25_En11
              I1 => IC,  -- ufix12
              Trip1 => Trip_Detector2_out1
              );

  u_Trip_Detector3 : Trip_curr_bits_src_Trip_Detector3
    PORT MAP( Umbral_P => Umbral_P,  -- sfix25_En11
              Umbral_N => Umbral_N,  -- sfix25_En11
              I1 => IAP,  -- ufix12
              Trip1 => Trip_Detector3_out1
              );

  u_Trip_Detector4 : Trip_curr_bits_src_Trip_Detector4
    PORT MAP( Umbral_P => Umbral_P,  -- sfix25_En11
              Umbral_N => Umbral_N,  -- sfix25_En11
              I1 => IBP,  -- ufix12
              Trip1 => Trip_Detector4_out1
              );

  u_Trip_Detector5 : Trip_curr_bits_src_Trip_Detector5
    PORT MAP( Umbral_P => Umbral_P,  -- sfix25_En11
              Umbral_N => Umbral_N,  -- sfix25_En11
              I1 => ICP,  -- ufix12
              Trip1 => Trip_Detector5_out1
              );

  u_Trip_Detector6 : Trip_curr_bits_src_Trip_Detector6
    PORT MAP( Umbral_P => Umbral_P,  -- sfix25_En11
              Umbral_N => Umbral_N,  -- sfix25_En11
              I1 => IAN,  -- ufix12
              Trip1 => Trip_Detector6_out1
              );

  u_Trip_Detector7 : Trip_curr_bits_src_Trip_Detector7
    PORT MAP( Umbral_P => Umbral_P,  -- sfix25_En11
              Umbral_N => Umbral_N,  -- sfix25_En11
              I1 => IBN,  -- ufix12
              Trip1 => Trip_Detector7_out1
              );

  u_Trip_Detector8 : Trip_curr_bits_src_Trip_Detector8
    PORT MAP( Umbral_P => Umbral_P,  -- sfix25_En11
              Umbral_N => Umbral_N,  -- sfix25_En11
              I1 => ICN,  -- ufix12
              Trip1 => Trip_Detector8_out1
              );

  u_Trip_Detector9 : Trip_curr_bits_src_Trip_Detector9
    PORT MAP( Umbral_P => Umbral_P,  -- sfix25_En11
              Umbral_N => Umbral_N,  -- sfix25_En11
              I1 => IO,  -- ufix12
              Trip1 => Trip_Detector9_out1
              );

  u_Trip_Detector10 : Trip_curr_bits_src_Trip_Detector10
    PORT MAP( Umbral_P => Umbral_P,  -- sfix25_En11
              Umbral_N => Umbral_N,  -- sfix25_En11
              I1 => aux0,  -- ufix12
              Trip1 => Trip_Detector10_out1
              );

  u_Trip_Detector11 : Trip_curr_bits_src_Trip_Detector11
    PORT MAP( Umbral_P => Umbral_P,  -- sfix25_En11
              Umbral_N => Umbral_N,  -- sfix25_En11
              I1 => aux1,  -- ufix12
              Trip1 => Trip_Detector11_out1
              );

  Trip_IA_0 <= Trip_Detector_out1;

  Trip_IB_1 <= Trip_Detector1_out1;

  Trip_IC_2 <= Trip_Detector2_out1;

  Trip_IAP_3 <= Trip_Detector3_out1;

  Trip_IBP_4 <= Trip_Detector4_out1;

  Trip_ICP_5 <= Trip_Detector5_out1;

  Trip_IAN_6 <= Trip_Detector6_out1;

  Trip_IBN_7 <= Trip_Detector7_out1;

  Trip_ICN_8 <= Trip_Detector8_out1;

  Trip_IO_9 <= Trip_Detector9_out1;

  Trip_aux0_10 <= Trip_Detector10_out1;

  Trip_aux1_11 <= Trip_Detector11_out1;

END rtl;

