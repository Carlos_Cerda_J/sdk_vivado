/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\ab0SD_abcPN\ipcore\ab0SD_abcPN_V_v1_1\include\ab0SD_abcPN_V_addr.h
 * Description:       C Header File
 * Created:           2020-01-16 10:55:05
*/

#ifndef AB0SD_ABCPN_V_H_
#define AB0SD_ABCPN_V_H_

#define  IPCore_Reset_ab0SD_abcPN_V       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_ab0SD_abcPN_V      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_ab0SD_abcPN_V   0x8  //contains unique IP timestamp (yymmddHHMM): 2001161055

#endif /* AB0SD_ABCPN_V_H_ */
