-- -------------------------------------------------------------
-- 
-- File Name: C:\IPCORES\AB0SD_ABCPN_I\hdlsrc\abcPN_ab0SD_abcPN_25bit_corrientes\ab0SD_abcPN_I_src_ab0SD_abcPN_I_pkg.vhd
-- Created: 2019-01-10 21:47:55
-- 
-- Generated by MATLAB 9.4 and HDL Coder 3.12
-- 
-- -------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

PACKAGE ab0SD_abcPN_I_src_ab0SD_abcPN_I_pkg IS
  TYPE vector_of_signed25 IS ARRAY (NATURAL RANGE <>) OF signed(24 DOWNTO 0);
END ab0SD_abcPN_I_src_ab0SD_abcPN_I_pkg;

