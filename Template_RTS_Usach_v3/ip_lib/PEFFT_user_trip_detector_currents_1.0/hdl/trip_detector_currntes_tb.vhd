library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity trip_detector_currents_v1_0_tb is
end;

architecture bench of trip_detector_currents_v1_0_tb is

  component trip_detector_currents_v1_0
  	generic (
  		C_S_AXI_Lite_DATA_WIDTH	: integer	:= 32;
  		C_S_AXI_Lite_ADDR_WIDTH	: integer	:= 4
  	);
  	port (
  		clock			:  in std_logic;
  		reset_n			:  in std_logic;
  		trip_ia_0		:  in std_logic;
  		trip_ib_1		:  in std_logic;
  		trip_ic_2		:  in std_logic;
  		trip_iap_3		:  in std_logic;
  		trip_ibp_4		:  in std_logic;
  		trip_icp_5		:  in std_logic;
  		trip_ian_6		:  in std_logic;
  		trip_ibn_7		:  in std_logic;
  		trip_icn_8		:  in std_logic;
  		trip_io_9		:  in std_logic;
  		trip_aux0_10	:  in std_logic;
  		trip_aux1_11	:  in std_logic;
  		trip_out		:  out std_logic;
  		s_axi_lite_aclk	: in std_logic;
  		s_axi_lite_aresetn	: in std_logic;
  		s_axi_lite_awaddr	: in std_logic_vector(C_S_AXI_Lite_ADDR_WIDTH-1 downto 0);
  		s_axi_lite_awprot	: in std_logic_vector(2 downto 0);
  		s_axi_lite_awvalid	: in std_logic;
  		s_axi_lite_awready	: out std_logic;
  		s_axi_lite_wdata	: in std_logic_vector(C_S_AXI_Lite_DATA_WIDTH-1 downto 0);
  		s_axi_lite_wstrb	: in std_logic_vector((C_S_AXI_Lite_DATA_WIDTH/8)-1 downto 0);
  		s_axi_lite_wvalid	: in std_logic;
  		s_axi_lite_wready	: out std_logic;
  		s_axi_lite_bresp	: out std_logic_vector(1 downto 0);
  		s_axi_lite_bvalid	: out std_logic;
  		s_axi_lite_bready	: in std_logic;
  		s_axi_lite_araddr	: in std_logic_vector(C_S_AXI_Lite_ADDR_WIDTH-1 downto 0);
  		s_axi_lite_arprot	: in std_logic_vector(2 downto 0);
  		s_axi_lite_arvalid	: in std_logic;
  		s_axi_lite_arready	: out std_logic;
  		s_axi_lite_rdata	: out std_logic_vector(C_S_AXI_Lite_DATA_WIDTH-1 downto 0);
  		s_axi_lite_rresp	: out std_logic_vector(1 downto 0);
  		s_axi_lite_rvalid	: out std_logic;
  		s_axi_lite_rready	: in std_logic
  	);
  end component;

  signal clock: std_logic;
  signal reset_n: std_logic;
  signal trip_ia_0: std_logic;
  signal trip_ib_1: std_logic;
  signal trip_ic_2: std_logic;
  signal trip_iap_3: std_logic;
  signal trip_ibp_4: std_logic;
  signal trip_icp_5: std_logic;
  signal trip_ian_6: std_logic;
  signal trip_ibn_7: std_logic;
  signal trip_icn_8: std_logic;
  signal trip_io_9: std_logic;
  signal trip_aux0_10: std_logic;
  signal trip_aux1_11: std_logic;
  signal trip_out: std_logic;
  signal s_axi_lite_aclk: std_logic;
  signal s_axi_lite_aresetn: std_logic;
  signal s_axi_lite_awaddr: std_logic_vector(4-1 downto 0);
  signal s_axi_lite_awprot: std_logic_vector(2 downto 0);
  signal s_axi_lite_awvalid: std_logic;
  signal s_axi_lite_awready: std_logic;
  signal s_axi_lite_wdata: std_logic_vector(32-1 downto 0);
  signal s_axi_lite_wstrb: std_logic_vector((32/8)-1 downto 0);
  signal s_axi_lite_wvalid: std_logic;
  signal s_axi_lite_wready: std_logic;
  signal s_axi_lite_bresp: std_logic_vector(1 downto 0);
  signal s_axi_lite_bvalid: std_logic;
  signal s_axi_lite_bready: std_logic;
  signal s_axi_lite_araddr: std_logic_vector(4-1 downto 0);
  signal s_axi_lite_arprot: std_logic_vector(2 downto 0);
  signal s_axi_lite_arvalid: std_logic;
  signal s_axi_lite_arready: std_logic;
  signal s_axi_lite_rdata: std_logic_vector(32-1 downto 0);
  signal s_axi_lite_rresp: std_logic_vector(1 downto 0);
  signal s_axi_lite_rvalid: std_logic;
  signal s_axi_lite_rready: std_logic ;

begin

  -- Insert values for generic parameters !!
  uut: trip_detector_currents_v1_0 generic map ( C_S_AXI_Lite_DATA_WIDTH => 32,
                                                 C_S_AXI_Lite_ADDR_WIDTH =>  4)
                                      port map ( clock                   => clock,
                                                 reset_n                 => reset_n,
                                                 trip_ia_0               => trip_ia_0,
                                                 trip_ib_1               => trip_ib_1,
                                                 trip_ic_2               => trip_ic_2,
                                                 trip_iap_3              => trip_iap_3,
                                                 trip_ibp_4              => trip_ibp_4,
                                                 trip_icp_5              => trip_icp_5,
                                                 trip_ian_6              => trip_ian_6,
                                                 trip_ibn_7              => trip_ibn_7,
                                                 trip_icn_8              => trip_icn_8,
                                                 trip_io_9               => trip_io_9,
                                                 trip_aux0_10            => trip_aux0_10,
                                                 trip_aux1_11            => trip_aux1_11,
                                                 trip_out                => trip_out,
                                                 s_axi_lite_aclk         => s_axi_lite_aclk,
                                                 s_axi_lite_aresetn      => s_axi_lite_aresetn,
                                                 s_axi_lite_awaddr       => s_axi_lite_awaddr,
                                                 s_axi_lite_awprot       => s_axi_lite_awprot,
                                                 s_axi_lite_awvalid      => s_axi_lite_awvalid,
                                                 s_axi_lite_awready      => s_axi_lite_awready,
                                                 s_axi_lite_wdata        => s_axi_lite_wdata,
                                                 s_axi_lite_wstrb        => s_axi_lite_wstrb,
                                                 s_axi_lite_wvalid       => s_axi_lite_wvalid,
                                                 s_axi_lite_wready       => s_axi_lite_wready,
                                                 s_axi_lite_bresp        => s_axi_lite_bresp,
                                                 s_axi_lite_bvalid       => s_axi_lite_bvalid,
                                                 s_axi_lite_bready       => s_axi_lite_bready,
                                                 s_axi_lite_araddr       => s_axi_lite_araddr,
                                                 s_axi_lite_arprot       => s_axi_lite_arprot,
                                                 s_axi_lite_arvalid      => s_axi_lite_arvalid,
                                                 s_axi_lite_arready      => s_axi_lite_arready,
                                                 s_axi_lite_rdata        => s_axi_lite_rdata,
                                                 s_axi_lite_rresp        => s_axi_lite_rresp,
                                                 s_axi_lite_rvalid       => s_axi_lite_rvalid,
                                                 s_axi_lite_rready       => s_axi_lite_rready );

  stimulus: process
  begin
  
    -- Put initialisation code here
	reset_n 	<= '0';
  	trip_ia_0	<= '0';
  	trip_ib_1	<= '0';
  	trip_ic_2	<= '0';
  	trip_iap_3	<= '0';
  	trip_ibp_4	<= '0';
  	trip_icp_5	<= '0';
  	trip_ian_6	<= '0';
  	trip_ibn_7	<= '0';
  	trip_icn_8	<= '0';
  	trip_io_9	<= '0';
  	trip_aux0_10<= '0';
  	trip_aux1_11<= '0';
	wait for 100ns;
	reset_n <= '1';
	wait for 50ns;
	trip_ia_0	<= '1';
	wait for 20ns;
	trip_ia_0	<= '0';
	wait for 50ns;
	reset_n <= '0';
	wait for 50ns;
	trip_ib_1	<= '1';
	wait for 100ns;
	trip_ib_1	<= '0';
	wait for 20ns;
	reset_n <= '1';
	wait for 100ns;
	trip_icp_5	<= '1';
	wait for 100ns;
	reset_n <= '0';

    -- Put test bench stimulus code here

    wait;
  end process;

  clocking: process
  begin
    clk_lop: loop
      clock <= '0';
      wait for 10ns;
	  clock <= '1';
	  wait for 10ns;
    end loop clk_lop;
    wait;
  end process;
end;
  