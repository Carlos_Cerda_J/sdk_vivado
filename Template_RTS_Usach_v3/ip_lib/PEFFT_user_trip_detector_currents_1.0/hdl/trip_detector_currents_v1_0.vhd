library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity trip_detector_currents_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S_AXI_Lite
		C_S_AXI_Lite_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_Lite_ADDR_WIDTH	: integer	:= 4
	);
	port (
		-- Users to add ports here
		clock			:  in std_logic;
		reset_n			:  in std_logic;
		trip_ia_0		:  in std_logic;
		trip_ib_1		:  in std_logic;
		trip_ic_2		:  in std_logic;
		trip_iap_3		:  in std_logic;
		trip_ibp_4		:  in std_logic;
		trip_icp_5		:  in std_logic;
		trip_ian_6		:  in std_logic;
		trip_ibn_7		:  in std_logic;
		trip_icn_8		:  in std_logic;
		trip_io_9		:  in std_logic;
		trip_aux0_10	:  in std_logic;
		trip_aux1_11	:  in std_logic;
		trip_out		:  out std_logic;
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S_AXI_Lite
		s_axi_lite_aclk	: in std_logic;
		s_axi_lite_aresetn	: in std_logic;
		s_axi_lite_awaddr	: in std_logic_vector(C_S_AXI_Lite_ADDR_WIDTH-1 downto 0);
		s_axi_lite_awprot	: in std_logic_vector(2 downto 0);
		s_axi_lite_awvalid	: in std_logic;
		s_axi_lite_awready	: out std_logic;
		s_axi_lite_wdata	: in std_logic_vector(C_S_AXI_Lite_DATA_WIDTH-1 downto 0);
		s_axi_lite_wstrb	: in std_logic_vector((C_S_AXI_Lite_DATA_WIDTH/8)-1 downto 0);
		s_axi_lite_wvalid	: in std_logic;
		s_axi_lite_wready	: out std_logic;
		s_axi_lite_bresp	: out std_logic_vector(1 downto 0);
		s_axi_lite_bvalid	: out std_logic;
		s_axi_lite_bready	: in std_logic;
		s_axi_lite_araddr	: in std_logic_vector(C_S_AXI_Lite_ADDR_WIDTH-1 downto 0);
		s_axi_lite_arprot	: in std_logic_vector(2 downto 0);
		s_axi_lite_arvalid	: in std_logic;
		s_axi_lite_arready	: out std_logic;
		s_axi_lite_rdata	: out std_logic_vector(C_S_AXI_Lite_DATA_WIDTH-1 downto 0);
		s_axi_lite_rresp	: out std_logic_vector(1 downto 0);
		s_axi_lite_rvalid	: out std_logic;
		s_axi_lite_rready	: in std_logic
	);
end trip_detector_currents_v1_0;

architecture arch_imp of trip_detector_currents_v1_0 is

	-- component declaration
	component trip_detector_currents_v1_0_S_AXI_Lite is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 4
		);
		port (
		trip_instantaneous	: in  std_logic_vector (11 downto 0);
		trip_triggered		: in std_logic_vector (11 downto 0);
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component trip_detector_currents_v1_0_S_AXI_Lite;
	
	component trip_detector_trg is
	port (
		-- Users to add ports here
		clock				: in std_logic;
		reset_n				: in std_logic;
		trip_instantaneous	: in  std_logic_vector (11 downto 0);
		trip_triggered		: out std_logic_vector (11 downto 0);
		trip_out			: out std_logic
		);
	end component trip_detector_trg;
	
	signal trip_instantaneous 	: std_logic_vector(11 downto 0) := (others => '0');
	signal trip_triggered		: std_logic_vector(11 downto 0) := (others => '0');
begin

-- Instantiation of Axi Bus Interface S_AXI_Lite
trip_detector_currents_v1_0_S_AXI_Lite_inst : trip_detector_currents_v1_0_S_AXI_Lite
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S_AXI_Lite_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S_AXI_Lite_ADDR_WIDTH
	)
	port map (
		trip_instantaneous	=> trip_instantaneous,
		trip_triggered		=> trip_triggered,
		S_AXI_ACLK	=> s_axi_lite_aclk,
		S_AXI_ARESETN	=> s_axi_lite_aresetn,
		S_AXI_AWADDR	=> s_axi_lite_awaddr,
		S_AXI_AWPROT	=> s_axi_lite_awprot,
		S_AXI_AWVALID	=> s_axi_lite_awvalid,
		S_AXI_AWREADY	=> s_axi_lite_awready,
		S_AXI_WDATA	=> s_axi_lite_wdata,
		S_AXI_WSTRB	=> s_axi_lite_wstrb,
		S_AXI_WVALID	=> s_axi_lite_wvalid,
		S_AXI_WREADY	=> s_axi_lite_wready,
		S_AXI_BRESP	=> s_axi_lite_bresp,
		S_AXI_BVALID	=> s_axi_lite_bvalid,
		S_AXI_BREADY	=> s_axi_lite_bready,
		S_AXI_ARADDR	=> s_axi_lite_araddr,
		S_AXI_ARPROT	=> s_axi_lite_arprot,
		S_AXI_ARVALID	=> s_axi_lite_arvalid,
		S_AXI_ARREADY	=> s_axi_lite_arready,
		S_AXI_RDATA	=> s_axi_lite_rdata,
		S_AXI_RRESP	=> s_axi_lite_rresp,
		S_AXI_RVALID	=> s_axi_lite_rvalid,
		S_AXI_RREADY	=> s_axi_lite_rready
	);
	
-- Instantiation of trip detector trg
	trip_detector_trg_0 : trip_detector_trg
	port map (
		clock				=> clock,
		reset_n				=> reset_n,
		trip_instantaneous	=> trip_instantaneous,
		trip_triggered		=> trip_triggered,
		trip_out			=> trip_out
		);

	-- Add user logic here
	trip_instantaneous <= trip_aux1_11 & trip_aux0_10 & trip_io_9 & trip_icn_8 & trip_ibn_7 & trip_ian_6 & trip_icp_5 & trip_ibp_4 & trip_iap_3 & trip_ic_2 & trip_ib_1 & trip_ia_0;
	-- User logic ends

end arch_imp;
