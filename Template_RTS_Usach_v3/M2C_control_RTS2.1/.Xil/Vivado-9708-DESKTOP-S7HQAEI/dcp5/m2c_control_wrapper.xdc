set_property SRC_FILE_INFO {cfile:C:/Users/Victor/Documents/Vivados/MMC_AIC_3CELL/M2C_control_RTS2.1/M2C_control_RTS2.1.srcs/constrs_1/imports/constraints/vivado_target.xdc rfile:../../M2C_control_RTS2.1.srcs/constrs_1/imports/constraints/vivado_target.xdc id:1 rxprname:$PSRCDIR/constrs_1/imports/constraints/vivado_target.xdc} [current_design]
set_property SRC_FILE_INFO {cfile:C:/Users/Victor/Documents/Vivados/MMC_AIC_3CELL/M2C_control_RTS2.1/M2C_control_RTS2.1.srcs/constrs_1/imports/constraints/_i_bitgen_common.xdc rfile:../../M2C_control_RTS2.1.srcs/constrs_1/imports/constraints/_i_bitgen_common.xdc id:2 rxprname:$PSRCDIR/constrs_1/imports/constraints/_i_bitgen_common.xdc} [current_design]
set_property SRC_FILE_INFO {cfile:C:/Users/Victor/Documents/Vivados/MMC_AIC_3CELL/M2C_control_RTS2.1/M2C_control_RTS2.1.srcs/constrs_1/imports/constraints/_i_unused_io.xdc rfile:../../M2C_control_RTS2.1.srcs/constrs_1/imports/constraints/_i_unused_io.xdc id:3 rxprname:$PSRCDIR/constrs_1/imports/constraints/_i_unused_io.xdc} [current_design]
set_property src_info {type:XDC file:1 line:1 export:INPUT save:INPUT read:READ} [current_design]
#-----------------------------
set_property src_info {type:XDC file:1 line:2 export:INPUT save:INPUT read:READ} [current_design]
# IMPORTANT
set_property src_info {type:XDC file:1 line:3 export:INPUT save:INPUT read:READ} [current_design]
# Use only for RTS Usach v3.1
set_property src_info {type:XDC file:1 line:4 export:INPUT save:INPUT read:READ} [current_design]
#-----------------------------
set_property src_info {type:XDC file:1 line:5 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:6 export:INPUT save:INPUT read:READ} [current_design]
# Clock definitions
set_property src_info {type:XDC file:1 line:7 export:INPUT save:INPUT read:READ} [current_design]
create_clock -period 100.000 -name ADCLK1_P -waveform {0.000 50.000} [get_ports ADCLK1_P]
set_property src_info {type:XDC file:1 line:8 export:INPUT save:INPUT read:READ} [current_design]
create_clock -period 100.000 -name ADCLK2_P -waveform {0.000 50.000} [get_ports ADCLK2_P]
set_property src_info {type:XDC file:1 line:9 export:INPUT save:INPUT read:READ} [current_design]
create_clock -period 16.667 -name LCLK1_P -waveform {0.000 8.334} [get_ports LCLK1_P]
set_property src_info {type:XDC file:1 line:10 export:INPUT save:INPUT read:READ} [current_design]
create_clock -period 16.667 -name LCLK2_P -waveform {0.000 8.334} [get_ports LCLK2_P]
set_property src_info {type:XDC file:1 line:11 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:12 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:13 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 0 - B35_IO0
set_property src_info {type:XDC file:1 line:14 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN B21 [get_ports {FO1[0]}]
set_property src_info {type:XDC file:1 line:15 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 1 - B35_IO8
set_property src_info {type:XDC file:1 line:16 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN B16 [get_ports {FO1[1]}]
set_property src_info {type:XDC file:1 line:17 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 2 - B35_IO9
set_property src_info {type:XDC file:1 line:18 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN A16 [get_ports {FO1[2]}]
set_property src_info {type:XDC file:1 line:19 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 3 - B35_IO10
set_property src_info {type:XDC file:1 line:20 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN B15 [get_ports {FO1[3]}]
set_property src_info {type:XDC file:1 line:21 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 4 - B35_IO23
set_property src_info {type:XDC file:1 line:22 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN D20 [get_ports {FO1[4]}]
set_property src_info {type:XDC file:1 line:23 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 5 - B35_IO24
set_property src_info {type:XDC file:1 line:24 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN C20 [get_ports {FO1[5]}]
set_property src_info {type:XDC file:1 line:25 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 6 - B35_IO25
set_property src_info {type:XDC file:1 line:26 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN E19 [get_ports {FO1[6]}]
set_property src_info {type:XDC file:1 line:27 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 7 - B35_IO22
set_property src_info {type:XDC file:1 line:28 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN E15 [get_ports {FO1[7]}]
set_property src_info {type:XDC file:1 line:29 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 8 - B35_IO21
set_property src_info {type:XDC file:1 line:30 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN E16 [get_ports {FO1[8]}]
set_property src_info {type:XDC file:1 line:31 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 9 - B35_IO20
set_property src_info {type:XDC file:1 line:32 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN F17 [get_ports {FO1[9]}]
set_property src_info {type:XDC file:1 line:33 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 10 - B35_IO19
set_property src_info {type:XDC file:1 line:34 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN D15 [get_ports {FO1[10]}]
set_property src_info {type:XDC file:1 line:35 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 11 - B35_IO18
set_property src_info {type:XDC file:1 line:36 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN C15 [get_ports {FO1[11]}]
set_property src_info {type:XDC file:1 line:37 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 12 - B35_IO17
set_property src_info {type:XDC file:1 line:38 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN G15 [get_ports {FO1[12]}]
set_property src_info {type:XDC file:1 line:39 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 13 - B35_IO1
set_property src_info {type:XDC file:1 line:40 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN A21 [get_ports {FO1[13]}]
set_property src_info {type:XDC file:1 line:41 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 14 - B35_IO2
set_property src_info {type:XDC file:1 line:42 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN B20 [get_ports {FO1[14]}]
set_property src_info {type:XDC file:1 line:43 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 15 - B35_IO3
set_property src_info {type:XDC file:1 line:44 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN B19 [get_ports {FO1[15]}]
set_property src_info {type:XDC file:1 line:45 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 16 - B35_IO4
set_property src_info {type:XDC file:1 line:46 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN A19 [get_ports {FO1[16]}]
set_property src_info {type:XDC file:1 line:47 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 17 - B35_IO5
set_property src_info {type:XDC file:1 line:48 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN A18 [get_ports {FO1[17]}]
set_property src_info {type:XDC file:1 line:49 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 18 - B35_IO6
set_property src_info {type:XDC file:1 line:50 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN B17 [get_ports {FO1[18]}]
set_property src_info {type:XDC file:1 line:51 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 19 - B35_IO7
set_property src_info {type:XDC file:1 line:52 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN A17 [get_ports {FO1[19]}]
set_property src_info {type:XDC file:1 line:53 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 20 - B35_IO11
set_property src_info {type:XDC file:1 line:54 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN E20 [get_ports {FO1[20]}]
set_property src_info {type:XDC file:1 line:55 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 21 - B35_IO12
set_property src_info {type:XDC file:1 line:56 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN F19 [get_ports {FO1[21]}]
set_property src_info {type:XDC file:1 line:57 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 22 - B35_IO13
set_property src_info {type:XDC file:1 line:58 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN D18 [get_ports {FO1[22]}]
set_property src_info {type:XDC file:1 line:59 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 23 - B35_IO14
set_property src_info {type:XDC file:1 line:60 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN E18 [get_ports {FO1[23]}]
set_property src_info {type:XDC file:1 line:61 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 24 - B35_IO15
set_property src_info {type:XDC file:1 line:62 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN D16 [get_ports {FO1[24]}]
set_property src_info {type:XDC file:1 line:63 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber1_FO 25 - B35_IO16
set_property src_info {type:XDC file:1 line:64 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN F18 [get_ports {FO1[25]}]
set_property src_info {type:XDC file:1 line:65 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports {FO1[*]}]
set_property src_info {type:XDC file:1 line:66 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:67 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:68 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 0 - B33_IO8
set_property src_info {type:XDC file:1 line:69 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U20 [get_ports {FO2[0]}]
set_property src_info {type:XDC file:1 line:70 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 1 - B33_IO3
set_property src_info {type:XDC file:1 line:71 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U22 [get_ports {FO2[1]}]
set_property src_info {type:XDC file:1 line:72 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 2 - B33_IO2
set_property src_info {type:XDC file:1 line:73 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN T22 [get_ports {FO2[2]}]
set_property src_info {type:XDC file:1 line:74 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 3 - B33_IO28
set_property src_info {type:XDC file:1 line:75 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U15 [get_ports {FO2[3]}]
set_property src_info {type:XDC file:1 line:76 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 4 - B33_IO31
set_property src_info {type:XDC file:1 line:77 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V17 [get_ports {FO2[4]}]
set_property src_info {type:XDC file:1 line:78 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 5 - B33_IO29
set_property src_info {type:XDC file:1 line:79 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U16 [get_ports {FO2[5]}]
set_property src_info {type:XDC file:1 line:80 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 6 - B33_IO37
set_property src_info {type:XDC file:1 line:81 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V15 [get_ports {FO2[6]}]
set_property src_info {type:XDC file:1 line:82 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 7 - B33_IO38
set_property src_info {type:XDC file:1 line:83 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V13 [get_ports {FO2[7]}]
set_property src_info {type:XDC file:1 line:84 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 8 - B33_IO44
set_property src_info {type:XDC file:1 line:85 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y13 [get_ports {FO2[8]}]
set_property src_info {type:XDC file:1 line:86 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 9 - B33_IO39
set_property src_info {type:XDC file:1 line:87 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W13 [get_ports {FO2[9]}]
set_property src_info {type:XDC file:1 line:88 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 10 - B33_IO42
set_property src_info {type:XDC file:1 line:89 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y14 [get_ports {FO2[10]}]
set_property src_info {type:XDC file:1 line:90 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 11 - B33_IO36
set_property src_info {type:XDC file:1 line:91 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V14 [get_ports {FO2[11]}]
set_property src_info {type:XDC file:1 line:92 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 12 - B33_IO41
set_property src_info {type:XDC file:1 line:93 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y15 [get_ports {FO2[12]}]
set_property src_info {type:XDC file:1 line:94 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 13 - B33_IO30
set_property src_info {type:XDC file:1 line:95 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U17 [get_ports {FO2[13]}]
set_property src_info {type:XDC file:1 line:96 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 14 - B33_IO9
set_property src_info {type:XDC file:1 line:97 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V20 [get_ports {FO2[14]}]
set_property src_info {type:XDC file:1 line:98 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 15 - B33_IO11
set_property src_info {type:XDC file:1 line:99 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V19 [get_ports {FO2[15]}]
set_property src_info {type:XDC file:1 line:100 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 16 - B33_IO6
set_property src_info {type:XDC file:1 line:101 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W20 [get_ports {FO2[16]}]
set_property src_info {type:XDC file:1 line:102 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 17 - B33_IO10
set_property src_info {type:XDC file:1 line:103 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V18 [get_ports {FO2[17]}]
set_property src_info {type:XDC file:1 line:104 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 18 - B33_IO16
set_property src_info {type:XDC file:1 line:105 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y20 [get_ports {FO2[18]}]
set_property src_info {type:XDC file:1 line:106 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 19 - B33_IO20
set_property src_info {type:XDC file:1 line:107 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y19 [get_ports {FO2[19]}]
set_property src_info {type:XDC file:1 line:108 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 20 - B33_IO25
set_property src_info {type:XDC file:1 line:109 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W18 [get_ports {FO2[20]}]
set_property src_info {type:XDC file:1 line:110 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 21 - B33_IO22
set_property src_info {type:XDC file:1 line:111 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y18 [get_ports {FO2[21]}]
set_property src_info {type:XDC file:1 line:112 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 22 - B33_IO24
set_property src_info {type:XDC file:1 line:113 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W17 [get_ports {FO2[22]}]
set_property src_info {type:XDC file:1 line:114 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 23 - B33_IO26
set_property src_info {type:XDC file:1 line:115 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W16 [get_ports {FO2[23]}]
set_property src_info {type:XDC file:1 line:116 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 24 - B33_IO27
set_property src_info {type:XDC file:1 line:117 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y16 [get_ports {FO2[24]}]
set_property src_info {type:XDC file:1 line:118 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber2_FO 25 - B33_IO40
set_property src_info {type:XDC file:1 line:119 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W15 [get_ports {FO2[25]}]
set_property src_info {type:XDC file:1 line:120 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports {FO2[*]}]
set_property src_info {type:XDC file:1 line:121 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:122 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:123 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 0 - B13_L7N
set_property src_info {type:XDC file:1 line:124 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB12 [get_ports {FO3[0]}]
set_property src_info {type:XDC file:1 line:125 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[0]}]
set_property src_info {type:XDC file:1 line:126 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 1 - B13_L8P
set_property src_info {type:XDC file:1 line:127 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA11 [get_ports {FO3[1]}]
set_property src_info {type:XDC file:1 line:128 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[1]}]
set_property src_info {type:XDC file:1 line:129 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 2 - B13_L8N
set_property src_info {type:XDC file:1 line:130 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB11 [get_ports {FO3[2]}]
set_property src_info {type:XDC file:1 line:131 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[2]}]
set_property src_info {type:XDC file:1 line:132 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 3 - B13_L9P
set_property src_info {type:XDC file:1 line:133 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB10 [get_ports {FO3[3]}]
set_property src_info {type:XDC file:1 line:134 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[3]}]
set_property src_info {type:XDC file:1 line:135 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 4 - B13_L9N
set_property src_info {type:XDC file:1 line:136 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB9 [get_ports {FO3[4]}]
set_property src_info {type:XDC file:1 line:137 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[4]}]
set_property src_info {type:XDC file:1 line:138 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 5 - B13_L17P
set_property src_info {type:XDC file:1 line:139 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB7 [get_ports {FO3[5]}]
set_property src_info {type:XDC file:1 line:140 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[5]}]
set_property src_info {type:XDC file:1 line:141 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 6 - B13_L17N
set_property src_info {type:XDC file:1 line:142 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB6 [get_ports {FO3[6]}]
set_property src_info {type:XDC file:1 line:143 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[6]}]
set_property src_info {type:XDC file:1 line:144 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 7 - B34_L8P - DISP_RX0_P
set_property src_info {type:XDC file:1 line:145 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN J21 [get_ports {FO3[7]}]
set_property src_info {type:XDC file:1 line:146 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[7]}]
set_property src_info {type:XDC file:1 line:147 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 8 - B34_L8N - DISP_RX0_N
set_property src_info {type:XDC file:1 line:148 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN J22 [get_ports {FO3[8]}]
set_property src_info {type:XDC file:1 line:149 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[8]}]
set_property src_info {type:XDC file:1 line:150 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 9 - B34_L10P - DISP_RX1_P
set_property src_info {type:XDC file:1 line:151 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN L21 [get_ports {FO3[9]}]
set_property src_info {type:XDC file:1 line:152 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[9]}]
set_property src_info {type:XDC file:1 line:153 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 10 - B34_L10N - DISP_RX1_N
set_property src_info {type:XDC file:1 line:154 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN L22 [get_ports {FO3[10]}]
set_property src_info {type:XDC file:1 line:155 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[10]}]
set_property src_info {type:XDC file:1 line:156 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 11 - B34_L15P - DISP_RX2_P
set_property src_info {type:XDC file:1 line:157 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M21 [get_ports {FO3[11]}]
set_property src_info {type:XDC file:1 line:158 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[11]}]
set_property src_info {type:XDC file:1 line:159 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 12 - B34_L15N - DISP_RX2_N
set_property src_info {type:XDC file:1 line:160 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN M22 [get_ports {FO3[12]}]
set_property src_info {type:XDC file:1 line:161 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[12]}]
set_property src_info {type:XDC file:1 line:162 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 13 - B13_L7P
set_property src_info {type:XDC file:1 line:163 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA12 [get_ports {FO3[13]}]
set_property src_info {type:XDC file:1 line:164 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[13]}]
set_property src_info {type:XDC file:1 line:165 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 14 - B33_IO12
set_property src_info {type:XDC file:1 line:166 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA22 [get_ports {FO3[14]}]
set_property src_info {type:XDC file:1 line:167 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[14]}]
set_property src_info {type:XDC file:1 line:168 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 15 - B33_IO17
set_property src_info {type:XDC file:1 line:169 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y21 [get_ports {FO3[15]}]
set_property src_info {type:XDC file:1 line:170 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[15]}]
set_property src_info {type:XDC file:1 line:171 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 16 - B33_IO7
set_property src_info {type:XDC file:1 line:172 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W21 [get_ports {FO3[16]}]
set_property src_info {type:XDC file:1 line:173 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[16]}]
set_property src_info {type:XDC file:1 line:174 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 17 - B33_IO5
set_property src_info {type:XDC file:1 line:175 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W22 [get_ports {FO3[17]}]
set_property src_info {type:XDC file:1 line:176 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[17]}]
set_property src_info {type:XDC file:1 line:177 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 18 - B33_IO4
set_property src_info {type:XDC file:1 line:178 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V22 [get_ports {FO3[18]}]
set_property src_info {type:XDC file:1 line:179 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[18]}]
set_property src_info {type:XDC file:1 line:180 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 19 - B33_IO1
set_property src_info {type:XDC file:1 line:181 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U21 [get_ports {FO3[19]}]
set_property src_info {type:XDC file:1 line:182 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[19]}]
set_property src_info {type:XDC file:1 line:183 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 20 - B33_IO0
set_property src_info {type:XDC file:1 line:184 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN T21 [get_ports {FO3[20]}]
set_property src_info {type:XDC file:1 line:185 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[20]}]
set_property src_info {type:XDC file:1 line:186 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 21 - B35_IO27
set_property src_info {type:XDC file:1 line:187 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN C18 [get_ports {FO3[21]}]
set_property src_info {type:XDC file:1 line:188 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[21]}]
set_property src_info {type:XDC file:1 line:189 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 22 - B34_L17N - DISP_RX_CLK_N
set_property src_info {type:XDC file:1 line:190 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN R21 [get_ports {FO3[22]}]
set_property src_info {type:XDC file:1 line:191 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[22]}]
set_property src_info {type:XDC file:1 line:192 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 23 - B34_L17P - DISP_RX_CLK_P
set_property src_info {type:XDC file:1 line:193 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN R20 [get_ports {FO3[23]}]
set_property src_info {type:XDC file:1 line:194 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[23]}]
set_property src_info {type:XDC file:1 line:195 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 24 - B34_L16N - DISP_RX3_N
set_property src_info {type:XDC file:1 line:196 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN P22 [get_ports {FO3[24]}]
set_property src_info {type:XDC file:1 line:197 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[24]}]
set_property src_info {type:XDC file:1 line:198 export:INPUT save:INPUT read:READ} [current_design]
#OpticalFiber3_FO 25 - B34_L16P - DISP_RX3_P
set_property src_info {type:XDC file:1 line:199 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN N22 [get_ports {FO3[25]}]
set_property src_info {type:XDC file:1 line:200 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[25]}]
set_property src_info {type:XDC file:1 line:201 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:202 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:203 export:INPUT save:INPUT read:READ} [current_design]
#ADCLK  -   B35_IO29
set_property src_info {type:XDC file:1 line:204 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN C17 [get_ports ADCLK]
set_property src_info {type:XDC file:1 line:205 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports ADCLK]
set_property src_info {type:XDC file:1 line:206 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:207 export:INPUT save:INPUT read:READ} [current_design]
#ADCLK_P1   -   B13_L12_P
set_property src_info {type:XDC file:1 line:208 export:INPUT save:INPUT read:READ} [current_design]
#ADCLK_N1   -   B13_L12_N
set_property src_info {type:XDC file:1 line:210 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y8 [get_ports ADCLK1_N]
set_property src_info {type:XDC file:1 line:210 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y9 [get_ports ADCLK1_P]
set_property src_info {type:XDC file:1 line:211 export:INPUT save:INPUT read:READ} [current_design]
#ADCLK_P2   -   B13_L14_P
set_property src_info {type:XDC file:1 line:212 export:INPUT save:INPUT read:READ} [current_design]
#ADCLK_N2   -   B13_L14_N
set_property src_info {type:XDC file:1 line:214 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA6 [get_ports ADCLK2_N]
set_property src_info {type:XDC file:1 line:214 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA7 [get_ports ADCLK2_P]
set_property src_info {type:XDC file:1 line:215 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVDS_25 [get_ports ADCLK1*]
set_property src_info {type:XDC file:1 line:216 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVDS_25 [get_ports ADCLK2*]
set_property src_info {type:XDC file:1 line:217 export:INPUT save:INPUT read:READ} [current_design]
set_property DIFF_TERM true [get_ports ADCLK1*]
set_property src_info {type:XDC file:1 line:218 export:INPUT save:INPUT read:READ} [current_design]
set_property DIFF_TERM true [get_ports ADCLK2*]
set_property src_info {type:XDC file:1 line:219 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:220 export:INPUT save:INPUT read:READ} [current_design]
#LCLK_P1   -   B13_L11_P
set_property src_info {type:XDC file:1 line:221 export:INPUT save:INPUT read:READ} [current_design]
#LCLK_N1   -   B13_L11_N
set_property src_info {type:XDC file:1 line:223 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA8 [get_ports LCLK1_N]
set_property src_info {type:XDC file:1 line:223 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA9 [get_ports LCLK1_P]
set_property src_info {type:XDC file:1 line:224 export:INPUT save:INPUT read:READ} [current_design]
#LCLK_P2   -   B13_L13_P
set_property src_info {type:XDC file:1 line:225 export:INPUT save:INPUT read:READ} [current_design]
#LCLK_N2   -   B13_L13_N
set_property src_info {type:XDC file:1 line:227 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y5 [get_ports LCLK2_N]
set_property src_info {type:XDC file:1 line:227 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y6 [get_ports LCLK2_P]
set_property src_info {type:XDC file:1 line:228 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVDS_25 [get_ports LCLK*]
set_property src_info {type:XDC file:1 line:229 export:INPUT save:INPUT read:READ} [current_design]
set_property DIFF_TERM true [get_ports LCLK*]
set_property src_info {type:XDC file:1 line:230 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:231 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P1   -   B13_L16_P
set_property src_info {type:XDC file:1 line:232 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N1   -   B13_L16_N
set_property src_info {type:XDC file:1 line:234 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB4 [get_ports {ADC1_N[0]}]
set_property src_info {type:XDC file:1 line:234 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB5 [get_ports {ADC1_P[0]}]
set_property src_info {type:XDC file:1 line:234 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_N[0]} {set_property PACKAGE_PIN AB4 [get_ports {ADC1_N[0]}]} {MOD}
set_property src_info {type:XDC file:1 line:234 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_P[0]} {set_property PACKAGE_PIN AB5 [get_ports {ADC1_P[0]}]} {MOD}
set_property src_info {type:XDC file:1 line:235 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P2   -   B13_L15_P
set_property src_info {type:XDC file:1 line:236 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N2   -   B13_L15_N
set_property src_info {type:XDC file:1 line:238 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB1 [get_ports {ADC1_N[1]}]
set_property src_info {type:XDC file:1 line:238 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB2 [get_ports {ADC1_P[1]}]
set_property src_info {type:XDC file:1 line:238 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_N[1]} {set_property PACKAGE_PIN AB1 [get_ports {ADC1_N[1]}]} {MOD}
set_property src_info {type:XDC file:1 line:238 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_P[1]} {set_property PACKAGE_PIN AB2 [get_ports {ADC1_P[1]}]} {MOD}
set_property src_info {type:XDC file:1 line:239 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P3   -   B13_L5_P
set_property src_info {type:XDC file:1 line:240 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N3   -   B13_L5_N
set_property src_info {type:XDC file:1 line:242 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U11 [get_ports {ADC1_N[2]}]
set_property src_info {type:XDC file:1 line:242 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U12 [get_ports {ADC1_P[2]}]
set_property src_info {type:XDC file:1 line:242 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_N[2]} {set_property PACKAGE_PIN U11 [get_ports {ADC1_N[2]}]} {MOD}
set_property src_info {type:XDC file:1 line:242 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_P[2]} {set_property PACKAGE_PIN U12 [get_ports {ADC1_P[2]}]} {MOD}
set_property src_info {type:XDC file:1 line:243 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P4   -   B13_L1_P
set_property src_info {type:XDC file:1 line:244 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N4   -   B13_L1_N
set_property src_info {type:XDC file:1 line:246 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V9 [get_ports {ADC1_N[3]}]
set_property src_info {type:XDC file:1 line:246 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V10 [get_ports {ADC1_P[3]}]
set_property src_info {type:XDC file:1 line:246 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_N[3]} {set_property PACKAGE_PIN V9 [get_ports {ADC1_N[3]}]} {MOD}
set_property src_info {type:XDC file:1 line:246 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_P[3]} {set_property PACKAGE_PIN V10 [get_ports {ADC1_P[3]}]} {MOD}
set_property src_info {type:XDC file:1 line:247 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P5   -   B13_L6_P
set_property src_info {type:XDC file:1 line:248 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N5   -   B13_L6_N
set_property src_info {type:XDC file:1 line:250 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U9 [get_ports {ADC1_N[4]}]
set_property src_info {type:XDC file:1 line:250 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U10 [get_ports {ADC1_P[4]}]
set_property src_info {type:XDC file:1 line:250 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_N[4]} {set_property PACKAGE_PIN U9 [get_ports {ADC1_N[4]}]} {MOD}
set_property src_info {type:XDC file:1 line:250 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_P[4]} {set_property PACKAGE_PIN U10 [get_ports {ADC1_P[4]}]} {MOD}
set_property src_info {type:XDC file:1 line:251 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P6   -   B13_L22_P
set_property src_info {type:XDC file:1 line:252 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N6   -   B13_L22_N
set_property src_info {type:XDC file:1 line:254 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U5 [get_ports {ADC1_N[5]}]
set_property src_info {type:XDC file:1 line:254 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U6 [get_ports {ADC1_P[5]}]
set_property src_info {type:XDC file:1 line:254 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_N[5]} {set_property PACKAGE_PIN U5 [get_ports {ADC1_N[5]}]} {MOD}
set_property src_info {type:XDC file:1 line:254 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_P[5]} {set_property PACKAGE_PIN U6 [get_ports {ADC1_P[5]}]} {MOD}
set_property src_info {type:XDC file:1 line:255 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P7   -   B13_L19_P
set_property src_info {type:XDC file:1 line:256 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N7   -   B13_L19_N
set_property src_info {type:XDC file:1 line:258 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN T6 [get_ports {ADC1_N[6]}]
set_property src_info {type:XDC file:1 line:258 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN R6 [get_ports {ADC1_P[6]}]
set_property src_info {type:XDC file:1 line:258 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_N[6]} {set_property PACKAGE_PIN T6 [get_ports {ADC1_N[6]}]} {MOD}
set_property src_info {type:XDC file:1 line:258 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_P[6]} {set_property PACKAGE_PIN R6 [get_ports {ADC1_P[6]}]} {MOD}
set_property src_info {type:XDC file:1 line:259 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P8   -   B13_L23_P
set_property src_info {type:XDC file:1 line:260 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N8   -   B13_L23_N
set_property src_info {type:XDC file:1 line:262 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W7 [get_ports {ADC1_N[7]}]
set_property src_info {type:XDC file:1 line:262 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V7 [get_ports {ADC1_P[7]}]
set_property src_info {type:XDC file:1 line:262 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_N[7]} {set_property PACKAGE_PIN W7 [get_ports {ADC1_N[7]}]} {MOD}
set_property src_info {type:XDC file:1 line:262 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC1_P[7]} {set_property PACKAGE_PIN V7 [get_ports {ADC1_P[7]}]} {MOD}
set_property src_info {type:XDC file:1 line:263 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P9   -   B13_L20_P
set_property src_info {type:XDC file:1 line:264 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N9   -   B13_L20_N
set_property src_info {type:XDC file:1 line:266 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U4 [get_ports {ADC2_N[0]}]
set_property src_info {type:XDC file:1 line:266 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN T4 [get_ports {ADC2_P[0]}]
set_property src_info {type:XDC file:1 line:266 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_N[0]} {set_property PACKAGE_PIN U4 [get_ports {ADC2_N[0]}]} {MOD}
set_property src_info {type:XDC file:1 line:266 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_P[0]} {set_property PACKAGE_PIN T4 [get_ports {ADC2_P[0]}]} {MOD}
set_property src_info {type:XDC file:1 line:267 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P10   -   B13_L24_P
set_property src_info {type:XDC file:1 line:268 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N10   -   B13_L24_N
set_property src_info {type:XDC file:1 line:270 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W5 [get_ports {ADC2_N[1]}]
set_property src_info {type:XDC file:1 line:270 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W6 [get_ports {ADC2_P[1]}]
set_property src_info {type:XDC file:1 line:270 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_N[1]} {set_property PACKAGE_PIN W5 [get_ports {ADC2_N[1]}]} {MOD}
set_property src_info {type:XDC file:1 line:270 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_P[1]} {set_property PACKAGE_PIN W6 [get_ports {ADC2_P[1]}]} {MOD}
set_property src_info {type:XDC file:1 line:271 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P11   -   B13_L18_P
set_property src_info {type:XDC file:1 line:272 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N11   -   B13_L18_N
set_property src_info {type:XDC file:1 line:274 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA4 [get_ports {ADC2_N[2]}]
set_property src_info {type:XDC file:1 line:274 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y4 [get_ports {ADC2_P[2]}]
set_property src_info {type:XDC file:1 line:274 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_N[2]} {set_property PACKAGE_PIN AA4 [get_ports {ADC2_N[2]}]} {MOD}
set_property src_info {type:XDC file:1 line:274 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_P[2]} {set_property PACKAGE_PIN Y4 [get_ports {ADC2_P[2]}]} {MOD}
set_property src_info {type:XDC file:1 line:275 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P12   -   B13_L21_P
set_property src_info {type:XDC file:1 line:276 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N12   -   B13_L21_N
set_property src_info {type:XDC file:1 line:278 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V4 [get_ports {ADC2_N[3]}]
set_property src_info {type:XDC file:1 line:278 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V5 [get_ports {ADC2_P[3]}]
set_property src_info {type:XDC file:1 line:278 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_N[3]} {set_property PACKAGE_PIN V4 [get_ports {ADC2_N[3]}]} {MOD}
set_property src_info {type:XDC file:1 line:278 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_P[3]} {set_property PACKAGE_PIN V5 [get_ports {ADC2_P[3]}]} {MOD}
set_property src_info {type:XDC file:1 line:279 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P13   -   B13_L4_P
set_property src_info {type:XDC file:1 line:280 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N13   -   B13_L4_N
set_property src_info {type:XDC file:1 line:282 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W12 [get_ports {ADC2_N[4]}]
set_property src_info {type:XDC file:1 line:282 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V12 [get_ports {ADC2_P[4]}]
set_property src_info {type:XDC file:1 line:282 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_N[4]} {set_property PACKAGE_PIN W12 [get_ports {ADC2_N[4]}]} {MOD}
set_property src_info {type:XDC file:1 line:282 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_P[4]} {set_property PACKAGE_PIN V12 [get_ports {ADC2_P[4]}]} {MOD}
set_property src_info {type:XDC file:1 line:283 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P14   -   B13_L10_P
set_property src_info {type:XDC file:1 line:284 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N14   -   B13_L10_N
set_property src_info {type:XDC file:1 line:286 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y10 [get_ports {ADC2_N[5]}]
set_property src_info {type:XDC file:1 line:286 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN Y11 [get_ports {ADC2_P[5]}]
set_property src_info {type:XDC file:1 line:286 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_N[5]} {set_property PACKAGE_PIN Y10 [get_ports {ADC2_N[5]}]} {MOD}
set_property src_info {type:XDC file:1 line:286 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_P[5]} {set_property PACKAGE_PIN Y11 [get_ports {ADC2_P[5]}]} {MOD}
set_property src_info {type:XDC file:1 line:287 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P15   -   B13_L3_P
set_property src_info {type:XDC file:1 line:288 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N15   -   B13_L3_N
set_property src_info {type:XDC file:1 line:290 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W10 [get_ports {ADC2_N[6]}]
set_property src_info {type:XDC file:1 line:290 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W11 [get_ports {ADC2_P[6]}]
set_property src_info {type:XDC file:1 line:290 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_N[6]} {set_property PACKAGE_PIN W10 [get_ports {ADC2_N[6]}]} {MOD}
set_property src_info {type:XDC file:1 line:290 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_P[6]} {set_property PACKAGE_PIN W11 [get_ports {ADC2_P[6]}]} {MOD}
set_property src_info {type:XDC file:1 line:291 export:INPUT save:INPUT read:READ} [current_design]
#ADC_P16   -   B13_L2_P
set_property src_info {type:XDC file:1 line:292 export:INPUT save:INPUT read:READ} [current_design]
#ADC_N16   -   B13_L2_N
set_property src_info {type:XDC file:1 line:294 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN W8 [get_ports {ADC2_N[7]}]
set_property src_info {type:XDC file:1 line:294 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V8 [get_ports {ADC2_P[7]}]
set_property src_info {type:XDC file:1 line:294 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_N[7]} {set_property PACKAGE_PIN W8 [get_ports {ADC2_N[7]}]} {MOD}
set_property src_info {type:XDC file:1 line:294 export:INPUT save:INPUT read:READ} [current_design]
# tool_modified_constraint_info {PC_IO_PIN:TERMINAL:ADC2_P[7]} {set_property PACKAGE_PIN V8 [get_ports {ADC2_P[7]}]} {MOD}
set_property src_info {type:XDC file:1 line:295 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVDS_25 [get_ports ADC1*]
set_property src_info {type:XDC file:1 line:296 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVDS_25 [get_ports ADC2*]
set_property src_info {type:XDC file:1 line:297 export:INPUT save:INPUT read:READ} [current_design]
set_property DIFF_TERM true [get_ports ADC1*]
set_property src_info {type:XDC file:1 line:298 export:INPUT save:INPUT read:READ} [current_design]
set_property DIFF_TERM true [get_ports ADC1*]
set_property src_info {type:XDC file:1 line:299 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:300 export:INPUT save:INPUT read:READ} [current_design]
#CLOCK_S_ADC    -   B33_IO35
set_property src_info {type:XDC file:1 line:301 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB16 [get_ports SPI_sck]
set_property src_info {type:XDC file:1 line:302 export:INPUT save:INPUT read:READ} [current_design]
#MOSI_S_ADC    -   B33_IO43
set_property src_info {type:XDC file:1 line:303 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA14 [get_ports SPI_mosi]
set_property src_info {type:XDC file:1 line:304 export:INPUT save:INPUT read:READ} [current_design]
#CS_S_ADC    -   B33_IO47
set_property src_info {type:XDC file:1 line:305 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB15 [get_ports SPI_ss_n]
set_property src_info {type:XDC file:1 line:306 export:INPUT save:INPUT read:READ} [current_design]
#MISO_S_ADC1    -   B33_IO45
set_property src_info {type:XDC file:1 line:307 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA13 [get_ports {SPI_miso[0]}]
set_property src_info {type:XDC file:1 line:308 export:INPUT save:INPUT read:READ} [current_design]
#MISO_S_ADC2    -   B33_IO46
set_property src_info {type:XDC file:1 line:309 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB14 [get_ports {SPI_miso[1]}]
set_property src_info {type:XDC file:1 line:310 export:INPUT save:INPUT read:READ} [current_design]
#MISO_S_ADC3    -   B33_IO34
set_property src_info {type:XDC file:1 line:311 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA16 [get_ports {SPI_miso[2]}]
set_property src_info {type:XDC file:1 line:312 export:INPUT save:INPUT read:READ} [current_design]
#MISO_S_ADC4    -   B33_IO33
set_property src_info {type:XDC file:1 line:313 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB17 [get_ports {SPI_miso[3]}]
set_property src_info {type:XDC file:1 line:314 export:INPUT save:INPUT read:READ} [current_design]
#MISO_S_ADC5    -   B33_IO23
set_property src_info {type:XDC file:1 line:315 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA18 [get_ports {SPI_miso[4]}]
set_property src_info {type:XDC file:1 line:316 export:INPUT save:INPUT read:READ} [current_design]
#MISO_S_ADC6    -   B33_IO32
set_property src_info {type:XDC file:1 line:317 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA17 [get_ports {SPI_miso[5]}]
set_property src_info {type:XDC file:1 line:318 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports SPI_*]
set_property src_info {type:XDC file:1 line:319 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:320 export:INPUT save:INPUT read:READ} [current_design]
#TRIP_GEN	-	B35_IO28
set_property src_info {type:XDC file:1 line:321 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN D17 [get_ports trip]
set_property src_info {type:XDC file:1 line:322 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports trip]
set_property src_info {type:XDC file:1 line:323 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:324 export:INPUT save:INPUT read:READ} [current_design]
#CPLD_ready	-	B35_IO26
set_property src_info {type:XDC file:1 line:325 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN C19 [get_ports CPLD_ready]
set_property src_info {type:XDC file:1 line:326 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports CPLD_ready]
set_property src_info {type:XDC file:1 line:327 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:328 export:INPUT save:INPUT read:READ} [current_design]
#ENC1A	-	B33_IO14
set_property src_info {type:XDC file:1 line:329 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA21 [get_ports {Encoder1[0]}]
set_property src_info {type:XDC file:1 line:330 export:INPUT save:INPUT read:READ} [current_design]
#ENC1B	-	B33_IO15
set_property src_info {type:XDC file:1 line:331 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB21 [get_ports {Encoder1[1]}]
set_property src_info {type:XDC file:1 line:332 export:INPUT save:INPUT read:READ} [current_design]
#ENC1C	-	B33_IO13
set_property src_info {type:XDC file:1 line:333 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB22 [get_ports {Encoder1[2]}]
set_property src_info {type:XDC file:1 line:334 export:INPUT save:INPUT read:READ} [current_design]
#ENC2A	-	B33_IO21
set_property src_info {type:XDC file:1 line:335 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AA19 [get_ports {Encoder2[0]}]
set_property src_info {type:XDC file:1 line:336 export:INPUT save:INPUT read:READ} [current_design]
#ENC2B	-	B33_IO19
set_property src_info {type:XDC file:1 line:337 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB20 [get_ports {Encoder2[1]}]
set_property src_info {type:XDC file:1 line:338 export:INPUT save:INPUT read:READ} [current_design]
#ENC2C	-	B33_IO18
set_property src_info {type:XDC file:1 line:339 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN AB19 [get_ports {Encoder2[2]}]
set_property src_info {type:XDC file:1 line:340 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports {Encoder1[*]}]
set_property src_info {type:XDC file:1 line:341 export:INPUT save:INPUT read:READ} [current_design]
set_property IOSTANDARD LVCMOS33 [get_ports {Encoder2[*]}]
set_property src_info {type:XDC file:1 line:342 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:343 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:1 line:344 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:2 line:1 export:INPUT save:INPUT read:READ} [current_design]
#
set_property src_info {type:XDC file:2 line:2 export:INPUT save:INPUT read:READ} [current_design]
# Common bitgen related settings
set_property src_info {type:XDC file:2 line:3 export:INPUT save:INPUT read:READ} [current_design]
#
set_property src_info {type:XDC file:2 line:4 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:2 line:5 export:INPUT save:INPUT read:READ} [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property src_info {type:XDC file:2 line:6 export:INPUT save:INPUT read:READ} [current_design]
#set_property BITSTREAM.CONFIG.CONFIGRATE 66 [current_design]
set_property src_info {type:XDC file:2 line:7 export:INPUT save:INPUT read:READ} [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property src_info {type:XDC file:2 line:8 export:INPUT save:INPUT read:READ} [current_design]
set_property CFGBVS VCCO [current_design]
set_property src_info {type:XDC file:2 line:9 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:2 line:10 export:INPUT save:INPUT read:READ} [current_design]
set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]
set_property src_info {type:XDC file:2 line:11 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:2 line:12 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:2 line:13 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:2 line:14 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:3 line:1 export:INPUT save:INPUT read:READ} [current_design]
#
set_property src_info {type:XDC file:3 line:2 export:INPUT save:INPUT read:READ} [current_design]
# Set unused pin pullup: PULLNONE, PULLUP, PULLDOWN
set_property src_info {type:XDC file:3 line:3 export:INPUT save:INPUT read:READ} [current_design]
#
set_property src_info {type:XDC file:3 line:4 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:3 line:5 export:INPUT save:INPUT read:READ} [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN PULLNONE [current_design]
set_property src_info {type:XDC file:3 line:6 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:3 line:7 export:INPUT save:INPUT read:READ} [current_design]
#set_property BITSTREAM.CONFIG.UNUSEDPIN PULLUP [current_design]
set_property src_info {type:XDC file:3 line:8 export:INPUT save:INPUT read:READ} [current_design]
#set_property BITSTREAM.CONFIG.UNUSEDPIN PULLDONE [current_design]
set_property src_info {type:XDC file:3 line:9 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:3 line:10 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:3 line:11 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:3 line:12 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:3 line:13 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:3 line:14 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:3 line:15 export:INPUT save:INPUT read:READ} [current_design]

set_property src_info {type:XDC file:3 line:16 export:INPUT save:INPUT read:READ} [current_design]

