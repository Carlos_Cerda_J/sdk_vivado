// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module m2c_control_wrapper(ADC1_N, ADC1_P, ADC2_N, ADC2_P, ADCLK, ADCLK1_N, 
  ADCLK1_P, ADCLK2_N, ADCLK2_P, CPLD_ready, DDR_addr, DDR_ba, DDR_cas_n, DDR_ck_n, DDR_ck_p, DDR_cke, 
  DDR_cs_n, DDR_dm, DDR_dq, DDR_dqs_n, DDR_dqs_p, DDR_odt, DDR_ras_n, DDR_reset_n, DDR_we_n, EOC_1, 
  Encoder1, Encoder2, FO1, FO2, FO3, LCLK1_N, LCLK1_P, LCLK2_N, LCLK2_P, SPI_miso, SPI_mosi, SPI_sck, 
  SPI_ss_n, trip);
  input [0:7]ADC1_N;
  input [0:7]ADC1_P;
  input [0:7]ADC2_N;
  input [0:7]ADC2_P;
  output ADCLK;
  input ADCLK1_N;
  input ADCLK1_P;
  input ADCLK2_N;
  input ADCLK2_P;
  input CPLD_ready;
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  input EOC_1;
  input [2:0]Encoder1;
  input [2:0]Encoder2;
  output [25:0]FO1;
  output [25:0]FO2;
  output [25:0]FO3;
  input LCLK1_N;
  input LCLK1_P;
  input LCLK2_N;
  input LCLK2_P;
  input [5:0]SPI_miso;
  output SPI_mosi;
  output SPI_sck;
  output SPI_ss_n;
  input trip;
endmodule
