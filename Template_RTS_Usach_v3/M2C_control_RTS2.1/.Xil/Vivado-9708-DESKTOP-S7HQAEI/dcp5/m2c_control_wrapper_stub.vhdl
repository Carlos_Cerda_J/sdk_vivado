-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity m2c_control_wrapper is
  Port ( 
    ADC1_N : in STD_LOGIC_VECTOR ( 0 to 7 );
    ADC1_P : in STD_LOGIC_VECTOR ( 0 to 7 );
    ADC2_N : in STD_LOGIC_VECTOR ( 0 to 7 );
    ADC2_P : in STD_LOGIC_VECTOR ( 0 to 7 );
    ADCLK : out STD_LOGIC;
    ADCLK1_N : in STD_LOGIC;
    ADCLK1_P : in STD_LOGIC;
    ADCLK2_N : in STD_LOGIC;
    ADCLK2_P : in STD_LOGIC;
    CPLD_ready : in STD_LOGIC;
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    EOC_1 : in STD_LOGIC;
    Encoder1 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    Encoder2 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    FO1 : out STD_LOGIC_VECTOR ( 25 downto 0 );
    FO2 : out STD_LOGIC_VECTOR ( 25 downto 0 );
    FO3 : out STD_LOGIC_VECTOR ( 25 downto 0 );
    LCLK1_N : in STD_LOGIC;
    LCLK1_P : in STD_LOGIC;
    LCLK2_N : in STD_LOGIC;
    LCLK2_P : in STD_LOGIC;
    SPI_miso : in STD_LOGIC_VECTOR ( 5 downto 0 );
    SPI_mosi : out STD_LOGIC;
    SPI_sck : out STD_LOGIC;
    SPI_ss_n : out STD_LOGIC;
    trip : in STD_LOGIC
  );

end m2c_control_wrapper;

architecture stub of m2c_control_wrapper is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
begin
end;
