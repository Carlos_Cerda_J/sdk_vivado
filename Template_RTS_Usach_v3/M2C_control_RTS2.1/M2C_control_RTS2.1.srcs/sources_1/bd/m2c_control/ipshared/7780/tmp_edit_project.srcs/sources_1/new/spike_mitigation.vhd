----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/06/2020 03:04:16 PM
-- Design Name: 
-- Module Name: spike_mitigation - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity spike_mitigation is
    Port ( clock : in STD_LOGIC;
           reset_n : in STD_LOGIC;
           data_in : in STD_LOGIC_VECTOR (11 downto 0);
           data_avg : in STD_LOGIC_VECTOR (11 downto 0);
		   band	 : in STD_LOGIC_VECTOR (11 downto 0);
           data_out : out STD_LOGIC_VECTOR (11 downto 0));
end spike_mitigation;

architecture Behavioral of spike_mitigation is
 	signal data_in_signed 	: signed (11 downto 0) := (others => '0');
	signal data_avg_signed 	: signed (11 downto 0) := (others => '0');
	signal band_signed 		: signed (11 downto 0) := (others => '0');

begin

	data_in_signed 	<= signed(data_in);
	data_avg_signed <= signed(data_avg);
	band_signed		<= signed(band);
	
	process(clock, reset_n)
	begin
		if (reset_n = '0') then
			data_out <= (others=>'0');
			
		elsif rising_edge(clock) then
			if (abs(data_in_signed-data_avg_signed) < band_signed) then
				data_out <= data_in;
			else
				data_out <= data_avg;
			end if;
		end if;
	end process;

end Behavioral;
