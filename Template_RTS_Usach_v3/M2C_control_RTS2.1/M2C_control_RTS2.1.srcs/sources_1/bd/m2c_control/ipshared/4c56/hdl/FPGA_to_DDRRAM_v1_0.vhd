library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity FPGA_to_DDRRAM_v1_0 is
	generic (
		-- Users to add parameters here
		-- Width of input data Buses
		DATA_WORD_WIDTH	: integer	:= 32;		
		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Master Bus Interface M_AXI_Full
		C_M_AXI_Full_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"1FF00000";
		C_M_AXI_Full_BURST_LEN	: integer	:= 16;
		C_M_AXI_Full_ID_WIDTH	: integer	:= 1;
		C_M_AXI_Full_ADDR_WIDTH	: integer	:= 32;
		C_M_AXI_Full_DATA_WIDTH	: integer	:= 32;
		C_M_AXI_Full_AWUSER_WIDTH	: integer	:= 1;
		C_M_AXI_Full_ARUSER_WIDTH	: integer	:= 1;
		C_M_AXI_Full_WUSER_WIDTH	: integer	:= 1;
		C_M_AXI_Full_RUSER_WIDTH	: integer	:= 1;
		C_M_AXI_Full_BUSER_WIDTH	: integer	:= 1;
		
		-- Parameters of Axi Slave Bus Interface S_AXI_Lite
		C_S_AXI_Lite_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_Lite_ADDR_WIDTH	: integer	:= 32
	);
	port (
		-- Users to add ports here
		data0	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data1	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data2	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data3	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data4	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data5	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data6	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data7	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data8	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data9	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data10	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data11	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data12	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data13	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data14	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data15	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data16	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data17	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data18	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data19	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data20	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data21	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data22	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data23	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data24	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data25	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data26	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data27	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data28	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data29	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data30	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data31	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data32	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data33	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data34	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data35	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data36	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data37	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data38	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data39	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data40	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data41	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data42	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data43	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data44	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data45	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data46	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data47	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data48	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data49	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data50	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data51	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data52	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data53	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data54	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data55	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data56	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data57	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data58	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data59	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data60	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data61	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data62	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data63	: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		reset_n 	: in std_logic;
		sample_clk 	: out std_logic;
			-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Master Bus Interface M_AXI_Full
		done	: out std_logic;
		error	: out std_logic;
		m_axi_full_aclk	: in std_logic;
		m_axi_full_aresetn	: in std_logic;
		m_axi_full_awid	: out std_logic_vector(C_M_AXI_Full_ID_WIDTH-1 downto 0);
		m_axi_full_awaddr	: out std_logic_vector(C_M_AXI_Full_ADDR_WIDTH-1 downto 0);
		m_axi_full_awlen	: out std_logic_vector(7 downto 0);
		m_axi_full_awsize	: out std_logic_vector(2 downto 0);
		m_axi_full_awburst	: out std_logic_vector(1 downto 0);
		m_axi_full_awlock	: out std_logic;
		m_axi_full_awcache	: out std_logic_vector(3 downto 0);
		m_axi_full_awprot	: out std_logic_vector(2 downto 0);
		m_axi_full_awqos	: out std_logic_vector(3 downto 0);
		m_axi_full_awuser	: out std_logic_vector(C_M_AXI_Full_AWUSER_WIDTH-1 downto 0);
		m_axi_full_awvalid	: out std_logic;
		m_axi_full_awready	: in std_logic;
		m_axi_full_wdata	: out std_logic_vector(C_M_AXI_Full_DATA_WIDTH-1 downto 0);
		m_axi_full_wstrb	: out std_logic_vector(C_M_AXI_Full_DATA_WIDTH/8-1 downto 0);
		m_axi_full_wlast	: out std_logic;
		m_axi_full_wuser	: out std_logic_vector(C_M_AXI_Full_WUSER_WIDTH-1 downto 0);
		m_axi_full_wvalid	: out std_logic;
		m_axi_full_wready	: in std_logic;
		m_axi_full_bid	: in std_logic_vector(C_M_AXI_Full_ID_WIDTH-1 downto 0);
		m_axi_full_bresp	: in std_logic_vector(1 downto 0);
		m_axi_full_buser	: in std_logic_vector(C_M_AXI_Full_BUSER_WIDTH-1 downto 0);
		m_axi_full_bvalid	: in std_logic;
		m_axi_full_bready	: out std_logic;
		m_axi_full_arid	: out std_logic_vector(C_M_AXI_Full_ID_WIDTH-1 downto 0);
		m_axi_full_araddr	: out std_logic_vector(C_M_AXI_Full_ADDR_WIDTH-1 downto 0);
		m_axi_full_arlen	: out std_logic_vector(7 downto 0);
		m_axi_full_arsize	: out std_logic_vector(2 downto 0);
		m_axi_full_arburst	: out std_logic_vector(1 downto 0);
		m_axi_full_arlock	: out std_logic;
		m_axi_full_arcache	: out std_logic_vector(3 downto 0);
		m_axi_full_arprot	: out std_logic_vector(2 downto 0);
		m_axi_full_arqos	: out std_logic_vector(3 downto 0);
		m_axi_full_aruser	: out std_logic_vector(C_M_AXI_Full_ARUSER_WIDTH-1 downto 0);
		m_axi_full_arvalid	: out std_logic;
		m_axi_full_arready	: in std_logic;
		m_axi_full_rid	: in std_logic_vector(C_M_AXI_Full_ID_WIDTH-1 downto 0);
		m_axi_full_rdata	: in std_logic_vector(C_M_AXI_Full_DATA_WIDTH-1 downto 0);
		m_axi_full_rresp	: in std_logic_vector(1 downto 0);
		m_axi_full_rlast	: in std_logic;
		m_axi_full_ruser	: in std_logic_vector(C_M_AXI_Full_RUSER_WIDTH-1 downto 0);
		m_axi_full_rvalid	: in std_logic;
		m_axi_full_rready	: out std_logic;

		-- Ports of Axi Slave Bus Interface S_AXI_Lite
		s_axi_lite_aclk	: in std_logic;
		s_axi_lite_aresetn	: in std_logic;
		s_axi_lite_awaddr	: in std_logic_vector(C_S_AXI_Lite_ADDR_WIDTH-1 downto 0);
		s_axi_lite_awprot	: in std_logic_vector(2 downto 0);
		s_axi_lite_awvalid	: in std_logic;
		s_axi_lite_awready	: out std_logic;
		s_axi_lite_wdata	: in std_logic_vector(C_S_AXI_Lite_DATA_WIDTH-1 downto 0);
		s_axi_lite_wstrb	: in std_logic_vector((C_S_AXI_Lite_DATA_WIDTH/8)-1 downto 0);
		s_axi_lite_wvalid	: in std_logic;
		s_axi_lite_wready	: out std_logic;
		s_axi_lite_bresp	: out std_logic_vector(1 downto 0);
		s_axi_lite_bvalid	: out std_logic;
		s_axi_lite_bready	: in std_logic;
		s_axi_lite_araddr	: in std_logic_vector(C_S_AXI_Lite_ADDR_WIDTH-1 downto 0);
		s_axi_lite_arprot	: in std_logic_vector(2 downto 0);
		s_axi_lite_arvalid	: in std_logic;
		s_axi_lite_arready	: out std_logic;
		s_axi_lite_rdata	: out std_logic_vector(C_S_AXI_Lite_DATA_WIDTH-1 downto 0);
		s_axi_lite_rresp	: out std_logic_vector(1 downto 0);
		s_axi_lite_rvalid	: out std_logic;
		s_axi_lite_rready	: in std_logic
	);
end FPGA_to_DDRRAM_v1_0;

architecture arch_imp of FPGA_to_DDRRAM_v1_0 is
	-- inner signals
	signal	done_S				: std_logic;
	signal	error_S				: std_logic;
	--Sample clock signals
	SIGNAL	sample_clock_S	:		STD_LOGIC;
	SIGNAL	sample_counter	:		INTEGER RANGE 1 TO 8191 :=1;
	SIGNAL	sample_div		:		INTEGER RANGE 1 TO 8191 :=1;	-- minimum 15kHz, maximum 50MHz from 100MHz source clock
	signal	sample_clk_div		: std_logic_vector(12 downto 0);
	
	--data transfer to DDR signals
	signal	burst_counter		: std_logic_vector(16 downto 0);		--17 bits for 128.000 max points
	signal	enable				: std_logic;
	signal	max_data_points		: std_logic_vector(16 downto 0);
	signal	ch_quantity			: std_logic_vector(1 downto 0);
	signal	selector_data_0		: std_logic_vector(5 downto 0);
	signal	selector_data_1		: std_logic_vector(5 downto 0);
	signal	selector_data_2		: std_logic_vector(5 downto 0);
	signal	selector_data_3		: std_logic_vector(5 downto 0);
	signal	selector_data_4		: std_logic_vector(5 downto 0);
	signal	selector_data_5		: std_logic_vector(5 downto 0);
	signal	selector_data_6		: std_logic_vector(5 downto 0);
	signal	selector_data_7		: std_logic_vector(5 downto 0);
	signal	selector_data_8		: std_logic_vector(5 downto 0);
	signal	selector_data_9		: std_logic_vector(5 downto 0);
	signal	selector_data_10	: std_logic_vector(5 downto 0);
	signal	selector_data_11	: std_logic_vector(5 downto 0);
	signal	selector_data_12	: std_logic_vector(5 downto 0);
	signal	selector_data_13	: std_logic_vector(5 downto 0);
	signal	selector_data_14	: std_logic_vector(5 downto 0);
	signal	selector_data_15	: std_logic_vector(5 downto 0);	
	signal	data_out0			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out1			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out2			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out3			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out4			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out5			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out6			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out7			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out8			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out9			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out10			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out11			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out12			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out13			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out14			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	signal	data_out15			: std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
	
	-- component declaration
	component FPGA_to_DDRRAM_v1_0_M_AXI_Full is
		generic (
		DATA_WORD_WIDTH	: integer	:= 32;
		C_M_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"1FF00000";
		C_M_AXI_BURST_LEN	: integer	:= 16;
		C_M_AXI_ID_WIDTH	: integer	:= 1;
		C_M_AXI_ADDR_WIDTH	: integer	:= 32;
		C_M_AXI_DATA_WIDTH	: integer	:= 32;
		C_M_AXI_AWUSER_WIDTH	: integer	:= 1;
		C_M_AXI_ARUSER_WIDTH	: integer	:= 1;
		C_M_AXI_WUSER_WIDTH	: integer	:= 4;
		C_M_AXI_RUSER_WIDTH	: integer	:= 4;
		C_M_AXI_BUSER_WIDTH	: integer	:= 1
		);
		port (
		data_0		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_1		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_2		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_3		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_4		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_5		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_6		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_7		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_8		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_9		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_10		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_11		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_12		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_13		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_14		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);
		data_15		: in std_logic_vector(DATA_WORD_WIDTH-1 downto 0);		
		sample_clock: in std_logic;
		max_data_points	: in std_logic_vector(16 downto 0);
		burst_counter	: out std_logic_vector(16 downto 0);
		INIT_AXI_TXN	: in std_logic;
		TXN_DONE	: out std_logic;
		ERROR	: out std_logic;
		M_AXI_ACLK	: in std_logic;
		M_AXI_ARESETN	: in std_logic;
		M_AXI_AWID	: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_AWADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_AWLEN	: out std_logic_vector(7 downto 0);
		M_AXI_AWSIZE	: out std_logic_vector(2 downto 0);
		M_AXI_AWBURST	: out std_logic_vector(1 downto 0);
		M_AXI_AWLOCK	: out std_logic;
		M_AXI_AWCACHE	: out std_logic_vector(3 downto 0);
		M_AXI_AWPROT	: out std_logic_vector(2 downto 0);
		M_AXI_AWQOS	: out std_logic_vector(3 downto 0);
		M_AXI_AWUSER	: out std_logic_vector(C_M_AXI_AWUSER_WIDTH-1 downto 0);
		M_AXI_AWVALID	: out std_logic;
		M_AXI_AWREADY	: in std_logic;
		M_AXI_WDATA	: out std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_WSTRB	: out std_logic_vector(C_M_AXI_DATA_WIDTH/8-1 downto 0);
		M_AXI_WLAST	: out std_logic;
		M_AXI_WUSER	: out std_logic_vector(C_M_AXI_WUSER_WIDTH-1 downto 0);
		M_AXI_WVALID	: out std_logic;
		M_AXI_WREADY	: in std_logic;
		M_AXI_BID	: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_BRESP	: in std_logic_vector(1 downto 0);
		M_AXI_BUSER	: in std_logic_vector(C_M_AXI_BUSER_WIDTH-1 downto 0);
		M_AXI_BVALID	: in std_logic;
		M_AXI_BREADY	: out std_logic;
		M_AXI_ARID	: out std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_ARADDR	: out std_logic_vector(C_M_AXI_ADDR_WIDTH-1 downto 0);
		M_AXI_ARLEN	: out std_logic_vector(7 downto 0);
		M_AXI_ARSIZE	: out std_logic_vector(2 downto 0);
		M_AXI_ARBURST	: out std_logic_vector(1 downto 0);
		M_AXI_ARLOCK	: out std_logic;
		M_AXI_ARCACHE	: out std_logic_vector(3 downto 0);
		M_AXI_ARPROT	: out std_logic_vector(2 downto 0);
		M_AXI_ARQOS	: out std_logic_vector(3 downto 0);
		M_AXI_ARUSER	: out std_logic_vector(C_M_AXI_ARUSER_WIDTH-1 downto 0);
		M_AXI_ARVALID	: out std_logic;
		M_AXI_ARREADY	: in std_logic;
		M_AXI_RID	: in std_logic_vector(C_M_AXI_ID_WIDTH-1 downto 0);
		M_AXI_RDATA	: in std_logic_vector(C_M_AXI_DATA_WIDTH-1 downto 0);
		M_AXI_RRESP	: in std_logic_vector(1 downto 0);
		M_AXI_RLAST	: in std_logic;
		M_AXI_RUSER	: in std_logic_vector(C_M_AXI_RUSER_WIDTH-1 downto 0);
		M_AXI_RVALID	: in std_logic;
		M_AXI_RREADY	: out std_logic
		);
	end component FPGA_to_DDRRAM_v1_0_M_AXI_Full;

	component FPGA_to_DDRRAM_v1_0_S_AXI_Lite is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
		);
		port (
		done				: in  std_logic;
		error				: in  std_logic;
		burst_counter		: in  std_logic_vector(16 downto 0);
		enable				: out std_logic;
		sample_clk_div		: out std_logic_vector(12 downto 0);
		max_data_points		: out std_logic_vector(16 downto 0);
		ch_quantity			: out std_logic_vector(1 downto 0);
		selector_data_0		: out std_logic_vector(5 downto 0);
		selector_data_1		: out std_logic_vector(5 downto 0);
		selector_data_2		: out std_logic_vector(5 downto 0);
		selector_data_3		: out std_logic_vector(5 downto 0);
		selector_data_4		: out std_logic_vector(5 downto 0);
		selector_data_5		: out std_logic_vector(5 downto 0);
		selector_data_6		: out std_logic_vector(5 downto 0);
		selector_data_7		: out std_logic_vector(5 downto 0);
		selector_data_8		: out std_logic_vector(5 downto 0);
		selector_data_9		: out std_logic_vector(5 downto 0);
		selector_data_10	: out std_logic_vector(5 downto 0);
		selector_data_11	: out std_logic_vector(5 downto 0);
		selector_data_12	: out std_logic_vector(5 downto 0);
		selector_data_13	: out std_logic_vector(5 downto 0);
		selector_data_14	: out std_logic_vector(5 downto 0);
		selector_data_15	: out std_logic_vector(5 downto 0);		
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component FPGA_to_DDRRAM_v1_0_S_AXI_Lite;

begin

-- Instantiation of Axi Bus Interface M_AXI_Full
FPGA_to_DDRRAM_v1_0_M_AXI_Full_inst : FPGA_to_DDRRAM_v1_0_M_AXI_Full
	generic map (
		DATA_WORD_WIDTH	=> DATA_WORD_WIDTH,
		C_M_TARGET_SLAVE_BASE_ADDR	=> C_M_AXI_Full_TARGET_SLAVE_BASE_ADDR,
		C_M_AXI_BURST_LEN	=> C_M_AXI_Full_BURST_LEN,
		C_M_AXI_ID_WIDTH	=> C_M_AXI_Full_ID_WIDTH,
		C_M_AXI_ADDR_WIDTH	=> C_M_AXI_Full_ADDR_WIDTH,
		C_M_AXI_DATA_WIDTH	=> C_M_AXI_Full_DATA_WIDTH,
		C_M_AXI_AWUSER_WIDTH	=> C_M_AXI_Full_AWUSER_WIDTH,
		C_M_AXI_ARUSER_WIDTH	=> C_M_AXI_Full_ARUSER_WIDTH,
		C_M_AXI_WUSER_WIDTH	=> C_M_AXI_Full_WUSER_WIDTH,
		C_M_AXI_RUSER_WIDTH	=> C_M_AXI_Full_RUSER_WIDTH,
		C_M_AXI_BUSER_WIDTH	=> C_M_AXI_Full_BUSER_WIDTH
	)
	port map (
		data_0		=> data_out0,
		data_1		=> data_out1,
		data_2		=> data_out2,
		data_3		=> data_out3,
		data_4		=> data_out4,
		data_5		=> data_out5,
		data_6		=> data_out6,
		data_7		=> data_out7,
		data_8		=> data_out8,
		data_9		=> data_out9,
		data_10		=> data_out10,
		data_11		=> data_out11,
		data_12		=> data_out12,
		data_13		=> data_out13,
		data_14		=> data_out14,
		data_15		=> data_out15,
		sample_clock	=> sample_clock_S,
		max_data_points	=> max_data_points,
		burst_counter	=> burst_counter,
		INIT_AXI_TXN	=> enable,
		TXN_DONE		=> done_S,
		ERROR			=> error_S,
		M_AXI_ACLK	=> m_axi_full_aclk,
		M_AXI_ARESETN	=> m_axi_full_aresetn,
		M_AXI_AWID	=> m_axi_full_awid,
		M_AXI_AWADDR	=> m_axi_full_awaddr,
		M_AXI_AWLEN	=> m_axi_full_awlen,
		M_AXI_AWSIZE	=> m_axi_full_awsize,
		M_AXI_AWBURST	=> m_axi_full_awburst,
		M_AXI_AWLOCK	=> m_axi_full_awlock,
		M_AXI_AWCACHE	=> m_axi_full_awcache,
		M_AXI_AWPROT	=> m_axi_full_awprot,
		M_AXI_AWQOS	=> m_axi_full_awqos,
		M_AXI_AWUSER	=> m_axi_full_awuser,
		M_AXI_AWVALID	=> m_axi_full_awvalid,
		M_AXI_AWREADY	=> m_axi_full_awready,
		M_AXI_WDATA	=> m_axi_full_wdata,
		M_AXI_WSTRB	=> m_axi_full_wstrb,
		M_AXI_WLAST	=> m_axi_full_wlast,
		M_AXI_WUSER	=> m_axi_full_wuser,
		M_AXI_WVALID	=> m_axi_full_wvalid,
		M_AXI_WREADY	=> m_axi_full_wready,
		M_AXI_BID	=> m_axi_full_bid,
		M_AXI_BRESP	=> m_axi_full_bresp,
		M_AXI_BUSER	=> m_axi_full_buser,
		M_AXI_BVALID	=> m_axi_full_bvalid,
		M_AXI_BREADY	=> m_axi_full_bready,
		M_AXI_ARID	=> m_axi_full_arid,
		M_AXI_ARADDR	=> m_axi_full_araddr,
		M_AXI_ARLEN	=> m_axi_full_arlen,
		M_AXI_ARSIZE	=> m_axi_full_arsize,
		M_AXI_ARBURST	=> m_axi_full_arburst,
		M_AXI_ARLOCK	=> m_axi_full_arlock,
		M_AXI_ARCACHE	=> m_axi_full_arcache,
		M_AXI_ARPROT	=> m_axi_full_arprot,
		M_AXI_ARQOS	=> m_axi_full_arqos,
		M_AXI_ARUSER	=> m_axi_full_aruser,
		M_AXI_ARVALID	=> m_axi_full_arvalid,
		M_AXI_ARREADY	=> m_axi_full_arready,
		M_AXI_RID	=> m_axi_full_rid,
		M_AXI_RDATA	=> m_axi_full_rdata,
		M_AXI_RRESP	=> m_axi_full_rresp,
		M_AXI_RLAST	=> m_axi_full_rlast,
		M_AXI_RUSER	=> m_axi_full_ruser,
		M_AXI_RVALID	=> m_axi_full_rvalid,
		M_AXI_RREADY	=> m_axi_full_rready
	);

-- Instantiation of Axi Bus Interface S_AXI_Lite
FPGA_to_DDRRAM_v1_0_S_AXI_Lite_inst : FPGA_to_DDRRAM_v1_0_S_AXI_Lite
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S_AXI_Lite_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S_AXI_Lite_ADDR_WIDTH
	)
	port map (
		done				=> done_S,
		error				=> error_S,
		burst_counter		=> burst_counter,
		enable				=> enable,
		sample_clk_div		=> sample_clk_div,
		max_data_points		=> max_data_points,
		ch_quantity			=> ch_quantity,
		selector_data_0		=> selector_data_0,
		selector_data_1		=> selector_data_1,
		selector_data_2		=> selector_data_2,
		selector_data_3		=> selector_data_3,
		selector_data_4		=> selector_data_4,
		selector_data_5		=> selector_data_5,
		selector_data_6		=> selector_data_6,
		selector_data_7		=> selector_data_7,
		selector_data_8		=> selector_data_8,
		selector_data_9		=> selector_data_9,
		selector_data_10	=> selector_data_10,
		selector_data_11	=> selector_data_11,
		selector_data_12	=> selector_data_12,
		selector_data_13	=> selector_data_13,
		selector_data_14	=> selector_data_14,
		selector_data_15	=> selector_data_15,
		S_AXI_ACLK	=> s_axi_lite_aclk,
		S_AXI_ARESETN	=> s_axi_lite_aresetn,
		S_AXI_AWADDR	=> s_axi_lite_awaddr,
		S_AXI_AWPROT	=> s_axi_lite_awprot,
		S_AXI_AWVALID	=> s_axi_lite_awvalid,
		S_AXI_AWREADY	=> s_axi_lite_awready,
		S_AXI_WDATA	=> s_axi_lite_wdata,
		S_AXI_WSTRB	=> s_axi_lite_wstrb,
		S_AXI_WVALID	=> s_axi_lite_wvalid,
		S_AXI_WREADY	=> s_axi_lite_wready,
		S_AXI_BRESP	=> s_axi_lite_bresp,
		S_AXI_BVALID	=> s_axi_lite_bvalid,
		S_AXI_BREADY	=> s_axi_lite_bready,
		S_AXI_ARADDR	=> s_axi_lite_araddr,
		S_AXI_ARPROT	=> s_axi_lite_arprot,
		S_AXI_ARVALID	=> s_axi_lite_arvalid,
		S_AXI_ARREADY	=> s_axi_lite_arready,
		S_AXI_RDATA	=> s_axi_lite_rdata,
		S_AXI_RRESP	=> s_axi_lite_rresp,
		S_AXI_RVALID	=> s_axi_lite_rvalid,
		S_AXI_RREADY	=> s_axi_lite_rready
	);

	-- Add user logic here
	
	-- done and error signals
	done	<= done_S;
	error	<= error_S;
	
	-- Sample_clock logic
	frecuency_divider: PROCESS(m_axi_full_aclk)
    BEGIN
        IF reset_n = '0' THEN
            sample_clock_S <= '0';
            sample_counter <= 1;
        ELSIF RISING_EDGE(m_axi_full_aclk) THEN
            IF sample_counter = sample_div then
                sample_clock_S <= '1';
                sample_counter <= 1;
            ELSE
                sample_clock_S <= '0';
                sample_counter <= sample_counter + 1;
            END IF;
        END IF;
    END PROCESS frecuency_divider;
    
    --sample_div <= to_integer(unsigned(clk_div));
	sample_div <= to_integer(unsigned(sample_clk_div));
	sample_clk <= sample_clock_S;
	
	--selector switch
	with selector_data_0(5 downto 0) select data_out0 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";

	with selector_data_1(5 downto 0) select data_out1 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";

	with selector_data_2(5 downto 0) select data_out2 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";

	with selector_data_3(5 downto 0) select data_out3 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";

	with selector_data_4(5 downto 0) select data_out4 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";

	with selector_data_5(5 downto 0) select data_out5 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";	 
		 
	with selector_data_6(5 downto 0) select data_out6 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";	 
		 
	with selector_data_7(5 downto 0) select data_out7 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";	 
		 
	with selector_data_8(5 downto 0) select data_out8 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";	 
		 
	with selector_data_9(5 downto 0) select data_out9 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";	 
		 
	with selector_data_10(5 downto 0) select data_out10 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";	 
		 
	with selector_data_11(5 downto 0) select data_out11 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";	 

	with selector_data_12(5 downto 0) select data_out12 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";	 
		 
	with selector_data_13(5 downto 0) select data_out13 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";	 

	with selector_data_14(5 downto 0) select data_out14 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";	 

	with selector_data_15(5 downto 0) select data_out15 <=
		 data0  when "000000",
		 data1  when "000001",
		 data2  when "000010",
		 data3  when "000011",
		 data4  when "000100",
		 data5  when "000101",
		 data6  when "000110",
		 data7  when "000111",
		 data8  when "001000",
		 data9  when "001001",
		 data10 when "001010",
		 data11 when "001011",
		 data12 when "001100",
		 data13 when "001101",
		 data14 when "001110",
		 data15 when "001111",
		 data16 when "010000",
		 data17 when "010001",
		 data18 when "010010",
		 data19 when "010011",
		 data20 when "010100",
		 data21 when "010101",
		 data22 when "010110",
		 data23 when "010111",
		 data24 when "011000",
		 data25 when "011001",
		 data26 when "011010",
		 data27 when "011011",
		 data28 when "011100",
		 data29 when "011101",
		 data30 when "011110",
		 data31 when "011111",
		 data32 when "100000",
		 data33 when "100001",
		 data34 when "100010",
		 data35 when "100011",
		 data36 when "100100",
		 data37 when "100101",
		 data38 when "100110",
		 data39 when "100111",
		 data40 when "101000",
		 data41 when "101001",
		 data42 when "101010",
		 data43 when "101011",
		 data44 when "101100",
		 data45 when "101101",
		 data46 when "101110",
		 data47 when "101111",
		 data48 when "110000",
		 data49 when "110001",
		 data50 when "110010",
		 data51 when "110011",
		 data52 when "110100",
		 data53 when "110101",
		 data54 when "110110",
		 data55 when "110111",
		 data56 when "111000",
		 data57 when "111001",
		 data58 when "111010",
		 data59 when "111011",
		 data60 when "111100",
		 data61 when "111101",
		 data62 when "111110",
		 data63 when "111111";	 
	
	
	
	-- User logic ends

end arch_imp;
