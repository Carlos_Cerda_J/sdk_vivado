--Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
--Date        : Thu May 27 11:39:51 2021
--Host        : DESKTOP-PV4QG9A running 64-bit major release  (build 9200)
--Command     : generate_target m2c_control_wrapper.bd
--Design      : m2c_control_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity m2c_control_wrapper is
  port (
    ADC1_N : in STD_LOGIC_VECTOR ( 0 to 7 );
    ADC1_P : in STD_LOGIC_VECTOR ( 0 to 7 );
    ADC2_N : in STD_LOGIC_VECTOR ( 0 to 7 );
    ADC2_P : in STD_LOGIC_VECTOR ( 0 to 7 );
    ADCLK : out STD_LOGIC;
    ADCLK1_N : in STD_LOGIC;
    ADCLK1_P : in STD_LOGIC;
    ADCLK2_N : in STD_LOGIC;
    ADCLK2_P : in STD_LOGIC;
    CPLD_ready : in STD_LOGIC;
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    EOC_1 : in STD_LOGIC;
    Encoder1 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    Encoder2 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    FO1 : out STD_LOGIC_VECTOR ( 25 downto 0 );
    FO2 : out STD_LOGIC_VECTOR ( 25 downto 0 );
    FO3 : out STD_LOGIC_VECTOR ( 25 downto 0 );
    LCLK1_N : in STD_LOGIC;
    LCLK1_P : in STD_LOGIC;
    LCLK2_N : in STD_LOGIC;
    LCLK2_P : in STD_LOGIC;
    SPI_miso : in STD_LOGIC_VECTOR ( 5 downto 0 );
    SPI_mosi : out STD_LOGIC;
    SPI_sck : out STD_LOGIC;
    SPI_ss_n : out STD_LOGIC;
    trip : in STD_LOGIC
  );
end m2c_control_wrapper;

architecture STRUCTURE of m2c_control_wrapper is
  component m2c_control is
  port (
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    EOC_1 : in STD_LOGIC;
    Encoder1 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    Encoder2 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    trip : in STD_LOGIC;
    CPLD_ready : in STD_LOGIC;
    ADCLK : out STD_LOGIC;
    SPI_sck : out STD_LOGIC;
    SPI_mosi : out STD_LOGIC;
    SPI_ss_n : out STD_LOGIC;
    SPI_miso : in STD_LOGIC_VECTOR ( 5 downto 0 );
    LCLK2_P : in STD_LOGIC;
    LCLK2_N : in STD_LOGIC;
    ADCLK2_P : in STD_LOGIC;
    ADCLK2_N : in STD_LOGIC;
    ADCLK1_N : in STD_LOGIC;
    ADCLK1_P : in STD_LOGIC;
    LCLK1_N : in STD_LOGIC;
    LCLK1_P : in STD_LOGIC;
    ADC1_P : in STD_LOGIC_VECTOR ( 0 to 7 );
    ADC1_N : in STD_LOGIC_VECTOR ( 0 to 7 );
    ADC2_P : in STD_LOGIC_VECTOR ( 0 to 7 );
    ADC2_N : in STD_LOGIC_VECTOR ( 0 to 7 );
    FO1 : out STD_LOGIC_VECTOR ( 25 downto 0 );
    FO3 : out STD_LOGIC_VECTOR ( 25 downto 0 );
    FO2 : out STD_LOGIC_VECTOR ( 25 downto 0 )
  );
  end component m2c_control;
begin
m2c_control_i: component m2c_control
     port map (
      ADC1_N(0 to 7) => ADC1_N(0 to 7),
      ADC1_P(0 to 7) => ADC1_P(0 to 7),
      ADC2_N(0 to 7) => ADC2_N(0 to 7),
      ADC2_P(0 to 7) => ADC2_P(0 to 7),
      ADCLK => ADCLK,
      ADCLK1_N => ADCLK1_N,
      ADCLK1_P => ADCLK1_P,
      ADCLK2_N => ADCLK2_N,
      ADCLK2_P => ADCLK2_P,
      CPLD_ready => CPLD_ready,
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      EOC_1 => EOC_1,
      Encoder1(2 downto 0) => Encoder1(2 downto 0),
      Encoder2(2 downto 0) => Encoder2(2 downto 0),
      FO1(25 downto 0) => FO1(25 downto 0),
      FO2(25 downto 0) => FO2(25 downto 0),
      FO3(25 downto 0) => FO3(25 downto 0),
      LCLK1_N => LCLK1_N,
      LCLK1_P => LCLK1_P,
      LCLK2_N => LCLK2_N,
      LCLK2_P => LCLK2_P,
      SPI_miso(5 downto 0) => SPI_miso(5 downto 0),
      SPI_mosi => SPI_mosi,
      SPI_sck => SPI_sck,
      SPI_ss_n => SPI_ss_n,
      trip => trip
    );
end STRUCTURE;
