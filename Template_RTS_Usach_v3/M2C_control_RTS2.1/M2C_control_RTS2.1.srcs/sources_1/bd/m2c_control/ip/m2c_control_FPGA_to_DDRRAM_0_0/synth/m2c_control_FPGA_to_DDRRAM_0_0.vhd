-- (c) Copyright 1995-2021 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: PEFFT:user:FPGA_to_DDRRAM:7.2
-- IP Revision: 7

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY m2c_control_FPGA_to_DDRRAM_0_0 IS
  PORT (
    data0 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data1 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data2 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data3 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data4 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data5 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data6 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data7 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data8 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data9 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data10 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data11 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data12 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data13 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data14 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data15 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data16 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data17 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data18 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data19 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data20 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data21 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data22 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data23 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data24 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data25 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data26 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data27 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data28 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data29 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data30 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data31 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data32 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data33 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data34 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data35 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data36 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data37 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data38 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data39 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data40 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data41 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data42 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data43 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data44 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data45 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data46 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data47 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data48 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data49 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data50 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data51 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data52 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data53 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data54 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data55 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data56 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data57 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data58 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data59 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data60 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data61 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data62 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    data63 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    reset_n : IN STD_LOGIC;
    sample_clk : OUT STD_LOGIC;
    done : OUT STD_LOGIC;
    error : OUT STD_LOGIC;
    s_axi_lite_awaddr : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_lite_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_lite_awvalid : IN STD_LOGIC;
    s_axi_lite_awready : OUT STD_LOGIC;
    s_axi_lite_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_lite_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_lite_wvalid : IN STD_LOGIC;
    s_axi_lite_wready : OUT STD_LOGIC;
    s_axi_lite_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_lite_bvalid : OUT STD_LOGIC;
    s_axi_lite_bready : IN STD_LOGIC;
    s_axi_lite_araddr : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_lite_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s_axi_lite_arvalid : IN STD_LOGIC;
    s_axi_lite_arready : OUT STD_LOGIC;
    s_axi_lite_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_lite_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_lite_rvalid : OUT STD_LOGIC;
    s_axi_lite_rready : IN STD_LOGIC;
    s_axi_lite_aclk : IN STD_LOGIC;
    s_axi_lite_aresetn : IN STD_LOGIC;
    m_axi_full_awid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axi_full_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_full_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axi_full_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axi_full_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_full_awlock : OUT STD_LOGIC;
    m_axi_full_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_full_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axi_full_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_full_awuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axi_full_awvalid : OUT STD_LOGIC;
    m_axi_full_awready : IN STD_LOGIC;
    m_axi_full_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_full_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_full_wlast : OUT STD_LOGIC;
    m_axi_full_wuser : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_full_wvalid : OUT STD_LOGIC;
    m_axi_full_wready : IN STD_LOGIC;
    m_axi_full_bid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axi_full_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_full_buser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axi_full_bvalid : IN STD_LOGIC;
    m_axi_full_bready : OUT STD_LOGIC;
    m_axi_full_arid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axi_full_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_full_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axi_full_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axi_full_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_full_arlock : OUT STD_LOGIC;
    m_axi_full_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_full_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    m_axi_full_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_full_aruser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axi_full_arvalid : OUT STD_LOGIC;
    m_axi_full_arready : IN STD_LOGIC;
    m_axi_full_rid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    m_axi_full_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axi_full_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    m_axi_full_rlast : IN STD_LOGIC;
    m_axi_full_ruser : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    m_axi_full_rvalid : IN STD_LOGIC;
    m_axi_full_rready : OUT STD_LOGIC;
    m_axi_full_aclk : IN STD_LOGIC;
    m_axi_full_aresetn : IN STD_LOGIC
  );
END m2c_control_FPGA_to_DDRRAM_0_0;

ARCHITECTURE m2c_control_FPGA_to_DDRRAM_0_0_arch OF m2c_control_FPGA_to_DDRRAM_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF m2c_control_FPGA_to_DDRRAM_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT FPGA_to_DDRRAM_v1_0 IS
    GENERIC (
      C_S_AXI_Lite_DATA_WIDTH : INTEGER; -- Width of S_AXI data bus
      C_S_AXI_Lite_ADDR_WIDTH : INTEGER; -- Width of S_AXI address bus
      C_M_AXI_Full_TARGET_SLAVE_BASE_ADDR : STD_LOGIC_VECTOR; -- Base address of targeted slave
      C_M_AXI_Full_BURST_LEN : INTEGER; -- Burst Length. Supports 1, 2, 4, 8, 16, 32, 64, 128, 256 burst lengths
      C_M_AXI_Full_ID_WIDTH : INTEGER; -- Thread ID Width
      C_M_AXI_Full_ADDR_WIDTH : INTEGER; -- Width of Address Bus
      C_M_AXI_Full_DATA_WIDTH : INTEGER; -- Width of Data Bus
      C_M_AXI_Full_AWUSER_WIDTH : INTEGER; -- Width of User Write Address Bus
      C_M_AXI_Full_ARUSER_WIDTH : INTEGER; -- Width of User Read Address Bus
      C_M_AXI_Full_WUSER_WIDTH : INTEGER; -- Width of User Write Data Bus
      C_M_AXI_Full_RUSER_WIDTH : INTEGER; -- Width of User Read Data Bus
      C_M_AXI_Full_BUSER_WIDTH : INTEGER; -- Width of User Response Bus
      DATA_WORD_WIDTH : INTEGER
    );
    PORT (
      data0 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data1 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data2 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data3 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data4 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data5 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data6 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data7 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data8 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data9 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data10 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data11 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data12 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data13 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data14 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data15 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data16 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data17 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data18 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data19 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data20 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data21 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data22 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data23 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data24 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data25 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data26 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data27 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data28 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data29 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data30 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data31 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data32 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data33 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data34 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data35 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data36 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data37 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data38 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data39 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data40 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data41 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data42 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data43 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data44 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data45 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data46 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data47 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data48 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data49 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data50 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data51 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data52 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data53 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data54 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data55 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data56 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data57 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data58 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data59 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data60 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data61 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data62 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      data63 : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
      reset_n : IN STD_LOGIC;
      sample_clk : OUT STD_LOGIC;
      done : OUT STD_LOGIC;
      error : OUT STD_LOGIC;
      s_axi_lite_awaddr : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axi_lite_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      s_axi_lite_awvalid : IN STD_LOGIC;
      s_axi_lite_awready : OUT STD_LOGIC;
      s_axi_lite_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axi_lite_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      s_axi_lite_wvalid : IN STD_LOGIC;
      s_axi_lite_wready : OUT STD_LOGIC;
      s_axi_lite_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s_axi_lite_bvalid : OUT STD_LOGIC;
      s_axi_lite_bready : IN STD_LOGIC;
      s_axi_lite_araddr : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axi_lite_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      s_axi_lite_arvalid : IN STD_LOGIC;
      s_axi_lite_arready : OUT STD_LOGIC;
      s_axi_lite_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axi_lite_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s_axi_lite_rvalid : OUT STD_LOGIC;
      s_axi_lite_rready : IN STD_LOGIC;
      s_axi_lite_aclk : IN STD_LOGIC;
      s_axi_lite_aresetn : IN STD_LOGIC;
      m_axi_full_awid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axi_full_awaddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      m_axi_full_awlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      m_axi_full_awsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      m_axi_full_awburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      m_axi_full_awlock : OUT STD_LOGIC;
      m_axi_full_awcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      m_axi_full_awprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      m_axi_full_awqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      m_axi_full_awuser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axi_full_awvalid : OUT STD_LOGIC;
      m_axi_full_awready : IN STD_LOGIC;
      m_axi_full_wdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      m_axi_full_wstrb : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      m_axi_full_wlast : OUT STD_LOGIC;
      m_axi_full_wuser : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      m_axi_full_wvalid : OUT STD_LOGIC;
      m_axi_full_wready : IN STD_LOGIC;
      m_axi_full_bid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axi_full_bresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      m_axi_full_buser : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axi_full_bvalid : IN STD_LOGIC;
      m_axi_full_bready : OUT STD_LOGIC;
      m_axi_full_arid : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axi_full_araddr : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      m_axi_full_arlen : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      m_axi_full_arsize : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      m_axi_full_arburst : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      m_axi_full_arlock : OUT STD_LOGIC;
      m_axi_full_arcache : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      m_axi_full_arprot : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
      m_axi_full_arqos : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      m_axi_full_aruser : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axi_full_arvalid : OUT STD_LOGIC;
      m_axi_full_arready : IN STD_LOGIC;
      m_axi_full_rid : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      m_axi_full_rdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      m_axi_full_rresp : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      m_axi_full_rlast : IN STD_LOGIC;
      m_axi_full_ruser : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      m_axi_full_rvalid : IN STD_LOGIC;
      m_axi_full_rready : OUT STD_LOGIC;
      m_axi_full_aclk : IN STD_LOGIC;
      m_axi_full_aresetn : IN STD_LOGIC
    );
  END COMPONENT FPGA_to_DDRRAM_v1_0;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF m2c_control_FPGA_to_DDRRAM_0_0_arch: ARCHITECTURE IS "FPGA_to_DDRRAM_v1_0,Vivado 2017.4";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF m2c_control_FPGA_to_DDRRAM_0_0_arch : ARCHITECTURE IS "m2c_control_FPGA_to_DDRRAM_0_0,FPGA_to_DDRRAM_v1_0,{}";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axi_full_aresetn: SIGNAL IS "XIL_INTERFACENAME M_AXI_Full_RST, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 M_AXI_Full_RST RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axi_full_aclk: SIGNAL IS "XIL_INTERFACENAME M_AXI_Full_CLK, ASSOCIATED_BUSIF M_AXI_Full, ASSOCIATED_RESET m_axi_full_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN m2c_control_processing_system7_0_0_FCLK_CLK0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 M_AXI_Full_CLK CLK";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_rready: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_rvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_ruser: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full RUSER";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_rlast: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full RLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_rresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_rdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_rid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full RID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_arready: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_arvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_aruser: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full ARUSER";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_arqos: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full ARQOS";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_arprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full ARPROT";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_arcache: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full ARCACHE";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_arlock: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full ARLOCK";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_arburst: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full ARBURST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_arsize: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full ARSIZE";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_arlen: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full ARLEN";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_araddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_arid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full ARID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_bready: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_bvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_buser: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full BUSER";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_bresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_bid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full BID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_wready: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_wvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_wuser: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full WUSER";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_wlast: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full WLAST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_wstrb: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_wdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_awready: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_awvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full AWVALID";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_awuser: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full AWUSER";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_awqos: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full AWQOS";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_awprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full AWPROT";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_awcache: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full AWCACHE";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_awlock: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full AWLOCK";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_awburst: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full AWBURST";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_awsize: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full AWSIZE";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_awlen: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full AWLEN";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_awaddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full AWADDR";
  ATTRIBUTE X_INTERFACE_PARAMETER OF m_axi_full_awid: SIGNAL IS "XIL_INTERFACENAME M_AXI_Full, WIZ_DATA_WIDTH 32, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4, FREQ_HZ 100000000, ID_WIDTH 1, ADDR_WIDTH 32, AWUSER_WIDTH 1, ARUSER_WIDTH 1, WUSER_WIDTH 4, RUSER_WIDTH 4, BUSER_WIDTH 1, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 1, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 1, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 256, PHASE 0.000, CLK_DOMAIN m2c_control_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  ATTRIBUTE X_INTERFACE_INFO OF m_axi_full_awid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI_Full AWID";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axi_lite_aresetn: SIGNAL IS "XIL_INTERFACENAME S_AXI_Lite_RST, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 S_AXI_Lite_RST RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axi_lite_aclk: SIGNAL IS "XIL_INTERFACENAME S_AXI_Lite_CLK, ASSOCIATED_BUSIF S_AXI_Lite, ASSOCIATED_RESET s_axi_lite_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN m2c_control_processing_system7_0_0_FCLK_CLK0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 S_AXI_Lite_CLK CLK";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_rready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_rvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_rresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_rdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_arready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_arvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_arprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite ARPROT";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_araddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_bready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_bvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_bresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_wready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_wvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_wstrb: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_wdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_awready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_awvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite AWVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_awprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite AWPROT";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axi_lite_awaddr: SIGNAL IS "XIL_INTERFACENAME S_AXI_Lite, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 12, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 32, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN m2c_control_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_lite_awaddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_Lite AWADDR";
  ATTRIBUTE X_INTERFACE_PARAMETER OF sample_clk: SIGNAL IS "XIL_INTERFACENAME sample_clk, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN m2c_control_FPGA_to_DDRRAM_0_0_sample_clk";
  ATTRIBUTE X_INTERFACE_INFO OF sample_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 sample_clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF reset_n: SIGNAL IS "XIL_INTERFACENAME reset_n, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF reset_n: SIGNAL IS "xilinx.com:signal:reset:1.0 reset_n RST";
BEGIN
  U0 : FPGA_to_DDRRAM_v1_0
    GENERIC MAP (
      C_S_AXI_Lite_DATA_WIDTH => 32,
      C_S_AXI_Lite_ADDR_WIDTH => 32,
      C_M_AXI_Full_TARGET_SLAVE_BASE_ADDR => X"1F800000",
      C_M_AXI_Full_BURST_LEN => 16,
      C_M_AXI_Full_ID_WIDTH => 1,
      C_M_AXI_Full_ADDR_WIDTH => 32,
      C_M_AXI_Full_DATA_WIDTH => 32,
      C_M_AXI_Full_AWUSER_WIDTH => 1,
      C_M_AXI_Full_ARUSER_WIDTH => 1,
      C_M_AXI_Full_WUSER_WIDTH => 4,
      C_M_AXI_Full_RUSER_WIDTH => 4,
      C_M_AXI_Full_BUSER_WIDTH => 1,
      DATA_WORD_WIDTH => 25
    )
    PORT MAP (
      data0 => data0,
      data1 => data1,
      data2 => data2,
      data3 => data3,
      data4 => data4,
      data5 => data5,
      data6 => data6,
      data7 => data7,
      data8 => data8,
      data9 => data9,
      data10 => data10,
      data11 => data11,
      data12 => data12,
      data13 => data13,
      data14 => data14,
      data15 => data15,
      data16 => data16,
      data17 => data17,
      data18 => data18,
      data19 => data19,
      data20 => data20,
      data21 => data21,
      data22 => data22,
      data23 => data23,
      data24 => data24,
      data25 => data25,
      data26 => data26,
      data27 => data27,
      data28 => data28,
      data29 => data29,
      data30 => data30,
      data31 => data31,
      data32 => data32,
      data33 => data33,
      data34 => data34,
      data35 => data35,
      data36 => data36,
      data37 => data37,
      data38 => data38,
      data39 => data39,
      data40 => data40,
      data41 => data41,
      data42 => data42,
      data43 => data43,
      data44 => data44,
      data45 => data45,
      data46 => data46,
      data47 => data47,
      data48 => data48,
      data49 => data49,
      data50 => data50,
      data51 => data51,
      data52 => data52,
      data53 => data53,
      data54 => data54,
      data55 => data55,
      data56 => data56,
      data57 => data57,
      data58 => data58,
      data59 => data59,
      data60 => data60,
      data61 => data61,
      data62 => data62,
      data63 => data63,
      reset_n => reset_n,
      sample_clk => sample_clk,
      done => done,
      error => error,
      s_axi_lite_awaddr => s_axi_lite_awaddr,
      s_axi_lite_awprot => s_axi_lite_awprot,
      s_axi_lite_awvalid => s_axi_lite_awvalid,
      s_axi_lite_awready => s_axi_lite_awready,
      s_axi_lite_wdata => s_axi_lite_wdata,
      s_axi_lite_wstrb => s_axi_lite_wstrb,
      s_axi_lite_wvalid => s_axi_lite_wvalid,
      s_axi_lite_wready => s_axi_lite_wready,
      s_axi_lite_bresp => s_axi_lite_bresp,
      s_axi_lite_bvalid => s_axi_lite_bvalid,
      s_axi_lite_bready => s_axi_lite_bready,
      s_axi_lite_araddr => s_axi_lite_araddr,
      s_axi_lite_arprot => s_axi_lite_arprot,
      s_axi_lite_arvalid => s_axi_lite_arvalid,
      s_axi_lite_arready => s_axi_lite_arready,
      s_axi_lite_rdata => s_axi_lite_rdata,
      s_axi_lite_rresp => s_axi_lite_rresp,
      s_axi_lite_rvalid => s_axi_lite_rvalid,
      s_axi_lite_rready => s_axi_lite_rready,
      s_axi_lite_aclk => s_axi_lite_aclk,
      s_axi_lite_aresetn => s_axi_lite_aresetn,
      m_axi_full_awid => m_axi_full_awid,
      m_axi_full_awaddr => m_axi_full_awaddr,
      m_axi_full_awlen => m_axi_full_awlen,
      m_axi_full_awsize => m_axi_full_awsize,
      m_axi_full_awburst => m_axi_full_awburst,
      m_axi_full_awlock => m_axi_full_awlock,
      m_axi_full_awcache => m_axi_full_awcache,
      m_axi_full_awprot => m_axi_full_awprot,
      m_axi_full_awqos => m_axi_full_awqos,
      m_axi_full_awuser => m_axi_full_awuser,
      m_axi_full_awvalid => m_axi_full_awvalid,
      m_axi_full_awready => m_axi_full_awready,
      m_axi_full_wdata => m_axi_full_wdata,
      m_axi_full_wstrb => m_axi_full_wstrb,
      m_axi_full_wlast => m_axi_full_wlast,
      m_axi_full_wuser => m_axi_full_wuser,
      m_axi_full_wvalid => m_axi_full_wvalid,
      m_axi_full_wready => m_axi_full_wready,
      m_axi_full_bid => m_axi_full_bid,
      m_axi_full_bresp => m_axi_full_bresp,
      m_axi_full_buser => m_axi_full_buser,
      m_axi_full_bvalid => m_axi_full_bvalid,
      m_axi_full_bready => m_axi_full_bready,
      m_axi_full_arid => m_axi_full_arid,
      m_axi_full_araddr => m_axi_full_araddr,
      m_axi_full_arlen => m_axi_full_arlen,
      m_axi_full_arsize => m_axi_full_arsize,
      m_axi_full_arburst => m_axi_full_arburst,
      m_axi_full_arlock => m_axi_full_arlock,
      m_axi_full_arcache => m_axi_full_arcache,
      m_axi_full_arprot => m_axi_full_arprot,
      m_axi_full_arqos => m_axi_full_arqos,
      m_axi_full_aruser => m_axi_full_aruser,
      m_axi_full_arvalid => m_axi_full_arvalid,
      m_axi_full_arready => m_axi_full_arready,
      m_axi_full_rid => m_axi_full_rid,
      m_axi_full_rdata => m_axi_full_rdata,
      m_axi_full_rresp => m_axi_full_rresp,
      m_axi_full_rlast => m_axi_full_rlast,
      m_axi_full_ruser => m_axi_full_ruser,
      m_axi_full_rvalid => m_axi_full_rvalid,
      m_axi_full_rready => m_axi_full_rready,
      m_axi_full_aclk => m_axi_full_aclk,
      m_axi_full_aresetn => m_axi_full_aresetn
    );
END m2c_control_FPGA_to_DDRRAM_0_0_arch;
