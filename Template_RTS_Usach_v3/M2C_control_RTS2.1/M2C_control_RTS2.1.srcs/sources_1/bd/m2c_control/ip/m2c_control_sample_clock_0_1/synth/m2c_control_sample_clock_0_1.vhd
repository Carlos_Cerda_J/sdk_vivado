-- (c) Copyright 1995-2021 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:user:sample_clock:2.0
-- IP Revision: 4

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY m2c_control_sample_clock_0_1 IS
  PORT (
    clk : IN STD_LOGIC;
    reset_n : IN STD_LOGIC;
    sample : OUT STD_LOGIC;
    s0_axi_lite_awaddr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s0_axi_lite_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s0_axi_lite_awvalid : IN STD_LOGIC;
    s0_axi_lite_awready : OUT STD_LOGIC;
    s0_axi_lite_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s0_axi_lite_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s0_axi_lite_wvalid : IN STD_LOGIC;
    s0_axi_lite_wready : OUT STD_LOGIC;
    s0_axi_lite_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s0_axi_lite_bvalid : OUT STD_LOGIC;
    s0_axi_lite_bready : IN STD_LOGIC;
    s0_axi_lite_araddr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s0_axi_lite_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    s0_axi_lite_arvalid : IN STD_LOGIC;
    s0_axi_lite_arready : OUT STD_LOGIC;
    s0_axi_lite_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s0_axi_lite_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s0_axi_lite_rvalid : OUT STD_LOGIC;
    s0_axi_lite_rready : IN STD_LOGIC;
    s0_axi_lite_aclk : IN STD_LOGIC;
    s0_axi_lite_aresetn : IN STD_LOGIC
  );
END m2c_control_sample_clock_0_1;

ARCHITECTURE m2c_control_sample_clock_0_1_arch OF m2c_control_sample_clock_0_1 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF m2c_control_sample_clock_0_1_arch: ARCHITECTURE IS "yes";
  COMPONENT sample_clock_v2_0 IS
    GENERIC (
      C_S0_AXI_Lite_DATA_WIDTH : INTEGER; -- Width of S_AXI data bus
      C_S0_AXI_Lite_ADDR_WIDTH : INTEGER -- Width of S_AXI address bus
    );
    PORT (
      clk : IN STD_LOGIC;
      reset_n : IN STD_LOGIC;
      sample : OUT STD_LOGIC;
      s0_axi_lite_awaddr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      s0_axi_lite_awprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      s0_axi_lite_awvalid : IN STD_LOGIC;
      s0_axi_lite_awready : OUT STD_LOGIC;
      s0_axi_lite_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s0_axi_lite_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      s0_axi_lite_wvalid : IN STD_LOGIC;
      s0_axi_lite_wready : OUT STD_LOGIC;
      s0_axi_lite_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s0_axi_lite_bvalid : OUT STD_LOGIC;
      s0_axi_lite_bready : IN STD_LOGIC;
      s0_axi_lite_araddr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      s0_axi_lite_arprot : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      s0_axi_lite_arvalid : IN STD_LOGIC;
      s0_axi_lite_arready : OUT STD_LOGIC;
      s0_axi_lite_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      s0_axi_lite_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s0_axi_lite_rvalid : OUT STD_LOGIC;
      s0_axi_lite_rready : IN STD_LOGIC;
      s0_axi_lite_aclk : IN STD_LOGIC;
      s0_axi_lite_aresetn : IN STD_LOGIC
    );
  END COMPONENT sample_clock_v2_0;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF m2c_control_sample_clock_0_1_arch: ARCHITECTURE IS "sample_clock_v2_0,Vivado 2017.4";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF m2c_control_sample_clock_0_1_arch : ARCHITECTURE IS "m2c_control_sample_clock_0_1,sample_clock_v2_0,{}";
  ATTRIBUTE CORE_GENERATION_INFO : STRING;
  ATTRIBUTE CORE_GENERATION_INFO OF m2c_control_sample_clock_0_1_arch: ARCHITECTURE IS "m2c_control_sample_clock_0_1,sample_clock_v2_0,{x_ipProduct=Vivado 2017.4,x_ipVendor=xilinx.com,x_ipLibrary=user,x_ipName=sample_clock,x_ipVersion=2.0,x_ipCoreRevision=4,x_ipLanguage=VHDL,x_ipSimLanguage=MIXED,C_S0_AXI_Lite_DATA_WIDTH=32,C_S0_AXI_Lite_ADDR_WIDTH=4}";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF s0_axi_lite_aresetn: SIGNAL IS "XIL_INTERFACENAME S0_AXI_Lite_RST, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 S0_AXI_Lite_RST RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s0_axi_lite_aclk: SIGNAL IS "XIL_INTERFACENAME S0_AXI_Lite_CLK, ASSOCIATED_BUSIF S0_AXI_Lite, ASSOCIATED_RESET s0_axi_lite_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN m2c_control_processing_system7_0_0_FCLK_CLK0";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 S0_AXI_Lite_CLK CLK";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_rready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_rvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_rresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_rdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_arready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_arvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_arprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite ARPROT";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_araddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_bready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_bvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_bresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_wready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_wvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_wstrb: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_wdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_awready: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_awvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite AWVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_awprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite AWPROT";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s0_axi_lite_awaddr: SIGNAL IS "XIL_INTERFACENAME S0_AXI_Lite, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN m2c_control_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  ATTRIBUTE X_INTERFACE_INFO OF s0_axi_lite_awaddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 S0_AXI_Lite AWADDR";
  ATTRIBUTE X_INTERFACE_PARAMETER OF reset_n: SIGNAL IS "XIL_INTERFACENAME reset_n, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF reset_n: SIGNAL IS "xilinx.com:signal:reset:1.0 reset_n RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF clk: SIGNAL IS "XIL_INTERFACENAME clk, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN m2c_control_processing_system7_0_0_FCLK_CLK0";
  ATTRIBUTE X_INTERFACE_INFO OF clk: SIGNAL IS "xilinx.com:signal:clock:1.0 clk CLK";
BEGIN
  U0 : sample_clock_v2_0
    GENERIC MAP (
      C_S0_AXI_Lite_DATA_WIDTH => 32,
      C_S0_AXI_Lite_ADDR_WIDTH => 4
    )
    PORT MAP (
      clk => clk,
      reset_n => reset_n,
      sample => sample,
      s0_axi_lite_awaddr => s0_axi_lite_awaddr,
      s0_axi_lite_awprot => s0_axi_lite_awprot,
      s0_axi_lite_awvalid => s0_axi_lite_awvalid,
      s0_axi_lite_awready => s0_axi_lite_awready,
      s0_axi_lite_wdata => s0_axi_lite_wdata,
      s0_axi_lite_wstrb => s0_axi_lite_wstrb,
      s0_axi_lite_wvalid => s0_axi_lite_wvalid,
      s0_axi_lite_wready => s0_axi_lite_wready,
      s0_axi_lite_bresp => s0_axi_lite_bresp,
      s0_axi_lite_bvalid => s0_axi_lite_bvalid,
      s0_axi_lite_bready => s0_axi_lite_bready,
      s0_axi_lite_araddr => s0_axi_lite_araddr,
      s0_axi_lite_arprot => s0_axi_lite_arprot,
      s0_axi_lite_arvalid => s0_axi_lite_arvalid,
      s0_axi_lite_arready => s0_axi_lite_arready,
      s0_axi_lite_rdata => s0_axi_lite_rdata,
      s0_axi_lite_rresp => s0_axi_lite_rresp,
      s0_axi_lite_rvalid => s0_axi_lite_rvalid,
      s0_axi_lite_rready => s0_axi_lite_rready,
      s0_axi_lite_aclk => s0_axi_lite_aclk,
      s0_axi_lite_aresetn => s0_axi_lite_aresetn
    );
END m2c_control_sample_clock_0_1_arch;
