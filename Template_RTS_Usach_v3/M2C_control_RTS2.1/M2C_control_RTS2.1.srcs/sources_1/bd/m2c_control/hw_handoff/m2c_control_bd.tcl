
################################################################
# This is a generated script based on design: m2c_control
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2017.4
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source m2c_control_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z020clg484-2
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name m2c_control

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: Slow_ADC
proc create_hier_cell_Slow_ADC { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_Slow_ADC() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S0_AXI_Lite

  # Create pins
  create_bd_pin -dir I -from 5 -to 0 SPI_miso
  create_bd_pin -dir O SPI_mosi
  create_bd_pin -dir O SPI_sck
  create_bd_pin -dir O SPI_ss_n
  create_bd_pin -dir I -type clk clk
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_1_ch1
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_1_ch2
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_1_ch3
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_1_ch4
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_2_ch1
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_2_ch2
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_2_ch3
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_2_ch4
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_3_ch1
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_3_ch2
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_3_ch3
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_3_ch4
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_4_ch1
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_4_ch2
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_4_ch3
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_4_ch4
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_5_ch1
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_5_ch2
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_5_ch3
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_5_ch4
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_6_ch1
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_6_ch2
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_6_ch3
  create_bd_pin -dir O -from 11 -to 0 data_board_2_input_6_ch4
  create_bd_pin -dir I enable_measure
  create_bd_pin -dir O error
  create_bd_pin -dir O init_done
  create_bd_pin -dir O meas_done
  create_bd_pin -dir I -type rst reset_n
  create_bd_pin -dir I -type rst s0_axi_lite_aresetn

  # Create instance: ADC_max11331_AXI_0, and set properties
  set ADC_max11331_AXI_0 [ create_bd_cell -type ip -vlnv E2Tech:user:ADC_max11331_AXI:2.0 ADC_max11331_AXI_0 ]
  set_property -dict [ list \
   CONFIG.OUTPUT_WORD_WIDTH {12} \
   CONFIG.clk_div {3} \
 ] $ADC_max11331_AXI_0

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins S0_AXI_Lite] [get_bd_intf_pins ADC_max11331_AXI_0/S0_AXI_Lite]

  # Create port connections
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_1_ch1 [get_bd_pins data_board_2_input_1_ch1] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_1_ch1]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_1_ch2 [get_bd_pins data_board_2_input_1_ch2] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_1_ch2]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_1_ch3 [get_bd_pins data_board_2_input_1_ch3] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_1_ch3]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_1_ch4 [get_bd_pins data_board_2_input_1_ch4] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_1_ch4]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_2_ch1 [get_bd_pins data_board_2_input_2_ch1] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_2_ch1]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_2_ch2 [get_bd_pins data_board_2_input_2_ch2] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_2_ch2]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_2_ch3 [get_bd_pins data_board_2_input_2_ch3] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_2_ch3]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_2_ch4 [get_bd_pins data_board_2_input_2_ch4] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_2_ch4]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_3_ch1 [get_bd_pins data_board_2_input_3_ch1] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_3_ch1]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_3_ch2 [get_bd_pins data_board_2_input_3_ch2] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_3_ch2]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_3_ch3 [get_bd_pins data_board_2_input_3_ch3] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_3_ch3]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_3_ch4 [get_bd_pins data_board_2_input_3_ch4] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_3_ch4]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_4_ch1 [get_bd_pins data_board_2_input_4_ch1] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_4_ch1]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_4_ch2 [get_bd_pins data_board_2_input_4_ch2] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_4_ch2]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_4_ch3 [get_bd_pins data_board_2_input_4_ch3] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_4_ch3]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_4_ch4 [get_bd_pins data_board_2_input_4_ch4] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_4_ch4]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_5_ch1 [get_bd_pins data_board_2_input_5_ch1] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_5_ch1]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_5_ch2 [get_bd_pins data_board_2_input_5_ch2] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_5_ch2]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_5_ch3 [get_bd_pins data_board_2_input_5_ch3] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_5_ch3]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_5_ch4 [get_bd_pins data_board_2_input_5_ch4] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_5_ch4]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_6_ch1 [get_bd_pins data_board_2_input_6_ch1] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_6_ch1]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_6_ch2 [get_bd_pins data_board_2_input_6_ch2] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_6_ch2]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_6_ch3 [get_bd_pins data_board_2_input_6_ch3] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_6_ch3]
  connect_bd_net -net ADC_max11331_AXI_0_data_board_2_input_6_ch4 [get_bd_pins data_board_2_input_6_ch4] [get_bd_pins ADC_max11331_AXI_0/data_board_2_input_6_ch4]
  connect_bd_net -net ADC_max11331_AXI_0_error [get_bd_pins error] [get_bd_pins ADC_max11331_AXI_0/error]
  connect_bd_net -net ADC_max11331_AXI_0_init_done [get_bd_pins init_done] [get_bd_pins ADC_max11331_AXI_0/init_done]
  connect_bd_net -net ADC_max11331_AXI_0_meas_done [get_bd_pins meas_done] [get_bd_pins ADC_max11331_AXI_0/meas_done]
  connect_bd_net -net ADC_max11331_AXI_0_mosi [get_bd_pins SPI_mosi] [get_bd_pins ADC_max11331_AXI_0/mosi]
  connect_bd_net -net ADC_max11331_AXI_0_sclk [get_bd_pins SPI_sck] [get_bd_pins ADC_max11331_AXI_0/sclk]
  connect_bd_net -net ADC_max11331_AXI_0_ss_n [get_bd_pins SPI_ss_n] [get_bd_pins ADC_max11331_AXI_0/ss_n]
  connect_bd_net -net SPI_miso_1 [get_bd_pins SPI_miso] [get_bd_pins ADC_max11331_AXI_0/miso]
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins clk] [get_bd_pins ADC_max11331_AXI_0/clk] [get_bd_pins ADC_max11331_AXI_0/s0_axi_lite_aclk]
  connect_bd_net -net s0_axi_lite_aresetn_1 [get_bd_pins s0_axi_lite_aresetn] [get_bd_pins ADC_max11331_AXI_0/s0_axi_lite_aresetn]
  connect_bd_net -net sample_clock_1_sample [get_bd_pins enable_measure] [get_bd_pins ADC_max11331_AXI_0/enable_measure]
  connect_bd_net -net xlslice_1_Dout [get_bd_pins reset_n] [get_bd_pins ADC_max11331_AXI_0/reset_n]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: Resets
proc create_hier_cell_Resets { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_Resets() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -from 11 -to 0 Din
  create_bd_pin -dir O -from 0 -to 0 Dout
  create_bd_pin -dir O -from 0 -to 0 Dout1
  create_bd_pin -dir O -from 0 -to 0 Dout2
  create_bd_pin -dir O -from 0 -to 0 Dout3
  create_bd_pin -dir O -from 0 -to 0 Dout4
  create_bd_pin -dir O -from 0 -to 0 Dout5
  create_bd_pin -dir O -from 0 -to 0 Dout6
  create_bd_pin -dir O -from 0 -to 0 Dout7
  create_bd_pin -dir O -from 0 -to 0 Dout8
  create_bd_pin -dir O -from 0 -to 0 Dout9
  create_bd_pin -dir O -from 0 -to 0 Dout10
  create_bd_pin -dir O -from 0 -to 0 Dout11

  # Create instance: xlslice_0, and set properties
  set xlslice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {0} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {12} \
 ] $xlslice_0

  # Create instance: xlslice_1, and set properties
  set xlslice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_1 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {1} \
   CONFIG.DIN_TO {1} \
   CONFIG.DIN_WIDTH {12} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_1

  # Create instance: xlslice_2, and set properties
  set xlslice_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_2 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {2} \
   CONFIG.DIN_TO {2} \
   CONFIG.DIN_WIDTH {12} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_2

  # Create instance: xlslice_3, and set properties
  set xlslice_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_3 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {3} \
   CONFIG.DIN_TO {3} \
   CONFIG.DIN_WIDTH {12} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_3

  # Create instance: xlslice_4, and set properties
  set xlslice_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_4 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {4} \
   CONFIG.DIN_TO {4} \
   CONFIG.DIN_WIDTH {12} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_4

  # Create instance: xlslice_5, and set properties
  set xlslice_5 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_5 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {5} \
   CONFIG.DIN_TO {5} \
   CONFIG.DIN_WIDTH {12} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_5

  # Create instance: xlslice_6, and set properties
  set xlslice_6 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_6 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {6} \
   CONFIG.DIN_TO {6} \
   CONFIG.DIN_WIDTH {12} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_6

  # Create instance: xlslice_7, and set properties
  set xlslice_7 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_7 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {7} \
   CONFIG.DIN_TO {7} \
   CONFIG.DIN_WIDTH {12} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_7

  # Create instance: xlslice_8, and set properties
  set xlslice_8 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_8 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {8} \
   CONFIG.DIN_TO {8} \
   CONFIG.DIN_WIDTH {12} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_8

  # Create instance: xlslice_9, and set properties
  set xlslice_9 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_9 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {9} \
   CONFIG.DIN_TO {9} \
   CONFIG.DIN_WIDTH {12} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_9

  # Create instance: xlslice_10, and set properties
  set xlslice_10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_10 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {10} \
   CONFIG.DIN_TO {10} \
   CONFIG.DIN_WIDTH {12} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_10

  # Create instance: xlslice_11, and set properties
  set xlslice_11 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_11 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {11} \
   CONFIG.DIN_TO {11} \
   CONFIG.DIN_WIDTH {12} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_11

  # Create port connections
  connect_bd_net -net axi_gpio_reset_gpio_io_o1 [get_bd_pins Din] [get_bd_pins xlslice_0/Din] [get_bd_pins xlslice_1/Din] [get_bd_pins xlslice_10/Din] [get_bd_pins xlslice_11/Din] [get_bd_pins xlslice_2/Din] [get_bd_pins xlslice_3/Din] [get_bd_pins xlslice_4/Din] [get_bd_pins xlslice_5/Din] [get_bd_pins xlslice_6/Din] [get_bd_pins xlslice_7/Din] [get_bd_pins xlslice_8/Din] [get_bd_pins xlslice_9/Din]
  connect_bd_net -net xlslice_0_Dout [get_bd_pins Dout] [get_bd_pins xlslice_0/Dout]
  connect_bd_net -net xlslice_10_Dout [get_bd_pins Dout10] [get_bd_pins xlslice_10/Dout]
  connect_bd_net -net xlslice_11_Dout [get_bd_pins Dout11] [get_bd_pins xlslice_11/Dout]
  connect_bd_net -net xlslice_1_Dout [get_bd_pins Dout1] [get_bd_pins xlslice_1/Dout]
  connect_bd_net -net xlslice_2_Dout [get_bd_pins Dout6] [get_bd_pins xlslice_2/Dout]
  connect_bd_net -net xlslice_3_Dout [get_bd_pins Dout2] [get_bd_pins xlslice_3/Dout]
  connect_bd_net -net xlslice_4_Dout [get_bd_pins Dout3] [get_bd_pins xlslice_4/Dout]
  connect_bd_net -net xlslice_5_Dout [get_bd_pins Dout4] [get_bd_pins xlslice_5/Dout]
  connect_bd_net -net xlslice_6_Dout [get_bd_pins Dout5] [get_bd_pins xlslice_6/Dout]
  connect_bd_net -net xlslice_7_Dout [get_bd_pins Dout7] [get_bd_pins xlslice_9/Dout]
  connect_bd_net -net xlslice_7_Dout1 [get_bd_pins Dout8] [get_bd_pins xlslice_7/Dout]
  connect_bd_net -net xlslice_8_Dout [get_bd_pins Dout9] [get_bd_pins xlslice_8/Dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: FO_BOARD1
proc create_hier_cell_FO_BOARD1 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_FO_BOARD1() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -from 0 -to 0 AP11
  create_bd_pin -dir I -from 0 -to 0 AP12
  create_bd_pin -dir I -from 0 -to 0 AP21
  create_bd_pin -dir I -from 0 -to 0 AP22
  create_bd_pin -dir I -from 0 -to 0 AP31
  create_bd_pin -dir I -from 0 -to 0 AP32
  create_bd_pin -dir I -from 0 -to 0 AP41
  create_bd_pin -dir I -from 0 -to 0 AP42
  create_bd_pin -dir I -from 0 -to 0 AP51
  create_bd_pin -dir I -from 0 -to 0 AP52
  create_bd_pin -dir I -from 0 -to 0 AP61
  create_bd_pin -dir I -from 0 -to 0 AP62
  create_bd_pin -dir I -from 0 -to 0 BP11
  create_bd_pin -dir I -from 0 -to 0 BP12
  create_bd_pin -dir I -from 0 -to 0 BP21
  create_bd_pin -dir I -from 0 -to 0 BP22
  create_bd_pin -dir I -from 0 -to 0 BP31
  create_bd_pin -dir I -from 0 -to 0 BP32
  create_bd_pin -dir I -from 0 -to 0 BP41
  create_bd_pin -dir I -from 0 -to 0 BP42
  create_bd_pin -dir I -from 0 -to 0 BP51
  create_bd_pin -dir I -from 0 -to 0 BP52
  create_bd_pin -dir I -from 0 -to 0 BP61
  create_bd_pin -dir I -from 0 -to 0 BP62
  create_bd_pin -dir I -from 0 -to 0 ENABLE
  create_bd_pin -dir O -from 25 -to 0 FO1
  create_bd_pin -dir I -from 0 -to 0 PRELOAD1

  # Create instance: fo1_C, and set properties
  set fo1_C [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 fo1_C ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {26} \
 ] $fo1_C

  # Create port connections
  connect_bd_net -net CONSTANTS_1_dout [get_bd_pins BP41] [get_bd_pins fo1_C/In20]
  connect_bd_net -net CONSTANTS_1_dout1 [get_bd_pins BP42] [get_bd_pins fo1_C/In21]
  connect_bd_net -net CONSTANTS_1_dout2 [get_bd_pins BP51] [get_bd_pins fo1_C/In22]
  connect_bd_net -net CONSTANTS_1_dout3 [get_bd_pins BP52] [get_bd_pins fo1_C/In23]
  connect_bd_net -net CONSTANTS_1_dout4 [get_bd_pins BP61] [get_bd_pins fo1_C/In24]
  connect_bd_net -net CONSTANTS_1_dout5 [get_bd_pins BP62] [get_bd_pins fo1_C/In25]
  connect_bd_net -net nlc_3c_0_Sap11 [get_bd_pins AP11] [get_bd_pins fo1_C/In1]
  connect_bd_net -net nlc_3c_0_Sap12 [get_bd_pins AP12] [get_bd_pins fo1_C/In2]
  connect_bd_net -net nlc_3c_0_Sap21 [get_bd_pins AP21] [get_bd_pins fo1_C/In3]
  connect_bd_net -net nlc_3c_0_Sap22 [get_bd_pins AP22] [get_bd_pins fo1_C/In4]
  connect_bd_net -net nlc_3c_0_Sap31 [get_bd_pins AP31] [get_bd_pins fo1_C/In5]
  connect_bd_net -net nlc_3c_0_Sap32 [get_bd_pins AP32] [get_bd_pins fo1_C/In6]
  connect_bd_net -net nlc_3c_0_Sbp11 [get_bd_pins BP11] [get_bd_pins fo1_C/In14]
  connect_bd_net -net nlc_3c_0_Sbp12 [get_bd_pins BP12] [get_bd_pins fo1_C/In15]
  connect_bd_net -net nlc_3c_0_Sbp21 [get_bd_pins BP21] [get_bd_pins fo1_C/In16]
  connect_bd_net -net nlc_3c_0_Sbp22 [get_bd_pins BP22] [get_bd_pins fo1_C/In17]
  connect_bd_net -net nlc_3c_0_Sbp31 [get_bd_pins BP31] [get_bd_pins fo1_C/In18]
  connect_bd_net -net nlc_3c_0_Sbp32 [get_bd_pins BP32] [get_bd_pins fo1_C/In19]
  connect_bd_net -net trip_enable_0_enable_out [get_bd_pins ENABLE] [get_bd_pins fo1_C/In0]
  connect_bd_net -net xlconcat_3_dout [get_bd_pins FO1] [get_bd_pins fo1_C/dout]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins AP41] [get_bd_pins fo1_C/In7]
  connect_bd_net -net xlconstant_11_dout [get_bd_pins AP42] [get_bd_pins fo1_C/In8]
  connect_bd_net -net xlconstant_12_dout [get_bd_pins AP51] [get_bd_pins fo1_C/In9]
  connect_bd_net -net xlconstant_13_dout [get_bd_pins AP52] [get_bd_pins fo1_C/In10]
  connect_bd_net -net xlconstant_14_dout [get_bd_pins AP61] [get_bd_pins fo1_C/In11]
  connect_bd_net -net xlconstant_15_dout [get_bd_pins AP62] [get_bd_pins fo1_C/In12]
  connect_bd_net -net xlslice_0_Dout1 [get_bd_pins PRELOAD1] [get_bd_pins fo1_C/In13]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set DDR [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR ]

  # Create ports
  set ADC1_N [ create_bd_port -dir I -from 0 -to 7 -type data ADC1_N ]
  set ADC1_P [ create_bd_port -dir I -from 0 -to 7 -type data ADC1_P ]
  set ADC2_N [ create_bd_port -dir I -from 0 -to 7 -type data ADC2_N ]
  set ADC2_P [ create_bd_port -dir I -from 0 -to 7 -type data ADC2_P ]
  set ADCLK [ create_bd_port -dir O -type clk ADCLK ]
  set ADCLK1_N [ create_bd_port -dir I -type clk ADCLK1_N ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {20000000} \
 ] $ADCLK1_N
  set ADCLK1_P [ create_bd_port -dir I -type clk ADCLK1_P ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {20000000} \
 ] $ADCLK1_P
  set ADCLK2_N [ create_bd_port -dir I -type data ADCLK2_N ]
  set ADCLK2_P [ create_bd_port -dir I -type data ADCLK2_P ]
  set CPLD_ready [ create_bd_port -dir I -type data CPLD_ready ]
  set EOC_1 [ create_bd_port -dir I -type data EOC_1 ]
  set Encoder1 [ create_bd_port -dir I -from 2 -to 0 -type data Encoder1 ]
  set Encoder2 [ create_bd_port -dir I -from 2 -to 0 -type data Encoder2 ]
  set FO1 [ create_bd_port -dir O -from 25 -to 0 -type data FO1 ]
  set FO2 [ create_bd_port -dir O -from 25 -to 0 -type data FO2 ]
  set FO3 [ create_bd_port -dir O -from 25 -to 0 -type data FO3 ]
  set LCLK1_N [ create_bd_port -dir I -type clk LCLK1_N ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {120000000} \
 ] $LCLK1_N
  set LCLK1_P [ create_bd_port -dir I -type clk LCLK1_P ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {120000000} \
 ] $LCLK1_P
  set LCLK2_N [ create_bd_port -dir I -type data LCLK2_N ]
  set LCLK2_P [ create_bd_port -dir I -type data LCLK2_P ]
  set SPI_miso [ create_bd_port -dir I -from 5 -to 0 -type data SPI_miso ]
  set SPI_mosi [ create_bd_port -dir O -type data SPI_mosi ]
  set SPI_sck [ create_bd_port -dir O -type clk SPI_sck ]
  set SPI_ss_n [ create_bd_port -dir O -type data SPI_ss_n ]
  set trip [ create_bd_port -dir I -type data trip ]

  # Create instance: ADC_ads5272_1, and set properties
  set ADC_ads5272_1 [ create_bd_cell -type ip -vlnv PEFFT:user:ADC_ads5272:2.4 ADC_ads5272_1 ]

  # Create instance: ADC_ads5272_2, and set properties
  set ADC_ads5272_2 [ create_bd_cell -type ip -vlnv PEFFT:user:ADC_ads5272:2.4 ADC_ads5272_2 ]

  # Create instance: Conv_ADC_slow_Full_0, and set properties
  set Conv_ADC_slow_Full_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:Conv_ADC_slow_Full:1.0 Conv_ADC_slow_Full_0 ]

  set_property -dict [ list \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.NUM_READ_OUTSTANDING {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {1} \
   CONFIG.MAX_BURST_LENGTH {1} \
 ] [get_bd_intf_pins /Conv_ADC_slow_Full_0/AXI4_Lite]

  # Create instance: FO_BOARD1
  create_hier_cell_FO_BOARD1 [current_bd_instance .] FO_BOARD1

  # Create instance: FO_precarga, and set properties
  set FO_precarga [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 FO_precarga ]
  set_property -dict [ list \
   CONFIG.C_ALL_OUTPUTS {1} \
   CONFIG.C_GPIO_WIDTH {3} \
 ] $FO_precarga

  # Create instance: FPGA_to_DDRRAM_0, and set properties
  set FPGA_to_DDRRAM_0 [ create_bd_cell -type ip -vlnv PEFFT:user:FPGA_to_DDRRAM:7.2 FPGA_to_DDRRAM_0 ]
  set_property -dict [ list \
   CONFIG.C_M_AXI_Full_RUSER_WIDTH {4} \
   CONFIG.C_M_AXI_Full_TARGET_SLAVE_BASE_ADDR {0x1F800000} \
   CONFIG.C_M_AXI_Full_WUSER_WIDTH {4} \
   CONFIG.DATA_WORD_WIDTH {25} \
 ] $FPGA_to_DDRRAM_0

  # Create instance: PWM_28_HB_0, and set properties
  set PWM_28_HB_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:PWM_28_HB:1.0 PWM_28_HB_0 ]

  set_property -dict [ list \
   CONFIG.NUM_READ_OUTSTANDING {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {1} \
 ] [get_bd_intf_pins /PWM_28_HB_0/AXI4_Lite]

  # Create instance: Resets
  create_hier_cell_Resets [current_bd_instance .] Resets

  # Create instance: Slow_ADC
  create_hier_cell_Slow_ADC [current_bd_instance .] Slow_ADC

  # Create instance: average_8_samples_slow_adc_0, and set properties
  set average_8_samples_slow_adc_0 [ create_bd_cell -type ip -vlnv E2Tech:user:average_8_samples_slow_adc:2.0 average_8_samples_slow_adc_0 ]

  # Create instance: axi_gpio_0, and set properties
  set axi_gpio_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0 ]
  set_property -dict [ list \
   CONFIG.C_ALL_OUTPUTS {1} \
   CONFIG.C_GPIO_WIDTH {1} \
 ] $axi_gpio_0

  # Create instance: axi_gpio_band, and set properties
  set axi_gpio_band [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_band ]
  set_property -dict [ list \
   CONFIG.C_ALL_OUTPUTS {1} \
   CONFIG.C_GPIO_WIDTH {12} \
 ] $axi_gpio_band

  # Create instance: axi_gpio_input, and set properties
  set axi_gpio_input [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_input ]
  set_property -dict [ list \
   CONFIG.C_ALL_INPUTS {1} \
   CONFIG.C_GPIO_WIDTH {2} \
   CONFIG.C_INTERRUPT_PRESENT {1} \
   CONFIG.GPIO_BOARD_INTERFACE {Custom} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $axi_gpio_input

  # Create instance: axi_gpio_interrupt_DDRRAM, and set properties
  set axi_gpio_interrupt_DDRRAM [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_interrupt_DDRRAM ]
  set_property -dict [ list \
   CONFIG.C_ALL_INPUTS {1} \
   CONFIG.C_GPIO_WIDTH {1} \
   CONFIG.C_INTERRUPT_PRESENT {1} \
   CONFIG.GPIO_BOARD_INTERFACE {Custom} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $axi_gpio_interrupt_DDRRAM

  # Create instance: axi_gpio_reset, and set properties
  set axi_gpio_reset [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_reset ]
  set_property -dict [ list \
   CONFIG.C_ALL_OUTPUTS {1} \
   CONFIG.C_DOUT_DEFAULT {0x00000004} \
   CONFIG.C_GPIO_WIDTH {12} \
 ] $axi_gpio_reset

  # Create instance: axi_smc1, and set properties
  set axi_smc1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 axi_smc1 ]
  set_property -dict [ list \
   CONFIG.NUM_SI {1} \
 ] $axi_smc1

  # Create instance: axi_timer_interrupt_to_PS, and set properties
  set axi_timer_interrupt_to_PS [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_timer:2.0 axi_timer_interrupt_to_PS ]
  set_property -dict [ list \
   CONFIG.enable_timer2 {0} \
 ] $axi_timer_interrupt_to_PS

  # Create instance: clk_wiz_adclk, and set properties
  set clk_wiz_adclk [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:5.4 clk_wiz_adclk ]
  set_property -dict [ list \
   CONFIG.AXI_DRP {false} \
   CONFIG.CLKOUT1_DRIVES {BUFGCE} \
   CONFIG.CLKOUT1_JITTER {191.690} \
   CONFIG.CLKOUT1_PHASE_ERROR {105.461} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {20} \
   CONFIG.CLKOUT2_DRIVES {BUFGCE} \
   CONFIG.CLKOUT2_JITTER {159.475} \
   CONFIG.CLKOUT2_PHASE_ERROR {105.461} \
   CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {50} \
   CONFIG.CLKOUT2_USED {true} \
   CONFIG.CLKOUT3_DRIVES {BUFGCE} \
   CONFIG.CLKOUT4_DRIVES {BUFGCE} \
   CONFIG.CLKOUT5_DRIVES {BUFGCE} \
   CONFIG.CLKOUT6_DRIVES {BUFGCE} \
   CONFIG.CLKOUT7_DRIVES {BUFGCE} \
   CONFIG.CLK_OUT1_PORT {adclk} \
   CONFIG.FEEDBACK_SOURCE {FDBK_AUTO} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {9} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {45} \
   CONFIG.MMCM_CLKOUT1_DIVIDE {18} \
   CONFIG.MMCM_COMPENSATION {ZHOLD} \
   CONFIG.MMCM_DIVCLK_DIVIDE {1} \
   CONFIG.NUM_OUT_CLKS {2} \
   CONFIG.PHASE_DUTY_CONFIG {false} \
   CONFIG.PRIMITIVE {PLL} \
   CONFIG.PRIM_SOURCE {Single_ended_clock_capable_pin} \
   CONFIG.RESET_PORT {resetn} \
   CONFIG.RESET_TYPE {ACTIVE_LOW} \
   CONFIG.SECONDARY_SOURCE {Single_ended_clock_capable_pin} \
   CONFIG.USE_DYN_RECONFIG {false} \
   CONFIG.USE_LOCKED {false} \
   CONFIG.USE_PHASE_ALIGNMENT {true} \
   CONFIG.USE_SAFE_CLOCK_STARTUP {true} \
 ] $clk_wiz_adclk

  # Create instance: debug_8_channel_1, and set properties
  set debug_8_channel_1 [ create_bd_cell -type ip -vlnv PEFFT:user:debug_8_channel:1.3 debug_8_channel_1 ]
  set_property -dict [ list \
   CONFIG.CHANNEL_DATA_WIDTH {25} \
 ] $debug_8_channel_1

  # Create instance: ila_Average, and set properties
  set ila_Average [ create_bd_cell -type ip -vlnv xilinx.com:ip:ila:6.2 ila_Average ]
  set_property -dict [ list \
   CONFIG.C_DATA_DEPTH {8192} \
   CONFIG.C_ENABLE_ILA_AXI_MON {false} \
   CONFIG.C_MONITOR_TYPE {Native} \
   CONFIG.C_NUM_OF_PROBES {12} \
   CONFIG.C_PROBE0_WIDTH {12} \
   CONFIG.C_PROBE10_WIDTH {12} \
   CONFIG.C_PROBE11_WIDTH {12} \
   CONFIG.C_PROBE1_WIDTH {12} \
   CONFIG.C_PROBE2_WIDTH {12} \
   CONFIG.C_PROBE3_WIDTH {12} \
   CONFIG.C_PROBE4_WIDTH {12} \
   CONFIG.C_PROBE5_WIDTH {12} \
   CONFIG.C_PROBE6_WIDTH {12} \
   CONFIG.C_PROBE7_WIDTH {12} \
   CONFIG.C_PROBE8_WIDTH {12} \
   CONFIG.C_PROBE9_WIDTH {12} \
 ] $ila_Average

  # Create instance: ila_Slow_ADC, and set properties
  set ila_Slow_ADC [ create_bd_cell -type ip -vlnv xilinx.com:ip:ila:6.2 ila_Slow_ADC ]
  set_property -dict [ list \
   CONFIG.C_DATA_DEPTH {8192} \
   CONFIG.C_ENABLE_ILA_AXI_MON {false} \
   CONFIG.C_MONITOR_TYPE {Native} \
   CONFIG.C_NUM_OF_PROBES {24} \
   CONFIG.C_PROBE0_WIDTH {12} \
   CONFIG.C_PROBE10_WIDTH {12} \
   CONFIG.C_PROBE11_WIDTH {12} \
   CONFIG.C_PROBE12_WIDTH {12} \
   CONFIG.C_PROBE13_WIDTH {12} \
   CONFIG.C_PROBE14_WIDTH {12} \
   CONFIG.C_PROBE15_WIDTH {12} \
   CONFIG.C_PROBE16_WIDTH {12} \
   CONFIG.C_PROBE17_WIDTH {12} \
   CONFIG.C_PROBE18_WIDTH {12} \
   CONFIG.C_PROBE19_WIDTH {12} \
   CONFIG.C_PROBE1_WIDTH {12} \
   CONFIG.C_PROBE20_WIDTH {12} \
   CONFIG.C_PROBE21_WIDTH {12} \
   CONFIG.C_PROBE22_WIDTH {12} \
   CONFIG.C_PROBE23_WIDTH {12} \
   CONFIG.C_PROBE2_WIDTH {12} \
   CONFIG.C_PROBE3_WIDTH {12} \
   CONFIG.C_PROBE4_WIDTH {12} \
   CONFIG.C_PROBE5_WIDTH {12} \
   CONFIG.C_PROBE6_WIDTH {12} \
   CONFIG.C_PROBE7_WIDTH {12} \
   CONFIG.C_PROBE8_WIDTH {12} \
   CONFIG.C_PROBE9_WIDTH {12} \
 ] $ila_Slow_ADC

  # Create instance: processing_system7_0, and set properties
  set processing_system7_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0 ]
  set_property -dict [ list \
   CONFIG.PCW_ACT_APU_PERIPHERAL_FREQMHZ {666.666687} \
   CONFIG.PCW_ACT_CAN0_PERIPHERAL_FREQMHZ {23.8095} \
   CONFIG.PCW_ACT_CAN1_PERIPHERAL_FREQMHZ {23.8095} \
   CONFIG.PCW_ACT_CAN_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_DCI_PERIPHERAL_FREQMHZ {10.158730} \
   CONFIG.PCW_ACT_ENET0_PERIPHERAL_FREQMHZ {25.000000} \
   CONFIG.PCW_ACT_ENET1_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_FPGA0_PERIPHERAL_FREQMHZ {100.000000} \
   CONFIG.PCW_ACT_FPGA1_PERIPHERAL_FREQMHZ {50.000000} \
   CONFIG.PCW_ACT_FPGA2_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_FPGA3_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_I2C_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_ACT_PCAP_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_QSPI_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_SDIO_PERIPHERAL_FREQMHZ {100.000000} \
   CONFIG.PCW_ACT_SMC_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_SPI_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_TPIU_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_TTC0_CLK0_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC0_CLK1_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC0_CLK2_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC1_CLK0_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC1_CLK1_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC1_CLK2_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_ACT_UART_PERIPHERAL_FREQMHZ {100.000000} \
   CONFIG.PCW_ACT_USB0_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_ACT_USB1_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_ACT_WDT_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_APU_CLK_RATIO_ENABLE {6:2:1} \
   CONFIG.PCW_APU_PERIPHERAL_FREQMHZ {666.666666} \
   CONFIG.PCW_ARMPLL_CTRL_FBDIV {40} \
   CONFIG.PCW_CAN0_BASEADDR {0xE0008000} \
   CONFIG.PCW_CAN0_GRP_CLK_ENABLE {0} \
   CONFIG.PCW_CAN0_HIGHADDR {0xE0008FFF} \
   CONFIG.PCW_CAN0_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_CAN0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_CAN0_PERIPHERAL_FREQMHZ {-1} \
   CONFIG.PCW_CAN1_BASEADDR {0xE0009000} \
   CONFIG.PCW_CAN1_CAN1_IO {<Select>} \
   CONFIG.PCW_CAN1_GRP_CLK_ENABLE {0} \
   CONFIG.PCW_CAN1_HIGHADDR {0xE0009FFF} \
   CONFIG.PCW_CAN1_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_CAN1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_CAN1_PERIPHERAL_FREQMHZ {-1} \
   CONFIG.PCW_CAN_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_CAN_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_CAN_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_CAN_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_CAN_PERIPHERAL_VALID {0} \
   CONFIG.PCW_CLK0_FREQ {100000000} \
   CONFIG.PCW_CLK1_FREQ {50000000} \
   CONFIG.PCW_CLK2_FREQ {10000000} \
   CONFIG.PCW_CLK3_FREQ {10000000} \
   CONFIG.PCW_CORE0_FIQ_INTR {0} \
   CONFIG.PCW_CORE0_IRQ_INTR {0} \
   CONFIG.PCW_CORE1_FIQ_INTR {0} \
   CONFIG.PCW_CORE1_IRQ_INTR {0} \
   CONFIG.PCW_CPU_CPU_6X4X_MAX_RANGE {767} \
   CONFIG.PCW_CPU_CPU_PLL_FREQMHZ {1333.333} \
   CONFIG.PCW_CPU_PERIPHERAL_CLKSRC {ARM PLL} \
   CONFIG.PCW_CPU_PERIPHERAL_DIVISOR0 {2} \
   CONFIG.PCW_CRYSTAL_PERIPHERAL_FREQMHZ {33.333333} \
   CONFIG.PCW_DCI_PERIPHERAL_CLKSRC {DDR PLL} \
   CONFIG.PCW_DCI_PERIPHERAL_DIVISOR0 {15} \
   CONFIG.PCW_DCI_PERIPHERAL_DIVISOR1 {7} \
   CONFIG.PCW_DCI_PERIPHERAL_FREQMHZ {10.159} \
   CONFIG.PCW_DDRPLL_CTRL_FBDIV {32} \
   CONFIG.PCW_DDR_DDR_PLL_FREQMHZ {1066.667} \
   CONFIG.PCW_DDR_HPRLPR_QUEUE_PARTITION {HPR(0)/LPR(32)} \
   CONFIG.PCW_DDR_HPR_TO_CRITICAL_PRIORITY_LEVEL {15} \
   CONFIG.PCW_DDR_LPR_TO_CRITICAL_PRIORITY_LEVEL {2} \
   CONFIG.PCW_DDR_PERIPHERAL_CLKSRC {DDR PLL} \
   CONFIG.PCW_DDR_PERIPHERAL_DIVISOR0 {2} \
   CONFIG.PCW_DDR_PORT0_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT1_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT2_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT3_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_RAM_BASEADDR {0x00100000} \
   CONFIG.PCW_DDR_RAM_HIGHADDR {0x1FFFFFFF} \
   CONFIG.PCW_DDR_WRITE_TO_CRITICAL_PRIORITY_LEVEL {2} \
   CONFIG.PCW_DM_WIDTH {4} \
   CONFIG.PCW_DQS_WIDTH {4} \
   CONFIG.PCW_DQ_WIDTH {32} \
   CONFIG.PCW_ENET0_BASEADDR {0xE000B000} \
   CONFIG.PCW_ENET0_ENET0_IO {MIO 16 .. 27} \
   CONFIG.PCW_ENET0_GRP_MDIO_ENABLE {1} \
   CONFIG.PCW_ENET0_GRP_MDIO_IO {MIO 52 .. 53} \
   CONFIG.PCW_ENET0_HIGHADDR {0xE000BFFF} \
   CONFIG.PCW_ENET0_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR0 {8} \
   CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR1 {5} \
   CONFIG.PCW_ENET0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_ENET0_PERIPHERAL_FREQMHZ {100 Mbps} \
   CONFIG.PCW_ENET0_RESET_ENABLE {1} \
   CONFIG.PCW_ENET0_RESET_IO {MIO 51} \
   CONFIG.PCW_ENET1_BASEADDR {0xE000C000} \
   CONFIG.PCW_ENET1_GRP_MDIO_ENABLE {0} \
   CONFIG.PCW_ENET1_HIGHADDR {0xE000CFFF} \
   CONFIG.PCW_ENET1_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_ENET1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_ENET1_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_ENET1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_ENET1_PERIPHERAL_FREQMHZ {1000 Mbps} \
   CONFIG.PCW_ENET1_RESET_ENABLE {0} \
   CONFIG.PCW_ENET_RESET_ENABLE {1} \
   CONFIG.PCW_ENET_RESET_POLARITY {Active Low} \
   CONFIG.PCW_ENET_RESET_SELECT {Share reset pin} \
   CONFIG.PCW_EN_4K_TIMER {0} \
   CONFIG.PCW_EN_CAN0 {0} \
   CONFIG.PCW_EN_CAN1 {0} \
   CONFIG.PCW_EN_CLK0_PORT {1} \
   CONFIG.PCW_EN_CLK1_PORT {1} \
   CONFIG.PCW_EN_CLK2_PORT {0} \
   CONFIG.PCW_EN_CLK3_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG0_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG1_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG2_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG3_PORT {0} \
   CONFIG.PCW_EN_DDR {1} \
   CONFIG.PCW_EN_EMIO_CAN0 {0} \
   CONFIG.PCW_EN_EMIO_CAN1 {0} \
   CONFIG.PCW_EN_EMIO_CD_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_CD_SDIO1 {0} \
   CONFIG.PCW_EN_EMIO_ENET0 {0} \
   CONFIG.PCW_EN_EMIO_ENET1 {0} \
   CONFIG.PCW_EN_EMIO_GPIO {0} \
   CONFIG.PCW_EN_EMIO_I2C0 {0} \
   CONFIG.PCW_EN_EMIO_I2C1 {0} \
   CONFIG.PCW_EN_EMIO_MODEM_UART0 {0} \
   CONFIG.PCW_EN_EMIO_MODEM_UART1 {0} \
   CONFIG.PCW_EN_EMIO_PJTAG {0} \
   CONFIG.PCW_EN_EMIO_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_SDIO1 {0} \
   CONFIG.PCW_EN_EMIO_SPI0 {0} \
   CONFIG.PCW_EN_EMIO_SPI1 {0} \
   CONFIG.PCW_EN_EMIO_SRAM_INT {0} \
   CONFIG.PCW_EN_EMIO_TRACE {0} \
   CONFIG.PCW_EN_EMIO_TTC0 {1} \
   CONFIG.PCW_EN_EMIO_TTC1 {0} \
   CONFIG.PCW_EN_EMIO_UART0 {0} \
   CONFIG.PCW_EN_EMIO_UART1 {0} \
   CONFIG.PCW_EN_EMIO_WDT {0} \
   CONFIG.PCW_EN_EMIO_WP_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_WP_SDIO1 {0} \
   CONFIG.PCW_EN_ENET0 {1} \
   CONFIG.PCW_EN_ENET1 {0} \
   CONFIG.PCW_EN_GPIO {1} \
   CONFIG.PCW_EN_I2C0 {1} \
   CONFIG.PCW_EN_I2C1 {1} \
   CONFIG.PCW_EN_MODEM_UART0 {0} \
   CONFIG.PCW_EN_MODEM_UART1 {0} \
   CONFIG.PCW_EN_PJTAG {0} \
   CONFIG.PCW_EN_PTP_ENET0 {0} \
   CONFIG.PCW_EN_PTP_ENET1 {0} \
   CONFIG.PCW_EN_QSPI {1} \
   CONFIG.PCW_EN_RST0_PORT {1} \
   CONFIG.PCW_EN_RST1_PORT {0} \
   CONFIG.PCW_EN_RST2_PORT {0} \
   CONFIG.PCW_EN_RST3_PORT {0} \
   CONFIG.PCW_EN_SDIO0 {1} \
   CONFIG.PCW_EN_SDIO1 {0} \
   CONFIG.PCW_EN_SMC {0} \
   CONFIG.PCW_EN_SPI0 {0} \
   CONFIG.PCW_EN_SPI1 {0} \
   CONFIG.PCW_EN_TRACE {0} \
   CONFIG.PCW_EN_TTC0 {1} \
   CONFIG.PCW_EN_TTC1 {0} \
   CONFIG.PCW_EN_UART0 {1} \
   CONFIG.PCW_EN_UART1 {0} \
   CONFIG.PCW_EN_USB0 {1} \
   CONFIG.PCW_EN_USB1 {0} \
   CONFIG.PCW_EN_WDT {0} \
   CONFIG.PCW_FCLK0_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR0 {5} \
   CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR1 {2} \
   CONFIG.PCW_FCLK1_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR0 {5} \
   CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR1 {4} \
   CONFIG.PCW_FCLK2_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_FCLK2_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK3_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK3_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_FCLK3_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK_CLK0_BUF {TRUE} \
   CONFIG.PCW_FCLK_CLK1_BUF {TRUE} \
   CONFIG.PCW_FCLK_CLK2_BUF {FALSE} \
   CONFIG.PCW_FCLK_CLK3_BUF {FALSE} \
   CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_FPGA1_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_FPGA2_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_FPGA3_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_FPGA_FCLK0_ENABLE {1} \
   CONFIG.PCW_FPGA_FCLK1_ENABLE {1} \
   CONFIG.PCW_FPGA_FCLK2_ENABLE {0} \
   CONFIG.PCW_FPGA_FCLK3_ENABLE {0} \
   CONFIG.PCW_FTM_CTI_IN0 {<Select>} \
   CONFIG.PCW_FTM_CTI_IN1 {<Select>} \
   CONFIG.PCW_FTM_CTI_IN2 {<Select>} \
   CONFIG.PCW_FTM_CTI_IN3 {<Select>} \
   CONFIG.PCW_FTM_CTI_OUT0 {<Select>} \
   CONFIG.PCW_FTM_CTI_OUT1 {<Select>} \
   CONFIG.PCW_FTM_CTI_OUT2 {<Select>} \
   CONFIG.PCW_FTM_CTI_OUT3 {<Select>} \
   CONFIG.PCW_GP0_EN_MODIFIABLE_TXN {0} \
   CONFIG.PCW_GP0_NUM_READ_THREADS {4} \
   CONFIG.PCW_GP0_NUM_WRITE_THREADS {4} \
   CONFIG.PCW_GP1_EN_MODIFIABLE_TXN {0} \
   CONFIG.PCW_GP1_NUM_READ_THREADS {4} \
   CONFIG.PCW_GP1_NUM_WRITE_THREADS {4} \
   CONFIG.PCW_GPIO_BASEADDR {0xE000A000} \
   CONFIG.PCW_GPIO_EMIO_GPIO_ENABLE {0} \
   CONFIG.PCW_GPIO_EMIO_GPIO_WIDTH {64} \
   CONFIG.PCW_GPIO_HIGHADDR {0xE000AFFF} \
   CONFIG.PCW_GPIO_MIO_GPIO_ENABLE {1} \
   CONFIG.PCW_GPIO_MIO_GPIO_IO {MIO} \
   CONFIG.PCW_GPIO_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_I2C0_BASEADDR {0xE0004000} \
   CONFIG.PCW_I2C0_GRP_INT_ENABLE {0} \
   CONFIG.PCW_I2C0_HIGHADDR {0xE0004FFF} \
   CONFIG.PCW_I2C0_I2C0_IO {MIO 10 .. 11} \
   CONFIG.PCW_I2C0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_I2C0_RESET_ENABLE {0} \
   CONFIG.PCW_I2C0_RESET_IO {<Select>} \
   CONFIG.PCW_I2C1_BASEADDR {0xE0005000} \
   CONFIG.PCW_I2C1_GRP_INT_ENABLE {0} \
   CONFIG.PCW_I2C1_HIGHADDR {0xE0005FFF} \
   CONFIG.PCW_I2C1_I2C1_IO {MIO 12 .. 13} \
   CONFIG.PCW_I2C1_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_I2C1_RESET_ENABLE {0} \
   CONFIG.PCW_I2C_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_I2C_RESET_ENABLE {0} \
   CONFIG.PCW_I2C_RESET_POLARITY {Active Low} \
   CONFIG.PCW_I2C_RESET_SELECT {<Select>} \
   CONFIG.PCW_IMPORT_BOARD_PRESET {None} \
   CONFIG.PCW_INCLUDE_ACP_TRANS_CHECK {0} \
   CONFIG.PCW_INCLUDE_TRACE_BUFFER {0} \
   CONFIG.PCW_IOPLL_CTRL_FBDIV {30} \
   CONFIG.PCW_IO_IO_PLL_FREQMHZ {1000.000} \
   CONFIG.PCW_IRQ_F2P_INTR {1} \
   CONFIG.PCW_IRQ_F2P_MODE {DIRECT} \
   CONFIG.PCW_MIO_0_DIRECTION {inout} \
   CONFIG.PCW_MIO_0_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_0_PULLUP {enabled} \
   CONFIG.PCW_MIO_0_SLEW {slow} \
   CONFIG.PCW_MIO_10_DIRECTION {inout} \
   CONFIG.PCW_MIO_10_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_10_PULLUP {enabled} \
   CONFIG.PCW_MIO_10_SLEW {slow} \
   CONFIG.PCW_MIO_11_DIRECTION {inout} \
   CONFIG.PCW_MIO_11_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_11_PULLUP {enabled} \
   CONFIG.PCW_MIO_11_SLEW {slow} \
   CONFIG.PCW_MIO_12_DIRECTION {inout} \
   CONFIG.PCW_MIO_12_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_12_PULLUP {enabled} \
   CONFIG.PCW_MIO_12_SLEW {slow} \
   CONFIG.PCW_MIO_13_DIRECTION {inout} \
   CONFIG.PCW_MIO_13_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_13_PULLUP {enabled} \
   CONFIG.PCW_MIO_13_SLEW {slow} \
   CONFIG.PCW_MIO_14_DIRECTION {in} \
   CONFIG.PCW_MIO_14_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_14_PULLUP {enabled} \
   CONFIG.PCW_MIO_14_SLEW {slow} \
   CONFIG.PCW_MIO_15_DIRECTION {out} \
   CONFIG.PCW_MIO_15_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_15_PULLUP {enabled} \
   CONFIG.PCW_MIO_15_SLEW {slow} \
   CONFIG.PCW_MIO_16_DIRECTION {out} \
   CONFIG.PCW_MIO_16_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_16_PULLUP {enabled} \
   CONFIG.PCW_MIO_16_SLEW {slow} \
   CONFIG.PCW_MIO_17_DIRECTION {out} \
   CONFIG.PCW_MIO_17_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_17_PULLUP {enabled} \
   CONFIG.PCW_MIO_17_SLEW {slow} \
   CONFIG.PCW_MIO_18_DIRECTION {out} \
   CONFIG.PCW_MIO_18_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_18_PULLUP {enabled} \
   CONFIG.PCW_MIO_18_SLEW {slow} \
   CONFIG.PCW_MIO_19_DIRECTION {out} \
   CONFIG.PCW_MIO_19_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_19_PULLUP {enabled} \
   CONFIG.PCW_MIO_19_SLEW {slow} \
   CONFIG.PCW_MIO_1_DIRECTION {out} \
   CONFIG.PCW_MIO_1_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_1_PULLUP {enabled} \
   CONFIG.PCW_MIO_1_SLEW {slow} \
   CONFIG.PCW_MIO_20_DIRECTION {out} \
   CONFIG.PCW_MIO_20_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_20_PULLUP {enabled} \
   CONFIG.PCW_MIO_20_SLEW {slow} \
   CONFIG.PCW_MIO_21_DIRECTION {out} \
   CONFIG.PCW_MIO_21_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_21_PULLUP {enabled} \
   CONFIG.PCW_MIO_21_SLEW {slow} \
   CONFIG.PCW_MIO_22_DIRECTION {in} \
   CONFIG.PCW_MIO_22_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_22_PULLUP {enabled} \
   CONFIG.PCW_MIO_22_SLEW {slow} \
   CONFIG.PCW_MIO_23_DIRECTION {in} \
   CONFIG.PCW_MIO_23_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_23_PULLUP {enabled} \
   CONFIG.PCW_MIO_23_SLEW {slow} \
   CONFIG.PCW_MIO_24_DIRECTION {in} \
   CONFIG.PCW_MIO_24_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_24_PULLUP {enabled} \
   CONFIG.PCW_MIO_24_SLEW {slow} \
   CONFIG.PCW_MIO_25_DIRECTION {in} \
   CONFIG.PCW_MIO_25_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_25_PULLUP {enabled} \
   CONFIG.PCW_MIO_25_SLEW {slow} \
   CONFIG.PCW_MIO_26_DIRECTION {in} \
   CONFIG.PCW_MIO_26_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_26_PULLUP {enabled} \
   CONFIG.PCW_MIO_26_SLEW {slow} \
   CONFIG.PCW_MIO_27_DIRECTION {in} \
   CONFIG.PCW_MIO_27_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_27_PULLUP {enabled} \
   CONFIG.PCW_MIO_27_SLEW {slow} \
   CONFIG.PCW_MIO_28_DIRECTION {inout} \
   CONFIG.PCW_MIO_28_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_28_PULLUP {enabled} \
   CONFIG.PCW_MIO_28_SLEW {slow} \
   CONFIG.PCW_MIO_29_DIRECTION {in} \
   CONFIG.PCW_MIO_29_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_29_PULLUP {enabled} \
   CONFIG.PCW_MIO_29_SLEW {slow} \
   CONFIG.PCW_MIO_2_DIRECTION {inout} \
   CONFIG.PCW_MIO_2_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_2_PULLUP {disabled} \
   CONFIG.PCW_MIO_2_SLEW {slow} \
   CONFIG.PCW_MIO_30_DIRECTION {out} \
   CONFIG.PCW_MIO_30_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_30_PULLUP {enabled} \
   CONFIG.PCW_MIO_30_SLEW {slow} \
   CONFIG.PCW_MIO_31_DIRECTION {in} \
   CONFIG.PCW_MIO_31_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_31_PULLUP {enabled} \
   CONFIG.PCW_MIO_31_SLEW {slow} \
   CONFIG.PCW_MIO_32_DIRECTION {inout} \
   CONFIG.PCW_MIO_32_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_32_PULLUP {enabled} \
   CONFIG.PCW_MIO_32_SLEW {slow} \
   CONFIG.PCW_MIO_33_DIRECTION {inout} \
   CONFIG.PCW_MIO_33_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_33_PULLUP {enabled} \
   CONFIG.PCW_MIO_33_SLEW {slow} \
   CONFIG.PCW_MIO_34_DIRECTION {inout} \
   CONFIG.PCW_MIO_34_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_34_PULLUP {enabled} \
   CONFIG.PCW_MIO_34_SLEW {slow} \
   CONFIG.PCW_MIO_35_DIRECTION {inout} \
   CONFIG.PCW_MIO_35_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_35_PULLUP {enabled} \
   CONFIG.PCW_MIO_35_SLEW {slow} \
   CONFIG.PCW_MIO_36_DIRECTION {in} \
   CONFIG.PCW_MIO_36_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_36_PULLUP {enabled} \
   CONFIG.PCW_MIO_36_SLEW {slow} \
   CONFIG.PCW_MIO_37_DIRECTION {inout} \
   CONFIG.PCW_MIO_37_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_37_PULLUP {enabled} \
   CONFIG.PCW_MIO_37_SLEW {slow} \
   CONFIG.PCW_MIO_38_DIRECTION {inout} \
   CONFIG.PCW_MIO_38_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_38_PULLUP {enabled} \
   CONFIG.PCW_MIO_38_SLEW {slow} \
   CONFIG.PCW_MIO_39_DIRECTION {inout} \
   CONFIG.PCW_MIO_39_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_39_PULLUP {enabled} \
   CONFIG.PCW_MIO_39_SLEW {slow} \
   CONFIG.PCW_MIO_3_DIRECTION {inout} \
   CONFIG.PCW_MIO_3_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_3_PULLUP {disabled} \
   CONFIG.PCW_MIO_3_SLEW {slow} \
   CONFIG.PCW_MIO_40_DIRECTION {inout} \
   CONFIG.PCW_MIO_40_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_40_PULLUP {disabled} \
   CONFIG.PCW_MIO_40_SLEW {slow} \
   CONFIG.PCW_MIO_41_DIRECTION {inout} \
   CONFIG.PCW_MIO_41_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_41_PULLUP {disabled} \
   CONFIG.PCW_MIO_41_SLEW {slow} \
   CONFIG.PCW_MIO_42_DIRECTION {inout} \
   CONFIG.PCW_MIO_42_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_42_PULLUP {disabled} \
   CONFIG.PCW_MIO_42_SLEW {slow} \
   CONFIG.PCW_MIO_43_DIRECTION {inout} \
   CONFIG.PCW_MIO_43_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_43_PULLUP {disabled} \
   CONFIG.PCW_MIO_43_SLEW {slow} \
   CONFIG.PCW_MIO_44_DIRECTION {inout} \
   CONFIG.PCW_MIO_44_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_44_PULLUP {disabled} \
   CONFIG.PCW_MIO_44_SLEW {slow} \
   CONFIG.PCW_MIO_45_DIRECTION {inout} \
   CONFIG.PCW_MIO_45_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_45_PULLUP {disabled} \
   CONFIG.PCW_MIO_45_SLEW {slow} \
   CONFIG.PCW_MIO_46_DIRECTION {inout} \
   CONFIG.PCW_MIO_46_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_46_PULLUP {enabled} \
   CONFIG.PCW_MIO_46_SLEW {slow} \
   CONFIG.PCW_MIO_47_DIRECTION {inout} \
   CONFIG.PCW_MIO_47_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_47_PULLUP {enabled} \
   CONFIG.PCW_MIO_47_SLEW {slow} \
   CONFIG.PCW_MIO_48_DIRECTION {inout} \
   CONFIG.PCW_MIO_48_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_48_PULLUP {enabled} \
   CONFIG.PCW_MIO_48_SLEW {slow} \
   CONFIG.PCW_MIO_49_DIRECTION {out} \
   CONFIG.PCW_MIO_49_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_49_PULLUP {enabled} \
   CONFIG.PCW_MIO_49_SLEW {slow} \
   CONFIG.PCW_MIO_4_DIRECTION {inout} \
   CONFIG.PCW_MIO_4_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_4_PULLUP {disabled} \
   CONFIG.PCW_MIO_4_SLEW {slow} \
   CONFIG.PCW_MIO_50_DIRECTION {inout} \
   CONFIG.PCW_MIO_50_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_50_PULLUP {enabled} \
   CONFIG.PCW_MIO_50_SLEW {slow} \
   CONFIG.PCW_MIO_51_DIRECTION {out} \
   CONFIG.PCW_MIO_51_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_51_PULLUP {enabled} \
   CONFIG.PCW_MIO_51_SLEW {slow} \
   CONFIG.PCW_MIO_52_DIRECTION {out} \
   CONFIG.PCW_MIO_52_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_52_PULLUP {enabled} \
   CONFIG.PCW_MIO_52_SLEW {slow} \
   CONFIG.PCW_MIO_53_DIRECTION {inout} \
   CONFIG.PCW_MIO_53_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_53_PULLUP {enabled} \
   CONFIG.PCW_MIO_53_SLEW {slow} \
   CONFIG.PCW_MIO_5_DIRECTION {inout} \
   CONFIG.PCW_MIO_5_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_5_PULLUP {disabled} \
   CONFIG.PCW_MIO_5_SLEW {slow} \
   CONFIG.PCW_MIO_6_DIRECTION {out} \
   CONFIG.PCW_MIO_6_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_6_PULLUP {disabled} \
   CONFIG.PCW_MIO_6_SLEW {slow} \
   CONFIG.PCW_MIO_7_DIRECTION {out} \
   CONFIG.PCW_MIO_7_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_7_PULLUP {disabled} \
   CONFIG.PCW_MIO_7_SLEW {slow} \
   CONFIG.PCW_MIO_8_DIRECTION {out} \
   CONFIG.PCW_MIO_8_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_8_PULLUP {disabled} \
   CONFIG.PCW_MIO_8_SLEW {slow} \
   CONFIG.PCW_MIO_9_DIRECTION {inout} \
   CONFIG.PCW_MIO_9_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_9_PULLUP {enabled} \
   CONFIG.PCW_MIO_9_SLEW {slow} \
   CONFIG.PCW_MIO_PRIMITIVE {54} \
   CONFIG.PCW_MIO_TREE_PERIPHERALS {GPIO#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#GPIO#GPIO#GPIO#I2C 0#I2C 0#I2C 1#I2C 1#UART 0#UART 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#Enet 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#SD 0#SD 0#SD 0#SD 0#SD 0#SD 0#GPIO#GPIO#GPIO#USB Reset#GPIO#ENET Reset#Enet 0#Enet 0} \
   CONFIG.PCW_MIO_TREE_SIGNALS {gpio[0]#qspi0_ss_b#qspi0_io[0]#qspi0_io[1]#qspi0_io[2]#qspi0_io[3]/HOLD_B#qspi0_sclk#gpio[7]#gpio[8]#gpio[9]#scl#sda#scl#sda#rx#tx#tx_clk#txd[0]#txd[1]#txd[2]#txd[3]#tx_ctl#rx_clk#rxd[0]#rxd[1]#rxd[2]#rxd[3]#rx_ctl#data[4]#dir#stp#nxt#data[0]#data[1]#data[2]#data[3]#clk#data[5]#data[6]#data[7]#clk#cmd#data[0]#data[1]#data[2]#data[3]#gpio[46]#gpio[47]#gpio[48]#reset#gpio[50]#reset#mdc#mdio} \
   CONFIG.PCW_M_AXI_GP0_ENABLE_STATIC_REMAP {0} \
   CONFIG.PCW_M_AXI_GP0_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP0_SUPPORT_NARROW_BURST {0} \
   CONFIG.PCW_M_AXI_GP0_THREAD_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP1_ENABLE_STATIC_REMAP {0} \
   CONFIG.PCW_M_AXI_GP1_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP1_SUPPORT_NARROW_BURST {0} \
   CONFIG.PCW_M_AXI_GP1_THREAD_ID_WIDTH {12} \
   CONFIG.PCW_NAND_CYCLES_T_AR {1} \
   CONFIG.PCW_NAND_CYCLES_T_CLR {1} \
   CONFIG.PCW_NAND_CYCLES_T_RC {2} \
   CONFIG.PCW_NAND_CYCLES_T_REA {1} \
   CONFIG.PCW_NAND_CYCLES_T_RR {1} \
   CONFIG.PCW_NAND_CYCLES_T_WC {2} \
   CONFIG.PCW_NAND_CYCLES_T_WP {1} \
   CONFIG.PCW_NAND_GRP_D8_ENABLE {0} \
   CONFIG.PCW_NAND_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_NOR_CS0_T_CEOE {1} \
   CONFIG.PCW_NOR_CS0_T_PC {1} \
   CONFIG.PCW_NOR_CS0_T_RC {2} \
   CONFIG.PCW_NOR_CS0_T_TR {1} \
   CONFIG.PCW_NOR_CS0_T_WC {2} \
   CONFIG.PCW_NOR_CS0_T_WP {1} \
   CONFIG.PCW_NOR_CS0_WE_TIME {0} \
   CONFIG.PCW_NOR_CS1_T_CEOE {1} \
   CONFIG.PCW_NOR_CS1_T_PC {1} \
   CONFIG.PCW_NOR_CS1_T_RC {2} \
   CONFIG.PCW_NOR_CS1_T_TR {1} \
   CONFIG.PCW_NOR_CS1_T_WC {2} \
   CONFIG.PCW_NOR_CS1_T_WP {1} \
   CONFIG.PCW_NOR_CS1_WE_TIME {0} \
   CONFIG.PCW_NOR_GRP_A25_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_CS0_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_CS1_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_CS0_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_CS1_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_INT_ENABLE {0} \
   CONFIG.PCW_NOR_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_NOR_SRAM_CS0_T_CEOE {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_PC {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_RC {2} \
   CONFIG.PCW_NOR_SRAM_CS0_T_TR {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_WC {2} \
   CONFIG.PCW_NOR_SRAM_CS0_T_WP {1} \
   CONFIG.PCW_NOR_SRAM_CS0_WE_TIME {0} \
   CONFIG.PCW_NOR_SRAM_CS1_T_CEOE {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_PC {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_RC {2} \
   CONFIG.PCW_NOR_SRAM_CS1_T_TR {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_WC {2} \
   CONFIG.PCW_NOR_SRAM_CS1_T_WP {1} \
   CONFIG.PCW_NOR_SRAM_CS1_WE_TIME {0} \
   CONFIG.PCW_OVERRIDE_BASIC_CLOCK {0} \
   CONFIG.PCW_P2F_CAN0_INTR {0} \
   CONFIG.PCW_P2F_CAN1_INTR {0} \
   CONFIG.PCW_P2F_CTI_INTR {0} \
   CONFIG.PCW_P2F_DMAC0_INTR {0} \
   CONFIG.PCW_P2F_DMAC1_INTR {0} \
   CONFIG.PCW_P2F_DMAC2_INTR {0} \
   CONFIG.PCW_P2F_DMAC3_INTR {0} \
   CONFIG.PCW_P2F_DMAC4_INTR {0} \
   CONFIG.PCW_P2F_DMAC5_INTR {0} \
   CONFIG.PCW_P2F_DMAC6_INTR {0} \
   CONFIG.PCW_P2F_DMAC7_INTR {0} \
   CONFIG.PCW_P2F_DMAC_ABORT_INTR {0} \
   CONFIG.PCW_P2F_ENET0_INTR {0} \
   CONFIG.PCW_P2F_ENET1_INTR {0} \
   CONFIG.PCW_P2F_GPIO_INTR {0} \
   CONFIG.PCW_P2F_I2C0_INTR {0} \
   CONFIG.PCW_P2F_I2C1_INTR {0} \
   CONFIG.PCW_P2F_QSPI_INTR {0} \
   CONFIG.PCW_P2F_SDIO0_INTR {0} \
   CONFIG.PCW_P2F_SDIO1_INTR {0} \
   CONFIG.PCW_P2F_SMC_INTR {0} \
   CONFIG.PCW_P2F_SPI0_INTR {0} \
   CONFIG.PCW_P2F_SPI1_INTR {0} \
   CONFIG.PCW_P2F_UART0_INTR {0} \
   CONFIG.PCW_P2F_UART1_INTR {0} \
   CONFIG.PCW_P2F_USB0_INTR {0} \
   CONFIG.PCW_P2F_USB1_INTR {0} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY0 {0.063} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY1 {0.062} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY2 {0.065} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY3 {0.083} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_0 {-0.007} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_1 {-0.010} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_2 {-0.006} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_3 {-0.048} \
   CONFIG.PCW_PACKAGE_NAME {clg484} \
   CONFIG.PCW_PCAP_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_PCAP_PERIPHERAL_DIVISOR0 {5} \
   CONFIG.PCW_PCAP_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_PERIPHERAL_BOARD_PRESET {part0} \
   CONFIG.PCW_PJTAG_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_PLL_BYPASSMODE_ENABLE {0} \
   CONFIG.PCW_PRESET_BANK0_VOLTAGE {LVCMOS 3.3V} \
   CONFIG.PCW_PRESET_BANK1_VOLTAGE {LVCMOS 1.8V} \
   CONFIG.PCW_PS7_SI_REV {PRODUCTION} \
   CONFIG.PCW_QSPI_GRP_FBCLK_ENABLE {0} \
   CONFIG.PCW_QSPI_GRP_IO1_ENABLE {0} \
   CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE {1} \
   CONFIG.PCW_QSPI_GRP_SINGLE_SS_IO {MIO 1 .. 6} \
   CONFIG.PCW_QSPI_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_QSPI_INTERNAL_HIGHADDRESS {0xFCFFFFFF} \
   CONFIG.PCW_QSPI_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_QSPI_PERIPHERAL_DIVISOR0 {5} \
   CONFIG.PCW_QSPI_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_QSPI_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_QSPI_QSPI_IO {MIO 1 .. 6} \
   CONFIG.PCW_SD0_GRP_CD_ENABLE {0} \
   CONFIG.PCW_SD0_GRP_POW_ENABLE {0} \
   CONFIG.PCW_SD0_GRP_WP_ENABLE {0} \
   CONFIG.PCW_SD0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_SD0_SD0_IO {MIO 40 .. 45} \
   CONFIG.PCW_SD1_GRP_CD_ENABLE {0} \
   CONFIG.PCW_SD1_GRP_POW_ENABLE {0} \
   CONFIG.PCW_SD1_GRP_WP_ENABLE {0} \
   CONFIG.PCW_SD1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SDIO0_BASEADDR {0xE0100000} \
   CONFIG.PCW_SDIO0_HIGHADDR {0xE0100FFF} \
   CONFIG.PCW_SDIO1_BASEADDR {0xE0101000} \
   CONFIG.PCW_SDIO1_HIGHADDR {0xE0101FFF} \
   CONFIG.PCW_SDIO_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SDIO_PERIPHERAL_DIVISOR0 {10} \
   CONFIG.PCW_SDIO_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_SDIO_PERIPHERAL_VALID {1} \
   CONFIG.PCW_SINGLE_QSPI_DATA_MODE {x4} \
   CONFIG.PCW_SMC_CYCLE_T0 {NA} \
   CONFIG.PCW_SMC_CYCLE_T1 {NA} \
   CONFIG.PCW_SMC_CYCLE_T2 {NA} \
   CONFIG.PCW_SMC_CYCLE_T3 {NA} \
   CONFIG.PCW_SMC_CYCLE_T4 {NA} \
   CONFIG.PCW_SMC_CYCLE_T5 {NA} \
   CONFIG.PCW_SMC_CYCLE_T6 {NA} \
   CONFIG.PCW_SMC_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SMC_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_SMC_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_SMC_PERIPHERAL_VALID {0} \
   CONFIG.PCW_SPI0_BASEADDR {0xE0006000} \
   CONFIG.PCW_SPI0_GRP_SS0_ENABLE {0} \
   CONFIG.PCW_SPI0_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_SPI0_GRP_SS2_ENABLE {0} \
   CONFIG.PCW_SPI0_HIGHADDR {0xE0006FFF} \
   CONFIG.PCW_SPI0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SPI1_BASEADDR {0xE0007000} \
   CONFIG.PCW_SPI1_GRP_SS0_ENABLE {0} \
   CONFIG.PCW_SPI1_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_SPI1_GRP_SS2_ENABLE {0} \
   CONFIG.PCW_SPI1_HIGHADDR {0xE0007FFF} \
   CONFIG.PCW_SPI1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SPI_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SPI_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_SPI_PERIPHERAL_FREQMHZ {166.666666} \
   CONFIG.PCW_SPI_PERIPHERAL_VALID {0} \
   CONFIG.PCW_S_AXI_ACP_ARUSER_VAL {31} \
   CONFIG.PCW_S_AXI_ACP_AWUSER_VAL {31} \
   CONFIG.PCW_S_AXI_ACP_ID_WIDTH {3} \
   CONFIG.PCW_S_AXI_GP0_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_GP1_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP0_DATA_WIDTH {32} \
   CONFIG.PCW_S_AXI_HP0_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP1_DATA_WIDTH {32} \
   CONFIG.PCW_S_AXI_HP1_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP2_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP2_ID_WIDTH {6} \
   CONFIG.PCW_S_AXI_HP3_DATA_WIDTH {64} \
   CONFIG.PCW_S_AXI_HP3_ID_WIDTH {6} \
   CONFIG.PCW_TPIU_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_TPIU_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TPIU_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_TRACE_BUFFER_CLOCK_DELAY {12} \
   CONFIG.PCW_TRACE_BUFFER_FIFO_SIZE {128} \
   CONFIG.PCW_TRACE_GRP_16BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_2BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_32BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_4BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_8BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_INTERNAL_WIDTH {2} \
   CONFIG.PCW_TRACE_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_TRACE_PIPELINE_WIDTH {8} \
   CONFIG.PCW_TTC0_BASEADDR {0xE0104000} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_HIGHADDR {0xE0104fff} \
   CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_TTC0_TTC0_IO {EMIO} \
   CONFIG.PCW_TTC1_BASEADDR {0xE0105000} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_HIGHADDR {0xE0105fff} \
   CONFIG.PCW_TTC1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_TTC_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_UART0_BASEADDR {0xE0000000} \
   CONFIG.PCW_UART0_BAUD_RATE {115200} \
   CONFIG.PCW_UART0_GRP_FULL_ENABLE {0} \
   CONFIG.PCW_UART0_HIGHADDR {0xE0000FFF} \
   CONFIG.PCW_UART0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_UART0_UART0_IO {MIO 14 .. 15} \
   CONFIG.PCW_UART1_BASEADDR {0xE0001000} \
   CONFIG.PCW_UART1_BAUD_RATE {115200} \
   CONFIG.PCW_UART1_GRP_FULL_ENABLE {0} \
   CONFIG.PCW_UART1_HIGHADDR {0xE0001FFF} \
   CONFIG.PCW_UART1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_UART_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_UART_PERIPHERAL_DIVISOR0 {10} \
   CONFIG.PCW_UART_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_UART_PERIPHERAL_VALID {1} \
   CONFIG.PCW_UIPARAM_ACT_DDR_FREQ_MHZ {533.333374} \
   CONFIG.PCW_UIPARAM_DDR_ADV_ENABLE {0} \
   CONFIG.PCW_UIPARAM_DDR_AL {0} \
   CONFIG.PCW_UIPARAM_DDR_BANK_ADDR_COUNT {3} \
   CONFIG.PCW_UIPARAM_DDR_BL {8} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY0 {0.25} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY1 {0.25} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY2 {0.25} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY3 {0.25} \
   CONFIG.PCW_UIPARAM_DDR_BUS_WIDTH {16 Bit} \
   CONFIG.PCW_UIPARAM_DDR_CL {7} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_PACKAGE_LENGTH {61.0905} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_PACKAGE_LENGTH {61.0905} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_PACKAGE_LENGTH {61.0905} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_PACKAGE_LENGTH {61.0905} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_STOP_EN {0} \
   CONFIG.PCW_UIPARAM_DDR_COL_ADDR_COUNT {10} \
   CONFIG.PCW_UIPARAM_DDR_CWL {5.000000} \
   CONFIG.PCW_UIPARAM_DDR_DEVICE_CAPACITY {4096 MBits} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_PACKAGE_LENGTH {68.4725} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_PACKAGE_LENGTH {71.086} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_PACKAGE_LENGTH {66.794} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_PACKAGE_LENGTH {108.7385} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_0 {0.0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_1 {0.0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_2 {0.0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_3 {0.0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_PACKAGE_LENGTH {64.1705} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_PACKAGE_LENGTH {63.686} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_PACKAGE_LENGTH {68.46} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_PACKAGE_LENGTH {105.4895} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DRAM_WIDTH {16 Bits} \
   CONFIG.PCW_UIPARAM_DDR_ECC {Disabled} \
   CONFIG.PCW_UIPARAM_DDR_ENABLE {1} \
   CONFIG.PCW_UIPARAM_DDR_FREQ_MHZ {533.333333} \
   CONFIG.PCW_UIPARAM_DDR_HIGH_TEMP {Normal (0-85)} \
   CONFIG.PCW_UIPARAM_DDR_MEMORY_TYPE {DDR 3} \
   CONFIG.PCW_UIPARAM_DDR_PARTNO {Custom} \
   CONFIG.PCW_UIPARAM_DDR_ROW_ADDR_COUNT {15} \
   CONFIG.PCW_UIPARAM_DDR_SPEED_BIN {DDR3_1066F} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_DATA_EYE {1} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_READ_GATE {1} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_WRITE_LEVEL {1} \
   CONFIG.PCW_UIPARAM_DDR_T_FAW {25} \
   CONFIG.PCW_UIPARAM_DDR_T_RAS_MIN {34.0} \
   CONFIG.PCW_UIPARAM_DDR_T_RC {48.91} \
   CONFIG.PCW_UIPARAM_DDR_T_RCD {7} \
   CONFIG.PCW_UIPARAM_DDR_T_RP {7} \
   CONFIG.PCW_UIPARAM_DDR_USE_INTERNAL_VREF {0} \
   CONFIG.PCW_UIPARAM_GENERATE_SUMMARY {NA} \
   CONFIG.PCW_USB0_BASEADDR {0xE0102000} \
   CONFIG.PCW_USB0_HIGHADDR {0xE0102fff} \
   CONFIG.PCW_USB0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_USB0_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_USB0_RESET_ENABLE {1} \
   CONFIG.PCW_USB0_RESET_IO {MIO 49} \
   CONFIG.PCW_USB0_USB0_IO {MIO 28 .. 39} \
   CONFIG.PCW_USB1_BASEADDR {0xE0103000} \
   CONFIG.PCW_USB1_HIGHADDR {0xE0103fff} \
   CONFIG.PCW_USB1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_USB1_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_USB1_RESET_ENABLE {0} \
   CONFIG.PCW_USB_RESET_ENABLE {1} \
   CONFIG.PCW_USB_RESET_POLARITY {Active Low} \
   CONFIG.PCW_USB_RESET_SELECT {Share reset pin} \
   CONFIG.PCW_USE_AXI_FABRIC_IDLE {0} \
   CONFIG.PCW_USE_AXI_NONSECURE {0} \
   CONFIG.PCW_USE_CORESIGHT {0} \
   CONFIG.PCW_USE_CROSS_TRIGGER {0} \
   CONFIG.PCW_USE_CR_FABRIC {1} \
   CONFIG.PCW_USE_DDR_BYPASS {0} \
   CONFIG.PCW_USE_DEBUG {0} \
   CONFIG.PCW_USE_DEFAULT_ACP_USER_VAL {0} \
   CONFIG.PCW_USE_DMA0 {0} \
   CONFIG.PCW_USE_DMA1 {0} \
   CONFIG.PCW_USE_DMA2 {0} \
   CONFIG.PCW_USE_DMA3 {0} \
   CONFIG.PCW_USE_EXPANDED_IOP {0} \
   CONFIG.PCW_USE_EXPANDED_PS_SLCR_REGISTERS {0} \
   CONFIG.PCW_USE_FABRIC_INTERRUPT {1} \
   CONFIG.PCW_USE_HIGH_OCM {0} \
   CONFIG.PCW_USE_M_AXI_GP0 {1} \
   CONFIG.PCW_USE_M_AXI_GP1 {1} \
   CONFIG.PCW_USE_PROC_EVENT_BUS {0} \
   CONFIG.PCW_USE_PS_SLCR_REGISTERS {0} \
   CONFIG.PCW_USE_S_AXI_ACP {0} \
   CONFIG.PCW_USE_S_AXI_GP0 {0} \
   CONFIG.PCW_USE_S_AXI_GP1 {0} \
   CONFIG.PCW_USE_S_AXI_HP0 {0} \
   CONFIG.PCW_USE_S_AXI_HP1 {1} \
   CONFIG.PCW_USE_S_AXI_HP2 {0} \
   CONFIG.PCW_USE_S_AXI_HP3 {0} \
   CONFIG.PCW_USE_TRACE {0} \
   CONFIG.PCW_USE_TRACE_DATA_EDGE_DETECTOR {0} \
   CONFIG.PCW_VALUE_SILVERSION {3} \
   CONFIG.PCW_WDT_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_WDT_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_WDT_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_WDT_PERIPHERAL_FREQMHZ {133.333333} \
 ] $processing_system7_0

  # Create instance: ps7_0_axi_periph, and set properties
  set ps7_0_axi_periph [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 ps7_0_axi_periph ]
  set_property -dict [ list \
   CONFIG.NUM_MI {45} \
 ] $ps7_0_axi_periph

  # Create instance: rst_processing_system7_0_50M, and set properties
  set rst_processing_system7_0_50M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_processing_system7_0_50M ]

  # Create instance: sample_clock_average_slow, and set properties
  set sample_clock_average_slow [ create_bd_cell -type ip -vlnv PEFFT:user:sample_clock_50duty:2.1 sample_clock_average_slow ]

  # Create instance: sample_clock_pulse_SlowADC, and set properties
  set sample_clock_pulse_SlowADC [ create_bd_cell -type ip -vlnv xilinx.com:user:sample_clock:2.0 sample_clock_pulse_SlowADC ]

  # Create instance: trip_enable_0, and set properties
  set trip_enable_0 [ create_bd_cell -type ip -vlnv PEFFT:user:trip_enable:2.0 trip_enable_0 ]

  # Create instance: vio_1, and set properties
  set vio_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:vio:3.0 vio_1 ]
  set_property -dict [ list \
   CONFIG.C_EN_PROBE_IN_ACTIVITY {0} \
   CONFIG.C_NUM_PROBE_IN {0} \
   CONFIG.C_NUM_PROBE_OUT {1} \
   CONFIG.C_PROBE_IN0_WIDTH {26} \
   CONFIG.C_PROBE_OUT0_WIDTH {26} \
 ] $vio_1

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {3} \
 ] $xlconcat_0

  # Create instance: xlconcat_1, and set properties
  set xlconcat_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_1 ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {26} \
 ] $xlconcat_1

  # Create instance: xlconcat_4, and set properties
  set xlconcat_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_4 ]

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_0

  # Create instance: xlconstant_1, and set properties
  set xlconstant_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {25} \
 ] $xlconstant_1

  # Create instance: xlconstant_2, and set properties
  set xlconstant_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_2 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $xlconstant_2

  # Create instance: xlconstant_3, and set properties
  set xlconstant_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_3 ]
  set_property -dict [ list \
   CONFIG.CONST_WIDTH {12} \
 ] $xlconstant_3

  # Create instance: xlslice_0, and set properties
  set xlslice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {0} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {3} \
 ] $xlslice_0

  # Create instance: xlslice_1, and set properties
  set xlslice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_1 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {1} \
   CONFIG.DIN_TO {1} \
   CONFIG.DIN_WIDTH {3} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_1

  # Create instance: xlslice_2, and set properties
  set xlslice_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_2 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {2} \
   CONFIG.DIN_TO {2} \
   CONFIG.DIN_WIDTH {3} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_2

  # Create interface connections
  connect_bd_intf_net -intf_net FPGA_to_DDRRAM_0_M_AXI_Full [get_bd_intf_pins FPGA_to_DDRRAM_0/M_AXI_Full] [get_bd_intf_pins axi_smc1/S00_AXI]
  connect_bd_intf_net -intf_net axi_smc1_M00_AXI [get_bd_intf_pins axi_smc1/M00_AXI] [get_bd_intf_pins processing_system7_0/S_AXI_HP1]
  connect_bd_intf_net -intf_net processing_system7_0_DDR [get_bd_intf_ports DDR] [get_bd_intf_pins processing_system7_0/DDR]
  connect_bd_intf_net -intf_net processing_system7_0_M_AXI_GP0 [get_bd_intf_pins processing_system7_0/M_AXI_GP0] [get_bd_intf_pins ps7_0_axi_periph/S00_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M00_AXI [get_bd_intf_pins axi_timer_interrupt_to_PS/S_AXI] [get_bd_intf_pins ps7_0_axi_periph/M00_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M01_AXI [get_bd_intf_pins Slow_ADC/S0_AXI_Lite] [get_bd_intf_pins ps7_0_axi_periph/M01_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M02_AXI [get_bd_intf_pins Conv_ADC_slow_Full_0/AXI4_Lite] [get_bd_intf_pins ps7_0_axi_periph/M02_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M03_AXI [get_bd_intf_pins PWM_28_HB_0/AXI4_Lite] [get_bd_intf_pins ps7_0_axi_periph/M03_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M11_AXI [get_bd_intf_pins axi_gpio_0/S_AXI] [get_bd_intf_pins ps7_0_axi_periph/M11_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M12_AXI [get_bd_intf_pins axi_gpio_input/S_AXI] [get_bd_intf_pins ps7_0_axi_periph/M12_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M13_AXI [get_bd_intf_pins axi_gpio_reset/S_AXI] [get_bd_intf_pins ps7_0_axi_periph/M13_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M21_AXI [get_bd_intf_pins ps7_0_axi_periph/M21_AXI] [get_bd_intf_pins sample_clock_average_slow/S_AXI_Lite]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M23_AXI [get_bd_intf_pins FO_precarga/S_AXI] [get_bd_intf_pins ps7_0_axi_periph/M23_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M28_AXI [get_bd_intf_pins ps7_0_axi_periph/M28_AXI] [get_bd_intf_pins sample_clock_pulse_SlowADC/S0_AXI_Lite]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M31_AXI [get_bd_intf_pins FPGA_to_DDRRAM_0/S_AXI_Lite] [get_bd_intf_pins ps7_0_axi_periph/M31_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M34_AXI [get_bd_intf_pins axi_gpio_band/S_AXI] [get_bd_intf_pins ps7_0_axi_periph/M34_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M40_AXI [get_bd_intf_pins debug_8_channel_1/S_AXI_Lite] [get_bd_intf_pins ps7_0_axi_periph/M40_AXI]
  connect_bd_intf_net -intf_net ps7_0_axi_periph_M42_AXI [get_bd_intf_pins axi_gpio_interrupt_DDRRAM/S_AXI] [get_bd_intf_pins ps7_0_axi_periph/M42_AXI]

  # Create port connections
  connect_bd_net -net ADC1_N_1 [get_bd_ports ADC1_N] [get_bd_pins ADC_ads5272_1/adc_N]
  connect_bd_net -net ADC1_P_1 [get_bd_ports ADC1_P] [get_bd_pins ADC_ads5272_1/adc_P]
  connect_bd_net -net ADC2_N_1 [get_bd_ports ADC2_N] [get_bd_pins ADC_ads5272_2/adc_N]
  connect_bd_net -net ADC2_P_1 [get_bd_ports ADC2_P] [get_bd_pins ADC_ads5272_2/adc_P]
  connect_bd_net -net ADCLK1_N_1 [get_bd_ports ADCLK1_N] [get_bd_pins ADC_ads5272_1/adclk_N]
  connect_bd_net -net ADCLK1_P_1 [get_bd_ports ADCLK1_P] [get_bd_pins ADC_ads5272_1/adclk_P]
  connect_bd_net -net ADCLK2_N_1 [get_bd_ports ADCLK2_N] [get_bd_pins ADC_ads5272_2/adclk_N]
  connect_bd_net -net ADCLK2_P_1 [get_bd_ports ADCLK2_P] [get_bd_pins ADC_ads5272_2/adclk_P]
  connect_bd_net -net ADC_max11331_0_mosi [get_bd_ports SPI_mosi] [get_bd_pins Slow_ADC/SPI_mosi]
  connect_bd_net -net ADC_max11331_0_sclk [get_bd_ports SPI_sck] [get_bd_pins Slow_ADC/SPI_sck]
  connect_bd_net -net ADC_max11331_0_ss_n [get_bd_ports SPI_ss_n] [get_bd_pins Slow_ADC/SPI_ss_n]
  connect_bd_net -net CPLD_ready_1 [get_bd_ports CPLD_ready] [get_bd_pins xlconcat_4/In1]
  connect_bd_net -net FO_precarga_gpio_io_o [get_bd_pins FO_precarga/gpio_io_o] [get_bd_pins xlslice_0/Din] [get_bd_pins xlslice_1/Din] [get_bd_pins xlslice_2/Din]
  connect_bd_net -net FPGA_to_DDRRAM_0_done [get_bd_pins FPGA_to_DDRRAM_0/done] [get_bd_pins axi_gpio_interrupt_DDRRAM/gpio_io_i]
  connect_bd_net -net LCLK1_N_1 [get_bd_ports LCLK1_N] [get_bd_pins ADC_ads5272_1/lclk_N]
  connect_bd_net -net LCLK1_P_1 [get_bd_ports LCLK1_P] [get_bd_pins ADC_ads5272_1/lclk_P]
  connect_bd_net -net LCLK2_N_1 [get_bd_ports LCLK2_N] [get_bd_pins ADC_ads5272_2/lclk_N]
  connect_bd_net -net LCLK2_P_1 [get_bd_ports LCLK2_P] [get_bd_pins ADC_ads5272_2/lclk_P]
  connect_bd_net -net PWM_28_HB_0_PWM1 [get_bd_pins PWM_28_HB_0/PWM1] [get_bd_pins xlconcat_1/In0]
  connect_bd_net -net PWM_28_HB_0_PWM2 [get_bd_pins PWM_28_HB_0/PWM2] [get_bd_pins xlconcat_1/In1]
  connect_bd_net -net PWM_28_HB_0_PWM3 [get_bd_pins PWM_28_HB_0/PWM3] [get_bd_pins xlconcat_1/In2]
  connect_bd_net -net PWM_28_HB_0_PWM4 [get_bd_pins PWM_28_HB_0/PWM4] [get_bd_pins xlconcat_1/In3]
  connect_bd_net -net PWM_28_HB_0_PWM5 [get_bd_pins PWM_28_HB_0/PWM5] [get_bd_pins xlconcat_1/In4]
  connect_bd_net -net PWM_28_HB_0_PWM6 [get_bd_pins PWM_28_HB_0/PWM6] [get_bd_pins xlconcat_1/In5]
  connect_bd_net -net PWM_28_HB_0_PWM7 [get_bd_pins PWM_28_HB_0/PWM7] [get_bd_pins xlconcat_1/In6]
  connect_bd_net -net PWM_28_HB_0_PWM8 [get_bd_pins PWM_28_HB_0/PWM8] [get_bd_pins xlconcat_1/In7]
  connect_bd_net -net PWM_28_HB_0_PWM9 [get_bd_pins PWM_28_HB_0/PWM9] [get_bd_pins xlconcat_1/In8]
  connect_bd_net -net PWM_28_HB_0_PWM10 [get_bd_pins PWM_28_HB_0/PWM10] [get_bd_pins xlconcat_1/In9]
  connect_bd_net -net PWM_28_HB_0_PWM11 [get_bd_pins PWM_28_HB_0/PWM11] [get_bd_pins xlconcat_1/In10]
  connect_bd_net -net PWM_28_HB_0_PWM12 [get_bd_pins PWM_28_HB_0/PWM12] [get_bd_pins xlconcat_1/In11]
  connect_bd_net -net PWM_28_HB_0_PWM13 [get_bd_pins PWM_28_HB_0/PWM13] [get_bd_pins xlconcat_1/In12]
  connect_bd_net -net PWM_28_HB_0_PWM14 [get_bd_pins PWM_28_HB_0/PWM14] [get_bd_pins xlconcat_1/In13]
  connect_bd_net -net PWM_28_HB_0_PWM15 [get_bd_pins PWM_28_HB_0/PWM15] [get_bd_pins xlconcat_1/In14]
  connect_bd_net -net PWM_28_HB_0_PWM16 [get_bd_pins PWM_28_HB_0/PWM16] [get_bd_pins xlconcat_1/In15]
  connect_bd_net -net PWM_28_HB_0_PWM17 [get_bd_pins PWM_28_HB_0/PWM17] [get_bd_pins xlconcat_1/In16]
  connect_bd_net -net PWM_28_HB_0_PWM18 [get_bd_pins PWM_28_HB_0/PWM18] [get_bd_pins xlconcat_1/In17]
  connect_bd_net -net PWM_28_HB_0_PWM19 [get_bd_pins PWM_28_HB_0/PWM19] [get_bd_pins xlconcat_1/In18]
  connect_bd_net -net PWM_28_HB_0_PWM20 [get_bd_pins PWM_28_HB_0/PWM20] [get_bd_pins xlconcat_1/In19]
  connect_bd_net -net PWM_28_HB_0_PWM21 [get_bd_pins PWM_28_HB_0/PWM21] [get_bd_pins xlconcat_1/In20]
  connect_bd_net -net PWM_28_HB_0_PWM22 [get_bd_pins PWM_28_HB_0/PWM22] [get_bd_pins xlconcat_1/In21]
  connect_bd_net -net PWM_28_HB_0_PWM23 [get_bd_pins PWM_28_HB_0/PWM23] [get_bd_pins xlconcat_1/In22]
  connect_bd_net -net PWM_28_HB_0_PWM24 [get_bd_pins PWM_28_HB_0/PWM24] [get_bd_pins xlconcat_1/In23]
  connect_bd_net -net PWM_28_HB_0_PWM25 [get_bd_pins PWM_28_HB_0/PWM25] [get_bd_pins xlconcat_1/In24]
  connect_bd_net -net PWM_28_HB_0_PWM26 [get_bd_pins PWM_28_HB_0/PWM26] [get_bd_pins xlconcat_1/In25]
  connect_bd_net -net Resets_Dout11 [get_bd_pins Resets/Dout11] [get_bd_pins debug_8_channel_1/reset_n]
  connect_bd_net -net SPI_miso_1 [get_bd_ports SPI_miso] [get_bd_pins Slow_ADC/SPI_miso]
  connect_bd_net -net Slow_ADC_data_board_2_input_1_ch2 [get_bd_pins Slow_ADC/data_board_2_input_1_ch1] [get_bd_pins average_8_samples_slow_adc_0/data_in_1] [get_bd_pins ila_Slow_ADC/probe0]
  connect_bd_net -net Slow_ADC_data_board_2_input_1_ch3 [get_bd_pins Slow_ADC/data_board_2_input_1_ch2] [get_bd_pins average_8_samples_slow_adc_0/data_in_2] [get_bd_pins ila_Slow_ADC/probe1]
  connect_bd_net -net Slow_ADC_data_board_2_input_1_ch4 [get_bd_pins Slow_ADC/data_board_2_input_1_ch3] [get_bd_pins average_8_samples_slow_adc_0/data_in_3] [get_bd_pins ila_Slow_ADC/probe2]
  connect_bd_net -net Slow_ADC_data_board_2_input_1_ch5 [get_bd_pins Slow_ADC/data_board_2_input_1_ch4] [get_bd_pins average_8_samples_slow_adc_0/data_in_4] [get_bd_pins ila_Slow_ADC/probe3]
  connect_bd_net -net Slow_ADC_data_board_2_input_2_ch1 [get_bd_pins Slow_ADC/data_board_2_input_2_ch1] [get_bd_pins ila_Slow_ADC/probe4]
  connect_bd_net -net Slow_ADC_data_board_2_input_2_ch2 [get_bd_pins Slow_ADC/data_board_2_input_2_ch2] [get_bd_pins ila_Slow_ADC/probe5]
  connect_bd_net -net Slow_ADC_data_board_2_input_2_ch3 [get_bd_pins Slow_ADC/data_board_2_input_2_ch3] [get_bd_pins ila_Slow_ADC/probe6]
  connect_bd_net -net Slow_ADC_data_board_2_input_2_ch4 [get_bd_pins Slow_ADC/data_board_2_input_2_ch4] [get_bd_pins ila_Slow_ADC/probe7]
  connect_bd_net -net Slow_ADC_data_board_2_input_3_ch1 [get_bd_pins Slow_ADC/data_board_2_input_3_ch1] [get_bd_pins ila_Slow_ADC/probe8]
  connect_bd_net -net Slow_ADC_data_board_2_input_3_ch2 [get_bd_pins Slow_ADC/data_board_2_input_3_ch2] [get_bd_pins ila_Slow_ADC/probe9]
  connect_bd_net -net Slow_ADC_data_board_2_input_3_ch3 [get_bd_pins Slow_ADC/data_board_2_input_3_ch3] [get_bd_pins ila_Slow_ADC/probe10]
  connect_bd_net -net Slow_ADC_data_board_2_input_3_ch4 [get_bd_pins Slow_ADC/data_board_2_input_3_ch4] [get_bd_pins ila_Slow_ADC/probe11]
  connect_bd_net -net Slow_ADC_data_board_2_input_4_ch1 [get_bd_pins Slow_ADC/data_board_2_input_4_ch1] [get_bd_pins ila_Slow_ADC/probe12]
  connect_bd_net -net Slow_ADC_data_board_2_input_4_ch2 [get_bd_pins Slow_ADC/data_board_2_input_4_ch2] [get_bd_pins ila_Slow_ADC/probe13]
  connect_bd_net -net Slow_ADC_data_board_2_input_4_ch3 [get_bd_pins Slow_ADC/data_board_2_input_4_ch3] [get_bd_pins ila_Slow_ADC/probe14]
  connect_bd_net -net Slow_ADC_data_board_2_input_4_ch4 [get_bd_pins Slow_ADC/data_board_2_input_4_ch4] [get_bd_pins ila_Slow_ADC/probe15]
  connect_bd_net -net Slow_ADC_data_board_2_input_5_ch1 [get_bd_pins Slow_ADC/data_board_2_input_5_ch1] [get_bd_pins ila_Slow_ADC/probe16]
  connect_bd_net -net Slow_ADC_data_board_2_input_5_ch2 [get_bd_pins Slow_ADC/data_board_2_input_5_ch2] [get_bd_pins ila_Slow_ADC/probe17]
  connect_bd_net -net Slow_ADC_data_board_2_input_5_ch3 [get_bd_pins Slow_ADC/data_board_2_input_5_ch3] [get_bd_pins ila_Slow_ADC/probe18]
  connect_bd_net -net Slow_ADC_data_board_2_input_5_ch4 [get_bd_pins Slow_ADC/data_board_2_input_5_ch4] [get_bd_pins ila_Slow_ADC/probe19]
  connect_bd_net -net Slow_ADC_data_board_2_input_6_ch1 [get_bd_pins Slow_ADC/data_board_2_input_6_ch1] [get_bd_pins ila_Slow_ADC/probe20]
  connect_bd_net -net Slow_ADC_data_board_2_input_6_ch2 [get_bd_pins Slow_ADC/data_board_2_input_6_ch2] [get_bd_pins ila_Slow_ADC/probe21]
  connect_bd_net -net Slow_ADC_data_board_2_input_6_ch3 [get_bd_pins Slow_ADC/data_board_2_input_6_ch3] [get_bd_pins ila_Slow_ADC/probe22]
  connect_bd_net -net Slow_ADC_data_board_2_input_6_ch4 [get_bd_pins Slow_ADC/data_board_2_input_6_ch4] [get_bd_pins ila_Slow_ADC/probe23]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_1 [get_bd_pins Conv_ADC_slow_Full_0/ADC1] [get_bd_pins average_8_samples_slow_adc_0/data_out_1] [get_bd_pins ila_Average/probe0]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_2 [get_bd_pins Conv_ADC_slow_Full_0/ADC2] [get_bd_pins average_8_samples_slow_adc_0/data_out_2] [get_bd_pins ila_Average/probe1]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_3 [get_bd_pins Conv_ADC_slow_Full_0/ADC3] [get_bd_pins average_8_samples_slow_adc_0/data_out_3] [get_bd_pins ila_Average/probe2]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_4 [get_bd_pins Conv_ADC_slow_Full_0/ADC4] [get_bd_pins average_8_samples_slow_adc_0/data_out_4] [get_bd_pins ila_Average/probe3]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_5 [get_bd_pins Conv_ADC_slow_Full_0/ADC5] [get_bd_pins average_8_samples_slow_adc_0/data_out_5] [get_bd_pins ila_Average/probe4]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_6 [get_bd_pins Conv_ADC_slow_Full_0/ADC6] [get_bd_pins average_8_samples_slow_adc_0/data_out_6] [get_bd_pins ila_Average/probe5]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_7 [get_bd_pins Conv_ADC_slow_Full_0/ADC7] [get_bd_pins average_8_samples_slow_adc_0/data_out_7] [get_bd_pins ila_Average/probe6]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_8 [get_bd_pins Conv_ADC_slow_Full_0/ADC8] [get_bd_pins average_8_samples_slow_adc_0/data_out_8] [get_bd_pins ila_Average/probe7]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_9 [get_bd_pins Conv_ADC_slow_Full_0/ADC9] [get_bd_pins average_8_samples_slow_adc_0/data_out_9] [get_bd_pins ila_Average/probe8]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_10 [get_bd_pins Conv_ADC_slow_Full_0/ADC10] [get_bd_pins average_8_samples_slow_adc_0/data_out_10] [get_bd_pins ila_Average/probe9]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_11 [get_bd_pins Conv_ADC_slow_Full_0/ADC11] [get_bd_pins average_8_samples_slow_adc_0/data_out_11] [get_bd_pins ila_Average/probe10]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_12 [get_bd_pins Conv_ADC_slow_Full_0/ADC12] [get_bd_pins average_8_samples_slow_adc_0/data_out_12] [get_bd_pins ila_Average/probe11]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_13 [get_bd_pins Conv_ADC_slow_Full_0/ADC13] [get_bd_pins average_8_samples_slow_adc_0/data_out_13]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_14 [get_bd_pins Conv_ADC_slow_Full_0/ADC14] [get_bd_pins average_8_samples_slow_adc_0/data_out_14]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_15 [get_bd_pins Conv_ADC_slow_Full_0/ADC15] [get_bd_pins average_8_samples_slow_adc_0/data_out_15]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_16 [get_bd_pins Conv_ADC_slow_Full_0/ADC16] [get_bd_pins average_8_samples_slow_adc_0/data_out_16]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_17 [get_bd_pins Conv_ADC_slow_Full_0/ADC17] [get_bd_pins average_8_samples_slow_adc_0/data_out_17]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_18 [get_bd_pins Conv_ADC_slow_Full_0/ADC18] [get_bd_pins average_8_samples_slow_adc_0/data_out_18]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_19 [get_bd_pins Conv_ADC_slow_Full_0/ADC19] [get_bd_pins average_8_samples_slow_adc_0/data_out_19]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_20 [get_bd_pins Conv_ADC_slow_Full_0/ADC20] [get_bd_pins average_8_samples_slow_adc_0/data_out_20]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_21 [get_bd_pins Conv_ADC_slow_Full_0/ADC21] [get_bd_pins average_8_samples_slow_adc_0/data_out_21]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_22 [get_bd_pins Conv_ADC_slow_Full_0/ADC22] [get_bd_pins average_8_samples_slow_adc_0/data_out_22]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_23 [get_bd_pins Conv_ADC_slow_Full_0/ADC23] [get_bd_pins average_8_samples_slow_adc_0/data_out_23]
  connect_bd_net -net average_8_samples_slow_adc_0_data_out_24 [get_bd_pins Conv_ADC_slow_Full_0/ADC24] [get_bd_pins average_8_samples_slow_adc_0/data_out_24]
  connect_bd_net -net axi_gpio_0_gpio_io_o1 [get_bd_pins axi_gpio_0/gpio_io_o] [get_bd_pins trip_enable_0/enable_in]
  connect_bd_net -net axi_gpio_2_gpio_io_o [get_bd_pins average_8_samples_slow_adc_0/band] [get_bd_pins axi_gpio_band/gpio_io_o]
  connect_bd_net -net axi_gpio_interrupt_DDRRAM_ip2intc_irpt [get_bd_pins axi_gpio_interrupt_DDRRAM/ip2intc_irpt] [get_bd_pins xlconcat_0/In2]
  connect_bd_net -net axi_gpio_reset_gpio_io_o1 [get_bd_pins Resets/Din] [get_bd_pins axi_gpio_reset/gpio_io_o]
  connect_bd_net -net axi_gpio_trip_ip2intc_irpt [get_bd_pins axi_gpio_input/ip2intc_irpt] [get_bd_pins xlconcat_0/In1]
  connect_bd_net -net axi_timer_interrupt_to_PS_interrupt [get_bd_pins axi_timer_interrupt_to_PS/interrupt] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net clk_wiz_adclk_adclk [get_bd_ports ADCLK] [get_bd_pins clk_wiz_adclk/adclk]
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins Conv_ADC_slow_Full_0/AXI4_Lite_ACLK] [get_bd_pins Conv_ADC_slow_Full_0/IPCORE_CLK] [get_bd_pins FO_precarga/s_axi_aclk] [get_bd_pins FPGA_to_DDRRAM_0/m_axi_full_aclk] [get_bd_pins FPGA_to_DDRRAM_0/s_axi_lite_aclk] [get_bd_pins PWM_28_HB_0/AXI4_Lite_ACLK] [get_bd_pins PWM_28_HB_0/IPCORE_CLK] [get_bd_pins Slow_ADC/clk] [get_bd_pins axi_gpio_0/s_axi_aclk] [get_bd_pins axi_gpio_band/s_axi_aclk] [get_bd_pins axi_gpio_input/s_axi_aclk] [get_bd_pins axi_gpio_interrupt_DDRRAM/s_axi_aclk] [get_bd_pins axi_gpio_reset/s_axi_aclk] [get_bd_pins axi_smc1/aclk] [get_bd_pins axi_timer_interrupt_to_PS/s_axi_aclk] [get_bd_pins clk_wiz_adclk/clk_in1] [get_bd_pins debug_8_channel_1/s_axi_lite_aclk] [get_bd_pins ila_Average/clk] [get_bd_pins ila_Slow_ADC/clk] [get_bd_pins processing_system7_0/FCLK_CLK0] [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK] [get_bd_pins processing_system7_0/M_AXI_GP1_ACLK] [get_bd_pins processing_system7_0/S_AXI_HP1_ACLK] [get_bd_pins ps7_0_axi_periph/ACLK] [get_bd_pins ps7_0_axi_periph/M00_ACLK] [get_bd_pins ps7_0_axi_periph/M01_ACLK] [get_bd_pins ps7_0_axi_periph/M02_ACLK] [get_bd_pins ps7_0_axi_periph/M03_ACLK] [get_bd_pins ps7_0_axi_periph/M04_ACLK] [get_bd_pins ps7_0_axi_periph/M05_ACLK] [get_bd_pins ps7_0_axi_periph/M06_ACLK] [get_bd_pins ps7_0_axi_periph/M07_ACLK] [get_bd_pins ps7_0_axi_periph/M08_ACLK] [get_bd_pins ps7_0_axi_periph/M09_ACLK] [get_bd_pins ps7_0_axi_periph/M10_ACLK] [get_bd_pins ps7_0_axi_periph/M11_ACLK] [get_bd_pins ps7_0_axi_periph/M12_ACLK] [get_bd_pins ps7_0_axi_periph/M13_ACLK] [get_bd_pins ps7_0_axi_periph/M14_ACLK] [get_bd_pins ps7_0_axi_periph/M15_ACLK] [get_bd_pins ps7_0_axi_periph/M16_ACLK] [get_bd_pins ps7_0_axi_periph/M17_ACLK] [get_bd_pins ps7_0_axi_periph/M18_ACLK] [get_bd_pins ps7_0_axi_periph/M19_ACLK] [get_bd_pins ps7_0_axi_periph/M20_ACLK] [get_bd_pins ps7_0_axi_periph/M21_ACLK] [get_bd_pins ps7_0_axi_periph/M22_ACLK] [get_bd_pins ps7_0_axi_periph/M23_ACLK] [get_bd_pins ps7_0_axi_periph/M24_ACLK] [get_bd_pins ps7_0_axi_periph/M25_ACLK] [get_bd_pins ps7_0_axi_periph/M26_ACLK] [get_bd_pins ps7_0_axi_periph/M27_ACLK] [get_bd_pins ps7_0_axi_periph/M28_ACLK] [get_bd_pins ps7_0_axi_periph/M29_ACLK] [get_bd_pins ps7_0_axi_periph/M30_ACLK] [get_bd_pins ps7_0_axi_periph/M31_ACLK] [get_bd_pins ps7_0_axi_periph/M32_ACLK] [get_bd_pins ps7_0_axi_periph/M33_ACLK] [get_bd_pins ps7_0_axi_periph/M34_ACLK] [get_bd_pins ps7_0_axi_periph/M35_ACLK] [get_bd_pins ps7_0_axi_periph/M36_ACLK] [get_bd_pins ps7_0_axi_periph/M37_ACLK] [get_bd_pins ps7_0_axi_periph/M38_ACLK] [get_bd_pins ps7_0_axi_periph/M39_ACLK] [get_bd_pins ps7_0_axi_periph/M40_ACLK] [get_bd_pins ps7_0_axi_periph/M41_ACLK] [get_bd_pins ps7_0_axi_periph/M42_ACLK] [get_bd_pins ps7_0_axi_periph/M43_ACLK] [get_bd_pins ps7_0_axi_periph/M44_ACLK] [get_bd_pins ps7_0_axi_periph/S00_ACLK] [get_bd_pins rst_processing_system7_0_50M/slowest_sync_clk] [get_bd_pins sample_clock_average_slow/clk] [get_bd_pins sample_clock_average_slow/s_axi_lite_aclk] [get_bd_pins sample_clock_pulse_SlowADC/clk] [get_bd_pins sample_clock_pulse_SlowADC/s0_axi_lite_aclk] [get_bd_pins vio_1/clk]
  connect_bd_net -net processing_system7_0_FCLK_RESET0_N [get_bd_pins processing_system7_0/FCLK_RESET0_N] [get_bd_pins rst_processing_system7_0_50M/ext_reset_in]
  connect_bd_net -net rst_processing_system7_0_50M_interconnect_aresetn [get_bd_pins ps7_0_axi_periph/ARESETN] [get_bd_pins rst_processing_system7_0_50M/interconnect_aresetn]
  connect_bd_net -net rst_processing_system7_0_50M_peripheral_aresetn [get_bd_pins Conv_ADC_slow_Full_0/AXI4_Lite_ARESETN] [get_bd_pins Conv_ADC_slow_Full_0/IPCORE_RESETN] [get_bd_pins FO_precarga/s_axi_aresetn] [get_bd_pins FPGA_to_DDRRAM_0/m_axi_full_aresetn] [get_bd_pins FPGA_to_DDRRAM_0/s_axi_lite_aresetn] [get_bd_pins PWM_28_HB_0/AXI4_Lite_ARESETN] [get_bd_pins PWM_28_HB_0/IPCORE_RESETN] [get_bd_pins Slow_ADC/s0_axi_lite_aresetn] [get_bd_pins axi_gpio_0/s_axi_aresetn] [get_bd_pins axi_gpio_band/s_axi_aresetn] [get_bd_pins axi_gpio_input/s_axi_aresetn] [get_bd_pins axi_gpio_interrupt_DDRRAM/s_axi_aresetn] [get_bd_pins axi_gpio_reset/s_axi_aresetn] [get_bd_pins axi_timer_interrupt_to_PS/s_axi_aresetn] [get_bd_pins debug_8_channel_1/s_axi_lite_aresetn] [get_bd_pins ps7_0_axi_periph/M00_ARESETN] [get_bd_pins ps7_0_axi_periph/M01_ARESETN] [get_bd_pins ps7_0_axi_periph/M02_ARESETN] [get_bd_pins ps7_0_axi_periph/M03_ARESETN] [get_bd_pins ps7_0_axi_periph/M04_ARESETN] [get_bd_pins ps7_0_axi_periph/M05_ARESETN] [get_bd_pins ps7_0_axi_periph/M06_ARESETN] [get_bd_pins ps7_0_axi_periph/M07_ARESETN] [get_bd_pins ps7_0_axi_periph/M08_ARESETN] [get_bd_pins ps7_0_axi_periph/M09_ARESETN] [get_bd_pins ps7_0_axi_periph/M10_ARESETN] [get_bd_pins ps7_0_axi_periph/M11_ARESETN] [get_bd_pins ps7_0_axi_periph/M12_ARESETN] [get_bd_pins ps7_0_axi_periph/M13_ARESETN] [get_bd_pins ps7_0_axi_periph/M14_ARESETN] [get_bd_pins ps7_0_axi_periph/M15_ARESETN] [get_bd_pins ps7_0_axi_periph/M16_ARESETN] [get_bd_pins ps7_0_axi_periph/M17_ARESETN] [get_bd_pins ps7_0_axi_periph/M18_ARESETN] [get_bd_pins ps7_0_axi_periph/M19_ARESETN] [get_bd_pins ps7_0_axi_periph/M20_ARESETN] [get_bd_pins ps7_0_axi_periph/M21_ARESETN] [get_bd_pins ps7_0_axi_periph/M22_ARESETN] [get_bd_pins ps7_0_axi_periph/M23_ARESETN] [get_bd_pins ps7_0_axi_periph/M24_ARESETN] [get_bd_pins ps7_0_axi_periph/M25_ARESETN] [get_bd_pins ps7_0_axi_periph/M26_ARESETN] [get_bd_pins ps7_0_axi_periph/M27_ARESETN] [get_bd_pins ps7_0_axi_periph/M28_ARESETN] [get_bd_pins ps7_0_axi_periph/M29_ARESETN] [get_bd_pins ps7_0_axi_periph/M30_ARESETN] [get_bd_pins ps7_0_axi_periph/M31_ARESETN] [get_bd_pins ps7_0_axi_periph/M32_ARESETN] [get_bd_pins ps7_0_axi_periph/M33_ARESETN] [get_bd_pins ps7_0_axi_periph/M34_ARESETN] [get_bd_pins ps7_0_axi_periph/M35_ARESETN] [get_bd_pins ps7_0_axi_periph/M36_ARESETN] [get_bd_pins ps7_0_axi_periph/M37_ARESETN] [get_bd_pins ps7_0_axi_periph/M38_ARESETN] [get_bd_pins ps7_0_axi_periph/M39_ARESETN] [get_bd_pins ps7_0_axi_periph/M40_ARESETN] [get_bd_pins ps7_0_axi_periph/M41_ARESETN] [get_bd_pins ps7_0_axi_periph/M42_ARESETN] [get_bd_pins ps7_0_axi_periph/M43_ARESETN] [get_bd_pins ps7_0_axi_periph/M44_ARESETN] [get_bd_pins ps7_0_axi_periph/S00_ARESETN] [get_bd_pins rst_processing_system7_0_50M/peripheral_aresetn] [get_bd_pins sample_clock_average_slow/s_axi_lite_aresetn] [get_bd_pins sample_clock_pulse_SlowADC/s0_axi_lite_aresetn]
  connect_bd_net -net sample_clock_0_sample [get_bd_pins Slow_ADC/enable_measure] [get_bd_pins sample_clock_pulse_SlowADC/sample]
  connect_bd_net -net sample_clock_SlowADC1_sample [get_bd_pins average_8_samples_slow_adc_0/clock] [get_bd_pins sample_clock_average_slow/sample]
  connect_bd_net -net trip_1 [get_bd_ports trip] [get_bd_pins trip_enable_0/trip] [get_bd_pins xlconcat_4/In0]
  connect_bd_net -net trip_enable_0_enable_out [get_bd_pins FO_BOARD1/ENABLE] [get_bd_pins trip_enable_0/enable_out]
  connect_bd_net -net vio_1_probe_out0 [get_bd_ports FO2] [get_bd_ports FO3] [get_bd_pins vio_1/probe_out0]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins processing_system7_0/IRQ_F2P] [get_bd_pins xlconcat_0/dout]
  connect_bd_net -net xlconcat_1_dout [get_bd_ports FO1] [get_bd_pins xlconcat_1/dout]
  connect_bd_net -net xlconcat_4_dout [get_bd_pins axi_gpio_input/gpio_io_i] [get_bd_pins xlconcat_4/dout]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins FO_BOARD1/AP11] [get_bd_pins FO_BOARD1/AP12] [get_bd_pins FO_BOARD1/AP21] [get_bd_pins FO_BOARD1/AP22] [get_bd_pins FO_BOARD1/AP31] [get_bd_pins FO_BOARD1/AP32] [get_bd_pins FO_BOARD1/AP41] [get_bd_pins FO_BOARD1/AP42] [get_bd_pins FO_BOARD1/AP51] [get_bd_pins FO_BOARD1/AP52] [get_bd_pins FO_BOARD1/AP61] [get_bd_pins FO_BOARD1/AP62] [get_bd_pins FO_BOARD1/BP11] [get_bd_pins FO_BOARD1/BP12] [get_bd_pins FO_BOARD1/BP21] [get_bd_pins FO_BOARD1/BP22] [get_bd_pins FO_BOARD1/BP31] [get_bd_pins FO_BOARD1/BP32] [get_bd_pins FO_BOARD1/BP41] [get_bd_pins FO_BOARD1/BP42] [get_bd_pins FO_BOARD1/BP51] [get_bd_pins FO_BOARD1/BP52] [get_bd_pins FO_BOARD1/BP61] [get_bd_pins FO_BOARD1/BP62] [get_bd_pins FO_BOARD1/PRELOAD1] [get_bd_pins xlconstant_0/dout]
  connect_bd_net -net xlconstant_1_dout [get_bd_pins FPGA_to_DDRRAM_0/data0] [get_bd_pins FPGA_to_DDRRAM_0/data1] [get_bd_pins FPGA_to_DDRRAM_0/data2] [get_bd_pins FPGA_to_DDRRAM_0/data3] [get_bd_pins FPGA_to_DDRRAM_0/data4] [get_bd_pins FPGA_to_DDRRAM_0/data5] [get_bd_pins FPGA_to_DDRRAM_0/data6] [get_bd_pins FPGA_to_DDRRAM_0/data7] [get_bd_pins FPGA_to_DDRRAM_0/data8] [get_bd_pins FPGA_to_DDRRAM_0/data9] [get_bd_pins FPGA_to_DDRRAM_0/data10] [get_bd_pins FPGA_to_DDRRAM_0/data11] [get_bd_pins FPGA_to_DDRRAM_0/data12] [get_bd_pins FPGA_to_DDRRAM_0/data13] [get_bd_pins FPGA_to_DDRRAM_0/data14] [get_bd_pins FPGA_to_DDRRAM_0/data15] [get_bd_pins FPGA_to_DDRRAM_0/data16] [get_bd_pins FPGA_to_DDRRAM_0/data17] [get_bd_pins FPGA_to_DDRRAM_0/data18] [get_bd_pins FPGA_to_DDRRAM_0/data19] [get_bd_pins FPGA_to_DDRRAM_0/data20] [get_bd_pins FPGA_to_DDRRAM_0/data21] [get_bd_pins FPGA_to_DDRRAM_0/data22] [get_bd_pins FPGA_to_DDRRAM_0/data23] [get_bd_pins FPGA_to_DDRRAM_0/data24] [get_bd_pins FPGA_to_DDRRAM_0/data25] [get_bd_pins FPGA_to_DDRRAM_0/data26] [get_bd_pins FPGA_to_DDRRAM_0/data27] [get_bd_pins FPGA_to_DDRRAM_0/data28] [get_bd_pins FPGA_to_DDRRAM_0/data29] [get_bd_pins FPGA_to_DDRRAM_0/data30] [get_bd_pins FPGA_to_DDRRAM_0/data31] [get_bd_pins FPGA_to_DDRRAM_0/data32] [get_bd_pins FPGA_to_DDRRAM_0/data33] [get_bd_pins FPGA_to_DDRRAM_0/data34] [get_bd_pins FPGA_to_DDRRAM_0/data35] [get_bd_pins FPGA_to_DDRRAM_0/data36] [get_bd_pins FPGA_to_DDRRAM_0/data37] [get_bd_pins FPGA_to_DDRRAM_0/data38] [get_bd_pins FPGA_to_DDRRAM_0/data39] [get_bd_pins FPGA_to_DDRRAM_0/data40] [get_bd_pins FPGA_to_DDRRAM_0/data41] [get_bd_pins FPGA_to_DDRRAM_0/data42] [get_bd_pins FPGA_to_DDRRAM_0/data43] [get_bd_pins FPGA_to_DDRRAM_0/data44] [get_bd_pins FPGA_to_DDRRAM_0/data45] [get_bd_pins FPGA_to_DDRRAM_0/data46] [get_bd_pins FPGA_to_DDRRAM_0/data47] [get_bd_pins FPGA_to_DDRRAM_0/data48] [get_bd_pins FPGA_to_DDRRAM_0/data49] [get_bd_pins FPGA_to_DDRRAM_0/data50] [get_bd_pins FPGA_to_DDRRAM_0/data51] [get_bd_pins FPGA_to_DDRRAM_0/data52] [get_bd_pins FPGA_to_DDRRAM_0/data53] [get_bd_pins FPGA_to_DDRRAM_0/data54] [get_bd_pins FPGA_to_DDRRAM_0/data55] [get_bd_pins FPGA_to_DDRRAM_0/data56] [get_bd_pins FPGA_to_DDRRAM_0/data57] [get_bd_pins FPGA_to_DDRRAM_0/data58] [get_bd_pins FPGA_to_DDRRAM_0/data59] [get_bd_pins FPGA_to_DDRRAM_0/data60] [get_bd_pins FPGA_to_DDRRAM_0/data61] [get_bd_pins FPGA_to_DDRRAM_0/data62] [get_bd_pins FPGA_to_DDRRAM_0/data63] [get_bd_pins debug_8_channel_1/channel_0] [get_bd_pins debug_8_channel_1/channel_1] [get_bd_pins debug_8_channel_1/channel_2] [get_bd_pins debug_8_channel_1/channel_3] [get_bd_pins debug_8_channel_1/channel_4] [get_bd_pins debug_8_channel_1/channel_5] [get_bd_pins debug_8_channel_1/channel_6] [get_bd_pins debug_8_channel_1/channel_7] [get_bd_pins xlconstant_1/dout]
  connect_bd_net -net xlconstant_2_dout [get_bd_pins trip_enable_0/trip_fpga] [get_bd_pins xlconstant_2/dout]
  connect_bd_net -net xlconstant_3_dout [get_bd_pins average_8_samples_slow_adc_0/data_in_5] [get_bd_pins average_8_samples_slow_adc_0/data_in_6] [get_bd_pins average_8_samples_slow_adc_0/data_in_7] [get_bd_pins average_8_samples_slow_adc_0/data_in_8] [get_bd_pins average_8_samples_slow_adc_0/data_in_9] [get_bd_pins average_8_samples_slow_adc_0/data_in_10] [get_bd_pins average_8_samples_slow_adc_0/data_in_11] [get_bd_pins average_8_samples_slow_adc_0/data_in_12] [get_bd_pins average_8_samples_slow_adc_0/data_in_13] [get_bd_pins average_8_samples_slow_adc_0/data_in_14] [get_bd_pins average_8_samples_slow_adc_0/data_in_15] [get_bd_pins average_8_samples_slow_adc_0/data_in_16] [get_bd_pins average_8_samples_slow_adc_0/data_in_17] [get_bd_pins average_8_samples_slow_adc_0/data_in_18] [get_bd_pins average_8_samples_slow_adc_0/data_in_19] [get_bd_pins average_8_samples_slow_adc_0/data_in_20] [get_bd_pins average_8_samples_slow_adc_0/data_in_21] [get_bd_pins average_8_samples_slow_adc_0/data_in_22] [get_bd_pins average_8_samples_slow_adc_0/data_in_23] [get_bd_pins average_8_samples_slow_adc_0/data_in_24] [get_bd_pins average_8_samples_slow_adc_0/data_in_25] [get_bd_pins average_8_samples_slow_adc_0/data_in_26] [get_bd_pins average_8_samples_slow_adc_0/data_in_27] [get_bd_pins average_8_samples_slow_adc_0/data_in_28] [get_bd_pins average_8_samples_slow_adc_0/data_in_29] [get_bd_pins average_8_samples_slow_adc_0/data_in_30] [get_bd_pins average_8_samples_slow_adc_0/data_in_31] [get_bd_pins average_8_samples_slow_adc_0/data_in_32] [get_bd_pins average_8_samples_slow_adc_0/data_in_33] [get_bd_pins average_8_samples_slow_adc_0/data_in_34] [get_bd_pins average_8_samples_slow_adc_0/data_in_35] [get_bd_pins average_8_samples_slow_adc_0/data_in_36] [get_bd_pins average_8_samples_slow_adc_0/data_in_37] [get_bd_pins average_8_samples_slow_adc_0/data_in_38] [get_bd_pins average_8_samples_slow_adc_0/data_in_39] [get_bd_pins average_8_samples_slow_adc_0/data_in_40] [get_bd_pins xlconstant_3/dout]
  connect_bd_net -net xlslice_0_Dout [get_bd_pins ADC_ads5272_1/reset_n] [get_bd_pins ADC_ads5272_2/reset_n] [get_bd_pins Resets/Dout]
  connect_bd_net -net xlslice_1_Dout [get_bd_pins Resets/Dout1] [get_bd_pins Slow_ADC/reset_n] [get_bd_pins average_8_samples_slow_adc_0/reset_n]
  connect_bd_net -net xlslice_2_Dout [get_bd_pins Resets/Dout6] [get_bd_pins clk_wiz_adclk/resetn]
  connect_bd_net -net xlslice_3_Dout [get_bd_pins FPGA_to_DDRRAM_0/reset_n] [get_bd_pins Resets/Dout2] [get_bd_pins sample_clock_average_slow/reset_n] [get_bd_pins sample_clock_pulse_SlowADC/reset_n]
  connect_bd_net -net xlslice_7_Dout [get_bd_pins ADC_ads5272_1/shift] [get_bd_pins ADC_ads5272_2/shift] [get_bd_pins Resets/Dout7]

  # Create address segments
  create_bd_addr_seg -range 0x00800000 -offset 0x1F800000 [get_bd_addr_spaces FPGA_to_DDRRAM_0/M_AXI_Full] [get_bd_addr_segs processing_system7_0/S_AXI_HP1/HP1_DDR_LOWOCM] SEG_processing_system7_0_HP1_DDR_LOWOCM
  create_bd_addr_seg -range 0x00010000 -offset 0x43C50000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs Slow_ADC/ADC_max11331_AXI_0/S0_AXI_Lite/S0_AXI_Lite_reg] SEG_ADC_max11331_AXI_0_S0_AXI_Lite_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43C10000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs Conv_ADC_slow_Full_0/AXI4_Lite/reg0] SEG_Conv_ADC_slow_Full_0_reg0
  create_bd_addr_seg -range 0x00010000 -offset 0x41220000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs FO_precarga/S_AXI/Reg] SEG_FO_precarga_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43DF0000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs FPGA_to_DDRRAM_0/S_AXI_Lite/S_AXI_Lite_reg] SEG_FPGA_to_DDRRAM_0_S_AXI_Lite_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43C20000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs PWM_28_HB_0/AXI4_Lite/reg0] SEG_PWM_28_HB_0_reg0
  create_bd_addr_seg -range 0x00010000 -offset 0x41200000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_gpio_0/S_AXI/Reg] SEG_axi_gpio_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x41240000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_gpio_band/S_AXI/Reg] SEG_axi_gpio_2_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x41210000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_gpio_input/S_AXI/Reg] SEG_axi_gpio_input_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43F60000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_gpio_interrupt_DDRRAM/S_AXI/Reg] SEG_axi_gpio_interrupt_DDRRAM_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x41230000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_gpio_reset/S_AXI/Reg] SEG_axi_gpio_reset_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x42800000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_timer_interrupt_to_PS/S_AXI/Reg] SEG_axi_timer_interrupt_to_PS_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43FA0000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs debug_8_channel_1/S_AXI_Lite/S_AXI_Lite_reg] SEG_debug_8_channel_1_S_AXI_Lite_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43D90000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs sample_clock_average_slow/S_AXI_Lite/S_AXI_Lite_reg] SEG_sample_clock_average_slow2_S_AXI_Lite_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43C00000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs sample_clock_pulse_SlowADC/S0_AXI_Lite/S0_AXI_Lite_reg] SEG_sample_clock_pulse_SlowADC_S0_AXI_Lite_reg


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


