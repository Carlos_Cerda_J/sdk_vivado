#-----------------------------
# IMPORTANT
# Use only for RTS Usach v3.1
#-----------------------------

# Clock definitions
create_clock -period 100.000 -name ADCLK1_P -waveform {0.000 50.000} [get_ports ADCLK1_P]
create_clock -period 100.000 -name ADCLK2_P -waveform {0.000 50.000} [get_ports ADCLK2_P]
create_clock -period 16.667 -name LCLK1_P -waveform {0.000 8.334} [get_ports LCLK1_P]
create_clock -period 16.667 -name LCLK2_P -waveform {0.000 8.334} [get_ports LCLK2_P]


#OpticalFiber1_FO 0 - B35_IO0
set_property PACKAGE_PIN B21 [get_ports {FO1[0]}]
#OpticalFiber1_FO 1 - B35_IO8
set_property PACKAGE_PIN B16 [get_ports {FO1[1]}]
#OpticalFiber1_FO 2 - B35_IO9
set_property PACKAGE_PIN A16 [get_ports {FO1[2]}]
#OpticalFiber1_FO 3 - B35_IO10
set_property PACKAGE_PIN B15 [get_ports {FO1[3]}]
#OpticalFiber1_FO 4 - B35_IO23
set_property PACKAGE_PIN D20 [get_ports {FO1[4]}]
#OpticalFiber1_FO 5 - B35_IO24
set_property PACKAGE_PIN C20 [get_ports {FO1[5]}]
#OpticalFiber1_FO 6 - B35_IO25
set_property PACKAGE_PIN E19 [get_ports {FO1[6]}]
#OpticalFiber1_FO 7 - B35_IO22
set_property PACKAGE_PIN E15 [get_ports {FO1[7]}]
#OpticalFiber1_FO 8 - B35_IO21
set_property PACKAGE_PIN E16 [get_ports {FO1[8]}]
#OpticalFiber1_FO 9 - B35_IO20
set_property PACKAGE_PIN F17 [get_ports {FO1[9]}]
#OpticalFiber1_FO 10 - B35_IO19
set_property PACKAGE_PIN D15 [get_ports {FO1[10]}]
#OpticalFiber1_FO 11 - B35_IO18
set_property PACKAGE_PIN C15 [get_ports {FO1[11]}]
#OpticalFiber1_FO 12 - B35_IO17
set_property PACKAGE_PIN G15 [get_ports {FO1[12]}]
#OpticalFiber1_FO 13 - B35_IO1
set_property PACKAGE_PIN A21 [get_ports {FO1[13]}]
#OpticalFiber1_FO 14 - B35_IO2
set_property PACKAGE_PIN B20 [get_ports {FO1[14]}]
#OpticalFiber1_FO 15 - B35_IO3
set_property PACKAGE_PIN B19 [get_ports {FO1[15]}]
#OpticalFiber1_FO 16 - B35_IO4
set_property PACKAGE_PIN A19 [get_ports {FO1[16]}]
#OpticalFiber1_FO 17 - B35_IO5
set_property PACKAGE_PIN A18 [get_ports {FO1[17]}]
#OpticalFiber1_FO 18 - B35_IO6
set_property PACKAGE_PIN B17 [get_ports {FO1[18]}]
#OpticalFiber1_FO 19 - B35_IO7
set_property PACKAGE_PIN A17 [get_ports {FO1[19]}]
#OpticalFiber1_FO 20 - B35_IO11
set_property PACKAGE_PIN E20 [get_ports {FO1[20]}]
#OpticalFiber1_FO 21 - B35_IO12
set_property PACKAGE_PIN F19 [get_ports {FO1[21]}]
#OpticalFiber1_FO 22 - B35_IO13
set_property PACKAGE_PIN D18 [get_ports {FO1[22]}]
#OpticalFiber1_FO 23 - B35_IO14
set_property PACKAGE_PIN E18 [get_ports {FO1[23]}]
#OpticalFiber1_FO 24 - B35_IO15
set_property PACKAGE_PIN D16 [get_ports {FO1[24]}]
#OpticalFiber1_FO 25 - B35_IO16
set_property PACKAGE_PIN F18 [get_ports {FO1[25]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO1[*]}]


#OpticalFiber2_FO 0 - B33_IO8
set_property PACKAGE_PIN U20 [get_ports {FO2[0]}]
#OpticalFiber2_FO 1 - B33_IO3
set_property PACKAGE_PIN U22 [get_ports {FO2[1]}]
#OpticalFiber2_FO 2 - B33_IO2
set_property PACKAGE_PIN T22 [get_ports {FO2[2]}]
#OpticalFiber2_FO 3 - B33_IO28
set_property PACKAGE_PIN U15 [get_ports {FO2[3]}]
#OpticalFiber2_FO 4 - B33_IO31
set_property PACKAGE_PIN V17 [get_ports {FO2[4]}]
#OpticalFiber2_FO 5 - B33_IO29
set_property PACKAGE_PIN U16 [get_ports {FO2[5]}]
#OpticalFiber2_FO 6 - B33_IO37
set_property PACKAGE_PIN V15 [get_ports {FO2[6]}]
#OpticalFiber2_FO 7 - B33_IO38
set_property PACKAGE_PIN V13 [get_ports {FO2[7]}]
#OpticalFiber2_FO 8 - B33_IO44
set_property PACKAGE_PIN Y13 [get_ports {FO2[8]}]
#OpticalFiber2_FO 9 - B33_IO39
set_property PACKAGE_PIN W13 [get_ports {FO2[9]}]
#OpticalFiber2_FO 10 - B33_IO42
set_property PACKAGE_PIN Y14 [get_ports {FO2[10]}]
#OpticalFiber2_FO 11 - B33_IO36
set_property PACKAGE_PIN V14 [get_ports {FO2[11]}]
#OpticalFiber2_FO 12 - B33_IO41
set_property PACKAGE_PIN Y15 [get_ports {FO2[12]}]
#OpticalFiber2_FO 13 - B33_IO30
set_property PACKAGE_PIN U17 [get_ports {FO2[13]}]
#OpticalFiber2_FO 14 - B33_IO9
set_property PACKAGE_PIN V20 [get_ports {FO2[14]}]
#OpticalFiber2_FO 15 - B33_IO11
set_property PACKAGE_PIN V19 [get_ports {FO2[15]}]
#OpticalFiber2_FO 16 - B33_IO6
set_property PACKAGE_PIN W20 [get_ports {FO2[16]}]
#OpticalFiber2_FO 17 - B33_IO10
set_property PACKAGE_PIN V18 [get_ports {FO2[17]}]
#OpticalFiber2_FO 18 - B33_IO16
set_property PACKAGE_PIN Y20 [get_ports {FO2[18]}]
#OpticalFiber2_FO 19 - B33_IO20
set_property PACKAGE_PIN Y19 [get_ports {FO2[19]}]
#OpticalFiber2_FO 20 - B33_IO25
set_property PACKAGE_PIN W18 [get_ports {FO2[20]}]
#OpticalFiber2_FO 21 - B33_IO22
set_property PACKAGE_PIN Y18 [get_ports {FO2[21]}]
#OpticalFiber2_FO 22 - B33_IO24
set_property PACKAGE_PIN W17 [get_ports {FO2[22]}]
#OpticalFiber2_FO 23 - B33_IO26
set_property PACKAGE_PIN W16 [get_ports {FO2[23]}]
#OpticalFiber2_FO 24 - B33_IO27
set_property PACKAGE_PIN Y16 [get_ports {FO2[24]}]
#OpticalFiber2_FO 25 - B33_IO40
set_property PACKAGE_PIN W15 [get_ports {FO2[25]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO2[*]}]


#OpticalFiber3_FO 0 - B13_L7N
set_property PACKAGE_PIN AB12 [get_ports {FO3[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[0]}]
#OpticalFiber3_FO 1 - B13_L8P
set_property PACKAGE_PIN AA11 [get_ports {FO3[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[1]}]
#OpticalFiber3_FO 2 - B13_L8N
set_property PACKAGE_PIN AB11 [get_ports {FO3[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[2]}]
#OpticalFiber3_FO 3 - B13_L9P
set_property PACKAGE_PIN AB10 [get_ports {FO3[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[3]}]
#OpticalFiber3_FO 4 - B13_L9N
set_property PACKAGE_PIN AB9 [get_ports {FO3[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[4]}]
#OpticalFiber3_FO 5 - B13_L17P
set_property PACKAGE_PIN AB7 [get_ports {FO3[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[5]}]
#OpticalFiber3_FO 6 - B13_L17N
set_property PACKAGE_PIN AB6 [get_ports {FO3[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[6]}]
#OpticalFiber3_FO 7 - B34_L8P - DISP_RX0_P
set_property PACKAGE_PIN J21 [get_ports {FO3[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[7]}]
#OpticalFiber3_FO 8 - B34_L8N - DISP_RX0_N
set_property PACKAGE_PIN J22 [get_ports {FO3[8]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[8]}]
#OpticalFiber3_FO 9 - B34_L10P - DISP_RX1_P
set_property PACKAGE_PIN L21 [get_ports {FO3[9]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[9]}]
#OpticalFiber3_FO 10 - B34_L10N - DISP_RX1_N
set_property PACKAGE_PIN L22 [get_ports {FO3[10]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[10]}]
#OpticalFiber3_FO 11 - B34_L15P - DISP_RX2_P
set_property PACKAGE_PIN M21 [get_ports {FO3[11]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[11]}]
#OpticalFiber3_FO 12 - B34_L15N - DISP_RX2_N
set_property PACKAGE_PIN M22 [get_ports {FO3[12]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[12]}]
#OpticalFiber3_FO 13 - B13_L7P
set_property PACKAGE_PIN AA12 [get_ports {FO3[13]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[13]}]
#OpticalFiber3_FO 14 - B33_IO12
set_property PACKAGE_PIN AA22 [get_ports {FO3[14]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[14]}]
#OpticalFiber3_FO 15 - B33_IO17
set_property PACKAGE_PIN Y21 [get_ports {FO3[15]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[15]}]
#OpticalFiber3_FO 16 - B33_IO7
set_property PACKAGE_PIN W21 [get_ports {FO3[16]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[16]}]
#OpticalFiber3_FO 17 - B33_IO5
set_property PACKAGE_PIN W22 [get_ports {FO3[17]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[17]}]
#OpticalFiber3_FO 18 - B33_IO4
set_property PACKAGE_PIN V22 [get_ports {FO3[18]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[18]}]
#OpticalFiber3_FO 19 - B33_IO1
set_property PACKAGE_PIN U21 [get_ports {FO3[19]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[19]}]
#OpticalFiber3_FO 20 - B33_IO0
set_property PACKAGE_PIN T21 [get_ports {FO3[20]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[20]}]
#OpticalFiber3_FO 21 - B35_IO27
set_property PACKAGE_PIN C18 [get_ports {FO3[21]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FO3[21]}]
#OpticalFiber3_FO 22 - B34_L17N - DISP_RX_CLK_N
set_property PACKAGE_PIN R21 [get_ports {FO3[22]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[22]}]
#OpticalFiber3_FO 23 - B34_L17P - DISP_RX_CLK_P
set_property PACKAGE_PIN R20 [get_ports {FO3[23]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[23]}]
#OpticalFiber3_FO 24 - B34_L16N - DISP_RX3_N
set_property PACKAGE_PIN P22 [get_ports {FO3[24]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[24]}]
#OpticalFiber3_FO 25 - B34_L16P - DISP_RX3_P
set_property PACKAGE_PIN N22 [get_ports {FO3[25]}]
set_property IOSTANDARD LVCMOS25 [get_ports {FO3[25]}]


#ADCLK  -   B35_IO29
set_property PACKAGE_PIN C17 [get_ports ADCLK]
set_property IOSTANDARD LVCMOS33 [get_ports ADCLK]

#ADCLK_P1   -   B13_L12_P
#ADCLK_N1   -   B13_L12_N
set_property PACKAGE_PIN Y9 [get_ports ADCLK1_P]
set_property PACKAGE_PIN Y8 [get_ports ADCLK1_N]
#ADCLK_P2   -   B13_L14_P
#ADCLK_N2   -   B13_L14_N
set_property PACKAGE_PIN AA7 [get_ports ADCLK2_P]
set_property PACKAGE_PIN AA6 [get_ports ADCLK2_N]
set_property IOSTANDARD LVDS_25 [get_ports ADCLK1*]
set_property IOSTANDARD LVDS_25 [get_ports ADCLK2*]
set_property DIFF_TERM true [get_ports ADCLK1*]
set_property DIFF_TERM true [get_ports ADCLK2*]

#LCLK_P1   -   B13_L11_P
#LCLK_N1   -   B13_L11_N
set_property PACKAGE_PIN AA9 [get_ports LCLK1_P]
set_property PACKAGE_PIN AA8 [get_ports LCLK1_N]
#LCLK_P2   -   B13_L13_P
#LCLK_N2   -   B13_L13_N
set_property PACKAGE_PIN Y6 [get_ports LCLK2_P]
set_property PACKAGE_PIN Y5 [get_ports LCLK2_N]
set_property IOSTANDARD LVDS_25 [get_ports LCLK*]
set_property DIFF_TERM true [get_ports LCLK*]

#ADC_P1   -   B13_L16_P
#ADC_N1   -   B13_L16_N
set_property PACKAGE_PIN AB5 [get_ports {ADC1_P[0]}]
set_property PACKAGE_PIN AB4 [get_ports {ADC1_N[0]}]
#ADC_P2   -   B13_L15_P
#ADC_N2   -   B13_L15_N
set_property PACKAGE_PIN AB2 [get_ports {ADC1_P[1]}]
set_property PACKAGE_PIN AB1 [get_ports {ADC1_N[1]}]
#ADC_P3   -   B13_L5_P
#ADC_N3   -   B13_L5_N
set_property PACKAGE_PIN U12 [get_ports {ADC1_P[2]}]
set_property PACKAGE_PIN U11 [get_ports {ADC1_N[2]}]
#ADC_P4   -   B13_L1_P
#ADC_N4   -   B13_L1_N
set_property PACKAGE_PIN V10 [get_ports {ADC1_P[3]}]
set_property PACKAGE_PIN V9 [get_ports {ADC1_N[3]}]
#ADC_P5   -   B13_L6_P
#ADC_N5   -   B13_L6_N
set_property PACKAGE_PIN U10 [get_ports {ADC1_P[4]}]
set_property PACKAGE_PIN U9 [get_ports {ADC1_N[4]}]
#ADC_P6   -   B13_L22_P
#ADC_N6   -   B13_L22_N
set_property PACKAGE_PIN U6 [get_ports {ADC1_P[5]}]
set_property PACKAGE_PIN U5 [get_ports {ADC1_N[5]}]
#ADC_P7   -   B13_L19_P
#ADC_N7   -   B13_L19_N
set_property PACKAGE_PIN R6 [get_ports {ADC1_P[6]}]
set_property PACKAGE_PIN T6 [get_ports {ADC1_N[6]}]
#ADC_P8   -   B13_L23_P
#ADC_N8   -   B13_L23_N
set_property PACKAGE_PIN V7 [get_ports {ADC1_P[7]}]
set_property PACKAGE_PIN W7 [get_ports {ADC1_N[7]}]
#ADC_P9   -   B13_L20_P
#ADC_N9   -   B13_L20_N
set_property PACKAGE_PIN T4 [get_ports {ADC2_P[0]}]
set_property PACKAGE_PIN U4 [get_ports {ADC2_N[0]}]
#ADC_P10   -   B13_L24_P
#ADC_N10   -   B13_L24_N
set_property PACKAGE_PIN W6 [get_ports {ADC2_P[1]}]
set_property PACKAGE_PIN W5 [get_ports {ADC2_N[1]}]
#ADC_P11   -   B13_L18_P
#ADC_N11   -   B13_L18_N
set_property PACKAGE_PIN Y4 [get_ports {ADC2_P[2]}]
set_property PACKAGE_PIN AA4 [get_ports {ADC2_N[2]}]
#ADC_P12   -   B13_L21_P
#ADC_N12   -   B13_L21_N
set_property PACKAGE_PIN V5 [get_ports {ADC2_P[3]}]
set_property PACKAGE_PIN V4 [get_ports {ADC2_N[3]}]
#ADC_P13   -   B13_L4_P
#ADC_N13   -   B13_L4_N
set_property PACKAGE_PIN V12 [get_ports {ADC2_P[4]}]
set_property PACKAGE_PIN W12 [get_ports {ADC2_N[4]}]
#ADC_P14   -   B13_L10_P
#ADC_N14   -   B13_L10_N
set_property PACKAGE_PIN Y11 [get_ports {ADC2_P[5]}]
set_property PACKAGE_PIN Y10 [get_ports {ADC2_N[5]}]
#ADC_P15   -   B13_L3_P
#ADC_N15   -   B13_L3_N
set_property PACKAGE_PIN W11 [get_ports {ADC2_P[6]}]
set_property PACKAGE_PIN W10 [get_ports {ADC2_N[6]}]
#ADC_P16   -   B13_L2_P
#ADC_N16   -   B13_L2_N
set_property PACKAGE_PIN V8 [get_ports {ADC2_P[7]}]
set_property PACKAGE_PIN W8 [get_ports {ADC2_N[7]}]
set_property IOSTANDARD LVDS_25 [get_ports ADC1*]
set_property IOSTANDARD LVDS_25 [get_ports ADC2*]
set_property DIFF_TERM true [get_ports ADC1*]
set_property DIFF_TERM true [get_ports ADC1*]

#CLOCK_S_ADC    -   B33_IO35
set_property PACKAGE_PIN AB14 [get_ports SPI_sck]
#MOSI_S_ADC    -   B33_IO43
set_property PACKAGE_PIN AA13 [get_ports SPI_mosi]
#CS_S_ADC    -   B33_IO47
set_property PACKAGE_PIN AB15 [get_ports SPI_ss_n]
#MISO_S_ADC1    -   B33_IO45
set_property PACKAGE_PIN AA14 [get_ports {SPI_miso[0]}]
#MISO_S_ADC2    -   B33_IO46
set_property PACKAGE_PIN AB16 [get_ports {SPI_miso[1]}]
#MISO_S_ADC3    -   B33_IO34
set_property PACKAGE_PIN AA16 [get_ports {SPI_miso[2]}]
#MISO_S_ADC4    -   B33_IO33
set_property PACKAGE_PIN AB17 [get_ports {SPI_miso[3]}]
#MISO_S_ADC5    -   B33_IO23
set_property PACKAGE_PIN AA18 [get_ports {SPI_miso[4]}]
#MISO_S_ADC6    -   B33_IO32
set_property PACKAGE_PIN AA17 [get_ports {SPI_miso[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports SPI_*]

#TRIP_GEN	-	B35_IO28
set_property PACKAGE_PIN D17 [get_ports trip]
set_property IOSTANDARD LVCMOS33 [get_ports trip]

#CPLD_ready	-	B35_IO26
set_property PACKAGE_PIN C19 [get_ports CPLD_ready]
set_property IOSTANDARD LVCMOS33 [get_ports CPLD_ready]

#ENC1A	-	B33_IO14
set_property PACKAGE_PIN AA21 [get_ports {Encoder1[0]}]
#ENC1B	-	B33_IO15
set_property PACKAGE_PIN AB21 [get_ports {Encoder1[1]}]
#ENC1C	-	B33_IO13
set_property PACKAGE_PIN AB22 [get_ports {Encoder1[2]}]
#ENC2A	-	B33_IO21
set_property PACKAGE_PIN AA19 [get_ports {Encoder2[0]}]
#ENC2B	-	B33_IO19
set_property PACKAGE_PIN AB20 [get_ports {Encoder2[1]}]
#ENC2C	-	B33_IO18
set_property PACKAGE_PIN AB19 [get_ports {Encoder2[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {Encoder1[*]}]
set_property IOSTANDARD LVCMOS33 [get_ports {Encoder2[*]}]


