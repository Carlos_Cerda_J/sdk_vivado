/*
 * File Name:         F:\IP_SORTING\switch_por_m1\ipcore\switch_by_minus_v1_0\include\switch_by_minus_addr.h
 * Description:       C Header File
 * Created:           2019-05-14 22:12:34
*/

#ifndef SWITCH_BY_MINUS_H_
#define SWITCH_BY_MINUS_H_

#define  IPCore_Reset_switch_by_minus       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_switch_by_minus      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_switch_by_minus   0x8  //contains unique IP timestamp (yymmddHHMM): 1905142212
#define  select_Data_switch_by_minus        0x100  //data register for Inport select

#endif /* SWITCH_BY_MINUS_H_ */
