/*
 * File Name:         E:\xilinx\IPCORES\PWM_scaler\ipcore\PWM_scaler_v1_0\include\PWM_scaler_addr.h
 * Description:       C Header File
 * Created:           2019-01-28 12:04:15
*/

#ifndef PWM_SCALER_H_
#define PWM_SCALER_H_

#define  IPCore_Reset_PWM_scaler       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_PWM_scaler      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_PWM_scaler   0x8  //contains unique IP timestamp (yymmddHHMM): 1901281204
#define  Cmax_i_Vdc_Data_PWM_scaler    0x100  //data register for Inport Cmax_i_Vdc
#define  Cmax_Data_PWM_scaler          0x104  //data register for Inport Cmax

#endif /* PWM_SCALER_H_ */
