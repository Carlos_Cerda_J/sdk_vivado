/*
 * File Name:         C:\IPCORES\AXI_CONST\ipcore\AXI_constant_v1_0\include\AXI_constant_addr.h
 * Description:       C Header File
 * Created:           2019-01-10 21:41:04
*/

#ifndef AXI_CONSTANT_H_
#define AXI_CONSTANT_H_

#define  IPCore_Reset_AXI_constant       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_AXI_constant      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_AXI_constant   0x8  //contains unique IP timestamp (yymmddHHMM): 1901102141
#define  value_Data_AXI_constant         0x100  //data register for Inport value

#endif /* AXI_CONSTANT_H_ */
