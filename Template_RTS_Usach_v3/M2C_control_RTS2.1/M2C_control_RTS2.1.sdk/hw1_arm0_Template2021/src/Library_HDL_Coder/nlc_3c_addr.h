/*
 * File Name:         F:\sort_3_cell\nlc3\ipcore\nlc_3c_v1_0\include\nlc_3c_addr.h
 * Description:       C Header File
 * Created:           2019-05-17 11:19:55
*/

#ifndef NLC_3C_H_
#define NLC_3C_H_

#define  IPCore_Reset_nlc_3c       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_nlc_3c      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_nlc_3c   0x8  //contains unique IP timestamp (yymmddHHMM): 1905171119
#define  Vc_ref_Data_nlc_3c        0x100  //data register for Inport Vc_ref

#endif /* NLC_3C_H_ */
