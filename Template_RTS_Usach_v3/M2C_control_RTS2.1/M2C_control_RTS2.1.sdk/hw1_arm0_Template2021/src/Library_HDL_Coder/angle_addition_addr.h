/*
 * File Name:         E:\sort_3_cell\angle_sum\ipcore\angle_addition_v1_0\include\angle_addition_addr.h
 * Description:       C Header File
 * Created:           2019-09-05 18:46:53
*/

#ifndef ANGLE_ADDITION_H_
#define ANGLE_ADDITION_H_

#define  IPCore_Reset_angle_addition       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_angle_addition      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_angle_addition   0x8  //contains unique IP timestamp (yymmddHHMM): 1909051846
#define  BETA_Data_angle_addition          0x100  //data register for Inport BETA

#endif /* ANGLE_ADDITION_H_ */
