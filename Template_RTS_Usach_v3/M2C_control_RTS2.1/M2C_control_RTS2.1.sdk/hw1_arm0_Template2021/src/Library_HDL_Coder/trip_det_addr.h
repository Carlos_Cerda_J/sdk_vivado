/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\trip_detector_fpga\trip_det\ipcore\trip_det_v1_0\include\trip_det_addr.h
 * Description:       C Header File
 * Created:           2020-01-02 17:37:45
*/

#ifndef TRIP_DET_H_
#define TRIP_DET_H_

#define  IPCore_Reset_trip_det       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_trip_det      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_trip_det   0x8  //contains unique IP timestamp (yymmddHHMM): 2001021737
#define  upTh_Data_trip_det          0x100  //data register for Inport upTh
#define  loTh_Data_trip_det          0x104  //data register for Inport loTh

#endif /* TRIP_DET_H_ */
