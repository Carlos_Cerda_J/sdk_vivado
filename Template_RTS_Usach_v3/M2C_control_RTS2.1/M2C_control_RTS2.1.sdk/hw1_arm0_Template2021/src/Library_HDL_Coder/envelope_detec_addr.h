/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\envelope_detector\env_detetor_trig\ipcore\envelope_detec_v1_0\include\envelope_detec_addr.h
 * Description:       C Header File
 * Created:           2019-09-03 09:44:31
*/

#ifndef ENVELOPE_DETEC_H_
#define ENVELOPE_DETEC_H_

#define  IPCore_Reset_envelope_detec       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_envelope_detec      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_envelope_detec   0x8  //contains unique IP timestamp (yymmddHHMM): 1909030944
#define  count_max_Data_envelope_detec     0x100  //data register for Inport count_max
#define  step_Data_envelope_detec          0x104  //data register for Inport step
#define  env_out_Data_envelope_detec       0x108  //data register for Outport env_out

#endif /* ENVELOPE_DETEC_H_ */
