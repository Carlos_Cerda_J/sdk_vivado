/*
 * debug_SoC.h
 *
 *  Created on: 25-10-2019
 *      Author: David Arancibia Gonzalez
 *      Version: 1.0
 */

#ifndef SRC_DEBUG_SOC_H_
#define SRC_DEBUG_SOC_H_

/***************************** Include Files *********************************/
//#include "FPGA_to_ARM_debug.h"
#include "FPGA_to_DDRRAM.h"

/************************** Constant Definitions *****************************/
#define OFFLINE_DATA_PRESET_NUMBER 4
#define DEBUG_SOC_OFFLINE_DATA_FPGA_MAX_MEMORY_SIZE 128000
#define DEBUG_SOC_OFFLINE_DATA_ARM0_MAX_MEMORY_SIZE 16000	// should be 64000 when data_offline_arm0_struct has only 16 32-bit variables.

#define DEBUG_SOC_OFFLINE_DATA_FPGA_MEMORY_ADDRESS	0x1F800000	//DDRRAM
#define DEBUG_SOC_OFFLINE_DATA_ARM0_MEMORY_ADDRESS	0x1F400000	//DDRRAM
#define DEBUG_SOC_ONLINE_DATA_FPGA_MEMORY_ADDRESS	0x00024000	//OCM
#define DEBUG_SOC_ONLINE_DATA_ARM0_MEMORY_ADDRESS	0x00020000	//OCM
#define DEBUG_SOC_ONLINE_DATA_ARM1_MEMORY_ADDRESS	0x00028000	//OCM

#define VARIABLE_INDEX_STATE_MACHINE	0
#define VARIABLE_INDEX_VO_RMS			1
#define VARIABLE_INDEX_FREQ_OUT			2
#define VARIABLE_INDEX_trips_fast		3
#define VARIABLE_INDEX_trips_slow1		4
#define VARIABLE_INDEX_trips_slow2		5

#define PARAMETER_INDEX_LIM_IABC		0
#define PARAMETER_INDEX_LIM_IO			1
#define PARAMETER_INDEX_LIM_VO			2
#define PARAMETER_INDEX_FREQ_OUT		3
#define PARAMETER_INDEX_VO_RMS_REF		4
#define PARAMETER_INDEX_START_CONTROL	5
#define PARAMETER_INDEX_REMOTE_CONTROL	6

/************************** Function Prototypes ******************************/
int init_debug_SoC_parameters(void);

/************************** Variable Definitions *****************************/
typedef struct _data_FPGA_to_ARM1_struct
{
	u32 variable[16];
} data_FPGA_to_ARM1_struct;

typedef struct _data_ARM0_to_ARM1_struct
{
	float variable[16];
	float timer_ns;
	float timer_ns_max;
	float timer_ns_min;
	u32 time_stamp;
	u32 status;
} data_ARM0_to_ARM1_struct;

typedef struct ACTIVATE_ST
{
	s16 notch_s;
	s16 notch_z;
	s16 notch_o;
	s16 notch_Vc_transf;
	s16 PR_s;
	s16 PR_z;
	s16 PR_o;
	s16 control_FPGA;
	s16 selector_NLC;
} ACTIVATE;

typedef struct SET_POINT_ST
{
	float Vo_peak;
	float Vo_freq_KHz;
//	float io_peak;
} SET_POINT;

typedef struct _data_ARM1_to_ARM0_struct
{
	//float parameter[16];
	//s16	selector_arm0[16];	//only 6 bits are necessary. use 16bits for change variable in expressions window
	SET_POINT ref;
	ACTIVATE activate;
	u32 time_stamp;
	u32 status;
	s8 activate_debugARM0;
} data_ARM1_to_ARM0_struct;

float *data_offline_64var_arm0_ptr[64];


//----------------------------------------------//
// Structs for debug_SoC_offline
typedef struct _debug_SoC_offline_fpga_data_struct
{
	u32 variable[16];
} debug_SoC_offline_fpga_data_struct;

typedef struct _debug_SoC_offline_arm0_data_struct
{
	float variable[64];
	u32 burst_counter;
} debug_SoC_offline_arm0_data_struct;

typedef struct _debug_SoC_parameters_struct
{
	s16	update_parameters;
	u16	enable;				//only 1 bit is necessary. use 16bits for change variable in expressions window
	u8 	status_done;
	u8 	status_error;
	u32 status_burst_count;
	u16 sample_clock_div;
	s32 fast_sample_frec_kHz;
	s32 max_data_points;
	u8 	ch_quantity;
	u16	plot_fpga_vars;		//only 1 bit is necessary. use 16bits for change variable in expressions window
	u16	plot_arm0_vars;		//only 1 bit is necessary. use 16bits for change variable in expressions window
	u16 plot_arm0_ALL;
	s16	selector_fpga[16];	//only 6 bits are necessary. use 16bits for change variable in expressions window
	s16	bits_frac_fpga[64];	//only 5 bits are necessary. use 16bits for change variable in expressions window
	s16	bits_word_fpga[64];	//only 5 bits are necessary. use 16bits for change variable in expressions window
	s16	selector_arm0[16];	//only 6 bits are necessary. use 16bits for change variable in expressions window
	float *variables_arm0_ptr[64];
	u32 counter_arm0;
	s16	ready_for_plot;
	s8	busy;
	u16	enable_slow_mode;
	u16	finish_slow_mode;
	s32 sample_frec_slow_mode;
	s32 max_data_points_slow_mode;
	s32 status_time_sampled_slow_mode_ms;
	s32 skip_samples_slow_mode;
} debug_SoC_parameters_struct;


typedef struct _offline_data_parameters_preset_struct
{
	s32 fast_sample_frec_kHz;
	s32 max_data_points;
	u8 ch_quantity;
	u16 plot_fpga_vars; //only 1 bit is necessary. use 16bits for change variable in expressions window
	u16 plot_arm0_vars; //only 1 bit is necessary. use 16bits for change variable in expressions window
	u16 plot_arm0_ALL;
	s16 selector_fpga[16]; //only 6 bits are necessary. use 16bits for change variable in expressions window
	s16 selector_arm0[16]; //only 6 bits are necessary. use 16bits for change variable in expressions window
} offline_data_parameters_preset_struct;


//----------------------------------------------//
// Struct for debug_SoC
typedef struct _debug_SoC_parameters_ARM0_struct
{
	s32 status_burst_count;
	s8 	status_enable;
	s8 	status_done;
	s8 	status_error;
	s8 	active;
	s32 counter_arm0;
	s16	selector_arm0[16];	//only 6 bits are necessary. use 16bits for change variable in expressions window
	float *variables_arm0_ptr[64];
} debug_SoC_parameters_ARM0_struct;

#endif /* SRC_DEBUG_SOC_H_ */
