#include "pefftaux.h"

const double THREE_INV		= 0.33333333333333333333333333333333;	// 1/3
const double SQRT_THREE	= 1.7320508075688772935274463415059;		// 1/sqrt(3)
const double TWO_SQRT_THREE = 3.4641016151377545870548926830117;		// 2*sqrt(3)
const double SQRT_THREE_INV	= 0.57735026918962576450914878050196;	// 1/sqrt(3)

const double PI_NUMBER	= 3.1415926535897932384626433832795;

//PR discretizado con tustin pre-warping
double CalculatePR(FORM_PR_CONTROLLER *PR, double in)
{
	double out_temp;
	double result_a0, result_a1, result_a2, result_b1, result_b2;

	result_a2 = PR->a2 * PR->in_2;
	result_a1 = PR->a1 * PR->in_1;
	result_a0 = PR->a0 * in;
	result_b2 = - PR->b2 * PR->out_2;
	result_b1 = - PR->b1 * PR->out_1;
	out_temp = result_a2 + result_a1 + result_a0 + result_b2 + result_b1;
	//out_temp = (PR->a2*PR->in_2) + (PR->a1*PR->in_1) + (PR->a0*in) - (PR->b2*PR->out_2) - (PR->b1*PR->out_1);

	PR->out = out_temp;
	PR->out_1 = out_temp;
	PR->out_2 = PR->out_1;
	PR->in_1  = in;
	PR->in_2  = PR->in_1;

	return out_temp;
}



void InitializeFilterNotch(FORM_FILTER_NOTCH *F, double fc, double delta_f, double h)
{
	//float h = 1/frequency_interrupt;	//sampling period
	//float fc = 50;	//notch frequency
	//float delta_f = 10;	//notch selectivity (-3db criteria)

	double w0 = 2.0 * PI_NUMBER * fc;
	double delta_w = 2.0 * PI_NUMBER * delta_f;

	double k1 = -cos(w0*h);
	double k2 = (1-tan((delta_w*h)/2))/(1+tan((delta_w*h)/2));
	double n1 = (1+k2)*0.5;
	double n2 = k1*(1+k2);

	F->a2 = n1;
	F->a1 = 2.0*n1*k1;
	F->a0 = n1;
	F->b2 = k2;
	F->b1 = n2;
	F->b0 = 1.0;
	F->fc = fc;
	F->delta_f = delta_f;
	F->h = h;

	ResetFilterNotch(F);	// Set to zero in, in_1, in_2, out, out_1, out_2
}


void ResetFilterNotch(FORM_FILTER_NOTCH *F)
{
	F->in = 0.0;
	F->in_1 = 0.0;
	F->in_2 = 0.0;
	F->out = 0.0;
	F->out_1 = 0.0;
	F->out_2 = 0.0;
}


double CalculateFilterNotchBandStop(FORM_FILTER_NOTCH *F, double in)
{
	double out_temp;
	F->in = in;

	out_temp = (F->a2*F->in_2) + (F->a1*F->in_1) + (F->a0*F->in) - (F->b2*F->out_2) - (F->b1*F->out_1);

	F->out = out_temp;
	F->out_1 = F->out;
	F->out_2 = F->out_1;
	F->in_1 = F->in;
	F->in_2 = F->in_1;

	return (out_temp);
}


double CalculateFilterNotchBandPass(FORM_FILTER_NOTCH *F, double in)
{
	double out_temp;

	out_temp = CalculateFilterNotchBandStop(F, in);

	return (in-out_temp);
}


void Transform_ABCPN2AB0SD(FORM_ABC_PN A,FORM_CLARKE_SIGMA_DELTA *B)
{
	B->alphaSigma = 0.5 * THREE_INV * (2.0 * A.ap + 2.0 * A.an - A.bp - A.bn - A.cp - A.cn);
	B->betaSigma  = 0.5 * SQRT_THREE_INV * (A.bp + A.bn - A.cp - A.cn);//Negative sequence 0.288675 * (-X_bP - X_bN + X_cP + X_cN);
	B->ceroSigma  = 0.5 * THREE_INV * (A.ap + A.an + A.bp + A.bn + A.cp + A.cn);
	B->alphaDelta = THREE_INV * (-2.0 * A.an + A.bn + A.cn + 2.0 * A.ap - A.bp - A.cp);
	B->betaDelta  = SQRT_THREE_INV * (A.bp - A.bn - A.cp + A.cn);//Negative sequence 0.577350 * (-X_bP + X_bN + X_cP - X_cN);
	B->ceroDelta  = THREE_INV * (A.ap - A.an + A.bp - A.bn + A.cp - A.cn);
}


void Transform_AB0SD2ABCPN(FORM_CLARKE_SIGMA_DELTA A, FORM_ABC_PN *B)
{
	B->ap = (A.alphaSigma + 0.5 * A.alphaDelta + A.ceroSigma + 0.5 * A.ceroDelta);
	B->bp = 0.25 * (-2.0 * A.alphaSigma - A.alphaDelta + TWO_SQRT_THREE * A.betaSigma + SQRT_THREE * A.betaDelta + 4.0 * A.ceroSigma + 2.0 * A.ceroDelta);
	B->cp = 0.25 * (-2.0 * A.alphaSigma - A.alphaDelta - TWO_SQRT_THREE * A.betaSigma - SQRT_THREE * A.betaDelta + 4.0 * A.ceroSigma + 2.0 * A.ceroDelta);
	B->an = (A.alphaSigma - 0.5 * A.alphaDelta + A.ceroSigma - 0.5 * A.ceroDelta);
	B->bn = 0.25 * (- 2.0 * A.alphaSigma + A.alphaDelta + TWO_SQRT_THREE * A.betaSigma - SQRT_THREE * A.betaDelta + 4.0 * A.ceroSigma - 2.0 * A.ceroDelta);
	B->cn = 0.25 * (- 2.0 * A.alphaSigma + A.alphaDelta - TWO_SQRT_THREE * A.betaSigma + SQRT_THREE * A.betaDelta + 4.0 * A.ceroSigma - 2.0 * A.ceroDelta);
}


void Transform_ABC2Clarke(FORM_ABC A, FORM_CLARKE *B)
{
	B->alpha = THREE_INV * (2.0*A.a - A.b - A.c);
	B->beta = SQRT_THREE_INV * (A.b - A.c);
	B->cero = A.a + A.b + A.c;
}


void Transform_Clarke2ABC(FORM_CLARKE A, FORM_ABC *B)
{
	B->a = A.alpha;
	B->b = 0.5 * (-A.alpha + (SQRT_THREE * A.beta));
	B->c = 0.5 * (-A.alpha - (SQRT_THREE * A.beta));
}


void Transform_Clarke2Park(FORM_CLARKE A, FORM_PARK *B, double theta)
{
	B->d =  (A.alpha * cos(theta)) + (A.beta * sin(theta));
	B->q = (-A.alpha * sin(theta)) + (A.beta * cos(theta));
	B->cero = A.cero;
}


void Transform_Park2Clarke(FORM_PARK A, FORM_CLARKE *B, double theta)
{
	B->alpha = (A.d * cos(theta)) - (A.q * sin(theta));
	B->beta  = (A.d * sin(theta)) + (A.q * cos(theta));
	B->cero = A.cero;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void PI_discreto_SAT(PI_form *PI,float in, float *out)
{
	double b0,b1;
	float umin;
	b0=PI->kp+PI->ki*PI->h/2.0;
	b1=-PI->kp+PI->ki*PI->h/2.0;

	umin=-1.0*PI->umax;
	double out_temp;
	out_temp=PI->out_1+in*b0+PI->in_1*b1;

	if(out_temp>PI->umax)
	{
		out_temp=PI->umax;
	}
	else if(out_temp<umin)
	{
		out_temp=umin;
	}
	else
	{
		PI->in_1=in;
	}
	PI->out_1=out_temp;
	*out=out_temp;

	PI->OUT=*out;
	PI->IN=in;
}

void PI_discreto_SAT_form2(PI_form *PI,float in, float *out)
{
	float kpzkiz,kpz;
	float umin;
	kpzkiz=PI->ki;
	kpz=PI->kp;

	umin=-1.0*PI->umax;
	float out_temp;
	out_temp=PI->out_1 + in*kpzkiz - PI->in_1*kpz;

	if(out_temp>PI->umax)
	{
		out_temp=PI->umax;
	}
	else if(out_temp<umin)
	{
		out_temp=umin;
	}
	else
	{
	PI->in_1=in;
	}

	PI->out_1=out_temp;
	*out=out_temp;

	PI->OUT=*out;
	PI->IN=in;
}


//PR discretizado con tustin pre-warping
void PR_tpw(PR_float_form *PR, float in, float *out)
{
	float out_temp;

	out_temp = (PR->a2*PR->in_2) + (PR->a1*PR->in_1) + (PR->a0*in) - (PR->b2*PR->out_2) - (PR->b1*PR->out_1);
	out_temp = out_temp + PR->FF;
	//*out = out_temp + PR->FF;

	if(out_temp > PR->umax)
	{
		*out = PR->umax;
	}
	else if(out_temp < -PR->umax)
	{
		*out = -PR->umax;
	}
	else
	{
		*out = out_temp;
	}

	PR->out = *out;
	PR->out_2 = PR->out_1;
	PR->out_1 = *out;//out_temp;	//Todo antes tomaba out_temp
	PR->in_2  = PR->in_1;
	PR->in_1  = in;
}


void abc2albe(float a, float b, float c, float *al, float *be)
{
	const float inv_raiz3 = 0.5773502692;
	const float inv_3 = 0.3333333333;

	*al = inv_3 * (2.0*a - b - c);
	*be = inv_raiz3 * (b - c);
}


void albe2dq(float al,float be,float *d,float *q,float ang)
{
	*d=al*cos(ang)+be*sin(ang);
	*q=-al*sin(ang)+be*cos(ang);
}


void dq2albe(float d,float q,float *al,float *be,float ang)
{
	*al=d*cos(ang)-q*sin(ang);
	*be=d*sin(ang)+q*cos(ang);
}


void gen_filter(Filter *F,float in, float *out)
{
	*out=-(F->out_1)*(F->a1)-(F->out_2)*(F->a2)+in*(F->b0)+(F->in_1)*(F->b1)+(F->in_2)*(F->b2);
	F->out_2=F->out_1;
	F->out_1=*out;
	F->in_2=F->in_1;
	F->in_1=in;
	F->in=in;
	F->out=*out;
}

// Author: David A.
void Calculate_filter_notch(Filter_notch_form *F, float in, float *out)
{
	*out = (F->a2*F->in_2) + (F->a1*F->in_1) + (F->a0*in) - (F->b2*F->out_2) - (F->b1*F->out_1);
	F->out_2 = F->out_1;
	F->out_1 = *out;
	F->in_2 = F->in_1;
	F->in_1 = in;
	F->in = in;
	F->out = *out;
}


void init_notch(Filter *F, float fc,float h)
{
	float a0;
	a0=fc*fc*h*h+2.0*sqrt(2.0)*fc*h+4.0;
	F->a0=a0;
	F->a1=(2.0*fc*fc*h*h-8.0)/a0;
	F->a2=(fc*h*fc*h-2.0*sqrt(2.0)*fc*h+4.0)/a0;
	F->b0=(fc*fc*h*h+4.0)/a0;
	F->b1=(2.0*h*h*fc*fc-8.0)/a0;
	F->b2=(F->b0);
	F->h=h;
	F->fc=fc;
}


// Author: David A.
void init_filter_notch(Filter_notch_form *F, float fc, float delta_f, float h)
{
	//float h = 1/frequency_interrupt;	//sampling period
	//float fc = 50;	//notch frequency
	//float delta_f = 10;	//notch selectivity (-3db criteria)

	const float pi = 3.14159265359;

	float w0 = 2.0 * pi * fc;
	float delta_w = 2.0 * pi * delta_f;

	float k1 = -cos(w0*h);
	float k2 = (1-tan((delta_w*h)/2))/(1+tan((delta_w*h)/2));
	float n1=(1+k2)*0.5;
	float n2=k1*(1+k2);

	F->a2 = n1;
	F->a1 = 2.0*n1*k1;
	F->a0 = n1;
	F->b2 = k2;
	F->b1 = n2;
	F->b0 = 1.0;
	F->fc = fc;
	F->delta_f = delta_f;
	F->h = h;

	F->in = 0.0;
	F->in_1 = 0.0;
	F->in_2 = 0.0;
	F->out = 0.0;
	F->out_1 = 0.0;
	F->out_2 = 0.0;
	//printf("Kp_s: %.2f\t Ki_s: %.2f\tTset_s: %f\n",Kp_s,Ki_s,Tset_s);
}


void init_butt_filter(Filter *F, float fc,float h)
{
	float a0;
	float G0=1.0;
	a0=h*fc+2.0;
	
	F->a0=a0;
	F->a1=(h*fc-2.0)/a0;
	F->a2=0.0;
	F->b0=(fc*h*G0)/a0;
	F->b1=F->b0;
	F->b2=0.0;
	F->h=h;
	F->fc=fc;
}

void init_zero_butt_filter(Filter *F, float fc,float h)
{
	float a0;
	float G0=1.0;
	a0=h*fc+2.0;

	F->in = 0.0;
	F->in_1 = 0.0;
	F->in_2 = 0.0;
	F->out = 0.0;
	F->out_1 = 0.0;
	F->out_2 = 0.0;
	F->a0=a0;
	F->a1=(h*fc-2.0)/a0;
	F->a2=0.0;
	F->b0=(fc*h*G0)/a0;
	F->b1=F->b0;
	F->b2=0.0;
	F->h=h;
	F->fc=fc;
}


void xy2ab0sd(XY A,AB0SD *B)
{
	B->al_S = 0.166667 * (2.0 *A.ap + 2.0 *A.an - A.bp - A.bn - A.cp - A.cn);
	B->beta_S = 0.288675 * (A.bp + A.bn - A.cp - A.cn);//Negative sequence 0.288675 * (-X_bP - X_bN + X_cP + X_cN);
	B->cero_S  = 0.166667 * (A.ap + A.an + A.bp + A.bn + A.cp + A.cn);
	B->al_D = 0.333333 * (-2.0 *A.an + A.bn + A.cn + 2.0 *A.ap - A.bp - A.cp);
	B->beta_D = 0.577350 * (A.bp - A.bn - A.cp + A.cn);//Negative sequence 0.577350 * (-X_bP + X_bN + X_cP - X_cN);
	B->cero_D  = 0.333333 * (A.ap - A.an + A.bp - A.bn + A.cp - A.cn);
}


void ramp_up_global(float *var, float delta, int samples)
{
    float increment;
    increment = delta / samples;
    *var = *var + increment;
}


float toFloat(int num,int bit_sig)
{
	num = num<<7;
	num = num>>7;
	return ((float)num/(float)(1 << bit_sig));
}


float toFloat2(int num, int bit_word, int bit_frac)
{
	int temp = 32 - bit_word;
	num = num<<temp;
	num = num>>temp;
	return ((float)num/(float)(1 << bit_frac));
}


int toFix(float num,int bit_sig)
{
	int result;

	result=(int)floor(num*pow(2,bit_sig));//round en vez de round

	return (result);
}


u16 Protection_func(float Value, float max, float min)
{
    u16 state = 0;   // 1 para sobre corriente/voltaje

    if((Value > max) || (Value < min))
    {
        state = 1;
    }

    return state;
}
