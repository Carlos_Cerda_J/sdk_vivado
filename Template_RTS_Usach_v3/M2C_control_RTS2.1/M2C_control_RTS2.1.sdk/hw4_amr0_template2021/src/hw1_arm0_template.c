/**
* @file  main.c
*
* MODIFICATION HISTORY:
*
* Ver   Who  Date	 Changes
* ---- ---- -------- -----------------------------------------------
* 1.00  DAG 30/07/18 First release
*
******************************************************************************/

/***************************** Include Files *********************************/
#include "main.h"

/**************************** Type Definitions *******************************/
typedef struct {
	u32 OutputHz;	/* Output frequency */
	XInterval Interval;	/* Interval value */
	u8 Prescaler;	/* Prescaler value */
	u16 Options;	/* Option settings */
} TmrCntrSetup;

/************************** Function Prototypes ******************************/
static int SetupTicker(void);
static int SetupTimer(int DeviceID);
static int SetupInterruptSystem(u16 IntcDeviceID, XScuGic *IntcInstancePtr);
static void TickHandler(void *CallBackRef);

/************************** Variable Definitions *****************************/
static XTtcPs TtcPsInst;	/* Two timer counters */
static TmrCntrSetup SettingsTable = {10000, 0, 0, 0};	/* Ticker timer counter initial setup, only output freq */
XScuGic InterruptController;  /* Interrupt controller instance */
static volatile u8 ErrorCount;		/* Errors seen at interrupt time */
static volatile u32 TickCount;		/* Ticker interrupts between PWM change */



int main(void)
{
	u32 aux, Status;

	for(int i=0; i<10000000;i++);	//uncomment if dual boot is used (BareMetal + FreeRTOS).

	init_global_variables();

	board_init();		// Initialize RTS. Drivers, ip-cores and Integrated circuits
	init_voltage_filters();
	mmc_control_init();	// Initialize all variables for controlling mmc
	init_PR_tpw_s();
	init_PR_double_s();
	init_PR_tpw_z();

	vars.enable.PR_s = 0;	// disable ARM PRs
	vars.enable.PR_z = 0;	// disable ARM PRs

	init_voltage_filters();

	/* Initialize timer PS for measure algorithms. */
//	Status = Timer_PS_init();
//	if (Status != XST_SUCCESS)
//	{
//		printf("PS timer Initialization Failed\r\n");
//		return XST_FAILURE;
//	}

//	/* Initialize AXI timer for PS periodic interrupts. */
//	Status = init_timer_fpga();
//	if (Status != XST_SUCCESS)
//	{
//		printf("Control_interrupt Initialization Failed\r\n");
//		return XST_FAILURE;
//	}

	//wait for n miliseconds
	for(int i=0; i<10000000; i++);

	/* Initialize AXI timer for PS periodic interrupts. */
	Status = init_interrupts();
	if (Status != XST_SUCCESS)
	{
		printf("Control_interrupt Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/*
	 * Set up the Ticker timer
	 */
	Status = SetupTicker();
	if (Status != XST_SUCCESS) {
		return Status;
	}

	boton_enable_SW = 0;
	init_PR_tpw_s();
	init_PR_double_s();
	init_PR_tpw_z();
	init_PR_tpw_AXI_1MHz();
	init_notch_cluster_currents();
	vars.ref.i0_sigma_peak = 0.0;

	//init NLC
	Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vdc_inv_Data_M2C_NLC_6cell_Control_Vo, toFix(1.0/10.0 ,17));
	Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vdc_inv_prop_Data_M2C_NLC_6cell_Control_Vo, toFix(1.0/10.0 ,17));
	Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_NLC_Data_M2C_NLC_6cell_Control_Vo, selector_NLC);	// classic NLC
	Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_V0S_Data_M2C_NLC_6cell_Control_Vo, selector_V0S);	// Vo from FPGA or ARM controllers

	// Activate notches
	Xil_Out32(XPAR_NOTCH_CLUSTER_CURRENTS_0_BASEADDR+activator_Data_Notch_cluster_currents, 0x1);

	//Band for slow and fast adc measurements
	XGpio_DiscreteWrite(&Gpio_band, GPIO_CHANNEL1, band_spikes_mitigation);

//	xil_printf("Initialization ends successfully\r\n");

	while (1)
	{

		/*--------------------------------------------------------------------------------------*/
		/* Check trip status																	*/
		/*--------------------------------------------------------------------------------------*/
		data.trip.cpld_gen = XGpio_DiscreteRead(&Gpio_trip, GPIO_CHANNEL1) & 0x0001;
		//data.trip.fpga_gen = TRIP_DETECTOR_CURRENTS_mReadReg(XPAR_TRIP_DETECTOR_CURRENTS_0_S_AXI_LITE_BASEADDR,TRIP_DETECTOR_CURRENTS_TRIGGERED_OFFSET);
		data.trip.fpga_voltP_gen = TRIP_DETECTOR_VOLTAGES_mReadReg(XPAR_TRIP_DETECTOR_VOLTAGES_0_S_AXI_BASEADDR,TRIP_DETECTOR_VOLTAGES_TRIGGERED_P_OFFSET);
		data.trip.fpga_voltN_gen = TRIP_DETECTOR_VOLTAGES_mReadReg(XPAR_TRIP_DETECTOR_VOLTAGES_0_S_AXI_BASEADDR,TRIP_DETECTOR_VOLTAGES_TRIGGERED_N_OFFSET);

		if((data.trip.cpld_gen >= 1) || (data.trip.fpga_voltP_gen >=1) || (data.trip.fpga_voltN_gen >=1))
		{
			data.trip.general = 1;
			Read_trip_triggered();
			Read_trip_instantaneous();
			switch(data.trip.triggered.fast_adc)
			{
				case 0x0001:
					strcpy(data.trip.triggered.name_fast, "i_aP");
					break;
				case 0x0002:
					strcpy(data.trip.triggered.name_fast, "i_aN");
					break;
				case 0x0004:
					strcpy(data.trip.triggered.name_fast, "i_bP");
					break;
				case 0x0008:
					strcpy(data.trip.triggered.name_fast, "i_bN");
					break;
				case 0x0020:
					strcpy(data.trip.triggered.name_fast, "v_o ");
					break;
				case 0x0100:
					strcpy(data.trip.triggered.name_fast, "i_o ");
					break;
				case 0x0200:
					strcpy(data.trip.triggered.name_fast, "i_cP");
					break;
				case 0x0400:
					strcpy(data.trip.triggered.name_fast, "i_cN");
					break;
				case 0x1000:
					strcpy(data.trip.triggered.name_fast, "i_a ");
					break;
				case 0x2000:
					strcpy(data.trip.triggered.name_fast, "i_b ");
					break;
				case 0x4000:
					strcpy(data.trip.triggered.name_fast, "i_c ");
					break;
				default:
					strcpy(data.trip.triggered.name_fast, "   ");
					break;
			}

		}
		else
		{
			data.trip.general = 0;
		}

		if(update_PR == 1)
		{
			init_PR_tpw_s();
			init_PR_double_s();
			init_PR_tpw_z();
			init_PR_tpw_AXI_1MHz();
			update_PR = 0;
		}


		if(reset_slow_adc == 1)
		{
			reset_gain_slow_adc();
			reset_slow_adc = 0;
		}

		if(offsets_update==1)
		{
			  Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset1_Data_fast_adc_conv,  toFix(offset_structs.Ia ,19));  //Iap
			  Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset2_Data_fast_adc_conv,  toFix(offset_structs.Iap,19));  //Iap
			  Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset3_Data_fast_adc_conv,  toFix(offset_structs.Ian,19));  //Ian
			  Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset4_Data_fast_adc_conv,  toFix(offset_structs.Io ,19));  //Io
			  Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset5_Data_fast_adc_conv,  toFix(offset_structs.Icp,19));  //Icp
			  Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset6_Data_fast_adc_conv,  toFix(offset_structs.Icn,19));  //Icp
			  Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset7_Data_fast_adc_conv,  toFix(offset_structs.Ibp,19));  //Icp
			  Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset8_Data_fast_adc_conv,  toFix(offset_structs.Ibn,19));  //Ibn
			  Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset9_Data_fast_adc_conv,  toFix(offset_structs.Ib,19));  //Ib
			  Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset10_Data_fast_adc_conv, toFix(offset_structs.Ic,19));  //Ic
			  Xil_Out32(XPAR_ADC_CONVERTER_0_BASEADDR + offset_Data_ADC_converter ,  toFix(offset_structs.Vo,11)); 	//Vo

			  Xil_Out32(XPAR_FAST_ADC_CONV_1_BASEADDR + Offset1_Data_fast_adc_conv,  toFix(offset_structs.Iap, 19));
			  Xil_Out32(XPAR_FAST_ADC_CONV_1_BASEADDR + Offset2_Data_fast_adc_conv,  toFix(offset_structs.Ibp, 19));
			  Xil_Out32(XPAR_FAST_ADC_CONV_1_BASEADDR + Offset3_Data_fast_adc_conv,  toFix(offset_structs.Icp, 19));
			  Xil_Out32(XPAR_FAST_ADC_CONV_1_BASEADDR + Offset4_Data_fast_adc_conv,  toFix(offset_structs.Ian, 19));
			  Xil_Out32(XPAR_FAST_ADC_CONV_1_BASEADDR + Offset5_Data_fast_adc_conv,  toFix(offset_structs.Ibn, 19));
			  Xil_Out32(XPAR_FAST_ADC_CONV_1_BASEADDR + Offset6_Data_fast_adc_conv,  toFix(offset_structs.Icn, 19));

			  offsets_update = 0;
		}

		if(cpld_res==1)	// Reset trip ip core and Reset CPLD
		{
			XGpio_DiscreteClear(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_TRIP_FPGA);
			Reset_CPLD();
			XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_TRIP_FPGA);
			cpld_res=0;
		}
		// End Check trip status ---------------------------------------------------------------//

		if(average_div_ant != average_div)
		{
			SAMPLE_CLOCK_mWriteReg(XPAR_SAMPLE_CLOCK_AVERAGE_S_AXI_LITE_BASEADDR, SAMPLE_CLOCK_S0_AXI_Lite_SLV_REG0_OFFSET, average_div); //clock for averaging 16*50kHzs
			average_div_ant = average_div;
		}

		if(change_fast_adc_pattern == 1)
		{
			fast_adc_pattern(pattern);
			change_fast_adc_pattern = 0;
		}

		if(re_init_fast_adcs==1)
		{
			fast_adc_init();
			re_init_fast_adcs = 0;
		}

		if(re_init_fast_adcs_IP==1)
		{
			XGpio_DiscreteClear(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_FAST_ADC);	// Reset debug memory IP Core
			XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_FAST_ADC);	// Reset debug memory IP Core
			re_init_fast_adcs_IP = 0;
		}

		// Change control interrupt frequency
		if(frequency_interrupt != frequency_interrupt_prev)
		{
			frequency_interrupt_prev = frequency_interrupt;

			aux = (AXI_CLOCK_FREQUENCY / frequency_interrupt) - 2;
			Interrupt_SetLoadReg(aux);
		}

		if(master_reset == 1)
		{
			board_init();		// Initialize RTS. Drivers, ip-cores and Integrated circuits
			mmc_control_init();	// Initialize all variables for controlling mmc

			/* Initialize AXI timer for PS periodic interrupts. */
			Status = Control_interrupt_init();
			if (Status != XST_SUCCESS)
			{
				xil_printf("Control_interrupt Initialization Failed\r\n");
				return XST_FAILURE;
			}

			master_reset = 0;
		}

		//Actualizacion de constantes de PR y de PI
		if(start_PR==0)
		{
			init_PI_voltage();
			init_PI_Vout();
			start_PR=1;
		}

/*
		if(offline_data_parameters.finish_slow_mode == 1)
		{
			printf("\n\nclear;\n");

			//------------------------
			//print out arm0 variables

			if(offline_data_parameters.plot_arm0_ALL == 0)
			{
				//Print out 16 arm0 variables
				for(int i=0; i<16; i++)
				{
					//printf("arm0_var%d = [", i);
					printf("arm0.var(%d,:) = [", i+1);
					for(int j=0; j<offline_data_parameters.max_data_points_slow_mode; j++)
					{
						printf("%.3f ", data_offline_arm0[j].variable[i]);
					}
					printf("];\n");
				}
			}
			else
			{
				//Print out ALL arm0 variables (60)
				for(int i=0; i<64; i++)
				{
					//printf("arm0_var%d = [", i);
					printf("arm0.var(%d,:) = [", i+1);
					for(int j=0; j<offline_data_parameters.max_data_points_slow_mode; j++)
					{
						printf("%.3f ", data_offline_arm0[j].variable[i]);
					}
					printf("];\n");
				}

			}
			//print out usefull constants
			printf("sampling_freq_target = %lu;\n", offline_data_parameters.sample_frec_slow_mode);
			printf("arm0_sampling_freq = %u;\n", (unsigned int)(10000/offline_data_parameters.skip_samples_slow_mode));
			printf("arm0_data_points = %lu;\n", offline_data_parameters.max_data_points_slow_mode);
			printf("arm0_data_points = %lu;\n", counter_arm0_points);
			printf("fpga_activate = 0;\n");
			printf("arm0_activate = %d;\n", offline_data_parameters.plot_arm0_vars);
			printf("arm0_ALL_activate = %d;\n", offline_data_parameters.plot_arm0_ALL);

			// Reset plot counter and enable plot
			counter_arm0_points = 0;
			offline_data_parameters.enable_slow_mode = 0;
			offline_data_parameters.finish_slow_mode = 0;
		}
*/

		//Update parameters coming from ARM1
		if(new_arm1_to_arm0_data_flag == 1)
		{
			//limit_Iabc 		= data_FreeRTOS2BareMetal.parameter[PARAMETER_INDEX_LIM_IABC];
			//limit_Io 		= data_FreeRTOS2BareMetal.parameter[PARAMETER_INDEX_LIM_IO];
			//limit_Vo 		= data_FreeRTOS2BareMetal.parameter[PARAMETER_INDEX_LIM_VO];
			//freq_out_ref 	= data_FreeRTOS2BareMetal.parameter[PARAMETER_INDEX_FREQ_OUT];
			//Vo_rms_ref 		= data_FreeRTOS2BareMetal.parameter[PARAMETER_INDEX_VO_RMS_REF];
			//start_control 	= (u8)data_FreeRTOS2BareMetal.parameter[PARAMETER_INDEX_START_CONTROL];
			//remote_control 	= (u8)data_FreeRTOS2BareMetal.parameter[PARAMETER_INDEX_REMOTE_CONTROL];
			activate_notch_s = data_FreeRTOS2BareMetal.activate.notch_s;
			activate_notch_z = data_FreeRTOS2BareMetal.activate.notch_z;
			activate_notch_Vc_transf = data_FreeRTOS2BareMetal.activate.notch_Vc_transf;
			vars.enable.PR_s = data_FreeRTOS2BareMetal.activate.PR_s;
			vars.enable.PR_z = data_FreeRTOS2BareMetal.activate.PR_z;
			vars.enable.PR_o = data_FreeRTOS2BareMetal.activate.PR_o;
			PR_control_FPGA = data_FreeRTOS2BareMetal.activate.control_FPGA;
			selector_NLC = data_FreeRTOS2BareMetal.activate.selector_NLC;
			out_openloop = 0.5*data_FreeRTOS2BareMetal.ref.Vo_peak;
			frequency_Vout_khz = data_FreeRTOS2BareMetal.ref.Vo_freq_KHz;

//			debug_SoC_parameters_ARM0.selector_arm0[0] = data_FreeRTOS2BareMetal.selector_arm0[0];
//			debug_SoC_parameters_ARM0.selector_arm0[1] = data_FreeRTOS2BareMetal.selector_arm0[1];
//			debug_SoC_parameters_ARM0.selector_arm0[2] = data_FreeRTOS2BareMetal.selector_arm0[2];
//			debug_SoC_parameters_ARM0.selector_arm0[3] = data_FreeRTOS2BareMetal.selector_arm0[3];
//			debug_SoC_parameters_ARM0.selector_arm0[4] = data_FreeRTOS2BareMetal.selector_arm0[4];
//			debug_SoC_parameters_ARM0.selector_arm0[5] = data_FreeRTOS2BareMetal.selector_arm0[5];
//			debug_SoC_parameters_ARM0.selector_arm0[6] = data_FreeRTOS2BareMetal.selector_arm0[6];
//			debug_SoC_parameters_ARM0.selector_arm0[7] = data_FreeRTOS2BareMetal.selector_arm0[7];
//			debug_SoC_parameters_ARM0.selector_arm0[8] = data_FreeRTOS2BareMetal.selector_arm0[8];
//			debug_SoC_parameters_ARM0.selector_arm0[9] = data_FreeRTOS2BareMetal.selector_arm0[9];
//			debug_SoC_parameters_ARM0.selector_arm0[10] = data_FreeRTOS2BareMetal.selector_arm0[10];
//			debug_SoC_parameters_ARM0.selector_arm0[11] = data_FreeRTOS2BareMetal.selector_arm0[11];
//			debug_SoC_parameters_ARM0.selector_arm0[12] = data_FreeRTOS2BareMetal.selector_arm0[12];
//			debug_SoC_parameters_ARM0.selector_arm0[13] = data_FreeRTOS2BareMetal.selector_arm0[13];
//			debug_SoC_parameters_ARM0.selector_arm0[14] = data_FreeRTOS2BareMetal.selector_arm0[14];
//			debug_SoC_parameters_ARM0.selector_arm0[15] = data_FreeRTOS2BareMetal.selector_arm0[15];
			arm1_time_stamp = data_FreeRTOS2BareMetal.time_stamp;

			new_arm1_to_arm0_data_flag = 0;
		}
	}

	return XST_SUCCESS;
}




int mmc_control_init(void)
{
	/*--------------------------------------------------------------------------------------*/
	/* Open grid switches (contactors)														*/
	/*--------------------------------------------------------------------------------------*/
	XGpio_DiscreteClear(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_0);
	XGpio_DiscreteClear(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_1);
	XGpio_DiscreteSet(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_2);
	// End Open grid switches (contactors) -------------------------------------------------//


	/*--------------------------------------------------------------------------------------*/
	/* unReset FPGA IP Cores																*/
	/*--------------------------------------------------------------------------------------*/
	XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_SAMPLE_CLK);	// unReset sample clock IP
	XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_FAST_ADC);		// unReset fast adc IP Core
	XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_SLOW_ADC);		// unReset slow adc IP Core
	XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_PLL);			// Enable PLL
	XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_TRIP_FPGA);
	//XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_NOTCH);
	// End unReset FPGA IP Cores --------------------------------------------------------------//


	/*--------------------------------------------------------------------------------------*/
	/* Set sample clocks limit values														*/
	/*--------------------------------------------------------------------------------------*/
	SAMPLE_CLOCK_mWriteReg(XPAR_SAMPLE_CLOCK_PULSE_SLOWADC_S0_AXI_LITE_BASEADDR, SAMPLE_CLOCK_50DUTY_S_AXI_Lite_SLV_REG0_OFFSET, 1250); //clock sampling slow adc 100k
	SAMPLE_CLOCK_mWriteReg(XPAR_SAMPLE_CLOCK_PLL_S0_AXI_LITE_BASEADDR, SAMPLE_CLOCK_S0_AXI_Lite_SLV_REG0_OFFSET, 1000);  //pll sampling clock
	SAMPLE_CLOCK_mWriteReg(XPAR_SAMPLE_CLOCK_AVERAGE_S_AXI_LITE_BASEADDR, SAMPLE_CLOCK_50DUTY_S_AXI_Lite_SLV_REG0_OFFSET, 125); //clock for averaging 16*50kHzs=125   16*10kHz=625
	SAMPLE_CLOCK_mWriteReg(XPAR_SAMPLE_CLOCK_AVERAGE_SLOW_S_AXI_LITE_BASEADDR, SAMPLE_CLOCK_50DUTY_S_AXI_Lite_SLV_REG0_OFFSET, 1250); //2000 antes, 1250 para 80 kHz ? clock sampling


	/*--------------------------------------------------------------------------------------*/
	/* Set Slow ADC gains and offsets 														*/
	/*--------------------------------------------------------------------------------------*/

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain1_Data_slow_adc_conv, toFix(0.3234432 ,23)); //ap1
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset1_Data_slow_adc_conv, toFix(0.0,20));	//toFix(-1.2819715,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain2_Data_slow_adc_conv, toFix(0.3161993 ,23)); //ap2
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset2_Data_slow_adc_conv, toFix(2.846 ,20));	//toFix(2.529602,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain3_Data_slow_adc_conv, toFix(0.3154786 ,23)); //ap3
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset3_Data_slow_adc_conv, toFix(2.8075 ,20));	//toFix(2.523804,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain4_Data_slow_adc_conv, toFix(0.3158449 ,23)); //ap4
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset4_Data_slow_adc_conv, toFix(1.5790 ,20));	//toFix(1.579224,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain5_Data_slow_adc_conv, toFix(0.3090250 ,23)); //ap5
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset5_Data_slow_adc_conv, toFix(0.8189 ,20));	//toFix(0.6180420,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain6_Data_slow_adc_conv, toFix(0.3152921 ,23)); //ap6
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset6_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(-0.3153076,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain7_Data_slow_adc_conv, toFix(0.3225754 ,23)); //bp1
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset7_Data_slow_adc_conv, toFix(-3.5480 ,20));	//toFix(-0.1911359,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain8_Data_slow_adc_conv, toFix(0.3160515 ,23)); //bp2
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset8_Data_slow_adc_conv, toFix(3.0279 ,20));	//toFix(1.1724759,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain9_Data_slow_adc_conv, toFix(0.3153245 ,23)); //bp3
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset9_Data_slow_adc_conv, toFix(-0.9554 ,20));	//toFix(0.0693470,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain10_Data_slow_adc_conv, toFix(0.3153332 ,23)); //bp4
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset10_Data_slow_adc_conv, toFix(1.6022 ,20));	//toFix(2.2590827,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain11_Data_slow_adc_conv, toFix(0.3149156 ,23)); //bp5
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset11_Data_slow_adc_conv, toFix(6.1846 ,20));	//toFix(0.9663132,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain12_Data_slow_adc_conv, toFix(0.3144327 ,23)); //bp6
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset12_Data_slow_adc_conv, toFix(-2.8300 ,20));	//toFix(-1.2574281,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain13_Data_slow_adc_conv, toFix(0.3085048 ,23)); //cp1
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset13_Data_slow_adc_conv, toFix(-1.2371 ,20));	//toFix(-2.0817070,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain14_Data_slow_adc_conv, toFix(0.3137527 ,23)); //cp2
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset14_Data_slow_adc_conv, toFix(4.6966 ,20));	//toFix(6.2407619,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain15_Data_slow_adc_conv, toFix(0.3151309 ,23)); //cp3
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset15_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(1.6560655,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain16_Data_slow_adc_conv, toFix(0.3140255 ,23)); //cp4
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset16_Data_slow_adc_conv, toFix(0.3360 ,20));	//toFix(2.4361996,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain17_Data_slow_adc_conv, toFix(0.3226172 ,23)); //cp5
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset17_Data_slow_adc_conv, toFix(-0.3262 ,20));	//toFix(2.7476129,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain18_Data_slow_adc_conv, toFix(0.3160331 ,23)); //cp6
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset18_Data_slow_adc_conv, toFix(-0.3161 ,20));	//toFix(-0.3931286,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain19_Data_slow_adc_conv, toFix(0.3091573 ,23)); //an1
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset19_Data_slow_adc_conv, toFix(2.4730 ,20));	//toFix(3.5181537,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain20_Data_slow_adc_conv, toFix(0.3143980 ,23)); //an2
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset20_Data_slow_adc_conv, toFix(3.7730 ,20));	//toFix(2.2878431,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain21_Data_slow_adc_conv, toFix(0.3146335 ,23)); //an3
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset21_Data_slow_adc_conv, toFix(3.1272 ,20));	//toFix(2.1738175,20));



	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain1_Data_slow_adc_conv, toFix(0.5 ,23)); //an4
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset1_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(0.3280158,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain2_Data_slow_adc_conv, toFix(0.5 ,23)); //an5
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset2_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(1.0582057,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain3_Data_slow_adc_conv, toFix(0.5 ,23)); //an6
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset3_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(-0.3211617,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain4_Data_slow_adc_conv, toFix(0.5 ,23)); //bn1
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset4_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(2.1052037,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain5_Data_slow_adc_conv, toFix(0.5 ,23)); //bn2
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset5_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(1.2583556,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain6_Data_slow_adc_conv, toFix(0.5 ,23)); //bn3
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset6_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(2.1037142,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain7_Data_slow_adc_conv, toFix(0.5 ,23)); //bn4
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset7_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(3.1131781,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain8_Data_slow_adc_conv, toFix(0.5 ,23)); //bn5
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset8_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(2.0403743,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain9_Data_slow_adc_conv, toFix(0.5 ,23)); //bn6
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset9_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(2.0616417,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain10_Data_slow_adc_conv, toFix(0.5 ,23)); //cn1
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset10_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(1.8878815,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain11_Data_slow_adc_conv, toFix(0.5 ,23)); //cn2
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset11_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(3.7693732,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain12_Data_slow_adc_conv, toFix(0.5 ,23)); //cn3
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset12_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(3.5730416,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain13_Data_slow_adc_conv, toFix(0.5 ,23)); //cn4
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset13_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(1.9002854,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain14_Data_slow_adc_conv, toFix(0.5 ,23)); //cn5
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset14_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(1.8864702,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain15_Data_slow_adc_conv, toFix(0.5 ,23)); //cn6
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset15_Data_slow_adc_conv, toFix(0.0 ,20));	//toFix(4.2970981,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain16_Data_slow_adc_conv, toFix(0.5 ,23)); //not used
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset16_Data_slow_adc_conv, toFix(0.0,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain17_Data_slow_adc_conv, toFix(0.5 ,23)); //not used
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset17_Data_slow_adc_conv, toFix(0.0,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain18_Data_slow_adc_conv, toFix(0.5 ,23)); //not used
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset18_Data_slow_adc_conv, toFix(0.0,20));



	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain19_Data_slow_adc_conv, toFix(0.3624933 ,23)); //vab
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset19_Data_slow_adc_conv, toFix(0.0,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain20_Data_slow_adc_conv, toFix(0.3650782 ,23)); //vbc
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset20_Data_slow_adc_conv, toFix(0.0,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain21_Data_slow_adc_conv, toFix(0.3681648 ,23)); //vca
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset21_Data_slow_adc_conv, toFix(0.0,20));
	// End Set Slow ADC gains and offsets --------------------------------------------------//


	/*--------------------------------------------------------------------------------------*/
	/* Set Fast ADC gains and offsets 														*/
	/*--------------------------------------------------------------------------------------*/
	//FAST ADC GAINS (25,24) AND OFFSETS (25,19)
	// Grid currents
	Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Gain1_Data_fast_adc_conv, toFix(0.00504616,24));  	//Ia
	//Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset1_Data_fast_adc_conv, toFix(-10.32077916,19));  //Ia

	Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Gain9_Data_fast_adc_conv, toFix(0.00504056,24));  	//Ib
	//Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset9_Data_fast_adc_conv, toFix(-10.29499514,19));	//Ib

	Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Gain10_Data_fast_adc_conv, toFix(0.00505362,24));		//Ic
	//Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset10_Data_fast_adc_conv, toFix(-10.34011213,19));	//Ic


	// Cluster currents
	Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Gain2_Data_fast_adc_conv,toFix(0.00509908,24));  			//Iap
	//Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset2_Data_fast_adc_conv, toFix(-10.48033718,19));  	//Iap

	Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Gain3_Data_fast_adc_conv, toFix(-0.00510340 ,24));  		//Ian
	//Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset3_Data_fast_adc_conv, toFix(10.46109474,19));  	//Ian

	Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Gain7_Data_fast_adc_conv, toFix(0.00510495,24));  		//Ibp
	//Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset7_Data_fast_adc_conv, toFix(-10.49052578,19));  	//Ibp

	Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Gain8_Data_fast_adc_conv, toFix(-0.00509499,24));  		//Ibn
	//Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset8_Data_fast_adc_conv, toFix(10.50017273,19)); 	//Ibn

	Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Gain5_Data_fast_adc_conv, toFix(0.00504185,24));  		//Icp
	//Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset5_Data_fast_adc_conv, toFix(-10.33508014,19));	//Icp

	Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Gain6_Data_fast_adc_conv, toFix(-0.00503965,24));  		//Icn
	//Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset6_Data_fast_adc_conv, toFix(10.28012334,19));  	//Icn

	// Output current and voltage
	Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Gain4_Data_fast_adc_conv, toFix(-0.00504293,24));  		//Io
	//Xil_Out32(XPAR_FAST_ADC_CONV_0_BASEADDR + Offset4_Data_fast_adc_conv, toFix(10.32316338,19));  	//Io

	Xil_Out32(XPAR_ADC_CONVERTER_0_BASEADDR + gain_Data_ADC_converter, toFix(-3.13663825,22));  		//Vo -3.12494832
	//Xil_Out32(XPAR_ADC_CONVERTER_0_BASEADDR + offset_Data_ADC_converter,toFix(6436.3817,11)); 	//Vo 6385.23814288

	// Cluster currents (debug, 2.5MHz sampling)
	Xil_Out32(XPAR_FAST_ADC_CONV_1_BASEADDR + Gain1_Data_fast_adc_conv, toFix(0.00509908,24)); 			//Iap
	Xil_Out32(XPAR_FAST_ADC_CONV_1_BASEADDR + Gain2_Data_fast_adc_conv, toFix(0.00510495,24));  		//Ibp
	Xil_Out32(XPAR_FAST_ADC_CONV_1_BASEADDR + Gain3_Data_fast_adc_conv, toFix(0.00504185,24));  		//Icp
	Xil_Out32(XPAR_FAST_ADC_CONV_1_BASEADDR + Gain4_Data_fast_adc_conv, toFix(-0.00510340 ,24));  		//Ian
	Xil_Out32(XPAR_FAST_ADC_CONV_1_BASEADDR + Gain5_Data_fast_adc_conv, toFix(-0.00509499,24));  		//Ibn
	Xil_Out32(XPAR_FAST_ADC_CONV_1_BASEADDR + Gain6_Data_fast_adc_conv, toFix(-0.00503965,24));  		//Icn

	// End Set fast ADC gains and offsets --------------------------------------------------//
//	  printf("\n original offsets \n");
//	  printf("Ia: -10.32077916\n");
//	  printf("Ib: -10.29499514\n");
//	  printf("Ic: -10.34011213\n");
//	  printf("Iap: -10.48033718\n");
//	  printf("Ian: 10.46109474\n");
//	  printf("Ibp: -10.49052578\n");
//	  printf("Ibn: 10.50017273\n");
//	  printf("Icp: -10.33508014\n");
//	  printf("Icn: 10.28012334\n");
//	  printf("Io: 10.32316338\n");
//	  printf("Vo: 6436.3817\n");
//	  printf("Vab: 5.3763805\n");
//	  printf("Vbc: 4.4411759\n");
//	  printf("Vca: 0.6185169\n");

	/*--------------------------------------------------------------------------------------*/
	/* Set grid voltage PLL parameters 														*/
	/*--------------------------------------------------------------------------------------*/
	int v_gg_aux=toFix(1/(V_grid*1.2),21);
	int v_g_aux=toFix(V_grid*1.2,14);
	Xil_Out32(XPAR_PLL2_IP_0_BASEADDR + kpz_Data_PLL2_ip, 0x085400a);  //25,17
	Xil_Out32(XPAR_PLL2_IP_0_BASEADDR + kpzPkiz_Data_PLL2_ip,0x0854b39); //25,17
	Xil_Out32(XPAR_PLL2_IP_0_BASEADDR + T_s_Data_PLL2_ip, 0x0000015); //25,21
	Xil_Out32(XPAR_PLL2_IP_0_BASEADDR + normalizer_Data_PLL2_ip, v_gg_aux); //25,21 //0x0001d42 //(1/Vd) 0x000c115
	Xil_Out32(XPAR_PLL2_IP_0_BASEADDR + denormalizer_Data_PLL2_ip, v_g_aux); //25,14 (Vd) 0x00a9b4a
	// End Set grid voltage PLL parameters -------------------------------------------------//


	/*--------------------------------------------------------------------------------------*/
	/* Set load voltage low-pass filter parameters 														*/
	/*--------------------------------------------------------------------------------------*/
	T_ipfil=0.000001;
	Tfil=T_ipfil;
	wcfil=2.0*3.14159*800.0;
	wfil=(2.0/Tfil)*tan(wcfil*Tfil/2.0);
	qqfil=1.0/sqrt(2);
	afil=4.0/(Tfil*Tfil);
	bfil=(2.0/Tfil)*(wfil/qqfil);
	pfil=afil+bfil+wfil*wfil;
	qfil=2.0*wfil*wfil-2*afil;
	rfil=wfil*wfil+afil-bfil;
	VOa1=wfil*wfil/pfil;
	VOa2=2.0*wfil*wfil/pfil;
	VOa3=VOa1;
	VOa4=qfil/pfil;
	VOa5=rfil/pfil;

	SAMPLE_CLOCK_50DUTY_mWriteReg(XPAR_SAMPLE_CLOCK_VOFILT_S_AXI_LITE_BASEADDR, SAMPLE_CLOCK_50DUTY_S_AXI_Lite_SLV_REG0_OFFSET, 100); //VO FILTER SAMPLE TIME
	Xil_Out32(XPAR_VO_LPF_0_BASEADDR + a1_Data_vo_LPF, toFix(VOa1,24));
	Xil_Out32(XPAR_VO_LPF_0_BASEADDR + a2_Data_vo_LPF, toFix(VOa2,24));
	Xil_Out32(XPAR_VO_LPF_0_BASEADDR + a3_Data_vo_LPF, toFix(VOa3,24));
	Xil_Out32(XPAR_VO_LPF_0_BASEADDR + a4_Data_vo_LPF, toFix(VOa4,23));
	Xil_Out32(XPAR_VO_LPF_0_BASEADDR + a5_Data_vo_LPF, toFix(VOa5,23));
	Xil_Out32(XPAR_VO_LPF_0_BASEADDR + offset_Data_vo_LPF, toFix(VO_filt_off,12));
	// End Set load voltage low-pass filter parameters -------------------------------------//
	// End Set load voltage low-pass filter parameters -------------------------------------//


	/*--------------------------------------------------------------------------------------*/
	/* Initialization for PR and PI controllers												*/
	/*--------------------------------------------------------------------------------------*/
	init_PI_voltage();
	init_PI_Vout();
	// End Initialization for PR and PI controllers ----------------------------------------//

	Xil_Out32(XPAR_THETA_GEN_EXTERN_0_BASEADDR+limit_Data_theta_gen_extern, toFix(1000,13)); //200 puntos para hacer seno ref

	Xil_Out32(XPAR_TRIP_CURR_BITS_0_BASEADDR+Umbral_P_Data_Trip_curr_bits, toFix(3916,11)); //limite superior trip corrientes
	Xil_Out32(XPAR_TRIP_CURR_BITS_0_BASEADDR+Umbral_N_Data_Trip_curr_bits, toFix(45,11)); //limite inferior trip corrientes

	// Vo amplitude
	Xil_Out32(XPAR_SINE_LUT_0_BASEADDR + A_Data_Sine_LUT, toFix(0.0,13));

	// Vo envelope detector offset (due to fix point resolution)
	Xil_Out32(XPAR_VO_LPF_0_BASEADDR + offset_Data_vo_LPF, toFix(VO_filt_off,12));

	// set position of cluster reference selector Switch
	Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_V0S_Data_M2C_NLC_6cell_Control_Vo, 0x0); //1 control FPGA: 0 control ARM

	/*--------------------------------------------------------------------------------------*/
	/* variables for ramp up operation instead of step changes								*/
	/*--------------------------------------------------------------------------------------*/
    Vo_ref.actual = 50;
    Vo_ref.delta = 0.015;//0.01; //100V/seg
    Vo_ref.final = 50;

    freqOut_ref.actual = frequency_Vout_khz;
    freqOut_ref.delta = 0.0015;
    freqOut_ref.final = frequency_Vout_khz;

    //read button status
    enable_button.actual = (XGpio_DiscreteRead(&Gpio_button, GPIO_CHANNEL1) & 0x0008) >> 3;
    enable_button.previous = enable_button.actual;

    //Set trip fpga threshold values
    Xil_Out32(XPAR_TRIP_DET_0_BASEADDR + upTh_Data_trip_det, 1750);	//1920 for 600V
    Xil_Out32(XPAR_TRIP_DET_0_BASEADDR + loTh_Data_trip_det, -2000);	//-350 for -100V

    //Set trip fpga counter to avoid false trips
    TRIP_DETECTOR_VOLTAGES_mWriteReg(XPAR_TRIP_DETECTOR_VOLTAGES_0_S_AXI_BASEADDR, TRIP_DETECTOR_VOLTAGES_MAX_COUNT_OFFSET, 5);

    //Reset trips
	XGpio_DiscreteClear(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_TRIP_FPGA);
	Reset_CPLD();
	XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_TRIP_FPGA);
	return XST_SUCCESS;
}


void Offsets_function(void)
{
	int i;
	if(enable_offset==1 )
	{
		if(offset_counter<1000)
		{
			XGpio_DiscreteClear(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_DEBUG_8CH);	// Reset debug memory IP Core
			XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_DEBUG_8CH);		// Enable the begin of sampling

			for(i=0;i<100;i++);	// Wait a little time for debug to finish capture

			offset_acumulator_structs.Ia  = offset_acumulator_structs.Ia  + (0.001*toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST1_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG0_OFFSET),16));
			offset_acumulator_structs.Iap = offset_acumulator_structs.Iap + (0.001*toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST1_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG1_OFFSET),16));
			offset_acumulator_structs.Ian = offset_acumulator_structs.Ian + (0.001*toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST1_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG2_OFFSET),16));
			offset_acumulator_structs.Io  = offset_acumulator_structs.Io  + (0.001*toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST1_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG3_OFFSET),16));
			offset_acumulator_structs.Icp = offset_acumulator_structs.Icp + (0.001*toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST1_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG4_OFFSET),16));
			offset_acumulator_structs.Icn = offset_acumulator_structs.Icn + (0.001*toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST1_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG5_OFFSET),16));
			offset_acumulator_structs.Ibp = offset_acumulator_structs.Ibp + (0.001*toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST1_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG6_OFFSET),16));
			offset_acumulator_structs.Ibn = offset_acumulator_structs.Ibn + (0.001*toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST1_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG7_OFFSET),16));

			offset_acumulator_structs.Ib  = offset_acumulator_structs.Ib  + (0.001*toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST2_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG0_OFFSET),16));
			offset_acumulator_structs.Ic  = offset_acumulator_structs.Ic  + (0.001*toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST2_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG1_OFFSET),16));
			offset_acumulator_structs.Vo  = offset_acumulator_structs.Vo  + (0.001*toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST2_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG2_OFFSET),12));
			offset_acumulator_structs.Vab = offset_acumulator_structs.Vab + (0.001*toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR+out_19_Data_slow_adc_conv),14));
			offset_acumulator_structs.Vbc = offset_acumulator_structs.Vbc + (0.001*toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR+out_20_Data_slow_adc_conv),14));
			offset_acumulator_structs.Vca = offset_acumulator_structs.Vca + (0.001*toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR+out_21_Data_slow_adc_conv),14));

			offset_counter++;
		}
		else
		{

			offset_counter = 0;
			enable_offset = 0;

			//check offsets calculado
			int offset_error = 0;

			if( (int)offset_acumulator_structs.Ia != 10)
			{
				offset_error = 1;
			}
			if( (int)offset_acumulator_structs.Ib != 10)
			{
				offset_error = 1;
			}
			if( (int)offset_acumulator_structs.Ic != 10)
			{
				offset_error = 1;
			}
			if( (int)offset_acumulator_structs.Iap != 10)
			{
				offset_error = 1;
			}
			if( (int)offset_acumulator_structs.Ian != -10)
			{
				offset_error = 1;
			}
			if( (int)offset_acumulator_structs.Ibp != 10)
			{
				offset_error = 1;
			}
			if( (int)offset_acumulator_structs.Ibn != -10)
			{
				offset_error = 1;
			}
			if( (int)offset_acumulator_structs.Icp != 10)
			{
				offset_error = 1;
			}
			if( (int)offset_acumulator_structs.Icn != -10)
			{
				offset_error = 1;
			}
			if( (int)offset_acumulator_structs.Io != -10)
			{
				offset_error = 1;
			}
//			if( (int)offset_acumulator_structs.Vo != -6336)
//			{
//				offset_error = 1;
//			}

			if(offset_error == 1)
			{
				printf("!!! ERROR OFFSET !!!\n");
			}
		}
	}
	else
	{
		offset_structs.Ia  = -offset_acumulator_structs.Ia;
		offset_structs.Iap = -offset_acumulator_structs.Iap;
		offset_structs.Ian = -offset_acumulator_structs.Ian;
		offset_structs.Io  = -offset_acumulator_structs.Io;
		offset_structs.Icp = -offset_acumulator_structs.Icp;
		offset_structs.Icn = -offset_acumulator_structs.Icn;
		offset_structs.Ibp = -offset_acumulator_structs.Ibp;
		offset_structs.Ibn = -offset_acumulator_structs.Ibn;
		offset_structs.Ib  = -offset_acumulator_structs.Ib;
		offset_structs.Ic  = -offset_acumulator_structs.Ic;
		offset_structs.Vo  = -offset_acumulator_structs.Vo;
		offset_structs.Vab = -offset_acumulator_structs.Vab;
		offset_structs.Vbc = -offset_acumulator_structs.Vbc;
		offset_structs.Vca = -offset_acumulator_structs.Vca;
	}
}


void reset_PI(void)
{
	int i;

	id_plus=0.0;
	id_sigma_min_s=0.0;
	iq_sigma_min_s=0.0;
	id_sigma_plus_s=0.0;

	for(i=0;i<6;i++)
	{
		PI_Volt[i].in_1=0.0;
		PI_Volt[i].in_2=0.0;
		PI_Volt[i].OUT=0.0;
		PI_Volt[i].out_1=0.0;
		PI_Volt[i].out_2=0.0;
		//PI_Volt[i].ki=0.0;
		//PI_Volt[i].kp=0.0;
	}
}

void init_PI_Vout(void){
	//init the Vout PI
	//Tpi=1.0/10000;
	//kppi=57.0; //kp PI VO
	//api=276.0;

	kipi=api*kppi; //KI pi VO
	azpi=-(api*Tpi/2.0-1.0)/(api*Tpi/2.0+1.0);
	kzpi=kppi*(1.0+api*Tpi/2.0);
	kpzpi=kzpi*azpi;
	kizpi=kzpi*(1.0-azpi);

	PI_Vout.ki=kpzpi+kizpi;
	PI_Vout.kp=kpzpi;
	PI_Vout.h=Tpi;
	PI_Vout.umax=v0s_max;
	PI_Vout.in_1 = 0.0;
	PI_Vout.in_2 = 0.0;
	PI_Vout.out_1 = 0.0;
	PI_Vout.out_2 = 0.0;
	PI_Vout.FF = 0.0;
}

void Control_Vout(void){

}

void Control_voltage(void)
{
	int i;

	////////////////////////////////////////
	//			Control de voltaje  	  //
	////////////////////////////////////////

	if(control_promedio==1)
	{
		//PI_discreto_SAT(&PI_Volt[0], Vc_ref-Vc_transf_filt.cero_S, &id_plus);
		PI_discreto_SAT(&PI_Volt[0], Vc_ref-g.control.measFiltered.vCapacitorsAB0SD.ceroSigma , &id_plus);
	}
	else
	{
		PI_Volt[0].in_1=0.0;
		PI_Volt[0].in_2=0.0;
		PI_Volt[0].OUT=0.0;
		PI_Volt[0].out_1=0.0;
		PI_Volt[0].out_2=0.0;
		id_plus=0.0;
	}

	//Control Intra Falta agregar opcion seq negativa
	if(control_intra==1)
	{
		//Secuencia negativa de la corriente de entrada
		if(intra_al == 1)
		{
			//PI_discreto_SAT(&PI_Volt[1], -Vc_transf_filt.al_S, &id_min);
			PI_discreto_SAT(&PI_Volt[1], -g.control.measFiltered.vCapacitorsAB0SD.alphaSigma, &id_min);
		}
		else
		{
			id_min = 0.0;
			PI_Volt[1].in_1=0.0;
			PI_Volt[1].in_2=0.0;
			PI_Volt[1].OUT=0.0;
			PI_Volt[1].out_1=0.0;
			PI_Volt[1].out_2=0.0;
		}

		if(intra_be == 1)
		{
			//PI_discreto_SAT(&PI_Volt[2], -Vc_transf_filt.beta_S, &iq_min);
			PI_discreto_SAT(&PI_Volt[2], -g.control.measFiltered.vCapacitorsAB0SD.betaSigma, &iq_min);

		}
		else
		{
			iq_min = 0.0;
			PI_Volt[2].in_1=0.0;
			PI_Volt[2].in_2=0.0;
			PI_Volt[2].OUT=0.0;
			PI_Volt[2].out_1=0.0;
			PI_Volt[2].out_2=0.0;
		}
		id_sigma_plus_o=0.0;
		iq_sigma_plus_o=0.0;
	}
	else
	{
		for(i=1;i<3;i++)
		{
			PI_Volt[i].in_1=0.0;
			PI_Volt[i].in_2=0.0;
			PI_Volt[i].OUT=0.0;
			PI_Volt[i].out_1=0.0;
			PI_Volt[i].out_2=0.0;
		}
		id_sigma_plus_o=0.0;
		iq_sigma_plus_o=0.0;
		id_min=0.0;
		iq_min=0.0;
	}

	//Control Inner
	if(control_inner==1)
	{
//		PI_discreto_SAT(&PI_Volt[3], -Vc_transf_filt.al_D, &id_sigma_min_s);
//		PI_discreto_SAT(&PI_Volt[4], -Vc_transf_filt.beta_D, &iq_sigma_min_s);
//		PI_discreto_SAT(&PI_Volt[5], -Vc_transf_filt.cero_D, &id_sigma_plus_s);

		PI_discreto_SAT(&PI_Volt[3], -g.control.measFiltered.vCapacitorsAB0SD.alphaDelta, &id_sigma_min_s);
		PI_discreto_SAT(&PI_Volt[4], -g.control.measFiltered.vCapacitorsAB0SD.betaDelta, &iq_sigma_min_s);
		PI_discreto_SAT(&PI_Volt[5], -g.control.measFiltered.vCapacitorsAB0SD.ceroDelta, &id_sigma_plus_s);
	}
	else
	{
		for(i=3;i<6;i++)
		{
			PI_Volt[i].in_1=0.0;
			PI_Volt[i].in_2=0.0;
			PI_Volt[i].OUT=0.0;
			PI_Volt[i].out_1=0.0;
			PI_Volt[i].out_2=0.0;
		}
		id_sigma_min_s=0.0;
		iq_sigma_min_s=0.0;
		id_sigma_plus_s=0.0;

	}
}

void init_voltage_filters(void){
	init_filter_notch(&notch_s[0], 50, 150, T_proc);
	init_filter_notch(&notch_s[1], 50, 150, T_proc);

	InitializeFilterNotch(&g.filters.notchIAlpha, 50.0, 150.0, T_proc);
	InitializeFilterNotch(&g.filters.notchIBeta,  50.0, 150.0, T_proc);

	InitializeFilterNotch(&g.filters.notchIAlphaSigma, 50.0, 150.0, T_proc);
	InitializeFilterNotch(&g.filters.notchIBetaSigma,  50.0, 150.0, T_proc);
	InitializeFilterNotch(&g.filters.notchIAlphaDelta, 50.0, 150.0, T_proc);
	InitializeFilterNotch(&g.filters.notchIBetaDelta,  50.0, 150.0, T_proc);

	init_filter_notch(&notch_Iap, 50, 30, T_proc);
	init_filter_notch(&notch_Ibp, 50, 30, T_proc);
	init_filter_notch(&notch_Icp, 50, 30, T_proc);
	init_filter_notch(&notch_Ian, 50, 30, T_proc);
	init_filter_notch(&notch_Ibn, 50, 30, T_proc);
	init_filter_notch(&notch_Icn, 50, 30, T_proc);

	InitializeFilterNotch(&g.filters.notchIap, 50.0, 30.0, T_proc);
	InitializeFilterNotch(&g.filters.notchIbp, 50.0, 30.0, T_proc);
	InitializeFilterNotch(&g.filters.notchIcp, 50.0, 30.0, T_proc);
	InitializeFilterNotch(&g.filters.notchIan, 50.0, 30.0, T_proc);
	InitializeFilterNotch(&g.filters.notchIbn, 50.0, 30.0, T_proc);
	InitializeFilterNotch(&g.filters.notchIcn, 50.0, 30.0, T_proc);


	init_filter_notch(&notch_Vab, 50, 30, T_proc);
	init_filter_notch(&notch_Vbc, 50, 30, T_proc);
	init_filter_notch(&notch_Vca, 50, 30, T_proc);

	init_zero_butt_filter(&low_pass_Vc_alpha_S, F_low*2.0*3.1416, T_proc);
	init_zero_butt_filter(&low_pass_Vc_beta_S,  F_low*2.0*3.1416, T_proc);
	init_zero_butt_filter(&low_pass_Vc_cero_S,  F_low*2.0*3.1416, T_proc);
	init_zero_butt_filter(&low_pass_Vc_alpha_D, F_low*2.0*3.1416, T_proc);
	init_zero_butt_filter(&low_pass_Vc_beta_D,  F_low*2.0*3.1416, T_proc);
	init_zero_butt_filter(&low_pass_Vc_cero_D,  F_low*2.0*3.1416, T_proc);

//	init_butt_filter(&low_pass_Vc_alpha_S2, F_low*2.0*3.1416, T_proc);
//	init_butt_filter(&low_pass_Vc_beta_S2,  F_low*2.0*3.1416, T_proc);
//	init_butt_filter(&low_pass_Vc_cero_S2,  F_low*2.0*3.1416, T_proc);
//	init_butt_filter(&low_pass_Vc_alpha_D2, F_low*2.0*3.1416, T_proc);
//	init_butt_filter(&low_pass_Vc_beta_D2,  F_low*2.0*3.1416, T_proc);
//	init_butt_filter(&low_pass_Vc_cero_D2,  F_low*2.0*3.1416, T_proc);

	init_butt_filter(&low_pass_vc_envelope,2.0*2.0*3.14159,T_proc);
	init_butt_filter(&low_pass_vc_envelop,2.0*2.0*3.14159,T_proc);

	init_zero_butt_filter(&low_pass_vc_envelope,2.0*2.0*3.14159,T_proc);
	init_zero_butt_filter(&low_pass_vc_envelop,2.0*2.0*3.14159,T_proc);

}

void init_PI_voltage(void)
{
	double hv = T_proc;
	float Vd = V_grid;
	float Vc = Vc_ref;
	double kk;

	//0S
	a1 = -2.0*exp(-xi_b1*wn_b1*hv)*cos(sqrt(1.0-xi_b1*xi_b1)*wn_b1*hv);
	a2 = exp(-2.0*xi_b1*wn_b1*hv);
	kk = 4.0*C*Vc/n/Vd;
	kp_b1D_prom = -(3.0*kk+kk*a1-kk*a2)/(2.0*hv);
	ki_b1D_prom = -kk/hv/hv*(1.0+a1+a2);

	//abS
	a1 = -2.0*exp(-xi_b2*wn_b2*hv)*cos(sqrt(1.0-xi_b2*xi_b2)*wn_b2*hv);
	a2 = exp(-2.0*xi_b2*wn_b2*hv);
	kk = 4.0*C*Vc/n/Vd;
	kp_b1D = -(3.0*kk+kk*a1-kk*a2)/(2.0*hv);
	ki_b1D = -kk/hv/hv*(1.0+a1+a2);

	//ab0D
	a1 = -2.0*exp(-xi_b3*wn_b3*hv)*cos(sqrt(1.0-xi_b3*xi_b3)*wn_b3*hv);
	a2 = exp(-2*xi_b3*wn_b3*hv);
	kk = C*Vc/n/Vd;
	kp_b2D = -(3.0*kk+kk*a1-kk*a2)/(2.0*hv);
	ki_b2D = -kk/hv/hv*(1.0+a1+a2);

	//sintonia PI promedio
	PI_Volt[0].ki = ki_b1D_prom;
	PI_Volt[0].kp = kp_b1D_prom;
	PI_Volt[0].h  = T_proc;
	PI_Volt[0].umax = id_max;

	//sintonia pi balance con sec negativa. 1 id_min, 2 iq_min
	PI_Volt[1].ki = ki_b1D;
	PI_Volt[1].kp = kp_b1D;
	PI_Volt[1].h  = T_proc;
	PI_Volt[1].umax = 5;

	PI_Volt[2].ki = -ki_b1D;
	PI_Volt[2].kp = -kp_b1D;
	PI_Volt[2].h  = T_proc;
	PI_Volt[2].umax = 5;

	//sintonia pi balance con corrientes circulantes. 3 id_sigma_min, 4 iq_sigma_min, 5 id_sigma_pos,
	PI_Volt[3].ki = ki_b2D;
	PI_Volt[3].kp = kp_b2D;
	PI_Volt[3].h  = T_proc;
	PI_Volt[3].umax = 5;

	PI_Volt[4].ki = -ki_b2D;
	PI_Volt[4].kp = -kp_b2D;
	PI_Volt[4].h  = T_proc;
	PI_Volt[4].umax = 5;

	PI_Volt[5].ki = ki_b2D;
	PI_Volt[5].kp = kp_b2D;
	PI_Volt[5].h  = T_proc;
	PI_Volt[5].umax = 5;
}


void Reset_CPLD(void)
{
	/* Clear GPIO bit 0: Reset CPLD */
	//XGpio_DiscreteClear(&Gpio, GPIO_CHANNEL, GPIO_pin_0);
	XGpioPs_WritePin(&Gpiops, GPIOps_pin_MIO9, 0x0);

	for(u16 i=0; i<1000 ; i++)
	{
		//NOP
	}

	/* SET GPIO bit 0: un-Reset CPLD */
	//XGpio_DiscreteWrite(&Gpio, GPIO_CHANNEL, GPIO_pin_0);
	XGpioPs_WritePin(&Gpiops, GPIOps_pin_MIO9, 0x1);
}


int Config_CPLD(u16 command2, u16 command1, u16 command0, u16 value)
{
	u16 send_buffer_size = 0;				// Init at 0. updated later.
	u8 SendBuffer[2];						// Max size of buffer
	u8 *SendBufferPtr;						// Send buffer pointer

	if(command2 == COMMAND2_FREE_1 || command2 == COMMAND2_FREE_2 || command2 == COMMAND2_FREE_3)		// These commands are not implemented
	{
		return XST_FAILURE;							// Nothing to do. Return error.
	}
	else											// All another commands require 2 byte of data
	{
		send_buffer_size = 2;
		SendBuffer[1] = value & 0xff;				// messsage 2
	}

	SendBuffer[0] = ((command2 & 0x07) << 5) | ((command1 & 0x07) << 2) | (command0 & 0x03);	//message 1

	/*
	 * Send the buffer using the IIC and ignore the number of bytes sent as the return value
	 */
	SendBufferPtr = SendBuffer;
	XIicPs_MasterSendPolled(&Iic, SendBufferPtr, send_buffer_size, IIC_CPLD_SLAVE_ADDR);

	/*
	 * Wait until bus is idle to start another transfer.
	 */
	while (XIicPs_BusIsBusy(&Iic))
	{
		//NOP
	}

	return XST_SUCCESS;
}



int Read_comms_stats(void)
{
	u32 Status;

	Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 16);

	for(u16 i=0; i<2; i++)
	{
		Status = XIicPs_MasterRecvPolled(&Iic, &RecvBuffer[i], 1, IIC_CPLD_SLAVE_ADDR);
		if (Status != XST_SUCCESS)
		{
			return XST_FAILURE;
		}

		while (XIicPs_BusIsBusy(&Iic))
		{
			/* NOP */
		}
	}

	data.stat.comms_ok = RecvBuffer[0];
	data.stat.comms_fail = RecvBuffer[1];

	return XST_SUCCESS;
}

int Read_trip_triggered(void)
{
	u32 Status;

	Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 0);

	for(u16 i=0; i<8; i++)
	{
		Status = XIicPs_MasterRecvPolled(&Iic, &RecvBuffer[i], 1, IIC_CPLD_SLAVE_ADDR);
		if (Status != XST_SUCCESS)
		{
			return XST_FAILURE;
		}

		while (XIicPs_BusIsBusy(&Iic))
		{
			/* NOP */
		}
	}

	data.trip.triggered.slow_adc1 =  (RecvBuffer[2]<<16) | (RecvBuffer[1]<<8) | RecvBuffer[0];
	data.trip.triggered.slow_adc2 =  (RecvBuffer[5]<<16) | (RecvBuffer[4]<<8) | RecvBuffer[3];
	data.trip.triggered.fast_adc =  (RecvBuffer[7]<<8) | RecvBuffer[6];
	data.trip.triggered.fast_adc_FPGA = TRIP_DETECTOR_CURRENTS_mReadReg(XPAR_TRIP_DETECTOR_CURRENTS_0_S_AXI_LITE_BASEADDR,TRIP_DETECTOR_CURRENTS_TRIGGERED_OFFSET);
	data.trip.triggered.slow_adcP_FPGA = TRIP_DETECTOR_CURRENTS_mReadReg(XPAR_TRIP_DETECTOR_VOLTAGES_0_S_AXI_BASEADDR,TRIP_DETECTOR_VOLTAGES_TRIGGERED_P_OFFSET);
	data.trip.triggered.slow_adcN_FPGA = TRIP_DETECTOR_CURRENTS_mReadReg(XPAR_TRIP_DETECTOR_VOLTAGES_0_S_AXI_BASEADDR,TRIP_DETECTOR_VOLTAGES_TRIGGERED_N_OFFSET);


	return XST_SUCCESS;
}

int Read_trip_instantaneous(void)
{
	u32 Status;

	Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 8);

	for(u16 i=0; i<8; i++)
	{
		Status = XIicPs_MasterRecvPolled(&Iic, &RecvBuffer[i], 1, IIC_CPLD_SLAVE_ADDR);
		if (Status != XST_SUCCESS)
		{
			return XST_FAILURE;
		}

		while (XIicPs_BusIsBusy(&Iic))
		{
			/* NOP */
		}
	}

	data.trip.instantaneous.slow_adc1 =  (RecvBuffer[2]<<16) | (RecvBuffer[1]<<8) | RecvBuffer[0];
	data.trip.instantaneous.slow_adc2 =  (RecvBuffer[5]<<16) | (RecvBuffer[4]<<8) | RecvBuffer[3];
	data.trip.instantaneous.fast_adc =  (RecvBuffer[7]<<8) | RecvBuffer[6];
	data.trip.instantaneous.fast_adc_FPGA = TRIP_DETECTOR_CURRENTS_mReadReg(XPAR_TRIP_DETECTOR_CURRENTS_0_S_AXI_LITE_BASEADDR,TRIP_DETECTOR_CURRENTS_INSTANTANEOUS_OFFSET);
	data.trip.instantaneous.slow_adcP_FPGA = TRIP_DETECTOR_CURRENTS_mReadReg(XPAR_TRIP_DETECTOR_VOLTAGES_0_S_AXI_BASEADDR,TRIP_DETECTOR_VOLTAGES_INSTANTANEOUS_P_OFFSET);
	data.trip.instantaneous.slow_adcN_FPGA = TRIP_DETECTOR_CURRENTS_mReadReg(XPAR_TRIP_DETECTOR_VOLTAGES_0_S_AXI_BASEADDR,TRIP_DETECTOR_VOLTAGES_INSTANTANEOUS_N_OFFSET);

	return XST_SUCCESS;
}


int Trimmer_values_init(void)
{
	// Change the values between 00-FF (00: 1,6V  / FF: 3,3V on Trimmer center pin)

	// Slow ADC Board 1
	data.trimmer.slow1.input1.ch1 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 1 -> Ch 1
	data.trimmer.slow1.input1.ch2 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 1 -> Ch 2
	data.trimmer.slow1.input1.ch3 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 1 -> Ch 3
	data.trimmer.slow1.input1.ch4 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 1 -> Ch 4

	data.trimmer.slow1.input2.ch1 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 2 -> Ch 1
	data.trimmer.slow1.input2.ch2 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 2 -> Ch 2
	data.trimmer.slow1.input2.ch3 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 2 -> Ch 3
	data.trimmer.slow1.input2.ch4 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 2 -> Ch 4

	data.trimmer.slow1.input3.ch1 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 3 -> Ch 1
	data.trimmer.slow1.input3.ch2 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 3 -> Ch 2
	data.trimmer.slow1.input3.ch3 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 3 -> Ch 3
	data.trimmer.slow1.input3.ch4 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 3 -> Ch 4

	data.trimmer.slow1.input4.ch1 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 4 -> Ch 1
	data.trimmer.slow1.input4.ch2 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 4 -> Ch 2
	data.trimmer.slow1.input4.ch3 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 4 -> Ch 3
	data.trimmer.slow1.input4.ch4 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 4 -> Ch 4

	data.trimmer.slow1.input5.ch1 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 5 -> Ch 1
	data.trimmer.slow1.input5.ch2 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 5 -> Ch 2
	data.trimmer.slow1.input5.ch3 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 5 -> Ch 3
	data.trimmer.slow1.input5.ch4 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 5 -> Ch 4

	data.trimmer.slow1.input6.ch1 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 6 -> Ch 1
	data.trimmer.slow1.input6.ch2 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 6 -> Ch 2
	data.trimmer.slow1.input6.ch3 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 6 -> Ch 3
	data.trimmer.slow1.input6.ch4 = trip_slow_Vcxx;	// Slow ADC 1 -> Input 6 -> Ch 4


	// Slow ADC Board 2
	data.trimmer.slow2.input1.ch1 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 1 -> Ch 1
	data.trimmer.slow2.input1.ch2 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 1 -> Ch 2
	data.trimmer.slow2.input1.ch3 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 1 -> Ch 3
	data.trimmer.slow2.input1.ch4 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 1 -> Ch 4

	data.trimmer.slow2.input2.ch1 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 2 -> Ch 1
	data.trimmer.slow2.input2.ch2 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 2 -> Ch 2
	data.trimmer.slow2.input2.ch3 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 2 -> Ch 3
	data.trimmer.slow2.input2.ch4 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 2 -> Ch 4

	data.trimmer.slow2.input3.ch1 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 3 -> Ch 1
	data.trimmer.slow2.input3.ch2 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 3 -> Ch 2
	data.trimmer.slow2.input3.ch3 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 3 -> Ch 3
	data.trimmer.slow2.input3.ch4 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 3 -> Ch 4

	data.trimmer.slow2.input4.ch1 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 4 -> Ch 1
	data.trimmer.slow2.input4.ch2 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 4 -> Ch 2
	data.trimmer.slow2.input4.ch3 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 4 -> Ch 3
	data.trimmer.slow2.input4.ch4 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 4 -> Ch 4

	data.trimmer.slow2.input5.ch1 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 5 -> Ch 1
	data.trimmer.slow2.input5.ch2 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 5 -> Ch 2
	data.trimmer.slow2.input5.ch3 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 5 -> Ch 3
	data.trimmer.slow2.input5.ch4 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 5 -> Ch 4

	data.trimmer.slow2.input6.ch1 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 6 -> Ch 1
	data.trimmer.slow2.input6.ch2 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 6 -> Ch 2
	data.trimmer.slow2.input6.ch3 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 6 -> Ch 3
	data.trimmer.slow2.input6.ch4 = trip_slow_Vcxx;	// Slow ADC 2 -> Input 6 -> Ch 4


	// Fast ADC
	data.trimmer.fast.input1.ch1 = trip_fast_curr;	// Fast ADC -> Input 1 -> Ch 1
	data.trimmer.fast.input1.ch2 = trip_fast_curr;	// Fast ADC -> Input 1 -> Ch 2
	data.trimmer.fast.input1.ch3 = trip_fast_curr;	// Fast ADC -> Input 1 -> Ch 3
	data.trimmer.fast.input1.ch4 = trip_fast_curr;	// Fast ADC -> Input 1 -> Ch 4

	data.trimmer.fast.input2.ch1 = 0xFF;			// Fast ADC -> Input 2 -> Ch 1
	data.trimmer.fast.input2.ch2 = 0xFF;	// Fast ADC -> Input 2 -> Ch 2
	data.trimmer.fast.input2.ch3 = 0xFF;			// Fast ADC -> Input 2 -> Ch 3
	data.trimmer.fast.input2.ch4 = trip_fast_Vo;			// Fast ADC -> Input 2 -> Ch 4

	data.trimmer.fast.input3.ch1 = trip_fast_curr_Io;	// Fast ADC -> Input 3 -> Ch 1
	data.trimmer.fast.input3.ch2 = trip_fast_curr;	// Fast ADC -> Input 3 -> Ch 2
	data.trimmer.fast.input3.ch3 = trip_fast_curr;	// Fast ADC -> Input 3 -> Ch 3
	data.trimmer.fast.input3.ch4 = trip_fast_curr;	// Fast ADC -> Input 3 -> Ch 4

	data.trimmer.fast.input4.ch1 = trip_fast_curr;	// Fast ADC -> Input 4 -> Ch 1
	data.trimmer.fast.input4.ch2 = trip_fast_curr;	// Fast ADC -> Input 4 -> Ch 2
	data.trimmer.fast.input4.ch3 = trip_fast_curr;	// Fast ADC -> Input 4 -> Ch 3
	data.trimmer.fast.input4.ch4 = trip_fast_curr;	// Fast ADC -> Input 4 -> Ch 4

	return XST_SUCCESS;
}

int CPLD_init(void)
{
	Reset_CPLD();
	int delay_I2C_Trimmer = 2000000;


	Read_comms_stats();

//	// CONFIG_TRIMMER_B1 -> Slow ADC board 1
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_C_ADDR, data.trimmer.slow1.input1.ch1);	// I2C Bus B1 -> Address 0_C || Slow ADC 1 -> Input 1 -> Ch 1
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_A_ADDR, data.trimmer.slow1.input1.ch2);	// I2C Bus B1 -> Address 0_A || Slow ADC 1 -> Input 1 -> Ch 2
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_C_ADDR, data.trimmer.slow1.input1.ch3);	// I2C Bus B1 -> Address 1_C || Slow ADC 1 -> Input 1 -> Ch 3
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_A_ADDR, data.trimmer.slow1.input1.ch4);	// I2C Bus B1 -> Address 1_A || Slow ADC 1 -> Input 1 -> Ch 4
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//		Read_comms_stats();
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_B_ADDR, data.trimmer.slow1.input2.ch1);	// I2C Bus B1 -> Address 0_B || Slow ADC 1 -> Input 2 -> Ch 1
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_D_ADDR, data.trimmer.slow1.input2.ch2);	// I2C Bus B1 -> Address 0_D || Slow ADC 1 -> Input 2 -> Ch 2
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_B_ADDR, data.trimmer.slow1.input2.ch3);	// I2C Bus B1 -> Address 1_B || Slow ADC 1 -> Input 2 -> Ch 3
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_D_ADDR, data.trimmer.slow1.input2.ch4);	// I2C Bus B1 -> Address 1_D || Slow ADC 1 -> Input 2 -> Ch 4
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//		Read_comms_stats();
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_C_ADDR, data.trimmer.slow1.input3.ch1);	// I2C Bus B1 -> Address 2_C || Slow ADC 1 -> Input 3 -> Ch 1
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_A_ADDR, data.trimmer.slow1.input3.ch2);	// I2C Bus B1 -> Address 2_A || Slow ADC 1 -> Input 3 -> Ch 2
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_C_ADDR, data.trimmer.slow1.input3.ch3);	// I2C Bus B1 -> Address 3_C || Slow ADC 1 -> Input 3 -> Ch 3
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_A_ADDR, data.trimmer.slow1.input3.ch4);	// I2C Bus B1 -> Address 3_A || Slow ADC 1 -> Input 3 -> Ch 4
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//		Read_comms_stats();
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_B_ADDR, data.trimmer.slow1.input4.ch1);	// I2C Bus B1 -> Address 2_B || Slow ADC 1 -> Input 4 -> Ch 1
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_D_ADDR, data.trimmer.slow1.input4.ch2);	// I2C Bus B1 -> Address 2_D || Slow ADC 1 -> Input 4 -> Ch 2
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_B_ADDR, data.trimmer.slow1.input4.ch3);	// I2C Bus B1 -> Address 3_B || Slow ADC 1 -> Input 4 -> Ch 3
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_D_ADDR, data.trimmer.slow1.input4.ch4);	// I2C Bus B1 -> Address 3_D || Slow ADC 1 -> Input 4 -> Ch 4
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//		Read_comms_stats();
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_4, COMMAND0_TRIMMER_C_ADDR, data.trimmer.slow1.input5.ch1);	// I2C Bus B1 -> Address 4_C || Slow ADC 1 -> Input 5 -> Ch 1
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_4, COMMAND0_TRIMMER_A_ADDR, data.trimmer.slow1.input5.ch2);	// I2C Bus B1 -> Address 4_A || Slow ADC 1 -> Input 5 -> Ch 2
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_5, COMMAND0_TRIMMER_C_ADDR, data.trimmer.slow1.input5.ch3);	// I2C Bus B1 -> Address 5_C || Slow ADC 1 -> Input 5 -> Ch 3
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_5, COMMAND0_TRIMMER_A_ADDR, data.trimmer.slow1.input5.ch4);	// I2C Bus B1 -> Address 5_A || Slow ADC 1 -> Input 5 -> Ch 4
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//		Read_comms_stats();
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_4, COMMAND0_TRIMMER_B_ADDR, data.trimmer.slow1.input6.ch1);	// I2C Bus B1 -> Address 4_B || Slow ADC 1 -> Input 6 -> Ch 1
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_4, COMMAND0_TRIMMER_D_ADDR, data.trimmer.slow1.input6.ch2);	// I2C Bus B1 -> Address 4_D || Slow ADC 1 -> Input 6 -> Ch 2
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_5, COMMAND0_TRIMMER_B_ADDR, data.trimmer.slow1.input6.ch3);	// I2C Bus B1 -> Address 5_B || Slow ADC 1 -> Input 6 -> Ch 3
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_5, COMMAND0_TRIMMER_D_ADDR, data.trimmer.slow1.input6.ch4);	// I2C Bus B1 -> Address 5_D || Slow ADC 1 -> Input 6 -> Ch 4
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//
//	Read_comms_stats();
//
//	if(data.stat.comms_fail != 0)
//	{
//		printf("Slow ADC Board 1 is not connected");
//		return XST_FAILURE;
//	}

	 //Uncomment if ADC Slow board 2 is connected
	 //CONFIG_TRIMMER_B2 -> Slow ADC board 2
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_C_ADDR, data.trimmer.slow2.input1.ch1);	// I2C Bus B2 -> Address 0_C || Slow ADC 2 -> Input 1 -> Ch 1
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_A_ADDR, data.trimmer.slow2.input1.ch2);	// I2C Bus B2 -> Address 0_A || Slow ADC 2 -> Input 1 -> Ch 2
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_C_ADDR, data.trimmer.slow2.input1.ch3);	// I2C Bus B2 -> Address 1_C || Slow ADC 2 -> Input 1 -> Ch 3
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_A_ADDR, data.trimmer.slow2.input1.ch4);	// I2C Bus B2 -> Address 1_A || Slow ADC 2 -> Input 1 -> Ch 4
		for(int i=0;i<delay_I2C_Trimmer;i++);
		Read_comms_stats();
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_B_ADDR, data.trimmer.slow2.input2.ch1);	// I2C Bus B2 -> Address 0_B || Slow ADC 2 -> Input 2 -> Ch 1
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_D_ADDR, data.trimmer.slow2.input2.ch2);	// I2C Bus B2 -> Address 0_D || Slow ADC 2 -> Input 2 -> Ch 2
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_B_ADDR, data.trimmer.slow2.input2.ch3);	// I2C Bus B2 -> Address 1_B || Slow ADC 2 -> Input 2 -> Ch 3
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_D_ADDR, data.trimmer.slow2.input2.ch4);	// I2C Bus B2 -> Address 1_D || Slow ADC 2 -> Input 2 -> Ch 4
		for(int i=0;i<delay_I2C_Trimmer;i++);
		Read_comms_stats();
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_C_ADDR, data.trimmer.slow2.input3.ch1);	// I2C Bus B2 -> Address 2_C || Slow ADC 2 -> Input 3 -> Ch 1
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_A_ADDR, data.trimmer.slow2.input3.ch2);	// I2C Bus B2 -> Address 2_A || Slow ADC 2 -> Input 3 -> Ch 2
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_C_ADDR, data.trimmer.slow2.input3.ch3);	// I2C Bus B2 -> Address 3_C || Slow ADC 2 -> Input 3 -> Ch 3
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_A_ADDR, data.trimmer.slow2.input3.ch4);	// I2C Bus B2 -> Address 3_A || Slow ADC 2 -> Input 3 -> Ch 4
		for(int i=0;i<delay_I2C_Trimmer;i++);
		Read_comms_stats();
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_B_ADDR, data.trimmer.slow2.input4.ch1);	// I2C Bus B2 -> Address 2_B || Slow ADC 2 -> Input 4 -> Ch 1
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_D_ADDR, data.trimmer.slow2.input4.ch2);	// I2C Bus B2 -> Address 2_D || Slow ADC 2 -> Input 4 -> Ch 2
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_B_ADDR, data.trimmer.slow2.input4.ch3);	// I2C Bus B2 -> Address 3_B || Slow ADC 2 -> Input 4 -> Ch 3
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_D_ADDR, data.trimmer.slow2.input4.ch4);	// I2C Bus B2 -> Address 3_D || Slow ADC 2 -> Input 4 -> Ch 4
		for(int i=0;i<delay_I2C_Trimmer;i++);
		Read_comms_stats();
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_4, COMMAND0_TRIMMER_C_ADDR, data.trimmer.slow2.input5.ch1);	// I2C Bus B2 -> Address 4_C || Slow ADC 2 -> Input 5 -> Ch 1
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_4, COMMAND0_TRIMMER_A_ADDR, data.trimmer.slow2.input5.ch2);	// I2C Bus B2 -> Address 4_A || Slow ADC 2 -> Input 5 -> Ch 2
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_5, COMMAND0_TRIMMER_C_ADDR, data.trimmer.slow2.input5.ch3);	// I2C Bus B2 -> Address 5_C || Slow ADC 2 -> Input 5 -> Ch 3
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_5, COMMAND0_TRIMMER_A_ADDR, data.trimmer.slow2.input5.ch4);	// I2C Bus B2 -> Address 5_A || Slow ADC 2 -> Input 5 -> Ch 4
		for(int i=0;i<delay_I2C_Trimmer;i++);
		Read_comms_stats();
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_4, COMMAND0_TRIMMER_B_ADDR, data.trimmer.slow2.input6.ch1);	// I2C Bus B2 -> Address 4_B || Slow ADC 2 -> Input 6 -> Ch 1
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_4, COMMAND0_TRIMMER_D_ADDR, data.trimmer.slow2.input6.ch2);	// I2C Bus B2 -> Address 4_D || Slow ADC 2 -> Input 6 -> Ch 2
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_5, COMMAND0_TRIMMER_B_ADDR, data.trimmer.slow2.input6.ch3);	// I2C Bus B2 -> Address 5_B || Slow ADC 2 -> Input 6 -> Ch 3
		for(int i=0;i<delay_I2C_Trimmer;i++);
	Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_5, COMMAND0_TRIMMER_D_ADDR, data.trimmer.slow2.input6.ch4);	// I2C Bus B2 -> Address 5_D || Slow ADC 2 -> Input 6 -> Ch 4
		for(int i=0;i<delay_I2C_Trimmer;i++);

	Read_comms_stats();

	if(data.stat.comms_fail != 0)
	{
		printf("Slow ADC Board 2 is not connected");
		return XST_FAILURE;
	}

//	// CONFIG_Fast ADC -> TRIMMER_B3
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_A_ADDR, data.trimmer.fast.input1.ch1);	// I2C Bus B3 -> Address 0_A || Fast ADC -> Input 1 -> Ch 1
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_D_ADDR, data.trimmer.fast.input1.ch2);	// I2C Bus B3 -> Address 0_D || Fast ADC -> Input 1 -> Ch 2
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_C_ADDR, data.trimmer.fast.input1.ch3);	// I2C Bus B3 -> Address 0_C || Fast ADC -> Input 1 -> Ch 3
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_B_ADDR, data.trimmer.fast.input1.ch4);	// I2C Bus B3 -> Address 0_B || Fast ADC -> Input 1 -> Ch 4
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_A_ADDR, data.trimmer.fast.input2.ch1);	// I2C Bus B3 -> Address 1_A || Fast ADC -> Input 2 -> Ch 1
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_D_ADDR, data.trimmer.fast.input2.ch2);	// I2C Bus B3 -> Address 1_D || Fast ADC -> Input 2 -> Ch 2
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_C_ADDR, data.trimmer.fast.input2.ch3);	// I2C Bus B3 -> Address 1_C || Fast ADC -> Input 2 -> Ch 3
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_B_ADDR, data.trimmer.fast.input2.ch4);	// I2C Bus B3 -> Address 1_B || Fast ADC -> Input 2 -> Ch 4
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_A_ADDR, data.trimmer.fast.input3.ch1);	// I2C Bus B3 -> Address 2_A || Fast ADC -> Input 3 -> Ch 1
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_D_ADDR, data.trimmer.fast.input3.ch2);	// I2C Bus B3 -> Address 2_D || Fast ADC -> Input 3 -> Ch 2
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_C_ADDR, data.trimmer.fast.input3.ch3);	// I2C Bus B3 -> Address 2_C || Fast ADC -> Input 3 -> Ch 3
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_B_ADDR, data.trimmer.fast.input3.ch4);	// I2C Bus B3 -> Address 2_B || Fast ADC -> Input 3 -> Ch 4
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_A_ADDR, data.trimmer.fast.input4.ch1);	// I2C Bus B3 -> Address 3_A || Fast ADC -> Input 4 -> Ch 1
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_D_ADDR, data.trimmer.fast.input4.ch2);	// I2C Bus B3 -> Address 3_D || Fast ADC -> Input 4 -> Ch 2
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_C_ADDR, data.trimmer.fast.input4.ch3);	// I2C Bus B3 -> Address 3_C || Fast ADC -> Input 4 -> Ch 3
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//	Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_B_ADDR, data.trimmer.fast.input4.ch4);	// I2C Bus B3 -> Address 3_B || Fast ADC -> Input 4 -> Ch 4
//		for(int i=0;i<delay_I2C_Trimmer;i++);
//
//	Read_comms_stats();
//
//	if(data.stat.comms_fail != 0)
//	{
//		printf("Fast ADC Board is not connected");
//		return XST_FAILURE;
//	}

	Read_comms_stats();

	if(data.stat.comms_fail == 0)
	{
		return XST_SUCCESS;
	}
	else
	{
		return XST_FAILURE;
	}

	return XST_SUCCESS;
}


/****************************************************************************/
/**
*
* This function runs a test on the XADC/ADC device using the
* driver APIs.
* This function does the following tasks:
*	- Initiate the XADC device driver instance
*	- Run self-test on the device
*	- Setup the sequence registers to continuously monitor on-chip
*	temperature and, VCCPINT, VCCPAUX and VCCPDRO Voltages
*	- Setup configuration registers to start the sequence
*	- Read the latest on-chip temperature and, VCCPINT, VCCPAUX and VCCPDRO
*	  Voltages
*
* @param	XAdcDeviceId is the XPAR_<SYSMON_ADC_instance>_DEVICE_ID value
*		from xparameters.h.
*
* @return
*		- XST_SUCCESS if the example has completed successfully.
*		- XST_FAILURE if the example has failed.
*
* @note   	None
*
****************************************************************************/
int Init_XAdc(u16 XAdcDeviceId)
{
	int Status;
	XAdcPs_Config *ConfigPtr;
	u32 TempRawData;
	float TempData;
	//float MaxData;
	//float MinData;

	/*
	 * Initialize the XAdc driver.
	 */
	ConfigPtr = XAdcPs_LookupConfig(XAdcDeviceId);
	if (ConfigPtr == NULL) {
		return XST_FAILURE;
	}
	XAdcPs_CfgInitialize(XAdcInstPtr, ConfigPtr,
				ConfigPtr->BaseAddress);

	/*
	 * Self Test the XADC/ADC device
	 */
	Status = XAdcPs_SelfTest(XAdcInstPtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Disable the Channel Sequencer before configuring the Sequence
	 * registers.
	 */
	XAdcPs_SetSequencerMode(XAdcInstPtr, XADCPS_SEQ_MODE_SAFE);
	/*
	 * Read the on-chip Temperature Data (Current/Maximum/Minimum)
	 * from the ADC data registers.
	 */
	TempRawData = XAdcPs_GetAdcData(XAdcInstPtr, XADCPS_CH_TEMP);
	TempData = XAdcPs_RawToTemperature(TempRawData);
	printf("\r\nSoC Temperature is %0d.%02d Centigrades.\r\n",
				(int)(TempData), XAdcFractionToInt(TempData));

//	TempRawData = XAdcPs_GetMinMaxMeasurement(XAdcInstPtr, XADCPS_MAX_TEMP);
//	MaxData = XAdcPs_RawToTemperature(TempRawData);
//	printf("The Maximum Temperature is %0d.%03d Centigrades. \r\n",
//				(int)(MaxData), XAdcFractionToInt(MaxData));
//
//	TempRawData = XAdcPs_GetMinMaxMeasurement(XAdcInstPtr, XADCPS_MIN_TEMP);
//	MinData = XAdcPs_RawToTemperature(TempRawData & 0xFFF0);
//	printf("The Minimum Temperature is %0d.%03d Centigrades. \r\n",
//				(int)(MinData), XAdcFractionToInt(MinData));


	return XST_SUCCESS;
}

/*****************************************************************************/
/**
* This function does a minimal test on the timer counter device and driver as a
* design example.  The purpose of this function is to illustrate how to use the
* XTmrCtr component.  It initializes a timer counter and then sets it up in
* compare mode with auto reload such that a periodic interrupt is generated.
*
* This function uses interrupt driven mode of the timer counter.
*
* @param	IntcInstancePtr is a pointer to the Interrupt Controller
*		driver Instance
* @param	TmrCtrInstancePtr is a pointer to the XTmrCtr driver Instance
* @param	DeviceId is the XPAR_<TmrCtr_instance>_DEVICE_ID value from
*		xparameters.h
* @param	IntrId is XPAR_<INTC_instance>_<TmrCtr_instance>_INTERRUPT_INTR
*		value from xparameters.h
* @param	TmrCtrNumber is the number of the timer to which this
*		handler is associated with.
*
* @return	XST_SUCCESS if the Test is successful, otherwise XST_FAILURE
*
* @note		This function contains an infinite loop such that if interrupts
*		are not working it may never return.
*
*****************************************************************************/
int Control_interrupt_init(void)
{
int Status;

INTC* IntcInstancePtr = &InterruptController;
XTmrCtr* TmrCtrInstancePtr = &TimerCounterInst;
u16 DeviceId = TMRCTR_DEVICE_ID;
u16 IntrId = TMRCTR_INTERRUPT_ID;
u8 TmrCtrNumber = TIMER_CNTR_0;

u32 RESET_VALUE = (AXI_CLOCK_FREQUENCY / frequency_interrupt) - 2; 	//PWM_PERIOD = (TLR0 + 2) * AXI_CLOCK_PERIOD

/*
 * Initialize the timer counter so that it's ready to use,
 * specify the device ID that is generated in xparameters.h
 */
Status = XTmrCtr_Initialize(TmrCtrInstancePtr, DeviceId);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

/*
 * Perform a self-test to ensure that the hardware was built
 * correctly, use the 1st timer in the device (0)
 */
Status = XTmrCtr_SelfTest(TmrCtrInstancePtr, TmrCtrNumber);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

/*
 * Connect the timer counter to the interrupt subsystem such that
 * interrupts can occur.  This function is application specific.
 */
Status = TmrCtrSetupIntrSystem(IntcInstancePtr,
				TmrCtrInstancePtr,
				DeviceId,
				IntrId,
				TmrCtrNumber);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

/*
 * Setup the handler for the timer counter that will be called from the
 * interrupt context when the timer expires, specify a pointer to the
 * timer counter driver instance as the callback reference so the handler
 * is able to access the instance data
 */
XTmrCtr_SetHandler(TmrCtrInstancePtr, TimerCounterHandler,
				   TmrCtrInstancePtr);

/*
 * Enable the interrupt of the timer counter so interrupts will occur
 * and use auto reload mode such that the timer counter will reload
 * itself automatically and continue repeatedly, without this option
 * it would expire once only. Also the timer count down to 0
 */
XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber,
			XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION  | XTC_DOWN_COUNT_OPTION);

/*
 * Set a reset value for the timer counter such that it will expire
 * eariler than letting it roll over from 0, the reset value is loaded
 * into the timer counter when it is started
 */
XTmrCtr_SetResetValue(TmrCtrInstancePtr, TmrCtrNumber, RESET_VALUE);

/*
 * Start the timer counter such that it's incrementing by default,
 * then wait for it to timeout a number of times
 */
XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);

return XST_SUCCESS;
}


/*****************************************************************************/
/**
* This function setups the interrupt system such that interrupts can occur
* for the timer counter. This function is application specific since the actual
* system may or may not have an interrupt controller.  The timer counter could
* be directly connected to a processor without an interrupt controller.  The
* user should modify this function to fit the application.
*
* @param	IntcInstancePtr is a pointer to the Interrupt Controller
*		driver Instance.
* @param	TmrCtrInstancePtr is a pointer to the XTmrCtr driver Instance.
* @param	DeviceId is the XPAR_<TmrCtr_instance>_DEVICE_ID value from
*		xparameters.h.
* @param	IntrId is XPAR_<INTC_instance>_<TmrCtr_instance>_VEC_ID
*		value from xparameters.h.
* @param	TmrCtrNumber is the number of the timer to which this
*		handler is associated with.
*
* @return	XST_SUCCESS if the Test is successful, otherwise XST_FAILURE.
*
* @note		This function contains an infinite loop such that if interrupts
*		are not working it may never return.
*
******************************************************************************/
int TmrCtrSetupIntrSystem(INTC* IntcInstancePtr,
			 XTmrCtr* TmrCtrInstancePtr,
			 u16 DeviceId,
			 u16 IntrId,
			 u8 TmrCtrNumber)
{
 int Status;

#ifdef XPAR_INTC_0_DEVICE_ID
#ifndef TESTAPP_GEN
/*
 * Initialize the interrupt controller driver so that
 * it's ready to use, specify the device ID that is generated in
 * xparameters.h
 */
Status = XIntc_Initialize(IntcInstancePtr, INTC_DEVICE_ID);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}
#endif
/*
 * Connect a device driver handler that will be called when an interrupt
 * for the device occurs, the device driver handler performs the specific
 * interrupt processing for the device
 */
Status = XIntc_Connect(IntcInstancePtr, IntrId,
			(XInterruptHandler)XTmrCtr_InterruptHandler,
			(void *)TmrCtrInstancePtr);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

#ifndef TESTAPP_GEN
/*
 * Start the interrupt controller such that interrupts are enabled for
 * all devices that cause interrupts, specific real mode so that
 * the timer counter can cause interrupts thru the interrupt controller.
 */
Status = XIntc_Start(IntcInstancePtr, XIN_REAL_MODE);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}
#endif

/*
 * Enable the interrupt for the timer counter
 */
XIntc_Enable(IntcInstancePtr, IntrId);

#else

#ifndef TESTAPP_GEN
XScuGic_Config *IntcConfig;

/*
 * Initialize the interrupt controller driver so that it is ready to
 * use.
 */
IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
if (NULL == IntcConfig) {
	return XST_FAILURE;
}

Status = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
				IntcConfig->CpuBaseAddress);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}
#endif /* TESTAPP_GEN */

XScuGic_SetPriorityTriggerType(IntcInstancePtr, IntrId,
				0xA0, 0x3);

/*
 * Connect the interrupt handler that will be called when an
 * interrupt occurs for the device.
 */
Status = XScuGic_Connect(IntcInstancePtr, IntrId,
			 (Xil_ExceptionHandler)XTmrCtr_InterruptHandler,
			 TmrCtrInstancePtr);
if (Status != XST_SUCCESS) {
	return Status;
}

/*
 * Enable the interrupt for the Timer device.
 */
XScuGic_Enable(IntcInstancePtr, IntrId);
#endif /* XPAR_INTC_0_DEVICE_ID */


#ifndef TESTAPP_GEN
/*
 * Initialize the exception table.
 */
Xil_ExceptionInit();

/*
 * Register the interrupt controller handler with the exception table.
 */
Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
				(Xil_ExceptionHandler)
				INTC_HANDLER,
				IntcInstancePtr);

/*
 * Enable non-critical exceptions.
 */
Xil_ExceptionEnable();

#endif
return XST_SUCCESS;
}



/****************************************************************************/
/**
*
* This function converts the fraction part of the given floating point number
* (after the decimal point)to an integer.
*
* @param	FloatNum is the floating point number.
*
* @return	Integer number to a precision of 3 digits.
*
* @note
* This function is used in the printing of floating point data to a STDIO device
* using the xil_printf function. The xil_printf is a very small foot-print
* printf function and does not support the printing of floating point numbers.
*
*****************************************************************************/
int XAdcFractionToInt(float FloatNum)
{
float Temp;

Temp = FloatNum;
if (FloatNum < 0) {
	Temp = -(FloatNum);
}

return( ((int)((Temp -(float)((int)Temp)) * (1000.0f))));
}


int Interrupt_SetLoadReg(u32 RegisterValue)
{
	XTmrCtr* TmrCtrInstancePtr = &TimerCounterInst;
	u8 TmrCtrNumber = TIMER_CNTR_0;

	/*
	 * Set a reset value for the timer counter such that it will expire
	 * eariler than letting it roll over from 0, the reset value is loaded
	 * into the timer counter when it is started
	 */
	XTmrCtr_SetResetValue(TmrCtrInstancePtr, TmrCtrNumber, RegisterValue);

	/*
	 * Start the timer counter such that it's incrementing by default,
	 * then wait for it to timeout a number of times
	 */
	XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);

	return XST_SUCCESS;
}


int Gpio_PL_init(void)
{
	int Status;

	/* Initialize the GPIO driver */
	Status = XGpio_Initialize(&Gpio_reset, GPIO_RESET_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/* Initialize the GPIO driver */
	Status = XGpio_Initialize(&Gpio_trip, GPIO_TRIP_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/* Initialize the GPIO driver */
	Status = XGpio_Initialize(&Gpio_constant, GPIO_CONSTANT_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	Status = XGpio_Initialize(&Gpio_preload, GPIO_PRELOAD_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	Status = XGpio_Initialize(&Gpio_button, GPIO_BUTTON_TEST_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/* Initialize the GPIO driver */
	Status = XGpio_Initialize(&Gpio_band, XPAR_AXI_GPIO_BAND_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

/* Set the direction for all signals as inputs */
//XGpio_SetDataDirection(&Gpio, GPIO_CHANNEL, 0xFFFFFFFF);
	XGpio_SetDataDirection(&Gpio_button, GPIO_CHANNEL, 0xFFFFFFFF);
return XST_SUCCESS;
}

int Gpio_ps_init(void)
{
	int Status;
	XGpioPs_Config *ConfigPtr;

	/* Initialize the GPIO driver. */
	ConfigPtr = XGpioPs_LookupConfig(GPIOps_DEVICE_ID);
	Status = XGpioPs_CfgInitialize(&Gpiops, ConfigPtr,
					ConfigPtr->BaseAddr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Set the direction for the pin to be output and
	 * Enable the Output enable for MIO Pins.
	 */
	XGpioPs_SetDirectionPin(&Gpiops, GPIOps_pin_MIO0, GPIOps_dir_output);
	XGpioPs_SetDirectionPin(&Gpiops, GPIOps_pin_MIO7, GPIOps_dir_output);
	XGpioPs_SetDirectionPin(&Gpiops, GPIOps_pin_MIO9, GPIOps_dir_output);

	XGpioPs_SetOutputEnablePin(&Gpiops, GPIOps_pin_MIO0, 1);
	XGpioPs_SetOutputEnablePin(&Gpiops, GPIOps_pin_MIO7, 1);
	XGpioPs_SetOutputEnablePin(&Gpiops, GPIOps_pin_MIO9, 1);

	XGpioPs_WritePin(&Gpiops, GPIOps_pin_MIO7, 0x1);

	return XST_SUCCESS;
}





int I2C_init(void)
{
	int Status;
	XIicPs_Config *Config;
	//	int Index;
	u16 DeviceId = IIC_DEVICE_ID;

	/*
	 * Initialize the IIC driver so that it's ready to use
	 * Look up the configuration in the config table,
	 * then initialize it.
	 */
	Config = XIicPs_LookupConfig(DeviceId);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XIicPs_CfgInitialize(&Iic, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test to ensure that the hardware was built correctly.
	 */
	Status = XIicPs_SelfTest(&Iic);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Set the IIC serial clock rate.
	 */
	XIicPs_SetSClk(&Iic, IIC_SCLK_RATE);

	return XST_SUCCESS;
}


int slow_adc_init(void)
{
//	printf("slow adc init...\n");
	u32 ADC_Status = 0;
	u8 ADC_init_done = 0;
	u8 ADC_meas_done = 0;
	u8 ADC_error = 0;
	u16 data_echoed_bipolar[6];
	u16 data_echoed_unipolar[6];
	u16 error_counter = 0;
	u32 temp = 0;

	XGpio_DiscreteClear(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_SLOW_ADC);			// Reset Slow ADC driver
	ADC_MAX11331_AXI_mWriteReg(XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR, ADC_MAX11331_ADC_SELECTOR_OFFSET, 0x38);	//Select adc chips to be used (verified)
	XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_SLOW_ADC);				// unReset Slow ADC driver. Enable init

	for(int i=0; i<1000000; i++);

	do
	{
		ADC_Status = ADC_MAX11331_AXI_mReadReg(XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR, ADC_MAX11331_STATUS_OFFSET);
		ADC_init_done = (ADC_Status & 0x00000001);
		ADC_meas_done = (ADC_Status & 0x00000100) >> 8;
		ADC_error = (ADC_Status & 0x00010000) >> 16;

		temp = ADC_MAX11331_AXI_mReadReg(XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR, ADC_MAX11331_ECHOED_BIPOLAR_12_OFFSET);
			data_echoed_bipolar[0] = temp & 0x0000FFFF;
			data_echoed_bipolar[1] = (temp & 0xFFFF0000) >> 16;
		temp = ADC_MAX11331_AXI_mReadReg(XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR, ADC_MAX11331_ECHOED_BIPOLAR_34_OFFSET);
			data_echoed_bipolar[2] = temp & 0x0000FFFF;
			data_echoed_bipolar[3] = (temp & 0xFFFF0000) >> 16;
		temp = ADC_MAX11331_AXI_mReadReg(XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR, ADC_MAX11331_ECHOED_BIPOLAR_56_OFFSET);
			data_echoed_bipolar[4] = temp & 0x0000FFFF;
			data_echoed_bipolar[5] = (temp & 0xFFFF0000) >> 16;

		temp = ADC_MAX11331_AXI_mReadReg(XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR, ADC_MAX11331_ECHOED_UNIPOLAR_12_OFFSET);
			data_echoed_unipolar[0] = temp & 0x0000FFFF;
			data_echoed_unipolar[1] = (temp & 0xFFFF0000) >> 16;
		temp = ADC_MAX11331_AXI_mReadReg(XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR, ADC_MAX11331_ECHOED_UNIPOLAR_34_OFFSET);
			data_echoed_unipolar[2] = temp & 0x0000FFFF;
			data_echoed_unipolar[3] = (temp & 0xFFFF0000) >> 16;
		temp = ADC_MAX11331_AXI_mReadReg(XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR, ADC_MAX11331_ECHOED_UNIPOLAR_56_OFFSET);
			data_echoed_unipolar[4] = temp & 0x0000FFFF;
			data_echoed_unipolar[5] = (temp & 0xFFFF0000) >> 16;

		error_counter = ADC_MAX11331_AXI_mReadReg(XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR, ADC_MAX11331_ERROR_COUNTER_OFFSET);
	}while(ADC_init_done == 0);	//init_done success)

//	for(int i=0; i<10; i++)
//	{
//		for(int j; j<100000; j++);
//	ADC_Status = ADC_MAX11331_AXI_mReadReg(XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR, ADC_MAX11331_STATUS_OFFSET);
//	ADC_init_done = (ADC_Status & 0x00000001);
//	ADC_meas_done = (ADC_Status & 0x00000100) >> 8;
//	ADC_error = (ADC_Status & 0x00010000) >> 16;
//	printf("\nintento %d",i);
//	printf("\tinit done: %d\n", ADC_init_done);
//	printf("\tmeas done: %d\n", ADC_meas_done);
//	printf("\terror: %d\n", ADC_error);
//	printf("\terror counter: %d\n", error_counter);
//	}

	printf("\tinit done: %d\n", ADC_init_done);
	printf("\tmeas done: %d\n", ADC_meas_done);
	printf("\terror: %d\n", ADC_error);
	printf("\terror counter: %d\n", error_counter);

	ADC_MAX11331_AXI_mWriteReg(XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR, ADC_MAX11331_ADC_SELECTOR_OFFSET, 0x080);	//Select force init
	//ADC_MAX11331_AXI_mWriteReg(XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR, ADC_MAX11331_ADC_SELECTOR_OFFSET, 0x000);	//UnSelect force init
	return XST_SUCCESS;
}

int fast_adc_init(void)
{
	XGpio_DiscreteClear(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_ADCLK);			// Reset adclk
	XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_FAST_ADC);	// Reset ADC IP Core
	Config_CPLD(COMMAND2_INIT_ADC, 0, COMMAND0_ADC_ALL_ADDR, 0);	// Init ADC internal registers via spi from CPLD
	//Config_CPLD(COMMAND2_INIT_ADC, 0x00000002, COMMAND0_ADC_ALL_ADDR, 0);	// Init ADC internal registers via spi from CPLD
	for(int i=0;i<10000000;i++);		// Wait until CPLD finish initialization
	XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_ADCLK);			// unReset adclk. Enable ADCLK -> start fast measures
	for(int i=0;i<1000000;i++);		// Wait until CPLD finish initialization
	XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_FAST_ADC);		// un-Reset ADC IP Core

	if(shift_adc==1)
	{
		XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, SHIFT_ADC);	// unReset debug memory IP Core
	}
	else
	{
		XGpio_DiscreteClear(&Gpio_reset, GPIO_CHANNEL1, SHIFT_ADC);	// unReset debug memory IP Core
	}

	return XST_SUCCESS;
}


int fast_adc_pattern(u16 adc_pattern)
{
	u16 command1 = 0;
	command1 = 0x0001 | (adc_pattern << 1);
	Config_CPLD(COMMAND2_INIT_ADC, command1, COMMAND0_ADC_ALL_ADDR, 0);	// Init ADC internal registers via spi from CPLD
	for(int i=0;i<10000000;i++);		// Wait until CPLD finish initialization
	return XST_SUCCESS;
}


int board_init(void)
{
	int Status;
	printf("\r\nRTS Usach v3: MMC Control\n");
	//printf("\r\nPAPER NLC\n");

	/* Initialize the FPGA GPIO driver */
	Status = Gpio_PL_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("Gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	XGpio_DiscreteWrite(&Gpio_reset, GPIO_CHANNEL1, 0x00000000);	// Reset All IP Cores (adclk reset use common logic)

	/* Initialize the processor GPIO driver */
	Status = Gpio_ps_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("Gpio_ps Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/* Initialize the XADC driver (for dice temperature) */
	Status = Init_XAdc(XADC_DEVICE_ID);
	if (Status != XST_SUCCESS)
	{
		xil_printf("xADC_PS Initializacion Failed\r\n");
		return XST_FAILURE;
	}

	/*Initialize the I2C driver */
	Status = I2C_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("I2C Initializacion Failed\r\n");
		return XST_FAILURE;
	}


	/* Initialize digital trimmer values */
	Status = Trimmer_values_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("Trimmer values Initializacion Failed\r\n");
		return XST_FAILURE;
	}

////	/* Initialize Optical Fibers (init sequence) */
//	Status = Optical_Fibers_init();
//	if (Status != XST_SUCCESS)
//	{
//		xil_printf("Optical Fibers Initializacion Failed\r\n");
//		return XST_FAILURE;
//	}

//	/* Initialize Fast ADC IP-Core */
//	Status = fast_adc_init();
//	if (Status != XST_SUCCESS)
//	{
//		xil_printf("Fast ADC Initializacion Failed\r\n");
//		return XST_FAILURE;
//	}

	/* Initialize Slow ADCs */
	Status = slow_adc_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("Slow ADC Initializacion Failed\r\n");
		return XST_FAILURE;
	}

	/* Initialize CPLD via I2C Communication */
	Status = CPLD_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("CPLD Initializacion Failed\r\n");
		return XST_FAILURE;
	}

	Reset_CPLD();	// To clear trip triggered

	//xil_printf("Board Initialization ends successfully\r\n");
	xil_printf("Initialization ends successfully\r\n");

	return XST_SUCCESS;
}


int slope_reference (var_slope_struct *variable_st)
{
     int status = 0;
     float actual = variable_st->actual;
     float final = variable_st->final;
     float delta = variable_st->delta;

     // delta incorporated to if for avoid continuous changes between
     if(actual < final - delta)
     {
         actual = actual + delta;
         status = 0;
     }
     else if (actual > final + delta)
     {
         actual = actual - delta;
         status = 0;
     }
     else
     {
         actual = final;
         status = 1;
     }

     //Update actual value
     variable_st->actual = actual;

     return status;
}

void init_notch_cluster_currents(void)
{
	float a0, a1, a2, b1, b2;
	float n1, n2, k1, k2;
	float h = 0.00002;			// sampling 50khz

	float fc = 50;
	float delta_f = 20;

	float w0 = 2.0 * pii * fc;
	float delta_w = 2.0 * pii * delta_f;

	k1 = -cos(w0*h);
	k2 = (1-tan((delta_w*h)/2))/(1+tan((delta_w*h)/2));
	n1=(1+k2)*0.5;
	n2=k1*(1+k2);

	b1 = n2;
	b2 = k2;
	a0 = n1;
	a1 = 2.0*n1*k1;
	a2 = n1;

	// store values for PR
	Xil_Out32(XPAR_NOTCH_CLUSTER_CURRENTS_0_BASEADDR+activator_Data_Notch_cluster_currents, 0x0);
	Xil_Out32(XPAR_NOTCH_CLUSTER_CURRENTS_0_BASEADDR+selector_Data_Notch_cluster_currents, 3);			//1: no filter. 2: from arm. 3: notch fpga
	Xil_Out32(XPAR_NOTCH_CLUSTER_CURRENTS_0_BASEADDR+a0_Data_Notch_cluster_currents, toFix(a0 ,23));
	Xil_Out32(XPAR_NOTCH_CLUSTER_CURRENTS_0_BASEADDR+a1_Data_Notch_cluster_currents, toFix(a1 ,23));
	Xil_Out32(XPAR_NOTCH_CLUSTER_CURRENTS_0_BASEADDR+a2_Data_Notch_cluster_currents, toFix(a2 ,23));
	Xil_Out32(XPAR_NOTCH_CLUSTER_CURRENTS_0_BASEADDR+b1_Data_Notch_cluster_currents, toFix(b1 ,23));
	Xil_Out32(XPAR_NOTCH_CLUSTER_CURRENTS_0_BASEADDR+b2_Data_Notch_cluster_currents, toFix(b2 ,23));
}

void init_PR_tpw_s(void)
{
	//--------------------------------------------------
	// Grid currents PRs
	float Kp_s, Ki_s;//, Tset_s;
	float Leq;

	Leq = Lacop+2.0*Lac;
	Kp_s = 2 * xi_s * Leq * wn_s;
	Ki_s = Leq * wn_s * wn_s;
	//Tset_s = 4 / (xi_s*wn_s);

	//printf("Kp_s: %.2f\t Ki_s: %.2f\tTset_s: %f\n",Kp_s,Ki_s,Tset_s);

	float a0, a1, a2, b1, b2;
	float h = T_proc;
	float w = w_grid;

	b1 = -2.0 * cos(h*w);
	b2 = 1.0;
	a0 = Kp_s + (Ki_s * sin(h*w) / (2.0*w));
	a1= -2.0 * Kp_s * cos(h*w);
	a2 = Kp_s - (Ki_s * sin(h*w) / (2.0*w));

	// store values for alpha PR
	PR_s[0].FF = 0.0;	//No feed-forward
	PR_s[0].h = Ts_arm;
	PR_s[0].w = w_grid;
	PR_s[0].kp = Kp_s;
	PR_s[0].ki = Ki_s;
	PR_s[0].in	 = 0.0;
	PR_s[0].in_1 = 0.0;
	PR_s[0].in_2 = 0.0;
	PR_s[0].out_1 = 0.0;
	PR_s[0].out_2 = 0.0;
	PR_s[0].a0 = a0;
	PR_s[0].a1 = a1;
	PR_s[0].a2 = a2;
	PR_s[0].b0 = b0;
	PR_s[0].b1 = b1;
	PR_s[0].b2 = b2;
	PR_s[0].umax = 700.0;	//Todo change for n*Vcell

	// store values for beta PR
	PR_s[1].FF = 0.0;	//No feed-forward
	PR_s[1].h = Ts_arm;
	PR_s[1].w = w_grid;
	PR_s[1].kp = Kp_s;
	PR_s[1].ki = Ki_s;
	PR_s[1].in	 = 0.0;
	PR_s[1].in_1 = 0.0;
	PR_s[1].in_2 = 0.0;
	PR_s[1].out_1 = 0.0;
	PR_s[1].out_2 = 0.0;
	PR_s[1].a0 = a0;
	PR_s[1].a1 = a1;
	PR_s[1].a2 = a2;
	PR_s[1].b0 = b0;
	PR_s[1].b1 = b1;
	PR_s[1].b2 = b2;
	PR_s[1].umax = 700.0;//2.0*Vdc_PWM;
}


void init_PR_double_s(void)
{
	//--------------------------------------------------
	// Grid currents PRs
	double Kp_s, Ki_s;//, Tset_s;
	double Leq;

	Leq = Lacop+2.0*Lac;
	Kp_s = 2 * xi_s * Leq * wn_s;
	Ki_s = Leq * wn_s * wn_s;
	//Tset_s = 4 / (xi_s*wn_s);

	//printf("Kp_s: %.2f\t Ki_s: %.2f\tTset_s: %f\n",Kp_s,Ki_s,Tset_s);

	double a0, a1, a2, b1, b2;
	double h = T_proc;
	double w = w_grid;

	b1 = -2.0 * cos(h*w);
	b2 = 1.0;
	a0 = Kp_s + (Ki_s * sin(h*w) / (2.0*w));
	a1= -2.0 * Kp_s * cos(h*w);
	a2 = Kp_s - (Ki_s * sin(h*w) / (2.0*w));

	// store values for alpha PR
	PR_alphaSigma.h = Ts_arm;
	PR_alphaSigma.w = w_grid;
	PR_alphaSigma.kp = Kp_s;
	PR_alphaSigma.ki = Ki_s;
	PR_alphaSigma.in	 = 0.0;
	PR_alphaSigma.in_1 = 0.0;
	PR_alphaSigma.in_2 = 0.0;
	PR_alphaSigma.out_1 = 0.0;
	PR_alphaSigma.out_2 = 0.0;
	PR_alphaSigma.a0 = a0;
	PR_alphaSigma.a1 = a1;
	PR_alphaSigma.a2 = a2;
	PR_alphaSigma.b0 = b0;
	PR_alphaSigma.b1 = b1;
	PR_alphaSigma.b2 = b2;

	// store values for beta PR
	PR_betaSigma.h = Ts_arm;
	PR_betaSigma.w = w_grid;
	PR_betaSigma.kp = Kp_s;
	PR_betaSigma.ki = Ki_s;
	PR_betaSigma.in	 = 0.0;
	PR_betaSigma.in_1 = 0.0;
	PR_betaSigma.in_2 = 0.0;
	PR_betaSigma.out_1 = 0.0;
	PR_betaSigma.out_2 = 0.0;
	PR_betaSigma.a0 = a0;
	PR_betaSigma.a1 = a1;
	PR_betaSigma.a2 = a2;
	PR_betaSigma.b0 = b0;
	PR_betaSigma.b1 = b1;
	PR_betaSigma.b2 = b2;
}


void init_PR_tpw_z(void)
{
	//--------------------------------------------------
	// Circulant current PRs
	float Kp_z, Ki_z;//, Tset_z;
	float Leq;

	Leq = L;
	Kp_z = 2 * xi_z * Leq * wn_z;
	Ki_z = Leq * wn_z * wn_z;
	//Tset_z = 4 / (xi_z*wn_z);

	//printf("Kp_z: %.2f\t Ki_z: %.2f\tTset_z: %f\n",Kp_z,Ki_z,Tset_z);

	float a0, a1, a2, b1, b2;
	float h = T_proc;
	float w = w_grid;

	b1 = -2.0 * cos(h*w);
	b2 = 1.0;
	a0 = Kp_z + (Ki_z * sin(h*w) / (2.0*w));
	a1= -2.0 * Kp_z * cos(h*w);
	a2 = Kp_z - (Ki_z * sin(h*w) / (2.0*w));

	// store values for alpha PR
	PR_z[0].FF = 0.0;	//No feed-forward
	PR_z[0].h = h;
	PR_z[0].w = w;
	PR_z[0].kp = Kp_z;
	PR_z[0].ki = Ki_z;
	PR_z[0].in_1 = 0.0;
	PR_z[0].in_2 = 0.0;
	PR_z[0].out_1 = 0.0;
	PR_z[0].out_2 = 0.0;
	PR_z[0].a0 = a0;
	PR_z[0].a1 = a1;
	PR_z[0].a2 = a2;
	PR_z[0].b0 = b0;
	PR_z[0].b1 = b1;
	PR_z[0].b2 = b2;
	PR_z[0].umax = 500.0;//Vdc_PWM;


	// store values for beta PR
	PR_z[1].FF = 0.0;	//No feed-forward
	PR_z[1].h = h;
	PR_z[1].w = w;
	PR_z[1].kp = Kp_z;
	PR_z[1].ki = Ki_z;
	PR_z[1].in_1 = 0.0;
	PR_z[1].in_2 = 0.0;
	PR_z[1].out_1 = 0.0;
	PR_z[1].out_2 = 0.0;
	PR_z[1].a0 = a0;
	PR_z[1].a1 = a1;
	PR_z[1].a2 = a2;
	PR_z[1].b0 = b0;
	PR_z[1].b1 = b1;
	PR_z[1].b2 = b2;
	PR_z[1].umax = 500.0;//Vdc_PWM;
}


void init_PR_tpw_AXI_1MHz(void)
{
	//--------------------------------------------------
	// Circulant current PRs
	float Kp_o, Ki_o, Tset_o;
	float Leq, Ro;

	Leq = 0.00047;//2*L/3;//
	Ro = 47;

	Kp_o = (2 * xi_o * Leq * wn_o);// + Ro;
	Ki_o = Leq * wn_o * wn_o;
	Tset_o = 4 / (xi_o*wn_o);

	//Kp_o = Kp_o;
	//Ki_o = Ki_o;

	//printf("Kp_z: %.2f\t Ki_z: %.2f\tTset_z: %f\n",Kp_z,Ki_z,Tset_z);

	float a0, a1, a2, b1, b2;
	float h = 0.000001;			// sampling 1Mhz
	float w = 2*PII*frequency_Vout_khz*1000;

	b1 = -2.0 * cos(h*w);
	b2 = 1.0;
	a0 = Kp_o + (Ki_o * sin(h*w) / (2.0*w));
	a1= -2.0 * Kp_o * cos(h*w);
	a2 = Kp_o - (Ki_o * sin(h*w) / (2.0*w));

	// store values for PR
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+activator_Data_PR_1MHz, 0x0);		// Deactivate PR
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+a0_Data_PR_1MHz, toFix(a0 ,23));
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+a1_Data_PR_1MHz, toFix(a1 ,22));
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+a2_Data_PR_1MHz, toFix(a2 ,23));
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+b1_Data_PR_1MHz, toFix(b1 ,30));
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+umax_pos_Data_PR_1MHz, (int)(2000));
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+umax_neg_Data_PR_1MHz, (int)(-2000));
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+gain_out_Data_PR_1MHz, toFix(-1.0 ,6));

	//Xil_Out32(XPAR_PR_10KHZ_0_BASEADDR+IPCore_Reset_PR_10kHz, 0x1); //reset IP-Core. Set outputs to zero
}


void recalcule_PR_tpw_AXI_1MHz(void)
{
	//--------------------------------------------------
	// Circulant current PRs
	float Kp_o, Ki_o;
	float Leq;

	Leq = 0.00047;//2*L/3;//

	float a0, a1, a2, b1;
	float h = 0.000001;			// sampling 1Mhz
	float w = 2*PII*frequency_Vout_khz*1000;
	float wn = wn_o;//;1.5 * w;

	Kp_o = (2 * xi_o * Leq * wn);// + Ro;
	Ki_o = Leq * wn * wn;

	b1 = -2.0 * cos(h*w);
	b2 = 1.0;
	a0 = Kp_o + (Ki_o * sin(h*w) / (2.0*w));
	a1= -2.0 * Kp_o * cos(h*w);
	a2 = Kp_o - (Ki_o * sin(h*w) / (2.0*w));

	// store values for PR
	//Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+activator_Data_PR_1MHz, 0x0);		// Deactivate PR
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+a0_Data_PR_1MHz, toFix(a0 ,23));
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+a1_Data_PR_1MHz, toFix(a1 ,22));
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+a2_Data_PR_1MHz, toFix(a2 ,23));
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+b1_Data_PR_1MHz, toFix(b1 ,30));
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+umax_pos_Data_PR_1MHz, (int)(2000));
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+umax_neg_Data_PR_1MHz, (int)(-2000));
	Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+gain_out_Data_PR_1MHz, toFix(-1.0 ,6));
}




void reset_gain_slow_adc(void)
{
	/*--------------------------------------------------------------------------------------*/
	/* Set Slow ADC gains and offsets 														*/
	/*--------------------------------------------------------------------------------------*/
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain1_Data_slow_adc_conv, toFix(0.3234432 ,23)); //ap1
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset1_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain2_Data_slow_adc_conv, toFix(0.3161993 ,23)); //ap2
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset2_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain3_Data_slow_adc_conv, toFix(0.3154786 ,23)); //ap3
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset3_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain4_Data_slow_adc_conv, toFix(0.3158449 ,23)); //ap4
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset4_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain5_Data_slow_adc_conv, toFix(0.3090250 ,23)); //ap5
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset5_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain6_Data_slow_adc_conv, toFix(0.3152921 ,23)); //ap6
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset6_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain7_Data_slow_adc_conv, toFix(0.3225754 ,23)); //bp1
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset7_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain8_Data_slow_adc_conv, toFix(0.3160515 ,23)); //bp2
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset8_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain9_Data_slow_adc_conv, toFix(0.3153245 ,23)); //bp3
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset9_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain10_Data_slow_adc_conv, toFix(0.3153332 ,23)); //bp4
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset10_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain11_Data_slow_adc_conv, toFix(0.3149156 ,23)); //bp5
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset11_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain12_Data_slow_adc_conv, toFix(0.3144327 ,23)); //bp6
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset12_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain13_Data_slow_adc_conv, toFix(0.3085048 ,23)); //cp1
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset13_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain14_Data_slow_adc_conv, toFix(0.3137527 ,23)); //cp2
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset14_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain15_Data_slow_adc_conv, toFix(0.3151309 ,23)); //cp3
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset15_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain16_Data_slow_adc_conv, toFix(0.3140255 ,23)); //cp4
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset16_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain17_Data_slow_adc_conv, toFix(0.3226172 ,23)); //cp5
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset17_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain18_Data_slow_adc_conv, toFix(0.3160331 ,23)); //cp6
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset18_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain19_Data_slow_adc_conv, toFix(0.3091573 ,23)); //an1
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset19_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain20_Data_slow_adc_conv, toFix(0.3143980 ,23)); //an2
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset20_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Gain21_Data_slow_adc_conv, toFix(0.3146335 ,23)); //an3
	Xil_Out32(XPAR_SLOW_ADC_CONV_0_BASEADDR + Offset21_Data_slow_adc_conv, toFix(0, 20));



	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain1_Data_slow_adc_conv, toFix(0.3152866 ,23)); //an4
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset1_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain2_Data_slow_adc_conv, toFix(0.3158801 ,23)); //an5
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset2_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain3_Data_slow_adc_conv, toFix(0.3155016 ,23)); //an6
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset3_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain4_Data_slow_adc_conv, toFix(0.3200473 ,23)); //bn1
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset4_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain5_Data_slow_adc_conv, toFix(0.3132383 ,23)); //bn2
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset5_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain6_Data_slow_adc_conv, toFix(0.3144540 ,23)); //bn3
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset6_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain7_Data_slow_adc_conv, toFix(0.3149316 ,23)); //bn4
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset7_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain8_Data_slow_adc_conv, toFix(0.3089251 ,23)); //bn5
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset8_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain9_Data_slow_adc_conv, toFix(0.3128437 ,23)); //bn6
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset9_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain10_Data_slow_adc_conv, toFix(0.3213017 ,23)); //cn1
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset10_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain11_Data_slow_adc_conv, toFix(0.3136354 ,23)); //cn2
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset11_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain12_Data_slow_adc_conv, toFix(0.3145236 ,23)); //cn3
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset12_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain13_Data_slow_adc_conv, toFix(0.3136103 ,23)); //cn4
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset13_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain14_Data_slow_adc_conv, toFix(0.3137107 ,23)); //cn5
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset14_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain15_Data_slow_adc_conv, toFix(0.3102255 ,23)); //cn6
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset15_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain16_Data_slow_adc_conv, toFix(0.5 ,23)); //not used
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset16_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain17_Data_slow_adc_conv, toFix(0.5 ,23)); //not used
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset17_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain18_Data_slow_adc_conv, toFix(0.5 ,23)); //not used
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset18_Data_slow_adc_conv, toFix(0, 20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain19_Data_slow_adc_conv, toFix(-0.3624933 ,23)); //vab
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset19_Data_slow_adc_conv, toFix(-5.3763805,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain20_Data_slow_adc_conv, toFix(-0.3650782 ,23)); //vbc
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset20_Data_slow_adc_conv, toFix(-4.4411759,20));

	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Gain21_Data_slow_adc_conv, toFix(-0.3681648 ,23)); //vca
	Xil_Out32(XPAR_SLOW_ADC_CONV_1_BASEADDR + Offset21_Data_slow_adc_conv, toFix(-0.6185169,20));
	// End Set Slow ADC gains and offsets --------------------------------------------------//


		//Update fpga trip threshold values
//	    Xil_Out32(XPAR_TRIP_DET_0_BASEADDR + upTh_Data_trip_det, 1750);	//1920 for 600V
//	    Xil_Out32(XPAR_TRIP_DET_0_BASEADDR + loTh_Data_trip_det, -2000);	//-350 for -200V

}


//int Timer_PS_init(void)
//{
//	int Status;
//	XScuTimer_Config *ConfigPtr;
//
//	// Initialize the Scu Private Timer so that it is ready to use.
//	ConfigPtr = XScuTimer_LookupConfig(TIMER_PS_DEVICE_ID);
//
//	Status = XScuTimer_CfgInitialize(&Timer_PS, ConfigPtr, ConfigPtr->BaseAddr);
//	if (Status != XST_SUCCESS)
//	{
//		return XST_FAILURE;
//	}
//
//	// Load the timer counter register.
//	XScuTimer_LoadTimer(&Timer_PS, 0xFFFFFFFF);
//
//	XScuTimer_EnableAutoReload(&Timer_PS);
//
//	// Start the Scu Private Timer device.
//	XScuTimer_Start(&Timer_PS);
//
//	return XST_SUCCESS;
//}


/*-----------------------------------------------------------------------------------------------------*/
int init_interrupts(void)
{
	int Status;
	XScuGic_Config *GicConfig;    /* The configuration parameters of the controller */

	/*
	 * Initialize the interrupt controller driver so that it is ready to
	 * use.
	 */
	GicConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
	if (NULL == GicConfig) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(&InterruptController, GicConfig,
					GicConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test to ensure that the hardware was built
	 * correctly
	 */
	Status = XScuGic_SelfTest(&InterruptController);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Connect the interrupt controller interrupt handler to the hardware
	 * interrupt handling logic in the ARM processor.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(Xil_ExceptionHandler) XScuGic_InterruptHandler,
			&InterruptController);

	/*
	 * Enable interrupts in the ARM
	 */
	Xil_ExceptionEnable();

	/*
	 * Connect a device driver handler that will be called when an
	 * interrupt for the device occurs, the device driver handler performs
	 * the specific interrupt processing for the device
	 */
	Status = XScuGic_Connect(&InterruptController, INTC_DEVICE_INT_ID,
			   (Xil_ExceptionHandler)IRQ_ARM1_to_ARM0_Handler,
			   (void *)&InterruptController);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

//	/*
//	 * Connect a device driver handler that will be called when an
//	 * interrupt for the device occurs, the device driver handler performs
//	 * the specific interrupt processing for the device
//	 */
//	Status = XScuGic_Connect(&InterruptController, TMRCTR_INTERRUPT_ID,
//			   (Xil_ExceptionHandler)XTmrCtr_InterruptHandler,
//			   (void *)&TimerCounterInst);
//
//	if (Status != XST_SUCCESS) {
//		return XST_FAILURE;
//	}

	/*
	 * Enable the interrupt for the device and then cause (simulate) an
	 * interrupt so the handlers will be called
	 */
	XScuGic_Enable(&InterruptController, INTC_DEVICE_INT_ID);
	//XScuGic_Enable(&InterruptController, TMRCTR_INTERRUPT_ID);

	return XST_SUCCESS;
}


int init_timer_fpga(void)
{
	int Status;

	XTmrCtr* TmrCtrInstancePtr = &TimerCounterInst;
	u16 DeviceId = TMRCTR_DEVICE_ID;
	u8 TmrCtrNumber = TIMER_CNTR_0;

	u32 RESET_VALUE = (AXI_CLOCK_FREQUENCY / frequency_interrupt) - 2; 	//PWM_PERIOD = (TLR0 + 2) * AXI_CLOCK_PERIOD

	/*
	 * Initialize the timer counter so that it's ready to use,
	 * specify the device ID that is generated in xparameters.h
	 */
	Status = XTmrCtr_Initialize(TmrCtrInstancePtr, DeviceId);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test to ensure that the hardware was built
	 * correctly, use the 1st timer in the device (0)
	 */
	Status = XTmrCtr_SelfTest(TmrCtrInstancePtr, TmrCtrNumber);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Setup the handler for the timer counter that will be called from the
	 * interrupt context when the timer expires, specify a pointer to the
	 * timer counter driver instance as the callback reference so the handler
	 * is able to access the instance data
	 */
	XTmrCtr_SetHandler(TmrCtrInstancePtr, TimerCounterHandler, TmrCtrInstancePtr);

	/*
	 * Enable the interrupt of the timer counter so interrupts will occur
	 * and use auto reload mode such that the timer counter will reload
	 * itself automatically and continue repeatedly, without this option
	 * it would expire once only. Also the timer count down to 0
	 */
	XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber,
				XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION  | XTC_DOWN_COUNT_OPTION);

	/*
	 * Set a reset value for the timer counter such that it will expire
	 * eariler than letting it roll over from 0, the reset value is loaded
	 * into the timer counter when it is started
	 */
	XTmrCtr_SetResetValue(TmrCtrInstancePtr, TmrCtrNumber, RESET_VALUE);

	/*
	 * Start the timer counter such that it's incrementing by default,
	 * then wait for it to timeout a number of times
	 */
	XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);


	//TmrCtrDisableIntr(IntcInstancePtr, INTC_DEVICE_ID);
	return XST_SUCCESS;
}


// A function to implement bubble sort. de menor a mayor
void sorting(double arr[], s16 pos[], int n, double current)
{
   int i, j;
   double swap;
   s16 swap_int;
   s16 pos_temp[6];

   //Init pos array. sort from 1 to n (descendente, para ordenar de mayor a menor. pos=1 significa m�s cargado. pos=6 m�s descargado)
   for (i = 0; i < n; i++)
   {
	   pos_temp[i] = i+1;
   }

   //sort voltage and position array in increasing order
   for (i = 0; i < n-1; i++)
   {
	   // Last i elements are already in place
	   for (j = 0; j < n-i-1; j++)
	   {
		   if (arr[j] < arr[j+1])	//Ordenar del m�s cargado al menos cargado (descendente)
		   {
			   //swap voltage array
			   swap     = arr[j];
			   arr[j]   = arr[j+1];
			   arr[j+1] = swap;
			   //swap position array
			   swap_int 	 = pos_temp[j];
			   pos_temp[j]   = pos_temp[j+1];
			   pos_temp[j+1] = swap_int;
		   }
	   }
   }

   if (current > 0)	//Si corriente es positiva, las posiciones quedan igual. assign pos_temp to pos array
   {
	   for (i = 0; i < n; i++)
	   {
		   pos[pos_temp[i]-1] = i+1;
	   }
   }
   else				//Si corriente es negativa, invertir posiciones para que queden ordenado del mas descargado al mas cargado.
   {
	   for (i = 0; i < n; i++)
	   {
		   pos[pos_temp[i]-1] = n-i;
	   }
   }
}


/*-----------------------------------------------------------------------------------------------------*/
void IRQ_ARM1_to_ARM0_Handler(void *CallbackRef)
{
	// Read data from FreeRTOS OCM
	data_FreeRTOS2BareMetal = OCM_FreeRTOS2BareMetal;	//Copy data from OCM to global variable (DDR RAM)
	//printf("\t\t BareMetal: Interrupt from FreeRTOS %d. Data OCM %c\n", data_FreeRTOS2BareMetal.time_stamp, mem_data);

	//Rise flag for use in while(1)
	new_arm1_to_arm0_data_flag = 1;
}


void init_global_variables(void)
{
	//--------------------------------------------------
	//main.c
	test_cableado = 0;						// Use 0 for test connection of the MMC. Also for pre-charge and discharge
	master_reset = 0;
	band_spikes_mitigation = 1000;

	interrupt_flag = 0;
	interrupt_fail_counter = 0;

	update_PR = 1;
	boton_enable_SW = 0;
	vars.ref.i0_sigma_peak = 0.0;

	PR_control_FPGA = 0;
	activate_notch_s = 0;
	activate_notch_z = 0;
	activate_notch_Vc_transf = 1;

	//Parameters
	limit_Iabc = 1;
	limit_Io = 2;
	limit_Vo = 3;
	freq_out_ref = 4;
	Vo_rms_ref = 5;
	remote_control =6;


	//Control variables
	state_machine = 0;
	Vo_rms_med = 0.0;
	freq_out_real = 0.0;
	trip_fast = 0;
	trip_slow1 = 0;
	trip_slow2 = 0;

//	Timer_ns_min = 99999999999.0;
//	Timer_ns_max = 0.0;

	//--------------------------------------------------
	//main.h
	counter_interrupt = 0;	// Timer interrupt counter

	button_status=0;

	F_low = 40.0;

	T_ipfil = 1.0;
	Tfil = 1.0;
	wcfil = 1.0;
	wfil = 1.0;
	qqfil = 1.0;
	afil = 1.0;
	bfil = 1.0;
	pfil = 1.0;
	qfil = 1.0;
	rfil = 1.0;
	VOa1 = 1.0;
	VOa2 = 1.0;
	VOa3 = 1.0;
	VOa4 = 1.0;
	VOa5 = 1.0;
	VO_filt_off = 13.0;
	vo_meas_freq = 400;

	debug_tr = 0;
	debug_tr_max = 201;
	// Digital trimmer values for trips
	trip_slow_Vacb = 20; // Voltajes linea linea 140 para 600 [V]
	trip_slow_Vcxx = 200; //Voltajes condensadores cluster: 200 para 500[V], Trimmer High 2.69V
	trip_fast_curr = 159; //Corrientes: 159 para 9[A], Trimmer High 2.94[V]
	trip_fast_curr_Io = 790;//790 para 10A (paper); //36 para 2A.
	trip_fast_Vo = 63;		//32 para 1200V. 21 para 800V. //45 1700

	// Fast adc ip-core
	re_init_fast_adcs = 0;
	re_init_fast_adcs_IP = 0;
	change_fast_adc_pattern = 0;
	pattern = 0;
	shift_adc=1;

	cpld_res=0;

	vars.enable.FO_MMC = 0;
	enable_control=0;
	start_control=1;					// RETURM TO ONE TO AUMOMATECALY START
	reset_control = 0;
	arm1_time_stamp = 0;

	start_PR=0;
	offset_counter=0;
	offsets_update = 0;
	enable_offset=0;

	T_proc=0.0001;

	frequency_interrupt = 10000;	//Hz
	frequency_interrupt_prev = 10000;

	out_openloop = 2.0;
	out_openloop_ant =49.0;
	frequency_Vout_khz = 2.0;//5.0;				//khz
	frequency_Vout_khz_1 = 0.0;
	actual_frequency = 0;

	average_div = 25;
	average_div_ant = 25;

	// Envelope detector variables/constants
	vref_env_r = 500.0;
	amp_factor =  1.05;
	vref_env = 0.0;
	vref_env_amp = 0.0;

	// vout control variables
	control_vo = 0;
	Vout_ref_ant = 40.0;
	Vout_meas = 0.0;
	Vout_ref = 50.0;
	VoPI_act = 0.0;
	error_vo = 0.0;

	counter_state_machine = 0;

	descarga_cond = 0;
	steady_first_time = 1;

	status_slope = 0;
	freq_chang_st = 0;
	status_slope_VO = 0;

	offset_acumulator_structs.Ia  = 0.0;
	offset_acumulator_structs.Ib  = 0.0;
	offset_acumulator_structs.Ic  = 0.0;
	offset_acumulator_structs.Iap = 0.0;
	offset_acumulator_structs.Ian = 0.0;
	offset_acumulator_structs.Ibp = 0.0;
	offset_acumulator_structs.Ibn = 0.0;
	offset_acumulator_structs.Icp = 0.0;
	offset_acumulator_structs.Icn = 0.0;
	offset_acumulator_structs.Io  = 0.0;
	offset_acumulator_structs.Vo  = 0.0;
	offset_acumulator_structs.Vab = 0.0;
	offset_acumulator_structs.Vbc = 0.0;
	offset_acumulator_structs.Vca = 0.0;

	offset_structs.Ia  = 0.0;
	offset_structs.Ib  = 0.0;
	offset_structs.Ic  = 0.0;
	offset_structs.Iap = 0.0;
	offset_structs.Ian = 0.0;
	offset_structs.Ibp = 0.0;
	offset_structs.Ibn = 0.0;
	offset_structs.Icp = 0.0;
	offset_structs.Icn = 0.0;
	offset_structs.Io  = 0.0;
	offset_structs.Vo  = 0.0;
	offset_structs.Vab = 0.0;
	offset_structs.Vbc = 0.0;
	offset_structs.Vca = 0.0;

	//------------------------------------------------------
	// control_constants.h
	V_grid = 220.0*sqrt(2.0);

	Vc_ref=250.0;
	Vc_ref_1=0;
	Vc_ref_aux = 300;
	Vc_ref_aux2 = 300;
	vc_fixed=0;

	vcref_source = 2;					// changed
	Vc_refH = 100;

	L=0.0004;
	//Lacop=0.0004*0.2;
	Lacop=0.0004*0.02;	//Updated 15-01-2021
	h=0.00002;
	C = 0.0009;
	Lac = 2*0.0025;		//Extra L was added Todo
	w_load=2.0*3.1416*1000.0;
	w_grid=2.0*3.1416*50.0;
	Vo=30.0;
	n=6.0;								// NUMERO DE CELDAS POR CLUSTER

	Ts_arm = 0.0001;


	//Anchos de banda PR de entrada s y circulantes z
	wn_s = 400.0*2.0*PII;//2000.0*2.0*PII;//800.0*2.0*PII;
	wn_z = 400.0*2.0*PII;// 2000.0*2.0*PII;//1100.0*2.0*PII;
	wn_o = 4.0*1000.0*frequency_Vout_khz*2.0*PII;	//40000.0;10000.0;//

	//Factor de amortiguamiento PR de entrada s y circulantes z
	xi_s=0.707;//0.907;
	xi_z=0.707;//0.9;
	xi_o=0.707;//0.9;

	//Anchos de banda control promedio b1, control inner b2 y control intra b3
	wn_b1 = 2*2.0*PII;
	wn_b2 = 1*2.0*PII;
	wn_b3 = 1*2.0*PII;

	//Factor de amortiguamiento control promedio b1, control inner b2 y control intra b3
	xi_b1=0.707;
	xi_b2=0.707;
	xi_b3=0.707;

	id_max = 7.0;

	control_intra=0;
	intra_al=1;
	intra_be=1;

	control_inner=0;
	init_pi=1;


	//tuning PI VOUT
	Tpi=0.0001;
	kipi=1.0;
	//kppi=2.8974;
	//api=31.36;
	kppi=0.010103;//3.0947; antiguos
	api=7172;//27.0; antiguos
	azpi=0.0;
	kzpi=0.0;
	kpzpi=0.0;
	kizpi=0.0;
	v0s_max=3000.0;


	debug_SoC_parameters_ARM0.active = 0;	//si se borra se cae el programa. Se puede sacar cuando se actualice el debug FPGA_DDRRA IP core

	counter_control = 0;
	counter_test = 0;

	vars.ref.v0_delta = 0.0;
	vars.ref.vap = 0.0;
	vars.ref.vbp = 0.0;
	vars.ref.vcp = 0.0;
	vars.ref.van = 0.0;
	vars.ref.vbn = 0.0;
	vars.ref.vcn = 0.0;


	vars.ref.val_sigma = 0.0;
	vars.ref.vbe_sigma = 0.0;
	vars.ref.val_delta = 0.0;
	vars.ref.vbe_delta = 0.0;

	///////////////////////////////////////////////////////////////////////////////////////
	g.control.ref.iClusterABCPN.ap = 0.0;
	g.control.ref.iClusterABCPN.bp = 0.0;
	g.control.ref.iClusterABCPN.cp = 0.0;
	g.control.ref.iClusterABCPN.an = 0.0;
	g.control.ref.iClusterABCPN.bn = 0.0;
	g.control.ref.iClusterABCPN.cn = 0.0;

	g.control.ref.iClustersAB0SD.alphaSigma = 0.0;
	g.control.ref.iClustersAB0SD.betaSigma = 0.0;
	g.control.ref.iClustersAB0SD.ceroSigma = 0.0;
	g.control.ref.iClustersAB0SD.alphaDelta = 0.0;
	g.control.ref.iClustersAB0SD.betaDelta = 0.0;
	g.control.ref.iClustersAB0SD.ceroDelta = 0.0;

	g.control.ref.iClustersNegativeAB0SD.alphaSigma = 0.0;
	g.control.ref.iClustersNegativeAB0SD.betaSigma = 0.0;
	g.control.ref.iClustersNegativeAB0SD.ceroSigma = 0.0;
	g.control.ref.iClustersNegativeAB0SD.alphaDelta = 0.0;
	g.control.ref.iClustersNegativeAB0SD.betaDelta = 0.0;
	g.control.ref.iClustersNegativeAB0SD.ceroDelta = 0.0;

	g.control.ref.iClustersPositiveSigma.d = 0.0;
	g.control.ref.iClustersPositiveSigma.q = 0.0;
	g.control.ref.iClustersPositiveSigma.cero = 0.0;

	g.control.ref.iClustersPositiveDelta.d = 0.0;
	g.control.ref.iClustersPositiveDelta.q = 0.0;
	g.control.ref.iClustersPositiveDelta.cero = 0.0;

	g.control.ref.iClustersNegativeSigma.d = 0.0;
	g.control.ref.iClustersNegativeSigma.q = 0.0;
	g.control.ref.iClustersNegativeSigma.cero = 0.0;

	g.control.ref.iClustersNegativeDelta.d = 0.0;
	g.control.ref.iClustersNegativeDelta.q = 0.0;
	g.control.ref.iClustersNegativeDelta.cero = 0.0;

	g.control.ref.iClustersSigma.clarkePositive.alpha = 0.0;
	g.control.ref.iClustersSigma.clarkePositive.beta = 0.0;
	g.control.ref.iClustersSigma.clarkePositive.cero = 0.0;
	g.control.ref.iClustersSigma.clarkeNegative.alpha = 0.0;
	g.control.ref.iClustersSigma.clarkeNegative.beta = 0.0;
	g.control.ref.iClustersSigma.clarkeNegative.cero = 0.0;
	g.control.ref.iClustersSigma.parkPositive.d = 0.0;
	g.control.ref.iClustersSigma.parkPositive.q = 0.0;
	g.control.ref.iClustersSigma.parkPositive.cero = 0.0;
	g.control.ref.iClustersSigma.parkNegative.d = 0.0;
	g.control.ref.iClustersSigma.parkNegative.q = 0.0;
	g.control.ref.iClustersSigma.parkNegative.cero = 0.0;

	g.control.ref.iClustersDelta.clarkePositive.alpha = 0.0;
	g.control.ref.iClustersDelta.clarkePositive.beta = 0.0;
	g.control.ref.iClustersDelta.clarkePositive.cero = 0.0;
	g.control.ref.iClustersDelta.clarkeNegative.alpha = 0.0;
	g.control.ref.iClustersDelta.clarkeNegative.beta = 0.0;
	g.control.ref.iClustersDelta.clarkeNegative.cero = 0.0;
	g.control.ref.iClustersDelta.parkPositive.d = 0.0;
	g.control.ref.iClustersDelta.parkPositive.q = 0.0;
	g.control.ref.iClustersDelta.parkPositive.cero = 0.0;
	g.control.ref.iClustersDelta.parkNegative.d = 0.0;
	g.control.ref.iClustersDelta.parkNegative.q = 0.0;
	g.control.ref.iClustersDelta.parkNegative.cero = 0.0;

	//g.control.ref.v0_delta_peak

	g.control.ref.vClustersABCPN.ap = 0.0;
	g.control.ref.vClustersABCPN.bp = 0.0;
	g.control.ref.vClustersABCPN.cp = 0.0;
	g.control.ref.vClustersABCPN.an = 0.0;
	g.control.ref.vClustersABCPN.bn = 0.0;
	g.control.ref.vClustersABCPN.cn = 0.0;

	g.control.ref.vClustersAB0SD.alphaSigma = 0.0;
	g.control.ref.vClustersAB0SD.betaSigma = 0.0;
	g.control.ref.vClustersAB0SD.ceroSigma = 0.0;
	g.control.ref.vClustersAB0SD.alphaDelta = 0.0;
	g.control.ref.vClustersAB0SD.betaDelta = 0.0;
	g.control.ref.vClustersAB0SD.ceroDelta = 0.0;

}




/****************************************************************************/
/**
*
* This function sets up the Ticker timer.
*
* @param	None
*
* @return	XST_SUCCESS if everything sets up well, XST_FAILURE otherwise.
*
* @note		None
*
*****************************************************************************/
int SetupTicker(void)
{
	int Status;
	TmrCntrSetup *TimerSetup;
	XTtcPs *TtcPsTick;

	TimerSetup = &SettingsTable;

	/*
	 * Set up appropriate options for Ticker: interval mode without
	 * waveform output.
	 */
	TimerSetup->Options |= (XTTCPS_OPTION_INTERVAL_MODE |
					      XTTCPS_OPTION_WAVE_DISABLE);

	/*
	 * Calling the timer setup routine
	 *  . initialize device
	 *  . set options
	 */
	Status = SetupTimer(TTC_TICK_DEVICE_ID);
	if(Status != XST_SUCCESS) {
		return Status;
	}

	TtcPsTick = &TtcPsInst;

	/*
	 * Connect to the interrupt controller
	 */
	Status = XScuGic_Connect(&InterruptController, TTC_TICK_INTR_ID,
		(Xil_ExceptionHandler)TickHandler, (void *)TtcPsTick);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Enable the interrupt for the Timer counter
	 */
	XScuGic_Enable(&InterruptController, TTC_TICK_INTR_ID);

	/*
	 * Enable the interrupts for the tick timer/counter
	 * We only care about the interval timeout.
	 */
	XTtcPs_EnableInterrupts(TtcPsTick, XTTCPS_IXR_INTERVAL_MASK);

	/*
	 * Start the tick timer/counter
	 */
	XTtcPs_Start(TtcPsTick);

	return Status;
}



/****************************************************************************/
/**
*
* This function sets up a timer counter device, using the information in its
* setup structure.
*  . initialize device
*  . set options
*  . set interval and prescaler value for given output frequency.
*
* @param	DeviceID is the unique ID for the device.
*
* @return	XST_SUCCESS if successful, otherwise XST_FAILURE.
*
* @note		None.
*
*****************************************************************************/
int SetupTimer(int DeviceID)
{
	int Status;
	XTtcPs_Config *Config;
	XTtcPs *Timer;
	TmrCntrSetup *TimerSetup;

	TimerSetup = &SettingsTable;

	Timer = &TtcPsInst;

	/*
	 * Look up the configuration based on the device identifier
	 */
	Config = XTtcPs_LookupConfig(DeviceID);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	/*
	 * Initialize the device
	 */
	Status = XTtcPs_CfgInitialize(Timer, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Set the options
	 */
	XTtcPs_SetOptions(Timer, TimerSetup->Options);

	/*
	 * Timer frequency is preset in the TimerSetup structure,
	 * however, the value is not reflected in its other fields, such as
	 * IntervalValue and PrescalerValue. The following call will map the
	 * frequency to the interval and prescaler values.
	 */
	XTtcPs_CalcIntervalFromFreq(Timer, TimerSetup->OutputHz,
		&(TimerSetup->Interval), &(TimerSetup->Prescaler));

	/*
	 * Set the interval and prescale
	 */
	XTtcPs_SetInterval(Timer, TimerSetup->Interval);
	XTtcPs_SetPrescaler(Timer, TimerSetup->Prescaler);

	return XST_SUCCESS;
}



/***************************************************************************/
/**
*
* This function is the handler which handles the periodic tick interrupt.
* It updates its count, and set a flag to signal PWM timer counter to
* update its duty cycle.
*
* This handler provides an example of how to handle data for the TTC and
* is application specific.
*
* @param	CallBackRef contains a callback reference from the driver, in
*		this case it is the instance pointer for the TTC driver.
*
* @return	None.
*
* @note		None.
*
*****************************************************************************/
static void TickHandler(void *CallBackRef)
{
	u32 StatusEvent;

	/*
	 * Read the interrupt status, then write it back to clear the interrupt.
	 */
	StatusEvent = XTtcPs_GetInterruptStatus((XTtcPs *)CallBackRef);
	XTtcPs_ClearInterruptStatus((XTtcPs *)CallBackRef, StatusEvent);

	if (0 != (XTTCPS_IXR_INTERVAL_MASK & StatusEvent))
	{
		TickCount++;
		counter_interrupt++;	// interrupt counter for lifecheck
//		Timer_start = XScuTimer_GetCounterValue(&Timer_PS);

		if(interrupt_flag == 1)
		{
			interrupt_fail_counter++;
			printf("\n\tFAILS: %d\n", interrupt_fail_counter);
		}
		interrupt_flag = 1;
		/* Set the on Board Led MIO7 */
		XGpioPs_WritePin(&Gpiops, GPIOps_pin_MIO7, 0x1);		// Debug Led for calculate interrupt time


		/*--------------------------------------------------------------------------------------*/
		/* External button logic																*/
		/*--------------------------------------------------------------------------------------*/
		//enable_button.actual = (XGpio_DiscreteRead(&Gpio_button, GPIO_CHANNEL1) & 0x0008) >> 3;
		enable_button.actual = boton_enable_SW;

		if(enable_button.actual != enable_button.previous)
		{
			enable_button.change = 1;
		}
		else
		{
			enable_button.change = 0;
		}

		switch (Enable_button_State)
		{
			case STOP:
				start_control = 0;
				if(enable_button.change)
				{
					if(enable_button.actual == 1)	// Change from 0 to 1. Change to START state
					{
						Enable_button_State = START;
					}
					else	// Change from 1 to 0. Remain in STOP state
					{
						Enable_button_State = STOP;
					}
				}
				break;

			case START:
				start_control = 1;
				if(data.trip.general == 1)	//Trip occurs
				{
					Enable_button_State = STOP;
				}
				else
				{
					if(enable_button.change)
					{
						if(enable_button.actual == 1)	// Change from 0 to 1. Remain in START state
						{
							Enable_button_State = START;
						}
						else	// Change from 1 to 0. Change to STOP state
						{
							Enable_button_State = STOP;
						}
					}
				}
				break;

			default:
				Enable_button_State = STOP;
				break;
		}
		enable_button.previous = enable_button.actual;

		//--------------------------------------------------------------------------------------------------------------
		// Start sampling variables from FPGA to ARM
//		Timer_meas_start = XScuTimer_GetCounterValue(&Timer_PS);

		// Sample current variables
		XGpio_DiscreteClear(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_SAMPLER);		// Reset debug memory IP Core
		XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_SAMPLER);		// Enable the begin of sampling

		XGpio_DiscreteClear(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_DEBUG_8CH);	// Reset debug memory IP Core
		XGpio_DiscreteSet(&Gpio_reset, GPIO_CHANNEL1, RESET_PIN_DEBUG_8CH);	// unReset debug memory IP Core

		/*--------------------------------------------------------------------------------------*/
		/* Read grid voltages from FPGA															*/
		/*--------------------------------------------------------------------------------------*/
		g.control.meas.vGridLine.ab = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_19_Data_slow_adc_conv), 14);
		g.control.meas.vGridLine.bc = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_20_Data_slow_adc_conv), 14);
		g.control.meas.vGridLine.ca = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_21_Data_slow_adc_conv), 14);

		/*--------------------------------------------------------------------------------------*/
		/* Read cell capacitor voltages from FPGA												*/
		/*--------------------------------------------------------------------------------------*/
		g.control.meas.vCapacitorsDetailedABCPN.ap1 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_1_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.bp1 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_7_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.cp1 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_13_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.an1 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_19_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.bn1 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_4_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.cn1 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_10_Data_slow_adc_conv), 14);

		g.control.meas.vCapacitorsDetailedABCPN.ap2 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_2_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.bp2 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_8_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.cp2 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_14_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.an2 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_20_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.bn2 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_5_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.cn2 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_11_Data_slow_adc_conv), 14);

		g.control.meas.vCapacitorsDetailedABCPN.ap3 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_3_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.bp3 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_9_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.cp3 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_15_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.an3 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_21_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.bn3 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_6_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.cn3 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_12_Data_slow_adc_conv), 14);

		g.control.meas.vCapacitorsDetailedABCPN.ap4 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_4_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.bp4 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_10_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.cp4 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_16_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.an4 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_1_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.bn4 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_7_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.cn4 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_13_Data_slow_adc_conv), 14);

		g.control.meas.vCapacitorsDetailedABCPN.ap5 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_5_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.bp5 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_11_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.cp5 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_17_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.an5 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_2_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.bn5 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_8_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.cn5 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_14_Data_slow_adc_conv), 14);

		g.control.meas.vCapacitorsDetailedABCPN.ap6 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_6_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.bp6 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_12_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.cp6 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_0_BASEADDR + out_18_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.an6 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_3_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.bn6 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_9_Data_slow_adc_conv), 14);
		g.control.meas.vCapacitorsDetailedABCPN.cn6 = toFloat(Xil_In32(XPAR_SLOW_ADC_CONV_1_BASEADDR + out_15_Data_slow_adc_conv), 14);


		/*--------------------------------------------------------------------------------------*/
		/* Read currents from FPGA for PR controllers											*/
		/*--------------------------------------------------------------------------------------*/
		vars.meas.theta_s   = toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_1_S_AXI_LITE_BASEADDR, THETA_S_OFFSET),21);
		vars.meas.i0_sigma 	= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_1_S_AXI_LITE_BASEADDR, I0_S_OFFSET),16);
		vars.meas.PLL_w  	= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_1_S_AXI_LITE_BASEADDR, PLL_OMEGA_OFFSET),14);
		vars.meas.PLL_Vq  	= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_1_S_AXI_LITE_BASEADDR, PLL_VQ_OFFSET),21);
		vars.meas.ia 		= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_1_S_AXI_LITE_BASEADDR, IA_OFFSET),16);
		vars.meas.ib 		= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_1_S_AXI_LITE_BASEADDR, IB_OFFSET),16);
		vars.meas.ic 		= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_1_S_AXI_LITE_BASEADDR, IC_OFFSET),16);

		vars.meas.iap 		= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, IAP_OFFSET),16);
		vars.meas.ibp 		= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, IBP_OFFSET),16);
		vars.meas.icp 		= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, ICP_OFFSET),16);
		vars.meas.ian 		= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, IAN_OFFSET),16);
		vars.meas.ibn 		= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, IBN_OFFSET),16);
		vars.meas.icn 		= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, ICN_OFFSET),16);
		vars.meas.Val_grid  = toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, VAL_GRID_OFFSET),14);
		vars.meas.Vbe_grid  = toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, VBE_GRID_OFFSET),14);

		g.control.meas.iGridABC.a = toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_1_S_AXI_LITE_BASEADDR, IA_OFFSET), 16);
		g.control.meas.iGridABC.b = toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_1_S_AXI_LITE_BASEADDR, IB_OFFSET), 16);
		g.control.meas.iGridABC.c = toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_1_S_AXI_LITE_BASEADDR, IC_OFFSET), 16);

		g.control.meas.iClusterABCPN.ap	= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, IAP_OFFSET), 16);
		g.control.meas.iClusterABCPN.bp	= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, IBP_OFFSET), 16);
		g.control.meas.iClusterABCPN.cp	= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, ICP_OFFSET), 16);
		g.control.meas.iClusterABCPN.an	= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, IAN_OFFSET), 16);
		g.control.meas.iClusterABCPN.bn	= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, IBN_OFFSET), 16);
		g.control.meas.iClusterABCPN.cn	= toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR, ICN_OFFSET), 16);

//		Timer_meas_end = XScuTimer_GetCounterValue(&Timer_PS);
//		Timer_meas_diff = Timer_meas_start - Timer_meas_end;
//		Timer_meas_ns = ((Timer_meas_diff * 1.0) / COUNTS_PER_SECOND) * 1000000000.0;
		// End sampling variables from FPGA to ARM

		vars.meas.Vab_grid  = toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST2_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG3_OFFSET),14);
		vars.meas.Vbc_grid  = toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST2_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG4_OFFSET),14);
		vars.meas.Vca_grid  = toFloat(DEBUG_8_CHANNEL_mReadReg(XPAR_DEBUG_8_FAST2_S_AXI_LITE_BASEADDR, DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG5_OFFSET),14);

		//Notch a tensiones de red l-l
		Calculate_filter_notch(&notch_Vab, vars.meas.Vab_grid,   &vars.meas_filt.Vab_grid);
		Calculate_filter_notch(&notch_Vbc, vars.meas.Vbc_grid,   &vars.meas_filt.Vbc_grid);
		Calculate_filter_notch(&notch_Vca, vars.meas.Vca_grid,   &vars.meas_filt.Vca_grid);

		vars.meas_filt.Vab_grid = vars.meas.Vab_grid - vars.meas_filt.Vab_grid;
		vars.meas_filt.Vbc_grid = vars.meas.Vbc_grid - vars.meas_filt.Vbc_grid;
		vars.meas_filt.Vca_grid = vars.meas.Vca_grid - vars.meas_filt.Vca_grid;

		//tensiones de red l-l a alpha-beta l-l
		abc2albe(vars.meas_filt.Vab_grid, vars.meas_filt.Vbc_grid, vars.meas_filt.Vca_grid, &vars.meas.Val_grid_ll, &vars.meas.Vbe_grid_ll);

		//convertir a fase-neutro alpha-beta
		vars.meas.Val_grid_gen = vars.meas.Val_grid_ll / sqrt(3);
		vars.meas.Vbe_grid_gen = vars.meas.Vbe_grid_ll / sqrt(3);

		//pll
		vars.meas.theta_s_ll = atan2(vars.meas.Vbe_grid_gen,vars.meas.Val_grid_gen);

		//quitar offset theta (30�) entre l-l y f-n
		vars.meas.theta_s_gen = vars.meas.theta_s_ll - 0.523598775;

		if(vars.meas.theta_s_gen > pii)
		{
			vars.meas.theta_s_gen = vars.meas.theta_s_gen - 2.0*pii;
		}
		else if(vars.meas.theta_s_gen < -pii)
		{
			vars.meas.theta_s_gen = vars.meas.theta_s_gen + 2.0*pii;
		}

		dq2albe(311.0, 0.0, &clean_Val, &clean_Vbe, vars.meas.theta_s_gen);
//		vars.meas.theta_s = vars.meas.theta_s_gen;
		g.control.meas.thetaGrid = vars.meas.theta_s;

		albe2dq(vars.meas.Val_grid, vars.meas.Vbe_grid, &vars.meas.Vd_grid, &vars.meas.Vq_grid, vars.meas.theta_s);

		vars.meas.Vd_grid_gen = sqrt((vars.meas.Val_grid * vars.meas.Val_grid) + (vars.meas.Vbe_grid * vars.meas.Vbe_grid));
		//vars.meas.Val_grid_gen = cos(vars.meas.theta_s)*vars.meas.Vd_grid_gen;
		//vars.meas.Vbe_grid_gen = cos(vars.meas.theta_s-1.570796327)*vars.meas.Vd_grid_gen;

		if(reset_control == 1)
		{
			start_control = 0;
			MMC_State_Machine = INIT;
			printf("RESET CONTROL\n");
		}

		/*--------------------------------------------------------------------------------------*/
		/* M�quina de estados del MMC															*/
		/*--------------------------------------------------------------------------------------*/
				switch (MMC_State_Machine)
				{
					case INIT:
						// hacer nada hasta que se active el enable control

						// Abrir switches de red
						XGpio_DiscreteClear(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_0);
						XGpio_DiscreteClear(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_1);
						XGpio_DiscreteSet(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_2);

						boton_enable_SW = 1;	//ToDo: Borrar esto

						enable_control = 0;
						//start_control = 0;	//TODO BORRAR!!!!

						if(start_control == 1)
						{
							if(descarga_cond == 1)
							{
								cpld_res = 1;
								vars.enable.PR_s = 0;
								vars.enable.PR_z = 0;			//Todo: separar en 2 variables, 1 para sec pos y otra para sec neg

								out_openloop = Vc_ref;
								control_vo=2; //activar salida open loop
								Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_V0S_Data_M2C_NLC_6cell_Control_Vo, 0x0);	// Vo from FPGA or ARM controllers //Use 0x0 for discharge.

								MMC_State_Machine = DISCHARGE;
								printf("'tSM: Descarga condensadores\n");
								//offline_data_parameters.enable=1;
							}
							else
							{
								// inicializar PIs y PRs
								// activar PR entrada
								vars.enable.PR_s = 1;
								// activar control promedio
								Vc_ref = 300;
								cpld_res = 1;
								reset_control = 0;
								counter_state_machine = 0;    // reset counter to measure time
								enable_offset = 1;				// get measurements (currents) zero offset
								MMC_State_Machine = GET_MEAS_OFFSET;
								printf("\tSM: Get measurements zero offset\n");
							}
						}

						break;

					case GET_MEAS_OFFSET:
						// hacer nada hasta que se cumpla el tiempo
						if(counter_state_machine == delay_state_machine)    // 10.000 <=> 1s
						{
							counter_state_machine = 0;    // reset counter to measure time
							offsets_update = 1;				// set zero offset value (currents measurements)
							MMC_State_Machine = SET_MEAS_OFFSET;
							printf("\tSM: Set measurements zero offset\n");
						}
						break;

					case SET_MEAS_OFFSET:
						// hacer nada hasta que se cumpla el tiempo
						if(counter_state_machine == delay_state_machine)    // 10.000 <=> 1s
						{
							counter_state_machine = 0;    // reset counter to measure time
							XGpio_DiscreteSet(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_0);	// Close preload switch
							XGpio_DiscreteClear(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_1);	// Open direct grid connected switch
							XGpio_DiscreteClear(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_2);	// ~Enable switch
							MMC_State_Machine = PRELOAD_SWITCH;
							printf("\tSM: Preload MMC capacitors\n");
						}
						break;

					case PRELOAD_SWITCH:
						// hacer nada hasta que se cumpla el tiempo
						if(counter_state_machine == delay_state_machine)    // 10.000 <=> 1s	15000
						{
							counter_state_machine = 0;    // reset counter to measure time
							XGpio_DiscreteSet(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_0);	// Close preload switch
							XGpio_DiscreteSet(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_1);	// Close direct grid connected switch
							XGpio_DiscreteClear(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_2);	// ~Enable switch
							MMC_State_Machine = BYPASS_PRELOAD_SWITCH;
							//printf("\tSM: Bypass preload. MMC directly connected to grid\n");
							printf("\tSM: MMC directly connected to grid\n");
						}
						break;

					case BYPASS_PRELOAD_SWITCH:
						// hacer nada hasta que se cumpla el tiempo
						if(counter_state_machine == delay_state_machine)    // 10.000 <=> 1s
						{
							if(test_cableado == 1)
							{
								// Abrir switches de red
								XGpio_DiscreteClear(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_0);	// Open preload switch
								XGpio_DiscreteClear(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_1);	// Open direct grid connected switch
								XGpio_DiscreteSet(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_2);	// Disable enable switch

								MMC_State_Machine = TEST_CABLEADO;
								printf("\tSM: Test cableado\n");
								Vc_ref = 200;
								PR_control_FPGA = 0;//PR_control_FPGA = 1;
								//vars.enable.PR_s = 1;		// Activate input current PR
								frequency_Vout_khz = 1.0;
								control_vo=2;  //activar salida open loop
								Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_V0S_Data_M2C_NLC_6cell_Control_Vo, 0x0);	// Vo from FPGA or ARM controllers //Start with full control. use 0x1 for discharge.
								//offline_data_parameters.enable=1;
							}
							else
							{

//								// Abrir switches de red	ToDo only for debug.control. remove switch logic
//								XGpio_DiscreteClear(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_0);	// Open preload switch
//								XGpio_DiscreteClear(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_1);	// Open direct grid connected switch
//								XGpio_DiscreteSet(&Gpio_preload, GPIO_CHANNEL1, GPIO_pin_2);	// Disable enable switch

								counter_state_machine = 0;    // reset counter to measure time
								Vc_ref = 300.0;	//310 era el defecto
								vcref_source = 1;			// Set NLC Vc to Vc_ref setpoint
								PR_control_FPGA = 0;
								vars.enable.PR_s = 1;		// Activate input current PR
								MMC_State_Machine = ENABLE_CONTROL;
								printf("\tSM: Enable control\n");
							}
						}

						break;

					case TEST_CABLEADO:
						enable_control=1;
						//Xil_Out32(XPAR_SELECTOR_SW2513_0_BASEADDR + enable_preload_Data_selector_sw2513, 0x0); //Start with full control. use 0x1 for discharge.
						break;

					case ENABLE_CONTROL:
						// hacer nada hasta que se cumpla el tiempo
						enable_control=1;

						if(counter_state_machine == delay_state_machine)    // 10.000 <=> 1s	20000
						{
							Vc_ref = 360;	//320 Todo
							counter_state_machine = 0;    // reset counter to measure time
							MMC_State_Machine = SET_Vc;
							printf("\tSM: Set Vc to 320V\n");
						}
						break;

					case SET_Vc:
						// hacer nada hasta que se cumpla el tiempo
						//enable_control=1;

						if(enable_control == 0)	//trip occurs
						{
							start_control = 0;
							counter_state_machine = 0;    // reset counter to measure time
							MMC_State_Machine = INIT;
							printf("New State Machine: INIT, due to system trip\n");
						}
						if(counter_state_machine == delay_state_machine)    // 10.000 <=> 1s	40000
						{
							// activar balance inner PR y PI
							control_intra=1;
							counter_state_machine = 0;    // reset counter to measure time
							MMC_State_Machine = ENABLE_BALANCE_INTRA;
							printf("\tSM: Enable intra capacitor balance\n");
//							vars.enable.PR_z = 1;	//PR_act.activ_ab_sigma_s=1;
//							control_inner=1;//1
//							counter_state_machine = 0;    // reset counter to measure time
//							MMC_State_Machine = ENABLE_BALANCE_INNER;
//							printf("\tSM: Enable inner capacitor balance\n");
						}
						break;

					case ENABLE_BALANCE_INNER:
						// hacer nada hasta que se cumpla el tiempo

						if(enable_control == 0)	//trip occurs
						{
							start_control = 0;
							counter_state_machine = 0;    // reset counter to measure time
							MMC_State_Machine = INIT;
							printf("\tSM: INIT, due to system trip\n");
						}
						if(counter_state_machine == delay_state_machine)    // 10.000 <=> 1s	30000
						{
							// activar balance intra PR y PI
//							control_intra=1;
//							counter_state_machine = 0;    // reset counter to measure time
//							MMC_State_Machine = ENABLE_BALANCE_INTRA;
//							printf("\tSM: Enable intra capacitor balance\n");
							// activar referencia de salida
							out_openloop=0; //valor inicial ref salida.
							control_vo=2; //activar salida open loop
							Vo_ref.final=500.0;	//definir set point del estado estacionario para PI
							Vc_ref = vref_env_amp;	//Cambiar al valor entragado por la fpga
							freq_chang_st = 1;
							counter_state_machine = 0;    // reset counter to measure time
							MMC_State_Machine = ENABLE_Vo;
							printf("\tSM: Activate output voltage\n");
						}
						break;

					case ENABLE_BALANCE_INTRA:
						// hacer nada hasta que se cumpla el tiempo

						if(enable_control == 0)	//trip occurs
						{
							start_control = 0;
							counter_state_machine = 0;    // reset counter to measure time
							MMC_State_Machine = INIT;
							printf("\tSM: INIT, due to system trip\n");
						}
						if(counter_state_machine == delay_state_machine)    // 10.000 <=> 1s	30000
						{
							vars.enable.PR_z = 1;	//PR_act.activ_ab_sigma_s=1;
							control_inner=1;//1
							counter_state_machine = 0;    // reset counter to measure time
							MMC_State_Machine = ENABLE_BALANCE_INNER;
							printf("\tSM: Enable inner capacitor balance\n");
//							// activar referencia de salida
//							out_openloop=0; //valor inicial ref salida.
//							control_vo=2; //activar salida open loop
//							Vo_ref.final=500.0;	//definir set point del estado estacionario para PI
//							Vc_ref = vref_env_amp;	//Cambiar al valor entragado por la fpga
//							freq_chang_st = 1;
//							counter_state_machine = 0;    // reset counter to measure time
//							MMC_State_Machine = ENABLE_Vo;
//							printf("\tSM: Activate output voltage\n");
						}
						break;

					case ENABLE_Vo:
						//Hacer nada
						Vc_ref = vref_env_amp;	//Cambiar al valor entragado por la fpga
						PR_control_FPGA = 1;

						counter_test++;

						if(reset_control == 1)
						{
							start_control = 0;
							MMC_State_Machine = INIT;
							printf("RESET CONTROL\n");
						}
						if(enable_control == 0)	//trip occurs
						{
							start_control = 0;
							counter_state_machine = 0;    // reset counter to measure time
							MMC_State_Machine = INIT;
							printf("\tST: INIT, due to system trip\n");
						}
						if(counter_state_machine == delay_state_machine)    // 10.000 <=> 1s	10000
						{
							// empezar la rampa de Vc y Vo reference
							counter_state_machine = 0;    // reset counter to measure time
							MMC_State_Machine = RAMP_Vo;
							printf("\tSM: Vo and Vc ramp up\n");
						}
						break;

					case RAMP_Vo:
						//Aumentar el valor de Vo seg�n el contador
						//Vc aumenta seg�n lo necesario por Vo
						//Vo inicial 50V. Vo final 470V. Delta 300V.
						ramp_up_global(&out_openloop, 40, 10000);    // 10.000 <=> 1s
						Vc_ref = vref_env_amp;	//Cambiar al valor entragado por la fpga

						if(reset_control == 1)
						{
							start_control = 0;
							MMC_State_Machine = INIT;
							printf("RESET CONTROL\n");
						}
						if(enable_control == 0)	//trip occurs
						{
							start_control = 0;
							counter_state_machine = 0;    // reset counter to measure time
							MMC_State_Machine = INIT;
							printf("\tSM: INIT, due to system trip\n");
						}

						if(out_openloop >= 750) //140	70
						{
							// desactivar las rampas
							counter_state_machine = 0;    // reset counter to measure time
							MMC_State_Machine = STEADY_STATE;
							//Vo_ref.actual=Vout_meas*pii/2.0;
							PI_Vout.out_1=500;
//							printf("Vo ref actual: %f \n",Vo_ref.actual);
//							printf("ACT PI Vout: %f \n",VoPI_act);
							//Vo_ref.final=Vout_meas*pii/2.0;
							//VoPI_act = out_openloop * 2.0;

//							control_vo=1; //Todo ACTIVAR ESO
//							vars.enable.PR_z = 1;
//							control_inner=1;

							activate_notch_z = 1;	//Activate Notch circulante
							printf("\tSM: steady state\n");

						}
						break;

					case STEADY_STATE:
						 //cambio a PI ARM para VO
						if(vc_fixed==1)
						{
							Vc_ref = Vc_ref_aux;
						}
						else
						{
							Vc_ref = vref_env_amp; //Cambiar al valor entragado por la fpga
						}

						if(reset_control == 1)
						{
							start_control = 0;
							MMC_State_Machine = INIT;
							printf("RESET CONTROL\n");
						}
						if(enable_control == 0)	//trip occurs
						{
							start_control = 0;
							counter_state_machine = 0;    // reset counter to measure time
							MMC_State_Machine = INIT;
							printf("\tSM: INIT, due to system trip\n");
						}
						break;

					case DISCHARGE:
						//Hacer nada
						if(reset_control == 1)
						{
							start_control = 0;
							MMC_State_Machine = INIT;
							printf("RESET CONTROL\n");
						}
						control_vo=2; //activar salida open loop
						out_openloop=620; //valor inicial ref salida.	//Todo Eliminar esta variable
						vars.ref.v0_sigma_peak = Vc_refH;//100.0;
						enable_control = 1;
						break;

					default:
						MMC_State_Machine = INIT;
						printf("\tdefault SM. Go back to INIT\n");
						break;
				}
		counter_state_machine++;


		/*--------------------------------------------------------------------------------------*/
		/* Calculate voltajes de cluster															*/
		/*--------------------------------------------------------------------------------------*/
		g.control.meas.vCapacitorsABCPN.ap = g.control.meas.vCapacitorsDetailedABCPN.ap1 + g.control.meas.vCapacitorsDetailedABCPN.ap2 + g.control.meas.vCapacitorsDetailedABCPN.ap3
												+ g.control.meas.vCapacitorsDetailedABCPN.ap4 + g.control.meas.vCapacitorsDetailedABCPN.ap5 + g.control.meas.vCapacitorsDetailedABCPN.ap6;
		g.control.meas.vCapacitorsABCPN.bp = g.control.meas.vCapacitorsDetailedABCPN.bp1 + g.control.meas.vCapacitorsDetailedABCPN.bp2 + g.control.meas.vCapacitorsDetailedABCPN.bp3
												+ g.control.meas.vCapacitorsDetailedABCPN.bp4 + g.control.meas.vCapacitorsDetailedABCPN.bp5 + g.control.meas.vCapacitorsDetailedABCPN.bp6;
		g.control.meas.vCapacitorsABCPN.cp = g.control.meas.vCapacitorsDetailedABCPN.cp1 + g.control.meas.vCapacitorsDetailedABCPN.cp2 + g.control.meas.vCapacitorsDetailedABCPN.cp3
												+ g.control.meas.vCapacitorsDetailedABCPN.cp4 + g.control.meas.vCapacitorsDetailedABCPN.cp5 + g.control.meas.vCapacitorsDetailedABCPN.cp6;
		g.control.meas.vCapacitorsABCPN.an = g.control.meas.vCapacitorsDetailedABCPN.an1 + g.control.meas.vCapacitorsDetailedABCPN.an2 + g.control.meas.vCapacitorsDetailedABCPN.an3
												+ g.control.meas.vCapacitorsDetailedABCPN.an4 + g.control.meas.vCapacitorsDetailedABCPN.an5 + g.control.meas.vCapacitorsDetailedABCPN.an6;
		g.control.meas.vCapacitorsABCPN.bn = g.control.meas.vCapacitorsDetailedABCPN.bn1 + g.control.meas.vCapacitorsDetailedABCPN.bn2 + g.control.meas.vCapacitorsDetailedABCPN.bn3
												+ g.control.meas.vCapacitorsDetailedABCPN.bn4 + g.control.meas.vCapacitorsDetailedABCPN.bn5 + g.control.meas.vCapacitorsDetailedABCPN.bn6;
		g.control.meas.vCapacitorsABCPN.cn = g.control.meas.vCapacitorsDetailedABCPN.cn1 + g.control.meas.vCapacitorsDetailedABCPN.cn2 + g.control.meas.vCapacitorsDetailedABCPN.cn3
												+ g.control.meas.vCapacitorsDetailedABCPN.cn4 + g.control.meas.vCapacitorsDetailedABCPN.cn5 + g.control.meas.vCapacitorsDetailedABCPN.cn6;

//		//n = 3
//		g.control.meas.vCapacitorsABCPN.ap = g.control.meas.vCapacitorsDetailedABCPN.ap1 + g.control.meas.vCapacitorsDetailedABCPN.ap2 + g.control.meas.vCapacitorsDetailedABCPN.ap3;
//		g.control.meas.vCapacitorsABCPN.bp = g.control.meas.vCapacitorsDetailedABCPN.bp1 + g.control.meas.vCapacitorsDetailedABCPN.bp2 + g.control.meas.vCapacitorsDetailedABCPN.bp3;
//		g.control.meas.vCapacitorsABCPN.cp = g.control.meas.vCapacitorsDetailedABCPN.cp1 + g.control.meas.vCapacitorsDetailedABCPN.cp2 + g.control.meas.vCapacitorsDetailedABCPN.cp3;
//		g.control.meas.vCapacitorsABCPN.an = g.control.meas.vCapacitorsDetailedABCPN.an1 + g.control.meas.vCapacitorsDetailedABCPN.an2 + g.control.meas.vCapacitorsDetailedABCPN.an3;
//		g.control.meas.vCapacitorsABCPN.bn = g.control.meas.vCapacitorsDetailedABCPN.bn1 + g.control.meas.vCapacitorsDetailedABCPN.bn2 + g.control.meas.vCapacitorsDetailedABCPN.bn3;
//		g.control.meas.vCapacitorsABCPN.cn = g.control.meas.vCapacitorsDetailedABCPN.cn1 + g.control.meas.vCapacitorsDetailedABCPN.cn2 + g.control.meas.vCapacitorsDetailedABCPN.cn3;

		/*--------------------------------------------------------------------------------------*/
		/* Calculo de doble transformada para voltajes de condensadores de cluster				*/
		/*--------------------------------------------------------------------------------------*/
		Transform_ABCPN2AB0SD(g.control.meas.vCapacitorsABCPN, &g.control.meas.vCapacitorsAB0SD);

		gen_filter(&low_pass_Vc_alpha_S, g.control.meas.vCapacitorsAB0SD.alphaSigma,	&Vc_transf_prefilt.al_S);
		gen_filter(&low_pass_Vc_beta_S, g.control.meas.vCapacitorsAB0SD.betaSigma, 		&Vc_transf_prefilt.beta_S);
		gen_filter(&low_pass_Vc_cero_S,	g.control.meas.vCapacitorsAB0SD.ceroSigma, 		&Vc_transf_prefilt.cero_S);
		gen_filter(&low_pass_Vc_alpha_D, g.control.meas.vCapacitorsAB0SD.alphaDelta,   	&Vc_transf_prefilt.al_D);
		gen_filter(&low_pass_Vc_beta_D, g.control.meas.vCapacitorsAB0SD.betaDelta, 		&Vc_transf_prefilt.beta_D);
		gen_filter(&low_pass_Vc_cero_D,	g.control.meas.vCapacitorsAB0SD.ceroDelta, 		&Vc_transf_prefilt.cero_D);

		if(activate_notch_Vc_transf == 1)
		{
			g.control.measFiltered.vCapacitorsAB0SD.alphaSigma	= Vc_transf_prefilt.al_S;
			g.control.measFiltered.vCapacitorsAB0SD.betaSigma	= Vc_transf_prefilt.beta_S;
			g.control.measFiltered.vCapacitorsAB0SD.ceroSigma	= Vc_transf_prefilt.cero_S;
			g.control.measFiltered.vCapacitorsAB0SD.alphaDelta	= Vc_transf_prefilt.al_D;
			g.control.measFiltered.vCapacitorsAB0SD.betaDelta	= Vc_transf_prefilt.beta_D;
			g.control.measFiltered.vCapacitorsAB0SD.ceroDelta	= Vc_transf_prefilt.cero_D;

//			g.control.measFiltered.vCapacitorsAB0SD.alphaSigma	= g.control.meas.vCapacitorsAB0SD.alphaSigma;
//			g.control.measFiltered.vCapacitorsAB0SD.betaSigma	= g.control.meas.vCapacitorsAB0SD.betaSigma;
//			g.control.measFiltered.vCapacitorsAB0SD.ceroSigma	= g.control.meas.vCapacitorsAB0SD.ceroSigma;
//			g.control.measFiltered.vCapacitorsAB0SD.alphaDelta	= g.control.meas.vCapacitorsAB0SD.alphaDelta;
//			g.control.measFiltered.vCapacitorsAB0SD.betaDelta	= g.control.meas.vCapacitorsAB0SD.betaDelta;
//			g.control.measFiltered.vCapacitorsAB0SD.ceroDelta	= g.control.meas.vCapacitorsAB0SD.ceroDelta;
		}
		else
		{
			g.control.measFiltered.vCapacitorsAB0SD.alphaSigma	= g.control.meas.vCapacitorsAB0SD.alphaSigma;
			g.control.measFiltered.vCapacitorsAB0SD.betaSigma	= g.control.meas.vCapacitorsAB0SD.betaSigma;
			g.control.measFiltered.vCapacitorsAB0SD.ceroSigma	= g.control.meas.vCapacitorsAB0SD.ceroSigma;
			g.control.measFiltered.vCapacitorsAB0SD.alphaDelta	= g.control.meas.vCapacitorsAB0SD.alphaDelta;
			g.control.measFiltered.vCapacitorsAB0SD.betaDelta	= g.control.meas.vCapacitorsAB0SD.betaDelta;
			g.control.measFiltered.vCapacitorsAB0SD.ceroDelta	= g.control.meas.vCapacitorsAB0SD.ceroDelta;
		}
		// End Read cell capacitor voltages from FPGA	 -------------------------------------------//

		/*--------------------------------------------------------------------------------------*/
		/* Calculo de transformadas para corrientes del M2C										*/
		/*--------------------------------------------------------------------------------------*/
		abc2albe(vars.meas.ia, vars.meas.ib, vars.meas.ic, &vars.meas.ial_delta, &vars.meas.ibe_delta);
		Transform_ABC2Clarke(g.control.meas.iGridABC, &g.control.meas.iGridClarke);
		Transform_ABCPN2AB0SD(g.control.meas.iClusterABCPN, &g.control.meas.iClustersAB0SD);

		Calculate_filter_notch(&notch_s[0], vars.meas.ial_delta, &vars.meas_filt.ial_delta);
		Calculate_filter_notch(&notch_s[1], vars.meas.ibe_delta, &vars.meas_filt.ibe_delta);
		vars.meas_filt.ial_delta = vars.meas.ial_delta - vars.meas_filt.ial_delta;
		vars.meas_filt.ibe_delta = vars.meas.ibe_delta - vars.meas_filt.ibe_delta;

		g.control.measFiltered.iGridClarke.alpha = CalculateFilterNotchBandPass(&g.filters.notchIAlpha, g.control.meas.iGridClarke.alpha);
		g.control.measFiltered.iGridClarke.beta = CalculateFilterNotchBandPass(&g.filters.notchIBeta, g.control.meas.iGridClarke.beta);

		g.control.measFiltered.iClustersAB0SD.alphaSigma = CalculateFilterNotchBandPass(&g.filters.notchIAlphaSigma, g.control.meas.iClustersAB0SD.alphaSigma);
		g.control.measFiltered.iClustersAB0SD.betaSigma = CalculateFilterNotchBandPass(&g.filters.notchIBetaSigma, g.control.meas.iClustersAB0SD.betaSigma);
		g.control.measFiltered.iClustersAB0SD.alphaDelta = CalculateFilterNotchBandPass(&g.filters.notchIAlphaDelta, g.control.meas.iClustersAB0SD.alphaDelta);
		g.control.measFiltered.iClustersAB0SD.betaDelta = CalculateFilterNotchBandPass(&g.filters.notchIBetaDelta, g.control.meas.iClustersAB0SD.betaDelta);


		Calculate_filter_notch(&notch_Iap, vars.meas.iap, &vars.meas_filt.iap);
		Calculate_filter_notch(&notch_Ibp, vars.meas.ibp, &vars.meas_filt.ibp);
		Calculate_filter_notch(&notch_Icp, vars.meas.icp, &vars.meas_filt.icp);
		Calculate_filter_notch(&notch_Ian, vars.meas.ian, &vars.meas_filt.ian);
		Calculate_filter_notch(&notch_Ibn, vars.meas.ibn, &vars.meas_filt.ibn);
		Calculate_filter_notch(&notch_Icn, vars.meas.icn, &vars.meas_filt.icn);

		vars.meas_filt.iap = vars.meas.iap - vars.meas_filt.iap;
		vars.meas_filt.ibp = vars.meas.ibp - vars.meas_filt.ibp;
		vars.meas_filt.icp = vars.meas.icp - vars.meas_filt.icp;
		vars.meas_filt.ian = vars.meas.ian - vars.meas_filt.ian;
		vars.meas_filt.ibn = vars.meas.ibn - vars.meas_filt.ibn;
		vars.meas_filt.icn = vars.meas.icn - vars.meas_filt.icn;

		g.control.measFiltered.iClusterABCPN.ap = CalculateFilterNotchBandPass(&g.filters.notchIap, g.control.meas.iClusterABCPN.ap);
		g.control.measFiltered.iClusterABCPN.bp = CalculateFilterNotchBandPass(&g.filters.notchIbp, g.control.meas.iClusterABCPN.bp);
		g.control.measFiltered.iClusterABCPN.cp = CalculateFilterNotchBandPass(&g.filters.notchIcp, g.control.meas.iClusterABCPN.cp);
		g.control.measFiltered.iClusterABCPN.an = CalculateFilterNotchBandPass(&g.filters.notchIan, g.control.meas.iClusterABCPN.an);
		g.control.measFiltered.iClusterABCPN.bn = CalculateFilterNotchBandPass(&g.filters.notchIbn, g.control.meas.iClusterABCPN.bn);
		g.control.measFiltered.iClusterABCPN.cn = CalculateFilterNotchBandPass(&g.filters.notchIcn, g.control.meas.iClusterABCPN.cn);


		/*--------------------------------------------------------------------------------------*/
		/* Sorting voltages de cluster														*/
		/*--------------------------------------------------------------------------------------*/

		//crear array de valores
		memcpy(&Vcap_sort, &g.control.meas.vCapacitorsDetailedABCPN.ap1, sizeof(Vcap_sort));
		memcpy(&Vcbp_sort, &g.control.meas.vCapacitorsDetailedABCPN.bp1, sizeof(Vcbp_sort));
		memcpy(&Vccp_sort, &g.control.meas.vCapacitorsDetailedABCPN.cp1, sizeof(Vccp_sort));
		memcpy(&Vcan_sort, &g.control.meas.vCapacitorsDetailedABCPN.an1, sizeof(Vcan_sort));
		memcpy(&Vcbn_sort, &g.control.meas.vCapacitorsDetailedABCPN.bn1, sizeof(Vcbn_sort));
		memcpy(&Vccn_sort, &g.control.meas.vCapacitorsDetailedABCPN.cn1, sizeof(Vccn_sort));

		//sort 6 cluster
		sorting(Vcap_sort, pos_ap, 6, vars.meas_filt.iap);
		sorting(Vcbp_sort, pos_bp, 6, vars.meas_filt.ibp);
		sorting(Vccp_sort, pos_cp, 6, vars.meas_filt.icp);
		sorting(Vcan_sort, pos_an, 6, vars.meas_filt.ian);
		sorting(Vcbn_sort, pos_bn, 6, vars.meas_filt.ibn);
		sorting(Vccn_sort, pos_cn, 6, vars.meas_filt.icn);

//		sorting(Vcap_sort, pos_ap, 3, g.control.measFiltered.iClusterABCPN.ap);
//		sorting(Vcbp_sort, pos_bp, 3, g.control.measFiltered.iClusterABCPN.bp);
//		sorting(Vccp_sort, pos_cp, 3, g.control.measFiltered.iClusterABCPN.cp);
//		sorting(Vcan_sort, pos_an, 3, g.control.measFiltered.iClusterABCPN.an);
//		sorting(Vcbn_sort, pos_bn, 3, g.control.measFiltered.iClusterABCPN.bn);
//		sorting(Vccn_sort, pos_cn, 3, g.control.measFiltered.iClusterABCPN.cn);

		//concatenar posiciones
		u32 positions_ap, positions_bp, positions_cp, positions_an, positions_bn, positions_cn;
		positions_ap = (pos_ap[5]<<20) | (pos_ap[4]<<16) | (pos_ap[3]<<12) | (pos_ap[2]<<8) | (pos_ap[1]<<4) | pos_ap[0];
		positions_bp = (pos_bp[5]<<20) | (pos_bp[4]<<16) | (pos_bp[3]<<12) | (pos_bp[2]<<8) | (pos_bp[1]<<4) | pos_bp[0];
		positions_cp = (pos_cp[5]<<20) | (pos_cp[4]<<16) | (pos_cp[3]<<12) | (pos_cp[2]<<8) | (pos_cp[1]<<4) | pos_cp[0];
		positions_an = (pos_an[5]<<20) | (pos_an[4]<<16) | (pos_an[3]<<12) | (pos_an[2]<<8) | (pos_an[1]<<4) | pos_an[0];
		positions_bn = (pos_bn[5]<<20) | (pos_bn[4]<<16) | (pos_bn[3]<<12) | (pos_bn[2]<<8) | (pos_bn[1]<<4) | pos_bn[0];
		positions_cn = (pos_cn[5]<<20) | (pos_cn[4]<<16) | (pos_cn[3]<<12) | (pos_cn[2]<<8) | (pos_cn[1]<<4) | pos_cn[0];

//		// n = 3
//		positions_ap = (6<<20) | (6<<16) | (6<<12) | (pos_ap[2]<<8) | (pos_ap[1]<<4) | pos_ap[0];
//		positions_bp = (6<<20) | (6<<16) | (6<<12) | (pos_bp[2]<<8) | (pos_bp[1]<<4) | pos_bp[0];
//		positions_cp = (6<<20) | (6<<16) | (6<<12) | (pos_cp[2]<<8) | (pos_cp[1]<<4) | pos_cp[0];
//		positions_an = (6<<20) | (6<<16) | (6<<12) | (pos_an[2]<<8) | (pos_an[1]<<4) | pos_an[0];
//		positions_bn = (6<<20) | (6<<16) | (6<<12) | (pos_bn[2]<<8) | (pos_bn[1]<<4) | pos_bn[0];
//		positions_cn = (6<<20) | (6<<16) | (6<<12) | (pos_cn[2]<<8) | (pos_cn[1]<<4) | pos_cn[0];

		//send to NLC IP-core
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + pap_Data_M2C_NLC_6cell_Control_Vo, positions_ap);
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + pbp_Data_M2C_NLC_6cell_Control_Vo, positions_bp);
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + pcp_Data_M2C_NLC_6cell_Control_Vo, positions_cp);
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + pan_Data_M2C_NLC_6cell_Control_Vo, positions_an);
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + pbn_Data_M2C_NLC_6cell_Control_Vo, positions_bn);
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + pcn_Data_M2C_NLC_6cell_Control_Vo, positions_cn);

		/*--------------------------------------------------------------------------------------*/
		/* calculo envolvente para VCREF														*/
		/*--------------------------------------------------------------------------------------*/
		double envolventeVcRef;
		envolventeVcRef = (0.5 * sqrt((g.control.ref.vClustersAB0SD.alphaDelta * g.control.ref.vClustersAB0SD.alphaDelta)
							+ (g.control.ref.vClustersAB0SD.betaDelta * g.control.ref.vClustersAB0SD.betaDelta)))
							+ (out_openloop);

		/*--------------------------------------------------------------------------------------*/
		/* Envolvente para setpoint de Vref														*/
		/*--------------------------------------------------------------------------------------*/
		if(freq_chang_st==1)
		{
			vref_env_r = envolventeVcRef;
		}
		else
		{
			vref_env_r=Vc_ref_aux2;
		}

		gen_filter(&low_pass_vc_envelope,vref_env_r,&vref_env);
		//vref_env_amp = 1.05*vref_env*amp_factor;
		vref_env_amp = vref_env*amp_factor;


		/*--------------------------------------------------------------------------------------*/
		/* MMC control (Balance and currents)															*/
		/*--------------------------------------------------------------------------------------*/
		if(enable_control && start_control && !data.trip.general)
		{
			counter_control++;

			//Inicio de control de voltaje
			if(Vc_ref_1 != Vc_ref)
			{
				init_PI_voltage();
				//init_PI_Vout();
				Vc_ref_1 = Vc_ref;
			}

			//Inicio del control de voltajes
			Control_voltage();

			//-----------------------------------------------------------------------------
			// Bypass the control_voltage
			g.control.ref.iClustersDelta.parkPositive.d = id_plus;
			g.control.ref.iClustersDelta.parkNegative.d = id_min;
			g.control.ref.iClustersDelta.parkNegative.q = iq_min;

			g.control.ref.iClustersSigma.parkPositive.d = id_sigma_plus_s;
			g.control.ref.iClustersSigma.parkNegative.d = id_sigma_min_s;
			g.control.ref.iClustersSigma.parkNegative.q = iq_sigma_min_s;

			// Transform dq into alpha beta
			Transform_Park2Clarke(g.control.ref.iClustersDelta.parkPositive, &g.control.ref.iClustersDelta.clarkePositive, g.control.meas.thetaGrid);
			Transform_Park2Clarke(g.control.ref.iClustersDelta.parkNegative, &g.control.ref.iClustersDelta.clarkeNegative, -g.control.meas.thetaGrid);

			Transform_Park2Clarke(g.control.ref.iClustersSigma.parkPositive, &g.control.ref.iClustersSigma.clarkePositive, g.control.meas.thetaGrid);
			Transform_Park2Clarke(g.control.ref.iClustersSigma.parkNegative, &g.control.ref.iClustersSigma.clarkeNegative, -g.control.meas.thetaGrid);


			//Sum alpha beta
			g.control.ref.iClustersAB0SD.alphaDelta = g.control.ref.iClustersDelta.clarkePositive.alpha + g.control.ref.iClustersDelta.clarkeNegative.alpha;
			g.control.ref.iClustersAB0SD.betaDelta = g.control.ref.iClustersDelta.clarkePositive.beta + g.control.ref.iClustersDelta.clarkeNegative.beta;
			g.control.ref.iClustersAB0SD.alphaSigma = g.control.ref.iClustersSigma.clarkePositive.alpha + g.control.ref.iClustersSigma.clarkeNegative.alpha;
			g.control.ref.iClustersAB0SD.betaSigma = g.control.ref.iClustersSigma.clarkePositive.beta + g.control.ref.iClustersSigma.clarkeNegative.beta;

			//Io ref
			vars.ref.i0_sigma = 0.0;

			//Estados para activar PR en vivado
			if(vars.enable.PR_s == 1)
			{
				if(init_pi==1)
				{
					control_promedio=1;
					init_PI_voltage();
					init_PI_Vout();
					init_pi=0;
				}
			}
			else
			{
				control_promedio=0;
				reset_PI();
				init_pi=1;
			}

			if(vars.enable.PR_s == 1) //Control ARM
			{
				//Currents PR
				PR_s[0].FF = 0.0;//2*vars.meas.Val_grid;
				PR_s[1].FF = 0.0;//2*vars.meas.Vbe_grid;

				//printf("\nid:%0.3f",id_plus)

				float error_sa, error_sb;

				if(activate_notch_s == 1)
				{
					error_sa = g.control.ref.iClustersAB0SD.alphaDelta - vars.meas_filt.ial_delta;
					error_sb = g.control.ref.iClustersAB0SD.betaDelta - vars.meas_filt.ibe_delta;
				}
				else
				{
					error_sa = g.control.ref.iClustersAB0SD.alphaDelta - vars.meas.ial_delta;
					error_sb = g.control.ref.iClustersAB0SD.betaDelta - vars.meas.ibe_delta;
//                  error_sa = g.control.ref.iClustersAB0SD.alphaDelta - g.control.meas.iClustersAB0SD.alphaDelta;
//                  error_sb = g.control.ref.iClustersAB0SD.betaDelta - g.control.meas.iClustersAB0SD.betaDelta;

				}
				//Currents PRs
				PR_tpw(&PR_s[0], error_sa , &vars.ref.val_delta);
				PR_tpw(&PR_s[1], error_sb , &vars.ref.vbe_delta);

				val_delta_temp = vars.ref.val_delta;
				vbe_delta_temp = vars.ref.vbe_delta;
				//FedForward
				//vars.ref.val_delta = vars.ref.val_delta + 2*vars.meas.Val_grid;
				//vars.ref.vbe_delta = vars.ref.vbe_delta + 2*vars.meas.Vbe_grid;
				vars.ref.val_delta = vars.ref.val_delta + 2*clean_Val;
				vars.ref.vbe_delta = vars.ref.vbe_delta + 2*clean_Vbe;

				g.control.ref.vClustersAB0SD.alphaDelta = vars.ref.val_delta;
				g.control.ref.vClustersAB0SD.betaDelta = vars.ref.vbe_delta;

				vars.ref.val_delta2 = CalculatePR(&PR_alphaSigma, error_sa);
				vars.ref.vbe_delta2 = CalculatePR(&PR_betaSigma, error_sb);

				val_delta_temp = vars.ref.val_delta2;

				vars.ref.val_delta2 = vars.ref.val_delta2 + 2*clean_Val;
				vars.ref.vbe_delta2 = vars.ref.vbe_delta2 + 2*clean_Vbe;

//				// SObreescribir PR float por el PR double
//				vars.ref.val_delta = vars.ref.val_delta2;
//				vars.ref.vbe_delta = vars.ref.vbe_delta2;

			}
			else
			{
				init_PR_tpw_s();				//Reset PR_s
				init_PR_double_s();
				vars.ref.val_delta = 0.0;
				vars.ref.vbe_delta = 0.0;
			}

			if(vars.enable.PR_z == 1) //Control ARM
			{
				float error_za, error_zb;

				if(activate_notch_z == 1)
				{
					error_za = g.control.ref.iClustersAB0SD.alphaSigma - g.control.measFiltered.iClustersAB0SD.alphaSigma;
					error_zb = g.control.ref.iClustersAB0SD.betaSigma - g.control.measFiltered.iClustersAB0SD.betaSigma;
				}
				else
				{
					error_za = g.control.ref.iClustersAB0SD.alphaSigma - g.control.meas.iClustersAB0SD.alphaSigma;
					error_zb = g.control.ref.iClustersAB0SD.betaSigma - g.control.meas.iClustersAB0SD.betaSigma;
				}
				//Currents PRz
				PR_tpw(&PR_z[0], error_za , &vars.ref.val_sigma);
				PR_tpw(&PR_z[1], error_zb , &vars.ref.vbe_sigma);
			}
			else
			{
				init_PR_tpw_z();				//Reset PR_z
				vars.ref.val_sigma = 0.0;
				vars.ref.vbe_sigma = 0.0;
			}

			//vars.ref.v0_sigma_peak = Vc_transf.cero_S;	//For discharge
			vars.ref.v0_sigma_peak = out_openloop;

			// Select between control PR from FPGA or ARM
			if(PR_control_FPGA == 1)
			{
				//Xil_Out32(XPAR_SINE_LUT_0_BASEADDR + A_Data_Sine_LUT, toFix(vars.ref.i0_sigma_peak,13)); //Reference for FPGA PR i0_Sigma not io
				Xil_Out32(XPAR_SINE_LUT_0_BASEADDR + A_Data_Sine_LUT, toFix(vars.ref.v0_sigma_peak,13)); //Reference for FPGA V0S
	//			Xil_Out32(XPAR_SINE_LUT_0_BASEADDR + A_Data_Sine_LUT, toFix(VoPI_act,13)); //Reference for FPGA V0S

				Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + V0Sigma_peak_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(vars.ref.v0_sigma_peak,13)); //Reference for FPGA V0S

				Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+activator_Data_PR_1MHz, 0x1);		// Activate PR
				Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_V0S_Data_M2C_NLC_6cell_Control_Vo, 0x1); //1 control FPGA: 0 control ARM
			}
			else
			{
				Xil_Out32(XPAR_SINE_LUT_0_BASEADDR + A_Data_Sine_LUT, toFix(0.0,13)); //Reference for FPGA PR i0_Sigma not io
				Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + V0Sigma_peak_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(0.0,13)); //Reference for FPGA V0S
				Xil_Out32(XPAR_PR_1MHZ_0_BASEADDR+activator_Data_PR_1MHz, 0x0);		// Deactivate PR
				Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_V0S_Data_M2C_NLC_6cell_Control_Vo, 0x0); //1 control FPGA: 0 control ARM
			}

//			if(vars.enable.PR_o == 1) //Control ARM
//			{
//				if(PR_control_FPGA == 1)
//				{
//					vars.ref.v0_sigma = 0.0;
//				}
//				else
//				{
//					PR_tpw(&PR_o[0], vars.ref.i0_sigma-vars.meas_filt.i0_sigma , &vars.ref.v0_sigma);	// with current filter
//				}
//			}
//			else
//			{
//				init_PR_tpw_o();				//Reset PR_o
//				init_PR_tpw_AXI_1MHz();
//				//vars.ref.v0_sigma = 0.0;
//				if(vars.enable.OpenLoop_o == 1)
//				{
//					vars.ref.v0_sigma = (cos((double)ang))*out_openloop;
//				}
//				else
//				{
//					vars.ref.v0_sigma = 0.0;
//				}
//
//			}

			/*--------------------------------------------------------------------------------------*/
			/* Control de voltaje de salida por envolvente											*/
			/*--------------------------------------------------------------------------------------*/
			status_slope_VO=slope_reference(&Vo_ref);
			Vout_ref=Vo_ref.actual;
			// Read the VOut magnitude calculated by the LPF

			Vout_meas = toFloat(Xil_In32(XPAR_VO_LPF_0_BASEADDR+out_filtered_Data_vo_LPF),12);
			Vout_meas = Vout_meas*pii/2.0;
			//error_vo = (Vout_ref-Vout_meas);
			error_vo = (vars.ref.v0_sigma_peak-Vout_meas);

			PI_discreto_SAT_form2(&PI_Vout,error_vo,&VoPI_act);
//
//			if(control_vo==1)
//			{// activar PI vout
//				out_openloop = VoPI_act*0.5;
//			}
//			else if(control_vo==2)
//			{// open loop (bypass PI)
//				out_openloop = out_openloop;
//			}
//			else
//			{// PI off
//				out_openloop=0;
//			}


			// NLC propuesto
//			if(selector_NLC == 0)
//			{
//				double ang_cm;
//				counter_cm++;
//				ang_cm = 2.0*PII*counter_o*500.0/frequency_interrupt;
//				vars.ref.v0_delta_peak = sqrt(vars.ref.val_delta*vars.ref.val_delta+vars.ref.vbe_delta*vars.ref.vbe_delta)/(3);
//				vars.ref.v0_delta = (cos((double)ang_cm))*vars.ref.v0_delta_peak;
//				vars.ref.v0_delta = 0.0;
//			}
//			else
//			{
				vars.ref.v0_delta = 0.0;
//			}

			//-----------------------------------------------------------------------------
			// Doble transformada inversa para referencias de voltajes de cluster
			g.control.ref.vClustersAB0SD.alphaSigma = -vars.ref.val_sigma;
			g.control.ref.vClustersAB0SD.betaSigma = -vars.ref.vbe_sigma;
			g.control.ref.vClustersAB0SD.ceroSigma = 0.0;//-vars.ref.v0_sigma;
			g.control.ref.vClustersAB0SD.alphaDelta = -vars.ref.val_delta;
			g.control.ref.vClustersAB0SD.betaDelta = -vars.ref.vbe_delta;
			g.control.ref.vClustersAB0SD.ceroDelta = -vars.ref.v0_delta;

			Transform_AB0SD2ABCPN(g.control.ref.vClustersAB0SD, &g.control.ref.vClustersABCPN);

			//-----------------------------------------------------------------------------
			// Enable FO signal
			vars.enable.FO_MMC = 1;
		}
		else //Control desactivado, enviar estados a valor inicial
		{
			start_PR=0;
			vars.enable.FO_MMC = 0;
			reset_PI();
			enable_control=0;
			init_pi=1;
		}
		// End MMC control (ARM and FPGA) ----------------------------------------------------------//

		/*--------------------------------------------------------------------------------------*/
		/* MMC Modulation																		*/
		/*--------------------------------------------------------------------------------------*/
//		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_NLC_Data_M2C_NLC_6cell_Control_Vo, selector_NLC);	// classic NLC //Todo descomentar. se coment� por el paper.

		/*--------------------------------------------------------------------------------------*/
		/* Send cell voltage references to NLC (FPGA)											*/
		/*--------------------------------------------------------------------------------------*/
//		Vc_refH = Vc_transf.cero_S; //Todo
		Vc_refH = g.control.meas.vCapacitorsAB0SD.ceroSigma;

		int Vc_ref_inv_fix;
		if(vcref_source==2)
		{
			Vc_ref_inv_fix = toFix(n/Vc_refH,17); //fixed by hand
		}
		else
		{
			Vc_ref_inv_fix = toFix(n/Vc_ref,17); //vcref=VCref/n from envelope calculation
		}
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vdc_inv_Data_M2C_NLC_6cell_Control_Vo, Vc_ref_inv_fix);		//envia vcref a bloqueNLC
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vdc_inv_prop_Data_M2C_NLC_6cell_Control_Vo, Vc_ref_inv_fix);	//envia vcref a bloqueNLC
		// End Send cell voltage references to NLC (FPGA) --------------------------------------//

		//Send voltages references to FPGA
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vap_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(g.control.ref.vClustersABCPN.ap, 13));	// Vap*
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vbp_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(g.control.ref.vClustersABCPN.bp, 13));	// Vbp*
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vcp_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(g.control.ref.vClustersABCPN.cp, 13));	// Vcp*
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Van_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(g.control.ref.vClustersABCPN.an, 13));	// Van*
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vbn_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(g.control.ref.vClustersABCPN.bn, 13));	// Vbn*
		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vcn_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(g.control.ref.vClustersABCPN.cn, 13));	// Vcn*

		/*--------------------------------------------------------------------------------------*/
		/* Generaci�n de rampa de frecuencia													*/
		/*--------------------------------------------------------------------------------------*/
//		//Send output frequency value.
//		freqOut_ref.final=frequency_Vout_khz;
//		freq_chang_st=slope_reference (&freqOut_ref);
//		frec_out_div = (int)(100000 / freqOut_ref.actual / 200);
//		actual_frequency=100000000/(frec_out_div*200);
//
//		if(freq_chang_st==1)
//		{
//			Vc_ref_aux2=Vc_ref;
//		}
//		else
//		{
//			Vc_ref=Vc_ref_aux2;
//		}
//
////		if(frec_out_div_ant != frec_out_div)
////		{
////		SAMPLE_CLOCK_50DUTY_mWriteReg(XPAR_SAMPLE_CLOCK_50DUTY_0_S_AXI_LITE_BASEADDR, SAMPLE_CLOCK_50DUTY_S_AXI_Lite_SLV_REG0_OFFSET, frec_out_div); //50->10kHz  10->50kHz//clock for single sine 100e6/2000/f_out
////		frec_out_div_ant = frec_out_div;
////		}
//
//

		if(frequency_Vout_khz != frequency_Vout_khz_1)
		{
			float temp_freq, temp_freq_inv;
			s32 temp_freq_integer;
			float temp_freq_z, temp_freq_z_inv;
			s32 temp_freq_z_integer;

			temp_freq = (2000.0/frequency_Vout_khz-1);	// IP_Core_sampling_freq / target_freq - 1
			temp_freq_integer = temp_freq;
			temp_freq_inv = 1.0/temp_freq;

			temp_freq_z = (3.0*2000.0/frequency_Vout_khz/23.0-1);	// IP_Core_sampling_freq / target_freq - 1	(3times slower frequency for circulating injection)
			temp_freq_z_integer = temp_freq_z;
			temp_freq_z_inv = 1.0/temp_freq_z;

			SAMPLE_CLOCK_50DUTY_mWriteReg(XPAR_SAMPLE_CLOCK_50DUTY_0_S_AXI_LITE_BASEADDR, SAMPLE_CLOCK_50DUTY_S_AXI_Lite_SLV_REG0_OFFSET, (u32)(100/frequency_Vout_khz)); //50->10kHz  10->50kHz//clock for single sine 100e6/1000/f_out
			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + limit_counter_wo_Data_M2C_NLC_6cell_Control_Vo, (u32)(temp_freq_integer)); //1 control FPGA: 0 control ARM
			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + limit_counter_wo_inv_Data_M2C_NLC_6cell_Control_Vo, toFix(temp_freq_inv,24)); //1 control FPGA: 0 control ARM
			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + limit_counter_wz_Data_M2C_NLC_6cell_Control_Vo, (u32)(temp_freq_z_integer)); //1 control FPGA: 0 control ARM
			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + limit_counter_wz_inv_Data_M2C_NLC_6cell_Control_Vo, toFix(temp_freq_z_inv,24)); //1 control FPGA: 0 control ARM

			//recalcule_PR_tpw_AXI_1MHz();
			init_PR_tpw_AXI_1MHz();
			frequency_Vout_khz_1 = frequency_Vout_khz;
		}



//		if(debug_tr==1)
//		{
//			//offline_data_parameters.enable=1;
//			//out_openloop = 0.0;
//			debug_tr=2;
//		}
//		else if(debug_tr<debug_tr_max && debug_tr>1)
//		{
//			debug_tr++;
//			//PR_control_FPGA = 1;
//		}
//		else if(debug_tr==debug_tr_max)
//		{
//			//out_openloop = 0.0;
//			//id_plus_ref = 0.2;
//			//id_sigma_plus_s_ref = 0.2;
//			//vars.ref.i0_sigma_peak = 0.2;
//
//			//vars.enable.PR_s = 1;
//			//vars.enable.PR_z = 1;
//			//vars.enable.PR_o = 1;
//
//			activate_notch_s = 1;
//
//			PR_control_FPGA = 0;
//			debug_tr++;
//		}
//		else if(debug_tr==debug_tr_max+5)
//		{
//			debug_tr=0;
//		}

		//Funcion de obtener los offset y enviarlos a los conversores bits to voltage/currents
		Offsets_function();


		/*--------------------------------------------------------------------------------------*/
		/* Activar puentes-H													*/
		/*--------------------------------------------------------------------------------------*/
		XGpio_DiscreteWrite(&Gpio_constant, GPIO_CHANNEL1, vars.enable.FO_MMC);		// Enable signal for H-Bridges

		/*--------------------------------------------------------------------------------------*/
		/* Offline data from ARM0 - Slow mode																*/
		/*--------------------------------------------------------------------------------------*/
/*		offline_data_parameters.status_time_sampled_slow_mode_ms = 1000 * offline_data_parameters.max_data_points_slow_mode / offline_data_parameters.sample_frec_slow_mode;

		if(offline_data_parameters.enable_slow_mode == 1)
        {
			//calcule how many samples should be skiped to meet debug sampling frequency
			offline_data_parameters.skip_samples_slow_mode = (int)(10000 / offline_data_parameters.sample_frec_slow_mode);

			if(counter_arm0_slow < offline_data_parameters.max_data_points_slow_mode)
			{
				if(offline_data_parameters.skip_samples_slow_mode == offline_data_counter_arm0_skip)
				{
					 if(offline_data_parameters.plot_arm0_ALL == 0)
					 {
						 //save current data
						 data_offline_arm0[counter_arm0_slow].variable[0]  = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[0]];
						 data_offline_arm0[counter_arm0_slow].variable[1]  = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[1]];
						 data_offline_arm0[counter_arm0_slow].variable[2]  = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[2]];
						 data_offline_arm0[counter_arm0_slow].variable[3]  = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[3]];
						 data_offline_arm0[counter_arm0_slow].variable[4]  = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[4]];
						 data_offline_arm0[counter_arm0_slow].variable[5]  = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[5]];
						 data_offline_arm0[counter_arm0_slow].variable[6]  = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[6]];
						 data_offline_arm0[counter_arm0_slow].variable[7]  = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[7]];
						 data_offline_arm0[counter_arm0_slow].variable[8]  = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[8]];
						 data_offline_arm0[counter_arm0_slow].variable[9]  = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[9]];
						 data_offline_arm0[counter_arm0_slow].variable[10] = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[10]];
						 data_offline_arm0[counter_arm0_slow].variable[11] = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[11]];
						 data_offline_arm0[counter_arm0_slow].variable[12] = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[12]];
						 data_offline_arm0[counter_arm0_slow].variable[13] = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[13]];
						 data_offline_arm0[counter_arm0_slow].variable[14] = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[14]];
						 data_offline_arm0[counter_arm0_slow].variable[15] = *data_offline_64var_arm0_ptr[offline_data_parameters.selector_arm0[15]];
					 }
					 else
					 {
						 for(int k=0; k<64; k++)
						 {
							 data_offline_arm0[counter_arm0_slow].variable[k]  = *data_offline_64var_arm0_ptr[k];
						 }
					 }
					 counter_arm0_slow ++;	// used for position in the data array
					 counter_arm0_points = counter_arm0_slow;
					 offline_data_counter_arm0_skip = 0;
				}
				//else	//do nothing
				//{
				//}
			}
			else	//all samples are stored
			{
				offline_data_parameters.finish_slow_mode = 1;
				offline_data_parameters.enable_slow_mode = 0;
			}

			offline_data_counter_arm0_skip ++;
		 }
		else
		{
			counter_arm0_slow = 0;
			offline_data_counter_arm0_skip = 0;
		}

 		// End Offline data from ARM0 - Slow mode ----------------------------------------------------------//
*/


/*			OCM_BareMetal2FreeRTOS.time_stamp = counter_interrupt; //arm1_time_stamp; //

			for(int i=0; i<16; i++)
			{
				//OCM_BareMetal2FreeRTOS.variable[i] = *debug_SoC_variables_arm0_ptr[debug_SoC_selector_arm0[i]];
				OCM_BareMetal2FreeRTOS.variable[i] = *debug_SoC_parameters_ARM0.variables_arm0_ptr[debug_SoC_parameters_ARM0.selector_arm0[i]];
				OCM_BareMetal2FreeRTOS.timer_ns = Timer_ns;
				OCM_BareMetal2FreeRTOS.timer_ns_max = Timer_ns_max;
				OCM_BareMetal2FreeRTOS.timer_ns_min = Timer_ns_min;
			}
			Xil_DCacheFlushRange(DEBUG_SOC_ONLINE_DATA_ARM0_MEMORY_ADDRESS, sizeof(OCM_BareMetal2FreeRTOS));
*/
			//Simulate the Interrupt on ARM1
			XScuGic_SoftwareIntr(&InterruptController, INTC_DEVICE_INT_ID, XSCUGIC_SPI_CPU1_MASK);


			//offline data
			//if(debug_SoC_parameters_ARM0.status_done == 0)		//Current version of IP-Core starts with done = 0
			//if(FPGA_TO_DDRRAM_mReadReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_ENABLE_OFFSET) != 0)
			//{
				u32 temp_var;
				temp_var = FPGA_TO_DDRRAM_mReadReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_STATUS_OFFSET);
				debug_SoC_parameters_ARM0.status_done = (temp_var >> 24) & 0xF;
				debug_SoC_parameters_ARM0.status_error = (temp_var >> 28) & 0xF;
				debug_SoC_parameters_ARM0.status_burst_count = temp_var & 0x1FFFF;

				debug_SoC_parameters_ARM0.active = OCM_FreeRTOS2BareMetal.activate_debugARM0;

				//verify done pin status
				if(debug_SoC_parameters_ARM0.status_done == 0 && debug_SoC_parameters_ARM0.status_error == 0 && debug_SoC_parameters_ARM0.active == 1)	//If not error and IP core still get data
				//if(debug_SoC_parameters_ARM0.status_error == 0)	//Add error status
				{
					//save burst counter to synchronize fpga and arm0 data
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].burst_counter = debug_SoC_parameters_ARM0.status_burst_count;

					//save current data
//					for(int i=0; i<64; i++)
//					{
//						//data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[i]  = *debug_SoC_parameters_ARM0.variables_arm0_ptr[debug_SoC_parameters_ARM0.selector_arm0[i]];	// selector 16 vars
//						data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[i]  = *debug_SoC_parameters_ARM0.variables_arm0_ptr[i];						 						// save all 64 vars
//					}
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[0]  = vars.meas.ial_delta;	//g.control.meas.vCapacitorsDetailedABCPN.ap1;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[1]  = vars.meas.ibe_delta;	//g.control.meas.vCapacitorsDetailedABCPN.ap2;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[2]  = vars.meas_filt.ial_delta;	//g.control.meas.vCapacitorsDetailedABCPN.ap3;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[3]  = vars.meas_filt.ibe_delta;	//g.control.meas.vCapacitorsDetailedABCPN.ap4;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[4]  = 0.0;	//g.control.meas.vCapacitorsDetailedABCPN.ap5;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[5]  = 0.0;	//Vc_transf_prefilt.cero_D;	//g.control.meas.vCapacitorsDetailedABCPN.ap6;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[6]  = g.control.meas.iGridClarke.alpha;	//g.control.meas.vCapacitorsDetailedABCPN.bp1;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[7]  = g.control.meas.iGridClarke.beta;	//g.control.meas.vCapacitorsDetailedABCPN.bp2;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[8]  = g.control.meas.iGridClarke.cero;	//g.control.meas.vCapacitorsDetailedABCPN.bp3;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[9]  = g.control.meas.iClustersAB0SD.alphaDelta;	//g.control.meas.vCapacitorsDetailedABCPN.bp4;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[10] = g.control.meas.iClustersAB0SD.betaDelta;	//g.control.meas.vCapacitorsDetailedABCPN.bp5;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[11] = g.control.meas.iClustersAB0SD.ceroDelta;	//g.control.meas.vCapacitorsDetailedABCPN.bp6;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[12] = g.control.measFiltered.iGridClarke.alpha;	//g.control.meas.vCapacitorsDetailedABCPN.cp1;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[13] = g.control.measFiltered.iGridClarke.beta;	//g.control.meas.vCapacitorsDetailedABCPN.cp2;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[14] = g.control.measFiltered.iClustersAB0SD.alphaDelta;	//g.control.meas.vCapacitorsDetailedABCPN.cp3;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[15] = g.control.measFiltered.iClustersAB0SD.betaDelta;	//g.control.meas.vCapacitorsDetailedABCPN.cp4;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[16] = vars.meas.ibn;//g.control.meas.vCapacitorsDetailedABCPN.cp5;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[17] = vars.meas.icn;//g.control.meas.vCapacitorsDetailedABCPN.cp6;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[18] = g.control.meas.iClusterABCPN.ap;	//g.control.meas.vCapacitorsDetailedABCPN.an1;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[19] = g.control.meas.iClusterABCPN.bp;//g.control.meas.vCapacitorsDetailedABCPN.an2;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[20] = g.control.meas.iClusterABCPN.cp;//g.control.meas.vCapacitorsDetailedABCPN.an3;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[21] = g.control.meas.iClusterABCPN.an;//g.control.meas.vCapacitorsDetailedABCPN.an4;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[22] = g.control.meas.iClusterABCPN.bn;//g.control.meas.vCapacitorsDetailedABCPN.an5;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[23] = g.control.meas.iClusterABCPN.cn;//g.control.meas.vCapacitorsDetailedABCPN.an6;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[24] = g.control.meas.vCapacitorsDetailedABCPN.bn1;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[25] = g.control.meas.vCapacitorsDetailedABCPN.bn2;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[26] = g.control.meas.vCapacitorsDetailedABCPN.bn3;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[27] = g.control.meas.vCapacitorsDetailedABCPN.bn4;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[28] = g.control.meas.vCapacitorsDetailedABCPN.bn5;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[29] = g.control.meas.vCapacitorsDetailedABCPN.bn6;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[30] = g.control.meas.vCapacitorsDetailedABCPN.cn1;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[31] = g.control.meas.vCapacitorsDetailedABCPN.cn2;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[32] = g.control.meas.vCapacitorsDetailedABCPN.cn3;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[33] = g.control.meas.vCapacitorsDetailedABCPN.cn4;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[34] = g.control.meas.vCapacitorsDetailedABCPN.cn5;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[35] = g.control.meas.vCapacitorsDetailedABCPN.cn6;

					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[36] = g.control.measFiltered.vCapacitorsAB0SD.alphaSigma;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[37] = g.control.measFiltered.vCapacitorsAB0SD.betaSigma;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[38] = g.control.measFiltered.vCapacitorsAB0SD.ceroSigma;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[39] = g.control.measFiltered.vCapacitorsAB0SD.alphaDelta;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[40] = g.control.measFiltered.vCapacitorsAB0SD.betaDelta;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[41] = g.control.measFiltered.vCapacitorsAB0SD.ceroDelta;

					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[42] = g.control.ref.iClustersDelta.parkPositive.d;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[43] = g.control.ref.iClustersDelta.parkNegative.d;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[44] = g.control.ref.iClustersDelta.parkNegative.q;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[45] = g.control.ref.iClustersSigma.parkPositive.d;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[46] = g.control.ref.iClustersSigma.parkNegative.d;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[47] = g.control.ref.iClustersSigma.parkNegative.q;

					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[48] = g.control.ref.iClustersAB0SD.alphaDelta;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[49] = g.control.ref.iClustersAB0SD.betaDelta;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[50] = g.control.ref.iClustersAB0SD.alphaSigma;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[51] = g.control.ref.iClustersAB0SD.betaSigma;

					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[52] = Vc_transf_prefilt.al_S;//g.control.meas.iGridABC.a;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[53] = Vc_transf_prefilt.cero_S;//;g.control.meas.iGridABC.b;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[54] = g.control.meas.iGridABC.c;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[55] = g.control.meas.thetaGrid;

					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[56] = vars.meas.ial_delta;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[57] = vars.meas.ibe_delta;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[58] = g.control.measFiltered.iClustersAB0SD.alphaSigma;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[59] = g.control.measFiltered.iClustersAB0SD.betaSigma;

					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[60] = vars.ref.val_sigma;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[61] = vars.ref.val_delta;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[62] = vars.ref.vbe_delta;
					data_offline_arm0[debug_SoC_parameters_ARM0.counter_arm0].variable[63] = vars.ref.vbe_sigma;

					debug_SoC_parameters_ARM0.counter_arm0 ++;
				}
			//}
			else
			{
				debug_SoC_parameters_ARM0.counter_arm0 = 0;
			}


		XGpioPs_WritePin(&Gpiops, GPIOps_pin_MIO7, 0x0);		// Debug Led for calculate interrupt time

//		Timer_end = XScuTimer_GetCounterValue(&Timer_PS);
//		Timer_diff = Timer_start - Timer_end;
//		Timer_ns = ((Timer_diff * 1.0) / COUNTS_PER_SECOND) * 1000000000.0;

//		if(Timer_ns_max < Timer_ns)
//		{
//			Timer_ns_max = Timer_ns;	// Update max value
//		}
//
//		if(Timer_ns_min > Timer_ns)
//		{
//			Timer_ns_min = Timer_ns;	// Update min value
//		}
//
//		if(Timer_ns > 90000.0)
//		{
//			printf("time_problem\n");
//		}

		interrupt_flag = 0;

	}
	else
	{
		/*
		 * The Interval event should be the only one enabled. If it is
		 * not it is an error
		 */
		ErrorCount++;
	}
}


void TimerCounterHandler(void *CallBackRef, u8 TmrCtrNumber)		// Timer interrupt routine
{
	XTmrCtr *InstancePtr = (XTmrCtr *)CallBackRef;
	/*
	 * Check if the timer counter has expired, checking is not necessary
	 * since that's the reason this function is executed, this just shows
	 * how the callback reference can be used as a pointer to the instance
	 * of the timer counter that expired.
	 */
	if (XTmrCtr_IsExpired(InstancePtr, TmrCtrNumber))
	{
	}
}

int Optical_Fibers_init(void)
{
	int delay = 5000000;//10000000;
	// FO 1
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0xfffffff);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111111111111111110);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111111111111111101);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111111111111111011);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111111111111110111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111111111111101111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111111111111011111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111111111110111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111111111101111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111111111011111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111111110111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111111101111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111111011111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111110111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111101111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111111011111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111110111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111101111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111111011111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111110111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111101111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111111011111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111110111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111101111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111111011111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111110111111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0b11111101111111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0xffffffff);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0xfffffff0);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0xffffff00);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0xfffff000);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0xffff0000);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0xfff00000);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0xff000000);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0xf0000000);	for(int i=0; i<delay; i++);
	//XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL1, 0xffffffff);

	// FO 2
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0xfffffff);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111111111111111110);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111111111111111101);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111111111111111011);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111111111111110111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111111111111101111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111111111111011111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111111111110111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111111111101111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111111111011111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111111110111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111111101111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111111011111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111110111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111101111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111111011111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111110111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111101111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111111011111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111110111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111101111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111111011111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111110111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111101111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111111011111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111110111111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0b11111101111111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0xffffffff);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0xfffffff0);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0xffffff00);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0xfffff000);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0xffff0000);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0xfff00000);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0xff000000);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0xf0000000);	for(int i=0; i<delay; i++);
	//XGpio_DiscreteWrite(&Gpio_FO12, GPIO_CHANNEL2, 0xffffffff);

	// FO 3
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0x0fffffff);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111111111111111110);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111111111111111101);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111111111111111011);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111111111111110111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111111111111101111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111111111111011111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111111111110111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111111111101111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111111111011111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111111110111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111111101111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111111011111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111110111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111101111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111111011111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111110111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111101111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111111011111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111110111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111101111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111111011111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111110111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111101111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111111011111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111110111111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0b01111101111111111111111111111111);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0x0fffffff);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0x0ffffff0);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0x0fffff00);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0x0ffff000);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0x0fff0000);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0x0ff00000);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0x0f000000);	for(int i=0; i<delay; i++);
	XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0x00000000);	for(int i=0; i<delay; i++);
	//XGpio_DiscreteWrite(&Gpio_FO3, GPIO_CHANNEL1, 0x0fffffff);	for(int i=0; i<delay; i++);

	return XST_SUCCESS;
}
