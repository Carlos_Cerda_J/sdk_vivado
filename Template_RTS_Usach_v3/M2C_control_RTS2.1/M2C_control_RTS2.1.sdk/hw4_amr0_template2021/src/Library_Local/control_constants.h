/*
 * control_constants.h
 *
 *  Created on: 19-03-2019
 *      Author: Victor
 */

#ifndef SRC_CONTROL_CONSTANTS_H_
#define SRC_CONTROL_CONSTANTS_H_

#define PII		3.14159265359
const float pii = 3.14159265359;

float V_grid = 220.0*sqrt(2.0);

float Vc_ref=250.0;
float Vc_ref_1=0;
float Vc_ref_aux = 300;
float Vc_ref_aux2 = 300;
int vc_fixed=0;

int vcref_source = 1;
int Vc_refH = 450;

float L=0.0004;
float Lacop=0.0004*0.02;	//Updated 15-01-2021
float h=0.00002;
float C = 0.0009;
float Lac = 0.0025;
float w_load=2.0*3.1416*1000.0;
float w_grid=2.0*3.1416*50.0;
float Vo=30.0;
float n=3.0;

double Ts_arm = 0.0001;


//Anchos de banda PR de entrada s y circulantes z
float wn_s = 400.0*2.0*PII;//2000.0*2.0*PII;//800.0*2.0*PII;	//Todo volver a los 400Hz
float wn_z = 400.0*2.0*PII;// 2000.0*2.0*PII;//1100.0*2.0*PII;
float wn_o = 1600.0*2.0*PII;

//Factor de amortiguamiento PR de entrada s y circulantes z
float xi_s=0.707;//0.907;
float xi_z=0.707;//0.9;
float xi_o=0.707;//0.9;

//Anchos de banda control promedio b1, control inner b2 y control intra b3
float wn_b1 = 1*2.0*PII;
float wn_b2 = 1*2.0*PII;
float wn_b3 = 1*2.0*PII;

//Factor de amortiguamiento control promedio b1, control inner b2 y control intra b3
float xi_b1=0.707;
float xi_b2=0.707;
float xi_b3=0.707;

float id_max = 7.0;

int control_intra=0;
int intra_al=1;
int intra_be=1;

int control_inner=0;
int init_pi=1;



//tuning PI VOUT
float Tpi=0.0001;
float kipi=1.0;
//float kppi=2.8974;
//float api=31.36;
float kppi=0.010103;//3.0947; antiguos
float api=7172;//27.0; antiguos
float azpi=0.0;
float kzpi=0.0;
float kpzpi=0.0;
float kizpi=0.0;
float v0s_max=3000.0;



#endif /* SRC_CONTROL_CONSTANTS_H_ */
