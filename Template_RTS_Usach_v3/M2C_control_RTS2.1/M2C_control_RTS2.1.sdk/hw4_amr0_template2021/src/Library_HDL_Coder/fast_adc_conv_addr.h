/*
 * File Name:         D:\xilinx\IPCORES\fast_adc_conv\ipcore\fast_adc_conv_v1_0\include\fast_adc_conv_addr.h
 * Description:       C Header File
 * Created:           2019-01-20 15:31:22
*/

#ifndef FAST_ADC_CONV_H_
#define FAST_ADC_CONV_H_

#define  IPCore_Reset_fast_adc_conv       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_fast_adc_conv      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_fast_adc_conv   0x8  //contains unique IP timestamp (yymmddHHMM): 1901201531
#define  Gain1_Data_fast_adc_conv         0x100  //data register for Inport Gain1
#define  Offset1_Data_fast_adc_conv       0x104  //data register for Inport Offset1
#define  Gain2_Data_fast_adc_conv         0x108  //data register for Inport Gain2
#define  Offset2_Data_fast_adc_conv       0x10C  //data register for Inport Offset2
#define  Gain3_Data_fast_adc_conv         0x110  //data register for Inport Gain3
#define  Offset3_Data_fast_adc_conv       0x114  //data register for Inport Offset3
#define  Gain4_Data_fast_adc_conv         0x118  //data register for Inport Gain4
#define  Offset4_Data_fast_adc_conv       0x11C  //data register for Inport Offset4
#define  Gain5_Data_fast_adc_conv         0x120  //data register for Inport Gain5
#define  Offset5_Data_fast_adc_conv       0x124  //data register for Inport Offset5
#define  Gain6_Data_fast_adc_conv         0x128  //data register for Inport Gain6
#define  Offset6_Data_fast_adc_conv       0x12C  //data register for Inport Offset6
#define  Gain7_Data_fast_adc_conv         0x130  //data register for Inport Gain7
#define  Offset7_Data_fast_adc_conv       0x134  //data register for Inport Offset7
#define  Gain8_Data_fast_adc_conv         0x138  //data register for Inport Gain8
#define  Offset8_Data_fast_adc_conv       0x13C  //data register for Inport Offset8
#define  Gain9_Data_fast_adc_conv         0x140  //data register for Inport Gain9
#define  Offset9_Data_fast_adc_conv       0x144  //data register for Inport Offset9
#define  Gain10_Data_fast_adc_conv        0x148  //data register for Inport Gain10
#define  Offset10_Data_fast_adc_conv      0x14C  //data register for Inport Offset10

#endif /* FAST_ADC_CONV_H_ */
