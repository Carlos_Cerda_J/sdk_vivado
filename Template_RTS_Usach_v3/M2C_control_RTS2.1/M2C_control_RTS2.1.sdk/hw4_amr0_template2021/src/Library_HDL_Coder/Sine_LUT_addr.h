/*
 * File Name:         C:\IPCORES\SINE_LUT\ipcore\Sine_LUT_v1_0\include\Sine_LUT_addr.h
 * Description:       C Header File
 * Created:           2019-01-15 11:10:05
*/

#ifndef SINE_LUT_H_
#define SINE_LUT_H_

#define  IPCore_Reset_Sine_LUT       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_Sine_LUT      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_Sine_LUT   0x8  //contains unique IP timestamp (yymmddHHMM): 1901151110
#define  A_Data_Sine_LUT             0x104  //data register for Inport A

#endif /* SINE_LUT_H_ */
