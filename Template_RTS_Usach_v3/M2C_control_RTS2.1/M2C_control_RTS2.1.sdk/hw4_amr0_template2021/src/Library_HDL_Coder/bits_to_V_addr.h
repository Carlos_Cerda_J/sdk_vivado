/*
 * File Name:         C:\IPCORES\bits_to_V\ipcore\bits_to_V_v1_0\include\bits_to_V_addr.h
 * Description:       C Header File
 * Created:           2019-01-16 18:06:21
*/

#ifndef BITS_TO_V_H_
#define BITS_TO_V_H_

#define  IPCore_Reset_bits_to_V        0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_bits_to_V       0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_bits_to_V    0x8  //contains unique IP timestamp (yymmddHHMM): 1901161806
#define  gain_Data_bits_to_V           0x100  //data register for Inport gain
#define  offset_Data_bits_to_V         0x104  //data register for Inport offset
#define  gain1_Data_bits_to_V          0x108  //data register for Inport gain1
#define  offset1_Data_bits_to_V        0x10C  //data register for Inport offset1
#define  Salida_test_Data_bits_to_V    0x110  //data register for Outport Salida_test
#define  Salida_test1_Data_bits_to_V   0x114  //data register for Outport Salida_test1

#endif /* BITS_TO_V_H_ */
