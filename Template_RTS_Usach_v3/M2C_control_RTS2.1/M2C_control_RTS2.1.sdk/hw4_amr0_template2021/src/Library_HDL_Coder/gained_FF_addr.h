/*
 * File Name:         F:\sort_3_cell\gained_FF\ipcore\gained_FF_v1_0\include\gained_FF_addr.h
 * Description:       C Header File
 * Created:           2019-05-24 12:08:02
*/

#ifndef GAINED_FF_H_
#define GAINED_FF_H_

#define  IPCore_Reset_gained_FF       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_gained_FF      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_gained_FF   0x8  //contains unique IP timestamp (yymmddHHMM): 1905241208
#define  FF_factor1_Data_gained_FF    0x100  //data register for Inport FF_factor1
#define  FF_factor2_Data_gained_FF    0x104  //data register for Inport FF_factor2

#endif /* GAINED_FF_H_ */
