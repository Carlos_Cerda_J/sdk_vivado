/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\envelope_detector\to_ARM\ipcore\to_ARM_v1_0\include\to_ARM_addr.h
 * Description:       C Header File
 * Created:           2019-11-04 11:29:24
*/

#ifndef TO_ARM_H_
#define TO_ARM_H_

#define  IPCore_Reset_to_ARM       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_to_ARM      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_to_ARM   0x8  //contains unique IP timestamp (yymmddHHMM): 1911041129
#define  alpha_S_Data_to_ARM       0x100  //data register for Outport alpha_S
#define  alpha_D_Data_to_ARM       0x104  //data register for Outport alpha_D
#define  beta_S_Data_to_ARM        0x108  //data register for Outport beta_S
#define  beta_D_Data_to_ARM        0x10C  //data register for Outport beta_D

#endif /* TO_ARM_H_ */
