/*
 * File Name:         D:\xilinx\IPCORES\slow_adc_conv\ipcore\slow_adc_conv_v1_0\include\slow_adc_conv_addr.h
 * Description:       C Header File
 * Created:           2019-01-20 21:54:57
*/

#ifndef SLOW_ADC_CONV_H_
#define SLOW_ADC_CONV_H_

#define  IPCore_Reset_slow_adc_conv       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_slow_adc_conv      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_slow_adc_conv   0x8  //contains unique IP timestamp (yymmddHHMM): 1901202154
#define  Gain1_Data_slow_adc_conv         0x100  //data register for Inport Gain1
#define  Offset1_Data_slow_adc_conv       0x104  //data register for Inport Offset1
#define  Gain2_Data_slow_adc_conv         0x108  //data register for Inport Gain2
#define  Offset2_Data_slow_adc_conv       0x10C  //data register for Inport Offset2
#define  Gain3_Data_slow_adc_conv         0x110  //data register for Inport Gain3
#define  Offset3_Data_slow_adc_conv       0x114  //data register for Inport Offset3
#define  Gain4_Data_slow_adc_conv         0x118  //data register for Inport Gain4
#define  Offset4_Data_slow_adc_conv       0x11C  //data register for Inport Offset4
#define  Gain5_Data_slow_adc_conv         0x120  //data register for Inport Gain5
#define  Offset5_Data_slow_adc_conv       0x124  //data register for Inport Offset5
#define  Gain6_Data_slow_adc_conv         0x128  //data register for Inport Gain6
#define  Offset6_Data_slow_adc_conv       0x12C  //data register for Inport Offset6
#define  Gain7_Data_slow_adc_conv         0x130  //data register for Inport Gain7
#define  Offset7_Data_slow_adc_conv       0x134  //data register for Inport Offset7
#define  Gain8_Data_slow_adc_conv         0x138  //data register for Inport Gain8
#define  Offset8_Data_slow_adc_conv       0x13C  //data register for Inport Offset8
#define  Gain9_Data_slow_adc_conv         0x140  //data register for Inport Gain9
#define  Offset9_Data_slow_adc_conv       0x144  //data register for Inport Offset9
#define  Gain10_Data_slow_adc_conv        0x148  //data register for Inport Gain10
#define  Offset10_Data_slow_adc_conv      0x14C  //data register for Inport Offset10
#define  Gain11_Data_slow_adc_conv        0x150  //data register for Inport Gain11
#define  Offset11_Data_slow_adc_conv      0x154  //data register for Inport Offset11
#define  Gain12_Data_slow_adc_conv        0x158  //data register for Inport Gain12
#define  Offset12_Data_slow_adc_conv      0x15C  //data register for Inport Offset12
#define  Gain13_Data_slow_adc_conv        0x160  //data register for Inport Gain13
#define  Offset13_Data_slow_adc_conv      0x164  //data register for Inport Offset13
#define  Gain14_Data_slow_adc_conv        0x168  //data register for Inport Gain14
#define  Offset14_Data_slow_adc_conv      0x16C  //data register for Inport Offset14
#define  Gain15_Data_slow_adc_conv        0x170  //data register for Inport Gain15
#define  Offset15_Data_slow_adc_conv      0x174  //data register for Inport Offset15
#define  Gain16_Data_slow_adc_conv        0x178  //data register for Inport Gain16
#define  Offset16_Data_slow_adc_conv      0x17C  //data register for Inport Offset16
#define  Gain17_Data_slow_adc_conv        0x180  //data register for Inport Gain17
#define  Offset17_Data_slow_adc_conv      0x184  //data register for Inport Offset17
#define  Gain18_Data_slow_adc_conv        0x188  //data register for Inport Gain18
#define  Offset18_Data_slow_adc_conv      0x18C  //data register for Inport Offset18
#define  Gain19_Data_slow_adc_conv        0x190  //data register for Inport Gain19
#define  Offset19_Data_slow_adc_conv      0x194  //data register for Inport Offset19
#define  Gain20_Data_slow_adc_conv        0x198  //data register for Inport Gain20
#define  Offset20_Data_slow_adc_conv      0x19C  //data register for Inport Offset20
#define  Gain21_Data_slow_adc_conv        0x1A0  //data register for Inport Gain21
#define  Offset21_Data_slow_adc_conv      0x1A4  //data register for Inport Offset21
#define  out_1_Data_slow_adc_conv         0x1A8  //data register for Outport out_1
#define  out_2_Data_slow_adc_conv         0x1AC  //data register for Outport out_2
#define  out_3_Data_slow_adc_conv         0x1B0  //data register for Outport out_3
#define  out_4_Data_slow_adc_conv         0x1B4  //data register for Outport out_4
#define  out_5_Data_slow_adc_conv         0x1B8  //data register for Outport out_5
#define  out_6_Data_slow_adc_conv         0x1BC  //data register for Outport out_6
#define  out_7_Data_slow_adc_conv         0x1C0  //data register for Outport out_7
#define  out_8_Data_slow_adc_conv         0x1C4  //data register for Outport out_8
#define  out_9_Data_slow_adc_conv         0x1C8  //data register for Outport out_9
#define  out_10_Data_slow_adc_conv        0x1CC  //data register for Outport out_10
#define  out_11_Data_slow_adc_conv        0x1D0  //data register for Outport out_11
#define  out_12_Data_slow_adc_conv        0x1D4  //data register for Outport out_12
#define  out_13_Data_slow_adc_conv        0x1D8  //data register for Outport out_13
#define  out_14_Data_slow_adc_conv        0x1DC  //data register for Outport out_14
#define  out_15_Data_slow_adc_conv        0x1E0  //data register for Outport out_15
#define  out_16_Data_slow_adc_conv        0x1E4  //data register for Outport out_16
#define  out_17_Data_slow_adc_conv        0x1E8  //data register for Outport out_17
#define  out_18_Data_slow_adc_conv        0x1EC  //data register for Outport out_18
#define  out_19_Data_slow_adc_conv        0x1F0  //data register for Outport out_19
#define  out_20_Data_slow_adc_conv        0x1F4  //data register for Outport out_20
#define  out_21_Data_slow_adc_conv        0x1F8  //data register for Outport out_21

#endif /* SLOW_ADC_CONV_H_ */
