/*
 * File Name:         hdl_prj\ipcore\NLC_M2C_1cell_v1_0\include\NLC_M2C_1cell_addr.h
 * Description:       C Header File
 * Created:           2021-01-21 12:05:58
*/

#ifndef NLC_M2C_1CELL_H_
#define NLC_M2C_1CELL_H_

#define  IPCore_Reset_NLC_M2C_1cell       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_NLC_M2C_1cell      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_NLC_M2C_1cell   0x8  //contains unique IP timestamp (yymmddHHMM): 2101211205
#define  Vdc_inv_Data_NLC_M2C_1cell       0x100  //data register for Inport Vdc_inv

#endif /* NLC_M2C_1CELL_H_ */
