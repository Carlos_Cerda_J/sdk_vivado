/*
 * File Name:         C:\Users\Victor\Documents\matlab_ipcores\Simulink\TRIP\TRIP_bits\ipcore\Trip_curr_bits_v1_0\include\Trip_curr_bits_addr.h
 * Description:       C Header File
 * Created:           2019-08-09 15:33:10
*/

#ifndef TRIP_CURR_BITS_H_
#define TRIP_CURR_BITS_H_

#define  IPCore_Reset_Trip_curr_bits       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_Trip_curr_bits      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_Trip_curr_bits   0x8  //contains unique IP timestamp (yymmddHHMM): 1908091533
#define  Umbral_P_Data_Trip_curr_bits      0x100  //data register for Inport Umbral_P
#define  Umbral_N_Data_Trip_curr_bits      0x104  //data register for Inport Umbral_N

#endif /* TRIP_CURR_BITS_H_ */
