/*
 * File Name:         hdl_prj\ipcore\Notch_25_16_v1_1\include\Notch_25_16_addr.h
 * Description:       C Header File
 * Created:           2020-03-09 16:39:49
*/

#ifndef NOTCH_25_16_H_
#define NOTCH_25_16_H_

#define  IPCore_Reset_Notch_25_16       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_Notch_25_16      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_Notch_25_16   0x8  //contains unique IP timestamp (yymmddHHMM): 2003091639
#define  k1_Data_Notch_25_16            0x100  //data register for Inport k1
#define  k2_Data_Notch_25_16            0x104  //data register for Inport k2
#define  n1_Data_Notch_25_16            0x108  //data register for Inport n1
#define  n2_Data_Notch_25_16            0x10C  //data register for Inport n2

#endif /* NOTCH_25_16_H_ */
