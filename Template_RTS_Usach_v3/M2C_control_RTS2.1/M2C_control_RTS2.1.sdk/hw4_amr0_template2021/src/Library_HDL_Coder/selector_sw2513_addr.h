/*
 * File Name:         E:\xilinx\IPCORES\selector_sw\ipcore\selector_sw2513_v1_0\include\selector_sw2513_addr.h
 * Description:       C Header File
 * Created:           2019-01-28 11:24:08
*/

#ifndef SELECTOR_SW2513_H_
#define SELECTOR_SW2513_H_

#define  IPCore_Reset_selector_sw2513          0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_selector_sw2513         0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_selector_sw2513      0x8  //contains unique IP timestamp (yymmddHHMM): 1901281124
#define  enable_preload_Data_selector_sw2513   0x100  //data register for Inport enable_preload

#endif /* SELECTOR_SW2513_H_ */
