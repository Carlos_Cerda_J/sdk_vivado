/*****************************************************************************/
/**
* @file  main.c
*
* This file contains a design example using the timer counter driver
* (XTmCtr) and hardware device using interrupt mode.This example assumes
* that the interrupt controller is also present as a part of the system
*
*
* MODIFICATION HISTORY:
*
* Ver   Who  Date	 Changes
* ---- ---- -------- -----------------------------------------------
* 1.00  DAG 30/07/18 First release
*
******************************************************************************/

/***************************** Include Files *********************************/

#include "xparameters.h"
#include "xtmrctr.h"
#include "xil_exception.h"

#include "xscugic.h"
#include "xgpio.h"
#include "xgpiops.h"
#include "xadcps.h"
#include "xil_printf.h"

/************************** Constant Definitions *****************************/
/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are only defined here such that a user can easily
 * change all the needed parameters in one place.
 */

//Timer Interrupt
#define TMRCTR_DEVICE_ID	XPAR_TMRCTR_0_DEVICE_ID
#define TMRCTR_INTERRUPT_ID	XPAR_FABRIC_TMRCTR_0_VEC_ID

#define INTC_DEVICE_ID		XPAR_SCUGIC_SINGLE_DEVICE_ID


/*
 * In PWM Mode, Timer-counter 0 is used for period setting and Timer-counter 1 is used for duty cycle.
 */
#define TIMER_FOR_PERIOD 0
#define TIMER_FOR_DUTY	 1

#define AXI_CLOCK_FREQUENCY XPAR_AXI_TIMER_INTERRUPT_TO_PS_CLOCK_FREQ_HZ

/*
 * The following constant determines which timer counter of the device that is
 * used for this example, there are currently 2 timer counters in a device
 * and this example uses the first one, 0, the timer numbers are 0 based
 */
#define TIMER_CNTR_0	 0


/*
 * ScuGic manage interrupts in PS
 */
#define INTC			XScuGic
#define INTC_HANDLER	XScuGic_InterruptHandler


/*
 * The following constant is used to set the reset value of the timer counter,
 * making this number larger reduces the amount of time this example consumes
 * because it is the value the timer counter is loaded with when it is started
 */

// GPIO
#define LED_CHANNEL 1
#define LED 0x0F   /* Assumes bits 0-3 of GPIO is connected to an LED  */
/*
 * The following constant is used to wait after an LED is turned on to make
 * sure that it is visible to the human eye.  This constant might need to be
 * tuned for faster or slower processor speeds.
 */
#define LED_DELAY     1000000

// GPIO PS
#define GPIO_DEVICE_ID  	XPAR_XGPIOPS_0_DEVICE_ID

// XADC
#define XADC_DEVICE_ID 		XPAR_XADCPS_0_DEVICE_ID

#define printf				xil_printf

/**************************** Type Definitions *******************************/


/***************** Macros (Inline Functions) Definitions *********************/


/************************** Function Prototypes ******************************/

int Control_interrupt_init(void);

static int TmrCtrSetupIntrSystem(INTC* IntcInstancePtr,
				XTmrCtr* InstancePtr,
				u16 DeviceId,
				u16 IntrId,
				u8 TmrCtrNumber);

void TimerCounterHandler(void *CallBackRef, u8 TmrCtrNumber);

void TmrCtrDisableIntr(INTC* IntcInstancePtr, u16 IntrId);

static int Init_XAdc(u16 XAdcDeviceId);
static int XAdcFractionToInt(float FloatNum);

/************************** Variable Definitions *****************************/
u32 frequency_interrupt = 1000;
u32 frequency_PWM = 10000;
u32 duty_PWM = 50;

u32 frequency_interrupt_prev = 1000;
u32 frequency_PWM_prev = 10000;
u32 duty_PWM_prev = 50;


INTC InterruptController;  /* The instance of the Interrupt Controller */

XTmrCtr TimerCounterInst;   /* The instance of the Timer Counter */

XGpio Gpio; /* The Instance of the GPIO Driver */

static XAdcPs XAdcInst;      /* XADC driver instance */
XAdcPs *XAdcInstPtr = &XAdcInst;

volatile u16 counter = 0;

/*****************************************************************************/
/**
* This function is the main function of the Tmrctr example using Interrupts.
*
* @param	None.
*
* @return	XST_SUCCESS to indicate success, else XST_FAILURE to indicate a
*		Failure.
*
* @note		None.
*
******************************************************************************/
int main(void)
{
	int Status;
	volatile int Delay;


	/* Initialize AXI timer for PS periodic interrupts. */
	Status = Control_interrupt_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("Control_interrupt Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/*
	 * Initialize the GPIO driver
	 */
	Status = XGpio_Initialize(&Gpio, XPAR_GPIO_0_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Set the direction for all signals to be outputs
	 */
	XGpio_SetDataDirection(&Gpio, LED_CHANNEL, 0x00000000);


	/*
	 * Initialize the XADC driver
	 */
	Status = Init_XAdc(XADC_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("adcps polled printf Example Failed\r\n");
		return XST_FAILURE;
	}


	while (1)
	{
		XGpio_DiscreteSet(&Gpio, LED_CHANNEL, LED);

		/* Wait a small amount of time so the LED is visible */
		for (Delay = 0; Delay < LED_DELAY; Delay++);

		XGpio_DiscreteClear(&Gpio, LED_CHANNEL, LED);
		/* Wait a small amount of time so the LED is visible */
		for (Delay = 0; Delay < LED_DELAY; Delay++);
	}

	return XST_SUCCESS;
}



	/****************************************************************************/
	/**
	*
	* This function runs a test on the XADC/ADC device using the
	* driver APIs.
	* This function does the following tasks:
	*	- Initiate the XADC device driver instance
	*	- Run self-test on the device
	*	- Setup the sequence registers to continuously monitor on-chip
	*	temperature and, VCCPINT, VCCPAUX and VCCPDRO Voltages
	*	- Setup configuration registers to start the sequence
	*	- Read the latest on-chip temperature and, VCCPINT, VCCPAUX and VCCPDRO
	*	  Voltages
	*
	* @param	XAdcDeviceId is the XPAR_<SYSMON_ADC_instance>_DEVICE_ID value
	*		from xparameters.h.
	*
	* @return
	*		- XST_SUCCESS if the example has completed successfully.
	*		- XST_FAILURE if the example has failed.
	*
	* @note   	None
	*
	****************************************************************************/
	int Init_XAdc(u16 XAdcDeviceId)
	{
		int Status;
		XAdcPs_Config *ConfigPtr;
		u32 TempRawData;
		float TempData;
		float MaxData;
		float MinData;

		/*
		 * Initialize the XAdc driver.
		 */
		ConfigPtr = XAdcPs_LookupConfig(XAdcDeviceId);
		if (ConfigPtr == NULL) {
			return XST_FAILURE;
		}
		XAdcPs_CfgInitialize(XAdcInstPtr, ConfigPtr,
					ConfigPtr->BaseAddress);

		/*
		 * Self Test the XADC/ADC device
		 */
		Status = XAdcPs_SelfTest(XAdcInstPtr);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Disable the Channel Sequencer before configuring the Sequence
		 * registers.
		 */
		XAdcPs_SetSequencerMode(XAdcInstPtr, XADCPS_SEQ_MODE_SAFE);
		/*
		 * Read the on-chip Temperature Data (Current/Maximum/Minimum)
		 * from the ADC data registers.
		 */
		TempRawData = XAdcPs_GetAdcData(XAdcInstPtr, XADCPS_CH_TEMP);
		TempData = XAdcPs_RawToTemperature(TempRawData);
		printf("\r\nThe Current Temperature is %0d.%03d Centigrades.\r\n",
					(int)(TempData), XAdcFractionToInt(TempData));

		TempRawData = XAdcPs_GetMinMaxMeasurement(XAdcInstPtr, XADCPS_MAX_TEMP);
		MaxData = XAdcPs_RawToTemperature(TempRawData);
		printf("The Maximum Temperature is %0d.%03d Centigrades. \r\n",
					(int)(MaxData), XAdcFractionToInt(MaxData));

		TempRawData = XAdcPs_GetMinMaxMeasurement(XAdcInstPtr, XADCPS_MIN_TEMP);
		MinData = XAdcPs_RawToTemperature(TempRawData & 0xFFF0);
		printf("The Minimum Temperature is %0d.%03d Centigrades. \r\n",
					(int)(MinData), XAdcFractionToInt(MinData));


		return XST_SUCCESS;
	}

/*****************************************************************************/
/**
* This function does a minimal test on the timer counter device and driver as a
* design example.  The purpose of this function is to illustrate how to use the
* XTmrCtr component.  It initializes a timer counter and then sets it up in
* compare mode with auto reload such that a periodic interrupt is generated.
*
* This function uses interrupt driven mode of the timer counter.
*
* @param	IntcInstancePtr is a pointer to the Interrupt Controller
*		driver Instance
* @param	TmrCtrInstancePtr is a pointer to the XTmrCtr driver Instance
* @param	DeviceId is the XPAR_<TmrCtr_instance>_DEVICE_ID value from
*		xparameters.h
* @param	IntrId is XPAR_<INTC_instance>_<TmrCtr_instance>_INTERRUPT_INTR
*		value from xparameters.h
* @param	TmrCtrNumber is the number of the timer to which this
*		handler is associated with.
*
* @return	XST_SUCCESS if the Test is successful, otherwise XST_FAILURE
*
* @note		This function contains an infinite loop such that if interrupts
*		are not working it may never return.
*
*****************************************************************************/
int Control_interrupt_init(void)
{
	int Status;

	INTC* IntcInstancePtr = &InterruptController;
	XTmrCtr* TmrCtrInstancePtr = &TimerCounterInst;
	u16 DeviceId = TMRCTR_DEVICE_ID;
	u16 IntrId = TMRCTR_INTERRUPT_ID;
	u8 TmrCtrNumber = TIMER_CNTR_0;

	u32 RESET_VALUE = (AXI_CLOCK_FREQUENCY / frequency_interrupt) - 2; 	//PWM_PERIOD = (TLR0 + 2) * AXI_CLOCK_PERIOD

	/*
	 * Initialize the timer counter so that it's ready to use,
	 * specify the device ID that is generated in xparameters.h
	 */
	Status = XTmrCtr_Initialize(TmrCtrInstancePtr, DeviceId);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test to ensure that the hardware was built
	 * correctly, use the 1st timer in the device (0)
	 */
	Status = XTmrCtr_SelfTest(TmrCtrInstancePtr, TmrCtrNumber);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Connect the timer counter to the interrupt subsystem such that
	 * interrupts can occur.  This function is application specific.
	 */
	Status = TmrCtrSetupIntrSystem(IntcInstancePtr,
					TmrCtrInstancePtr,
					DeviceId,
					IntrId,
					TmrCtrNumber);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Setup the handler for the timer counter that will be called from the
	 * interrupt context when the timer expires, specify a pointer to the
	 * timer counter driver instance as the callback reference so the handler
	 * is able to access the instance data
	 */
	XTmrCtr_SetHandler(TmrCtrInstancePtr, TimerCounterHandler,
					   TmrCtrInstancePtr);

	/*
	 * Enable the interrupt of the timer counter so interrupts will occur
	 * and use auto reload mode such that the timer counter will reload
	 * itself automatically and continue repeatedly, without this option
	 * it would expire once only. Also the timer count down to 0
	 */
	XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber,
				XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION  | XTC_DOWN_COUNT_OPTION);

	/*
	 * Set a reset value for the timer counter such that it will expire
	 * eariler than letting it roll over from 0, the reset value is loaded
	 * into the timer counter when it is started
	 */
	XTmrCtr_SetResetValue(TmrCtrInstancePtr, TmrCtrNumber, RESET_VALUE);

	/*
	 * Start the timer counter such that it's incrementing by default,
	 * then wait for it to timeout a number of times
	 */
	XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);


	//TmrCtrDisableIntr(IntcInstancePtr, DeviceId);
	return XST_SUCCESS;
}

/*****************************************************************************/
/**
* This function is the handler which performs processing for the timer counter.
* It is called from an interrupt context such that the amount of processing
* performed should be minimized.  It is called when the timer counter expires
* if interrupts are enabled.
*
* This handler provides an example of how to handle timer counter interrupts
* but is application specific.
*
* @param	CallBackRef is a pointer to the callback function
* @param	TmrCtrNumber is the number of the timer to which this
*		handler is associated with.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
void TimerCounterHandler(void *CallBackRef, u8 TmrCtrNumber)
{
	XTmrCtr *InstancePtr = (XTmrCtr *)CallBackRef;

	/*
	 * Check if the timer counter has expired, checking is not necessary
	 * since that's the reason this function is executed, this just shows
	 * how the callback reference can be used as a pointer to the instance
	 * of the timer counter that expired.
	 */
	if (XTmrCtr_IsExpired(InstancePtr, TmrCtrNumber))
	{
		counter++;
		xil_printf("Timer_Interrupt: %d\n", counter);

		u32 TempRawData;
		float TempData;
		/*
		 * Read the on-chip Temperature Data (Current/Maximum/Minimum)
		 * from the ADC data registers.
		 */
		TempRawData = XAdcPs_GetAdcData(XAdcInstPtr, XADCPS_CH_TEMP);
		TempData = XAdcPs_RawToTemperature(TempRawData);
		printf("Dice Temperature is %0d Centigrades.\r\n\n",
					(int)(TempData));
	}
}

/*****************************************************************************/
/**
* This function setups the interrupt system such that interrupts can occur
* for the timer counter. This function is application specific since the actual
* system may or may not have an interrupt controller.  The timer counter could
* be directly connected to a processor without an interrupt controller.  The
* user should modify this function to fit the application.
*
* @param	IntcInstancePtr is a pointer to the Interrupt Controller
*		driver Instance.
* @param	TmrCtrInstancePtr is a pointer to the XTmrCtr driver Instance.
* @param	DeviceId is the XPAR_<TmrCtr_instance>_DEVICE_ID value from
*		xparameters.h.
* @param	IntrId is XPAR_<INTC_instance>_<TmrCtr_instance>_VEC_ID
*		value from xparameters.h.
* @param	TmrCtrNumber is the number of the timer to which this
*		handler is associated with.
*
* @return	XST_SUCCESS if the Test is successful, otherwise XST_FAILURE.
*
* @note		This function contains an infinite loop such that if interrupts
*		are not working it may never return.
*
******************************************************************************/
static int TmrCtrSetupIntrSystem(INTC* IntcInstancePtr,
				 XTmrCtr* TmrCtrInstancePtr,
				 u16 DeviceId,
				 u16 IntrId,
				 u8 TmrCtrNumber)
{
	int Status;

	XScuGic_Config *IntcConfig;

	/*
	 * Initialize the interrupt controller driver so that it is ready to
	 * use.
	 */
	IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
	if (NULL == IntcConfig) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
					IntcConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XScuGic_SetPriorityTriggerType(IntcInstancePtr, IntrId,
					0xA0, 0x3);

	/*
	 * Connect the interrupt handler that will be called when an
	 * interrupt occurs for the device.
	 */
	Status = XScuGic_Connect(IntcInstancePtr, IntrId,
				 (Xil_ExceptionHandler)XTmrCtr_InterruptHandler,
				 TmrCtrInstancePtr);
	if (Status != XST_SUCCESS) {
		return Status;
	}

	/*
	 * Enable the interrupt for the Timer device.
	 */
	XScuGic_Enable(IntcInstancePtr, IntrId);


	/*
	 * Initialize the exception table.
	 */
	Xil_ExceptionInit();

	/*
	 * Register the interrupt controller handler with the exception table.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
					(Xil_ExceptionHandler)
					INTC_HANDLER,
					IntcInstancePtr);

	/*
	 * Enable non-critical exceptions.
	 */
	Xil_ExceptionEnable();

	return XST_SUCCESS;
}


/******************************************************************************/
/**
*
* This function disables the interrupts for the Timer.
*
* @param	IntcInstancePtr is a reference to the Interrupt Controller
*		driver Instance.
* @param	IntrId is XPAR_<INTC_instance>_<Timer_instance>_VEC_ID
*		value from xparameters.h.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
void TmrCtrDisableIntr(INTC* IntcInstancePtr, u16 IntrId)
{
	/*
	 * Disable the interrupt for the timer counter
	 */
#ifdef XPAR_INTC_0_DEVICE_ID
	XIntc_Disable(IntcInstancePtr, IntrId);
#else
	/* Disconnect the interrupt */
	XScuGic_Disable(IntcInstancePtr, IntrId);
	XScuGic_Disconnect(IntcInstancePtr, IntrId);
#endif

	return;
}


/****************************************************************************/
/**
*
* This function converts the fraction part of the given floating point number
* (after the decimal point)to an integer.
*
* @param	FloatNum is the floating point number.
*
* @return	Integer number to a precision of 3 digits.
*
* @note
* This function is used in the printing of floating point data to a STDIO device
* using the xil_printf function. The xil_printf is a very small foot-print
* printf function and does not support the printing of floating point numbers.
*
*****************************************************************************/
int XAdcFractionToInt(float FloatNum)
{
	float Temp;

	Temp = FloatNum;
	if (FloatNum < 0) {
		Temp = -(FloatNum);
	}

	return( ((int)((Temp -(float)((int)Temp)) * (1000.0f))));
}
