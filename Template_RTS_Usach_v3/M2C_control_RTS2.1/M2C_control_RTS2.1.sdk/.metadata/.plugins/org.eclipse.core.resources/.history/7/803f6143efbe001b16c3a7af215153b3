#ifndef SRC_PEFFTAUX_H_
#define SRC_PEFFTAUX_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <xil_types.h>

typedef struct FORM_FILTER_NOTCH
{
	double a0;
	double a1;
	double a2;
	double b0;
	double b1;
	double b2;
	double in;
	double out;
	double in_1;
	double in_2;
	double out_1;
	double out_2;
	double h;
	double fc;
	double delta_f;
}FORM_FILTER_NOTCH;


typedef struct FORM_ABC
{
	double a;
	double b;
	double c;
} FORM_ABC;


typedef struct FORM_AB_BC_CA
{
	double ab;
	double bc;
	double ca;
} FORM_AB_BC_CA;


typedef struct FORM_CLARKE
{
	double alpha;
	double beta;
	double cero;
} FORM_CLARKE;


typedef struct FORM_PARK
{
	double d;
	double q;
	double cero;
} FORM_PARK;


typedef struct FORM_ABC_PN
{
	double ap;
	double bp;
	double cp;
	double an;
	double bn;
	double cn;
	//FORM_ABC p;
	//FORM_ABC n;
} FORM_ABC_PN;


typedef struct FORM_CLARKE_SIGMA_DELTA
{
	double alphaSigma;
	double betaSigma;
	double ceroSigma;
	double alphaDelta;
	double betaDelta;
	double ceroDelta;
	//FORM_CLARKE sigma;
	//FORM_CLARKE delta;
} FORM_CLARKE_SIGMA_DELTA;


typedef struct FORM_CAPACITORS_DETAILED_ABC_PN
{
	double ap1;
	double ap2;
	double ap3;
	double ap4;
	double ap5;
	double ap6;
	double bp1;
	double bp2;
	double bp3;
	double bp4;
	double bp5;
	double bp6;
	double cp1;
	double cp2;
	double cp3;
	double cp4;
	double cp5;
	double cp6;
	double an1;
	double an2;
	double an3;
	double an4;
	double an5;
	double an6;
	double bn1;
	double bn2;
	double bn3;
	double bn4;
	double bn5;
	double bn6;
	double cn1;
	double cn2;
	double cn3;
	double cn4;
	double cn5;
	double cn6;
} FORM_CAPACITORS_DETAILED_ABC_PN;

//////////////////////////////////////////////////////////
//This is for measurements for the Corfo�s project
///////////////////////////////////////////////////////////
typedef struct FORM_MEAS_RECT{
	double Vg;
	double Ig;
	double Vdc;
}FORM_MEAS_RECT;

typedef struct FORM_MEAS_PARALLEL{
	double Iax[11];
	double Ibx[11];
	double IM3a[11];
	double IM3b[11];
}FORM_MEAS_PARALLEL;

typedef struct FORM_IM4{
	double IM4a[11];
	double IM4b[11];
}FORM_IM4;

typedef struct FORM_REF_RECT{
	double Vdc;
	double Ig;
} FORM_REF_RECT;

typedef struct FORM_REF_PARALLEL{
	double Itotal;
	double Icirc;
} FORM_REF_PARALLEL;
////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

typedef struct FORM_SEQUENCES
{
    FORM_CLARKE    clarkePositive;
    FORM_CLARKE    clarkeNegative;

    FORM_PARK    parkPositive;
    FORM_PARK    parkNegative;

} FORM_SEQUENCES;


typedef struct FORM_CONTROL_TRANSFORM
{
    FORM_IM4  Parallel;
} FORM_CONTROL_TRANSFORM;

typedef struct FORM_CONTROL_REFERENCES
{

	FORM_REF_RECT          Rectifier;
	FORM_REF_PARALLEL      Parallel;

} FORM_CONTROL_REFERENCES;


typedef struct FORM_CONTROL_MEASUREMENTS{

	FORM_MEAS_RECT   Rectifier;
	FORM_MEAS_PARALLEL Parallel;

} FORM_CONTROL_MEASUREMENTS;

//typedef struct CONTROL_ENABLE_ST{
//	s16 PR_s;
//	s16 PR_z;
//	s16 PR_o;
//	s16 OpenLoop_o;
//	s16 FO_MMC;
//}CONTROL_ENABLE;

typedef struct FORM_FILTERS
{
	FORM_FILTER_NOTCH notchIap;
	FORM_FILTER_NOTCH notchIbp;
	FORM_FILTER_NOTCH notchIcp;
	FORM_FILTER_NOTCH notchIan;
	FORM_FILTER_NOTCH notchIbn;
	FORM_FILTER_NOTCH notchIcn;

	FORM_FILTER_NOTCH notchIAlphaSigma;
	FORM_FILTER_NOTCH notchIBetaSigma;
	FORM_FILTER_NOTCH notchIAlphaDelta;
	FORM_FILTER_NOTCH notchIBetaDelta;

	FORM_FILTER_NOTCH notchIAlpha;
	FORM_FILTER_NOTCH notchIBeta;
} FORM_FILTERS;


typedef struct FORM_CONTROL
{
	FORM_CONTROL_REFERENCES		ref;
	FORM_CONTROL_MEASUREMENTS	meas;
	FORM_CONTROL_MEASUREMENTS 	measFiltered;
	FORM_CONTROL_TRANSFORM      Transform;
//	FORM_CONTROL_ACTIVATORS enable;
} FORM_CONTROL;


typedef struct FORM_GLOBAL_VARIABLE
{
	FORM_CONTROL	control;
	FORM_FILTERS	filters;
} FORM_GLOBAL_VARIABLE;


typedef struct FORM_PR_CONTROLLER{
	double in;
	double out;
	double in_1;
	double in_2;
	double out_1;
	double out_2;
	double kp;
	double ki;
	double h;
	double w;
	double a0;
	double a1;
	double a2;
	double b0;
	double b1;
	double b2;
}FORM_PR_CONTROLLER;


double CalculatePR(FORM_PR_CONTROLLER *PR, double in);

double CalculateFilterNotchBandStop(FORM_FILTER_NOTCH *F, double in);
double CalculateFilterNotchBandPass(FORM_FILTER_NOTCH *F, double in);
void InitializeFilterNotch(FORM_FILTER_NOTCH *F, double fc, double delta_f, double h);
void ResetFilterNotch(FORM_FILTER_NOTCH *F);

void Transform_ABCPN2AB0SD(FORM_ABC_PN A, FORM_CLARKE_SIGMA_DELTA *B);
void Transform_AB0SD2ABCPN(FORM_CLARKE_SIGMA_DELTA A, FORM_ABC_PN *B);
void Transform_ABC2Clarke(FORM_ABC A, FORM_CLARKE *B);
void Transform_Clarke2ABC(FORM_CLARKE A, FORM_ABC *B);
void Transform_Clarke2Park(FORM_CLARKE A, FORM_PARK *B, double theta);
void Transform_Park2Clarke(FORM_PARK A, FORM_CLARKE *B, double theta);






////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct PI_form{
	float IN;
	float OUT;
	float in_1;
	float in_2;
	float out_1;
	float out_2;
	double kp;
	double ki;
	float umax;
	double h;
	float FF;//feed forward
	char name[50];
}PI_form;

typedef struct{
	float IN;
	float OUT;
	float in_1;
	float in_2;
	float out_1;
	float out_2;
	float kp;
	float ki;
	float umax;
	float out_amp;
	float h;
	float w;//frecuencia de sintonia
	float FF;//feed forward
	char name[50];
	float d;
	float q;
	float ang;
}PR_form;

typedef struct PR_float{
	float in;
	float out;
	float in_1;
	float in_2;
	float out_1;
	float out_2;
	float kp;
	float ki;
	float h;
	float w;//frecuencia de sintonia
	float a0;
	float a1;
	float a2;
	float b0;
	float b1;
	float b2;
	float FF;//feed forward
	float umax;
}PR_float_form;

typedef struct Filter{
	float a0;
	float a1;
	float a2;
	float b0;
	float b1;
	float b2;
	float in;
	float out;
	float in_1;
	float in_2;
	float out_1;
	float out_2;
	float h;
	float fc;
}Filter;

typedef struct Filter_notch_ST{
	float a0;
	float a1;
	float a2;
	float b0;
	float b1;
	float b2;
	float in;
	float out;
	float in_1;
	float in_2;
	float out_1;
	float out_2;
	float h;
	float fc;
	float delta_f;
}Filter_notch_form;

typedef struct Filter_double{
	double a0;
	double a1;
	double a2;
	double b0;
	double b1;
	double b2;
	double in;
	double out;
	double in_1;
	double in_2;
	double out_1;
	double out_2;
	double h;
	double fc;
}Filter_double;

typedef struct XY{
	float ap;
	float bp;
	float cp;
	float an;
	float bn;
	float cn;
	float ap1;
	float ap2;
	float ap3;
	float ap4;
	float ap5;
	float ap6;
	float bp1;
	float bp2;
	float bp3;
	float bp4;
	float bp5;
	float bp6;
	float cp1;
	float cp2;
	float cp3;
	float cp4;
	float cp5;
	float cp6;
	float an1;
	float an2;
	float an3;
	float an4;
	float an5;
	float an6;
	float bn1;
	float bn2;
	float bn3;
	float bn4;
	float bn5;
	float bn6;
	float cn1;
	float cn2;
	float cn3;
	float cn4;
	float cn5;
	float cn6;
}XY;

typedef struct AB0SD{
	float al_S;
	float beta_S;
	float cero_S;
	float al_D;
	float beta_D;
	float cero_D;
}AB0SD;

typedef struct ABC{
	float a;
	float b;
	float c;
}ABC;

typedef struct CLARKE{
	float al;
	float be;
	float cero;
}CLARKE;

typedef struct PARK{
	float d;
	float q;
	float cero;
}PARK;


void PI_discreto_SAT(PI_form *PI,float in,float *out);
void PI_discreto_SAT_form2(PI_form *PI,float in, float *out);
void PR_tpw(PR_float_form *PR, float in, float *out);

void init_notch(Filter *F, float fc,float h);
void init_butt_filter(Filter *F, float fc,float h);
void init_zero_butt_filter(Filter *F, float fc,float h);
void xy2ab0sd(XY A,AB0SD *B);
void ab0sd2xy(AB0SD A,XY *B);
void albe2dq(float al,float be,float *d,float *q,float ang);
void dq2albe(float d,float q,float *al,float *be,float ang);
void gen_filter(Filter *F,float in,float *out);
void ramp_up_global(float *var, float delta, int samples);
float toFloat(int num,int bit_sig);
float toFloat2(int num, int bit_word, int bit_frac);
int toFix(float num,int bit_sig);
u16 Protection_func(float Value, float max, float min);

void init_filter_notch(Filter_notch_form *F, float fc, float delta_f, float h);
void Calculate_filter_notch(Filter_notch_form *F, float in, float *out);
void abc2albe(float a, float b, float c, float *al, float *be);

//corfo�s project

typedef struct PI_form_C{
	float out_act;
	float error_act;
	float out_1;
	float error_1;
	double kpz;
	double az;
	float umax;
}PI_form_C;

typedef struct PR_float_form_C{

	float error_act;
	float out_act;
	float error_1;
	float error_2;
	float out_1;
	float out_2;
	float a0;
	float a1;
	float a2;
	float b1;
	float b2;

}PR_float_form_C;

typedef struct PR_float_AnC{

	float error_act;
	float out_act;
	float error_1;
	float error_2;
	float out_1;
	float out_2;
	float a0;
	float a1;
	float a2;
	float b1;
	float b2;

}PR_float_AnC;

void InitializePI_tustin(PI_form_C *PI, float kp, float ki, float ts, float lim);
double CalculatePI_tustin(PI_form_C *PI,float ref, float meas);

void InitializePR_tpw(PR_float_form_C *PR, float kp, float ki, float w, float ts);
double CalculatePR_tpw(PR_float_form_C *PR,float ref, float meas);

void InitializePR_tpw_AnC(PR_float_AnC *PRAnC, float kp, float ki, float w, float ts, float Dn);
double CalculatePR_tpw_AnC(PR_float_AnC *PRAnC, float ref, float meas);








#endif /* SRC_PEFFTAUX_H_ */
