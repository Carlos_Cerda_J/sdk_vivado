/*
 * main.h
 *
 *  Created on: 21-11-2018
 *      Author: David
 */

#ifndef SRC_MAIN_H_
#define SRC_MAIN_H_

/***************************** Include Files *********************************/
#include "main.h"
#include "math.h"

#include "xparameters.h"
#include "xil_exception.h"
#include "xil_printf.h"


#include "xscugic.h"	/* scu-GIC PS device driver (General Interrupt Controller) */
#include "xgpiops.h"	/* GPIO PS device driver */
#include "xiicps.h"		/* I2C PS device driver */
#include "xadcps.h"		/* ADC PS device driver (For internal temperature measure)*/

#include "xtmrctr.h"	/* Timer-Counter PL device driver */
#include "xgpio.h"		/* GPIO PL device driver */
//#include "ADC_ADS5272.h"		/* Fast ADC device driver */
#include "PR_NOsatv4_addr.h"			/* adclk generator device driver */
#include "PWM_s25f13_addr.h"			/* adclk generator device driver */
#include "selector_sw2513_addr.h"
#include "pefftaux.h"
#include "ARM_to_FPGA.h"
#include "fast_adc_conv_addr.h"
#include "switch_dpdt_addr.h"
#include "PR_oversamp_addr.h"
#include "debug_triple_memory_8x100.h"
#include "debug_triple_memory_16x100.h"
#include "debug_8_channel.h"
#include "sample_clock.h"
#include "THREE_SINE_IP_addr.h"
#include "PLL2_ip_addr.h"
#include "Sine_LUT_addr.h"
#include "bits_to_V_addr.h"
#include "PWM_scaler_addr.h"
#include "AXI_constant_addr.h"
#include "slow_adc_conv_addr.h"

/************************** Constant Definitions *****************************/
/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are only defined here such that a user can easily
 * change all the needed parameters in one place.
 */

#define PWM_IP_BASEADDR 		XPAR_OPTICAL_FIBERS_PWM_H_BRIDGE_0_BASEADDR
#define TMRCTR_INT_BASEADDR		XPAR_AXI_TIMER_INTERRUPT_TO_PS_BASEADDR


/////////////////////////////////////////////////////////////////////////


#define PR_alpha_delta			XPAR_CONTROL_IALPHA_DELTA_PR_OVERSAMP_0_BASEADDR
#define PR_beta_delta			XPAR_CONTROL_IBETA_DELTA_PR_OVERSAMP_0_BASEADDR
#define PR_alpha_sigma_s		XPAR_CONTROL_IALPHA_SIGMA_S_PR_OVERSAMP_0_BASEADDR
#define PR_beta_sigma_s			XPAR_CONTROL_IBETA_SIGMA_S_PR_OVERSAMP_0_BASEADDR
#define PR_alpha_sigma_o		XPAR_CONTROL_IALPHA_SIGMA_O_PR_OVERSAMP_0_BASEADDR
#define PR_beta_sigma_o			XPAR_CONTROL_IBETA_SIGMA_O_PR_OVERSAMP_0_BASEADDR
#define PR_cero_sigma			XPAR_CONTROL_I0_SIGMA_PR_OVERSAMP_0_BASEADDR ///Error de nombres!

#define PII		3.14159265359

#define idp_ref		ARM_TO_FPGA_id_pos_OFFSET
#define iqp_ref		ARM_TO_FPGA_iq_pos_OFFSET
#define idn_ref		ARM_TO_FPGA_id_neg_OFFSET
#define iqn_ref		ARM_TO_FPGA_iq_neg_OFFSET
#define idp_s_ref	ARM_TO_FPGA_id_sigma_s_pos_OFFSET
#define iqp_s_ref	ARM_TO_FPGA_iq_sigma_s_pos_OFFSET
#define idn_s_ref	ARM_TO_FPGA_id_sigma_s_neg_OFFSET
#define iqn_s_ref	ARM_TO_FPGA_iq_sigma_s_neg_OFFSET
#define idp_o_ref	ARM_TO_FPGA_id_sigma_o_pos_OFFSET
#define iqp_o_ref	ARM_TO_FPGA_iq_sigma_o_pos_OFFSET
#define idn_o_ref	ARM_TO_FPGA_id_sigma_o_neg_OFFSET
#define iqn_o_ref	ARM_TO_FPGA_iq_sigma_o_neg_OFFSET




int control_intra=0;
int control_inner=0;
float xi=0.707;

float V_grid = 60.0*sqrt(2.0);

float xi_b1=0.807;
float xi_b2=0.807;
float xi_b3=0.807;
float L=0.0004;
float h=0.00002;
float C = 0.0009;
float Lac = 0.001;
float w_load=2.0*3.1416*1000.0;
float w_grid=2.0*3.1416*50.0;
float Vo=70.0;
float n=1.0;
float Vc=100.0;
float w;
float a1d,a2d,ki,kp;
unsigned int a1F,a2F,b0F,b1F,b2F;
float a1,a2,b0,b1,b2;
float a1_npr,a2_npr,b0_npr,b1_npr,b2_npr;
float a1_spr,b0_spr,b1_spr;
unsigned int a1_nprF,a2_nprF,b0_nprF,b1_nprF,b2_nprF;
unsigned int a1_sprF,b0_sprF,b1_sprF;
float F;
float kp_sD,ki_sD,kp_oD,ki_oD,kp_zDo,ki_zDo,kp_zDs,ki_zDs;
float kp_b1D,ki_b1D,kp_b2D,ki_b2D,kp_b3D,ki_b3D;
int bits_frac=13;
float wn_s = 400*2.0*PII;
float wn_z = 400*2.0*PII;
float wn_o = 400*2.0*PII;
float vcap,vcbp,vccp,vcan,vcbn,vccn;
float Vc_ref=40.0;
float wn_b1 = 10.0*2.0*PII;
	float wn_b2 = 10.0*2.0*PII;
	float wn_b3 = 10.0*2.0*PII;
	float xi_z=0.9;
Filter notches[6]={{0}};
XY Vcxy;
AB0SD Vc_transf,Vc_transf_filt;
int neg_sequence=0;
int activ_PR_sigma_s,activ_PR_sigma_o;
PI_form PI_Volt[6]={{0}};

float id_plus,id_sigma_plus_o,iq_sigma_plus_o,id_sigma_min_s,iq_sigma_min_s,id_sigma_plus_s;
float id_min,iq_min;
float out_test;


float i_alpha_plus,i_beta_plus,i_alpha_min,i_beta_min;
float i_alpha,i_beta;
float i_alpha_sigma_plus_s,i_beta_sigma_plus_s,i_alpha_sigma_min_s,i_beta_sigma_min_s;
float i_alpha_sigma_s,i_beta_sigma_s;
float i_alpha_sigma_plus_o,i_beta_sigma_plus_o;
float i_alpha_sigma_o,i_beta_sigma_o;








/////////////////////////////////////////////////////////////////////////


float max_point=500.0;
int aux = 0;
u16 trip_input1_slow = 240;// Voltajes linea linea 140 para 600 [V]
u16 trip_input2_slow = 120;//Voltajes Vcap Vcbp Vccp Vcan 120 para 505 [V] app
u16 trip_input3_slow = 120;//Voltajes Vcbn Vccn
u16 trip_input4_slow = 0xff;

u16 trip_slow = 0xff;
u16 trip_input1_fast = 146; //146 para Delta V de 0.971, lo que se obtiene con 10[A] medidos en chroma
u16 trip_input2_fast = 146;

int transition=0;
int debug_data_1[100];
int debug_data_2[100];
int debug_data_3[100];
float debug_data_4[100];
float debug_data_5[100];
float debug_data_6[100];
float debug_data_7[100];
float debug_data_8[100];
float debug_data_9[100];
float debug_data_10[100];
float debug_data_11[100];
float debug_data_12[100];
float debug_error[100];
unsigned int constant = 0;
int  debug_8ch_1[8];
int debug_8ch_2[8];
float debug_8ch_3[8];
float debug_8ch_slow_conv1[8];
float debug_8ch_slow_conv2[8];
int trip_software=0;
short re_init_fast_adcs = 0;
short re_init_fast_adcs_IP = 0;
short start_debugging = 0;
short start_debug_1sample = 1;
short debug_ends = 0;
short change_fast_adc_pattern = 0;
u16 pattern = 0;
int enable_mmc = 0;
short plot_data = 0;
short adc_values[8];
int shift_adc=1;
unsigned int frequency_sampler = 100000;
float carrier_max=500.0;
float vdc=40.0;
float Cm_over_vdc;
float out_ref=0.0;
int enable_control=0;
int open_loop=0;
int debug_selector = 0;
int r=0;
float Vc_prom[100]={0};
float id_time[100]={0};
float kkk;
float Vc_prom_sn[100]={0};
typedef struct PR_activacion
{
	int activ_alpha_delta;
	int activ_beta_delta ;
	int activ_alpha_sigma_s;
	int activ_alpha_sigma_o;
	int activ_beta_sigma_s;
	int activ_beta_sigma_o;
	int activ_0_sigma ;

}PR_activ;

PR_activ PR_act={1};
int debug_div=20000;
int start_PR=0;


float error_prom[100];
float error_inner1[100];
float error_inner2[100];
float error_inner3[100];
float error_intra1[100];
float error_intra2[100];
int plot_counter_inner=0;
int plot_counter_prom=0;
int plot_counter_intra=0;

float error_PR_alpha_delta[100];
float error_PR_beta_delta[100];
float error_PR_alpha_sigma_s[100];
float error_PR_beta_sigma_s[100];
float error_PR_alpha_sigma_o[100];
float error_PR_beta_sigma_o[100];
float error_PR_cero_sigma[100];







//Valores finales de constantes;
float kp_sDt,ki_sDt;
float kp_zDst,ki_zDst,kp_zDot,ki_zDot;
float kp_oDt,ki_oDt;

float kp_b1Dt,ki_b1Dt,kp_b2Dt,ki_b2Dt,kp_b3Dt,ki_b3Dt;

float constant_step,constant_step_v_inner,constant_step_v_intra,constant_step_I_inner,constant_step_I_intra;
float constant_step_v_prom;
int cv_inner,cv_intra,ci_inner,ci_intra,cv_prom;
int l=1;
//Timer Interrupt
#define TMRCTR_DEVICE_ID	XPAR_TMRCTR_0_DEVICE_ID
#define TMRCTR_INTERRUPT_ID	XPAR_FABRIC_TMRCTR_0_VEC_ID

#define INTC_DEVICE_ID		XPAR_SCUGIC_SINGLE_DEVICE_ID


/*
 * In PWM Mode, Timer-counter 0 is used for period setting and Timer-counter 1 is used for duty cycle.
 */
#define TIMER_FOR_PERIOD 0
#define TIMER_FOR_DUTY	 1

#define AXI_CLOCK_FREQUENCY XPAR_AXI_TIMER_INTERRUPT_TO_PS_CLOCK_FREQ_HZ

/*
 * The following constant determines which timer counter of the device that is
 * used for this example, there are currently 2 timer counters in a device
 * and this example uses the first one, 0, the timer numbers are 0 based
 */
#define TIMER_CNTR_0	 0


/*
 * ScuGic manage interrupts in PS
 */
#define INTC			XScuGic
#define INTC_HANDLER	XScuGic_InterruptHandler


/*
 * The following constant is used to set the reset value of the timer counter,
 * making this number larger reduces the amount of time this example consumes
 * because it is the value the timer counter is loaded with when it is started
 */

/*
 * The following constant is used to determine which channel of the GPIO is
 * used. There are 2 channels supported.
 */

#define GPIO_RESET_DEVICE_ID  	XPAR_AXI_GPIO_RESET_DEVICE_ID
#define GPIO_TRIP_DEVICE_ID  	XPAR_AXI_GPIO_INPUT_DEVICE_ID
#define GPIO_CONSTANT_DEVICE_ID		XPAR_AXI_GPIO_0_DEVICE_ID
#define GPIO_PRELOAD		XPAR_AXI_GPIO_0_DEVICE_ID

#define GPIO_CHANNEL 1
#define GPIO_CHANNEL1 1
#define GPIO_CHANNEL2 2

#define GPIO_pin_0  0x00000001
#define GPIO_pin_1  0x00000002
#define GPIO_pin_2  0x00000004
#define GPIO_pin_3  0x00000008
#define GPIO_pin_4  0x00000010
#define GPIO_pin_5  0x00000020
#define GPIO_pin_6  0x00000040
#define GPIO_pin_7  0x00000080
#define GPIO_pin_8  0x00000100
#define GPIO_pin_9  0x00000200
#define GPIO_pin_10 0x00000400
#define GPIO_pin_11 0x00000800
#define GPIO_pin_12 0x00001000
#define GPIO_pin_13 0x00002000
#define GPIO_pin_14 0x00004000
#define GPIO_pin_15 0x00008000
#define GPIO_pin_16 0x00010000
#define GPIO_pin_17 0x00020000
#define GPIO_pin_18 0x00040000
#define GPIO_pin_19 0x00080000
#define GPIO_pin_20 0x00100000
#define GPIO_pin_21 0x00200000
#define GPIO_pin_22 0x00400000
#define GPIO_pin_23 0x00800000
#define GPIO_pin_24 0x01000000
#define GPIO_pin_25 0x02000000
#define GPIO_pin_26 0x04000000
#define GPIO_pin_27 0x08000000
#define GPIO_pin_28 0x10000000
#define GPIO_pin_29 0x20000000
#define GPIO_pin_30 0x40000000
#define GPIO_pin_31 0x80000000

#define RESET_PIN_FAST_ADC 		0x00000001
#define RESET_PIN_SLOW_ADC 		0x00000002
#define RESET_PIN_ADCLK 		0x00000004
#define RESET_PIN_SAMPLE_CLK 	0x00000008
#define RESET_PIN_DEBUG_MEM 	0x00000010
#define RESET_PIN_DEBUG_8CH 	0x00000020
#define RESET_PIN_ENABLE_MEAS 	0x00000040
#define RESET_PIN_PLL 			0x00000080
#define SHIFT_ADC 				0x00000200


// GPIO PS
#define GPIOps_DEVICE_ID  	XPAR_XGPIOPS_0_DEVICE_ID

#define GPIOps_dir_output  0x0001
#define GPIOps_dir_input  0x0000

#define GPIOps_pin_MIO0  0
#define GPIOps_pin_MIO9  9
#define GPIOps_pin_MIO7  7

// XADC
#define XADC_DEVICE_ID 		XPAR_XADCPS_0_DEVICE_ID

#define printf				xil_printf


/*
 *  This is the size of the buffer to be transmitted/received in this example.
 */
#define BUFFER_SIZE		12


// I2C CPLD
#define IIC_DEVICE_ID		XPAR_XIICPS_0_DEVICE_ID

#define COMMAND2_FREE_1				0x00
#define COMMAND2_CONFIG_CPLD_B1		0x01
#define COMMAND2_CONFIG_CPLD_B2		0x02
#define COMMAND2_CONFIG_CPLD_B3		0x03
#define COMMAND2_FREE_2				0x04
#define COMMAND2_FREE_3				0x05
#define COMMAND2_CONFIG_READ_ADDR	0x06
#define COMMAND2_INIT_ADC			0x07

#define COMMAND1_CHIP_TRIMMER_0		0x00
#define COMMAND1_CHIP_TRIMMER_1		0x01
#define COMMAND1_CHIP_TRIMMER_2		0x02
#define COMMAND1_CHIP_TRIMMER_3		0x03
#define COMMAND1_CHIP_TRIMMER_4		0x04
#define COMMAND1_CHIP_TRIMMER_5		0x05

#define ADC_INIT_TYPE_FULL			0x00
#define ADC_INIT_TYPE_SHORT			0x01
#define ADC_PATTERN_NORMAL			0x00
#define ADC_PATTERN_SYNC			0x01
#define ADC_PATTERN_DESKEW			0x02
#define ADC_PATTERN_CUSTOM			0x03

#define COMMAND0_TRIMMER_A_ADDR		0x00
#define COMMAND0_TRIMMER_B_ADDR		0x01
#define COMMAND0_TRIMMER_C_ADDR		0x02
#define COMMAND0_TRIMMER_D_ADDR		0x03

#define COMMAND0_ADC_0_ADDR			0x01
#define COMMAND0_ADC_1_ADDR			0x02
#define COMMAND0_ADC_ALL_ADDR		0x03


/*
 * The slave address to send to and receive from.
 */
#define IIC_CPLD_SLAVE_ADDR		0x10
#define IIC_SCLK_RATE		100000

/*
 * The following constant controls the length of the buffers to be sent
 * and received with the IIC.
 */
//#define TEST_BUFFER_SIZE	2





/************************** Variable Definitions *****************************/
int frequency_interrupt = 10000;//1000; Hz
float T_proc=0.0001;
int frequency_PWM = 10000;
int duty_PWM = 50;

int frequency_interrupt_prev = 10000;
int frequency_PWM_prev = 10000;
int duty_PWM_prev = 50;

u32 pwm_max_points = 5000;	//5000->10kHz	frequency=100.000.000/(2*max_points)
int pwm_reference = 0;
u32 pwm_sine_freq = 50;


INTC InterruptController;  	/* The instance of the Interrupt Controller */
XTmrCtr TimerCounterInst;   /* The instance of the Timer Counter */
XGpio Gpio_FO12; 				/* The Instance of the GPIO Driver */
XGpio Gpio_FO3; 				/* The Instance of the GPIO Driver */
XGpio Gpio_reset; 				/* The Instance of the GPIO Driver */
XGpio Gpio_trip; 				/* The Instance of the GPIO Driver */
XGpio Gpio_constant; 				/* The Instance of the GPIO Driver */
XGpioPs Gpiops;				/* The driver instance for GPIO Device. */

static XAdcPs XAdcInst;     /* XADC driver instance */
XAdcPs *XAdcInstPtr = &XAdcInst;
XIicPs Iic;					/* Instance of the IIC Device */







/************************** Function Prototypes ******************************/

int Control_interrupt_init(void);
int TmrCtrSetupIntrSystem(INTC* IntcInstancePtr,
				XTmrCtr* InstancePtr,
				u16 DeviceId,
				u16 IntrId,
				u8 TmrCtrNumber);

void TimerCounterHandler(void *CallBackRef, u8 TmrCtrNumber);
void TmrCtrDisableIntr(INTC* IntcInstancePtr, u16 IntrId);
int Init_XAdc(u16 XAdcDeviceId);
int XAdcFractionToInt(float FloatNum);
int Interrupt_SetLoadReg(u32 RegisterValue);
int Gpio_PL_init(void);
int Gpio_ps_init(void);
int I2C_init(void);
int SPI_init(void);
int CPLD_init(void);
int Config_CPLD(u16 command2, u16 command1, u16 command0, u16 value);
void Reset_CPLD(void);
void init_PR_AXI(void);
int Trimmer_values_init(void);
int Read_comms_stats(void);
int Read_trip_triggered(void);
int Read_trip_instantaneous(void);
int Optical_Fibers_init(void);
int get_fast_adc_measurements(void);
int get_slow_adc_measurements(void);
int fast_adc_pattern(u16 adc_pattern);
int fast_adc_init(void);
int slow_adc_init(void);
int board_init(void);
void init_PR_axis(void);
void Control_voltage(void);
void init_PI_voltage(void);
u16 Protection_func(float Value, float max, float min);
float toFloat(int num,int bit_sig);
int toFix(float num,int bit_sig);
void reset_PI(void);
void begin_PR(void);
void Increment_control_PR(void);
void Increment_control_voltage_inner(void);
void Increment_control_voltage_intra(void);
void Increment_control_voltage_prom(void);
void plot_error_function(void);
/************************** Variable Definitions *****************************/
volatile u32 counter = 0;

#define B1 0
#define B2 1
#define B3 2

/*
 * The following buffers are used in this example to send and receive data
 * with the IIC.
 */
u8 RecvBuffer[19];    /**< Buffer for Receiving Data */

typedef struct _slow_adc_rxdata_struct
{
	u32 spi_busy;
	u32 spi_rxdata1;
	u32 spi_rxdata2;
	u32 spi_rxdata3;
	u32 spi_rxdata4;
	u32 spi_rxdata5;
	u32 spi_rxdata6;
} slow_adc_rxdata_struct;

typedef struct trimmer_chip_addr_struct
{
	u8 pot[4];
} trimmer_chip_addr_struct;

typedef struct _trimmer_bus_addr_struct
{
	trimmer_chip_addr_struct chip[6];
} trimmer_bus_addr_struct;

typedef struct _trimmer_struct
{
	trimmer_bus_addr_struct bus[3];
} trimmer_struct;

typedef struct _trip_detail_struct
{
	u32 slow_adc1;
	u32 slow_adc2;
	u16 fast_adc;
} trip_detail_struct;

typedef struct _trip_struct
{
	trip_detail_struct triggered;
	trip_detail_struct instantaneous;
	u8 general;
} trip_struct;

typedef struct _status_struct
{
	u16 comms_ok;
	u16 comms_fail;
} status_struct;

typedef struct _adc_input_struct
{
	short	ch1;
	short	ch2;
	short	ch3;
	short	ch4;
} adc_input_struct;

typedef struct _fast_adc_board_struct
{
	adc_input_struct	input1;
	adc_input_struct	input2;
	adc_input_struct	input3;
	adc_input_struct	input4;
} fast_adc_board_struct;

typedef struct _slow_adc_board_struct
{
	adc_input_struct	input1;
	adc_input_struct	input2;
	adc_input_struct	input3;
	adc_input_struct	input4;
	adc_input_struct	input5;
	adc_input_struct	input6;
} slow_adc_board_struct;

typedef struct _measure_slow_adc_struct
{
	slow_adc_board_struct	board1;
	slow_adc_board_struct	board2;
} measure_slow_adc_struct;

typedef struct _meas_struct
{
	fast_adc_board_struct	fast_adc;
	measure_slow_adc_struct	slow_adc;
} measure_struct;

typedef struct _data_struct
{
//	control_struct cntr;
	status_struct 	stat;
	trip_struct 	trip;
	trimmer_struct 	trimmer;
	measure_struct	measure;
} data_struct;

data_struct data;
slow_adc_rxdata_struct spi_rxdata;



#endif /* SRC_MAIN_H_ */
