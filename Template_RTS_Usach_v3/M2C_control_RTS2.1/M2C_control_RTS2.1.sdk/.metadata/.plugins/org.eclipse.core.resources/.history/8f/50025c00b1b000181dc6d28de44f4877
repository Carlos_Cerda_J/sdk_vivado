/*****************************************************************************/
/**
* @file  main.c
*
* This file contains a design example using the timer counter driver
* (XTmCtr) and hardware device using interrupt mode.This example assumes
* that the interrupt controller is also present as a part of the system
*
*
* MODIFICATION HISTORY:
*
* Ver   Who  Date	 Changes
* ---- ---- -------- -----------------------------------------------
* 1.00  DAG 30/07/18 First release
*
******************************************************************************/

/***************************** Include Files *********************************/
#include "xparameters.h"
#include "xil_exception.h"
#include "xil_printf.h"

#include "xscugic.h"	/* scu-GIC PS device driver (General Interrupt Controller) */
#include "xgpiops.h"	/* GPIO PS device driver */
#include "xiicps.h"		/* I2C PS device driver */
#include "xadcps.h"		/* ADC PS device driver (For internal temperature measure)*/

#include "xtmrctr.h"	/* Timer-Counter PL device driver */
#include "xgpio.h"		/* GPIO PL device driver */
#include "xspi.h"		/* SPI PL device driver */
#include "xspi_l.h"		/* SPI PL device macros driver */


/************************** Constant Definitions *****************************/
/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are only defined here such that a user can easily
 * change all the needed parameters in one place.
 */

#define TMRCTR_PWM_BASEADDR		XPAR_AXI_TIMER_PWM_BASEADDR
#define TMRCTR_INT_BASEADDR		XPAR_AXI_TIMER_INTERRUPT_TO_PS_BASEADDR


//Timer Interrupt
#define TMRCTR_DEVICE_ID	XPAR_TMRCTR_0_DEVICE_ID
#define TMRCTR_INTERRUPT_ID	XPAR_FABRIC_TMRCTR_0_VEC_ID

#define INTC_DEVICE_ID		XPAR_SCUGIC_SINGLE_DEVICE_ID


/*
 * In PWM Mode, Timer-counter 0 is used for period setting and Timer-counter 1 is used for duty cycle.
 */
#define TIMER_FOR_PERIOD 0
#define TIMER_FOR_DUTY	 1

#define AXI_CLOCK_FREQUENCY XPAR_AXI_TIMER_INTERRUPT_TO_PS_CLOCK_FREQ_HZ

/*
 * The following constant determines which timer counter of the device that is
 * used for this example, there are currently 2 timer counters in a device
 * and this example uses the first one, 0, the timer numbers are 0 based
 */
#define TIMER_CNTR_0	 0


/*
 * ScuGic manage interrupts in PS
 */
#define INTC			XScuGic
#define INTC_HANDLER	XScuGic_InterruptHandler


/*
 * The following constant is used to set the reset value of the timer counter,
 * making this number larger reduces the amount of time this example consumes
 * because it is the value the timer counter is loaded with when it is started
 */

/*
 * The following constant is used to determine which channel of the GPIO is
 * used. There are 2 channels supported.
 */
#define GPIO_DEVICE_ID  	XPAR_GPIO_0_DEVICE_ID

#define GPIO_CHANNEL 1

#define GPIO_pin_0  0x00000001
#define GPIO_pin_1  0x00000002
#define GPIO_pin_2  0x00000004
#define GPIO_pin_3  0x00000008
#define GPIO_pin_4  0x00000010
#define GPIO_pin_5  0x00000020
#define GPIO_pin_6  0x00000040
#define GPIO_pin_7  0x00000080
#define GPIO_pin_8  0x00000100
#define GPIO_pin_9  0x00000200
#define GPIO_pin_10 0x00000400
#define GPIO_pin_11 0x00000800
#define GPIO_pin_12 0x00001000
#define GPIO_pin_13 0x00002000
#define GPIO_pin_14 0x00004000
#define GPIO_pin_16 0x00008000
#define GPIO_pin_17 0x00010000
#define GPIO_pin_18 0x00020000
#define GPIO_pin_19 0x00040000
#define GPIO_pin_20 0x00080000
#define GPIO_pin_21 0x00100000
#define GPIO_pin_22 0x00200000
#define GPIO_pin_23 0x00400000
#define GPIO_pin_24 0x00800000
#define GPIO_pin_25 0x01000000
#define GPIO_pin_26 0x02000000
#define GPIO_pin_27 0x04000000
#define GPIO_pin_28 0x08000000
#define GPIO_pin_29 0x10000000
#define GPIO_pin_30 0x20000000
#define GPIO_pin_31 0x40000000
#define GPIO_pin_32 0x80000000

// GPIO PS
#define GPIOps_DEVICE_ID  	XPAR_XGPIOPS_0_DEVICE_ID

#define GPIOps_dir_output  0x0001
#define GPIOps_dir_input  0x0000

#define GPIOps_pin_MIO0  0
#define GPIOps_pin_MIO9  9
#define GPIOps_pin_MIO7  7

// XADC
#define XADC_DEVICE_ID 		XPAR_XADCPS_0_DEVICE_ID

#define printf				xil_printf


// SPI
#define SPI_DEVICE_ID		XPAR_SPI_0_DEVICE_ID
/*
 *  This is the size of the buffer to be transmitted/received in this example.
 */
#define BUFFER_SIZE		12


// I2C CPLD
#define IIC_DEVICE_ID		XPAR_XIICPS_1_DEVICE_ID

#define COMMAND2_FREE_1				0x00
#define COMMAND2_CONFIG_CPLD_B1	0x01
#define COMMAND2_CONFIG_CPLD_B2	0x02
#define COMMAND2_CONFIG_CPLD_B3	0x03
#define COMMAND2_CONFIG_ADC_REG		0x04
#define COMMAND2_CONFIG_ADC_PIN		0x05
#define COMMAND2_CONFIG_READ_ADDR	0x06
//#define COMMAND2_CPLD_AVALAIBLE		0x07

#define COMMAND1_CHIP_TRIMMER_0		0x00
#define COMMAND1_CHIP_TRIMMER_1		0x01
#define COMMAND1_CHIP_TRIMMER_2		0x02
#define COMMAND1_CHIP_TRIMMER_3		0x03
#define COMMAND1_CHIP_TRIMMER_4		0x04
#define COMMAND1_CHIP_TRIMMER_5		0x05

#define COMMAND1_ADC_PIN_RST		0x01
#define COMMAND1_ADC_PIN_PD			0x02

#define COMMAND0_TRIMMER_A_ADDR			0x00
#define COMMAND0_TRIMMER_B_ADDR			0x01
#define COMMAND0_TRIMMER_C_ADDR			0x02
#define COMMAND0_TRIMMER_D_ADDR			0x03

#define COMMAND0_ADC_0_ADDR				0x01
#define COMMAND0_ADC_1_ADDR				0x02

#define COMMAND0_I2C_B1_ADDR			0x01
#define COMMAND0_I2C_B2_ADDR			0x02
#define COMMAND0_I2C_B3_ADDR			0x03


/*
 * The slave address to send to and receive from.
 */
#define IIC_CPLD_SLAVE_ADDR		0x10
#define IIC_SCLK_RATE		100000

/*
 * The following constant controls the length of the buffers to be sent
 * and received with the IIC.
 */
//#define TEST_BUFFER_SIZE	2

/**************************** Type Definitions *******************************/


/***************** Macros (Inline Functions) Definitions *********************/



/************************** Variable Definitions *****************************/
u32 frequency_interrupt = 1;//1000; Hz
u32 frequency_PWM = 10000;
u32 duty_PWM = 50;

u32 frequency_interrupt_prev = 1;
u32 frequency_PWM_prev = 10000;
u32 duty_PWM_prev = 50;


u16 command = 1;
unsigned short data = 5;
u16 frame;

u16 debug_data_I2C = 0;

INTC InterruptController;  	/* The instance of the Interrupt Controller */
XTmrCtr TimerCounterInst;   /* The instance of the Timer Counter */
XGpio Gpio; 				/* The Instance of the GPIO Driver */
XGpioPs Gpiops;				/* The driver instance for GPIO Device. */
XSpi  SpiInstance;	 		/* The instance of the SPI device */
static XAdcPs XAdcInst;     /* XADC driver instance */
XAdcPs *XAdcInstPtr = &XAdcInst;
XIicPs Iic;					/* Instance of the IIC Device */

/************************** Function Prototypes ******************************/

int Control_interrupt_init(void);

int TmrCtrSetupIntrSystem(INTC* IntcInstancePtr,
				XTmrCtr* InstancePtr,
				u16 DeviceId,
				u16 IntrId,
				u8 TmrCtrNumber);

void TimerCounterHandler(void *CallBackRef, u8 TmrCtrNumber);
void TmrCtrDisableIntr(INTC* IntcInstancePtr, u16 IntrId);

int Init_XAdc(u16 XAdcDeviceId);
int XAdcFractionToInt(float FloatNum);
int Interrupt_SetLoadReg(u32 RegisterValue);
int PWM_SetLoadReg(u8 TmrCtrNumber, u32 RegisterValue);
int Gpio_init(void);
int Gpio_ps_init(void);
int PWM_init();
int SPI_init(void);
int I2C_init(void);
int CPLD_init(void);
int Config_CPLD(u16 command2, u16 command1, u16 command0, u16 value);
void Reset_CPLD(void);

/************************** Variable Definitions *****************************/
volatile u16 counter = 0;

/*
 * The following variables are used to read and write to the  Spi device, they
 * are global to avoid having large buffers on the stack.
 */
u8 ReadBuffer[BUFFER_SIZE];
u8 WriteBuffer[BUFFER_SIZE];


/*
 * The following buffers are used in this example to send and receive data
 * with the IIC.
 */
//u8 SendBuffer[TEST_BUFFER_SIZE] = {17};    /**< Buffer for Transmitting Data */
//u8 RecvBuffer[TEST_BUFFER_SIZE];    /**< Buffer for Receiving Data */



/*****************************************************************************/
/**
* This function is the main function.
*
* @param	None.
*
* @return	XST_SUCCESS to indicate success, else XST_FAILURE to indicate a
*		Failure.
*
* @note		None.
*
******************************************************************************/
int main(void)
{
	int Status;
	u32 aux;

	xil_printf("\r\n\nTemplate v3 Zynq 7020\r\n");

	/* Initialize AXI timer for PS periodic interrupts. */
	Status = Control_interrupt_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("Control_interrupt Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/* Initialize PWM timer-counters. */
	Status = PWM_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("PWM Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/*
	 * Initialize the FPGA GPIO driver
	 */
	Status = Gpio_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("Gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/*
	 * Initialize the processor GPIO driver
	 */
	Status = Gpio_ps_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("Gpio_ps Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/*
	 * Initialize the XADC driver
	 */
	Status = Init_XAdc(XADC_DEVICE_ID);
	if (Status != XST_SUCCESS)
	{
		xil_printf("xADC_PS Initializacion Failed\r\n");
		return XST_FAILURE;
	}


	/*
	 * Initialize the I2C driver
	 */
	Status = I2C_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("I2C Initializacion Failed\r\n");
		return XST_FAILURE;
	}


	/*
	 * Initialize the SPI driver
	 */
	Status = SPI_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("SPI Initializacion Failed\r\n");
		return XST_FAILURE;
	}


	/*
	 * Initialize CPLD via I2C Communication
	 */
	Status = CPLD_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("I2C Initializacion Failed\r\n");
		return XST_FAILURE;
	}

	while (1)
	{
		/* Clear GPIO bit 0: Reset CPLD */
		XGpio_DiscreteClear(&Gpio, GPIO_CHANNEL, GPIO_pin_0);

		/* Clear on Board Led MIO7 */
		XGpioPs_WritePin(&Gpiops, GPIOps_pin_MIO7, 0x0);


		/*
		 * Initialize the send buffer bytes with a pattern to send and the
		 * the receive buffer bytes to zero to allow the receive data to be
		 * verified.
		 */

		/* SET GPIO bit 0: un-Reset CPLD */
		XGpio_DiscreteWrite(&Gpio, GPIO_CHANNEL, GPIO_pin_0);

		// CONFIG_ADC0_PIN
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_ADC_PIN, COMMAND1_ADC_PIN_PD, COMMAND0_ADC_0_ADDR, 1);
//		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_ADC_PIN, COMMAND1_ADC_PIN_RST, COMMAND0_ADC_0_ADDR, 1);

//		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_ADC_PIN, COMMAND1_ADC_PIN_PD, COMMAND0_ADC_0_ADDR, 0);
//		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_ADC_PIN, COMMAND1_ADC_PIN_RST, COMMAND0_ADC_0_ADDR, 0);

		// CONFIG_ADC1_PIN
//		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_ADC_PIN, COMMAND1_ADC_PIN_PD, COMMAND0_ADC_1_ADDR, 1);
//		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_ADC_PIN, COMMAND1_ADC_PIN_RST, COMMAND0_ADC_1_ADDR, 1);

//		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_ADC_PIN, COMMAND1_ADC_PIN_PD, COMMAND0_ADC_1_ADDR, 0);
//		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_ADC_PIN, COMMAND1_ADC_PIN_RST, COMMAND0_ADC_1_ADDR, 0);

		// CONFIG_ADC_REG
//		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_ADC_REG, 0, COMMAND0_ADC_0_ADDR, 129);
//		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_ADC_REG, 0, COMMAND0_ADC_1_ADDR, 126);

		// CONFIG READ ADDRESS
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 0);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 1);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 2);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 3);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 4);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 5);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 6);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 7);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 8);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 9);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 10);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 11);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 12);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 13);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 14);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 15);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 16);
		Config_CPLD(COMMAND2_CONFIG_READ_ADDR, 0, 0, 17);
		// READ TRIP

		// CONFIG_TRIMMER_B1
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_A_ADDR, 126);
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_B_ADDR, 125);
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_C_ADDR, 124);
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_D_ADDR, 123);

		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_A_ADDR, 126);
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_B_ADDR, 126);
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_C_ADDR, 126);
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_1, COMMAND0_TRIMMER_D_ADDR, 126);

		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_A_ADDR, 126);
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_B_ADDR, 126);
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_C_ADDR, 126);
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_2, COMMAND0_TRIMMER_D_ADDR, 126);

		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_A_ADDR, 126);
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_B_ADDR, 126);
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_C_ADDR, 126);
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_3, COMMAND0_TRIMMER_D_ADDR, 126);

		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_4, COMMAND0_TRIMMER_A_ADDR, 126);
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_4, COMMAND0_TRIMMER_B_ADDR, 126);
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_4, COMMAND0_TRIMMER_C_ADDR, 126);
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_4, COMMAND0_TRIMMER_D_ADDR, 126);

		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_5, COMMAND0_TRIMMER_A_ADDR, 126);
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_5, COMMAND0_TRIMMER_B_ADDR, 126);
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_5, COMMAND0_TRIMMER_C_ADDR, 126);
		Config_CPLD(COMMAND2_CONFIG_CPLD_B1, COMMAND1_CHIP_TRIMMER_5, COMMAND0_TRIMMER_D_ADDR, 126);

		// CONFIG_TRIMMER_B2
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B2, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_A_ADDR, 126);

		// CONFIG_TRIMMER_B3
		Reset_CPLD();
		Config_CPLD(COMMAND2_CONFIG_CPLD_B3, COMMAND1_CHIP_TRIMMER_0, COMMAND0_TRIMMER_A_ADDR, 126);


		// Change PWM duty cycle
		if(duty_PWM != duty_PWM_prev)
		{
			duty_PWM_prev = duty_PWM;

			aux = ((AXI_CLOCK_FREQUENCY / frequency_PWM) - 2) * 0.01 * duty_PWM;

			PWM_SetLoadReg(TIMER_FOR_DUTY, aux);
		}

		// Change PWM frequency and adjust duty cycle for new frequency
		if(frequency_PWM != frequency_PWM_prev)
		{
			frequency_PWM_prev = frequency_PWM;

			aux = (AXI_CLOCK_FREQUENCY / frequency_PWM) - 2;
			PWM_SetLoadReg(TIMER_FOR_PERIOD, aux);

			aux = aux * 0.01 * duty_PWM;
			PWM_SetLoadReg(TIMER_FOR_DUTY, aux);
		}

		// Change control interrupt frequency
		if(frequency_interrupt != frequency_interrupt_prev)
		{
			frequency_interrupt_prev = frequency_interrupt;

			aux = (AXI_CLOCK_FREQUENCY / frequency_interrupt) - 2;
			Interrupt_SetLoadReg(aux);
		}

	}

	return XST_SUCCESS;
}

void Reset_CPLD(void)
{
	/* Clear GPIO bit 0: Reset CPLD */
	XGpio_DiscreteClear(&Gpio, GPIO_CHANNEL, GPIO_pin_0);

	for(u16 i=0; i<10 ; i++)
	{
		//NOP
	}

	/* SET GPIO bit 0: un-Reset CPLD */
	XGpio_DiscreteWrite(&Gpio, GPIO_CHANNEL, GPIO_pin_0);
}


int Config_CPLD(u16 command2, u16 command1, u16 command0, u16 value)
{
	u16 send_buffer_size = 0;				// Init at 0. updated later.
	u8 SendBuffer[2];						// Max size of buffer

	if((command2 == 0x0) || (command2 == 0x7))		// These commands are not implemented
	{
		return XST_FAILURE;							// Nothing to do. Return error.
	}
	else											// All another commands require 2 byte of data
	{
		send_buffer_size = 2;
		SendBuffer[1] = value & 0xff;				// messsage 2
	}

	SendBuffer[0] = ((command2 & 0x07) << 5) | ((command1 & 0x07) << 2) | (command0 & 0x03);	//message 1

	/*
	 * Send the buffer using the IIC and ignore the number of bytes sent as the return value
	 */
	XIicPs_MasterSendPolled(&Iic, &SendBuffer, send_buffer_size, IIC_CPLD_SLAVE_ADDR);

	/*
	 * Wait until bus is idle to start another transfer.
	 */
	while (XIicPs_BusIsBusy(&Iic))
	{
		//NOP
	}

	return XST_SUCCESS;
}


int CPLD_init(void)
{
	//int Status;



/*
 * Initialize the send buffer bytes with a pattern to send and the
 * the receive buffer bytes to zero to allow the receive data to be
 * verified.
 */
//	for (Index = 0; Index < TEST_BUFFER_SIZE; Index++) {
//		SendBuffer[Index] = (Index % TEST_BUFFER_SIZE);
//		RecvBuffer[Index] = 0;
//	}

/*
 * Send the buffer using the IIC and ignore the number of bytes sent
 * as the return value since we are using it in interrupt mode.
 */
//	Status = XIicPs_MasterSendPolled(&Iic, SendBuffer,
//			 TEST_BUFFER_SIZE, IIC_CPLD_SLAVE_ADDR);
//	if (Status != XST_SUCCESS) {
//		return XST_FAILURE;
//	}

/*
 * Wait until bus is idle to start another transfer.
 */
//	while (XIicPs_BusIsBusy(&Iic)) {
	/* NOP */
//	}

//	Status = XIicPs_MasterRecvPolled(&Iic, RecvBuffer,
//			  TEST_BUFFER_SIZE, IIC_CPLD_SLAVE_ADDR);
//	if (Status != XST_SUCCESS) {
//		return XST_FAILURE;
//	}


return XST_SUCCESS;
}


/*****************************************************************************/
/**
* This function is the handler which performs processing for the timer counter.
* It is called from an interrupt context such that the amount of processing
* performed should be minimized.  It is called when the timer counter expires
* if interrupts are enabled.
*
* This handler provides an example of how to handle timer counter interrupts
* but is application specific.
*
* @param	CallBackRef is a pointer to the callback function
* @param	TmrCtrNumber is the number of the timer to which this
*		handler is associated with.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
void TimerCounterHandler(void *CallBackRef, u8 TmrCtrNumber)
{
	XTmrCtr *InstancePtr = (XTmrCtr *)CallBackRef;

	/*
	 * Check if the timer counter has expired, checking is not necessary
	 * since that's the reason this function is executed, this just shows
	 * how the callback reference can be used as a pointer to the instance
	 * of the timer counter that expired.
	 */
	if (XTmrCtr_IsExpired(InstancePtr, TmrCtrNumber))
	{
//		XGpio_DiscreteWrite(&Gpio, GPIO_CHANNEL, GPIO_pin_0);

		/* Set the on Board Led MIO7 */
		XGpioPs_WritePin(&Gpiops, GPIOps_pin_MIO7, 0x1);

		counter++;
		xil_printf("Timer_Interrupt: %d\n", counter);

		u32 TempRawData;
		float TempData;
		/*
		 * Read the on-chip Temperature Data (Current/Maximum/Minimum)
		 * from the ADC data registers.
		 */
		TempRawData = XAdcPs_GetAdcData(XAdcInstPtr, XADCPS_CH_TEMP);
		TempData = XAdcPs_RawToTemperature(TempRawData);
		printf("Dice Temperature is %0d Centigrades.\r\n\n", (int)(TempData));
	}
}



/****************************************************************************/
/**
*
* This function runs a test on the XADC/ADC device using the
* driver APIs.
* This function does the following tasks:
*	- Initiate the XADC device driver instance
*	- Run self-test on the device
*	- Setup the sequence registers to continuously monitor on-chip
*	temperature and, VCCPINT, VCCPAUX and VCCPDRO Voltages
*	- Setup configuration registers to start the sequence
*	- Read the latest on-chip temperature and, VCCPINT, VCCPAUX and VCCPDRO
*	  Voltages
*
* @param	XAdcDeviceId is the XPAR_<SYSMON_ADC_instance>_DEVICE_ID value
*		from xparameters.h.
*
* @return
*		- XST_SUCCESS if the example has completed successfully.
*		- XST_FAILURE if the example has failed.
*
* @note   	None
*
****************************************************************************/
int Init_XAdc(u16 XAdcDeviceId)
{
	int Status;
	XAdcPs_Config *ConfigPtr;
	u32 TempRawData;
	float TempData;
	float MaxData;
	float MinData;

	/*
	 * Initialize the XAdc driver.
	 */
	ConfigPtr = XAdcPs_LookupConfig(XAdcDeviceId);
	if (ConfigPtr == NULL) {
		return XST_FAILURE;
	}
	XAdcPs_CfgInitialize(XAdcInstPtr, ConfigPtr,
				ConfigPtr->BaseAddress);

	/*
	 * Self Test the XADC/ADC device
	 */
	Status = XAdcPs_SelfTest(XAdcInstPtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Disable the Channel Sequencer before configuring the Sequence
	 * registers.
	 */
	XAdcPs_SetSequencerMode(XAdcInstPtr, XADCPS_SEQ_MODE_SAFE);
	/*
	 * Read the on-chip Temperature Data (Current/Maximum/Minimum)
	 * from the ADC data registers.
	 */
	TempRawData = XAdcPs_GetAdcData(XAdcInstPtr, XADCPS_CH_TEMP);
	TempData = XAdcPs_RawToTemperature(TempRawData);
	printf("\r\nThe Current Temperature is %0d.%03d Centigrades.\r\n",
				(int)(TempData), XAdcFractionToInt(TempData));

	TempRawData = XAdcPs_GetMinMaxMeasurement(XAdcInstPtr, XADCPS_MAX_TEMP);
	MaxData = XAdcPs_RawToTemperature(TempRawData);
	printf("The Maximum Temperature is %0d.%03d Centigrades. \r\n",
				(int)(MaxData), XAdcFractionToInt(MaxData));

	TempRawData = XAdcPs_GetMinMaxMeasurement(XAdcInstPtr, XADCPS_MIN_TEMP);
	MinData = XAdcPs_RawToTemperature(TempRawData & 0xFFF0);
	printf("The Minimum Temperature is %0d.%03d Centigrades. \r\n",
				(int)(MinData), XAdcFractionToInt(MinData));


	return XST_SUCCESS;
}

/*****************************************************************************/
/**
* This function does a minimal test on the timer counter device and driver as a
* design example.  The purpose of this function is to illustrate how to use the
* XTmrCtr component.  It initializes a timer counter and then sets it up in
* compare mode with auto reload such that a periodic interrupt is generated.
*
* This function uses interrupt driven mode of the timer counter.
*
* @param	IntcInstancePtr is a pointer to the Interrupt Controller
*		driver Instance
* @param	TmrCtrInstancePtr is a pointer to the XTmrCtr driver Instance
* @param	DeviceId is the XPAR_<TmrCtr_instance>_DEVICE_ID value from
*		xparameters.h
* @param	IntrId is XPAR_<INTC_instance>_<TmrCtr_instance>_INTERRUPT_INTR
*		value from xparameters.h
* @param	TmrCtrNumber is the number of the timer to which this
*		handler is associated with.
*
* @return	XST_SUCCESS if the Test is successful, otherwise XST_FAILURE
*
* @note		This function contains an infinite loop such that if interrupts
*		are not working it may never return.
*
*****************************************************************************/
int Control_interrupt_init(void)
{
int Status;

INTC* IntcInstancePtr = &InterruptController;
XTmrCtr* TmrCtrInstancePtr = &TimerCounterInst;
u16 DeviceId = TMRCTR_DEVICE_ID;
u16 IntrId = TMRCTR_INTERRUPT_ID;
u8 TmrCtrNumber = TIMER_CNTR_0;

u32 RESET_VALUE = (AXI_CLOCK_FREQUENCY / frequency_interrupt) - 2; 	//PWM_PERIOD = (TLR0 + 2) * AXI_CLOCK_PERIOD

/*
 * Initialize the timer counter so that it's ready to use,
 * specify the device ID that is generated in xparameters.h
 */
Status = XTmrCtr_Initialize(TmrCtrInstancePtr, DeviceId);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

/*
 * Perform a self-test to ensure that the hardware was built
 * correctly, use the 1st timer in the device (0)
 */
Status = XTmrCtr_SelfTest(TmrCtrInstancePtr, TmrCtrNumber);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

/*
 * Connect the timer counter to the interrupt subsystem such that
 * interrupts can occur.  This function is application specific.
 */
Status = TmrCtrSetupIntrSystem(IntcInstancePtr,
				TmrCtrInstancePtr,
				DeviceId,
				IntrId,
				TmrCtrNumber);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

/*
 * Setup the handler for the timer counter that will be called from the
 * interrupt context when the timer expires, specify a pointer to the
 * timer counter driver instance as the callback reference so the handler
 * is able to access the instance data
 */
XTmrCtr_SetHandler(TmrCtrInstancePtr, TimerCounterHandler,
				   TmrCtrInstancePtr);

/*
 * Enable the interrupt of the timer counter so interrupts will occur
 * and use auto reload mode such that the timer counter will reload
 * itself automatically and continue repeatedly, without this option
 * it would expire once only. Also the timer count down to 0
 */
XTmrCtr_SetOptions(TmrCtrInstancePtr, TmrCtrNumber,
			XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION  | XTC_DOWN_COUNT_OPTION);

/*
 * Set a reset value for the timer counter such that it will expire
 * eariler than letting it roll over from 0, the reset value is loaded
 * into the timer counter when it is started
 */
XTmrCtr_SetResetValue(TmrCtrInstancePtr, TmrCtrNumber, RESET_VALUE);

/*
 * Start the timer counter such that it's incrementing by default,
 * then wait for it to timeout a number of times
 */
XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);


//TmrCtrDisableIntr(IntcInstancePtr, DeviceId);
return XST_SUCCESS;
}


int PWM_init(void)
{
u32 ControlStatus;
u32 PERIOD = (AXI_CLOCK_FREQUENCY / frequency_PWM) - 2; 	//PWM_PERIOD = (TLR0 + 2) * AXI_CLOCK_PERIOD
u32 DUTY = PERIOD * 0.01 * duty_PWM;						//PWM_HIGH_TIME = (TLR1 + 2) * AXI_CLOCK_PERIOD

//-----------------------------------------------------------------------
// Timer-counter for period
//-----------------------------------------------------------------------

/*
 * Clear the Control Status Register
 */
XTmrCtr_SetControlStatusReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_PERIOD,0x0);

/*
 * Set the value that is loaded into the timer counter and cause it to
 * be loaded into the timer counter
 */
XTmrCtr_SetLoadReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_PERIOD, PERIOD);
XTmrCtr_LoadTimerCounterReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_PERIOD);

/*
 * Clear the Load Timer bit in the Control Status Register
 */
ControlStatus = XTmrCtr_GetControlStatusReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_PERIOD);
XTmrCtr_SetControlStatusReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_PERIOD,
			 ControlStatus & (~XTC_CSR_LOAD_MASK));

/*
 * Set options for timer-counter: Automatic reload, activate generated output signal, down counter, enable PWM mode
 */
ControlStatus = XTmrCtr_GetControlStatusReg(TMRCTR_PWM_BASEADDR,
		TIMER_FOR_PERIOD);
XTmrCtr_SetControlStatusReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_PERIOD,
			 ControlStatus | XTC_CSR_AUTO_RELOAD_MASK | XTC_CSR_EXT_GENERATE_MASK | XTC_CSR_DOWN_COUNT_MASK | XTC_CSR_ENABLE_PWM_MASK);


//-----------------------------------------------------------------------
// Timer-counter for duty value
//-----------------------------------------------------------------------

/*
 * Clear the Control Status Register
 */
XTmrCtr_SetControlStatusReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_DUTY,0x0);

/*
 * Set the value that is loaded into the timer counter and cause it to
 * be loaded into the timer counter
 */
XTmrCtr_SetLoadReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_DUTY, DUTY);
XTmrCtr_LoadTimerCounterReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_DUTY);

/*
 * Clear the Load Timer bit in the Control Status Register
 */
ControlStatus = XTmrCtr_GetControlStatusReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_DUTY);
XTmrCtr_SetControlStatusReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_DUTY,
			 ControlStatus & (~XTC_CSR_LOAD_MASK));

/*
 * Set options for timer-counter: Automatic reload, activate generated output signal, down counter, enable PWM mode
 */
ControlStatus = XTmrCtr_GetControlStatusReg(TMRCTR_PWM_BASEADDR,
		TIMER_FOR_DUTY);
XTmrCtr_SetControlStatusReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_DUTY,
			 ControlStatus | XTC_CSR_AUTO_RELOAD_MASK | XTC_CSR_EXT_GENERATE_MASK | XTC_CSR_DOWN_COUNT_MASK | XTC_CSR_ENABLE_PWM_MASK);

/*
 * Enable both timers
 */
ControlStatus = XTmrCtr_GetControlStatusReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_PERIOD);
XTmrCtr_SetControlStatusReg(TMRCTR_PWM_BASEADDR, TIMER_FOR_PERIOD, ControlStatus | XTC_CSR_ENABLE_ALL_MASK);

/*
 * Disable the timer counter such that it stops incrementing
 */
//	XTmrCtr_Disable(TmrCtrBaseAddress, TmrCtrNumber);

return XST_SUCCESS;
}


/*****************************************************************************/
/**
* This function setups the interrupt system such that interrupts can occur
* for the timer counter. This function is application specific since the actual
* system may or may not have an interrupt controller.  The timer counter could
* be directly connected to a processor without an interrupt controller.  The
* user should modify this function to fit the application.
*
* @param	IntcInstancePtr is a pointer to the Interrupt Controller
*		driver Instance.
* @param	TmrCtrInstancePtr is a pointer to the XTmrCtr driver Instance.
* @param	DeviceId is the XPAR_<TmrCtr_instance>_DEVICE_ID value from
*		xparameters.h.
* @param	IntrId is XPAR_<INTC_instance>_<TmrCtr_instance>_VEC_ID
*		value from xparameters.h.
* @param	TmrCtrNumber is the number of the timer to which this
*		handler is associated with.
*
* @return	XST_SUCCESS if the Test is successful, otherwise XST_FAILURE.
*
* @note		This function contains an infinite loop such that if interrupts
*		are not working it may never return.
*
******************************************************************************/
int TmrCtrSetupIntrSystem(INTC* IntcInstancePtr,
			 XTmrCtr* TmrCtrInstancePtr,
			 u16 DeviceId,
			 u16 IntrId,
			 u8 TmrCtrNumber)
{
 int Status;

#ifdef XPAR_INTC_0_DEVICE_ID
#ifndef TESTAPP_GEN
/*
 * Initialize the interrupt controller driver so that
 * it's ready to use, specify the device ID that is generated in
 * xparameters.h
 */
Status = XIntc_Initialize(IntcInstancePtr, INTC_DEVICE_ID);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}
#endif
/*
 * Connect a device driver handler that will be called when an interrupt
 * for the device occurs, the device driver handler performs the specific
 * interrupt processing for the device
 */
Status = XIntc_Connect(IntcInstancePtr, IntrId,
			(XInterruptHandler)XTmrCtr_InterruptHandler,
			(void *)TmrCtrInstancePtr);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

#ifndef TESTAPP_GEN
/*
 * Start the interrupt controller such that interrupts are enabled for
 * all devices that cause interrupts, specific real mode so that
 * the timer counter can cause interrupts thru the interrupt controller.
 */
Status = XIntc_Start(IntcInstancePtr, XIN_REAL_MODE);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}
#endif

/*
 * Enable the interrupt for the timer counter
 */
XIntc_Enable(IntcInstancePtr, IntrId);

#else

#ifndef TESTAPP_GEN
XScuGic_Config *IntcConfig;

/*
 * Initialize the interrupt controller driver so that it is ready to
 * use.
 */
IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
if (NULL == IntcConfig) {
	return XST_FAILURE;
}

Status = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
				IntcConfig->CpuBaseAddress);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}
#endif /* TESTAPP_GEN */

XScuGic_SetPriorityTriggerType(IntcInstancePtr, IntrId,
				0xA0, 0x3);

/*
 * Connect the interrupt handler that will be called when an
 * interrupt occurs for the device.
 */
Status = XScuGic_Connect(IntcInstancePtr, IntrId,
			 (Xil_ExceptionHandler)XTmrCtr_InterruptHandler,
			 TmrCtrInstancePtr);
if (Status != XST_SUCCESS) {
	return Status;
}

/*
 * Enable the interrupt for the Timer device.
 */
XScuGic_Enable(IntcInstancePtr, IntrId);
#endif /* XPAR_INTC_0_DEVICE_ID */


#ifndef TESTAPP_GEN
/*
 * Initialize the exception table.
 */
Xil_ExceptionInit();

/*
 * Register the interrupt controller handler with the exception table.
 */
Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
				(Xil_ExceptionHandler)
				INTC_HANDLER,
				IntcInstancePtr);

/*
 * Enable non-critical exceptions.
 */
Xil_ExceptionEnable();

#endif
return XST_SUCCESS;
}


/******************************************************************************/
/**
*
* This function disables the interrupts for the Timer.
*
* @param	IntcInstancePtr is a reference to the Interrupt Controller
*		driver Instance.
* @param	IntrId is XPAR_<INTC_instance>_<Timer_instance>_VEC_ID
*		value from xparameters.h.
*
* @return	None.
*
* @note		None.
*
******************************************************************************/
void TmrCtrDisableIntr(INTC* IntcInstancePtr, u16 IntrId)
{
/*
 * Disable the interrupt for the timer counter
 */
#ifdef XPAR_INTC_0_DEVICE_ID
XIntc_Disable(IntcInstancePtr, IntrId);
#else
/* Disconnect the interrupt */
XScuGic_Disable(IntcInstancePtr, IntrId);
XScuGic_Disconnect(IntcInstancePtr, IntrId);
#endif

return;
}


/****************************************************************************/
/**
*
* This function converts the fraction part of the given floating point number
* (after the decimal point)to an integer.
*
* @param	FloatNum is the floating point number.
*
* @return	Integer number to a precision of 3 digits.
*
* @note
* This function is used in the printing of floating point data to a STDIO device
* using the xil_printf function. The xil_printf is a very small foot-print
* printf function and does not support the printing of floating point numbers.
*
*****************************************************************************/
int XAdcFractionToInt(float FloatNum)
{
float Temp;

Temp = FloatNum;
if (FloatNum < 0) {
	Temp = -(FloatNum);
}

return( ((int)((Temp -(float)((int)Temp)) * (1000.0f))));
}


int Interrupt_SetLoadReg(u32 RegisterValue)
{
XTmrCtr* TmrCtrInstancePtr = &TimerCounterInst;
u8 TmrCtrNumber = TIMER_CNTR_0;

/*
 * Set a reset value for the timer counter such that it will expire
 * eariler than letting it roll over from 0, the reset value is loaded
 * into the timer counter when it is started
 */
XTmrCtr_SetResetValue(TmrCtrInstancePtr, TmrCtrNumber, RegisterValue);

/*
 * Start the timer counter such that it's incrementing by default,
 * then wait for it to timeout a number of times
 */
XTmrCtr_Start(TmrCtrInstancePtr, TmrCtrNumber);

return XST_SUCCESS;
}


int PWM_SetLoadReg(u8 TmrCtrNumber, u32 RegisterValue)
{
u32 ControlStatus;

/*
 * Set new Load register value and activate Load bit
 */
XTmrCtr_SetLoadReg(TMRCTR_PWM_BASEADDR, TmrCtrNumber, RegisterValue);
XTmrCtr_LoadTimerCounterReg(TMRCTR_PWM_BASEADDR, TmrCtrNumber);

/*
 * Clear the Load Timer bit in the Control Status Register
 */
ControlStatus = XTmrCtr_GetControlStatusReg(TMRCTR_PWM_BASEADDR,
		TmrCtrNumber);
XTmrCtr_SetControlStatusReg(TMRCTR_PWM_BASEADDR, TmrCtrNumber,
			 ControlStatus & (~XTC_CSR_LOAD_MASK));

return XST_SUCCESS;
}


int Gpio_init(void)
{
int Status;

/* Initialize the GPIO driver */
Status = XGpio_Initialize(&Gpio, GPIO_DEVICE_ID);
if (Status != XST_SUCCESS) {
	xil_printf("Gpio Initialization Failed\r\n");
	return XST_FAILURE;
}

/* Set the direction for all signals as outputs */
XGpio_SetDataDirection(&Gpio, GPIO_CHANNEL, 0xFFFFFFFF);

return XST_SUCCESS;
}


int Gpio_ps_init(void)
{
int Status;
XGpioPs_Config *ConfigPtr;

/* Initialize the GPIO driver. */
ConfigPtr = XGpioPs_LookupConfig(GPIOps_DEVICE_ID);
Status = XGpioPs_CfgInitialize(&Gpiops, ConfigPtr,
				ConfigPtr->BaseAddr);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

/*
 * Set the direction for the pin to be output and
 * Enable the Output enable for MIO Pins.
 */
XGpioPs_SetDirectionPin(&Gpiops, GPIOps_pin_MIO0, GPIOps_dir_output);
XGpioPs_SetDirectionPin(&Gpiops, GPIOps_pin_MIO7, GPIOps_dir_output);
XGpioPs_SetDirectionPin(&Gpiops, GPIOps_pin_MIO9, GPIOps_dir_output);

XGpioPs_SetOutputEnablePin(&Gpiops, GPIOps_pin_MIO0, 1);
XGpioPs_SetOutputEnablePin(&Gpiops, GPIOps_pin_MIO7, 1);
XGpioPs_SetOutputEnablePin(&Gpiops, GPIOps_pin_MIO9, 1);

XGpioPs_WritePin(&Gpiops, GPIOps_pin_MIO7, 0x1);

return XST_SUCCESS;
}


int SPI_init(void)
{
int Status;
//	u32 Count;
//	u8 Test;
XSpi_Config *ConfigPtr;	/* Pointer to Configuration data */

XSpi *SpiInstancePtr = &SpiInstance;
u16 SpiDeviceId = SPI_DEVICE_ID;

/*
 * Initialize the SPI driver so that it is  ready to use.
 */
ConfigPtr = XSpi_LookupConfig(SpiDeviceId);
if (ConfigPtr == NULL) {
	return XST_DEVICE_NOT_FOUND;
}

Status = XSpi_CfgInitialize(SpiInstancePtr, ConfigPtr,
			  ConfigPtr->BaseAddress);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

/*
 * Perform a self-test to ensure that the hardware was built correctly.
 */
Status = XSpi_SelfTest(SpiInstancePtr);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

/*
 * Run loopback test only in case of standard SPI mode.
 */
if (SpiInstancePtr->SpiMode != XSP_STANDARD_MODE) {
	return XST_SUCCESS;
}

/*
 * Set the Spi device as a master and in loopback mode.
 */
Status = XSpi_SetOptions(SpiInstancePtr, XSP_MASTER_OPTION);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}


/*
 * Start the SPI driver so that the device is enabled.
 */
XSpi_Start(SpiInstancePtr);

/*
 * Disable Global interrupt to use polled mode operation
 */
//	XSpi_IntrGlobalDisable(SpiInstancePtr);

/*
 * Initialize the write buffer with pattern to write, initialize the
 * read buffer to zero so it can be verified after the read, the
 * Test value that is added to the unique value allows the value to be
 * changed in a debug environment.
 */
//	Test = 0x10;
//	for (Count = 0; Count < BUFFER_SIZE; Count++) {
//		WriteBuffer[Count] = (u8)(Count + Test);
//		ReadBuffer[Count] = 0;
//	}


/*
 * Transmit the data.
 */
//	XSpi_Transfer(SpiInstancePtr, WriteBuffer, ReadBuffer, BUFFER_SIZE);

/*
 * Compare the data received with the data that was transmitted.
 */
//	for (Count = 0; Count < BUFFER_SIZE; Count++) {
//		if (WriteBuffer[Count] != ReadBuffer[Count]) {
//			return XST_FAILURE;
//		}
//	}

//XSpi_IntrGlobalEnable(SpiInstancePtr);

return XST_SUCCESS;
}



int I2C_init(void)
{
int Status;
XIicPs_Config *Config;
//	int Index;
u16 DeviceId = IIC_DEVICE_ID;

/*
 * Initialize the IIC driver so that it's ready to use
 * Look up the configuration in the config table,
 * then initialize it.
 */
Config = XIicPs_LookupConfig(DeviceId);
if (NULL == Config) {
	return XST_FAILURE;
}

Status = XIicPs_CfgInitialize(&Iic, Config, Config->BaseAddress);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

/*
 * Perform a self-test to ensure that the hardware was built correctly.
 */
Status = XIicPs_SelfTest(&Iic);
if (Status != XST_SUCCESS) {
	return XST_FAILURE;
}

/*
 * Set the IIC serial clock rate.
 */
XIicPs_SetSClk(&Iic, IIC_SCLK_RATE);




/*
 * Initialize the send buffer bytes with a pattern to send and the
 * the receive buffer bytes to zero to allow the receive data to be
 * verified.
 */
//	for (Index = 0; Index < TEST_BUFFER_SIZE; Index++) {
//		SendBuffer[Index] = (Index % TEST_BUFFER_SIZE);
//		RecvBuffer[Index] = 0;
//	}

/*
 * Send the buffer using the IIC and ignore the number of bytes sent
 * as the return value since we are using it in interrupt mode.
 */
//	Status = XIicPs_MasterSendPolled(&Iic, SendBuffer,
//			 TEST_BUFFER_SIZE, IIC_CPLD_SLAVE_ADDR);
//	if (Status != XST_SUCCESS) {
//		return XST_FAILURE;
//	}

/*
 * Wait until bus is idle to start another transfer.
 */
//	while (XIicPs_BusIsBusy(&Iic)) {
	/* NOP */
//	}

//	Status = XIicPs_MasterRecvPolled(&Iic, RecvBuffer,
//			  TEST_BUFFER_SIZE, IIC_CPLD_SLAVE_ADDR);
//	if (Status != XST_SUCCESS) {
//		return XST_FAILURE;
//	}


return XST_SUCCESS;
}
