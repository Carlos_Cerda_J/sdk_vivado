/*
 * main.h
 *
 *  Created on: 21-11-2018
 *      Author: David
 */

#ifndef SRC_MAIN_H_
#define SRC_MAIN_H_

/***************************** Include Files *********************************/
// System libraries
#include "math.h"
#include "xil_cache.h"
#include "xil_cache_l.h"
#include "xparameters.h"
#include "xil_exception.h"
#include "xscugic.h"	/* scu-GIC PS device driver (General Interrupt Controller) */
#include "xgpiops.h"	/* GPIO PS device driver */
#include "xiicps.h"		/* I2C PS device driver */
#include "xadcps.h"		/* ADC PS device driver (For internal temperature measure)*/
#include "xtmrctr.h"	/* Timer-Counter PL device driver */
#include "xgpio.h"		/* GPIO PL device driver */
#include "xscutimer.h"		// For Private PS Timer
#include "xttcps.h"		//PS tripple counter

// Local libraries
#include "Library_Local/control_constants.h"
#include "Library_Local/pefftaux.h"
#include "debug_SoC.h"

// Vivado create custom IP libraries
#include "debug_8_channel.h"
#include "sample_clock.h"
#include "sample_clock_50duty.h"
#include "FPGA_to_DDRRAM.h"
#include "ADC_max11331_AXI.h"

// Simulink HDL Coder libraries
#include "Library_HDL_Coder/ADC_converter_addr.h"
#include "Library_HDL_Coder/fast_adc_conv_addr.h"
#include "Library_HDL_Coder/PLL2_ip_addr.h"
#include "Library_HDL_Coder/slow_adc_conv_addr.h"
#include "Library_HDL_Coder/Sine_LUT_addr.h"
#include "Library_HDL_Coder/theta_gen_extern_addr.h"
#include "Library_HDL_Coder/Trip_curr_bits_addr.h"
#include "Library_HDL_Coder/vo_LPF_addr.h"
#include "Library_HDL_Coder/to_ARM_addr.h"
#include "Library_HDL_Coder/trip_det_addr.h"
#include "Library_HDL_Coder/PR_1MHz_addr.h"
#include "Library_HDL_Coder/M2C_NLC_6cell_Control_Vo_addr.h"
#include "Library_HDL_Coder/Notch_cluster_currents_addr.h"
#include "Library_HDL_Coder/Conv_ADC_slow_Full_addr.h"
#include "Library_HDL_Coder/PWM_28_HB_addr.h"




/************************** Constant Definitions *****************************/
/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are only defined here such that a user can easily
 * change all the needed parameters in one place.
 */

//Timer Interrupt
#define TMRCTR_INT_BASEADDR		XPAR_AXI_TIMER_INTERRUPT_TO_PS_BASEADDR
#define TMRCTR_DEVICE_ID		XPAR_TMRCTR_0_DEVICE_ID
#define TMRCTR_INTERRUPT_ID		XPAR_FABRIC_TMRCTR_0_VEC_ID
#define TIMER_CNTR_0	 		0

//PS Timer Interrupt
#define TTC_TICK_DEVICE_ID	XPAR_XTTCPS_1_DEVICE_ID
#define TTC_TICK_INTR_ID	XPAR_XTTCPS_1_INTR
#define TTCPS_CLOCK_HZ		XPAR_XTTCPS_0_CLOCK_HZ
//#define INTC_DEVICE_ID		XPAR_SCUGIC_SINGLE_DEVICE_ID

//Scugic for interrupts
#define INTC_DEVICE_ID			XPAR_SCUGIC_0_DEVICE_ID
#define INTC_DEVICE_INT_ID		0x0E
#define INTC					XScuGic
#define INTC_HANDLER			XScuGic_InterruptHandler

#define TIMER_PS_DEVICE_ID		XPAR_XSCUTIMER_0_DEVICE_ID
#define COUNTS_PER_SECOND 		(XPAR_CPU_CORTEXA9_CORE_CLOCK_FREQ_HZ /2)

#define AXI_CLOCK_FREQUENCY 	XPAR_AXI_TIMER_INTERRUPT_TO_PS_CLOCK_FREQ_HZ


#define val_delta_ref	ARM_TO_FPGA_id_sigma_o_pos_OFFSET
#define vbe_delta_ref	ARM_TO_FPGA_iq_sigma_o_pos_OFFSET
#define val_sigma_ref	ARM_TO_FPGA_id_sigma_o_neg_OFFSET
#define vbe_sigma_ref	ARM_TO_FPGA_iq_sigma_o_neg_OFFSET
#define v0_sigma_ref	ARM_TO_FPGA_extra_1_OFFSET


#define THETA_S_OFFSET		DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG0_OFFSET
#define I0_S_OFFSET			DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG1_OFFSET
#define PLL_OMEGA_OFFSET	DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG2_OFFSET
#define PLL_VQ_OFFSET		DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG3_OFFSET
#define IA_OFFSET			DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG4_OFFSET
#define IB_OFFSET			DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG5_OFFSET
#define IC_OFFSET			DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG6_OFFSET

#define IAP_OFFSET			DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG0_OFFSET
#define IBP_OFFSET			DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG1_OFFSET
#define ICP_OFFSET			DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG2_OFFSET
#define IAN_OFFSET			DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG3_OFFSET
#define IBN_OFFSET			DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG4_OFFSET
#define ICN_OFFSET			DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG5_OFFSET
#define VAL_GRID_OFFSET		DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG6_OFFSET
#define VBE_GRID_OFFSET		DEBUG_8_CHANNEL_S_AXI_Lite_SLV_REG7_OFFSET

/*
 * The following constant determines which timer counter of the device that is
 * used for this example, there are currently 2 timer counters in a device
 * and this example uses the first one, 0, the timer numbers are 0 based
 */
#define TIMER_CNTR_0	 0

/*
 * ScuGic manage interrupts in PS
 */
#define INTC			XScuGic
#define INTC_HANDLER	XScuGic_InterruptHandler



// GPIOs PL
#define GPIO_RESET_DEVICE_ID  	XPAR_AXI_GPIO_RESET_DEVICE_ID
#define GPIO_TRIP_DEVICE_ID  	XPAR_AXI_GPIO_INPUT_DEVICE_ID
#define GPIO_CONSTANT_DEVICE_ID		XPAR_AXI_GPIO_0_DEVICE_ID
#define GPIO_PRELOAD_DEVICE_ID		XPAR_FO_PRECARGA_DEVICE_ID

#define GPIO_CHANNEL 1
#define GPIO_CHANNEL1 1
#define GPIO_CHANNEL2 2

#define GPIO_pin_0  0x00000001
#define GPIO_pin_1  0x00000002
#define GPIO_pin_2  0x00000004
#define GPIO_pin_3  0x00000008
#define GPIO_pin_4  0x00000010
#define GPIO_pin_5  0x00000020
#define GPIO_pin_6  0x00000040
#define GPIO_pin_7  0x00000080
#define GPIO_pin_8  0x00000100
#define GPIO_pin_9  0x00000200
#define GPIO_pin_10 0x00000400
#define GPIO_pin_11 0x00000800
#define GPIO_pin_12 0x00001000
#define GPIO_pin_13 0x00002000
#define GPIO_pin_14 0x00004000
#define GPIO_pin_15 0x00008000
#define GPIO_pin_16 0x00010000
#define GPIO_pin_17 0x00020000
#define GPIO_pin_18 0x00040000
#define GPIO_pin_19 0x00080000
#define GPIO_pin_20 0x00100000
#define GPIO_pin_21 0x00200000
#define GPIO_pin_22 0x00400000
#define GPIO_pin_23 0x00800000
#define GPIO_pin_24 0x01000000
#define GPIO_pin_25 0x02000000
#define GPIO_pin_26 0x04000000
#define GPIO_pin_27 0x08000000
#define GPIO_pin_28 0x10000000
#define GPIO_pin_29 0x20000000
#define GPIO_pin_30 0x40000000
#define GPIO_pin_31 0x80000000

// Reset GPIO PL
#define RESET_PIN_FAST_ADC 		0x00000001 //Slice 0
#define RESET_PIN_SLOW_ADC 		0x00000002 //Slice 1
#define RESET_PIN_ADCLK 		0x00000004 //Slice 2
#define RESET_PIN_SAMPLE_CLK 	0x00000008 //Slice 3
#define RESET_PIN_TRIP_FPGA		0x00000010 //Slice 4
#define RESET_PIN_DEBUG_8CH 	0x00000020 //Slice 5
//#define RESET_PIN_ENABLE_MEAS 	0x00000040 //Slice 6
#define RESET_PIN_PLL 			0x00000080 //Slice 7
//#define RESET_PIN_ARM_TRAFO		0x00000100 //Slice 8
#define SHIFT_ADC 				0x00000200 //Slice 9
//#define RESET_NOTCH				0x00000400 //Slice 10
#define RESET_PIN_SAMPLER		0x00000800 //Slice 11

// GPIO PS
#define GPIOps_DEVICE_ID  	XPAR_XGPIOPS_0_DEVICE_ID
#define GPIOps_dir_output	0x0001
#define GPIOps_dir_input  	0x0000
#define GPIOps_pin_MIO0  	0
#define GPIOps_pin_MIO9 	9
#define GPIOps_pin_MIO7  	7

// XADC
#define XADC_DEVICE_ID 		XPAR_XADCPS_0_DEVICE_ID

// I2C CPLD
#define IIC_DEVICE_ID			XPAR_XIICPS_0_DEVICE_ID
#define IIC_CPLD_SLAVE_ADDR		0x10
#define IIC_SCLK_RATE			100000

#define COMMAND2_FREE_1				0x00
#define COMMAND2_CONFIG_CPLD_B1		0x01
#define COMMAND2_CONFIG_CPLD_B2		0x02
#define COMMAND2_CONFIG_CPLD_B3		0x03
#define COMMAND2_FREE_2				0x04
#define COMMAND2_FREE_3				0x05
#define COMMAND2_CONFIG_READ_ADDR	0x06
#define COMMAND2_INIT_ADC			0x07

#define COMMAND1_CHIP_TRIMMER_0		0x00
#define COMMAND1_CHIP_TRIMMER_1		0x01
#define COMMAND1_CHIP_TRIMMER_2		0x02
#define COMMAND1_CHIP_TRIMMER_3		0x03
#define COMMAND1_CHIP_TRIMMER_4		0x04
#define COMMAND1_CHIP_TRIMMER_5		0x05

#define ADC_INIT_TYPE_FULL			0x00
#define ADC_INIT_TYPE_SHORT			0x01
#define ADC_PATTERN_NORMAL			0x00
#define ADC_PATTERN_SYNC			0x01
#define ADC_PATTERN_DESKEW			0x02
#define ADC_PATTERN_CUSTOM			0x03

#define COMMAND0_TRIMMER_A_ADDR		0x00
#define COMMAND0_TRIMMER_B_ADDR		0x01
#define COMMAND0_TRIMMER_C_ADDR		0x02
#define COMMAND0_TRIMMER_D_ADDR		0x03

#define COMMAND0_ADC_0_ADDR			0x01
#define COMMAND0_ADC_1_ADDR			0x02
#define COMMAND0_ADC_ALL_ADDR		0x03

#define B1 0
#define B2 1
#define B3 2

//Corfo�s Project
#define HB 11
//#define PII 3.14159265359
#define DC 1 //0 means AC control for parallel
//
/************************** Function Prototypes ******************************/
int Control_interrupt_init(void);
int TmrCtrSetupIntrSystem(INTC* IntcInstancePtr,
				XTmrCtr* InstancePtr,
				u16 DeviceId,
				u16 IntrId,
				u8 TmrCtrNumber);

void TimerCounterHandler(void *CallBackRef, u8 TmrCtrNumber);
int Init_XAdc(u16 XAdcDeviceId);
int XAdcFractionToInt(float FloatNum);
int Interrupt_SetLoadReg(u32 RegisterValue);
int Gpio_PL_init(void);
int Gpio_ps_init(void);
int I2C_init(void);

int CPLD_init(void);
int Config_CPLD(u16 command2, u16 command1, u16 command0, u16 value);
void Reset_CPLD(void);
void init_PR_AXI(void);
int Trimmer_values_init(void);
int Read_comms_stats(void);
int Read_trip_triggered(void);
int Read_trip_instantaneous(void);
int Optical_Fibers_init(void);
int fast_adc_pattern(u16 adc_pattern);
int fast_adc_init(void);
int slow_adc_init(void);
int board_init(void);
void init_PR_axis(void);
void Control_voltage(void);
void init_PI_voltage(void);
void init_voltage_filters(void);
void Control_Vout(void);
void init_PI_Vout(void);
void reset_PI(void);
void begin_PR(void);
void Offsets_function(void);
int init_offline_data_parameters(void);
int mmc_control_init(void);
//int Timer_PS_init(void);
//u32 Timer_start, Timer_end, Timer_diff;
void reset_gain_slow_adc(void);

void set_offline_data_parameters(int preset);
int init_offline_data_parameters_preset(void);

int init_timer_fpga(void);
int init_interrupts(void);
void IRQ_ARM1_to_ARM0_Handler(void *CallbackRef);

void init_PR_double_s(void);

void init_PR_tpw_s(void);
void init_PR_tpw_z(void);

void init_global_variables(void);
void sorting(double arr[], s16 pos[], int n, double current);

/************************** Variable Definitions *****************************/

INTC InterruptController;  	/* The instance of the Interrupt Controller */
XTmrCtr TimerCounterInst;   /* The instance of the Timer Counter */
XGpio Gpio_FO12; 			/* The Instance of the GPIO Driver */
XGpio Gpio_FO3; 			/* The Instance of the GPIO Driver */
XGpio Gpio_reset; 			/* The Instance of the GPIO Driver */
XGpio Gpio_band; 			/* The Instance of the GPIO Driver */
XGpio Gpio_trip;
XGpio Gpio_button; 			/* The Instance of the GPIO Driver */
XGpio Gpio_DeltaSpike;
XGpio Gpio_constant; 		/* The Instance of the GPIO Driver */
XGpio FO_test1;
XGpio FO_test2;
XGpio FO_test3;
XGpio Gpio_preload; 		/* The Instance of the GPIO Driver */
XGpioPs Gpiops;				/* The driver instance for GPIO Device. */
XIicPs Iic;					/* Instance of the IIC Device */
XAdcPs XAdcInst;    		/* XADC driver instance */
XAdcPs *XAdcInstPtr = &XAdcInst;
//XScuTimer Timer_PS;			/* Cortex A9 SCU Private Timer Instance */

volatile u32 counter_interrupt = 0;	// Timer interrupt counter

u8 RecvBuffer[19];    // Buffer for Receiving I2C Data

u32 button_status=0;

float a1d,a2d,ki,kp;
unsigned int a1F,a2F,b0F,b1F,b2F,kpF;
double a1,a2,b0,b1,b2;
double kp_sD,ki_sD,kp_oD,ki_oD,kp_zDo,ki_zDo,kp_zDs,ki_zDs;
double kp_b1D,ki_b1D,kp_b2D,ki_b2D,kp_b3D,ki_b3D,ki_b1D_prom, kp_b1D_prom;

float F_low = 100.0;

// Cell Capacitors struct
double Vcap_sort[6], Vcbp_sort[6], Vccp_sort[6], Vcan_sort[6], Vcbn_sort[6], Vccn_sort[6];
s16 pos_ap[6], pos_bp[6], pos_cp[6], pos_an[6], pos_bn[6], pos_cn[6];
AB0SD Vc_transf_prefilt;


typedef struct button_struct
{
	u8 actual;
	u8 previous;
	u8 change;
}button_st;

button_st enable_button;

//vars state machine boton
enum States_Button {STOP, START};
enum States_Button Enable_button_State = STOP;

// PI Controllers struct
PI_form PI_Volt[6]={{0}};
PI_form PI_Vout;

//PR Controllers struct
PR_float_form PR_s[2]={{0}};
PR_float_form PR_z[2]={{0}};

FORM_PR_CONTROLLER PR_alphaSigma={};
FORM_PR_CONTROLLER PR_betaSigma={};

float id_plus,id_sigma_plus_o,iq_sigma_plus_o,id_sigma_min_s,iq_sigma_min_s,id_sigma_plus_s;
float id_min,iq_min;
int control_promedio=0;


double T_ipfil = 1.0;
double Tfil = 1.0;
double wcfil = 1.0;
double wfil = 1.0;
double qqfil = 1.0;
double afil = 1.0;
double bfil = 1.0;
double pfil = 1.0;
double qfil = 1.0;
double rfil = 1.0;
float VOa1 = 1.0;
float VOa2 = 1.0;
float VOa3 = 1.0;
float VOa4 = 1.0;
float VOa5 = 1.0;
float VO_filt_off = 13.0;
float vo_meas_freq = 400;

int debug_tr = 0;
int debug_tr_max = 201;
// Digital trimmer values for trips
u16 trip_slow_Vacb = 254; // Voltajes linea linea 140 para 600 [V]
u16 trip_slow_Vcxx = 230; //Voltajes condensadores cluster: 200 para 500[V], Trimmer High 2.69V
u16 trip_fast_curr = 159; //Corrientes: 159 para 9[A], Trimmer High 2.94[V]
u16 trip_fast_curr_Io = 198;// //36 para 2A. //159 para 9A
u16 trip_fast_Vo = 63;		//32 para 1200V. 21 para 800V. //45 1700


// Fast adc ip-core
short re_init_fast_adcs = 0;
short re_init_fast_adcs_IP = 0;
short change_fast_adc_pattern = 0;
u16 pattern = 0;
int shift_adc=1;

int cpld_res=0;

int enable_control=0;
int start_control=0;
int reset_control = 0;
int arm1_time_stamp = 0;

int start_PR=0;
int offset_counter=0;
int offsets_update = 0;
int enable_offset=0;


double T_proc=0.0001;

int frequency_interrupt = 10000;	//Hz
int frequency_interrupt_prev = 10000;

float out_openloop = 50.0;
float out_openloop_ant =49.0;
float frequency_Vout_khz = 20.0;	//khz
float frequency_Vout_khz_1 = 0.0;	//khz
float actual_frequency = 0;

s16 average_div = 25;
s16 average_div_ant = 25;

// Envelope detector variables/constants
float vref_env_r = 500.0;
float vref_env = 500.0;
float vref_env_amp = 500.0;
float amp_factor =  1.05;

// vout control variables
int control_vo = 0;
float Vout_ref_ant = 40.0;
float Vout_meas = 0.0;
float Vout_ref = 50.0;
float VoPI_act = 0.0;
float error_vo = 0.0;

enum States {INIT, GET_MEAS_OFFSET, SET_MEAS_OFFSET, PRELOAD_SWITCH, BYPASS_PRELOAD_SWITCH, ENABLE_CONTROL, SET_Vc,
            ENABLE_BALANCE_INNER, ENABLE_BALANCE_INTRA, RAMP_Vc, ENABLE_Vo, RAMP_Vo,
			STEADY_STATE, DISCHARGE, TEST_CABLEADO};
enum States MMC_State_Machine = INIT;

unsigned long counter_state_machine = 0;

int descarga_cond = 0;
int steady_first_time = 1;

int test_cableado = 1;
int master_reset = 0;
int band_spikes_mitigation = 1000;
int band_spikes_mitigation_fast = 2500;

int delay_state_machine = 5000;   // 10.000 <=> 1s	10000
int reset_slow_adc = 0;

//float Timer_ns,Timer_ns_min, Timer_ns_max;
//u32 Timer_meas_start, Timer_meas_end, Timer_meas_diff;
//float Timer_meas_ns;

short interrupt_flag = 0;
int interrupt_fail_counter = 0;

int counter_test = 0;

s16 update_PR = 1;
s16 boton_enable_SW = 0;

float val_delta_temp, vbe_delta_temp;
float clean_Val, clean_Vbe;

int PR_control_FPGA = 0;
int activate_notch_s = 0;
int activate_notch_z = 0;
int activate_notch_Vc_transf = 0;
u32 counter_cm = 0;

u8 new_arm1_to_arm0_data_flag = 0;

//Parameters
float limit_Iabc;
float limit_Io;
float limit_Vo;
float freq_out_ref;
float Vo_rms_ref;
//u8 start_control;
u8 remote_control;

s32 counter_control = 0;


//Control variables
float state_machine = 0;
float Vo_rms_med = 0.0;
float freq_out_real = 0.0;
float trip_fast = 0;
float trip_slow1 = 0;
float trip_slow2 = 0;

int selector_NLC = 1;
int selector_V0S = 0;

typedef struct offset_channel
{
	float Iap;
	float Ibp;
	float Icp;
	float Ian;
	float Ibn;
	float Icn;
	float Io;
	float Vo;
	float Ia;
	float Ib;
	float Ic;
	float Vab;
	float Vbc;
	float Vca;
}offset_channel_struct;

offset_channel_struct offset_structs;
offset_channel_struct offset_acumulator_structs;

// Filters struct
Filter_notch_form notch_s[2]={{0}};

Filter_notch_form notch_Iap = {0};
Filter_notch_form notch_Ibp = {0};
Filter_notch_form notch_Icp = {0};
Filter_notch_form notch_Ian = {0};
Filter_notch_form notch_Ibn = {0};
Filter_notch_form notch_Icn = {0};

Filter_notch_form notch_Vab = {0};
Filter_notch_form notch_Vbc = {0};
Filter_notch_form notch_Vca = {0};

Filter low_pass_Vc_alpha_S;
Filter low_pass_Vc_beta_S;
Filter low_pass_Vc_cero_S;
Filter low_pass_Vc_alpha_D;
Filter low_pass_Vc_beta_D;
Filter low_pass_Vc_cero_D;

Filter low_pass_Vc_alpha_S2;
Filter low_pass_Vc_beta_S2;
Filter low_pass_Vc_cero_S2;
Filter low_pass_Vc_alpha_D2;
Filter low_pass_Vc_beta_D2;
Filter low_pass_Vc_cero_D2;

Filter low_pass_vc_envelope;
Filter low_pass_vc_envelop;

//-----------------------------------------------------------------------//
// Global struct sub-structs

typedef struct _trip_detail_struct
{
	u32 slow_adc1;
	u32 slow_adc2;
	u16 fast_adc;
	u16 fast_adc_FPGA;
	u32 slow_adcP_FPGA;
	u32 slow_adcN_FPGA;
	char name_fast[4];
} trip_detail_struct;

typedef struct _trip_struct
{
	trip_detail_struct triggered;
	trip_detail_struct instantaneous;
	u8 general;
	u8 cpld_gen;
	u32 fpga_voltP_gen;
	u32 fpga_voltN_gen;
} trip_struct;

typedef struct _status_struct
{
	u16 comms_ok;
	u16 comms_fail;
} status_struct;

typedef struct _adc_input_struct
{
	short	ch1;
	short	ch2;
	short	ch3;
	short	ch4;
	short	add1;
	short	add2;
	short	add3;
	short	add4;
} adc_input_struct;

typedef struct _fast_adc_board_struct
{
	adc_input_struct	input1;
	adc_input_struct	input2;
	adc_input_struct	input3;
	adc_input_struct	input4;
} fast_adc_board_struct;

typedef struct _slow_adc_board_struct
{
	adc_input_struct	input1;
	adc_input_struct	input2;
	adc_input_struct	input3;
	adc_input_struct	input4;
	adc_input_struct	input5;
	adc_input_struct	input6;
} slow_adc_board_struct;

typedef struct _measure_slow_adc_struct
{
	slow_adc_board_struct	board1;
	slow_adc_board_struct	board2;
} measure_slow_adc_struct;

typedef struct _meas_struct
{
	fast_adc_board_struct	fast_adc;
	measure_slow_adc_struct	slow_adc;
} measure_struct;

typedef struct _trimmer_struct
{
	slow_adc_board_struct slow1;
	slow_adc_board_struct slow2;
	fast_adc_board_struct fast;
} trimmer_struct;

typedef struct _data_struct
{
//	control_struct cntr;
	status_struct 	stat;
	trip_struct 	trip;
	trimmer_struct 	trimmer;
	measure_struct	measure;
} data_struct;

data_struct data;


typedef struct CONTROL_REF_ST{
	float id_delta_plus;
	float id_delta_min;
	float iq_delta_min;
	float id_sigma_min;
	float iq_sigma_min;
	float id_sigma_plus;

	float ial_delta_plus;
	float ibe_delta_plus;
	float ial_delta_min;
	float ibe_delta_min;
	float ial_delta;
	float ibe_delta;

	float ial_sigma_plus;
	float ibe_sigma_plus;
	float ial_sigma_min;
	float ibe_sigma_min;
	float ial_sigma;
	float ibe_sigma;

	float i0_sigma_peak;
	float i0_sigma;
	float v0_sigma_peak;
	float v0_delta_peak;

	float val_delta;
	float vbe_delta;
	float v0_delta;
	float val_sigma;
	float vbe_sigma;
	float v0_sigma;

	float val_delta2;
	float vbe_delta2;

	float vap;
	float vbp;
	float vcp;
	float van;
	float vbn;
	float vcn;
}CONTROL_REF;

typedef struct CONTROL_MEAS_ST{
	float theta_s;
	float omega_s;

	float ial_delta;
	float ibe_delta;
	float ial_sigma;
	float ibe_sigma;
	float i0_sigma;

	float ia;
	float ib;
	float ic;

	float iap;
	float ibp;
	float icp;
	float ian;
	float ibn;
	float icn;

	float Vab_grid;
	float Vbc_grid;
	float Vca_grid;
	float Vd_grid;
	float Vq_grid;
	float Val_grid;
	float Vbe_grid;

	float Val_grid_ll;
	float Vbe_grid_ll;

	float PLL_w;
	float PLL_Vq;

	float Vd_grid_gen;
	float Val_grid_gen;
	float Vbe_grid_gen;

	float theta_s_ll;
	float theta_s_gen;

}CONTROL_MEAS;

typedef struct CONTROL_ENABLE_ST{
	s16 PR_s;
	s16 PR_z;
	s16 PR_o;
	s16 OpenLoop_o;
	s16 FO_MMC;
}CONTROL_ENABLE;

typedef struct CONTROL_ST{
	CONTROL_REF ref;
	CONTROL_MEAS meas;
	CONTROL_MEAS meas_filt;
	CONTROL_ENABLE enable;
}CONTROL;

CONTROL vars;



//online data
volatile data_ARM0_to_ARM1_struct OCM_BareMetal2FreeRTOS	__attribute__((section(".sharedRAM_Oszidata")));
volatile data_ARM1_to_ARM0_struct OCM_FreeRTOS2BareMetal 	__attribute__((section(".sharedRAM_Controldata")));
data_ARM1_to_ARM0_struct data_FreeRTOS2BareMetal;			// Copy the info from OCM_FreeRTOS2BareMetal

//offline data
volatile debug_SoC_offline_arm0_data_struct data_offline_arm0[DEBUG_SOC_OFFLINE_DATA_ARM0_MAX_MEMORY_SIZE] __attribute__((section(".ARM0data_offline")));
debug_SoC_parameters_ARM0_struct debug_SoC_parameters_ARM0;


typedef struct _var_slope_struct
{
     float actual;
     float final;
     float delta;
} var_slope_struct;

var_slope_struct Vo_ref;
var_slope_struct freqOut_ref;
int status_slope = 0;
int freq_chang_st = 0;
int status_slope_VO = 0;

int slope_reference (var_slope_struct *variable_st);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FORM_GLOBAL_VARIABLE g;

//corfo�s project
//typedef struct PI_form{
//	float out_act;
//	float error_act;
//	float out_1;
//	float error_1;
//	double kpz;
//	double az;
//	float umax;
//}PI_form;
//
//typedef struct PR_float_form{
//
//	float error_act;
//	float out_act;
//	float error_1;
//	float error_2;
//	float out_1;
//	float out_2;
//	float a0;
//	float a1;
//	float a2;
//	float b1;
//	float b2;
//
//}PR_float_form;
//
//typedef struct PR_float_AnC{
//
//	float error_act;
//	float out_act;
//	float error_1;
//	float error_2;
//	float out_1;
//	float out_2;
//	float a0;
//	float a1;
//	float a2;
//	float b1;
//	float b2;
//
//}PR_float_AnC;
//
//void InitializePI_tustin(PI_form *PI, float kp, float ki, float ts, float lim);
////double CalculatePI_tustin(PI_form *PI,float ref, float meas);
//
//void InitializePR_tpw(PR_float_form *PR, float kp, float ki, float w, float ts);
////double CalculatePR_tpw(PR_float_form *PR,float ref, float meas);
//
//void InitializePR_tpw_AnC(PR_float_AnC *PRAnC, float kp, float ki, float w, float ts, float Dn);
////double CalculatePR_tpw_AnC(PR_float_AnC *PRAnC, float ref, float meas);

//Signal for Modulation
int enable = 1;
int Max_point = 1000;
float Offset[HB+1];
//float Offset[12];

//Cos generation
//float counter_cm = 0; // already defined in 484
double ang_cm;
//double cos_ref;

//Reference rectifier
//float Vdc_ref;
//float Ig_ref;
//Measurement rectifier

float sine; //Sine sample (PLL)



//Rectifier Structures
PI_form_C PI_Vdc;//struct for PI used in rectifier controller
PR_float_form_C PR_Ig;//struct for PI used in rectifier controller

//PI structures for DC control
PI_form_C PI_Io_Total_DC;
PI_form_C PI_Circ_a_DC[HB-1];
PI_form_C PI_Circ_b_DC[HB-1];

//PR structures for AC control
PR_float_form_C PR_Io_Total_AC;
PR_float_form_C PR_Circ_a_AC[HB-1];
PR_float_form_C PR_Circ_b_AC[HB-1];

//Rectifier
double actuacion_PI; //Control for Vdc
double actuacion_PR_Ig; //Control for Vdc
//Inversor
double actuacion_PR_total;// Control for output current without Delay compesation for superior HB
double actuacion_PR_total_neg;// Control for output current without Delay compesation for inferior HB
double actuacion_PR_circa[HB-1];//Control for circulant current Iax without Delay compesation for superior HB
double actuacion_PR_circb[HB-1];//Control for circulant current Ibx without Delay compesation for inferior HB

//float Vg;
float wg;
float Lg;
float Rlg;

float C;
float Rcarga;

float wn_Vdc;
float xhi_Vdc;
float Kp_Vdc;
float Ki_Vdc;

float xhi_g;
float wn_g;
float Kp_g;
float Ki_g;

float L;
float R;

float lim;
float xhi;
float wn;

float kp;
float ki;

float wR;
float w0;
float kp_R;
float ki_R;

float Ts;


#endif /* SRC_MAIN_H_ */
