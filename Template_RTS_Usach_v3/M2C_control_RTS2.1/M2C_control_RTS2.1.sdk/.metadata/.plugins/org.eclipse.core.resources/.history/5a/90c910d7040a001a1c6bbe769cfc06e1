/*
 * main.h
 *
 *  Created on: 21-11-2018
 *      Author: David
 */

#ifndef SRC_MAIN_H_
#define SRC_MAIN_H_

/***************************** Include Files *********************************/
// System libraries
#include "math.h"
#include "xil_cache.h"
#include "xil_cache_l.h"
#include "xparameters.h"
#include "xil_exception.h"
#include "xscugic.h"	/* scu-GIC PS device driver (General Interrupt Controller) */
#include "xgpiops.h"	/* GPIO PS device driver */
#include "xiicps.h"		/* I2C PS device driver */
#include "xadcps.h"		/* ADC PS device driver (For internal temperature measure)*/
#include "xtmrctr.h"	/* Timer-Counter PL device driver */
#include "xgpio.h"		/* GPIO PL device driver */

// Local libraries
#include "Library_Local/control_constants.h"
#include "Library_Local/pefftaux.h"

// Vivado create custom IP libraries
#include "ARM_to_FPGA.h"
#include "debug_8_channel.h"
#include "sample_clock.h"
#include "Trip_detector_currents.h"
#include "sample_clock_50duty.h"
#include "FPGA_to_DDRRAM.h"

// Simulink HDL Coder libraries
#include "Library_HDL_Coder/angle_addition_addr.h"
#include "Library_HDL_Coder/ADC_converter_addr.h"
#include "Library_HDL_Coder/AXI_constant_addr.h"
#include "Library_HDL_Coder/bits_to_V_addr.h"
#include "Library_HDL_Coder/CONSTANT_2514_addr.h"
#include "Library_HDL_Coder/envelope_detec_addr.h"
#include "Library_HDL_Coder/envelope_detecOS_addr.h"
#include "Library_HDL_Coder/fast_adc_conv_addr.h"
#include "Library_HDL_Coder/gained_FF_addr.h"
#include "Library_HDL_Coder/nlc_3c_addr.h"
#include "Library_HDL_Coder/PLL2_ip_addr.h"
#include "Library_HDL_Coder/PR_NOsatv4_addr.h"
#include "Library_HDL_Coder/PR_oversamp_addr.h"
#include "Library_HDL_Coder/PR_oversampF2_addr.h"
#include "Library_HDL_Coder/PULSE_DELAY_NLC_addr.h"
#include "Library_HDL_Coder/selector_sw2513_addr.h"
#include "Library_HDL_Coder/slow_adc_conv_addr.h"
#include "Library_HDL_Coder/switch_6vias_addr.h"
#include "Library_HDL_Coder/switch_by_minus_addr.h"
#include "Library_HDL_Coder/switch_dpdt_addr.h"
#include "Library_HDL_Coder/Sine_LUT_addr.h"
#include "Library_HDL_Coder/theta_gen_extern_addr.h"
#include "Library_HDL_Coder/THREE_SINE_IP_addr.h"
#include "Library_HDL_Coder/Trip_curr_bits_addr.h"
#include "Library_HDL_Coder/vo_LPF_addr.h"
#include "Library_HDL_Coder/to_ARM_addr.h"

/************************** Constant Definitions *****************************/
/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are only defined here such that a user can easily
 * change all the needed parameters in one place.
 */

//Timer Interrupt
#define TMRCTR_INT_BASEADDR		XPAR_AXI_TIMER_INTERRUPT_TO_PS_BASEADDR
#define TMRCTR_DEVICE_ID		XPAR_TMRCTR_0_DEVICE_ID
#define TMRCTR_INTERRUPT_ID		XPAR_FABRIC_TMRCTR_0_VEC_ID
#define INTC_DEVICE_ID			XPAR_SCUGIC_SINGLE_DEVICE_ID

#define PR_alpha_delta			XPAR_CONTROL_IALPHA_DELTA_PR_OVERSAMPF2_0_BASEADDR
#define PR_beta_delta			XPAR_CONTROL_IBETA_DELTA_PR_OVERSAMPF2_0_BASEADDR
#define PR_alpha_sigma_s		XPAR_CONTROL_IALPHA_SIGMA_S_PR_OVERSAMPF2_0_BASEADDR
#define PR_beta_sigma_s			XPAR_CONTROL_IBETA_SIGMA_S_PR_OVERSAMPF2_0_BASEADDR
#define PR_cero_sigma			XPAR_CONTROL_I0_SIGMA_PR_OVERSAMPF2_0_BASEADDR ///Error de nombres!

#define AXI_CLOCK_FREQUENCY 	XPAR_AXI_TIMER_INTERRUPT_TO_PS_CLOCK_FREQ_HZ

#define idp_ref		ARM_TO_FPGA_id_pos_OFFSET
#define iqp_ref		ARM_TO_FPGA_iq_pos_OFFSET
#define idn_ref		ARM_TO_FPGA_id_neg_OFFSET
#define iqn_ref		ARM_TO_FPGA_iq_neg_OFFSET
#define idp_s_ref	ARM_TO_FPGA_id_sigma_s_pos_OFFSET
#define iqp_s_ref	ARM_TO_FPGA_iq_sigma_s_pos_OFFSET
#define idn_s_ref	ARM_TO_FPGA_id_sigma_s_neg_OFFSET
#define iqn_s_ref	ARM_TO_FPGA_iq_sigma_s_neg_OFFSET
#define idp_o_ref	ARM_TO_FPGA_id_sigma_o_pos_OFFSET
#define iqp_o_ref	ARM_TO_FPGA_iq_sigma_o_pos_OFFSET
#define idn_o_ref	ARM_TO_FPGA_id_sigma_o_neg_OFFSET
#define iqn_o_ref	ARM_TO_FPGA_iq_sigma_o_neg_OFFSET



/*
 * The following constant determines which timer counter of the device that is
 * used for this example, there are currently 2 timer counters in a device
 * and this example uses the first one, 0, the timer numbers are 0 based
 */
#define TIMER_CNTR_0	 0

/*
 * ScuGic manage interrupts in PS
 */
#define INTC			XScuGic
#define INTC_HANDLER	XScuGic_InterruptHandler



// GPIOs PL
#define GPIO_RESET_DEVICE_ID  	XPAR_AXI_GPIO_RESET_DEVICE_ID
#define GPIO_TRIP_DEVICE_ID  	XPAR_AXI_GPIO_INPUT_DEVICE_ID
#define GPIO_CONSTANT_DEVICE_ID		XPAR_AXI_GPIO_0_DEVICE_ID
#define GPIO_PRELOAD_DEVICE_ID		XPAR_FO_PRECARGA_DEVICE_ID
#define GPIO_FO_TEST_1_DEVICE_ID	XPAR_AXI_FO1_DEVICE_ID
#define GPIO_FO_TEST_2_DEVICE_ID	XPAR_AXI_FO2_DEVICE_ID
#define GPIO_FO_TEST_3_DEVICE_ID	XPAR_AXI_FO3_DEVICE_ID

#define GPIO_CHANNEL 1
#define GPIO_CHANNEL1 1
#define GPIO_CHANNEL2 2

#define GPIO_pin_0  0x00000001
#define GPIO_pin_1  0x00000002
#define GPIO_pin_2  0x00000004
#define GPIO_pin_3  0x00000008
#define GPIO_pin_4  0x00000010
#define GPIO_pin_5  0x00000020
#define GPIO_pin_6  0x00000040
#define GPIO_pin_7  0x00000080
#define GPIO_pin_8  0x00000100
#define GPIO_pin_9  0x00000200
#define GPIO_pin_10 0x00000400
#define GPIO_pin_11 0x00000800
#define GPIO_pin_12 0x00001000
#define GPIO_pin_13 0x00002000
#define GPIO_pin_14 0x00004000
#define GPIO_pin_15 0x00008000
#define GPIO_pin_16 0x00010000
#define GPIO_pin_17 0x00020000
#define GPIO_pin_18 0x00040000
#define GPIO_pin_19 0x00080000
#define GPIO_pin_20 0x00100000
#define GPIO_pin_21 0x00200000
#define GPIO_pin_22 0x00400000
#define GPIO_pin_23 0x00800000
#define GPIO_pin_24 0x01000000
#define GPIO_pin_25 0x02000000
#define GPIO_pin_26 0x04000000
#define GPIO_pin_27 0x08000000
#define GPIO_pin_28 0x10000000
#define GPIO_pin_29 0x20000000
#define GPIO_pin_30 0x40000000
#define GPIO_pin_31 0x80000000

// Reset GPIO PL
#define RESET_PIN_FAST_ADC 		0x00000001 //Slice 0
#define RESET_PIN_SLOW_ADC 		0x00000002 //Slice 1
#define RESET_PIN_ADCLK 		0x00000004 //Slice 2
#define RESET_PIN_SAMPLE_CLK 	0x00000008 //Slice 3
#define RESET_PIN_TRIP_FPGA		0x00000010 //Slice 4
#define RESET_PIN_DEBUG_8CH 	0x00000020 //Slice 5
#define RESET_PIN_ENABLE_MEAS 	0x00000040 //Slice 6
#define RESET_PIN_PLL 			0x00000080 //Slice 7
#define RESET_PIN_ARM_TRAFO		0x00000100 //Slice 8
#define SHIFT_ADC 				0x00000200 //Slice 9

// GPIO PS
#define GPIOps_DEVICE_ID  	XPAR_XGPIOPS_0_DEVICE_ID
#define GPIOps_dir_output	0x0001
#define GPIOps_dir_input  	0x0000
#define GPIOps_pin_MIO0  	0
#define GPIOps_pin_MIO9 	9
#define GPIOps_pin_MIO7  	7

// XADC
#define XADC_DEVICE_ID 		XPAR_XADCPS_0_DEVICE_ID

// I2C CPLD
#define IIC_DEVICE_ID			XPAR_XIICPS_0_DEVICE_ID
#define IIC_CPLD_SLAVE_ADDR		0x10
#define IIC_SCLK_RATE			100000

#define COMMAND2_FREE_1				0x00
#define COMMAND2_CONFIG_CPLD_B1		0x01
#define COMMAND2_CONFIG_CPLD_B2		0x02
#define COMMAND2_CONFIG_CPLD_B3		0x03
#define COMMAND2_FREE_2				0x04
#define COMMAND2_FREE_3				0x05
#define COMMAND2_CONFIG_READ_ADDR	0x06
#define COMMAND2_INIT_ADC			0x07

#define COMMAND1_CHIP_TRIMMER_0		0x00
#define COMMAND1_CHIP_TRIMMER_1		0x01
#define COMMAND1_CHIP_TRIMMER_2		0x02
#define COMMAND1_CHIP_TRIMMER_3		0x03
#define COMMAND1_CHIP_TRIMMER_4		0x04
#define COMMAND1_CHIP_TRIMMER_5		0x05

#define ADC_INIT_TYPE_FULL			0x00
#define ADC_INIT_TYPE_SHORT			0x01
#define ADC_PATTERN_NORMAL			0x00
#define ADC_PATTERN_SYNC			0x01
#define ADC_PATTERN_DESKEW			0x02
#define ADC_PATTERN_CUSTOM			0x03

#define COMMAND0_TRIMMER_A_ADDR		0x00
#define COMMAND0_TRIMMER_B_ADDR		0x01
#define COMMAND0_TRIMMER_C_ADDR		0x02
#define COMMAND0_TRIMMER_D_ADDR		0x03

#define COMMAND0_ADC_0_ADDR			0x01
#define COMMAND0_ADC_1_ADDR			0x02
#define COMMAND0_ADC_ALL_ADDR		0x03

#define B1 0
#define B2 1
#define B3 2

/************************** Function Prototypes ******************************/
int Control_interrupt_init(void);
int TmrCtrSetupIntrSystem(INTC* IntcInstancePtr,
				XTmrCtr* InstancePtr,
				u16 DeviceId,
				u16 IntrId,
				u8 TmrCtrNumber);

void TimerCounterHandler(void *CallBackRef, u8 TmrCtrNumber);
void TmrCtrDisableIntr(INTC* IntcInstancePtr, u16 IntrId);
int Init_XAdc(u16 XAdcDeviceId);
int XAdcFractionToInt(float FloatNum);
int Interrupt_SetLoadReg(u32 RegisterValue);
int Gpio_PL_init(void);
int Gpio_ps_init(void);
int I2C_init(void);
int SPI_init(void);
int CPLD_init(void);
int Config_CPLD(u16 command2, u16 command1, u16 command0, u16 value);
void Reset_CPLD(void);
void init_PR_AXI(void);
int Trimmer_values_init(void);
int Read_comms_stats(void);
int Read_trip_triggered(void);
int Read_trip_instantaneous(void);
int Optical_Fibers_init(void);
int fast_adc_pattern(u16 adc_pattern);
int fast_adc_init(void);
int slow_adc_init(void);
int board_init(void);
void init_PR_axis(void);
void Control_voltage(void);
void init_PI_voltage(void);
void Control_Vout(void);
void init_PI_Vout(void);
void reset_PI(void);
void begin_PR(void);
void Offsets_function(void);
int init_offline_data_parameters(void);
int mmc_control_init(void);

void set_offline_data_parameters(int preset);
int init_offline_data_parameters_preset(void);

/************************** Variable Definitions *****************************/

INTC InterruptController;  	/* The instance of the Interrupt Controller */
XTmrCtr TimerCounterInst;   /* The instance of the Timer Counter */
XGpio Gpio_FO12; 			/* The Instance of the GPIO Driver */
XGpio Gpio_FO3; 			/* The Instance of the GPIO Driver */
XGpio Gpio_reset; 			/* The Instance of the GPIO Driver */
XGpio Gpio_trip; 			/* The Instance of the GPIO Driver */
XGpio Gpio_constant; 		/* The Instance of the GPIO Driver */
XGpio FO_test1;
XGpio FO_test2;
XGpio FO_test3;
XGpio Gpio_preload; 		/* The Instance of the GPIO Driver */
XGpioPs Gpiops;				/* The driver instance for GPIO Device. */
XIicPs Iic;					/* Instance of the IIC Device */
XAdcPs XAdcInst;    		/* XADC driver instance */
XAdcPs *XAdcInstPtr = &XAdcInst;


volatile u32 counter_interrupt = 0;	// Timer interrupt counter

u8 RecvBuffer[19];    // Buffer for Receiving I2C Data

int fo_test_ch=0;

float a1d,a2d,ki,kp;
unsigned int a1F,a2F,b0F,b1F,b2F,kpF;
float a1,a2,b0,b1,b2;
float kp_sD,ki_sD,kp_oD,ki_oD,kp_zDo,ki_zDo,kp_zDs,ki_zDs;
float kp_b1D,ki_b1D,kp_b2D,ki_b2D,kp_b3D,ki_b3D;
int bits_frac=13;

float F_delta = 2.0;
float F_low=20.0;
float F_sigma = 2.0;

// Cell Capacitors struct
XY Vcxy;
XY Vcxy_prom;
XY envXY;
AB0SD Vc_transf,Vc_transf_filt;
AB0SD envAB0SD;
float VaD_ref=0.0;
float VbD_ref=0.0;
float envXY_ap=0.0;

// PI Controllers struct
PI_form PI_Volt[6]={{0}};
PI_form PI_Vout;

float id_plus,id_sigma_plus_o,iq_sigma_plus_o,id_sigma_min_s,iq_sigma_min_s,id_sigma_plus_s;
float id_min,iq_min;
int control_promedio=0;

double T_ipfil = 1.0;
double Tfil = 1.0;
double wcfil = 1.0;
double wfil = 1.0;
double qqfil = 1.0;
double afil = 1.0;
double bfil = 1.0;
double pfil = 1.0;
double qfil = 1.0;
double rfil = 1.0;
float VOa1 = 1.0;
float VOa2 = 1.0;
float VOa3 = 1.0;
float VOa4 = 1.0;
float VOa5 = 1.0;
float VO_filt_off = 13.0;
float vo_meas_freq = 400;

int debug_tr = 0;
int debug_tr_max = 30;
// Digital trimmer values for trips
u16 trip_input1_slow = 240;// Voltajes linea linea 140 para 600 [V]
u16 trip_input2_slow = 143;//Voltajes Vcap Vcbp Vccp Vcan 120 para 505 [V] app 144 para 600V
u16 trip_input3_slow = 143;//Voltajes Vcbn Vccn
u16 trip_input4_slow = 143;
u16 trip_input1_slow2 = 143;
u16 trip_slow = 0xff;
u16 trip_input1_fast = 146; //146 para Delta V de 0.971, lo que se obtiene con 10[A] medidos en chroma
u16 trip_input2_fast = 146;

// Debug 1 sample
short start_debug_1sample = 1;
int debug_8ch_1[8];
int debug_8ch_2[8];
int debug_8ch_4[8];
float debug_8ch_slow_conv1[8];

// Fast adc ip-core
short re_init_fast_adcs = 0;
short re_init_fast_adcs_IP = 0;
short change_fast_adc_pattern = 0;
u16 pattern = 0;
int shift_adc=1;

int trip_software=0;
int cpld_res=0;

int enable_mmc = 0;
int enable_control=0;
int start_control=0;
int reset_control = 0;
int delay_nlc=0;
int Cdelay_nlc=0;
int delay_nlc_ant=1;
float angle_comp=0;
float angle_comp_ant=1;
float kkk;

int start_PR=0;
int offset_counter=0;
int offsets_update = 0;
int enable_offset=0;

float constant_step;
float constant_step_v_prom;
int cv_prom;


float T_proc=0.0001;

int frequency_interrupt = 10000;	//Hz
int frequency_interrupt_prev = 10000;

float out_openloop = 50.0;
float out_openloop_ant =49.0;
float frequency_Vout_khz = 10;	//khz
float actual_frequency = 0;

int sine_ref_samp=10;
int sine_ref_samp_1=200;

s32 frec_out_div = 50;
s32 frec_out_div_ant = 49;
s32 frec_nlc_khz = 1000;
s32 frec_nlc_khz_ant = 990;

s32 frec_sorting_khz = 50;
s32 frec_sorting_khz_ant = 50;

s16 average_div = 25;
s16 average_div_ant = 25;

// Envelope detector variables/constants
s16 avg_env_divisor = 1000;
s16 avg_env_divisor_ant = 98;
float env_step_ant=0;
int env_count_ant=0;
float env_step=0.005;//0.01;
int env_count=3000;//3000;
float vref_env_r = 500.0;
float vref_env = 500.0;
float env_offset =  30.0;

// vout control variables
int control_vo = 0;
float Vout_ref_ant = 40.0;
float Vout_meas = 0.0;
float Vout_ref = 50.0;
float VoPI_act = 0.0;
float error_vo = 0.0;

//variables for presets
int preset=0;
int preset_ant=0;

enum States {INIT, GET_MEAS_OFFSET, SET_MEAS_OFFSET, PRELOAD_SWITCH, BYPASS_PRELOAD_SWITCH, ENABLE_CONTROL, SET_Vc,
            ENABLE_BALANCE_INNER, ENABLE_BALANCE_INTRA, RAMP_Vc, ENABLE_Vo, RAMP_Vc_Vo,
			STEADY_STATE, RAMP_FREQ, STEADY_STATE_50kHz, DISCHARGE};
enum States MMC_State_Machine = INIT;
unsigned long counter_state_machine = 0;

int descarga_cond = 0;
int steady_first_time = 1;


typedef struct PR_activacion
{
	int activ_ab_delta;
	int activ_ab_sigma_s;
	int activ_ab_neg_sec;
	int activ_0_sigma ;
}PR_activ;

typedef struct offset_channel
{
	float Iap;
	float Ibp;
	float Icp;
	float Ian;
	float Ibn;
	float Icn;
	float Io;
	float Ia;
	float Ib;
	float Ic;
	float Vab;
	float Vbc;
	float Vca;
}offset_channel_struct;

typedef struct offset_acum
{
	float Iap[1000];
	float Ibp[1000];
	float Icp[1000];
	float Ian[1000];
	float Ibn[1000];
	float Icn[1000];
	float Io[1000];
	float Ia[1000];
	float Ib[1000];
	float Ic[1000];
	float Vab[1000];
	float Vbc[1000];
	float Vca[1000];
}offset_acum_struct;

offset_channel_struct offset_structs;
offset_acum_struct acumulator_channel;
PR_activ PR_act={0};

// Filters struct
Filter notches[6]={{0}};
Filter low_pass_cero_sigma;
Filter low_pass_vcxy1,low_pass_vcxy2,low_pass_vcxy3,low_pass_vcxy4,low_pass_vcxy5,low_pass_vcxy6;
Filter low_pass_Vc_alpha_delta;
Filter low_pass_Vc_beta_delta;
Filter low_pass_Vc_alpha_sigma;
Filter low_pass_Vc_beta_sigma;
Filter low_pass_vc_envelope;
Filter low_pass_vc_envelop;

//-----------------------------------------------------------------------//
// Global struct sub-structs
typedef struct trimmer_chip_addr_struct
{
	u8 pot[4];
} trimmer_chip_addr_struct;

typedef struct _trimmer_bus_addr_struct
{
	trimmer_chip_addr_struct chip[6];
} trimmer_bus_addr_struct;

typedef struct _trimmer_struct
{
	trimmer_bus_addr_struct bus[3];
} trimmer_struct;

typedef struct _trip_detail_struct
{
	u32 slow_adc1;
	u32 slow_adc2;
	u16 fast_adc;
	u16 fast_adc_FPGA;
} trip_detail_struct;

typedef struct _trip_struct
{
	trip_detail_struct triggered;
	trip_detail_struct instantaneous;
	u8 general;
	u8 cpld_gen;
	u16 fpga_gen;
} trip_struct;

typedef struct _status_struct
{
	u16 comms_ok;
	u16 comms_fail;
} status_struct;

typedef struct _adc_input_struct
{
	short	ch1;
	short	ch2;
	short	ch3;
	short	ch4;
} adc_input_struct;

typedef struct _fast_adc_board_struct
{
	adc_input_struct	input1;
	adc_input_struct	input2;
	adc_input_struct	input3;
	adc_input_struct	input4;
} fast_adc_board_struct;

typedef struct _slow_adc_board_struct
{
	adc_input_struct	input1;
	adc_input_struct	input2;
	adc_input_struct	input3;
	adc_input_struct	input4;
	adc_input_struct	input5;
	adc_input_struct	input6;
} slow_adc_board_struct;

typedef struct _measure_slow_adc_struct
{
	slow_adc_board_struct	board1;
	slow_adc_board_struct	board2;
} measure_slow_adc_struct;

typedef struct _meas_struct
{
	fast_adc_board_struct	fast_adc;
	measure_slow_adc_struct	slow_adc;
} measure_struct;

typedef struct _data_struct
{
//	control_struct cntr;
	status_struct 	stat;
	trip_struct 	trip;
	trimmer_struct 	trimmer;
	measure_struct	measure;
} data_struct;

data_struct data;


//------------------------------------------------------------------------//
// Variables for offline data
typedef struct _data_offline_fpga_struct
{
	u32 variable[16];
} data_offline_fpga_struct;

typedef struct _data_offline_arm0_struct
{
	float variable[16];
	u32 burst_counter;
} data_offline_arm0_struct;

float *data_offline_64var_arm0_ptr[64];

typedef struct _offline_data_parameters_struct
{
	u16	enable;				//only 1 bit is necessary. use 16bits for change variable in expressions window
	u32 status_burst_count;
	u8 	status_done;
	u8 	status_error;
	u16 sample_clock_div;
	s32 fast_sample_frec_kHz;
	s32 max_data_points;
	u8 	ch_quantity;
	u16	plot_fpga_vars;		//only 1 bit is necessary. use 16bits for change variable in expressions window
	u16	plot_arm0_vars;		//only 1 bit is necessary. use 16bits for change variable in expressions window
	s16	selector_fpga[16];	//only 6 bits are necessary. use 16bits for change variable in expressions window
	s16	bits_frac_fpga[64];	//only 5 bits are necessary. use 16bits for change variable in expressions window
	s16	bits_word_fpga[64];	//only 5 bits are necessary. use 16bits for change variable in expressions window
	s16	selector_arm0[16];	//only 6 bits are necessary. use 16bits for change variable in expressions window
	u32 counter_arm0;
} offline_data_parameters_struct;

typedef struct _offline_data_parameters_preset_struct
{
s32 fast_sample_frec_kHz;
s32 max_data_points;
u8 ch_quantity;
u16 plot_fpga_vars; //only 1 bit is necessary. use 16bits for change variable in expressions window
u16 plot_arm0_vars; //only 1 bit is necessary. use 16bits for change variable in expressions window
s16 selector_fpga[16]; //only 6 bits are necessary. use 16bits for change variable in expressions window
s16 selector_arm0[16]; //only 6 bits are necessary. use 16bits for change variable in expressions window
} offline_data_parameters_preset_struct;

#define OFFLINE_DATA_PRESET_NUMBER 4
#define OFFLINE_DATA_FPGA_MAX_MEMORY_SIZE 128000
#define OFFLINE_DATA_ARM0_MAX_MEMORY_SIZE 32000	// shoud be 64000 when data_offline_arm0_struct has only 16 32-bit variables.
volatile data_offline_fpga_struct data_offline_fpga[OFFLINE_DATA_FPGA_MAX_MEMORY_SIZE] __attribute__((section(".FPGAdata_offline")));
volatile data_offline_arm0_struct data_offline_arm0[OFFLINE_DATA_ARM0_MAX_MEMORY_SIZE] __attribute__((section(".ARM0data_offline")));

offline_data_parameters_struct offline_data_parameters;
offline_data_parameters_preset_struct offline_data_parameters_preset[OFFLINE_DATA_PRESET_NUMBER];
u32 offline_data_counter_arm0 = 0;
u32 counter_arm0_points = 0;

typedef struct _var_slope_struct
{
     float actual;
     float final;
     float delta;
} var_slope_struct;

var_slope_struct Vo_ref;
var_slope_struct freqOut_ref;
int status_slope = 0;
int freq_chang_st = 0;

int slope_reference (var_slope_struct *variable_st);


#endif /* SRC_MAIN_H_ */
