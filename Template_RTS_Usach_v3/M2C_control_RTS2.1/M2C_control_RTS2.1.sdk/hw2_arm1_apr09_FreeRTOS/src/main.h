/*
 * main.h
 *
 *  Created on: 21-11-2018
 *      Author: David
 */

#ifndef SRC_MAIN_H_
#define SRC_MAIN_H_

/***************************** Include Files *********************************/
#include "main.h"
#include "stdio.h"
#include "math.h"

#include "xparameters.h"
#include "xil_exception.h"
#include "xil_printf.h"

#include "xscugic.h"	/* scu-GIC PS device driver (General Interrupt Controller) */
#include "xtmrctr.h"	/* Timer-Counter PL device driver */
#include "xgpio.h"		/* GPIO PL device driver */
#include "xgpiops.h"	/* GPIO PS device driver */

#include "xscutimer.h"		// For Private PS Timer
#include "xil_cache.h"		// For disable cache

#include "xuartps.h"
#include "debug_SoC.h"
#include "M2C_NLC_6cell_Control_Vo_addr.h"


/************************** Constant Definitions *****************************/
/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are only defined here such that a user can easily
 * change all the needed parameters in one place.
 */
//Timer PS
#define COUNTS_PER_SECOND       (XPAR_CPU_CORTEXA9_CORE_CLOCK_FREQ_HZ /2)
#define TIMER_LOAD_VALUE		COUNTS_PER_SECOND
#define TIMER_PS_DEVICE_ID		XPAR_XSCUTIMER_0_DEVICE_ID

//Timer Interrupt
#define TMRCTR_INT_BASEADDR		XPAR_AXI_TIMER_INTERRUPT_TO_PS_BASEADDR
#define TMRCTR_DEVICE_ID		XPAR_TMRCTR_0_DEVICE_ID
#define TMRCTR_INTERRUPT_ID		XPAR_FABRIC_TMRCTR_0_VEC_ID
#define TIMER_CNTR_0	 		0

//Scugic for interrupts
#define INTC_DEVICE_ID			XPAR_SCUGIC_0_DEVICE_ID
#define INTC_DEVICE_INT_ID		0x0E
#define INTC					XScuGic
#define INTC_HANDLER			XScuGic_InterruptHandler

#define AXI_CLOCK_FREQUENCY XPAR_AXI_TIMER_INTERRUPT_TO_PS_CLOCK_FREQ_HZ

// GPIO PL
#define GPIO_DEVICE_ID  	XPAR_AXI_GPIO_0_DEVICE_ID
#define GPIO_CHANNEL 1
#define GPIO_pin_0  0x00000001
#define GPIO_pin_1  0x00000002
#define GPIO_pin_2  0x00000004
#define GPIO_pin_3  0x00000008
#define GPIO_pin_4  0x00000010
#define GPIO_pin_5  0x00000020
#define GPIO_pin_6  0x00000040
#define GPIO_pin_7  0x00000080
#define GPIO_pin_8  0x00000100
#define GPIO_pin_9  0x00000200
#define GPIO_pin_10 0x00000400
#define GPIO_pin_11 0x00000800
#define GPIO_pin_12 0x00001000
#define GPIO_pin_13 0x00002000
#define GPIO_pin_14 0x00004000
#define GPIO_pin_15 0x00008000
#define GPIO_pin_16 0x00010000
#define GPIO_pin_17 0x00020000
#define GPIO_pin_18 0x00040000
#define GPIO_pin_19 0x00080000
#define GPIO_pin_20 0x00100000
#define GPIO_pin_21 0x00200000
#define GPIO_pin_22 0x00400000
#define GPIO_pin_23 0x00800000
#define GPIO_pin_24 0x01000000
#define GPIO_pin_25 0x02000000
#define GPIO_pin_26 0x04000000
#define GPIO_pin_27 0x08000000
#define GPIO_pin_28 0x10000000
#define GPIO_pin_29 0x20000000
#define GPIO_pin_30 0x40000000
#define GPIO_pin_31 0x80000000

// GPIO PS
#define GPIOps_DEVICE_ID  	XPAR_XGPIOPS_0_DEVICE_ID
#define GPIOps_dir_output  	0x0001
#define GPIOps_dir_input  	0x0000
#define GPIOps_pin_MIO0  	0
#define GPIOps_pin_MIO9  	9
#define GPIOps_pin_MIO7  	7

/************************** Function Prototypes ******************************/
int init_interrupt(void);
int Gpio_interrupt_init(void);
void IRQ_ARM0_to_ARM1_Handler(void *CallbackRef);
void IRQ_FPGA_to_OCM_Handler(void *CallbackRef);
int Timer_PS_init(void);
float toFloat(int num, int bit_word, int bit_frac);
int toFix(float num,int bit_sig);
int main_thread();


/************************** Variable Definitions *****************************/
XScuGic InterruptController; 	     /* Instance of the Interrupt Controller */
XScuTimer Timer_PS;		/* Cortex A9 SCU Private Timer Instance */
//XGpio Gpio_interrupt_OCM;			/* The Instance of the GPIO Driver */
XGpio Gpio_interrupt_DDRRAM;		/* The Instance of the GPIO Driver */

volatile u32 counter = 0;
u32 Timer_start, Timer_end, Timer_diff;
float Timer_ns;


//online data
volatile data_FPGA_to_ARM1_struct OCM_FPGA2FreeRTOS 		__attribute__((section(".sharedRAM_FPGAdata")));
volatile data_ARM0_to_ARM1_struct OCM_BareMetal2FreeRTOS	__attribute__((section(".sharedRAM_Oszidata")));
volatile data_ARM1_to_ARM0_struct OCM_FreeRTOS2BareMetal 	__attribute__((section(".sharedRAM_Controldata")));
volatile data_ARM0_to_ARM1_struct data_BareMetal2FreeRTOS;	// Copy the info from OCM_BareMetal2FreeRTOS
volatile data_FPGA_to_ARM1_struct data_FPGA2FreeRTOS;		// Copy the info from OCM_FPGA2FreeRTOS

#define PLOT_DATA_LENGTH	500	// total points per variable to plot over serial console
data_FPGA_to_ARM1_struct plot_data_fpga[PLOT_DATA_LENGTH];
data_ARM0_to_ARM1_struct plot_data_arm0[PLOT_DATA_LENGTH];	// Copy the info from OCM_BareMetal2FreeRTOS
data_ARM1_to_ARM0_struct data_FreeRTOS2BareMetal;			// Copy the info from OCM_FreeRTOS2BareMetal

//offline data
volatile debug_SoC_offline_fpga_data_struct data_offline_fpga[DEBUG_SOC_OFFLINE_DATA_FPGA_MAX_MEMORY_SIZE] __attribute__((section(".FPGAdata_offline")));
volatile debug_SoC_offline_arm0_data_struct data_offline_arm0[DEBUG_SOC_OFFLINE_DATA_ARM0_MAX_MEMORY_SIZE] __attribute__((section(".ARM0data_offline")));
debug_SoC_parameters_struct debug_SoC_parameters;

#endif /* SRC_MAIN_H_ */
