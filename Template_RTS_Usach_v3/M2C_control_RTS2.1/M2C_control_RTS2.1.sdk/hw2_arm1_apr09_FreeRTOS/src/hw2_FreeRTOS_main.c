/******************************************************************************
*
* Copyright (C) 2016 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

#include "netif/xadapter.h"
#include "platform_config.h"

#include "main.h"


/************************** Constant Definitions *****************************/
#define THREAD_STACKSIZE 1024


/************************** Variable Definitions *****************************/
// flag when new data from BareMetal arrives
u8 new_arm0_data_flag = 0;
// flag when new data from FPGA arrives
u8 new_fpga_data_flag = 0;

u16 plot_online_data = 0;
u16 counter_plot_fpga_data = 0;
u16 counter_plot_arm0_data = 0;

s16 update_parameters_to_ARM0 = 0;

s16 start_test = 0;
s16 running_test = 0;

const s32 interval = 20000;		//10000/50*number_of_periods

volatile u32 counter_100us = 0;

float VzSigma_peak = 0.0;
float Vcm_peak = 0.0;

int main()
{
	u16 Status;

//	Xil_ICacheDisable(); 	//Disable the instruction caches.
//	Xil_L1DCacheDisable(); 	//Disable the level 1 data cache.
	Xil_L1ICacheDisable(); 	//Disable level 1 the instruction cache. kkkkkkkkkkkkkkkkkkkkkkkkkkkk
//	Xil_L2CacheDisable(); 	//Disable the L2 cache.

	//for(int i=0; i<10000000; i++);

	/* Initialize timer PS for measure algorithms. */
	Status = Timer_PS_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("Control_interrupt Initialization Failed\r\n");
		return XST_FAILURE;
	}

	//Delay for wait memories to get init
	for(int i=0; i<10000000; i++);

	sys_thread_new("main_thrd", (void(*)(void*))main_thread, 0,
	                THREAD_STACKSIZE,
	                DEFAULT_THREAD_PRIO);
	vTaskStartScheduler();
	while(1);
	return 0;
}

int main_thread()
{
	int Status;
	//printf("FreeRTOS: SoC_Debug Test\r\n");

	Status = init_interrupt();
	if (Status != XST_SUCCESS) {
		xil_printf("init interrupt Failed\r\n");
		return XST_FAILURE;
	}

	/*
	 * Initialize the GPIO interrupt driver
	 */
	Status = Gpio_interrupt_init();
	if (Status != XST_SUCCESS)
	{
		xil_printf("Gpio Interrupt Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/* Initialize parameters for debug SoC online and offline*/
	init_debug_SoC_parameters();

	OCM_FreeRTOS2BareMetal.activate.PR_s = 1;
	OCM_FreeRTOS2BareMetal.activate.PR_z = 1;
	OCM_FreeRTOS2BareMetal.activate.PR_o = 1;
	OCM_FreeRTOS2BareMetal.activate.notch_z = 1;
	OCM_FreeRTOS2BareMetal.activate.control_FPGA = 1;
	OCM_FreeRTOS2BareMetal.activate.selector_NLC = 1;
	OCM_FreeRTOS2BareMetal.ref.Vo_peak = 1500; //330
	OCM_FreeRTOS2BareMetal.ref.Vo_freq_KHz = 2.0;
	OCM_FreeRTOS2BareMetal.activate.notch_Vc_transf = 1;;

	counter_100us = 0;

	printf("RTOS: ready\r\n");
    while (1)
    {
    	if(start_test == 1)
    	{
    		counter_100us = 0;		//Reset counter
//    		debug_SoC_parameters.enable = 1;	//Start getting data into RAM
    		start_test = 0;			// Only run once
    		running_test = 1;		// to change setpoints

    		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_NLC_Data_M2C_NLC_6cell_Control_Vo, 0x1);	// 1 for classic NLC, 0 for proposed NLC
    		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + VzSigma_peak_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(0.0,13));	// Deactivate circulating reference
   			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vcm_peak_ref_Data_M2C_NLC_6cell_Control_Vo, 	 toFix(0.0,13));	// Deactivate common mode reference
    		printf("RTOS: interval1;\n");
    	}

    	if(running_test == 1)
    	{
    		if(counter_100us == 1*interval)	//activate proposed NLC
    		{
    			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_NLC_Data_M2C_NLC_6cell_Control_Vo, 0x0);	// 1 for classic NLC, 0 for proposed NLC
    			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + VzSigma_peak_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(0.0,13));	// Deactivate circulating reference
    			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vcm_peak_ref_Data_M2C_NLC_6cell_Control_Vo, 	 toFix(0.0,13));	// Deactivate common mode reference
    			printf("RTOS: interval2;\n");
    			running_test = 0;
    		}
    		if(counter_100us == 2*interval)	//activate proposed NLC + circulating injection
    		{
    			Vcm_peak = 30.0;//80.0;
    			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_NLC_Data_M2C_NLC_6cell_Control_Vo, 0x0);	// 1 for classic NLC, 0 for proposed NLC
   				Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vcm_peak_ref_Data_M2C_NLC_6cell_Control_Vo, 	 toFix(Vcm_peak,13)); //30 for Vo=180 n=6
   				//Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + VzSigma_peak_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(0.7*OCM_FreeRTOS2BareMetal.ref.Vo_peak/(2*3*2),13));	// Activate circulating reference
    			//Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vcm_peak_ref_Data_M2C_NLC_6cell_Control_Vo, 	 toFix(0.0,13));	// Deactivate common mode reference
    			printf("RTOS: interval3;\n");
    		}
    		if(counter_100us == 3*interval)	//activate proposed NLC + common mode injection
    		{
    			VzSigma_peak = 30.0;//80.0;
    			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_NLC_Data_M2C_NLC_6cell_Control_Vo, 0x0);	// 1 for classic NLC, 0 for proposed NLC
    			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + VzSigma_peak_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(VzSigma_peak,13));	// 1 for classic NLC, 0 for proposed NLC
    			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vcm_peak_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(Vcm_peak,13));//	 toFix(350.0/6.0,13));	// Activate common mode reference
    			//Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + VzSigma_peak_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(0.0,13));	// Deactivate circulating reference
    			//Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vcm_peak_ref_Data_M2C_NLC_6cell_Control_Vo, 	 toFix(350.0/6.0,13));	// Activate common mode reference
    			printf("RTOS: interval4;\n");
    			running_test = 0;
    		}
    		if(counter_100us == 4*interval)	//activate proposed NLC + circulating and common mode injection
    		{
    			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_NLC_Data_M2C_NLC_6cell_Control_Vo, 0x0);	// 1 for classic NLC, 0 for proposed NLC
    			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + VzSigma_peak_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(0.7*OCM_FreeRTOS2BareMetal.ref.Vo_peak/(2*3*2),13));	// Activate circulating reference
    			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vcm_peak_ref_Data_M2C_NLC_6cell_Control_Vo, 	 toFix(350.0/6.0,13));	// Activate common mode reference
    			//printf("RTOS: interval5;\n");
    		}
    		if(counter_100us == 5*interval)	//end test
    		{
    			printf("RTOS: end test;\n");
        		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + selector_NLC_Data_M2C_NLC_6cell_Control_Vo, 0x1);	// 1 for classic NLC, 0 for proposed NLC
        		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + VzSigma_peak_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(0.0,13));	// Deactivate circulating reference
       			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vcm_peak_ref_Data_M2C_NLC_6cell_Control_Vo, 	 toFix(0.0,13));	// Deactivate common mode reference
    			OCM_FreeRTOS2BareMetal.ref.Vo_peak = 300.0;
    			update_parameters_to_ARM0 = 1;
    			running_test = 0;
    		}
    	}

		if(debug_SoC_parameters.enable == 1)
		{
			if(debug_SoC_parameters.busy == 0)
			{
				//Add a hw reset!!

				//Enable REG = 0
				FPGA_TO_DDRRAM_mWriteReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_ENABLE_OFFSET, 0x0);

				//select variables to store
				u32 select_temp;
				select_temp = ((debug_SoC_parameters.selector_fpga[3]&0xFF)<<24) | ((debug_SoC_parameters.selector_fpga[2]&0xFF)<<16) | ((debug_SoC_parameters.selector_fpga[1]&0xFF)<<8) | (debug_SoC_parameters.selector_fpga[0]&0xFF);
				FPGA_TO_DDRRAM_mWriteReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_SELECTOR_DATA_0_3_OFFSET, select_temp);
				select_temp = ((debug_SoC_parameters.selector_fpga[7]&0xFF)<<24) | ((debug_SoC_parameters.selector_fpga[6]&0xFF)<<16) | ((debug_SoC_parameters.selector_fpga[5]&0xFF)<<8) | (debug_SoC_parameters.selector_fpga[4]&0xFF);
				FPGA_TO_DDRRAM_mWriteReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_SELECTOR_DATA_4_7_OFFSET, select_temp);
				select_temp = ((debug_SoC_parameters.selector_fpga[11]&0xFF)<<24) | ((debug_SoC_parameters.selector_fpga[10]&0xFF)<<16) | ((debug_SoC_parameters.selector_fpga[9]&0xFF)<<8) | (debug_SoC_parameters.selector_fpga[8]&0xFF);
				FPGA_TO_DDRRAM_mWriteReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_SELECTOR_DATA_8_11_OFFSET, select_temp);
				select_temp = ((debug_SoC_parameters.selector_fpga[15]&0xFF)<<24) | ((debug_SoC_parameters.selector_fpga[14]&0xFF)<<16) | ((debug_SoC_parameters.selector_fpga[13]&0xFF)<<8) | (debug_SoC_parameters.selector_fpga[12]&0xFF);
				FPGA_TO_DDRRAM_mWriteReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_SELECTOR_DATA_12_15_OFFSET, select_temp);

				//set sample clock
				debug_SoC_parameters.sample_clock_div = 100000 / debug_SoC_parameters.fast_sample_frec_kHz;	// 100MHz / fast_sample_frec_kHz / 1000;
				FPGA_TO_DDRRAM_mWriteReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_SAMPLE_CLK_DIV_OFFSET, debug_SoC_parameters.sample_clock_div);
				//set Max data points
				FPGA_TO_DDRRAM_mWriteReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_MAX_DATA_POINTS_OFFSET, debug_SoC_parameters.max_data_points);
				//set CH quantity (Not implemented at the moment)
				//FPGA_TO_DDRRAM_mWriteReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_CH_QUANTITY_OFFSET, debug_SoC_parameters.ch_quantity);

				//Activate fpga_to_ddrram
				//Enable REG = 1
				FPGA_TO_DDRRAM_mWriteReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_ENABLE_OFFSET, 0x1);

				//Activate debug in ARM0. Eliminate this when IP is updated with done pin
				OCM_FreeRTOS2BareMetal.activate_debugARM0 = 1;
				//update_parameters_to_ARM0 = 1;

				debug_SoC_parameters.busy = 1;
			}
		}


		if(debug_SoC_parameters.ready_for_plot == 1)
		{
			u32 temp_var;
			temp_var = FPGA_TO_DDRRAM_mReadReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_STATUS_OFFSET);
			debug_SoC_parameters.status_done = (temp_var >> 24) & 0xF;
			debug_SoC_parameters.status_error = (temp_var >> 28) & 0xF;
			debug_SoC_parameters.status_burst_count = temp_var & 0x1FFFF;

			//Clear fpga enable
			FPGA_TO_DDRRAM_mWriteReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_DDRRAM_ENABLE_OFFSET, 0x0);

			//------------------------
			//print out fpga variables

			//Cache need to be invalidate in the range of offline data
			Xil_DCacheInvalidateRange(DEBUG_SOC_OFFLINE_DATA_FPGA_MEMORY_ADDRESS,16*4*(debug_SoC_parameters.max_data_points));

			// Delay is needed to wait until ARM1 can read DDR RAM (Cache is disabled right now). delay was set by trial and error
			for(int i=0; i<2000000;i++);

			printf("\n\nclear;\n");

			if(debug_SoC_parameters.plot_fpga_vars == 1)
			{
				//Print out 16 fpga variables
				for(int i=0; i<16; i++)
				{
					//printf("fpga_var%d = [", i);
					printf("fpga.var(%d,:) = [", i+1);
					for(int j=0; j<debug_SoC_parameters.max_data_points; j++)
					//for(int j=0; j<debug_SoC_parameters.max_data_points+DEBUG_SOC_OFFLINE_AVOID_DATA_OFFSET; j++)
					{
						//printf("%.3f ", toFloat(data_offline_fpga[j].variable[i], debug_SoC_parameters.bits_frac_fpga[debug_SoC_parameters.selector_fpga[i]]));
						printf("%.3f ", toFloat(data_offline_fpga[j].variable[i],
										debug_SoC_parameters.bits_word_fpga[debug_SoC_parameters.selector_fpga[i]],
										debug_SoC_parameters.bits_frac_fpga[debug_SoC_parameters.selector_fpga[i]]));
					}
					printf("];\n");
				}
			}

			//------------------------
			//print out arm0 variables

			//David To do. reemplazar "10" por frecuencia del procesador ARM0
			debug_SoC_parameters.counter_arm0 = 10 * debug_SoC_parameters.max_data_points / debug_SoC_parameters.fast_sample_frec_kHz;

			//Cache need to be invalidate in the range of offline data
			//Xil_DCacheInvalidateRange(DEBUG_SOC_OFFLINE_DATA_ARM0_MEMORY_ADDRESS,16*4*debug_SoC_parameters.counter_arm0);

			if(debug_SoC_parameters.plot_arm0_vars == 1)
			{
				if(debug_SoC_parameters.plot_arm0_ALL == 0)
				{
					//Print out 16 arm0 variables
					for(int i=0; i<16; i++)
					{
						//printf("arm0_var%d = [", i);
						printf("arm0.var(%d,:) = [", i+1);
						for(int j=0; j<debug_SoC_parameters.counter_arm0; j++)
						{
							printf("%.3f ", data_offline_arm0[j].variable[i]);
						}
						printf("];\n");
					}
				}
				else
				{
					//Print all 64 arm0 variables
					for(int i=0; i<64; i++)
					{
						//printf("arm0_var%d = [", i);
						printf("arm0.var(%d,:) = [", i+1);
						for(int j=0; j<debug_SoC_parameters.counter_arm0; j++)
						{
							printf("%.2f ", data_offline_arm0[j].variable[i]);
						}
						printf("];\n");
					}
				}



				//print out arm0 burst counter
				printf("arm0_burst_counter = [");
				for(int j=0; j<debug_SoC_parameters.counter_arm0; j++)
				{
					printf("%lu ", data_offline_arm0[j].burst_counter);
				}
				printf("];\n");
			}
			//print out usefull constants
			printf("fpga_sampling_freq = %lu;\n", debug_SoC_parameters.fast_sample_frec_kHz*1000);
			printf("fpga_data_points = %lu;\n", debug_SoC_parameters.max_data_points);
			printf("arm0_sampling_freq = 10000;\n");
			printf("arm0_data_points = %lu;\n", debug_SoC_parameters.counter_arm0);
			printf("fpga_activate = %d;\n", debug_SoC_parameters.plot_fpga_vars);
			printf("arm0_activate = %d;\n", debug_SoC_parameters.plot_arm0_vars);
			printf("arm0_ALL_activate = %d;\n", debug_SoC_parameters.plot_arm0_ALL);

			// Reset to zero enable and ready_for_plot
			debug_SoC_parameters.enable = 0;
			debug_SoC_parameters.ready_for_plot = 0;
			debug_SoC_parameters.busy = 0;
		}


    	if(plot_online_data == 1)
    	{
			if(new_fpga_data_flag == 1)
			{
				plot_data_fpga[counter_plot_fpga_data] = data_FPGA2FreeRTOS;
				counter_plot_fpga_data++;
				new_fpga_data_flag = 0;
			}

			if(new_arm0_data_flag == 1)
			{
				plot_data_arm0[counter_plot_arm0_data] = data_BareMetal2FreeRTOS;
				counter_plot_arm0_data++;
				new_arm0_data_flag = 0;
			}

			if(counter_plot_fpga_data == PLOT_DATA_LENGTH)
			{
				printf("clear;\n");
				//Print out 16 fpga variables
				for(int i=0; i<16; i++)
				{
					printf("online_fpga.var(%d,:) = [", i+1);
					for(int j=0; j<PLOT_DATA_LENGTH; j++)
					{
						printf("%.3f ", toFloat(plot_data_fpga[j].variable[i],
									debug_SoC_parameters.bits_word_fpga[debug_SoC_parameters.selector_fpga[i]],
									debug_SoC_parameters.bits_frac_fpga[debug_SoC_parameters.selector_fpga[i]]));
					}
					printf("];\n");
				}

				//Print out 16 arm0 variables
				for(int i=0; i<16; i++)
				{
					printf("online_arm0.var(%d,:) = [", i+1);
					for(int j=0; j<counter_plot_arm0_data; j++)
					{
						printf("%.3f ", plot_data_arm0[j].variable[i]);
					}
					printf("];\n");
				}

				// Reset plot counter and enable plot
				counter_plot_fpga_data = 0;
				counter_plot_arm0_data = 0;
				plot_online_data = 0;
			}
    	}


    	if(update_parameters_to_ARM0)
    	{
    		//Update time stamp on FreeRTOS
    		OCM_FreeRTOS2BareMetal.time_stamp++;

    		//send refs


    		//for(int i=0; i<16; i++)
			//{
    		//	OCM_FreeRTOS2BareMetal.selector_arm0[i] = debug_SoC_parameters.selector_arm0[i];
			//}

    		Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + VzSigma_peak_ref_Data_M2C_NLC_6cell_Control_Vo, toFix(VzSigma_peak,13));	// Deactivate circulating reference
   			Xil_Out32(XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR + Vcm_peak_ref_Data_M2C_NLC_6cell_Control_Vo, 	 toFix(Vcm_peak,13));	// Deactivate common mode reference

    		//Simulate the Interrupt on ARM0
    		XScuGic_SoftwareIntr(&InterruptController, INTC_DEVICE_INT_ID, XSCUGIC_SPI_CPU0_MASK);

    		//return variable to zero to avoid multiple calls
    		update_parameters_to_ARM0 = 0;
    	}


		if(XUartPs_IsReceiveData(STDIN_BASEADDRESS))
		{
			unsigned char inp;
			inp = XUartPs_RecvByte(STDIN_BASEADDRESS);      // reading from hyperterminal
			xil_printf("\nFreeRTOS char: %c\n\n", inp);     // printing data from hyperterminal
			Xil_Out32(0x29000, inp);						// save data to OCM ARM1
			inp = XUartPs_RecvByte(STDIN_BASEADDRESS);      // read the "\0" character
		}
	}
    vTaskDelete(NULL);
    return 0;
}


/*-------------------------------------------------------------------------------------*/
// IRQ function called when BareMetal is done storing data to the OCM.
// FreeRTOS should extract the OCM data from BareMetal into a global structure
void IRQ_ARM0_to_ARM1_Handler(void *CallbackRef)
{
	// Read ARM0 OCM and measure the time spend
	//Timer_start = XScuTimer_GetCounterValue(&Timer_PS);
	//Xil_DCacheInvalidateRange(0x20000,sizeof(OCM_BareMetal2FreeRTOS));
//	data_BareMetal2FreeRTOS = OCM_BareMetal2FreeRTOS;	//Copy data from OCM to global variable (DDR RAM)	//TODO descomentar
	//Timer_end = XScuTimer_GetCounterValue(&Timer_PS);
	//Timer_diff = Timer_start - Timer_end;
	//Timer_ns = ((Timer_diff * 1.0) / COUNTS_PER_SECOND) * 1000000000.0;

	//xil_printf("\t FreeRTOS: Interrupt from BareMetal %d \n", data_BareMetal2FreeRTOS.time_stamp);
	//printf("\t Read timming: %lu cycles\t %.1f ns.\n", Timer_diff, Timer_ns);

	counter_100us ++;
	// update new_slow_data flag
	new_arm0_data_flag = 1;
}


/*-----------------------------------------------------------------------------------------------------
void IRQ_FPGA_to_OCM_Handler(void *CallbackRef)
{
	//xil_printf("FPGA interrupt\n");

	//Cache must be invalidate in order to ensure correct data read
	Xil_DCacheInvalidateRange(DEBUG_SOC_ONLINE_DATA_FPGA_MEMORY_ADDRESS, sizeof(OCM_FPGA2FreeRTOS));

	//Copy data from OCM to DDRRAM global variable
	data_FPGA2FreeRTOS = OCM_FPGA2FreeRTOS;

	//Rise flag for use in while(1)
	new_fpga_data_flag = 1;

	//Read status from fpga
	//u32 temp_var;
	//temp_var = FPGA_TO_ARM_DEBUG_mReadReg(XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR, FPGA_TO_ARM_OCM_STATUS_OFFSET);
	//debug_SoC_parameters.online.status_burst_count = temp_var & 0x3FFF;
	//debug_SoC_parameters.online.status_done = (temp_var >> 24) & 0xF;
	//debug_SoC_parameters.online.status_error = (temp_var >> 28) & 0xF;

	// Clear the Interrupt
	//XGpio_InterruptClear(&Gpio_interrupt_OCM, GPIO_CHANNEL);
}
*/


/*-----------------------------------------------------------------------------------------------------*/
void IRQ_FPGA_to_DDRRAM_Handler(void *CallbackRef)
{
	//printf("FPGA interrupt DDRRAM\n");

	//Set "ready for plot" to 1 for use in while(1)
	debug_SoC_parameters.ready_for_plot = 1;

	// Clear the Interrupt
	XGpio_InterruptClear(&Gpio_interrupt_DDRRAM, GPIO_CHANNEL);
}


int init_interrupt(void)
{
	int Status;
	XScuGic_Config *GicConfig;    //The configuration parameters of the controller
	/*
	 * Initialize the interrupt controller driver so that it is ready to
	 * use.
	 */
	GicConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
	if (NULL == GicConfig) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(&InterruptController, GicConfig,
					GicConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test to ensure that the hardware was built
	 * correctly
	 */
	Status = XScuGic_SelfTest(&InterruptController);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Connect the interrupt controller interrupt handler to the hardware
	 * interrupt handling logic in the ARM processor.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(Xil_ExceptionHandler) XScuGic_InterruptHandler,
			&InterruptController);

	/*
	 * Enable interrupts in the ARM
	 */
	Xil_ExceptionEnable();

	/*
	 * Connect a device driver handler that will be called when an
	 * interrupt for the device occurs, the device driver handler performs
	 * the specific interrupt processing for the device
	 */
	Status = XScuGic_Connect(&InterruptController, INTC_DEVICE_INT_ID,
			   (Xil_ExceptionHandler)IRQ_ARM0_to_ARM1_Handler,
			   (void *)&InterruptController);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Enable the interrupt for the device and then cause (simulate) an
	 * interrupt so the handlers will be called
	 */
	XScuGic_Enable(&InterruptController, INTC_DEVICE_INT_ID);

	return XST_SUCCESS;
}


int Timer_PS_init(void)
{
	int Status;
	XScuTimer_Config *ConfigPtr;

	/*
	 * Initialize the Scu Private Timer so that it is ready to use.
	 */
	ConfigPtr = XScuTimer_LookupConfig(TIMER_PS_DEVICE_ID);

	/*
	 * This is where the virtual address would be used, this example
	 * uses physical address.
	 */
	Status = XScuTimer_CfgInitialize(&Timer_PS, ConfigPtr,
				 ConfigPtr->BaseAddr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	// Timer was already set on BareMetal, so nothing else is needed

	/*
	 * Load the timer counter register.
	 */
	//XScuTimer_LoadTimer(&Timer_PS, COUNTS_PER_SECOND);
//	XScuTimer_LoadTimer(&Timer_PS, 0xFFFFFFFF);
//
//	//u8 prescaler = 0;
//	//prescaler = XScuTimer_GetPrescaler(&Timer_PS);
//	//xil_printf("\tprescaler: %d\n", prescaler);
//
//	XScuTimer_EnableAutoReload(&Timer_PS);
//
//	// Start the Scu Private Timer device.
//	XScuTimer_Start(&Timer_PS);

	return XST_SUCCESS;
}

int Gpio_interrupt_init(void)
{
	int Status;

	// GPIO FOR ONLINE DATA

	/* Initialize the GPIO driver */
//	Status = XGpio_Initialize(&Gpio_interrupt_OCM, XPAR_AXI_GPIO_INTERRUPT_OCM_DEVICE_ID);
//	if (Status != XST_SUCCESS) {
//		xil_printf("Gpio interrupt PL Initialization Failed\r\n");
//		return XST_FAILURE;
//	}

	/* Set the direction for all signals as inputs */
	//XGpio_SetDataDirection(&Gpio_interrupt_OCM, GPIO_CHANNEL, 0x00000000);

	/*
	 * Enable the GPIO channel interrupts can be
	 * detected and enable interrupts for the GPIO device
	 */
	//XGpio_InterruptEnable(&Gpio_interrupt_OCM, GPIO_CHANNEL);
	//XGpio_InterruptGlobalEnable(&Gpio_interrupt_OCM);


	/*
	 * Connect the interrupt handler that will be called when an
	 * interrupt occurs for the device.
	 */
//	Status = XScuGic_Connect(&InterruptController, XPAR_FABRIC_AXI_GPIO_INTERRUPT_OCM_IP2INTC_IRPT_INTR,
//				 (Xil_ExceptionHandler)IRQ_FPGA_to_OCM_Handler, &Gpio_interrupt_OCM);
//	if (Status != XST_SUCCESS) {
//		return Status;
//	}

	/* Enable the interrupt for the GPIO device.*/
	//XScuGic_Enable(&InterruptController, XPAR_FABRIC_AXI_GPIO_INTERRUPT_OCM_IP2INTC_IRPT_INTR);


	// GPIO FOR OFFLINE DATA

	/* Initialize the GPIO driver */
	Status = XGpio_Initialize(&Gpio_interrupt_DDRRAM, XPAR_AXI_GPIO_INTERRUPT_DDRRAM_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Gpio interrupt PL Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/* Set the direction for all signals as inputs */
	XGpio_SetDataDirection(&Gpio_interrupt_DDRRAM, GPIO_CHANNEL, 0x00000000);

	/*
	 * Enable the GPIO channel interrupts can be
	 * detected and enable interrupts for the GPIO device
	 */
	XGpio_InterruptEnable(&Gpio_interrupt_DDRRAM, GPIO_CHANNEL);
	XGpio_InterruptGlobalEnable(&Gpio_interrupt_DDRRAM);


	/*
	 * Connect the interrupt handler that will be called when an
	 * interrupt occurs for the device.
	 */
	Status = XScuGic_Connect(&InterruptController, XPAR_FABRIC_AXI_GPIO_INTERRUPT_DDRRAM_IP2INTC_IRPT_INTR,
				 (Xil_ExceptionHandler)IRQ_FPGA_to_DDRRAM_Handler, &Gpio_interrupt_DDRRAM);
	if (Status != XST_SUCCESS) {
		return Status;
	}

	/* Enable the interrupt for the GPIO device.*/
	XScuGic_Enable(&InterruptController, XPAR_FABRIC_AXI_GPIO_INTERRUPT_DDRRAM_IP2INTC_IRPT_INTR);

	return XST_SUCCESS;
}

float toFloat(int num, int bit_word, int bit_frac)
{
	int temp = 32 - bit_word;
	num = num<<temp;
	num = num>>temp;
	return ((float)num/(float)(1 << bit_frac));
}


int toFix(float num,int bit_sig)
{
	int result;

	result=(int)floor(num*pow(2,bit_sig));//round en vez de round

	return (result);
}




int init_debug_SoC_parameters(void)
{
	// init offline data parameters
	debug_SoC_parameters.enable = 0;
	debug_SoC_parameters.ch_quantity = 0x3;
	debug_SoC_parameters.fast_sample_frec_kHz = 25;	//50kHz@1000  20ms
	debug_SoC_parameters.max_data_points = 1000;
	debug_SoC_parameters.plot_fpga_vars = 1;
	debug_SoC_parameters.plot_arm0_vars = 1;
	debug_SoC_parameters.plot_arm0_ALL = 1;

	debug_SoC_parameters.ready_for_plot = 0;
	debug_SoC_parameters.busy = 0;
	debug_SoC_parameters.update_parameters = 1;

	//debug_SoC_parameters.ch_quantity = 0x3;
	debug_SoC_parameters.enable_slow_mode = 0;
	debug_SoC_parameters.finish_slow_mode = 0;
	debug_SoC_parameters.skip_samples_slow_mode = 0;
	debug_SoC_parameters.sample_frec_slow_mode = 500;
	debug_SoC_parameters.max_data_points_slow_mode = 3000;

//	// For view NLCs
//	debug_SoC_parameters.selector_fpga[0] = 17;	//
//	debug_SoC_parameters.selector_fpga[1] = 18;	//
//	debug_SoC_parameters.selector_fpga[2] = 19;	//
//	debug_SoC_parameters.selector_fpga[3] = 20;	//
//	debug_SoC_parameters.selector_fpga[4] = 42;	//
//	debug_SoC_parameters.selector_fpga[5] = 43;	//
//	debug_SoC_parameters.selector_fpga[6] = 44;	//
//	debug_SoC_parameters.selector_fpga[7] = 45;	//
//	debug_SoC_parameters.selector_fpga[8] = 55;	//
//	debug_SoC_parameters.selector_fpga[9] = 56;	//
//	debug_SoC_parameters.selector_fpga[10] = 57;//
//	debug_SoC_parameters.selector_fpga[11] =52;	//
//	debug_SoC_parameters.selector_fpga[12] =58;	//
//	debug_SoC_parameters.selector_fpga[13] =59;	//
//	debug_SoC_parameters.selector_fpga[14] =60;	//
//	debug_SoC_parameters.selector_fpga[15] =26;	//

//	// For debug NLC upgrades
//	debug_SoC_parameters.selector_fpga[0] = 0;	// Vza
//	debug_SoC_parameters.selector_fpga[1] = 1;	// Vzb
//	debug_SoC_parameters.selector_fpga[2] = 2;	// Vzc
//	debug_SoC_parameters.selector_fpga[3] = 4;	// Vcm
//	debug_SoC_parameters.selector_fpga[4] = 5;	// VaS
//	debug_SoC_parameters.selector_fpga[5] = 6;	// VbS
//	debug_SoC_parameters.selector_fpga[6] = 7;	// VcS
//	debug_SoC_parameters.selector_fpga[7] = 8;	// VaD
//	debug_SoC_parameters.selector_fpga[8] = 9;	// VbD
//	debug_SoC_parameters.selector_fpga[9] = 10;	// VcD
//	debug_SoC_parameters.selector_fpga[10] =52; // Level_aS
//	debug_SoC_parameters.selector_fpga[11] =53;	// Level_bS
//	debug_SoC_parameters.selector_fpga[12] =54;	// Level_cS
//	debug_SoC_parameters.selector_fpga[13] =55;	// Level_aD
//	debug_SoC_parameters.selector_fpga[14] =56;	// Level_bD
//	debug_SoC_parameters.selector_fpga[15] =57;	// Level_cD

	// For debug NLC upgrades
	debug_SoC_parameters.selector_fpga[0] = 25;	// Io
	debug_SoC_parameters.selector_fpga[1] = 26;	// Vo
	debug_SoC_parameters.selector_fpga[2] = 29;	// Vo LPF
	debug_SoC_parameters.selector_fpga[3] = 17;	// Vap*
	debug_SoC_parameters.selector_fpga[4] = 14;	// Ia
	debug_SoC_parameters.selector_fpga[5] = 15;	// Ib
	debug_SoC_parameters.selector_fpga[6] = 16;	// Ic
	debug_SoC_parameters.selector_fpga[7] = 42;	// Vap* level
	debug_SoC_parameters.selector_fpga[8] = 30;	// iap
	debug_SoC_parameters.selector_fpga[9] = 31;	// ibp
	debug_SoC_parameters.selector_fpga[10] =32; // icp
	debug_SoC_parameters.selector_fpga[11] =33;	// ian
	debug_SoC_parameters.selector_fpga[12] =34;	// ibn
	debug_SoC_parameters.selector_fpga[13] =35;	// icn
	debug_SoC_parameters.selector_fpga[14] =5;	// VaS prop
	debug_SoC_parameters.selector_fpga[15] =8;	// VaD prop


    debug_SoC_parameters.selector_arm0[0] = 36;        //Vcap
    debug_SoC_parameters.selector_arm0[1] = 37;        //Vcbp
    debug_SoC_parameters.selector_arm0[2] = 38;        //Vccp
    debug_SoC_parameters.selector_arm0[3] = 39;        //Vcan
    debug_SoC_parameters.selector_arm0[4] = 40;        //Vcbn
    debug_SoC_parameters.selector_arm0[5] = 41;        //Vccn
    debug_SoC_parameters.selector_arm0[6] = 52;    //Vc_al_S
    debug_SoC_parameters.selector_arm0[7] = 53;    //Vc_be_S
    debug_SoC_parameters.selector_arm0[8] = 54;    //Vc_0_S
    debug_SoC_parameters.selector_arm0[9] = 55;    //Vc_al_D
    debug_SoC_parameters.selector_arm0[10] = 56;    //Vc_be_D
    debug_SoC_parameters.selector_arm0[11] = 57;    //Vc_0_D
    debug_SoC_parameters.selector_arm0[12] = 58;    //id_plus
    debug_SoC_parameters.selector_arm0[13] = 59;
    debug_SoC_parameters.selector_arm0[14] = 61;
    debug_SoC_parameters.selector_arm0[15] = 62;

	// For fpga variables, define amount of bits dedicated for whole word. For Unsigned variables use 1 extra bit
	debug_SoC_parameters.bits_word_fpga[0] = 25;
	debug_SoC_parameters.bits_word_fpga[1] = 25;
	debug_SoC_parameters.bits_word_fpga[2] = 25;
	debug_SoC_parameters.bits_word_fpga[3] = 25;
	debug_SoC_parameters.bits_word_fpga[4] = 25;
	debug_SoC_parameters.bits_word_fpga[5] = 25;
	debug_SoC_parameters.bits_word_fpga[6] = 25;
	debug_SoC_parameters.bits_word_fpga[7] = 25;
	debug_SoC_parameters.bits_word_fpga[8] = 25;
	debug_SoC_parameters.bits_word_fpga[9] = 25;
	debug_SoC_parameters.bits_word_fpga[10] = 25;
	debug_SoC_parameters.bits_word_fpga[11] = 4;
	debug_SoC_parameters.bits_word_fpga[12] = 4;
	debug_SoC_parameters.bits_word_fpga[13] = 25;
	debug_SoC_parameters.bits_word_fpga[14] = 25;
	debug_SoC_parameters.bits_word_fpga[15] = 25;
	debug_SoC_parameters.bits_word_fpga[16] = 25;
	debug_SoC_parameters.bits_word_fpga[17] = 25;
	debug_SoC_parameters.bits_word_fpga[18] = 25;
	debug_SoC_parameters.bits_word_fpga[19] = 25;
	debug_SoC_parameters.bits_word_fpga[20] = 25;
	debug_SoC_parameters.bits_word_fpga[21] = 25;
	debug_SoC_parameters.bits_word_fpga[22] = 25;
	debug_SoC_parameters.bits_word_fpga[23] = 25;
	debug_SoC_parameters.bits_word_fpga[24] = 25;
	debug_SoC_parameters.bits_word_fpga[25] = 25;
	debug_SoC_parameters.bits_word_fpga[26] = 25;
	debug_SoC_parameters.bits_word_fpga[27] = 25;
	debug_SoC_parameters.bits_word_fpga[28] = 5;
	debug_SoC_parameters.bits_word_fpga[29] = 25;
	debug_SoC_parameters.bits_word_fpga[30] = 25;
	debug_SoC_parameters.bits_word_fpga[31] = 25;
	debug_SoC_parameters.bits_word_fpga[32] = 25;
	debug_SoC_parameters.bits_word_fpga[33] = 25;
	debug_SoC_parameters.bits_word_fpga[34] = 25;
	debug_SoC_parameters.bits_word_fpga[35] = 25;
	debug_SoC_parameters.bits_word_fpga[36] = 25;
	debug_SoC_parameters.bits_word_fpga[37] = 25;
	debug_SoC_parameters.bits_word_fpga[38] = 25;
	debug_SoC_parameters.bits_word_fpga[39] = 25;
	debug_SoC_parameters.bits_word_fpga[40] = 25;
	debug_SoC_parameters.bits_word_fpga[41] = 25;
	debug_SoC_parameters.bits_word_fpga[42] = 4;
	debug_SoC_parameters.bits_word_fpga[43] = 4;
	debug_SoC_parameters.bits_word_fpga[44] = 4;
	debug_SoC_parameters.bits_word_fpga[45] = 4;
	debug_SoC_parameters.bits_word_fpga[46] = 4;
	debug_SoC_parameters.bits_word_fpga[47] = 4;
	debug_SoC_parameters.bits_word_fpga[48] = 25;
	debug_SoC_parameters.bits_word_fpga[49] = 25;
	debug_SoC_parameters.bits_word_fpga[50] = 2;
	debug_SoC_parameters.bits_word_fpga[51] = 2;
	debug_SoC_parameters.bits_word_fpga[52] = 4;
	debug_SoC_parameters.bits_word_fpga[53] = 4;
	debug_SoC_parameters.bits_word_fpga[54] = 4;
	debug_SoC_parameters.bits_word_fpga[55] = 4;
	debug_SoC_parameters.bits_word_fpga[56] = 4;
	debug_SoC_parameters.bits_word_fpga[57] = 4;
	debug_SoC_parameters.bits_word_fpga[58] = 4;
	debug_SoC_parameters.bits_word_fpga[59] = 4;
	debug_SoC_parameters.bits_word_fpga[60] = 4;
	debug_SoC_parameters.bits_word_fpga[61] = 4;
	debug_SoC_parameters.bits_word_fpga[62] = 4;
	debug_SoC_parameters.bits_word_fpga[63] = 4;

	// For fpga variables, define amount of bits dedicated for fractional part
	debug_SoC_parameters.bits_frac_fpga[0] = 13;
	debug_SoC_parameters.bits_frac_fpga[1] = 13;
	debug_SoC_parameters.bits_frac_fpga[2] = 13;
	debug_SoC_parameters.bits_frac_fpga[3] = 21;
	debug_SoC_parameters.bits_frac_fpga[4] = 13;
	debug_SoC_parameters.bits_frac_fpga[5] = 12;
	debug_SoC_parameters.bits_frac_fpga[6] = 12;
	debug_SoC_parameters.bits_frac_fpga[7] = 12;
	debug_SoC_parameters.bits_frac_fpga[8] = 12;
	debug_SoC_parameters.bits_frac_fpga[9] = 12;
	debug_SoC_parameters.bits_frac_fpga[10] = 12;
	debug_SoC_parameters.bits_frac_fpga[11] = 0;
	debug_SoC_parameters.bits_frac_fpga[12] = 0;
	debug_SoC_parameters.bits_frac_fpga[13] = 13;
	debug_SoC_parameters.bits_frac_fpga[14] = 16;
	debug_SoC_parameters.bits_frac_fpga[15] = 16;
	debug_SoC_parameters.bits_frac_fpga[16] = 16;
	debug_SoC_parameters.bits_frac_fpga[17] = 13;
	debug_SoC_parameters.bits_frac_fpga[18] = 13;
	debug_SoC_parameters.bits_frac_fpga[19] = 13;
	debug_SoC_parameters.bits_frac_fpga[20] = 13;
	debug_SoC_parameters.bits_frac_fpga[21] = 13;
	debug_SoC_parameters.bits_frac_fpga[22] = 13;
	debug_SoC_parameters.bits_frac_fpga[23] = 13;
	debug_SoC_parameters.bits_frac_fpga[24] = 21;
	debug_SoC_parameters.bits_frac_fpga[25] = 16;
	debug_SoC_parameters.bits_frac_fpga[26] = 12;
	debug_SoC_parameters.bits_frac_fpga[27] = 21;
	debug_SoC_parameters.bits_frac_fpga[28] = 0;
	debug_SoC_parameters.bits_frac_fpga[29] = 12;
	debug_SoC_parameters.bits_frac_fpga[30] = 16;
	debug_SoC_parameters.bits_frac_fpga[31] = 16;
	debug_SoC_parameters.bits_frac_fpga[32] = 16;
	debug_SoC_parameters.bits_frac_fpga[33] = 16;
	debug_SoC_parameters.bits_frac_fpga[34] = 16;
	debug_SoC_parameters.bits_frac_fpga[35] = 16;
	debug_SoC_parameters.bits_frac_fpga[36] = 16;
	debug_SoC_parameters.bits_frac_fpga[37] = 16;
	debug_SoC_parameters.bits_frac_fpga[38] = 16;
	debug_SoC_parameters.bits_frac_fpga[39] = 16;
	debug_SoC_parameters.bits_frac_fpga[40] = 16;
	debug_SoC_parameters.bits_frac_fpga[41] = 16;
	debug_SoC_parameters.bits_frac_fpga[42] = 0;
	debug_SoC_parameters.bits_frac_fpga[43] = 0;
	debug_SoC_parameters.bits_frac_fpga[44] = 0;
	debug_SoC_parameters.bits_frac_fpga[45] = 0;
	debug_SoC_parameters.bits_frac_fpga[46] = 0;
	debug_SoC_parameters.bits_frac_fpga[47] = 0;
	debug_SoC_parameters.bits_frac_fpga[48] = 13;
	debug_SoC_parameters.bits_frac_fpga[49] = 21;
	debug_SoC_parameters.bits_frac_fpga[50] = 0;
	debug_SoC_parameters.bits_frac_fpga[51] = 0;
	debug_SoC_parameters.bits_frac_fpga[52] = 0;
	debug_SoC_parameters.bits_frac_fpga[53] = 0;
	debug_SoC_parameters.bits_frac_fpga[54] = 0;
	debug_SoC_parameters.bits_frac_fpga[55] = 0;
	debug_SoC_parameters.bits_frac_fpga[56] = 0;
	debug_SoC_parameters.bits_frac_fpga[57] = 0;
	debug_SoC_parameters.bits_frac_fpga[58] = 0;
	debug_SoC_parameters.bits_frac_fpga[59] = 0;
	debug_SoC_parameters.bits_frac_fpga[60] = 0;
	debug_SoC_parameters.bits_frac_fpga[61] = 0;
	debug_SoC_parameters.bits_frac_fpga[62] = 0;
	debug_SoC_parameters.bits_frac_fpga[63] = 0;

	return XST_SUCCESS;
}
