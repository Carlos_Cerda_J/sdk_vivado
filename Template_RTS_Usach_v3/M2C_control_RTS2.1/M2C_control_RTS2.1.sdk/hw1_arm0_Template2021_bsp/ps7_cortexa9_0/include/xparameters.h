#ifndef XPARAMETERS_H   /* prevent circular inclusions */
#define XPARAMETERS_H   /* by using protection macros */

/* Definition for CPU ID */
#define XPAR_CPU_ID 0U

/* Definitions for peripheral PS7_CORTEXA9_0 */
#define XPAR_PS7_CORTEXA9_0_CPU_CLK_FREQ_HZ 666666687


/******************************************************************/

/* Canonical definitions for peripheral PS7_CORTEXA9_0 */
#define XPAR_CPU_CORTEXA9_0_CPU_CLK_FREQ_HZ 666666687


/******************************************************************/

#include "xparameters_ps.h"

#define STDIN_BASEADDRESS 0xE0000000
#define STDOUT_BASEADDRESS 0xE0000000

/******************************************************************/

/* Definitions for driver ADC_MAX11331_AXI */
#define XPAR_ADC_MAX11331_AXI_NUM_INSTANCES 1

/* Definitions for peripheral SLOW_ADC_ADC_MAX11331_AXI_0 */
#define XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_DEVICE_ID 0
#define XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_BASEADDR 0x43C50000
#define XPAR_SLOW_ADC_ADC_MAX11331_AXI_0_S0_AXI_LITE_HIGHADDR 0x43C5FFFF


/******************************************************************/

/* Definitions for driver FPGA_TO_DDRRAM */
#define XPAR_FPGA_TO_DDRRAM_NUM_INSTANCES 1

/* Definitions for peripheral FPGA_TO_DDRRAM_0 */
#define XPAR_FPGA_TO_DDRRAM_0_DEVICE_ID 0
#define XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_BASEADDR 0x43DF0000
#define XPAR_FPGA_TO_DDRRAM_0_S_AXI_LITE_HIGHADDR 0x43DFFFFF


/******************************************************************/


/* Definitions for peripheral PS7_DDR_0 */
#define XPAR_PS7_DDR_0_S_AXI_BASEADDR 0x00100000
#define XPAR_PS7_DDR_0_S_AXI_HIGHADDR 0x1FFFFFFF


/******************************************************************/

/* Definitions for driver DEBUG_8_CHANNEL */
#define XPAR_DEBUG_8_CHANNEL_NUM_INSTANCES 4

/* Definitions for peripheral DEBUG_8_CHANNEL_1 */
#define XPAR_DEBUG_8_CHANNEL_1_DEVICE_ID 0
#define XPAR_DEBUG_8_CHANNEL_1_S_AXI_LITE_BASEADDR 0x43FA0000
#define XPAR_DEBUG_8_CHANNEL_1_S_AXI_LITE_HIGHADDR 0x43FAFFFF


/* Definitions for peripheral DEBUG_8_CHANNEL_2 */
#define XPAR_DEBUG_8_CHANNEL_2_DEVICE_ID 1
#define XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_BASEADDR 0x43FB0000
#define XPAR_DEBUG_8_CHANNEL_2_S_AXI_LITE_HIGHADDR 0x43FBFFFF


/* Definitions for peripheral DEBUG_8_FAST1 */
#define XPAR_DEBUG_8_FAST1_DEVICE_ID 2
#define XPAR_DEBUG_8_FAST1_S_AXI_LITE_BASEADDR 0x43CF0000
#define XPAR_DEBUG_8_FAST1_S_AXI_LITE_HIGHADDR 0x43CFFFFF


/* Definitions for peripheral DEBUG_8_FAST2 */
#define XPAR_DEBUG_8_FAST2_DEVICE_ID 3
#define XPAR_DEBUG_8_FAST2_S_AXI_LITE_BASEADDR 0x43C10000
#define XPAR_DEBUG_8_FAST2_S_AXI_LITE_HIGHADDR 0x43C1FFFF


/******************************************************************/

/* Definitions for driver DEVCFG */
#define XPAR_XDCFG_NUM_INSTANCES 1U

/* Definitions for peripheral PS7_DEV_CFG_0 */
#define XPAR_PS7_DEV_CFG_0_DEVICE_ID 0U
#define XPAR_PS7_DEV_CFG_0_BASEADDR 0xF8007000U
#define XPAR_PS7_DEV_CFG_0_HIGHADDR 0xF80070FFU


/******************************************************************/

/* Canonical definitions for peripheral PS7_DEV_CFG_0 */
#define XPAR_XDCFG_0_DEVICE_ID XPAR_PS7_DEV_CFG_0_DEVICE_ID
#define XPAR_XDCFG_0_BASEADDR 0xF8007000U
#define XPAR_XDCFG_0_HIGHADDR 0xF80070FFU


/******************************************************************/

/* Definitions for driver DMAPS */
#define XPAR_XDMAPS_NUM_INSTANCES 2

/* Definitions for peripheral PS7_DMA_NS */
#define XPAR_PS7_DMA_NS_DEVICE_ID 0
#define XPAR_PS7_DMA_NS_BASEADDR 0xF8004000
#define XPAR_PS7_DMA_NS_HIGHADDR 0xF8004FFF


/* Definitions for peripheral PS7_DMA_S */
#define XPAR_PS7_DMA_S_DEVICE_ID 1
#define XPAR_PS7_DMA_S_BASEADDR 0xF8003000
#define XPAR_PS7_DMA_S_HIGHADDR 0xF8003FFF


/******************************************************************/

/* Canonical definitions for peripheral PS7_DMA_NS */
#define XPAR_XDMAPS_0_DEVICE_ID XPAR_PS7_DMA_NS_DEVICE_ID
#define XPAR_XDMAPS_0_BASEADDR 0xF8004000
#define XPAR_XDMAPS_0_HIGHADDR 0xF8004FFF

/* Canonical definitions for peripheral PS7_DMA_S */
#define XPAR_XDMAPS_1_DEVICE_ID XPAR_PS7_DMA_S_DEVICE_ID
#define XPAR_XDMAPS_1_BASEADDR 0xF8003000
#define XPAR_XDMAPS_1_HIGHADDR 0xF8003FFF


/******************************************************************/

/* Definitions for driver EMACPS */
#define XPAR_XEMACPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_ETHERNET_0 */
#define XPAR_PS7_ETHERNET_0_DEVICE_ID 0
#define XPAR_PS7_ETHERNET_0_BASEADDR 0xE000B000
#define XPAR_PS7_ETHERNET_0_HIGHADDR 0xE000BFFF
#define XPAR_PS7_ETHERNET_0_ENET_CLK_FREQ_HZ 25000000
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_1000MBPS_DIV0 8
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_1000MBPS_DIV1 1
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_100MBPS_DIV0 8
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_100MBPS_DIV1 5
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_10MBPS_DIV0 8
#define XPAR_PS7_ETHERNET_0_ENET_SLCR_10MBPS_DIV1 50


/******************************************************************/

#define XPAR_PS7_ETHERNET_0_IS_CACHE_COHERENT 0
/* Canonical definitions for peripheral PS7_ETHERNET_0 */
#define XPAR_XEMACPS_0_DEVICE_ID XPAR_PS7_ETHERNET_0_DEVICE_ID
#define XPAR_XEMACPS_0_BASEADDR 0xE000B000
#define XPAR_XEMACPS_0_HIGHADDR 0xE000BFFF
#define XPAR_XEMACPS_0_ENET_CLK_FREQ_HZ 25000000
#define XPAR_XEMACPS_0_ENET_SLCR_1000Mbps_DIV0 8
#define XPAR_XEMACPS_0_ENET_SLCR_1000Mbps_DIV1 1
#define XPAR_XEMACPS_0_ENET_SLCR_100Mbps_DIV0 8
#define XPAR_XEMACPS_0_ENET_SLCR_100Mbps_DIV1 5
#define XPAR_XEMACPS_0_ENET_SLCR_10Mbps_DIV0 8
#define XPAR_XEMACPS_0_ENET_SLCR_10Mbps_DIV1 50


/******************************************************************/


/* Definitions for peripheral ADC_CONVERTER_0 */
#define XPAR_ADC_CONVERTER_0_BASEADDR 0x43F10000
#define XPAR_ADC_CONVERTER_0_HIGHADDR 0x43F1FFFF


/* Definitions for peripheral LL_TO_LN_IP_0 */
#define XPAR_LL_TO_LN_IP_0_BASEADDR 0x43F80000
#define XPAR_LL_TO_LN_IP_0_HIGHADDR 0x43F8FFFF


/* Definitions for peripheral M2C_NLC_6CELL_CONTROL_VO_0 */
#define XPAR_M2C_NLC_6CELL_CONTROL_VO_0_BASEADDR 0x43C60000
#define XPAR_M2C_NLC_6CELL_CONTROL_VO_0_HIGHADDR 0x43C6FFFF


/* Definitions for peripheral NOTCH_CLUSTER_CURRENTS_0 */
#define XPAR_NOTCH_CLUSTER_CURRENTS_0_BASEADDR 0x43C20000
#define XPAR_NOTCH_CLUSTER_CURRENTS_0_HIGHADDR 0x43C2FFFF


/* Definitions for peripheral PLL2_IP_0 */
#define XPAR_PLL2_IP_0_BASEADDR 0x43E40000
#define XPAR_PLL2_IP_0_HIGHADDR 0x43E4FFFF


/* Definitions for peripheral PR_1MHZ_0 */
#define XPAR_PR_1MHZ_0_BASEADDR 0x43FC0000
#define XPAR_PR_1MHZ_0_HIGHADDR 0x43FCFFFF


/* Definitions for peripheral SINE_LUT_0 */
#define XPAR_SINE_LUT_0_BASEADDR 0x43E80000
#define XPAR_SINE_LUT_0_HIGHADDR 0x43E8FFFF


/* Definitions for peripheral TRIP_CURR_BITS_0 */
#define XPAR_TRIP_CURR_BITS_0_BASEADDR 0x43ED0000
#define XPAR_TRIP_CURR_BITS_0_HIGHADDR 0x43EDFFFF


/* Definitions for peripheral FAST_ADC_CONV_0 */
#define XPAR_FAST_ADC_CONV_0_BASEADDR 0x43CD0000
#define XPAR_FAST_ADC_CONV_0_HIGHADDR 0x43CDFFFF


/* Definitions for peripheral FAST_ADC_CONV_1 */
#define XPAR_FAST_ADC_CONV_1_BASEADDR 0x43C30000
#define XPAR_FAST_ADC_CONV_1_HIGHADDR 0x43C3FFFF


/* Definitions for peripheral PS7_AFI_0 */
#define XPAR_PS7_AFI_0_S_AXI_BASEADDR 0xF8008000
#define XPAR_PS7_AFI_0_S_AXI_HIGHADDR 0xF8008FFF


/* Definitions for peripheral PS7_AFI_1 */
#define XPAR_PS7_AFI_1_S_AXI_BASEADDR 0xF8009000
#define XPAR_PS7_AFI_1_S_AXI_HIGHADDR 0xF8009FFF


/* Definitions for peripheral PS7_AFI_2 */
#define XPAR_PS7_AFI_2_S_AXI_BASEADDR 0xF800A000
#define XPAR_PS7_AFI_2_S_AXI_HIGHADDR 0xF800AFFF


/* Definitions for peripheral PS7_AFI_3 */
#define XPAR_PS7_AFI_3_S_AXI_BASEADDR 0xF800B000
#define XPAR_PS7_AFI_3_S_AXI_HIGHADDR 0xF800BFFF


/* Definitions for peripheral PS7_DDRC_0 */
#define XPAR_PS7_DDRC_0_S_AXI_BASEADDR 0xF8006000
#define XPAR_PS7_DDRC_0_S_AXI_HIGHADDR 0xF8006FFF


/* Definitions for peripheral PS7_GLOBALTIMER_0 */
#define XPAR_PS7_GLOBALTIMER_0_S_AXI_BASEADDR 0xF8F00200
#define XPAR_PS7_GLOBALTIMER_0_S_AXI_HIGHADDR 0xF8F002FF


/* Definitions for peripheral PS7_GPV_0 */
#define XPAR_PS7_GPV_0_S_AXI_BASEADDR 0xF8900000
#define XPAR_PS7_GPV_0_S_AXI_HIGHADDR 0xF89FFFFF


/* Definitions for peripheral PS7_INTC_DIST_0 */
#define XPAR_PS7_INTC_DIST_0_S_AXI_BASEADDR 0xF8F01000
#define XPAR_PS7_INTC_DIST_0_S_AXI_HIGHADDR 0xF8F01FFF


/* Definitions for peripheral PS7_IOP_BUS_CONFIG_0 */
#define XPAR_PS7_IOP_BUS_CONFIG_0_S_AXI_BASEADDR 0xE0200000
#define XPAR_PS7_IOP_BUS_CONFIG_0_S_AXI_HIGHADDR 0xE0200FFF


/* Definitions for peripheral PS7_L2CACHEC_0 */
#define XPAR_PS7_L2CACHEC_0_S_AXI_BASEADDR 0xF8F02000
#define XPAR_PS7_L2CACHEC_0_S_AXI_HIGHADDR 0xF8F02FFF


/* Definitions for peripheral PS7_OCMC_0 */
#define XPAR_PS7_OCMC_0_S_AXI_BASEADDR 0xF800C000
#define XPAR_PS7_OCMC_0_S_AXI_HIGHADDR 0xF800CFFF


/* Definitions for peripheral PS7_PL310_0 */
#define XPAR_PS7_PL310_0_S_AXI_BASEADDR 0xF8F02000
#define XPAR_PS7_PL310_0_S_AXI_HIGHADDR 0xF8F02FFF


/* Definitions for peripheral PS7_PMU_0 */
#define XPAR_PS7_PMU_0_S_AXI_BASEADDR 0xF8891000
#define XPAR_PS7_PMU_0_S_AXI_HIGHADDR 0xF8891FFF
#define XPAR_PS7_PMU_0_PMU1_S_AXI_BASEADDR 0xF8893000
#define XPAR_PS7_PMU_0_PMU1_S_AXI_HIGHADDR 0xF8893FFF


/* Definitions for peripheral PS7_QSPI_LINEAR_0 */
#define XPAR_PS7_QSPI_LINEAR_0_S_AXI_BASEADDR 0xFC000000
#define XPAR_PS7_QSPI_LINEAR_0_S_AXI_HIGHADDR 0xFCFFFFFF


/* Definitions for peripheral PS7_RAM_0 */
#define XPAR_PS7_RAM_0_S_AXI_BASEADDR 0x00000000
#define XPAR_PS7_RAM_0_S_AXI_HIGHADDR 0x0003FFFF


/* Definitions for peripheral PS7_RAM_1 */
#define XPAR_PS7_RAM_1_S_AXI_BASEADDR 0xFFFC0000
#define XPAR_PS7_RAM_1_S_AXI_HIGHADDR 0xFFFFFFFF


/* Definitions for peripheral PS7_SCUC_0 */
#define XPAR_PS7_SCUC_0_S_AXI_BASEADDR 0xF8F00000
#define XPAR_PS7_SCUC_0_S_AXI_HIGHADDR 0xF8F000FC


/* Definitions for peripheral PS7_SLCR_0 */
#define XPAR_PS7_SLCR_0_S_AXI_BASEADDR 0xF8000000
#define XPAR_PS7_SLCR_0_S_AXI_HIGHADDR 0xF8000FFF


/* Definitions for peripheral SLOW_ADC_CONV_0 */
#define XPAR_SLOW_ADC_CONV_0_BASEADDR 0x43CA0000
#define XPAR_SLOW_ADC_CONV_0_HIGHADDR 0x43CAFFFF


/* Definitions for peripheral SLOW_ADC_CONV_1 */
#define XPAR_SLOW_ADC_CONV_1_BASEADDR 0x43D10000
#define XPAR_SLOW_ADC_CONV_1_HIGHADDR 0x43D1FFFF


/* Definitions for peripheral THETA_GEN_EXTERN_0 */
#define XPAR_THETA_GEN_EXTERN_0_BASEADDR 0x43DC0000
#define XPAR_THETA_GEN_EXTERN_0_HIGHADDR 0x43DCFFFF


/* Definitions for peripheral TRIP_DET_0 */
#define XPAR_TRIP_DET_0_BASEADDR 0x43D30000
#define XPAR_TRIP_DET_0_HIGHADDR 0x43D3FFFF


/* Definitions for peripheral VO_LPF_0 */
#define XPAR_VO_LPF_0_BASEADDR 0x43F40000
#define XPAR_VO_LPF_0_HIGHADDR 0x43F4FFFF


/******************************************************************/

/* Definitions for driver GPIO */
#define XPAR_XGPIO_NUM_INSTANCES 7

/* Definitions for peripheral FO_PRECARGA */
#define XPAR_FO_PRECARGA_BASEADDR 0x41220000
#define XPAR_FO_PRECARGA_HIGHADDR 0x4122FFFF
#define XPAR_FO_PRECARGA_DEVICE_ID 0
#define XPAR_FO_PRECARGA_INTERRUPT_PRESENT 0
#define XPAR_FO_PRECARGA_IS_DUAL 0


/* Definitions for peripheral AXI_GPIO_0 */
#define XPAR_AXI_GPIO_0_BASEADDR 0x41200000
#define XPAR_AXI_GPIO_0_HIGHADDR 0x4120FFFF
#define XPAR_AXI_GPIO_0_DEVICE_ID 1
#define XPAR_AXI_GPIO_0_INTERRUPT_PRESENT 0
#define XPAR_AXI_GPIO_0_IS_DUAL 0


/* Definitions for peripheral AXI_GPIO_1 */
#define XPAR_AXI_GPIO_1_BASEADDR 0x41260000
#define XPAR_AXI_GPIO_1_HIGHADDR 0x4126FFFF
#define XPAR_AXI_GPIO_1_DEVICE_ID 2
#define XPAR_AXI_GPIO_1_INTERRUPT_PRESENT 0
#define XPAR_AXI_GPIO_1_IS_DUAL 0


/* Definitions for peripheral AXI_GPIO_BAND */
#define XPAR_AXI_GPIO_BAND_BASEADDR 0x41240000
#define XPAR_AXI_GPIO_BAND_HIGHADDR 0x4124FFFF
#define XPAR_AXI_GPIO_BAND_DEVICE_ID 3
#define XPAR_AXI_GPIO_BAND_INTERRUPT_PRESENT 0
#define XPAR_AXI_GPIO_BAND_IS_DUAL 0


/* Definitions for peripheral AXI_GPIO_INPUT */
#define XPAR_AXI_GPIO_INPUT_BASEADDR 0x41210000
#define XPAR_AXI_GPIO_INPUT_HIGHADDR 0x4121FFFF
#define XPAR_AXI_GPIO_INPUT_DEVICE_ID 4
#define XPAR_AXI_GPIO_INPUT_INTERRUPT_PRESENT 1
#define XPAR_AXI_GPIO_INPUT_IS_DUAL 0


/* Definitions for peripheral AXI_GPIO_INTERRUPT_DDRRAM */
#define XPAR_AXI_GPIO_INTERRUPT_DDRRAM_BASEADDR 0x43F60000
#define XPAR_AXI_GPIO_INTERRUPT_DDRRAM_HIGHADDR 0x43F6FFFF
#define XPAR_AXI_GPIO_INTERRUPT_DDRRAM_DEVICE_ID 5
#define XPAR_AXI_GPIO_INTERRUPT_DDRRAM_INTERRUPT_PRESENT 1
#define XPAR_AXI_GPIO_INTERRUPT_DDRRAM_IS_DUAL 0


/* Definitions for peripheral AXI_GPIO_RESET */
#define XPAR_AXI_GPIO_RESET_BASEADDR 0x41230000
#define XPAR_AXI_GPIO_RESET_HIGHADDR 0x4123FFFF
#define XPAR_AXI_GPIO_RESET_DEVICE_ID 6
#define XPAR_AXI_GPIO_RESET_INTERRUPT_PRESENT 0
#define XPAR_AXI_GPIO_RESET_IS_DUAL 0


/******************************************************************/

/* Canonical definitions for peripheral FO_PRECARGA */
#define XPAR_GPIO_0_BASEADDR 0x41220000
#define XPAR_GPIO_0_HIGHADDR 0x4122FFFF
#define XPAR_GPIO_0_DEVICE_ID XPAR_FO_PRECARGA_DEVICE_ID
#define XPAR_GPIO_0_INTERRUPT_PRESENT 0
#define XPAR_GPIO_0_IS_DUAL 0

/* Canonical definitions for peripheral AXI_GPIO_0 */
#define XPAR_GPIO_1_BASEADDR 0x41200000
#define XPAR_GPIO_1_HIGHADDR 0x4120FFFF
#define XPAR_GPIO_1_DEVICE_ID XPAR_AXI_GPIO_0_DEVICE_ID
#define XPAR_GPIO_1_INTERRUPT_PRESENT 0
#define XPAR_GPIO_1_IS_DUAL 0

/* Canonical definitions for peripheral AXI_GPIO_1 */
#define XPAR_GPIO_2_BASEADDR 0x41260000
#define XPAR_GPIO_2_HIGHADDR 0x4126FFFF
#define XPAR_GPIO_2_DEVICE_ID XPAR_AXI_GPIO_1_DEVICE_ID
#define XPAR_GPIO_2_INTERRUPT_PRESENT 0
#define XPAR_GPIO_2_IS_DUAL 0

/* Canonical definitions for peripheral AXI_GPIO_BAND */
#define XPAR_GPIO_3_BASEADDR 0x41240000
#define XPAR_GPIO_3_HIGHADDR 0x4124FFFF
#define XPAR_GPIO_3_DEVICE_ID XPAR_AXI_GPIO_BAND_DEVICE_ID
#define XPAR_GPIO_3_INTERRUPT_PRESENT 0
#define XPAR_GPIO_3_IS_DUAL 0

/* Canonical definitions for peripheral AXI_GPIO_INPUT */
#define XPAR_GPIO_4_BASEADDR 0x41210000
#define XPAR_GPIO_4_HIGHADDR 0x4121FFFF
#define XPAR_GPIO_4_DEVICE_ID XPAR_AXI_GPIO_INPUT_DEVICE_ID
#define XPAR_GPIO_4_INTERRUPT_PRESENT 1
#define XPAR_GPIO_4_IS_DUAL 0

/* Canonical definitions for peripheral AXI_GPIO_INTERRUPT_DDRRAM */
#define XPAR_GPIO_5_BASEADDR 0x43F60000
#define XPAR_GPIO_5_HIGHADDR 0x43F6FFFF
#define XPAR_GPIO_5_DEVICE_ID XPAR_AXI_GPIO_INTERRUPT_DDRRAM_DEVICE_ID
#define XPAR_GPIO_5_INTERRUPT_PRESENT 1
#define XPAR_GPIO_5_IS_DUAL 0

/* Canonical definitions for peripheral AXI_GPIO_RESET */
#define XPAR_GPIO_6_BASEADDR 0x41230000
#define XPAR_GPIO_6_HIGHADDR 0x4123FFFF
#define XPAR_GPIO_6_DEVICE_ID XPAR_AXI_GPIO_RESET_DEVICE_ID
#define XPAR_GPIO_6_INTERRUPT_PRESENT 0
#define XPAR_GPIO_6_IS_DUAL 0


/******************************************************************/

/* Definitions for driver GPIOPS */
#define XPAR_XGPIOPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_GPIO_0 */
#define XPAR_PS7_GPIO_0_DEVICE_ID 0
#define XPAR_PS7_GPIO_0_BASEADDR 0xE000A000
#define XPAR_PS7_GPIO_0_HIGHADDR 0xE000AFFF


/******************************************************************/

/* Canonical definitions for peripheral PS7_GPIO_0 */
#define XPAR_XGPIOPS_0_DEVICE_ID XPAR_PS7_GPIO_0_DEVICE_ID
#define XPAR_XGPIOPS_0_BASEADDR 0xE000A000
#define XPAR_XGPIOPS_0_HIGHADDR 0xE000AFFF


/******************************************************************/

/* Definitions for driver IICPS */
#define XPAR_XIICPS_NUM_INSTANCES 2

/* Definitions for peripheral PS7_I2C_0 */
#define XPAR_PS7_I2C_0_DEVICE_ID 0
#define XPAR_PS7_I2C_0_BASEADDR 0xE0004000
#define XPAR_PS7_I2C_0_HIGHADDR 0xE0004FFF
#define XPAR_PS7_I2C_0_I2C_CLK_FREQ_HZ 111111115


/* Definitions for peripheral PS7_I2C_1 */
#define XPAR_PS7_I2C_1_DEVICE_ID 1
#define XPAR_PS7_I2C_1_BASEADDR 0xE0005000
#define XPAR_PS7_I2C_1_HIGHADDR 0xE0005FFF
#define XPAR_PS7_I2C_1_I2C_CLK_FREQ_HZ 111111115


/******************************************************************/

/* Canonical definitions for peripheral PS7_I2C_0 */
#define XPAR_XIICPS_0_DEVICE_ID XPAR_PS7_I2C_0_DEVICE_ID
#define XPAR_XIICPS_0_BASEADDR 0xE0004000
#define XPAR_XIICPS_0_HIGHADDR 0xE0004FFF
#define XPAR_XIICPS_0_I2C_CLK_FREQ_HZ 111111115

/* Canonical definitions for peripheral PS7_I2C_1 */
#define XPAR_XIICPS_1_DEVICE_ID XPAR_PS7_I2C_1_DEVICE_ID
#define XPAR_XIICPS_1_BASEADDR 0xE0005000
#define XPAR_XIICPS_1_HIGHADDR 0xE0005FFF
#define XPAR_XIICPS_1_I2C_CLK_FREQ_HZ 111111115


/******************************************************************/

/* Definitions for driver QSPIPS */
#define XPAR_XQSPIPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_QSPI_0 */
#define XPAR_PS7_QSPI_0_DEVICE_ID 0
#define XPAR_PS7_QSPI_0_BASEADDR 0xE000D000
#define XPAR_PS7_QSPI_0_HIGHADDR 0xE000DFFF
#define XPAR_PS7_QSPI_0_QSPI_CLK_FREQ_HZ 200000000
#define XPAR_PS7_QSPI_0_QSPI_MODE 0
#define XPAR_PS7_QSPI_0_QSPI_BUS_WIDTH 2


/******************************************************************/

/* Canonical definitions for peripheral PS7_QSPI_0 */
#define XPAR_XQSPIPS_0_DEVICE_ID XPAR_PS7_QSPI_0_DEVICE_ID
#define XPAR_XQSPIPS_0_BASEADDR 0xE000D000
#define XPAR_XQSPIPS_0_HIGHADDR 0xE000DFFF
#define XPAR_XQSPIPS_0_QSPI_CLK_FREQ_HZ 200000000
#define XPAR_XQSPIPS_0_QSPI_MODE 0
#define XPAR_XQSPIPS_0_QSPI_BUS_WIDTH 2


/******************************************************************/

/* Definitions for driver SAMPLE_CLOCK */
#define XPAR_SAMPLE_CLOCK_NUM_INSTANCES 2

/* Definitions for peripheral SAMPLE_CLOCK_PLL */
#define XPAR_SAMPLE_CLOCK_PLL_DEVICE_ID 0
#define XPAR_SAMPLE_CLOCK_PLL_S0_AXI_LITE_BASEADDR 0x43EE0000
#define XPAR_SAMPLE_CLOCK_PLL_S0_AXI_LITE_HIGHADDR 0x43EEFFFF


/* Definitions for peripheral SAMPLE_CLOCK_PULSE_SLOWADC */
#define XPAR_SAMPLE_CLOCK_PULSE_SLOWADC_DEVICE_ID 1
#define XPAR_SAMPLE_CLOCK_PULSE_SLOWADC_S0_AXI_LITE_BASEADDR 0x43C00000
#define XPAR_SAMPLE_CLOCK_PULSE_SLOWADC_S0_AXI_LITE_HIGHADDR 0x43C0FFFF


/******************************************************************/

/* Definitions for driver SAMPLE_CLOCK_50DUTY */
#define XPAR_SAMPLE_CLOCK_50DUTY_NUM_INSTANCES 4

/* Definitions for peripheral SAMPLE_CLOCK_50DUTY_0 */
#define XPAR_SAMPLE_CLOCK_50DUTY_0_DEVICE_ID 0
#define XPAR_SAMPLE_CLOCK_50DUTY_0_S_AXI_LITE_BASEADDR 0x43E70000
#define XPAR_SAMPLE_CLOCK_50DUTY_0_S_AXI_LITE_HIGHADDR 0x43E7FFFF


/* Definitions for peripheral SAMPLE_CLOCK_AVERAGE */
#define XPAR_SAMPLE_CLOCK_AVERAGE_DEVICE_ID 1
#define XPAR_SAMPLE_CLOCK_AVERAGE_S_AXI_LITE_BASEADDR 0x43D20000
#define XPAR_SAMPLE_CLOCK_AVERAGE_S_AXI_LITE_HIGHADDR 0x43D2FFFF


/* Definitions for peripheral SAMPLE_CLOCK_AVERAGE_SLOW */
#define XPAR_SAMPLE_CLOCK_AVERAGE_SLOW_DEVICE_ID 2
#define XPAR_SAMPLE_CLOCK_AVERAGE_SLOW_S_AXI_LITE_BASEADDR 0x43D90000
#define XPAR_SAMPLE_CLOCK_AVERAGE_SLOW_S_AXI_LITE_HIGHADDR 0x43D9FFFF


/* Definitions for peripheral SAMPLE_CLOCK_VOFILT */
#define XPAR_SAMPLE_CLOCK_VOFILT_DEVICE_ID 3
#define XPAR_SAMPLE_CLOCK_VOFILT_S_AXI_LITE_BASEADDR 0x43F50000
#define XPAR_SAMPLE_CLOCK_VOFILT_S_AXI_LITE_HIGHADDR 0x43F5FFFF


/******************************************************************/

/* Definitions for Fabric interrupts connected to ps7_scugic_0 */
#define XPAR_FABRIC_AXI_TIMER_INTERRUPT_TO_PS_INTERRUPT_INTR 61U
#define XPAR_FABRIC_AXI_GPIO_INPUT_IP2INTC_IRPT_INTR 62U
#define XPAR_FABRIC_AXI_GPIO_INTERRUPT_DDRRAM_IP2INTC_IRPT_INTR 63U

/******************************************************************/

/* Canonical definitions for Fabric interrupts connected to ps7_scugic_0 */
#define XPAR_FABRIC_TMRCTR_0_VEC_ID XPAR_FABRIC_AXI_TIMER_INTERRUPT_TO_PS_INTERRUPT_INTR
#define XPAR_FABRIC_GPIO_4_VEC_ID XPAR_FABRIC_AXI_GPIO_INPUT_IP2INTC_IRPT_INTR
#define XPAR_FABRIC_GPIO_5_VEC_ID XPAR_FABRIC_AXI_GPIO_INTERRUPT_DDRRAM_IP2INTC_IRPT_INTR

/******************************************************************/

/* Definitions for driver SCUGIC */
#define XPAR_XSCUGIC_NUM_INSTANCES 1U

/* Definitions for peripheral PS7_SCUGIC_0 */
#define XPAR_PS7_SCUGIC_0_DEVICE_ID 0U
#define XPAR_PS7_SCUGIC_0_BASEADDR 0xF8F00100U
#define XPAR_PS7_SCUGIC_0_HIGHADDR 0xF8F001FFU
#define XPAR_PS7_SCUGIC_0_DIST_BASEADDR 0xF8F01000U


/******************************************************************/

/* Canonical definitions for peripheral PS7_SCUGIC_0 */
#define XPAR_SCUGIC_0_DEVICE_ID 0U
#define XPAR_SCUGIC_0_CPU_BASEADDR 0xF8F00100U
#define XPAR_SCUGIC_0_CPU_HIGHADDR 0xF8F001FFU
#define XPAR_SCUGIC_0_DIST_BASEADDR 0xF8F01000U


/******************************************************************/

/* Definitions for driver SCUTIMER */
#define XPAR_XSCUTIMER_NUM_INSTANCES 1

/* Definitions for peripheral PS7_SCUTIMER_0 */
#define XPAR_PS7_SCUTIMER_0_DEVICE_ID 0
#define XPAR_PS7_SCUTIMER_0_BASEADDR 0xF8F00600
#define XPAR_PS7_SCUTIMER_0_HIGHADDR 0xF8F0061F


/******************************************************************/

/* Canonical definitions for peripheral PS7_SCUTIMER_0 */
#define XPAR_XSCUTIMER_0_DEVICE_ID XPAR_PS7_SCUTIMER_0_DEVICE_ID
#define XPAR_XSCUTIMER_0_BASEADDR 0xF8F00600
#define XPAR_XSCUTIMER_0_HIGHADDR 0xF8F0061F


/******************************************************************/

/* Definitions for driver SCUWDT */
#define XPAR_XSCUWDT_NUM_INSTANCES 1

/* Definitions for peripheral PS7_SCUWDT_0 */
#define XPAR_PS7_SCUWDT_0_DEVICE_ID 0
#define XPAR_PS7_SCUWDT_0_BASEADDR 0xF8F00620
#define XPAR_PS7_SCUWDT_0_HIGHADDR 0xF8F006FF


/******************************************************************/

/* Canonical definitions for peripheral PS7_SCUWDT_0 */
#define XPAR_SCUWDT_0_DEVICE_ID XPAR_PS7_SCUWDT_0_DEVICE_ID
#define XPAR_SCUWDT_0_BASEADDR 0xF8F00620
#define XPAR_SCUWDT_0_HIGHADDR 0xF8F006FF


/******************************************************************/

/* Definitions for driver SDPS */
#define XPAR_XSDPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_SD_0 */
#define XPAR_PS7_SD_0_DEVICE_ID 0
#define XPAR_PS7_SD_0_BASEADDR 0xE0100000
#define XPAR_PS7_SD_0_HIGHADDR 0xE0100FFF
#define XPAR_PS7_SD_0_SDIO_CLK_FREQ_HZ 100000000
#define XPAR_PS7_SD_0_HAS_CD 0
#define XPAR_PS7_SD_0_HAS_WP 0
#define XPAR_PS7_SD_0_BUS_WIDTH 0
#define XPAR_PS7_SD_0_MIO_BANK 0
#define XPAR_PS7_SD_0_HAS_EMIO 0


/******************************************************************/

#define XPAR_PS7_SD_0_IS_CACHE_COHERENT 0
/* Canonical definitions for peripheral PS7_SD_0 */
#define XPAR_XSDPS_0_DEVICE_ID XPAR_PS7_SD_0_DEVICE_ID
#define XPAR_XSDPS_0_BASEADDR 0xE0100000
#define XPAR_XSDPS_0_HIGHADDR 0xE0100FFF
#define XPAR_XSDPS_0_SDIO_CLK_FREQ_HZ 100000000
#define XPAR_XSDPS_0_HAS_CD 0
#define XPAR_XSDPS_0_HAS_WP 0
#define XPAR_XSDPS_0_BUS_WIDTH 0
#define XPAR_XSDPS_0_MIO_BANK 0
#define XPAR_XSDPS_0_HAS_EMIO 0


/******************************************************************/

/* Definitions for driver TMRCTR */
#define XPAR_XTMRCTR_NUM_INSTANCES 1U

/* Definitions for peripheral AXI_TIMER_INTERRUPT_TO_PS */
#define XPAR_AXI_TIMER_INTERRUPT_TO_PS_DEVICE_ID 0U
#define XPAR_AXI_TIMER_INTERRUPT_TO_PS_BASEADDR 0x42800000U
#define XPAR_AXI_TIMER_INTERRUPT_TO_PS_HIGHADDR 0x4280FFFFU
#define XPAR_AXI_TIMER_INTERRUPT_TO_PS_CLOCK_FREQ_HZ 100000000U


/******************************************************************/

/* Canonical definitions for peripheral AXI_TIMER_INTERRUPT_TO_PS */
#define XPAR_TMRCTR_0_DEVICE_ID 0U
#define XPAR_TMRCTR_0_BASEADDR 0x42800000U
#define XPAR_TMRCTR_0_HIGHADDR 0x4280FFFFU
#define XPAR_TMRCTR_0_CLOCK_FREQ_HZ XPAR_AXI_TIMER_INTERRUPT_TO_PS_CLOCK_FREQ_HZ

/******************************************************************/

/* Definitions for driver TRIP_DETECTOR_CURRENTS */
#define XPAR_TRIP_DETECTOR_CURRENTS_NUM_INSTANCES 1

/* Definitions for peripheral TRIP_DETECTOR_CURRENTS_0 */
#define XPAR_TRIP_DETECTOR_CURRENTS_0_DEVICE_ID 0
#define XPAR_TRIP_DETECTOR_CURRENTS_0_S_AXI_LITE_BASEADDR 0x43F30000
#define XPAR_TRIP_DETECTOR_CURRENTS_0_S_AXI_LITE_HIGHADDR 0x43F3FFFF


/******************************************************************/

/* Definitions for driver TRIP_DETECTOR_VOLTAGES */
#define XPAR_TRIP_DETECTOR_VOLTAGES_NUM_INSTANCES 1

/* Definitions for peripheral TRIP_DETECTOR_VOLTAGES_0 */
#define XPAR_TRIP_DETECTOR_VOLTAGES_0_DEVICE_ID 0
#define XPAR_TRIP_DETECTOR_VOLTAGES_0_S_AXI_BASEADDR 0x43D70000
#define XPAR_TRIP_DETECTOR_VOLTAGES_0_S_AXI_HIGHADDR 0x43D7FFFF


/******************************************************************/

/* Definitions for driver TTCPS */
#define XPAR_XTTCPS_NUM_INSTANCES 3U

/* Definitions for peripheral PS7_TTC_0 */
#define XPAR_PS7_TTC_0_DEVICE_ID 0U
#define XPAR_PS7_TTC_0_BASEADDR 0XF8001000U
#define XPAR_PS7_TTC_0_TTC_CLK_FREQ_HZ 111111115U
#define XPAR_PS7_TTC_0_TTC_CLK_CLKSRC 0U
#define XPAR_PS7_TTC_1_DEVICE_ID 1U
#define XPAR_PS7_TTC_1_BASEADDR 0XF8001004U
#define XPAR_PS7_TTC_1_TTC_CLK_FREQ_HZ 111111115U
#define XPAR_PS7_TTC_1_TTC_CLK_CLKSRC 0U
#define XPAR_PS7_TTC_2_DEVICE_ID 2U
#define XPAR_PS7_TTC_2_BASEADDR 0XF8001008U
#define XPAR_PS7_TTC_2_TTC_CLK_FREQ_HZ 111111115U
#define XPAR_PS7_TTC_2_TTC_CLK_CLKSRC 0U


/******************************************************************/

/* Canonical definitions for peripheral PS7_TTC_0 */
#define XPAR_XTTCPS_0_DEVICE_ID XPAR_PS7_TTC_0_DEVICE_ID
#define XPAR_XTTCPS_0_BASEADDR 0xF8001000U
#define XPAR_XTTCPS_0_TTC_CLK_FREQ_HZ 111111115U
#define XPAR_XTTCPS_0_TTC_CLK_CLKSRC 0U

#define XPAR_XTTCPS_1_DEVICE_ID XPAR_PS7_TTC_1_DEVICE_ID
#define XPAR_XTTCPS_1_BASEADDR 0xF8001004U
#define XPAR_XTTCPS_1_TTC_CLK_FREQ_HZ 111111115U
#define XPAR_XTTCPS_1_TTC_CLK_CLKSRC 0U

#define XPAR_XTTCPS_2_DEVICE_ID XPAR_PS7_TTC_2_DEVICE_ID
#define XPAR_XTTCPS_2_BASEADDR 0xF8001008U
#define XPAR_XTTCPS_2_TTC_CLK_FREQ_HZ 111111115U
#define XPAR_XTTCPS_2_TTC_CLK_CLKSRC 0U


/******************************************************************/

/* Definitions for driver UARTPS */
#define XPAR_XUARTPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_UART_0 */
#define XPAR_PS7_UART_0_DEVICE_ID 0
#define XPAR_PS7_UART_0_BASEADDR 0xE0000000
#define XPAR_PS7_UART_0_HIGHADDR 0xE0000FFF
#define XPAR_PS7_UART_0_UART_CLK_FREQ_HZ 100000000
#define XPAR_PS7_UART_0_HAS_MODEM 0


/******************************************************************/

/* Canonical definitions for peripheral PS7_UART_0 */
#define XPAR_XUARTPS_0_DEVICE_ID XPAR_PS7_UART_0_DEVICE_ID
#define XPAR_XUARTPS_0_BASEADDR 0xE0000000
#define XPAR_XUARTPS_0_HIGHADDR 0xE0000FFF
#define XPAR_XUARTPS_0_UART_CLK_FREQ_HZ 100000000
#define XPAR_XUARTPS_0_HAS_MODEM 0


/******************************************************************/

/* Definitions for driver USBPS */
#define XPAR_XUSBPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_USB_0 */
#define XPAR_PS7_USB_0_DEVICE_ID 0
#define XPAR_PS7_USB_0_BASEADDR 0xE0002000
#define XPAR_PS7_USB_0_HIGHADDR 0xE0002FFF


/******************************************************************/

/* Canonical definitions for peripheral PS7_USB_0 */
#define XPAR_XUSBPS_0_DEVICE_ID XPAR_PS7_USB_0_DEVICE_ID
#define XPAR_XUSBPS_0_BASEADDR 0xE0002000
#define XPAR_XUSBPS_0_HIGHADDR 0xE0002FFF


/******************************************************************/

/* Definitions for driver XADCPS */
#define XPAR_XADCPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_XADC_0 */
#define XPAR_PS7_XADC_0_DEVICE_ID 0
#define XPAR_PS7_XADC_0_BASEADDR 0xF8007100
#define XPAR_PS7_XADC_0_HIGHADDR 0xF8007120


/******************************************************************/

/* Canonical definitions for peripheral PS7_XADC_0 */
#define XPAR_XADCPS_0_DEVICE_ID XPAR_PS7_XADC_0_DEVICE_ID
#define XPAR_XADCPS_0_BASEADDR 0xF8007100
#define XPAR_XADCPS_0_HIGHADDR 0xF8007120


/******************************************************************/

#endif  /* end of protection macro */
