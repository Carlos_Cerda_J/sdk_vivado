connect -url tcp:127.0.0.1:3121
source C:/Users/david/Documents/Vivado_projects/MMC_AIC_6CELL/M2C_control_RTS2.1/M2C_control_RTS2.1.sdk/m2c_control_wrapper_hw_platform_2/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "JTAG-ONB4 2516330017D8A"} -index 0
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "JTAG-ONB4 2516330017D8A" && level==0} -index 1
fpga -file C:/Users/david/Documents/Vivado_projects/MMC_AIC_6CELL/M2C_control_RTS2.1/M2C_control_RTS2.1.sdk/m2c_control_wrapper_hw_platform_2/m2c_control_wrapper.bit
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "JTAG-ONB4 2516330017D8A"} -index 0
loadhw -hw C:/Users/david/Documents/Vivado_projects/MMC_AIC_6CELL/M2C_control_RTS2.1/M2C_control_RTS2.1.sdk/m2c_control_wrapper_hw_platform_2/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "JTAG-ONB4 2516330017D8A"} -index 0
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "JTAG-ONB4 2516330017D8A"} -index 0
dow C:/Users/david/Documents/Vivado_projects/MMC_AIC_6CELL/M2C_control_RTS2.1/M2C_control_RTS2.1.sdk/hw2_arm0_apr09_Paper_n6/Debug/hw2_arm0_apr09_Paper_n6.elf
targets -set -nocase -filter {name =~ "ARM*#1" && jtag_cable_name =~ "JTAG-ONB4 2516330017D8A"} -index 0
dow C:/Users/david/Documents/Vivado_projects/MMC_AIC_6CELL/M2C_control_RTS2.1/M2C_control_RTS2.1.sdk/hw2_arm1_apr09_FreeRTOS/Debug/hw2_arm1_apr09_FreeRTOS.elf
configparams force-mem-access 0
bpadd -addr &main
