connect -url tcp:127.0.0.1:3121
source C:/Vivado/Template_RTS_Usach_v3/M2C_control_RTS2.1/M2C_control_RTS2.1.sdk/m2c_control_wrapper_hw_platform_4/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "JTAG-ONB4 251633001742A"} -index 0
loadhw -hw C:/Vivado/Template_RTS_Usach_v3/M2C_control_RTS2.1/M2C_control_RTS2.1.sdk/m2c_control_wrapper_hw_platform_4/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "JTAG-ONB4 251633001742A"} -index 0
stop
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "JTAG-ONB4 251633001742A"} -index 0
rst -processor
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "JTAG-ONB4 251633001742A"} -index 0
dow C:/Vivado/Template_RTS_Usach_v3/M2C_control_RTS2.1/M2C_control_RTS2.1.sdk/hw4_amr0_template2021/Debug/hw4_amr0_template2021.elf
configparams force-mem-access 0
bpadd -addr &main
