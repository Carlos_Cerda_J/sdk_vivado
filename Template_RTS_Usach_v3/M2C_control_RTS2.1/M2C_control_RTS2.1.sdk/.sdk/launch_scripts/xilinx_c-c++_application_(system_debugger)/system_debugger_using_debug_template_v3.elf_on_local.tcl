connect -url tcp:127.0.0.1:3121
source C:/Users/David/Documents/Vivado_Projects/RTS_Usach/vivado/RTS_Usach.sdk/pefft_system_wrapper_hw_platform_1/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "JTAG-ONB4 2516330017D8A"} -index 0
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "JTAG-ONB4 2516330017D8A" && level==0} -index 1
fpga -file C:/Users/David/Documents/Vivado_Projects/RTS_Usach/vivado/RTS_Usach.sdk/pefft_system_wrapper_hw_platform_1/pefft_system_wrapper.bit
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "JTAG-ONB4 2516330017D8A"} -index 0
loadhw -hw C:/Users/David/Documents/Vivado_Projects/RTS_Usach/vivado/RTS_Usach.sdk/pefft_system_wrapper_hw_platform_1/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "JTAG-ONB4 2516330017D8A"} -index 0
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "JTAG-ONB4 2516330017D8A"} -index 0
dow C:/Users/David/Documents/Vivado_Projects/RTS_Usach/vivado/RTS_Usach.sdk/Template_v3/Debug/Template_v3.elf
configparams force-mem-access 0
bpadd -addr &main
