-- (c) Copyright 1995-2021 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: E2Tech:user:average_8_samples_slow_adc:2.0
-- IP Revision: 4

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY m2c_control_average_8_samples_slow_adc_0_0 IS
  PORT (
    clock : IN STD_LOGIC;
    reset_n : IN STD_LOGIC;
    band : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_1 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_2 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_3 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_4 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_5 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_6 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_7 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_8 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_9 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_10 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_11 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_12 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_13 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_14 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_15 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_16 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_17 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_18 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_19 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_20 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_21 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_22 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_23 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_24 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_25 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_26 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_27 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_28 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_29 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_30 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_31 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_32 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_33 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_34 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_35 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_36 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_37 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_38 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_39 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_in_40 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_1 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_2 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_3 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_4 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_5 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_6 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_7 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_8 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_9 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_10 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_11 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_12 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_13 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_14 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_15 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_16 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_17 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_18 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_19 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_20 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_21 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_22 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_23 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_24 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_25 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_26 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_27 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_28 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_29 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_30 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_31 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_32 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_33 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_34 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_35 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_36 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_37 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_38 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_39 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    data_out_40 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    new_value : OUT STD_LOGIC
  );
END m2c_control_average_8_samples_slow_adc_0_0;

ARCHITECTURE m2c_control_average_8_samples_slow_adc_0_0_arch OF m2c_control_average_8_samples_slow_adc_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF m2c_control_average_8_samples_slow_adc_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT average_8_samples_slow_adc IS
    PORT (
      clock : IN STD_LOGIC;
      reset_n : IN STD_LOGIC;
      band : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_1 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_2 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_3 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_4 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_5 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_6 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_7 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_8 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_9 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_10 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_11 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_12 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_13 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_14 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_15 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_16 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_17 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_18 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_19 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_20 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_21 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_22 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_23 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_24 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_25 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_26 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_27 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_28 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_29 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_30 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_31 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_32 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_33 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_34 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_35 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_36 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_37 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_38 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_39 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_in_40 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_1 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_2 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_3 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_4 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_5 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_6 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_7 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_8 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_9 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_10 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_11 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_12 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_13 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_14 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_15 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_16 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_17 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_18 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_19 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_20 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_21 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_22 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_23 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_24 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_25 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_26 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_27 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_28 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_29 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_30 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_31 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_32 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_33 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_34 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_35 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_36 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_37 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_38 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_39 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      data_out_40 : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
      new_value : OUT STD_LOGIC
    );
  END COMPONENT average_8_samples_slow_adc;
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF reset_n: SIGNAL IS "XIL_INTERFACENAME reset_n, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF reset_n: SIGNAL IS "xilinx.com:signal:reset:1.0 reset_n RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF clock: SIGNAL IS "XIL_INTERFACENAME clock, ASSOCIATED_RESET reset_n, FREQ_HZ 100000000, PHASE 0.000";
  ATTRIBUTE X_INTERFACE_INFO OF clock: SIGNAL IS "xilinx.com:signal:clock:1.0 clock CLK";
BEGIN
  U0 : average_8_samples_slow_adc
    PORT MAP (
      clock => clock,
      reset_n => reset_n,
      band => band,
      data_in_1 => data_in_1,
      data_in_2 => data_in_2,
      data_in_3 => data_in_3,
      data_in_4 => data_in_4,
      data_in_5 => data_in_5,
      data_in_6 => data_in_6,
      data_in_7 => data_in_7,
      data_in_8 => data_in_8,
      data_in_9 => data_in_9,
      data_in_10 => data_in_10,
      data_in_11 => data_in_11,
      data_in_12 => data_in_12,
      data_in_13 => data_in_13,
      data_in_14 => data_in_14,
      data_in_15 => data_in_15,
      data_in_16 => data_in_16,
      data_in_17 => data_in_17,
      data_in_18 => data_in_18,
      data_in_19 => data_in_19,
      data_in_20 => data_in_20,
      data_in_21 => data_in_21,
      data_in_22 => data_in_22,
      data_in_23 => data_in_23,
      data_in_24 => data_in_24,
      data_in_25 => data_in_25,
      data_in_26 => data_in_26,
      data_in_27 => data_in_27,
      data_in_28 => data_in_28,
      data_in_29 => data_in_29,
      data_in_30 => data_in_30,
      data_in_31 => data_in_31,
      data_in_32 => data_in_32,
      data_in_33 => data_in_33,
      data_in_34 => data_in_34,
      data_in_35 => data_in_35,
      data_in_36 => data_in_36,
      data_in_37 => data_in_37,
      data_in_38 => data_in_38,
      data_in_39 => data_in_39,
      data_in_40 => data_in_40,
      data_out_1 => data_out_1,
      data_out_2 => data_out_2,
      data_out_3 => data_out_3,
      data_out_4 => data_out_4,
      data_out_5 => data_out_5,
      data_out_6 => data_out_6,
      data_out_7 => data_out_7,
      data_out_8 => data_out_8,
      data_out_9 => data_out_9,
      data_out_10 => data_out_10,
      data_out_11 => data_out_11,
      data_out_12 => data_out_12,
      data_out_13 => data_out_13,
      data_out_14 => data_out_14,
      data_out_15 => data_out_15,
      data_out_16 => data_out_16,
      data_out_17 => data_out_17,
      data_out_18 => data_out_18,
      data_out_19 => data_out_19,
      data_out_20 => data_out_20,
      data_out_21 => data_out_21,
      data_out_22 => data_out_22,
      data_out_23 => data_out_23,
      data_out_24 => data_out_24,
      data_out_25 => data_out_25,
      data_out_26 => data_out_26,
      data_out_27 => data_out_27,
      data_out_28 => data_out_28,
      data_out_29 => data_out_29,
      data_out_30 => data_out_30,
      data_out_31 => data_out_31,
      data_out_32 => data_out_32,
      data_out_33 => data_out_33,
      data_out_34 => data_out_34,
      data_out_35 => data_out_35,
      data_out_36 => data_out_36,
      data_out_37 => data_out_37,
      data_out_38 => data_out_38,
      data_out_39 => data_out_39,
      data_out_40 => data_out_40,
      new_value => new_value
    );
END m2c_control_average_8_samples_slow_adc_0_0_arch;
