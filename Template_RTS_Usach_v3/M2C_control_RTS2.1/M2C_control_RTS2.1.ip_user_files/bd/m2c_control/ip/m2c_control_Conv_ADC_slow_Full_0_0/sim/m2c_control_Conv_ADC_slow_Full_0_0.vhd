-- (c) Copyright 1995-2021 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:ip:Conv_ADC_slow_Full:1.0
-- IP Revision: 2105261242

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY m2c_control_Conv_ADC_slow_Full_0_0 IS
  PORT (
    IPCORE_CLK : IN STD_LOGIC;
    IPCORE_RESETN : IN STD_LOGIC;
    ADC1 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC2 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC3 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC4 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC5 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC6 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC7 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC8 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC9 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC10 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC11 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC12 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC13 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC14 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC15 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC16 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC17 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC18 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC19 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC20 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC21 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC22 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC23 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    ADC24 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    AXI4_Lite_ACLK : IN STD_LOGIC;
    AXI4_Lite_ARESETN : IN STD_LOGIC;
    AXI4_Lite_AWADDR : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    AXI4_Lite_AWVALID : IN STD_LOGIC;
    AXI4_Lite_WDATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI4_Lite_WSTRB : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    AXI4_Lite_WVALID : IN STD_LOGIC;
    AXI4_Lite_BREADY : IN STD_LOGIC;
    AXI4_Lite_ARADDR : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    AXI4_Lite_ARVALID : IN STD_LOGIC;
    AXI4_Lite_RREADY : IN STD_LOGIC;
    alpha1 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha2 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha3 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha4 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha5 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha6 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha7 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha8 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha9 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha10 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha11 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha12 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha13 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha14 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha15 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha16 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha17 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha18 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha19 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha20 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha21 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha22 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha23 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    alpha24 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    AXI4_Lite_AWREADY : OUT STD_LOGIC;
    AXI4_Lite_WREADY : OUT STD_LOGIC;
    AXI4_Lite_BRESP : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI4_Lite_BVALID : OUT STD_LOGIC;
    AXI4_Lite_ARREADY : OUT STD_LOGIC;
    AXI4_Lite_RDATA : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    AXI4_Lite_RRESP : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    AXI4_Lite_RVALID : OUT STD_LOGIC
  );
END m2c_control_Conv_ADC_slow_Full_0_0;

ARCHITECTURE m2c_control_Conv_ADC_slow_Full_0_0_arch OF m2c_control_Conv_ADC_slow_Full_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF m2c_control_Conv_ADC_slow_Full_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT Conv_ADC_slow_Full IS
    PORT (
      IPCORE_CLK : IN STD_LOGIC;
      IPCORE_RESETN : IN STD_LOGIC;
      ADC1 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC2 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC3 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC4 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC5 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC6 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC7 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC8 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC9 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC10 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC11 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC12 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC13 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC14 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC15 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC16 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC17 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC18 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC19 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC20 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC21 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC22 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC23 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      ADC24 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      AXI4_Lite_ACLK : IN STD_LOGIC;
      AXI4_Lite_ARESETN : IN STD_LOGIC;
      AXI4_Lite_AWADDR : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      AXI4_Lite_AWVALID : IN STD_LOGIC;
      AXI4_Lite_WDATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      AXI4_Lite_WSTRB : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      AXI4_Lite_WVALID : IN STD_LOGIC;
      AXI4_Lite_BREADY : IN STD_LOGIC;
      AXI4_Lite_ARADDR : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      AXI4_Lite_ARVALID : IN STD_LOGIC;
      AXI4_Lite_RREADY : IN STD_LOGIC;
      alpha1 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha2 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha3 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha4 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha5 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha6 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha7 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha8 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha9 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha10 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha11 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha12 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha13 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha14 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha15 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha16 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha17 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha18 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha19 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha20 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha21 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha22 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha23 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      alpha24 : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
      AXI4_Lite_AWREADY : OUT STD_LOGIC;
      AXI4_Lite_WREADY : OUT STD_LOGIC;
      AXI4_Lite_BRESP : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      AXI4_Lite_BVALID : OUT STD_LOGIC;
      AXI4_Lite_ARREADY : OUT STD_LOGIC;
      AXI4_Lite_RDATA : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      AXI4_Lite_RRESP : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      AXI4_Lite_RVALID : OUT STD_LOGIC
    );
  END COMPONENT Conv_ADC_slow_Full;
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_RVALID: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_RRESP: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_RDATA: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_ARREADY: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_BVALID: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_BRESP: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_WREADY: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_AWREADY: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_RREADY: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_ARVALID: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_ARADDR: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_BREADY: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_WVALID: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_WSTRB: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_WDATA: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_AWVALID: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite AWVALID";
  ATTRIBUTE X_INTERFACE_PARAMETER OF AXI4_Lite_AWADDR: SIGNAL IS "XIL_INTERFACENAME AXI4_Lite, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 16, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN m2c_control_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_AWADDR: SIGNAL IS "xilinx.com:interface:aximm:1.0 AXI4_Lite AWADDR";
  ATTRIBUTE X_INTERFACE_PARAMETER OF AXI4_Lite_ARESETN: SIGNAL IS "XIL_INTERFACENAME AXI4_Lite_signal_reset, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_ARESETN: SIGNAL IS "xilinx.com:signal:reset:1.0 AXI4_Lite_signal_reset RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF AXI4_Lite_ACLK: SIGNAL IS "XIL_INTERFACENAME AXI4_Lite_signal_clock, ASSOCIATED_BUSIF AXI4_Lite, ASSOCIATED_RESET AXI4_Lite_ARESETN, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN m2c_control_processing_system7_0_0_FCLK_CLK0";
  ATTRIBUTE X_INTERFACE_INFO OF AXI4_Lite_ACLK: SIGNAL IS "xilinx.com:signal:clock:1.0 AXI4_Lite_signal_clock CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF IPCORE_RESETN: SIGNAL IS "XIL_INTERFACENAME IPCORE_RESETN, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF IPCORE_RESETN: SIGNAL IS "xilinx.com:signal:reset:1.0 IPCORE_RESETN RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF IPCORE_CLK: SIGNAL IS "XIL_INTERFACENAME IPCORE_CLK, ASSOCIATED_RESET IPCORE_RESETN, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN m2c_control_processing_system7_0_0_FCLK_CLK0";
  ATTRIBUTE X_INTERFACE_INFO OF IPCORE_CLK: SIGNAL IS "xilinx.com:signal:clock:1.0 IPCORE_CLK CLK";
BEGIN
  U0 : Conv_ADC_slow_Full
    PORT MAP (
      IPCORE_CLK => IPCORE_CLK,
      IPCORE_RESETN => IPCORE_RESETN,
      ADC1 => ADC1,
      ADC2 => ADC2,
      ADC3 => ADC3,
      ADC4 => ADC4,
      ADC5 => ADC5,
      ADC6 => ADC6,
      ADC7 => ADC7,
      ADC8 => ADC8,
      ADC9 => ADC9,
      ADC10 => ADC10,
      ADC11 => ADC11,
      ADC12 => ADC12,
      ADC13 => ADC13,
      ADC14 => ADC14,
      ADC15 => ADC15,
      ADC16 => ADC16,
      ADC17 => ADC17,
      ADC18 => ADC18,
      ADC19 => ADC19,
      ADC20 => ADC20,
      ADC21 => ADC21,
      ADC22 => ADC22,
      ADC23 => ADC23,
      ADC24 => ADC24,
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      AXI4_Lite_ARESETN => AXI4_Lite_ARESETN,
      AXI4_Lite_AWADDR => AXI4_Lite_AWADDR,
      AXI4_Lite_AWVALID => AXI4_Lite_AWVALID,
      AXI4_Lite_WDATA => AXI4_Lite_WDATA,
      AXI4_Lite_WSTRB => AXI4_Lite_WSTRB,
      AXI4_Lite_WVALID => AXI4_Lite_WVALID,
      AXI4_Lite_BREADY => AXI4_Lite_BREADY,
      AXI4_Lite_ARADDR => AXI4_Lite_ARADDR,
      AXI4_Lite_ARVALID => AXI4_Lite_ARVALID,
      AXI4_Lite_RREADY => AXI4_Lite_RREADY,
      alpha1 => alpha1,
      alpha2 => alpha2,
      alpha3 => alpha3,
      alpha4 => alpha4,
      alpha5 => alpha5,
      alpha6 => alpha6,
      alpha7 => alpha7,
      alpha8 => alpha8,
      alpha9 => alpha9,
      alpha10 => alpha10,
      alpha11 => alpha11,
      alpha12 => alpha12,
      alpha13 => alpha13,
      alpha14 => alpha14,
      alpha15 => alpha15,
      alpha16 => alpha16,
      alpha17 => alpha17,
      alpha18 => alpha18,
      alpha19 => alpha19,
      alpha20 => alpha20,
      alpha21 => alpha21,
      alpha22 => alpha22,
      alpha23 => alpha23,
      alpha24 => alpha24,
      AXI4_Lite_AWREADY => AXI4_Lite_AWREADY,
      AXI4_Lite_WREADY => AXI4_Lite_WREADY,
      AXI4_Lite_BRESP => AXI4_Lite_BRESP,
      AXI4_Lite_BVALID => AXI4_Lite_BVALID,
      AXI4_Lite_ARREADY => AXI4_Lite_ARREADY,
      AXI4_Lite_RDATA => AXI4_Lite_RDATA,
      AXI4_Lite_RRESP => AXI4_Lite_RRESP,
      AXI4_Lite_RVALID => AXI4_Lite_RVALID
    );
END m2c_control_Conv_ADC_slow_Full_0_0_arch;
