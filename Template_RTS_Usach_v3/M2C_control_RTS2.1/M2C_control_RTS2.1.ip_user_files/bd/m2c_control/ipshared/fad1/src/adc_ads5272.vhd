----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/16/2019 11:04:53 AM
-- Design Name: 
-- Module Name: adc_ads5272 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity adc_ads5272 is
    Port (
        adclk_P               : IN STD_LOGIC;
        adclk_N               : IN STD_LOGIC;
        lclk_P                : IN STD_LOGIC;
        lclk_N                : IN STD_LOGIC;
        adc_P                 : IN STD_LOGIC_VECTOR(7 downto 0);
        adc_N                 : IN STD_LOGIC_VECTOR(7 downto 0);
        reset_n               : IN STD_LOGIC;
        shift                 : IN STD_LOGIC;
        adclk_out             : OUT STD_LOGIC;
        data_input_1_ch_1     : OUT STD_LOGIC_VECTOR(11 downto 0);
        data_input_1_ch_2     : OUT STD_LOGIC_VECTOR(11 downto 0);
        data_input_1_ch_3     : OUT STD_LOGIC_VECTOR(11 downto 0);
        data_input_1_ch_4     : OUT STD_LOGIC_VECTOR(11 downto 0);
        data_input_2_ch_1     : OUT STD_LOGIC_VECTOR(11 downto 0);
        data_input_2_ch_2     : OUT STD_LOGIC_VECTOR(11 downto 0);
        data_input_2_ch_3     : OUT STD_LOGIC_VECTOR(11 downto 0);
        data_input_2_ch_4     : OUT STD_LOGIC_VECTOR(11 downto 0)
);
end adc_ads5272;

architecture Behavioral of adc_ads5272 is

    signal adclk_S    :std_logic := '0';
    signal lc1k_S     :std_logic := '0';
    signal adc1_S     :std_logic := '0';
    signal adc2_S     :std_logic := '0';  
    signal adc3_S     :std_logic := '0';
    signal adc4_S     :std_logic := '0';    
    signal adc5_S     :std_logic := '0';    
    signal adc6_S     :std_logic := '0';    
    signal adc7_S     :std_logic := '0';    
    signal adc8_S     :std_logic := '0';
      
  -- Buffer data component
    component IBUFDS
        generic(
            DIFF_TERM : boolean :=TRUE
        );
        port (
            O: out std_logic;
            I: in std_logic;
            IB: in std_logic
        );
    end component;
    
    
  -- Buffer clock component
      component IBUFGDS
          generic(
              DIFF_TERM : boolean :=TRUE
          );
          port (
              O: out std_logic;
              I: in std_logic;
              IB: in std_logic
          );
      end component;    
    
    -- Fast ADC protocol component
    component fast_adc is
        port (
            reset_n   : IN STD_LOGIC;
            shift     : IN STD_LOGIC;            
            adclk     : IN STD_LOGIC;
            lclk     : IN STD_LOGIC;
            adc1     : IN STD_LOGIC;
            adc2     : IN STD_LOGIC;
            adc3     : IN STD_LOGIC;
            adc4     : IN STD_LOGIC;
            adc5     : IN STD_LOGIC;
            adc6     : IN STD_LOGIC;
            adc7     : IN STD_LOGIC;
            adc8     : IN STD_LOGIC;
            data1     : OUT STD_LOGIC_VECTOR(11 downto 0);
            data2     : OUT STD_LOGIC_VECTOR(11 downto 0);
            data3     : OUT STD_LOGIC_VECTOR(11 downto 0);
            data4     : OUT STD_LOGIC_VECTOR(11 downto 0);
            data5     : OUT STD_LOGIC_VECTOR(11 downto 0);
            data6     : OUT STD_LOGIC_VECTOR(11 downto 0);
            data7     : OUT STD_LOGIC_VECTOR(11 downto 0);
            data8     : OUT STD_LOGIC_VECTOR(11 downto 0)
        );
    end component;

begin
    ibufds_adclk    : ibufds    --IBUFGDS
    generic map (DIFF_TERM=>TRUE)
    port map (
        O => adclk_S,
        I => adclk_P,
        IB => adclk_N
    );
    
    ibufds_lclk    : IBUFGDS --ibufds
    generic map (DIFF_TERM=>TRUE)
    port map (
        O => lc1k_S,
        I => lclk_P,
        IB => lclk_N
    );    
    
    ibufds_adc1    : ibufds
    generic map (DIFF_TERM=>TRUE)
    port map (
        O => adc1_S,
        I => adc_P(0),
        IB => adc_N(0)
    );

    ibufds_adc2    : ibufds
    generic map (DIFF_TERM=>TRUE)
    port map (
        O => adc2_S,
        I => adc_P(1),
        IB => adc_N(1)
    );

    ibufds_adc3    : ibufds
    generic map (DIFF_TERM=>TRUE)
    port map (
        O => adc3_S,
        I => adc_P(2),
        IB => adc_N(2)
    );

    ibufds_adc4    : ibufds
    generic map (DIFF_TERM=>TRUE)
    port map (
        O => adc4_S,
        I => adc_P(3),
        IB => adc_N(3)
    );

    ibufds_adc5    : ibufds
    generic map (DIFF_TERM=>TRUE)
    port map (
        O => adc5_S,
        I => adc_P(4),
        IB => adc_N(4)
    );

    ibufds_adc6    : ibufds
    generic map (DIFF_TERM=>TRUE)
    port map (
        O => adc6_S,
        I => adc_P(5),
        IB => adc_N(5)
    );

    ibufds_adc7    : ibufds
    generic map (DIFF_TERM=>TRUE)
    port map (
        O => adc7_S,
        I => adc_P(6),
        IB => adc_N(6)
    );

    ibufds_adc8    : ibufds
    generic map (DIFF_TERM=>TRUE)
    port map (
        O => adc8_S,
        I => adc_P(7),
        IB => adc_N(7)
    );
    
    fast_adc_0  : fast_adc
    port map (
        reset_n     => reset_n,
        shift       => shift,
        adclk       => adclk_S,
        lclk        => lc1k_S,
        adc1        => adc1_S,
        adc2        => adc2_S,
        adc3        => adc3_S,
        adc4        => adc4_S,
        adc5        => adc5_S,
        adc6        => adc6_S,
        adc7        => adc7_S,
        adc8        => adc8_S,        
        data1       => data_input_2_ch_4,
        data2       => data_input_2_ch_3,
        data3       => data_input_2_ch_2,
        data4       => data_input_2_ch_1,
        data5       => data_input_1_ch_4,
        data6       => data_input_1_ch_3,
        data7       => data_input_1_ch_2,
        data8       => data_input_1_ch_1
        );
        
    adclk_out <= adclk_S;
end Behavioral;
