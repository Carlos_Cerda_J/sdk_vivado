----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.10.2018 11:40:44
-- Design Name: 
-- Module Name: fast_adc - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fast_adc is
  Port (
  reset_n   : IN STD_LOGIC;
  shift     : IN STD_LOGIC;
  adclk     : IN STD_LOGIC;
  lclk     : IN STD_LOGIC;
  adc1     : IN STD_LOGIC;
  adc2     : IN STD_LOGIC;
  adc3     : IN STD_LOGIC;
  adc4     : IN STD_LOGIC;
  adc5     : IN STD_LOGIC;
  adc6     : IN STD_LOGIC;
  adc7     : IN STD_LOGIC;
  adc8     : IN STD_LOGIC;
  data1     : OUT STD_LOGIC_VECTOR(11 downto 0);
  data2     : OUT STD_LOGIC_VECTOR(11 downto 0);
  data3     : OUT STD_LOGIC_VECTOR(11 downto 0);
  data4     : OUT STD_LOGIC_VECTOR(11 downto 0);
  data5     : OUT STD_LOGIC_VECTOR(11 downto 0);
  data6     : OUT STD_LOGIC_VECTOR(11 downto 0);
  data7     : OUT STD_LOGIC_VECTOR(11 downto 0);
  data8     : OUT STD_LOGIC_VECTOR(11 downto 0)
  );
end fast_adc;

architecture Behavioral of fast_adc is
    signal data1_pos_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data1_neg_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data2_pos_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data2_neg_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data3_pos_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data3_neg_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data4_pos_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data4_neg_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data5_pos_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data5_neg_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data6_pos_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data6_neg_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data7_pos_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data7_neg_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data8_pos_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');
    signal data8_neg_S  : STD_LOGIC_VECTOR(6 downto 0) := (others => '0');                           
    signal data1_S  : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');
    signal data2_S  : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');
    signal data3_S  : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');
    signal data4_S  : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');
    signal data5_S  : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');
    signal data6_S  : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');
    signal data7_S  : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');
    signal data8_S  : STD_LOGIC_VECTOR(11 downto 0) := (others => '0');
    
begin
    posedge_process_p : process (lclk, reset_n) is
    begin
        if (reset_n = '0') then
            data1_pos_S <= (others => '0');
            data2_pos_S <= (others => '0');
            data3_pos_S <= (others => '0');
            data4_pos_S <= (others => '0');
            data5_pos_S <= (others => '0');
            data6_pos_S <= (others => '0');
            data7_pos_S <= (others => '0');
            data8_pos_S <= (others => '0');
        elsif (rising_edge(lclk)) then
            data1_pos_S <= adc1 & data1_pos_S(6 downto 1); 
            data2_pos_S <= adc2 & data2_pos_S(6 downto 1);
            data3_pos_S <= adc3 & data3_pos_S(6 downto 1);
            data4_pos_S <= adc4 & data4_pos_S(6 downto 1);
            data5_pos_S <= adc5 & data5_pos_S(6 downto 1);
            data6_pos_S <= adc6 & data6_pos_S(6 downto 1);
            data7_pos_S <= adc7 & data7_pos_S(6 downto 1);
            data8_pos_S <= adc8 & data8_pos_S(6 downto 1);
        end if;                                                                            
    end process;

    negedge_process_p : process (lclk, reset_n) is
    begin
        if (reset_n = '0') then
            data1_neg_S <= (others => '0');
            data2_neg_S <= (others => '0');
            data3_neg_S <= (others => '0');
            data4_neg_S <= (others => '0');
            data5_neg_S <= (others => '0');
            data6_neg_S <= (others => '0');
            data7_neg_S <= (others => '0');
            data8_neg_S <= (others => '0');
        elsif (falling_edge(lclk)) then
            data1_neg_S <= adc1 & data1_neg_S(6 downto 1); 
            data2_neg_S <= adc2 & data2_neg_S(6 downto 1);
            data3_neg_S <= adc3 & data3_neg_S(6 downto 1);
            data4_neg_S <= adc4 & data4_neg_S(6 downto 1);
            data5_neg_S <= adc5 & data5_neg_S(6 downto 1);
            data6_neg_S <= adc6 & data6_neg_S(6 downto 1);
            data7_neg_S <= adc7 & data7_neg_S(6 downto 1);
            data8_neg_S <= adc8 & data8_neg_S(6 downto 1);
        end if;                                                                            
    end process;
 
    new_data_process : process (adclk, reset_n) is
    begin
        if (reset_n = '0') then
            data1_S <= (others => '0');
            data2_S <= (others => '0');
            data3_S <= (others => '0');
            data4_S <= (others => '0');
            data5_S <= (others => '0');
            data6_S <= (others => '0');
            data7_S <= (others => '0');
            data8_S <= (others => '0');
        elsif (rising_edge(adclk)) then
            if(shift = '0') then
                data1_S <= data1_pos_S(6) & data1_neg_S(6) & data1_pos_S(5) & data1_neg_S(5) & data1_pos_S(4) & data1_neg_S(4) & data1_pos_S(3) & data1_neg_S(3) & data1_pos_S(2) & data1_neg_S(2) & data1_pos_S(1) & data1_neg_S(1);
                data2_S <= data2_pos_S(6) & data2_neg_S(6) & data2_pos_S(5) & data2_neg_S(5) & data2_pos_S(4) & data2_neg_S(4) & data2_pos_S(3) & data2_neg_S(3) & data2_pos_S(2) & data2_neg_S(2) & data2_pos_S(1) & data2_neg_S(1);
                data3_S <= data3_pos_S(6) & data3_neg_S(6) & data3_pos_S(5) & data3_neg_S(5) & data3_pos_S(4) & data3_neg_S(4) & data3_pos_S(3) & data3_neg_S(3) & data3_pos_S(2) & data3_neg_S(2) & data3_pos_S(1) & data3_neg_S(1);
                data4_S <= data4_pos_S(6) & data4_neg_S(6) & data4_pos_S(5) & data4_neg_S(5) & data4_pos_S(4) & data4_neg_S(4) & data4_pos_S(3) & data4_neg_S(3) & data4_pos_S(2) & data4_neg_S(2) & data4_pos_S(1) & data4_neg_S(1);
                data5_S <= data5_pos_S(6) & data5_neg_S(6) & data5_pos_S(5) & data5_neg_S(5) & data5_pos_S(4) & data5_neg_S(4) & data5_pos_S(3) & data5_neg_S(3) & data5_pos_S(2) & data5_neg_S(2) & data5_pos_S(1) & data5_neg_S(1);
                data6_S <= data6_pos_S(6) & data6_neg_S(6) & data6_pos_S(5) & data6_neg_S(5) & data6_pos_S(4) & data6_neg_S(4) & data6_pos_S(3) & data6_neg_S(3) & data6_pos_S(2) & data6_neg_S(2) & data6_pos_S(1) & data6_neg_S(1);
                data7_S <= data7_pos_S(6) & data7_neg_S(6) & data7_pos_S(5) & data7_neg_S(5) & data7_pos_S(4) & data7_neg_S(4) & data7_pos_S(3) & data7_neg_S(3) & data7_pos_S(2) & data7_neg_S(2) & data7_pos_S(1) & data7_neg_S(1);
                data8_S <= data8_pos_S(6) & data8_neg_S(6) & data8_pos_S(5) & data8_neg_S(5) & data8_pos_S(4) & data8_neg_S(4) & data8_pos_S(3) & data8_neg_S(3) & data8_pos_S(2) & data8_neg_S(2) & data8_pos_S(1) & data8_neg_S(1);
            else
                data1_S <= data1_neg_S(6) & data1_pos_S(5) & data1_neg_S(5) & data1_pos_S(4) & data1_neg_S(4) & data1_pos_S(3) & data1_neg_S(3) & data1_pos_S(2) & data1_neg_S(2) & data1_pos_S(1) & data1_neg_S(1) & data1_pos_S(0);
                data2_S <= data2_neg_S(6) & data2_pos_S(5) & data2_neg_S(5) & data2_pos_S(4) & data2_neg_S(4) & data2_pos_S(3) & data2_neg_S(3) & data2_pos_S(2) & data2_neg_S(2) & data2_pos_S(1) & data2_neg_S(1) & data2_pos_S(0);
                data3_S <= data3_neg_S(6) & data3_pos_S(5) & data3_neg_S(5) & data3_pos_S(4) & data3_neg_S(4) & data3_pos_S(3) & data3_neg_S(3) & data3_pos_S(2) & data3_neg_S(2) & data3_pos_S(1) & data3_neg_S(1) & data3_pos_S(0);
                data4_S <= data4_neg_S(6) & data4_pos_S(5) & data4_neg_S(5) & data4_pos_S(4) & data4_neg_S(4) & data4_pos_S(3) & data4_neg_S(3) & data4_pos_S(2) & data4_neg_S(2) & data4_pos_S(1) & data4_neg_S(1) & data4_pos_S(0);
                data5_S <= data5_neg_S(6) & data5_pos_S(5) & data5_neg_S(5) & data5_pos_S(4) & data5_neg_S(4) & data5_pos_S(3) & data5_neg_S(3) & data5_pos_S(2) & data5_neg_S(2) & data5_pos_S(1) & data5_neg_S(1) & data5_pos_S(0);
                data6_S <= data6_neg_S(6) & data6_pos_S(5) & data6_neg_S(5) & data6_pos_S(4) & data6_neg_S(4) & data6_pos_S(3) & data6_neg_S(3) & data6_pos_S(2) & data6_neg_S(2) & data6_pos_S(1) & data6_neg_S(1) & data6_pos_S(0);
                data7_S <= data7_neg_S(6) & data7_pos_S(5) & data7_neg_S(5) & data7_pos_S(4) & data7_neg_S(4) & data7_pos_S(3) & data7_neg_S(3) & data7_pos_S(2) & data7_neg_S(2) & data7_pos_S(1) & data7_neg_S(1) & data7_pos_S(0);
                data8_S <= data8_neg_S(6) & data8_pos_S(5) & data8_neg_S(5) & data8_pos_S(4) & data8_neg_S(4) & data8_pos_S(3) & data8_neg_S(3) & data8_pos_S(2) & data8_neg_S(2) & data8_pos_S(1) & data8_neg_S(1) & data8_pos_S(0);
            end if;      
        end if;
    end process;
    
    data1 <= data1_S;
    data2 <= data2_S;
    data3 <= data3_S;
    data4 <= data4_S;
    data5 <= data5_S;
    data6 <= data6_S;
    data7 <= data7_S;
    data8 <= data8_S;
end Behavioral;
